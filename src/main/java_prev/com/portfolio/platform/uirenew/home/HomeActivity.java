package com.portfolio.platform.uirenew.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.er4;
import com.fossil.jr4;
import com.fossil.la3;
import com.fossil.pa3;
import com.fossil.sk2;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

import static android.provider.AlarmClock.ACTION_SET_ALARM;
import static android.provider.AlarmClock.ACTION_SET_TIMER;
import static android.provider.AlarmClock.ACTION_SHOW_ALARMS;

@DexEdit(defaultAction = DexAction.ADD)
public final class HomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((er4) null);
    @DexIgnore
    public HomePresenter B;

    @DexIgnore
    public static final class a {
        @DexIgnore
        public a() {
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        public final void a(Context context) {
            jr4.b(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(er4 er4) {
            this();
        }
    }

    @DexIgnore
    HomeActivity(){}

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        HomeActivity.super.onActivityResult(i, i2, intent);
        HomePresenter homePresenter = this.B;
        if (homePresenter != null) {
            homePresenter.a(i, i2, intent);
        } else {
            jr4.d("mPresenter");
            throw null;
        }
    }

    @SuppressLint("ResourceType")
    @DexReplace
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.home.HomeActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        la3 a2 = (la3) getSupportFragmentManager().a(sk2.content);
        if (a2 == null) {
            a2 = la3.y.a();
            a((Fragment) a2, (int) sk2.content);
        }
        PortfolioApp.Z.c().g().a(new pa3(a2)).a(this);

        // Ref https://github.com/carlosperate/LightUpDroid-Alarm/blob/master/app/src/main/java/com/embeddedlog/LightUpDroid/HandleApiCalls.java
        Intent intent = getIntent();
        if (intent != null) {
            if (ACTION_SET_ALARM.equals(intent.getAction())) {
                // handleSetAlarm(intent);
            } else if (ACTION_SHOW_ALARMS.equals(intent.getAction())) {
                // handleShowAlarms();
                HomePresenter homePresenter = this.B;
                homePresenter.a(2); // setTab 2
                return;
            } else if (ACTION_SET_TIMER.equals(intent.getAction())) {
                // handleSetTimer(intent);
            }
        }



        if (bundle != null) {
            HomePresenter homePresenter = this.B;
            if (homePresenter != null) {
                homePresenter.a(bundle.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
            } else {
                jr4.d("mPresenter");
                throw null;
            }
        } else {
            // Intent intent = getIntent();
            if (intent != null && intent.getExtras() != null) {
                Intent intent2 = getIntent();
                if (intent2 != null) {
                    Bundle extras = intent2.getExtras();
                    if (extras == null) {
                        jr4.a();
                        throw null;
                    } else if (extras.containsKey("OUT_STATE_DASHBOARD_CURRENT_TAB")) {
                        HomePresenter homePresenter2 = this.B;
                        if (homePresenter2 != null) {
                            Intent intent3 = getIntent();
                            if (intent3 != null) {
                                Bundle extras2 = intent3.getExtras();
                                if (extras2 != null) {
                                    homePresenter2.a(extras2.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
                                } else {
                                    jr4.a();
                                    throw null;
                                }
                            } else {
                                jr4.a();
                                throw null;
                            }
                        } else {
                            jr4.d("mPresenter");
                            throw null;
                        }
                    }
                } else {
                    jr4.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        jr4.b(bundle, "outState");
        HomePresenter homePresenter = this.B;
        if (homePresenter != null) {
            bundle.putInt("OUT_STATE_DASHBOARD_CURRENT_TAB", homePresenter.h());
            HomeActivity.super.onSaveInstanceState(bundle);
            return;
        }
        jr4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        throw null;
        // a((Class<? extends T>[]) new Class[]{MFDeviceService.class, ButtonService.class});
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        throw null;
        // b((Class<? extends T>[]) new Class[]{MFDeviceService.class, ButtonService.class});
    }
}
