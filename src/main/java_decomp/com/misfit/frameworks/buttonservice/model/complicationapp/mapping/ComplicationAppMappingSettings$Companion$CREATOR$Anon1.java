package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<ComplicationAppMappingSettings> {
    @DexIgnore
    public ComplicationAppMappingSettings createFromParcel(Parcel parcel) {
        wg6.b(parcel, "parcel");
        return new ComplicationAppMappingSettings(parcel, (qg6) null);
    }

    @DexIgnore
    public ComplicationAppMappingSettings[] newArray(int i) {
        return new ComplicationAppMappingSettings[i];
    }
}
