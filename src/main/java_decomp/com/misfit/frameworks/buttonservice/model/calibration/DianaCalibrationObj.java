package com.misfit.frameworks.buttonservice.model.calibration;

import com.fossil.kc6;
import com.fossil.n50;
import com.fossil.p50;
import com.fossil.q50;
import com.fossil.qg6;
import com.fossil.r50;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCalibrationObj {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ n50 handId;
    @DexIgnore
    public /* final */ p50 handMovingDirection;
    @DexIgnore
    public /* final */ q50 handMovingSpeed;
    @DexIgnore
    public /* final */ r50 handMovingType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[CalibrationEnums.MovingType.values().length];
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1; // = new int[CalibrationEnums.HandId.values().length];
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$2; // = new int[CalibrationEnums.Direction.values().length];
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$3; // = new int[CalibrationEnums.Speed.values().length];

            /*
            static {
                $EnumSwitchMapping$0[CalibrationEnums.MovingType.DISTANCE.ordinal()] = 1;
                $EnumSwitchMapping$0[CalibrationEnums.MovingType.POSITION.ordinal()] = 2;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.HOUR.ordinal()] = 1;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.MINUTE.ordinal()] = 2;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.SUB_EYE.ordinal()] = 3;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.CLOCKWISE.ordinal()] = 1;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.COUNTER_CLOCKWISE.ordinal()] = 2;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.SHORTEST_PATH.ordinal()] = 3;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.SIXTEENTH.ordinal()] = 1;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.EIGHTH.ordinal()] = 2;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.QUARTER.ordinal()] = 3;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.HALF.ordinal()] = 4;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.FULL.ordinal()] = 5;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final DianaCalibrationObj consume(HandCalibrationObj handCalibrationObj) {
            r50 r50;
            n50 n50;
            p50 p50;
            q50 q50;
            wg6.b(handCalibrationObj, "handCalibrationObj");
            int i = WhenMappings.$EnumSwitchMapping$0[handCalibrationObj.getMovingType().ordinal()];
            if (i == 1) {
                r50 = r50.DISTANCE;
            } else if (i == 2) {
                r50 = r50.POSITION;
            } else {
                throw new kc6();
            }
            r50 r502 = r50;
            int i2 = WhenMappings.$EnumSwitchMapping$1[handCalibrationObj.getHandId().ordinal()];
            if (i2 == 1) {
                n50 = n50.HOUR;
            } else if (i2 == 2) {
                n50 = n50.MINUTE;
            } else if (i2 == 3) {
                n50 = n50.SUB_EYE;
            } else {
                throw new kc6();
            }
            n50 n502 = n50;
            int i3 = WhenMappings.$EnumSwitchMapping$2[handCalibrationObj.getDirection().ordinal()];
            if (i3 == 1) {
                p50 = p50.CLOCKWISE;
            } else if (i3 == 2) {
                p50 = p50.COUNTER_CLOCKWISE;
            } else if (i3 == 3) {
                p50 = p50.SHORTEST_PATH;
            } else {
                throw new kc6();
            }
            p50 p502 = p50;
            int i4 = WhenMappings.$EnumSwitchMapping$3[handCalibrationObj.getSpeed().ordinal()];
            if (i4 == 1) {
                q50 = q50.SIXTEENTH;
            } else if (i4 == 2) {
                q50 = q50.EIGHTH;
            } else if (i4 == 3) {
                q50 = q50.QUARTER;
            } else if (i4 == 4) {
                q50 = q50.HALF;
            } else if (i4 == 5) {
                q50 = q50.FULL;
            } else {
                throw new kc6();
            }
            return new DianaCalibrationObj(r502, n502, p502, q50, handCalibrationObj.getDegree());
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DianaCalibrationObj(r50 r50, n50 n50, p50 p50, q50 q50, int i) {
        wg6.b(r50, "handMovingType");
        wg6.b(n50, "handId");
        wg6.b(p50, "handMovingDirection");
        wg6.b(q50, "handMovingSpeed");
        this.handMovingType = r50;
        this.handId = n50;
        this.handMovingDirection = p50;
        this.handMovingSpeed = q50;
        this.degree = i;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final n50 getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final p50 getHandMovingDirection() {
        return this.handMovingDirection;
    }

    @DexIgnore
    public final q50 getHandMovingSpeed() {
        return this.handMovingSpeed;
    }

    @DexIgnore
    public final r50 getHandMovingType() {
        return this.handMovingType;
    }
}
