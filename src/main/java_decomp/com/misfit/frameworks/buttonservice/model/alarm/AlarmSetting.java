package com.misfit.frameworks.buttonservice.model.alarm;

import com.fossil.c50;
import com.fossil.d50;
import com.fossil.e50;
import com.fossil.h50;
import com.fossil.i50;
import com.fossil.k50;
import com.fossil.k70;
import com.fossil.kc6;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.wg6;
import com.fossil.yd6;
import com.fossil.z40;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmSetting {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HashSet<AlarmDay> alarmDays;
    @DexIgnore
    public /* final */ int hour;
    @DexIgnore
    public /* final */ boolean isRepeat;
    @DexIgnore
    public /* final */ String message;
    @DexIgnore
    public /* final */ int minute;
    @DexIgnore
    public /* final */ String title;

    @DexIgnore
    public enum AlarmDay {
        SUNDAY(1),
        MONDAY(2),
        TUESDAY(3),
        WEDNESDAY(4),
        THURSDAY(5),
        FRIDAY(6),
        SATURDAY(7);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public AlarmDay(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[AlarmDay.values().length];

        /*
        static {
            $EnumSwitchMapping$0[AlarmDay.SUNDAY.ordinal()] = 1;
            $EnumSwitchMapping$0[AlarmDay.MONDAY.ordinal()] = 2;
            $EnumSwitchMapping$0[AlarmDay.TUESDAY.ordinal()] = 3;
            $EnumSwitchMapping$0[AlarmDay.WEDNESDAY.ordinal()] = 4;
            $EnumSwitchMapping$0[AlarmDay.THURSDAY.ordinal()] = 5;
            $EnumSwitchMapping$0[AlarmDay.FRIDAY.ordinal()] = 6;
            $EnumSwitchMapping$0[AlarmDay.SATURDAY.ordinal()] = 7;
        }
        */
    }

    /*
    static {
        String name = AlarmSetting.class.getName();
        wg6.a((Object) name, "AlarmSetting::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public AlarmSetting(String str, String str2, int i, int i2, boolean z, HashSet<AlarmDay> hashSet) {
        this.title = str;
        this.message = str2;
        this.hour = i;
        this.minute = i2;
        this.isRepeat = z;
        this.alarmDays = hashSet;
    }

    @DexIgnore
    private final k70 convertCalendarDayToSDKV2Day(AlarmDay alarmDay) {
        switch (WhenMappings.$EnumSwitchMapping$0[alarmDay.ordinal()]) {
            case 1:
                return k70.SUNDAY;
            case 2:
                return k70.MONDAY;
            case 3:
                return k70.TUESDAY;
            case 4:
                return k70.WEDNESDAY;
            case 5:
                return k70.THURSDAY;
            case 6:
                return k70.FRIDAY;
            case 7:
                return k70.SATURDAY;
            default:
                throw new kc6();
        }
    }

    @DexIgnore
    public final HashSet<AlarmDay> getAlarmDays() {
        return this.alarmDays;
    }

    @DexIgnore
    public final int getAlarmDaysAsInt() {
        HashSet<AlarmDay> hashSet = this.alarmDays;
        int i = 0;
        if (hashSet != null) {
            for (AlarmDay value : hashSet) {
                i |= value.getValue();
            }
        }
        return i;
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public final boolean isRepeat() {
        return this.isRepeat;
    }

    @DexIgnore
    public final z40 toSDKV2Setting() {
        String str = this.title;
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        k50 k50 = new k50(str);
        String str3 = this.message;
        if (str3 != null) {
            str2 = str3;
        }
        c50 c50 = new c50(str2);
        HashSet<AlarmDay> hashSet = this.alarmDays;
        k70 k70 = null;
        if (hashSet == null) {
            return new d50(new e50((byte) this.hour, (byte) this.minute, (k70) null), k50, c50);
        }
        if (hashSet.isEmpty()) {
            return new d50(new e50((byte) this.hour, (byte) this.minute, (k70) null), k50, c50);
        }
        if (this.isRepeat) {
            HashSet<AlarmDay> hashSet2 = this.alarmDays;
            ArrayList arrayList = new ArrayList(rd6.a(hashSet2, 10));
            for (AlarmDay convertCalendarDayToSDKV2Day : hashSet2) {
                arrayList.add(convertCalendarDayToSDKV2Day(convertCalendarDayToSDKV2Day));
            }
            k70 = new h50(new i50((byte) this.hour, (byte) this.minute, yd6.p(arrayList)), k50, c50);
        } else if (this.alarmDays.size() > 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local.e(str4, ".toSDKV2Setting(), the alarm setting is wrong format, alarmSetting=" + this);
        } else {
            if (!this.alarmDays.isEmpty()) {
                k70 = convertCalendarDayToSDKV2Day((AlarmDay) yd6.m(this.alarmDays).get(0));
            }
            return new d50(new e50((byte) this.hour, (byte) this.minute, k70), k50, c50);
        }
        return k70;
    }

    @DexIgnore
    public String toString() {
        return "AlarmSetting{alarmHour=" + this.hour + ", alarmMinute=" + this.minute + ", alarmDays=" + this.alarmDays + ", alarmRepeat=" + this.isRepeat + "}";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public AlarmSetting(String str, String str2, int i, int i2) {
        this(str, str2, i, i2, false, new HashSet());
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, "message");
    }
}
