package com.misfit.frameworks.buttonservice.model;

import com.fossil.cd6;
import com.fossil.e90;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeSetQueue;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickCommandQueue {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int NOTIFICATION_THRESHOLD; // = 20;
    @DexIgnore
    public /* final */ Object locker; // = new Object();
    @DexIgnore
    public /* final */ HashMap<e90, DeviceAppResponse> mDeviceAppResponseMap; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, MusicResponse> mMusicResponseMap; // = new HashMap<>();
    @DexIgnore
    public SynchronizeSetQueue<NotificationBaseObj> mNotificationQueue; // = new SynchronizeSetQueue<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    private final void addForComplicationResponse(DeviceAppResponse deviceAppResponse) {
        DeviceAppResponse deviceAppResponse2 = this.mDeviceAppResponseMap.get(deviceAppResponse.getDeviceEventId());
        if (deviceAppResponse2 == null || deviceAppResponse.getLifeTimeObject().life() > deviceAppResponse2.getLifeTimeObject().life()) {
            this.mDeviceAppResponseMap.put(deviceAppResponse.getDeviceEventId(), deviceAppResponse);
        }
    }

    @DexIgnore
    private final void addForMusicResponse(MusicResponse musicResponse) {
        MusicResponse musicResponse2 = this.mMusicResponseMap.get(musicResponse.getType());
        if (musicResponse2 == null || musicResponse.getCreatedTime() > musicResponse2.getCreatedTime()) {
            this.mMusicResponseMap.put(musicResponse.getType(), musicResponse);
        }
    }

    @DexIgnore
    private final void addForNotificationResponse(NotificationBaseObj notificationBaseObj) {
        this.mNotificationQueue.add(notificationBaseObj);
        this.mNotificationQueue.sortWith(new QuickCommandQueue$addForNotificationResponse$$inlined$compareBy$Anon1());
        if (this.mNotificationQueue.getSize() > 20) {
            this.mNotificationQueue.poll();
        }
    }

    @DexIgnore
    private final DeviceAppResponse pollNextComplicationResponse() {
        Iterator<e90> it = this.mDeviceAppResponseMap.keySet().iterator();
        if (!it.hasNext()) {
            return null;
        }
        e90 next = it.next();
        wg6.a((Object) next, "iterator.next()");
        e90 e90 = next;
        DeviceAppResponse deviceAppResponse = this.mDeviceAppResponseMap.get(e90);
        this.mDeviceAppResponseMap.remove(e90);
        return deviceAppResponse;
    }

    @DexIgnore
    private final MusicResponse pollNextMusicResponse() {
        Iterator<String> it = this.mMusicResponseMap.keySet().iterator();
        if (!it.hasNext()) {
            return null;
        }
        String next = it.next();
        wg6.a((Object) next, "iterator.next()");
        String str = next;
        MusicResponse musicResponse = this.mMusicResponseMap.get(str);
        this.mMusicResponseMap.remove(str);
        return musicResponse;
    }

    @DexIgnore
    private final NotificationBaseObj pollNextNotification() {
        return this.mNotificationQueue.poll();
    }

    @DexIgnore
    public final void add(Object obj) {
        wg6.b(obj, Constants.COMMAND);
        synchronized (this.locker) {
            if (obj instanceof DeviceAppResponse) {
                addForComplicationResponse((DeviceAppResponse) obj);
            } else if (obj instanceof MusicResponse) {
                addForMusicResponse((MusicResponse) obj);
            } else if (obj instanceof NotificationBaseObj) {
                addForNotificationResponse((NotificationBaseObj) obj);
            }
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public final void clear() {
        synchronized (this.locker) {
            this.mDeviceAppResponseMap.clear();
            this.mMusicResponseMap.clear();
            this.mNotificationQueue.clear();
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public final Object poll() {
        Object pollNextComplicationResponse;
        synchronized (this.locker) {
            pollNextComplicationResponse = pollNextComplicationResponse();
            if (pollNextComplicationResponse == null) {
                pollNextComplicationResponse = pollNextMusicResponse();
            }
            if (pollNextComplicationResponse == null) {
                pollNextComplicationResponse = pollNextNotification();
            }
        }
        return pollNextComplicationResponse;
    }

    @DexIgnore
    public final void remove(Object obj) {
        wg6.b(obj, Constants.COMMAND);
        synchronized (this.locker) {
            if (obj instanceof DeviceAppResponse) {
                if (wg6.a(obj, (Object) this.mDeviceAppResponseMap.get(((DeviceAppResponse) obj).getDeviceEventId()))) {
                    this.mDeviceAppResponseMap.remove(((DeviceAppResponse) obj).getDeviceEventId());
                }
            } else if (obj instanceof MusicResponse) {
                if (wg6.a(obj, (Object) this.mMusicResponseMap.get(((MusicResponse) obj).getType()))) {
                    this.mMusicResponseMap.remove(((MusicResponse) obj).getType());
                }
            } else if (obj instanceof DianaNotificationObj) {
                this.mNotificationQueue.remove(obj);
            }
            cd6 cd6 = cd6.a;
        }
    }
}
