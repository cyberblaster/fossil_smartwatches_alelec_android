package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import com.fossil.ad0;
import com.fossil.da0;
import com.fossil.e90;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.tc0;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.fossil.z80;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherInfoWatchAppResponse extends DeviceAppResponse {
    @DexIgnore
    public /* final */ List<WeatherWatchAppInfo> listOfWeatherInfo;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3, int i, qg6 qg6) {
        this(weatherWatchAppInfo, (i & 2) != 0 ? null : weatherWatchAppInfo2, (i & 4) != 0 ? null : weatherWatchAppInfo3);
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        for (WeatherWatchAppInfo sDKWeatherAppInfo : list) {
            arrayList.add(sDKWeatherAppInfo.toSDKWeatherAppInfo());
        }
        Object[] array = arrayList.toArray(new z80[0]);
        if (array != null) {
            return new ad0((z80[]) array);
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof da0)) {
            return null;
        }
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        for (WeatherWatchAppInfo sDKWeatherAppInfo : list) {
            arrayList.add(sDKWeatherAppInfo.toSDKWeatherAppInfo());
        }
        da0 da0 = (da0) x90;
        Object[] array = arrayList.toArray(new z80[0]);
        if (array != null) {
            return new ad0(da0, (z80[]) array);
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.listOfWeatherInfo);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3) {
        super(e90.WEATHER_WATCH_APP);
        wg6.b(weatherWatchAppInfo, "firstWeatherWatchAppInfo");
        this.listOfWeatherInfo = new ArrayList();
        this.listOfWeatherInfo.add(weatherWatchAppInfo);
        if (weatherWatchAppInfo2 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo2);
        }
        if (weatherWatchAppInfo3 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo3);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.listOfWeatherInfo = new ArrayList();
        parcel.readTypedList(this.listOfWeatherInfo, WeatherWatchAppInfo.CREATOR);
    }
}
