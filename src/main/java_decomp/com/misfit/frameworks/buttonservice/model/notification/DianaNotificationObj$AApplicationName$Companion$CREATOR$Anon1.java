package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaNotificationObj$AApplicationName$Companion$CREATOR$Anon1 implements Parcelable.Creator<DianaNotificationObj.AApplicationName> {
    @DexIgnore
    public DianaNotificationObj.AApplicationName createFromParcel(Parcel parcel) {
        wg6.b(parcel, "parcel");
        return new DianaNotificationObj.AApplicationName(parcel);
    }

    @DexIgnore
    public DianaNotificationObj.AApplicationName[] newArray(int i) {
        return new DianaNotificationObj.AApplicationName[i];
    }
}
