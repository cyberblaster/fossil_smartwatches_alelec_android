package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class PairingResponse implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<PairingResponse> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public final PairingLinkServerResponse buildPairingLinkServerResponse(boolean z, int i) {
            return new PairingLinkServerResponse(z, i);
        }

        @DexIgnore
        public final PairingUpdateFWResponse buildPairingUpdateFWResponse(FirmwareData firmwareData) {
            wg6.b(firmwareData, "fwData");
            return new PairingUpdateFWResponse(firmwareData);
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public PairingResponse createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    wg6.a((Object) cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Parcel.class});
                    wg6.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(new Object[]{parcel});
                    if (newInstance != null) {
                        return (PairingResponse) newInstance;
                    }
                    throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.pairing.PairingResponse");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public PairingResponse[] newArray(int i) {
            return new PairingResponse[i];
        }
    }

    @DexIgnore
    public PairingResponse() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PairingResponse(Parcel parcel) {
        this();
        wg6.b(parcel, "parcel");
    }
}
