package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReplyMessageMappingGroup implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public String iconFwPath;
    @DexIgnore
    public List<ReplyMessageMapping> replyMessageList;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ReplyMessageMappingGroup> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public ReplyMessageMappingGroup createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new ReplyMessageMappingGroup(parcel);
        }

        @DexIgnore
        public ReplyMessageMappingGroup[] newArray(int i) {
            return new ReplyMessageMappingGroup[i];
        }
    }

    @DexIgnore
    public ReplyMessageMappingGroup(List<ReplyMessageMapping> list, String str) {
        wg6.b(list, "replyMessageList");
        wg6.b(str, "iconFwPath");
        this.replyMessageList = new ArrayList();
        this.replyMessageList = list;
        this.iconFwPath = str;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final List<ReplyMessageMapping> getReplyMessageList() {
        return this.replyMessageList;
    }

    @DexIgnore
    public final void setIconFwPath(String str) {
        wg6.b(str, "<set-?>");
        this.iconFwPath = str;
    }

    @DexIgnore
    public final void setReplyMessageList(List<ReplyMessageMapping> list) {
        wg6.b(list, "<set-?>");
        this.replyMessageList = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedList(this.replyMessageList);
            parcel.writeString(this.iconFwPath);
        }
    }

    @DexIgnore
    public ReplyMessageMappingGroup(Parcel parcel) {
        wg6.b(parcel, "parcel");
        this.replyMessageList = new ArrayList();
        this.replyMessageList = new ArrayList();
        parcel.readTypedList(this.replyMessageList, ReplyMessageMapping.CREATOR);
        String readString = parcel.readString();
        this.iconFwPath = readString == null ? "" : readString;
    }
}
