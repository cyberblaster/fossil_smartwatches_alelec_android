package com.misfit.frameworks.buttonservice.model.watchapp.response;

import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicResponseFactory {
    @DexIgnore
    public static /* final */ MusicResponseFactory INSTANCE; // = new MusicResponseFactory();

    @DexIgnore
    public final NotifyMusicEventResponse createMusicEventResponse(NotifyMusicEventResponse.MusicMediaAction musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus) {
        wg6.b(musicMediaAction, "musicAction");
        wg6.b(musicMediaStatus, "status");
        return new NotifyMusicEventResponse(musicMediaAction, musicMediaStatus);
    }

    @DexIgnore
    public final MusicTrackInfoResponse createMusicTrackInfoResponse(String str, byte b, String str2, String str3, String str4) {
        wg6.b(str, "appName");
        wg6.b(str2, "trackTitle");
        wg6.b(str3, "artistName");
        wg6.b(str4, "albumName");
        return new MusicTrackInfoResponse(str, b, str2, str3, str4);
    }
}
