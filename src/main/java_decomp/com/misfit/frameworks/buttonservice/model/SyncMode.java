package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SyncMode {
    @DexIgnore
    public static /* final */ int SYNC_MODE_AUTO; // = 10;
    @DexIgnore
    public static /* final */ int SYNC_MODE_CHECK_ACTIVE_WORKOUT; // = 15;
    @DexIgnore
    public static /* final */ int SYNC_MODE_FULL; // = 13;
    @DexIgnore
    public static /* final */ int SYNC_MODE_HWLOG; // = 14;
    @DexIgnore
    public static /* final */ int SYNC_MODE_NONE; // = -1;
    @DexIgnore
    public static /* final */ int SYNC_MODE_PULL; // = 12;
}
