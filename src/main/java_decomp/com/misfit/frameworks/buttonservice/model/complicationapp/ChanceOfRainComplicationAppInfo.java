package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.e90;
import com.fossil.pc0;
import com.fossil.t90;
import com.fossil.tc0;
import com.fossil.u80;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChanceOfRainComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public int probability;

    @DexIgnore
    public ChanceOfRainComplicationAppInfo(int i, long j) {
        super(e90.CHANCE_OF_RAIN_COMPLICATION);
        this.probability = i;
        this.expiredAt = j;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return new pc0(new u80(this.expiredAt, this.probability));
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof t90)) {
            return null;
        }
        return new pc0((t90) x90, new u80(this.expiredAt, this.probability));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.probability);
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationAppInfo(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.probability = parcel.readInt();
        this.expiredAt = parcel.readLong();
    }
}
