package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.ba0;
import com.fossil.e90;
import com.fossil.id0;
import com.fossil.tc0;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.fossil.yc0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingPhoneComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public id0 mRingPhoneState; // = id0.OFF;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(id0 id0) {
        super(e90.RING_PHONE);
        wg6.b(id0, "ringPhoneState");
        this.mRingPhoneState = id0;
    }

    @DexIgnore
    public final id0 getMRingPhoneState() {
        return this.mRingPhoneState;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return new yc0(this.mRingPhoneState);
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (x90 instanceof ba0) {
            return new yc0((ba0) x90, this.mRingPhoneState);
        }
        return null;
    }

    @DexIgnore
    public final void setMRingPhoneState(id0 id0) {
        wg6.b(id0, "<set-?>");
        this.mRingPhoneState = id0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mRingPhoneState.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.mRingPhoneState = id0.values()[parcel.readInt()];
    }
}
