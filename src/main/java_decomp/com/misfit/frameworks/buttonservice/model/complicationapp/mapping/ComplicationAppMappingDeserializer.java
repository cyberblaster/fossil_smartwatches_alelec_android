package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import com.fossil.du3;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.lu3;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import com.portfolio.platform.data.model.PinObject;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingDeserializer implements hu3<ComplicationAppMapping> {
    @DexIgnore
    public ComplicationAppMapping deserialize(JsonElement jsonElement, Type type, gu3 gu3) throws lu3 {
        wg6.b(jsonElement, PinObject.COLUMN_JSON_DATA);
        wg6.b(type, "typeOfT");
        wg6.b(gu3, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = ComplicationAppMappingDeserializer.class.getName();
        wg6.a((Object) name, "ComplicationAppMappingDe\u2026rializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement a = jsonElement.d().a(ComplicationAppMapping.Companion.getFIELD_TYPE());
        wg6.a((Object) a, "jsonType");
        int b = a.b();
        du3 du3 = new du3();
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, WeatherComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getHEART_RATE_TYPE()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, HeartRateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getSTEPS_TYPE()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, StepsComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getDATE_TYPE()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, DateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getEMPTY_TYPE()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, NoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCHANCE_OF_RAIN()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, ChanceOfRainComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, SecondTimezoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getACTIVE_MINUTES()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, ActiveMinutesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCALORIES()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, CaloriesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getBATTERY()) {
            return (ComplicationAppMapping) du3.a().a(jsonElement, BatteryComplicationAppMapping.class);
        }
        return null;
    }
}
