package com.misfit.frameworks.buttonservice.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ScanService$autoStopTask$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ScanService this$0;

    @DexIgnore
    public ScanService$autoStopTask$Anon1(ScanService scanService) {
        this.this$0 = scanService;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stopScan();
    }
}
