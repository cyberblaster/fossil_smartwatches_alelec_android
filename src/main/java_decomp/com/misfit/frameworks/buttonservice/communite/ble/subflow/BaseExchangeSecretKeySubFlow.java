package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import android.util.Base64;
import com.fossil.bc0;
import com.fossil.u40;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseExchangeSecretKeySubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ CommunicateMode communicateMode;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapterV2;
    @DexIgnore
    public byte[] mRandomKey;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class AuthenticateDeviceSessionState extends BleStateAbs {
        @DexIgnore
        public zb0<byte[]> task;

        @DexIgnore
        public AuthenticateDeviceSessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        public void onAuthenticateDeviceFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            int code = bc0.getCode();
            if (code == u40.REQUEST_UNSUPPORTED.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(u40.REQUEST_UNSUPPORTED.getCode());
            } else if (code == u40.UNSUPPORTED_FORMAT.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(u40.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(1217);
            }
        }

        @DexIgnore
        public void onAuthenticateDeviceSuccess(byte[] bArr) {
            wg6.b(bArr, "randomKey");
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseExchangeSecretKeySubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(BaseExchangeSecretKeySubFlow.this.getBleAdapter()));
                bundle.putString(ButtonService.DEVICE_RANDOM_KEY, Base64.encodeToString(bArr, 2));
                bleSessionCallback.onAskForSecretKey(bundle);
            }
            BaseExchangeSecretKeySubFlow baseExchangeSecretKeySubFlow = BaseExchangeSecretKeySubFlow.this;
            baseExchangeSecretKeySubFlow.enterSubStateAsync(baseExchangeSecretKeySubFlow.createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onEnter AuthenticateDeviceSessionState " + BaseExchangeSecretKeySubFlow.this.mRandomKey);
            if (BaseExchangeSecretKeySubFlow.this.mRandomKey != null) {
                BleAdapterImpl mBleAdapterV2 = BaseExchangeSecretKeySubFlow.this.getMBleAdapterV2();
                FLogger.Session logSession = BaseExchangeSecretKeySubFlow.this.getLogSession();
                byte[] access$getMRandomKey$p = BaseExchangeSecretKeySubFlow.this.mRandomKey;
                if (access$getMRandomKey$p != null) {
                    this.task = mBleAdapterV2.startAuthenticate(logSession, access$getMRandomKey$p, this);
                    if (this.task == null) {
                        BaseExchangeSecretKeySubFlow.this.stopSubFlow(10000);
                        return true;
                    }
                    startTimeout();
                    return true;
                }
                wg6.a();
                throw null;
            }
            BaseExchangeSecretKeySubFlow.this.errorLog("AuthenticateDeviceSession: no random key", FLogger.Component.BLE, ErrorCodeBuilder.Step.START_AUTHEN, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout AuthenticateDeviceSessionState");
            zb0<byte[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySessionState extends BleStateAbs {
        @DexIgnore
        public zb0<byte[]> task;

        @DexIgnore
        public ExchangeSecretKeySessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            return true;
        }

        @DexIgnore
        public void onExchangeSecretKeyFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            int code = bc0.getCode();
            if (code == u40.REQUEST_UNSUPPORTED.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(u40.REQUEST_UNSUPPORTED.getCode());
            } else if (code == u40.UNSUPPORTED_FORMAT.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(u40.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(1218);
            }
        }

        @DexIgnore
        public void onExchangeSecretKeySuccess(byte[] bArr) {
            wg6.b(bArr, "secretKey");
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseExchangeSecretKeySubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                String serial = BaseExchangeSecretKeySubFlow.this.getSerial();
                String encodeToString = Base64.encodeToString(bArr, 2);
                wg6.a((Object) encodeToString, "Base64.encodeToString(secretKey, Base64.NO_WRAP)");
                bleSessionCallback.broadcastExchangeSecretKeySuccess(serial, encodeToString);
            }
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void onReceiveSecretKey(byte[] bArr, int i) {
            if (i != 0 || bArr == null) {
                BaseExchangeSecretKeySubFlow baseExchangeSecretKeySubFlow = BaseExchangeSecretKeySubFlow.this;
                baseExchangeSecretKeySubFlow.errorLog("ExchangeSecretKeySession: server errorCode=" + i, FLogger.Component.API, ErrorCodeBuilder.Step.EXCHANGE_SECRET_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(i);
                return;
            }
            this.task = BaseExchangeSecretKeySubFlow.this.getBleAdapter().exchangeSecretKey(BaseExchangeSecretKeySubFlow.this.getLogSession(), bArr, this);
            if (this.task == null) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(10000);
            } else {
                startTimeout();
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout ExchangeSecretKeySessionState");
            zb0<byte[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GenerateRandomKeySessionState extends BleStateAbs {
        @DexIgnore
        public GenerateRandomKeySessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter GenerateRandomKeySessionState");
            if (BaseExchangeSecretKeySubFlow.this.getBleSessionCallback() != null) {
                BaseExchangeSecretKeySubFlow.this.getBleSessionCallback().onAskForRandomKey(BaseExchangeSecretKeySubFlow.this.getSerial());
                return true;
            }
            BaseExchangeSecretKeySubFlow.this.errorLog("GenerateRandomKeySession: No callback", FLogger.Component.BLE, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout GenerateRandomKeySessionState");
            BaseExchangeSecretKeySubFlow.this.errorLog("GenerateRandomKeySession: timeout", FLogger.Component.BLE, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseExchangeSecretKeySubFlow(CommunicateMode communicateMode2, String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        wg6.b(communicateMode2, "communicateMode");
        wg6.b(str, "tagName");
        wg6.b(bleSession, "bleSession");
        wg6.b(session, "logSession");
        wg6.b(str2, "serial");
        wg6.b(bleAdapterImpl, "mBleAdapterV2");
        this.communicateMode = communicateMode2;
        this.mflog = mFLog;
        this.mBleAdapterV2 = bleAdapterImpl;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        return this.communicateMode;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapterV2() {
        return this.mBleAdapterV2;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.GENERATE_RANDOM_KEY;
        String name = GenerateRandomKeySessionState.class.getName();
        wg6.a((Object) name, "GenerateRandomKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.AUTHENTICATE_DEVICE;
        String name2 = AuthenticateDeviceSessionState.class.getName();
        wg6.a((Object) name2, "AuthenticateDeviceSessionState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.EXCHANGE_SECRET_KEY;
        String name3 = ExchangeSecretKeySessionState.class.getName();
        wg6.a((Object) name3, "ExchangeSecretKeySessionState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.GENERATE_RANDOM_KEY));
        return true;
    }

    @DexIgnore
    public final void onReceiveRandomKey(byte[] bArr, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey failureCode " + i + " randomKey " + bArr + " state " + getMCurrentState());
        if (i == 0) {
            this.mRandomKey = bArr;
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.AUTHENTICATE_DEVICE));
            return;
        }
        errorLog("GenerateRandomKeySession: server errorCode=" + i, FLogger.Component.API, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
        stopSubFlow(i);
    }

    @DexIgnore
    public final void onReceiveServerSecretKey(byte[] bArr, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey failureCode " + i + " state " + getMCurrentState());
        BleStateAbs mCurrentState = getMCurrentState();
        if (mCurrentState instanceof ExchangeSecretKeySessionState) {
            ((ExchangeSecretKeySessionState) mCurrentState).onReceiveSecretKey(bArr, i);
        }
    }
}
