package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.AppNotification;
import com.fossil.HandMovingConfig;
import com.fossil.NotificationFilter;
import com.fossil.NotificationType;
import com.fossil.a70;
import com.fossil.ab0;
import com.fossil.ac0;
import com.fossil.bb0;
import com.fossil.c90;
import com.fossil.cb0;
import com.fossil.cd6;
import com.fossil.d70;
import com.fossil.db0;
import com.fossil.e80;
import com.fossil.ea0;
import com.fossil.eb0;
import com.fossil.fb0;
import com.fossil.fitness.FitnessData;
import com.fossil.g60;
import com.fossil.ga0;
import com.fossil.gb0;
import com.fossil.h60;
import com.fossil.ha0;
import com.fossil.hb0;
import com.fossil.ia0;
import com.fossil.jb0;
import com.fossil.jh6;
import com.fossil.ka0;
import com.fossil.l40;
import com.fossil.la0;
import com.fossil.lb0;
import com.fossil.ma0;
import com.fossil.mb0;
import com.fossil.n50;
import com.fossil.n70;
import com.fossil.na0;
import com.fossil.nb0;
import com.fossil.nd0;
import com.fossil.o70;
import com.fossil.ob0;
import com.fossil.p50;
import com.fossil.pa0;
import com.fossil.pb0;
import com.fossil.q40;
import com.fossil.q50;
import com.fossil.qb0;
import com.fossil.qg6;
import com.fossil.r40;
import com.fossil.r50;
import com.fossil.r60;
import com.fossil.rb0;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.s60;
import com.fossil.ta0;
import com.fossil.tb0;
import com.fossil.tc0;
import com.fossil.ua0;
import com.fossil.ub0;
import com.fossil.v60;
import com.fossil.va0;
import com.fossil.vb0;
import com.fossil.vd6;
import com.fossil.w40;
import com.fossil.wa0;
import com.fossil.wb0;
import com.fossil.wg6;
import com.fossil.x40;
import com.fossil.xa0;
import com.fossil.xb0;
import com.fossil.xd0;
import com.fossil.xf6;
import com.fossil.y70;
import com.fossil.ya0;
import com.fossil.yb0;
import com.fossil.yd0;
import com.fossil.yf6;
import com.fossil.za0;
import com.fossil.zb0;
import com.fossil.zd0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.DianaCalibrationObj;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl extends BleAdapter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public q40 deviceObj;
    @DexIgnore
    public boolean isScanning;
    @DexIgnore
    public HashMap<s60, r60> mDeviceConfiguration;
    @DexIgnore
    public q40.b mDeviceStateListener;
    @DexIgnore
    public ScanService scanService;
    @DexIgnore
    public byte[] tSecretKey;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class BleScanServiceCallback implements ScanService.Callback {
        @DexIgnore
        public /* final */ ISessionSdkCallback callback;

        @DexIgnore
        public BleScanServiceCallback(ISessionSdkCallback iSessionSdkCallback) {
            this.callback = iSessionSdkCallback;
        }

        @DexIgnore
        public final ISessionSdkCallback getCallback() {
            return this.callback;
        }

        @DexIgnore
        public void onDeviceFound(q40 q40, int i) {
            wg6.b(q40, "device");
            if (q40.getState() == q40.c.CONNECTED || q40.getState() == q40.c.UPGRADING_FIRMWARE) {
                i = 0;
            } else if (q40.getBondState() == q40.a.BONDED) {
                i = ScanService.RETRIEVE_DEVICE_BOND_RSSI_MARK;
            }
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onDeviceFound(q40, i);
            }
        }

        @DexIgnore
        public void onScanFail(l40 l40) {
            wg6.b(l40, "scanError");
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onScanFail(l40);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[q40.c.values().length];

        /*
        static {
            $EnumSwitchMapping$0[q40.c.DISCONNECTED.ordinal()] = 1;
            $EnumSwitchMapping$0[q40.c.CONNECTED.ordinal()] = 2;
            $EnumSwitchMapping$0[q40.c.UPGRADING_FIRMWARE.ordinal()] = 3;
            $EnumSwitchMapping$0[q40.c.CONNECTING.ordinal()] = 4;
            $EnumSwitchMapping$0[q40.c.DISCONNECTING.ordinal()] = 5;
        }
        */
    }

    /*
    static {
        String simpleName = BleAdapterImpl.class.getSimpleName();
        wg6.a((Object) simpleName, "BleAdapterImpl::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl(Context context, String str, String str2) {
        super(context, str, str2);
        wg6.b(context, "context");
        wg6.b(str, "serial");
        wg6.b(str2, "macAddress");
    }

    @DexIgnore
    private final void requestPairing(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "requestPairing(), serial=" + getSerial());
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            cb0 cb0 = (cb0) q40.a(cb0.class);
            if (cb0 == null || cb0.e().e(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon1(this, iSessionSdkCallback, session)).d(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon2(this, iSessionSdkCallback, session)) == null) {
                log(session, "Device[" + getSerial() + "] does not support RequestPairingFeature");
                iSessionSdkCallback.onAuthorizeDeviceFailed();
                cd6 cd6 = cd6.a;
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final q40 buildDeviceBySerial(String str, String str2, long j) {
        wg6.b(str, "serial");
        wg6.b(str2, "macAddress");
        if (this.scanService == null) {
            this.scanService = new ScanService(getContext(), (ScanService.Callback) null, j);
        }
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            return scanService2.buildDeviceBySerial(str, str2);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> calibrationApplyHandPosition(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Apply Hands Positions");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            jb0 jb0 = (jb0) q40.a(jb0.class);
            if (jb0 != null) {
                return jb0.o().e(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingCalibrationPositionFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> calibrationMoveHand(FLogger.Session session, HandCalibrationObj handCalibrationObj, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(handCalibrationObj, "handCalibrationObj");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        DianaCalibrationObj consume = DianaCalibrationObj.Companion.consume(handCalibrationObj);
        HandMovingConfig[] handMovingConfigArr = {new HandMovingConfig(consume.getHandId(), consume.getDegree(), consume.getHandMovingDirection(), consume.getHandMovingSpeed())};
        log(session, "Move Hand: handMovingType=" + consume.getHandMovingType() + ", handMovingConfigs=" + handMovingConfigArr + ' ');
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            wa0 wa0 = (wa0) q40.a(wa0.class);
            if (wa0 != null) {
                DianaCalibrationObj dianaCalibrationObj = consume;
                HandMovingConfig[] handMovingConfigArr2 = handMovingConfigArr;
                FLogger.Session session2 = session;
                ISessionSdkCallback iSessionSdkCallback2 = iSessionSdkCallback;
                return wa0.a(consume.getHandMovingType(), handMovingConfigArr).e(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon1(this, dianaCalibrationObj, handMovingConfigArr2, session2, iSessionSdkCallback2)).d(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon2(this, dianaCalibrationObj, handMovingConfigArr2, session2, iSessionSdkCallback2));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> calibrationReleaseHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Release Hand Control");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            bb0 bb0 = (bb0) q40.a(bb0.class);
            if (bb0 != null) {
                return bb0.i().e(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReleasingHandsFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> calibrationRequestHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Request Hand Control");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            db0 db0 = (db0) q40.a(db0.class);
            if (db0 != null) {
                return db0.j().e(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support RequestingHandsFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> calibrationResetHandToZeroDegree(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Reset Hands to 0 degrees.");
        HandMovingConfig[] handMovingConfigArr = {new HandMovingConfig(n50.HOUR, 0, p50.SHORTEST_PATH, q50.FULL), new HandMovingConfig(n50.MINUTE, 0, p50.SHORTEST_PATH, q50.FULL), new HandMovingConfig(n50.SUB_EYE, 0, p50.SHORTEST_PATH, q50.FULL)};
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            wa0 wa0 = (wa0) q40.a(wa0.class);
            if (wa0 != null) {
                return wa0.a(r50.POSITION, handMovingConfigArr).e(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon1(this, handMovingConfigArr, session, iSessionSdkCallback)).d(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon2(this, handMovingConfigArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void clearCache(FLogger.Session session) {
        wg6.b(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Clear cache, serial=" + getSerial());
        q40 q40 = this.deviceObj;
        if (q40 == null || q40.k() == null) {
            log(session, "clearCache() failed, deviceObj is null.");
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public void closeConnection(FLogger.Session session, boolean z) {
        wg6.b(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Close Connection, serial=" + getSerial());
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ka0 ka0 = (ka0) q40.a(ka0.class);
            if (ka0 != null) {
                ka0.a();
                log(session, "Close Connection, serial=" + getSerial() + ", OK");
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support DisconnectingFeature");
            return;
        }
        logAppError(session, "closeConnection failed. DeviceObject is null. Connect has not been established.", ErrorCodeBuilder.Step.DISCONNECT, ErrorCodeBuilder.AppError.UNKNOWN);
    }

    @DexIgnore
    public final yb0<cd6> configureMicroApp(FLogger.Session session, List<nd0> list, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(list, "mappings");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Configure MicroApp, mapping=" + list);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ha0 ha0 = (ha0) q40.a(ha0.class);
            if (ha0 != null) {
                Object[] array = list.toArray(new nd0[0]);
                if (array != null) {
                    return ha0.a((nd0[]) array).e(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).d(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support ConfiguringMicroAppFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> confirmAuthorization(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "confirmAuthorization(), timeout=" + j);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ea0 ea0 = (ea0) q40.a(ea0.class);
            if (ea0 != null) {
                long j2 = j;
                FLogger.Session session2 = session;
                ISessionSdkCallback iSessionSdkCallback2 = iSessionSdkCallback;
                return ea0.a(j).e(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon1(this, j2, session2, iSessionSdkCallback2)).d(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon2(this, j2, session2, iSessionSdkCallback2));
            }
            log(session, "Device[" + getSerial() + "] does not support AuthorizationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final yb0<String> doOTA(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(bArr, "firmwareData");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do OTA");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            za0 za0 = (za0) q40.a(za0.class);
            if (za0 != null) {
                yb0 a = za0.a(bArr, (String) null);
                a.g(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback));
                return a.e(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback)).d(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon3(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support OTAFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final Boolean enableMaintainConnection(FLogger.Session session) {
        wg6.b(session, "logSession");
        log(session, "Enable Maintain Connection");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            la0 la0 = (la0) q40.a(la0.class);
            if (la0 != null) {
                return Boolean.valueOf(la0.g());
            }
            log(session, "Device[" + getSerial() + "] does not support EnablingMaintainConnectionFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> eraseDataFile(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Erase DataFiles");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ga0 ga0 = (ga0) q40.a(ga0.class);
            if (ga0 != null) {
                return ga0.cleanUp().e(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support CleanUpDeviceFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void error(FLogger.Session session, String str, ErrorCodeBuilder.Step step, String str2) {
        wg6.b(session, "logSession");
        wg6.b(str, "errorCode");
        wg6.b(step, "step");
        wg6.b(str2, "errorMessage");
        MFLog activeLog = MFLogManager.getInstance(getContext()).getActiveLog(getSerial());
        if (activeLog != null) {
            activeLog.error('[' + getSerial() + "] " + str2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, '[' + getSerial() + "] error=" + str + ' ' + str2);
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, session, getSerial(), TAG, str, step, str2);
    }

    @DexIgnore
    public final zb0<byte[]> exchangeSecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        zb0<byte[]> d;
        wg6.b(session, "logSession");
        wg6.b(bArr, "data");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Exchange Secret Key");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ma0 ma0 = (ma0) q40.a(ma0.class);
            if (ma0 != null && (d = ma0.c(bArr).e(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).d(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support ExchangingSecretKeyFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<r40> fetchDeviceInfo(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Fetch Device Information");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            na0 na0 = (na0) q40.a(na0.class);
            if (na0 != null) {
                return na0.c().e(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support FetchingDeviceInformationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public int getBatteryLevel() {
        HashMap<s60, r60> hashMap = this.mDeviceConfiguration;
        if (hashMap == null || !hashMap.containsKey(s60.BATTERY)) {
            return -1;
        }
        g60 g60 = hashMap.get(s60.BATTERY);
        if (g60 != null) {
            return g60.getPercentage();
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        r0 = (r0 = (r0 = (r0 = r0.l()).getWatchParameterVersions()).getCurrentVersion()).getShortDescription();
     */
    @DexIgnore
    public String getCurrentWatchParamVersion() {
        r40 l;
        x40 watchParameterVersions;
        w40 currentVersion;
        String shortDescription;
        q40 q40 = this.deviceObj;
        return (q40 == null || l == null || watchParameterVersions == null || currentVersion == null || shortDescription == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    public final zb0<HashMap<s60, r60>> getDeviceConfig(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Device Configuration");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            pa0 pa0 = (pa0) q40.a(pa0.class);
            if (pa0 != null) {
                return pa0.m().e(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support GettingConfigurationsFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = (r0 = r0.l()).getModelNumber();
     */
    @DexIgnore
    public String getDeviceModel() {
        r40 l;
        String modelNumber;
        q40 q40 = this.deviceObj;
        return (q40 == null || l == null || modelNumber == null) ? "" : modelNumber;
    }

    @DexIgnore
    public final q40 getDeviceObj() {
        return this.deviceObj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = (r0 = r0.l()).getFirmwareVersion();
     */
    @DexIgnore
    public String getFirmwareVersion() {
        r40 l;
        String firmwareVersion;
        q40 q40 = this.deviceObj;
        return (q40 == null || l == null || firmwareVersion == null) ? "" : firmwareVersion;
    }

    @DexIgnore
    public int getGattState() {
        return getGattState(this.deviceObj);
    }

    @DexIgnore
    public HeartRateMode getHeartRateMode() {
        HeartRateMode heartRateMode;
        HashMap<s60, r60> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(s60.HEART_RATE_MODE)) {
                HeartRateMode.Companion companion = HeartRateMode.Companion;
                v60 v60 = hashMap.get(s60.HEART_RATE_MODE);
                if (v60 != null) {
                    heartRateMode = companion.consume(v60.getHeartRateMode());
                } else {
                    throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
                }
            } else {
                heartRateMode = HeartRateMode.NONE;
            }
            if (heartRateMode != null) {
                return heartRateMode;
            }
        }
        return HeartRateMode.NONE;
    }

    @DexIgnore
    public int getHidState() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = (r0 = r0.l()).getLocaleString();
     */
    @DexIgnore
    public String getLocale() {
        r40 l;
        String localeString;
        q40 q40 = this.deviceObj;
        return (q40 == null || l == null || localeString == null) ? "" : localeString;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = (r0 = r0.l()).getLocaleVersions();
     */
    @DexIgnore
    public String getLocaleVersion() {
        r40 l;
        x40 localeVersions;
        q40 q40 = this.deviceObj;
        w40 currentVersion = (q40 == null || l == null || localeVersions == null) ? null : localeVersions.getCurrentVersion();
        return String.valueOf(currentVersion);
    }

    @DexIgnore
    public short getMicroAppMajorVersion() {
        r40 l;
        w40 microAppVersion;
        q40 q40 = this.deviceObj;
        if (q40 == null || (l = q40.l()) == null || (microAppVersion = l.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMajor();
    }

    @DexIgnore
    public short getMicroAppMinorVersion() {
        r40 l;
        w40 microAppVersion;
        q40 q40 = this.deviceObj;
        if (q40 == null || (l = q40.l()) == null || (microAppVersion = l.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMinor();
    }

    @DexIgnore
    public final byte[] getSecretKeyThroughSDK(FLogger.Session session) {
        wg6.b(session, "logSession");
        log(session, "Get Secret Key Through SDK");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ta0 ta0 = (ta0) q40.a(ta0.class);
            if (ta0 != null) {
                setTSecretKey(ta0.f());
                log(session, "Secret key from SDK " + getTSecretKey());
                return getTSecretKey();
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public byte getSupportedWatchParamMajor() {
        r40 l;
        x40 watchParameterVersions;
        w40 supportedVersion;
        q40 q40 = this.deviceObj;
        if (q40 == null || (l = q40.l()) == null || (watchParameterVersions = l.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMajor();
    }

    @DexIgnore
    public byte getSupportedWatchParamMinor() {
        r40 l;
        x40 watchParameterVersions;
        w40 supportedVersion;
        q40 q40 = this.deviceObj;
        if (q40 == null || (l = q40.l()) == null || (watchParameterVersions = l.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMinor();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        r0 = (r0 = (r0 = (r0 = r0.l()).getWatchParameterVersions()).getSupportedVersion()).getShortDescription();
     */
    @DexIgnore
    public String getSupportedWatchParamVersion() {
        r40 l;
        x40 watchParameterVersions;
        w40 supportedVersion;
        String shortDescription;
        q40 q40 = this.deviceObj;
        return (q40 == null || l == null || watchParameterVersions == null || supportedVersion == null || shortDescription == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    public byte[] getTSecretKey() {
        return this.tSecretKey;
    }

    @DexIgnore
    public VibrationStrengthObj getVibrationStrength() {
        VibrationStrengthObj vibrationStrengthObj;
        HashMap<s60, r60> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(s60.VIBE_STRENGTH)) {
                a70 a70 = hashMap.get(s60.VIBE_STRENGTH);
                if (a70 != null) {
                    a70.a vibeStrengthLevel = a70.getVibeStrengthLevel();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "Device config not null, vibeStrengthConfig not null, return vibrationStrength: value = " + vibeStrengthLevel);
                    vibrationStrengthObj = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, vibeStrengthLevel, false, 2, (Object) null);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                FLogger.INSTANCE.getLocal().e(TAG, "Device config not null but vibeStrengthConfig is null, vibrationStrength return default value MEDIUM");
                vibrationStrengthObj = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(a70.a.MEDIUM, true);
            }
            if (vibrationStrengthObj != null) {
                return vibrationStrengthObj;
            }
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Device config null, vibrationStrength return default value MEDIUM");
        return VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(a70.a.MEDIUM, true);
    }

    @DexIgnore
    public boolean isDeviceReady() {
        q40 q40 = this.deviceObj;
        boolean z = false;
        if (q40 != null) {
            if (q40.getState() == q40.c.CONNECTED) {
                z = true;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append(".isDeviceReady() - serial=");
            sb.append(getSerial());
            sb.append(", state=");
            sb.append(q40.getState());
            sb.append(", ready=");
            sb.append(!z ? "NO" : "YES");
            local.d(str, sb.toString());
            return z;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.e(str2, "Is device ready " + getSerial() + ". FAILED: deviceObject is NULL");
        return false;
    }

    @DexIgnore
    public <T> T isSupportedFeature(Class<T> cls) {
        wg6.b(cls, "feature");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            return q40.a(cls);
        }
        return null;
    }

    @DexIgnore
    public final void logAppError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        wg6.b(session, "logSession");
        wg6.b(str, "message");
        wg6.b(step, "step");
        wg6.b(appError, Constants.YO_ERROR_POST);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", appError=" + appError);
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, appError), step, str);
    }

    @DexIgnore
    public final void logSdkError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, ac0 ac0) {
        wg6.b(session, "logSession");
        wg6.b(str, "message");
        wg6.b(step, "step");
        wg6.b(ac0, "sdkError");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", sdkError=" + ac0.getErrorCode());
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, ac0), step, str);
    }

    @DexIgnore
    public final zb0<cd6> notifyMusicEvent(FLogger.Session session, n70 n70) {
        wg6.b(session, "logSession");
        wg6.b(n70, "musicEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Notify Music Event, musicEvent=" + n70);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Notify Music Event");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ya0 ya0 = (ya0) q40.a(ya0.class);
            if (ya0 != null) {
                return ya0.a(n70);
            }
            log(session, "Device[" + getSerial() + "] does not support NotifyingMusicEventFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> notifyNotificationEvent(FLogger.Session session, NotificationBaseObj notificationBaseObj, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(notificationBaseObj, "notifyNotificationEvent");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do notifyNotificationEvent");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            xa0 xa0 = (xa0) q40.a(xa0.class);
            DianaNotificationObj dianaNotificationObj = (DianaNotificationObj) notificationBaseObj;
            if (xa0 != null) {
                return xa0.a(dianaNotificationObj.toSDKNotifyNotificationEvent()).e(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon1(this, dianaNotificationObj, session, iSessionSdkCallback)).d(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon2(this, dianaNotificationObj, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support notifyNotificationEvent");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> playDeviceAnimation(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Play Device Animation");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            va0 va0 = (va0) q40.a(va0.class);
            if (va0 != null) {
                return va0.b().e(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support HandAnimationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<c90> readCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        zb0<c90> d;
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Workout Session");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ua0 ua0 = (ua0) q40.a(ua0.class);
            if (ua0 != null && (d = ua0.n().e(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback))) != null) {
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingWorkoutSessionFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final yb0<FitnessData[]> readDataFile(FLogger.Session session, h60 h60, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(h60, "biometricProfile");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read DataFiles");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            wb0 wb0 = (wb0) q40.a(wb0.class);
            if (wb0 != null) {
                yb0 a = wb0.a(h60);
                a.g(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1(this, h60, session, iSessionSdkCallback));
                return a.e(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(this, h60, session, iSessionSdkCallback)).d(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon3(this, h60, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SynchronizationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<Integer> readRssi(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read Rssi");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ab0 ab0 = (ab0) q40.a(ab0.class);
            if (ab0 != null) {
                return ab0.h().e(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReadingRSSIFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void registerBluetoothStateCallback(q40.b bVar) {
        wg6.b(bVar, Constants.CALLBACK);
        this.mDeviceStateListener = bVar;
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            q40.a(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final void sendCustomCommand(CustomRequest customRequest) {
        zb0 a;
        zb0 e;
        wg6.b(customRequest, Constants.COMMAND);
        FLogger.Session session = FLogger.Session.OTHER;
        log(session, "Send Custom Command: " + customRequest);
        q40 q40 = this.deviceObj;
        if (!(q40 instanceof ia0)) {
            q40 = null;
        }
        ia0 ia0 = (ia0) q40;
        if (ia0 == null || (a = ia0.a(customRequest.getCustomCommand(), d70.DC)) == null || (e = a.e(new BleAdapterImpl$sendCustomCommand$Anon1(this, customRequest))) == null || e.d(new BleAdapterImpl$sendCustomCommand$Anon2(this)) == null) {
            log(FLogger.Session.OTHER, "Send Custom Command not support");
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public final zb0<cd6> sendNotification(FLogger.Session session, NotificationBaseObj notificationBaseObj) {
        wg6.b(session, "logSession");
        wg6.b(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Notification, notification=" + notificationBaseObj);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Notification");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            eb0 eb0 = (eb0) q40.a(eb0.class);
            if (eb0 != null) {
                AppNotification sDKNotification = notificationBaseObj.toSDKNotification();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "send sdk notification " + sDKNotification.toString());
                return eb0.a(sDKNotification);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingAppNotificationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> sendRespond(FLogger.Session session, tc0 tc0) {
        wg6.b(session, "logSession");
        wg6.b(tc0, "deviceData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Respond, deviceData=" + tc0);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Respond");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            fb0 fb0 = (fb0) q40.a(fb0.class);
            if (fb0 != null) {
                return fb0.a(tc0);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingDataFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> sendTrackInfo(FLogger.Session session, o70 o70) {
        wg6.b(session, "logSession");
        wg6.b(o70, "trackInfo");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Track Info, trackInfo=" + o70);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Track Info");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            gb0 gb0 = (gb0) q40.a(gb0.class);
            if (gb0 != null) {
                return gb0.a(o70);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingTrackInfoFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> setAlarms(FLogger.Session session, List<AlarmSetting> list, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(list, "alarms");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set List Alarms: " + list);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            hb0 hb0 = (hb0) q40.a(hb0.class);
            if (hb0 != null) {
                return hb0.a(AlarmExtensionKt.toSDKV2Setting(list)).e(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).d(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingAlarmFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> setBackgroundImage(FLogger.Session session, BackgroundConfig backgroundConfig, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(backgroundConfig, "backgroundConfig");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Background Image: " + backgroundConfig);
        String str = getContext().getFilesDir() + File.separator;
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            pb0 pb0 = (pb0) q40.a(pb0.class);
            if (pb0 != null) {
                BackgroundConfig backgroundConfig2 = backgroundConfig;
                String str2 = str;
                FLogger.Session session2 = session;
                ISessionSdkCallback iSessionSdkCallback2 = iSessionSdkCallback;
                return pb0.a(backgroundConfig.toSDKBackgroundImageConfig(str)).e(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1(this, backgroundConfig2, str2, session2, iSessionSdkCallback2)).d(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2(this, backgroundConfig2, str2, session2, iSessionSdkCallback2));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - set BackgroundImageFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> setComplications(FLogger.Session session, ComplicationAppMappingSettings complicationAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Complication: " + complicationAppMappingSettings);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            pb0 pb0 = (pb0) q40.a(pb0.class);
            if (pb0 != null) {
                return pb0.a(complicationAppMappingSettings.toSDKSetting()).e(new BleAdapterImpl$setComplications$$inlined$let$lambda$Anon1(this, complicationAppMappingSettings, session, iSessionSdkCallback)).d(new BleAdapterImpl$setComplications$$inlined$let$lambda$Anon2(this, complicationAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - setComplications");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final boolean setDevice(q40 q40) {
        wg6.b(q40, "deviceObj");
        setDeviceObj(q40);
        return true;
    }

    @DexIgnore
    public final zb0<s60[]> setDeviceConfig(FLogger.Session session, r60[] r60Arr, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(r60Arr, "deviceConfigItems");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Device Config: " + new Gson().a(r60Arr));
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            lb0 lb0 = (lb0) q40.a(lb0.class);
            if (lb0 != null) {
                return lb0.a(r60Arr).e(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(this, r60Arr, session, iSessionSdkCallback)).d(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon2(this, r60Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingConfigurationsFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void setDeviceObj(q40 q40) {
        this.deviceObj = q40;
        if (q40 != null) {
            if (!TextUtils.isEmpty(q40.l().getMacAddress())) {
                setMacAddress(q40.l().getMacAddress());
            }
            q40.a(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final zb0<cd6> setFrontLightEnable(FLogger.Session session, boolean z, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Front Light Enable: " + z);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            mb0 mb0 = (mb0) q40.a(mb0.class);
            if (mb0 != null) {
                return mb0.a(z).e(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon1(this, z, session, iSessionSdkCallback)).d(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon2(this, z, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingFrontLightFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final yb0<String> setLocalizationData(LocalizationData localizationData, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(localizationData, "localizationData");
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Localization");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            nb0 nb0 = (nb0) q40.a(nb0.class);
            if (nb0 != null) {
                return nb0.a(localizationData.toLocalizationFile()).e(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon1(this, localizationData, session, iSessionSdkCallback)).d(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon2(this, localizationData, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final yb0<cd6> setNotificationFilters(FLogger.Session session, List<AppNotificationFilter> list, ISessionSdkCallback iSessionSdkCallback) {
        NotificationFilter notificationFilter;
        wg6.b(session, "logSession");
        wg6.b(list, "notificationFilters");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (T next : list) {
            String packageName = ((AppNotificationFilter) next).getPackageName();
            Object obj = linkedHashMap.get(packageName);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(packageName, obj);
            }
            ((List) obj).add(next);
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry value : linkedHashMap.entrySet()) {
            Iterable<AppNotificationFilter> iterable = (Iterable) value.getValue();
            ArrayList arrayList2 = new ArrayList(rd6.a(iterable, 10));
            boolean z = false;
            for (AppNotificationFilter appNotificationFilter : iterable) {
                if (appNotificationFilter.getHandMovingConfig() == null) {
                    notificationFilter = appNotificationFilter.toSDKNotificationFilter(getContext(), !z);
                    if (!z) {
                        z = notificationFilter.getIconConfig() != null;
                    }
                } else {
                    notificationFilter = appNotificationFilter.toSDKNotificationFilter(getContext(), false);
                }
                arrayList2.add(notificationFilter);
            }
            vd6.a(arrayList, arrayList2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Set Notification Filter: " + list + " bleNotifications " + arrayList);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Set Notification Filter");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ob0 ob0 = (ob0) q40.a(ob0.class);
            if (ob0 != null) {
                Object[] array = arrayList.toArray(new NotificationFilter[0]);
                if (array != null) {
                    yb0 a = ob0.a((NotificationFilter[]) array);
                    a.g(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1(this, arrayList, session, iSessionSdkCallback));
                    return a.e(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon2(this, arrayList, session, iSessionSdkCallback)).d(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon3(this, arrayList, session, iSessionSdkCallback));
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final yb0<cd6> setReplyMessageMapping(FLogger.Session session, ReplyMessageMappingGroup replyMessageMappingGroup, ISessionSdkCallback iSessionSdkCallback) {
        Throwable th;
        FLogger.Session session2 = session;
        wg6.b(session2, "logSession");
        wg6.b(replyMessageMappingGroup, "replyMessageGroup");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            qb0 qb0 = (qb0) q40.a(qb0.class);
            jh6 jh6 = new jh6();
            InputStream open = getContext().getAssets().open(replyMessageMappingGroup.getIconFwPath());
            try {
                String iconFwPath = replyMessageMappingGroup.getIconFwPath();
                wg6.a((Object) open, "it");
                jh6.element = new e80(iconFwPath, xf6.a(open));
                List<ReplyMessageMapping> replyMessageList = replyMessageMappingGroup.getReplyMessageList();
                ArrayList arrayList = new ArrayList(rd6.a(replyMessageList, 10));
                for (ReplyMessageMapping replyMessageMapping : replyMessageList) {
                    String id = replyMessageMapping.getId();
                    if (id != null) {
                        byte parseByte = Byte.parseByte(id);
                        String message = replyMessageMapping.getMessage();
                        if (message == null) {
                            message = "";
                        }
                        e80 e80 = (e80) jh6.element;
                        if (e80 != null) {
                            arrayList.add(new y70(parseByte, message, e80));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                Object[] array = arrayList.toArray(new y70[0]);
                if (array != null) {
                    yd0 yd0 = new yd0((y70[]) array);
                    xd0 xd0 = new xd0();
                    xd0.a(NotificationType.INCOMING_CALL, yd0);
                    xd0.a(NotificationType.TEXT, yd0);
                    xd0.a(NotificationType.MISSED_CALL, yd0);
                    zd0[] a = xd0.a();
                    if (qb0 != null) {
                        yb0 a2 = qb0.a(a);
                        BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1 bleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1 = new BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1(a, this, jh6, replyMessageMappingGroup, qb0, session, iSessionSdkCallback);
                        yb0<cd6> d = a2.e(bleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1).d(new BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2(a, this, jh6, replyMessageMappingGroup, qb0, session, iSessionSdkCallback));
                        yf6.a(open, (Throwable) null);
                        return d;
                    }
                    log(session2, "Device[" + getSerial() + "] does not support SettingReplyMessageFeature");
                    yf6.a(open, (Throwable) null);
                    return null;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            } catch (Throwable th2) {
                Throwable th3 = th2;
                yf6.a(open, th);
                throw th3;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setSecretKey(FLogger.Session session, byte[] bArr) {
        wg6.b(session, "logSession");
        wg6.b(bArr, "secretKey");
        log(session, "Set Secret Key");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            rb0 rb0 = (rb0) q40.a(rb0.class);
            if (rb0 != null) {
                rb0.d(bArr);
                setTSecretKey(bArr);
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return;
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Set Secret Key. FAILED: deviceObject is NULL");
    }

    @DexIgnore
    public void setTSecretKey(byte[] bArr) {
        this.tSecretKey = bArr;
    }

    @DexIgnore
    public final zb0<cd6> setWatchApps(FLogger.Session session, WatchAppMappingSettings watchAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(watchAppMappingSettings, "watchAppMappingSettings");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch Apps: " + watchAppMappingSettings);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            pb0 pb0 = (pb0) q40.a(pb0.class);
            if (pb0 != null) {
                return pb0.a(watchAppMappingSettings.toSDKSetting()).e(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon1(this, watchAppMappingSettings, session, iSessionSdkCallback)).d(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon2(this, watchAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - setWatchApps");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<cd6> setWatchParams(FLogger.Session session, WatchParamsFileMapping watchParamsFileMapping, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(watchParamsFileMapping, "watchParamFileMapping");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set WatchParams: " + watchParamsFileMapping);
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            tb0 tb0 = (tb0) q40.a(tb0.class);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "Setting watchParam with file value = " + watchParamsFileMapping.toSDKSetting());
            if (tb0 != null) {
                return tb0.a(watchParamsFileMapping.toSDKSetting()).e(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1(this, watchParamsFileMapping, session, iSessionSdkCallback)).d(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon2(this, watchParamsFileMapping, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingWatchParameterFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final zb0<byte[]> startAuthenticate(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(bArr, "phoneRandomNumber");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Start Authenticate phoneRandomNumber " + ConversionUtils.INSTANCE.byteArrayToString(bArr));
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Start Authenticate phoneRandomNumber");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            ub0 ub0 = (ub0) q40.a(ub0.class);
            if (ub0 != null) {
                return ub0.a(bArr).e(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).d(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StartingAuthenticateFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void startScanning(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Start scanning...");
        this.isScanning = true;
        this.scanService = new ScanService(getContext(), new BleScanServiceCallback(iSessionSdkCallback), j);
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.setActiveDeviceLog(getSerial());
            ScanService scanService3 = this.scanService;
            if (scanService3 != null) {
                scanService3.startScanWithAutoStopTimer();
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final zb0<cd6> stopCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        wg6.b(session, "logSession");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Stop Current Workout Session");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            vb0 vb0 = (vb0) q40.a(vb0.class);
            if (vb0 != null) {
                return vb0.d().e(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).d(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StoppingWorkoutSessionFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void stopScanning(FLogger.Session session) {
        wg6.b(session, "logSession");
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.stopScan();
        }
        if (this.isScanning) {
            log(session, "Stop scanning for " + getSerial());
        }
        this.isScanning = false;
    }

    @DexIgnore
    public final zb0<Boolean> verifySecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        zb0<Boolean> d;
        wg6.b(session, "logSession");
        wg6.b(bArr, "secretKey");
        wg6.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Verify Secret Key");
        q40 q40 = this.deviceObj;
        if (q40 != null) {
            xb0 xb0 = (xb0) q40.a(xb0.class);
            if (xb0 != null && (d = xb0.b(bArr).e(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).d(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support VerifyingSecretKeyFeature");
            return null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    private final int getGattState(q40 q40) {
        if (q40 != null) {
            int i = WhenMappings.$EnumSwitchMapping$0[q40.getState().ordinal()];
            if (i == 1) {
                return 0;
            }
            if (i == 2 || i == 3) {
                return 2;
            }
            if (i != 4) {
                return i != 5 ? 0 : 3;
            }
            return 1;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, "Inside " + TAG + ".getGattState - serial=" + getSerial() + ", shineDevice=NULL");
        return 0;
    }
}
