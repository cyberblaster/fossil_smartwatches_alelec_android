package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.r40;
import com.fossil.wg6;
import com.fossil.xg6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1 extends xg6 implements hg6<r40, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((r40) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(r40 r40) {
        wg6.b(r40, "it");
        this.this$0.log(this.$logSession$inlined, "Fetch Device Information Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$cp = BleAdapterImpl.TAG;
        local.d(access$getTAG$cp, ".fetchDeviceInfo(), data=" + new Gson().a(r40));
        this.$callback$inlined.onFetchDeviceInfoSuccess(r40);
    }
}
