package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.c90;
import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1 extends xg6 implements hg6<c90, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((c90) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(c90 c90) {
        wg6.b(c90, "it");
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Read Current Workout Session Success, session=" + new Gson().a(c90));
        this.$callback$inlined.onReadCurrentWorkoutSessionSuccess(c90);
    }
}
