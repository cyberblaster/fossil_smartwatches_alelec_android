package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetStepGoalSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ int stepGoal;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetStepGoalState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetStepGoalState() {
            super(SetStepGoalSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareConfigData() {
            b70 b70 = new b70();
            b70.e((long) SetStepGoalSession.this.stepGoal);
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetStepGoalSession setStepGoalSession = SetStepGoalSession.this;
            setStepGoalSession.log("Set Step Goal: " + SetStepGoalSession.this.stepGoal);
            this.task = SetStepGoalSession.this.getBleAdapter().setDeviceConfig(SetStepGoalSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetStepGoalSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetStepGoalSession.this.getContext(), SetStepGoalSession.this.getSerial())) {
                SetStepGoalSession.this.log("Reach the limit retry. Stop.");
                SetStepGoalSession.this.stop(FailureCode.FAILED_TO_SET_STEP_GOAL);
            }
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetStepGoalSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetStepGoalSession(int i, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.SET_STEP_GOAL, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.stepGoal = i;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetStepGoalSession setStepGoalSession = new SetStepGoalSession(this.stepGoal, getBleAdapter(), getBleSessionCallback());
        setStepGoalSession.setDevice(getDevice());
        return setStepGoalSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_STEP_GOAL_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_STEP_GOAL_STATE;
        String name = SetStepGoalState.class.getName();
        wg6.a((Object) name, "SetStepGoalState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
