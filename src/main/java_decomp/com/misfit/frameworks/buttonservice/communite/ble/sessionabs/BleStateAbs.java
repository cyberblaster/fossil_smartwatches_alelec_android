package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import com.fossil.bc0;
import com.fossil.c90;
import com.fossil.fitness.FitnessData;
import com.fossil.l40;
import com.fossil.q40;
import com.fossil.qg6;
import com.fossil.r40;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleStateAbs extends BleState implements ISessionSdkCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int MAKE_DEVICE_READY_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int OTA_SUCCESS_TIMEOUT; // = 60000;
    @DexIgnore
    public static /* final */ int PROGRESS_TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public static /* final */ int PROGRESS_UPDATE_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public int maxRetries; // = 3;
    @DexIgnore
    public int retryCounter;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleStateAbs(String str) {
        super(str);
        wg6.b(str, "tagName");
        setTimeout(30000);
    }

    @DexIgnore
    public final int getMaxRetries() {
        return this.maxRetries;
    }

    @DexIgnore
    public final int getRetryCounter() {
        return this.retryCounter;
    }

    @DexIgnore
    public void onApplyHandPositionFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthenticateDeviceFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthenticateDeviceSuccess(byte[] bArr) {
        wg6.b(bArr, "randomKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthorizeDeviceFailed() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthorizeDeviceSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onConfigureMicroAppFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onConfigureMicroAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDeviceFound(q40 q40, int i) {
        wg6.b(q40, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExchangeSecretKeyFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExchangeSecretKeySuccess(byte[] bArr) {
        wg6.b(bArr, "secretKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoSuccess(r40 r40) {
        wg6.b(r40, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
        wg6.b(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetWatchParamsFail() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNextSession() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNotifyNotificationEventFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNotifyNotificationEventSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayAnimationFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayAnimationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayDeviceAnimation(boolean z, bc0 bc0) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionSuccess(c90 c90) {
        wg6.b(c90, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        wg6.b(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onScanFail(l40 l40) {
        wg6.b(l40, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSendMicroAppDataFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSendMicroAppDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetLocalizationDataFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetLocalizationDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetReplyMessageMappingError(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetReplyMessageMappingSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchParamsFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchParamsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionFailed(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeyFail(bc0 bc0) {
        wg6.b(bc0, Constants.YO_ERROR_POST);
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final boolean retry(Context context, String str) {
        wg6.b(context, "context");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        MFLogManager instance = MFLogManager.getInstance(context);
        instance.addLogForActiveLog(str, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        int i = this.retryCounter;
        if (i >= this.maxRetries) {
            return false;
        }
        this.retryCounter = i + 1;
        onEnter();
        return true;
    }

    @DexIgnore
    public final void setMaxRetries(int i) {
        this.maxRetries = i;
    }

    @DexIgnore
    public final void setRetryCounter(int i) {
        this.retryCounter = i;
    }
}
