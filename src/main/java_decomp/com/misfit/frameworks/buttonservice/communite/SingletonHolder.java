package com.misfit.frameworks.buttonservice.communite;

import com.fossil.hg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SingletonHolder<T, A> {
    @DexIgnore
    public hg6<? super A, ? extends T> creator;
    @DexIgnore
    public volatile T instance;

    @DexIgnore
    public SingletonHolder(hg6<? super A, ? extends T> hg6) {
        wg6.b(hg6, "creator");
        this.creator = hg6;
    }

    @DexIgnore
    public final T getInstance(A a) {
        T t;
        T t2 = this.instance;
        if (t2 != null) {
            return t2;
        }
        synchronized (this) {
            t = this.instance;
            if (t == null) {
                hg6 hg6 = this.creator;
                if (hg6 != null) {
                    t = hg6.invoke(a);
                    this.instance = t;
                    this.creator = null;
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        return t;
    }
}
