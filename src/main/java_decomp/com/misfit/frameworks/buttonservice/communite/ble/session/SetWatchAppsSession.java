package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.wg6;
import com.fossil.zb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWatchAppsSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ WatchAppMappingSettings mWatchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppsState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetWatchAppsState() {
            super(SetWatchAppsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetWatchAppsSession.this.getBleAdapter().setWatchApps(SetWatchAppsSession.this.getLogSession(), SetWatchAppsSession.this.mWatchAppMappingSettings, this);
            if (this.task == null) {
                SetWatchAppsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetWatchAppFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetWatchAppsSession.this.getContext(), SetWatchAppsSession.this.getSerial())) {
                SetWatchAppsSession.this.log("Reach the limit retry. Stop.");
                SetWatchAppsSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APPS);
            }
        }

        @DexIgnore
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoWatchAppSettings(SetWatchAppsSession.this.getBleAdapter().getContext(), SetWatchAppsSession.this.getBleAdapter().getSerial(), new Gson().a(SetWatchAppsSession.this.mWatchAppMappingSettings));
            SetWatchAppsSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWatchAppsSession(WatchAppMappingSettings watchAppMappingSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_WATCH_APPS, bleAdapterImpl, bleSessionCallback);
        wg6.b(watchAppMappingSettings, "mWatchAppMappingSettings");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mWatchAppMappingSettings = watchAppMappingSettings;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_WATCH_APPS) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetWatchAppsSession setWatchAppsSession = new SetWatchAppsSession(this.mWatchAppMappingSettings, getBleAdapter(), getBleSessionCallback());
        setWatchAppsSession.setDevice(getDevice());
        return setWatchAppsSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
        String name = SetWatchAppsState.class.getName();
        wg6.a((Object) name, "SetWatchAppsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
