package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.c70;
import com.fossil.e70;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetImplicitDisplayUnitSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ UserDisplayUnit mUserDisplayUnit;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DISPLAY_UNIT_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetImplicitDisplayUnitSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDisplayUnitSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetDisplayUnitState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetDisplayUnitState() {
            super(SetImplicitDisplayUnitSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            b70 b70 = new b70();
            b70.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            b70.a(SetImplicitDisplayUnitSession.this.mUserDisplayUnit.getTemperatureUnit().toSDKTemperatureUnit(), c70.KCAL, SetImplicitDisplayUnitSession.this.mUserDisplayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(SetImplicitDisplayUnitSession.this.getBleAdapter().getContext()), e70.MONTH_DAY_YEAR);
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetImplicitDisplayUnitSession.this.getBleAdapter().setDeviceConfig(SetImplicitDisplayUnitSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetImplicitDisplayUnitSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SetImplicitDisplayUnitSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDisplayUnitSession setImplicitDisplayUnitSession = SetImplicitDisplayUnitSession.this;
            setImplicitDisplayUnitSession.enterStateAsync(setImplicitDisplayUnitSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDisplayUnitSession.this.log("Set Display Unit timeout. Cancel.");
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDisplayUnitSession(UserDisplayUnit userDisplayUnit, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, bleAdapterImpl, bleSessionCallback);
        wg6.b(userDisplayUnit, "mUserDisplayUnit");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mUserDisplayUnit = userDisplayUnit;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetImplicitDisplayUnitSession setImplicitDisplayUnitSession = new SetImplicitDisplayUnitSession(this.mUserDisplayUnit, getBleAdapter(), getBleSessionCallback());
        setImplicitDisplayUnitSession.setDevice(getDevice());
        return setImplicitDisplayUnitSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DISPLAY_UNIT_STATE;
        String name = SetDisplayUnitState.class.getName();
        wg6.a((Object) name, "SetDisplayUnitState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
