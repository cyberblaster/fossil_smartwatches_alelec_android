package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import android.util.Log;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.wg6;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalFLogger implements ILocalFLogger {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String LOG_PATTERN; // = "%s %s: %s\n";
    @DexIgnore
    public FileLogWriter fileLogWriter;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public String prefix; // = "";
    @DexIgnore
    public /* final */ SimpleDateFormat sdf; // = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS", Locale.US);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    private final boolean getCanWriteLog() {
        return this.isInitialized && this.isDebuggable;
    }

    @DexIgnore
    public void d(String str, String str2) {
        wg6.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.d(this.prefix + " - " + str, str2);
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /D", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    public void e(String str, String str2) {
        wg6.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.e(this.prefix + " - " + str, str2);
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /E", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r0.exportLogs();
     */
    @DexIgnore
    public List<File> exportAppLogs() {
        List<File> exportLogs;
        FileLogWriter fileLogWriter2 = this.fileLogWriter;
        return (fileLogWriter2 == null || exportLogs == null) ? new ArrayList() : exportLogs;
    }

    @DexIgnore
    public void i(String str, String str2) {
        wg6.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.i(this.prefix + " - " + str, str2);
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /I", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    public void init(Context context, String str, boolean z) {
        wg6.b(context, "context");
        wg6.b(str, "prefix");
        this.prefix = str;
        this.fileLogWriter = new FileLogWriter();
        FileLogWriter fileLogWriter2 = this.fileLogWriter;
        if (fileLogWriter2 != null) {
            String file = context.getFilesDir().toString();
            wg6.a((Object) file, "context.filesDir.toString()");
            fileLogWriter2.startWriter(file);
        }
        this.isDebuggable = z;
        this.isInitialized = true;
    }

    @DexIgnore
    public void v(String str, String str2) {
        wg6.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.v(this.prefix + " - " + str, str2);
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /V", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }
}
