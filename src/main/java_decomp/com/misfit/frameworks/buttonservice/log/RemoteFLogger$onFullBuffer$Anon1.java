package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1", f = "RemoteFLogger.kt", l = {153}, m = "invokeSuspend")
public final class RemoteFLogger$onFullBuffer$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public /* final */ /* synthetic */ List $logLines;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$onFullBuffer$Anon1(RemoteFLogger remoteFLogger, List list, boolean z, xe6 xe6) {
        super(2, xe6);
        this.this$0 = remoteFLogger;
        this.$logLines = list;
        this.$forceFlush = z;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        RemoteFLogger$onFullBuffer$Anon1 remoteFLogger$onFullBuffer$Anon1 = new RemoteFLogger$onFullBuffer$Anon1(this.this$0, this.$logLines, this.$forceFlush, xe6);
        remoteFLogger$onFullBuffer$Anon1.p$ = (il6) obj;
        return remoteFLogger$onFullBuffer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$onFullBuffer$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            if (this.this$0.isMainFLogger) {
                DBLogWriter access$getDbLogWriter$p = this.this$0.dbLogWriter;
                if (access$getDbLogWriter$p != null) {
                    access$getDbLogWriter$p.writeLog(this.$logLines);
                }
                if (this.$forceFlush) {
                    RemoteFLogger remoteFLogger = this.this$0;
                    this.L$0 = il6;
                    this.label = 1;
                    if (remoteFLogger.flushDB(this) == a) {
                        return a;
                    }
                }
            } else if (!this.$forceFlush) {
                FLogger.INSTANCE.getLocal().d(RemoteFLogger.TAG, ".onFullBuffer(), broadcast");
                RemoteFLogger remoteFLogger2 = this.this$0;
                String access$getFloggerName$p = remoteFLogger2.floggerName;
                RemoteFLogger.MessageTarget messageTarget = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle = Bundle.EMPTY;
                wg6.a((Object) bundle, "Bundle.EMPTY");
                remoteFLogger2.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER, access$getFloggerName$p, messageTarget, bundle);
            } else {
                RemoteFLogger remoteFLogger3 = this.this$0;
                String access$getFloggerName$p2 = remoteFLogger3.floggerName;
                RemoteFLogger.MessageTarget messageTarget2 = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle2 = Bundle.EMPTY;
                wg6.a((Object) bundle2, "Bundle.EMPTY");
                remoteFLogger3.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, access$getFloggerName$p2, messageTarget2, bundle2);
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
