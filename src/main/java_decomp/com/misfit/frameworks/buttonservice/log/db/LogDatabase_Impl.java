package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogDatabase_Impl extends LogDatabase {
    @DexIgnore
    public volatile LogDao _logDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `log` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timeStamp` INTEGER NOT NULL, `content` TEXT NOT NULL, `cloudFlag` TEXT NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1e76ae5948872f78452bd16b18a13dc4')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `log`");
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) LogDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) LogDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = LogDatabase_Impl.this.mDatabase = iiVar;
            LogDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) LogDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(4);
            hashMap.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap.put("timeStamp", new fi.a("timeStamp", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("content", new fi.a("content", "TEXT", true, 0, (String) null, 1));
            hashMap.put("cloudFlag", new fi.a("cloudFlag", "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("log", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "log");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "log(com.misfit.frameworks.buttonservice.log.db.Log).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        LogDatabase_Impl.super.assertNotMainThread();
        ii a = LogDatabase_Impl.super.getOpenHelper().a();
        try {
            LogDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `log`");
            LogDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            LogDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"log"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "1e76ae5948872f78452bd16b18a13dc4", "3ef7199be598e6814da70cf3eed15c90");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public LogDao getLogDao() {
        LogDao logDao;
        if (this._logDao != null) {
            return this._logDao;
        }
        synchronized (this) {
            if (this._logDao == null) {
                this._logDao = new LogDao_Impl(this);
            }
            logDao = this._logDao;
        }
        return logDao;
    }
}
