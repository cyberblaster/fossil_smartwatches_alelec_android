package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.ej6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xp6;
import com.fossil.yf6;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.FileLogWriter$pollLogEvent$1", f = "FileLogWriter.kt", l = {133}, m = "invokeSuspend")
public final class FileLogWriter$pollLogEvent$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FileLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileLogWriter$pollLogEvent$Anon1(FileLogWriter fileLogWriter, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fileLogWriter;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        FileLogWriter$pollLogEvent$Anon1 fileLogWriter$pollLogEvent$Anon1 = new FileLogWriter$pollLogEvent$Anon1(this.this$0, xe6);
        fileLogWriter$pollLogEvent$Anon1.p$ = (il6) obj;
        return fileLogWriter$pollLogEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((FileLogWriter$pollLogEvent$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0115, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        com.fossil.yf6.a(r6, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0119, code lost:
        throw r1;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        xp6 xp6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            xp6 access$getMFileLogWriterMutex$p = this.this$0.mFileLogWriterMutex;
            this.L$0 = il6;
            this.L$1 = access$getMFileLogWriterMutex$p;
            this.label = 1;
            if (access$getMFileLogWriterMutex$p.a((Object) null, this) == a) {
                return a;
            }
            xp6 = access$getMFileLogWriterMutex$p;
        } else if (i == 1) {
            xp6 = (xp6) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            if (!xj6.a(this.this$0.directoryPath)) {
                try {
                    FileLogWriter fileLogWriter = this.this$0;
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {hf6.a(0)};
                    String format = String.format(FileLogWriter.FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    File file = new File(fileLogWriter.getFilePath(format));
                    File parentFile = file.getParentFile();
                    Boolean a2 = parentFile != null ? hf6.a(parentFile.exists()) : null;
                    if (a2 != null) {
                        if (!a2.booleanValue()) {
                            File parentFile2 = file.getParentFile();
                            if (parentFile2 != null) {
                                parentFile2.mkdirs();
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        if (!file.exists()) {
                            file.createNewFile();
                        } else if (file.length() >= FileLogWriter.FILE_LOG_SIZE_THRESHOLD) {
                            this.this$0.rotateFiles();
                        }
                        String str = (String) this.this$0.logEventQueue.poll();
                        if (str != null) {
                            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                            byte[] bytes = str.getBytes(ej6.a);
                            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                            fileOutputStream.write(bytes);
                            fileOutputStream.flush();
                            cd6 cd6 = cd6.a;
                            yf6.a(fileOutputStream, (Throwable) null);
                            if (this.this$0.debugOption != null) {
                                FileLogWriter fileLogWriter2 = this.this$0;
                                int access$getMCount$p = fileLogWriter2.mCount;
                                fileLogWriter2.mCount = access$getMCount$p + 1;
                                hf6.a(access$getMCount$p);
                            }
                            if (file.length() >= FileLogWriter.FILE_LOG_SIZE_THRESHOLD) {
                                this.this$0.rotateFiles();
                            }
                            FileDebugOption access$getDebugOption$p = this.this$0.debugOption;
                            if (access$getDebugOption$p != null && this.this$0.mCount == access$getDebugOption$p.getMaximumLog()) {
                                access$getDebugOption$p.getCallback().onFinish();
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } catch (Exception e) {
                    System.out.print(e.toString());
                }
            }
            cd6 cd62 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } catch (Throwable th) {
            xp6.a((Object) null);
            throw th;
        }
    }
}
