package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FLogger {
    @DexIgnore
    public static /* final */ FLogger INSTANCE; // = new FLogger();
    @DexIgnore
    public static /* final */ ILocalFLogger local; // = new LocalFLogger();
    @DexIgnore
    public static /* final */ IRemoteFLogger remote; // = new RemoteFLogger();

    @DexIgnore
    public enum Component {
        API,
        DB,
        APP,
        BLE
    }

    @DexIgnore
    public enum LogLevel {
        INFO,
        DEBUG,
        ERROR,
        SUMMARY
    }

    @DexIgnore
    public enum Session {
        PAIR,
        OTA,
        SYNC,
        SET_COMPLICATION,
        SET_WATCH_APPS,
        SET_PRESET_APPS,
        SET_ALARM,
        HANDLE_WATCH_REQUEST,
        EXCHANGE_KEY,
        VERIFY_SECRET_KEY,
        SWITCH_DEVICE,
        REMOVE_DEVICE,
        DIANA_COMMUTE_TIME,
        OTHER
    }

    @DexIgnore
    public final ILocalFLogger getLocal() {
        return local;
    }

    @DexIgnore
    public final IRemoteFLogger getRemote() {
        return remote;
    }

    @DexIgnore
    public final void init(String str, AppLogInfo appLogInfo, ActiveDeviceInfo activeDeviceInfo, CloudLogConfig cloudLogConfig, Context context, boolean z, String str2) {
        wg6.b(str, "name");
        wg6.b(appLogInfo, "appLogInfo");
        wg6.b(activeDeviceInfo, "activeDeviceInfo");
        wg6.b(cloudLogConfig, "cloudLogConfig");
        wg6.b(context, "context");
        wg6.b(str2, "prefixForLocalLog");
        boolean z2 = (context.getApplicationInfo().flags & 2) != 0;
        local.init(context, str2, z2);
        remote.init(str, appLogInfo, activeDeviceInfo, cloudLogConfig, context, z, z2);
    }

    @DexIgnore
    public final void updateActiveDeviceInfo(ActiveDeviceInfo activeDeviceInfo) {
        wg6.b(activeDeviceInfo, "activeDeviceInfo");
        remote.updateActiveDeviceInfo(activeDeviceInfo);
    }

    @DexIgnore
    public final void updateAppLogInfo(AppLogInfo appLogInfo) {
        wg6.b(appLogInfo, "appLogInfo");
        remote.updateAppLogInfo(appLogInfo);
    }

    @DexIgnore
    public final void updateCloudLogConfig(CloudLogConfig cloudLogConfig) {
        wg6.b(cloudLogConfig, "cloudLogConfig");
        remote.updateCloudLogConfig(cloudLogConfig);
    }

    @DexIgnore
    public final void updateSessionDetailInfo(SessionDetailInfo sessionDetailInfo) {
        wg6.b(sessionDetailInfo, "sessionDetailInfo");
        remote.updateSessionDetailInfo(sessionDetailInfo);
    }
}
