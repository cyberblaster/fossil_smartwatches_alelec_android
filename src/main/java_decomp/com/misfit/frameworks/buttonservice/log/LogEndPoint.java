package com.misfit.frameworks.buttonservice.log;

import com.fossil.wg6;
import com.fossil.xj6;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPoint {
    @DexIgnore
    public static /* final */ LogEndPoint INSTANCE; // = new LogEndPoint();
    @DexIgnore
    public static LogApiService logApiService;

    @DexIgnore
    public final LogApiService getLogApiService() {
        return logApiService;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4) {
        wg6.b(str, "logBrandName");
        wg6.b(str2, "logBaseUrl");
        wg6.b(str3, "accessKey");
        wg6.b(str4, "secretKey");
        if ((!xj6.a(str2)) && (!xj6.a(str3)) && (!xj6.a(str4))) {
            if (!xj6.a(str2, ZendeskConfig.SLASH, false, 2, (Object) null)) {
                str2 = str2 + ZendeskConfig.SLASH;
            }
            HttpLoggingInterceptor a = new HttpLoggingInterceptor(LogEndPoint$init$httpLogInterceptor$Anon1.INSTANCE).a(wg6.a((Object) "release", (Object) "release") ? HttpLoggingInterceptor.a.BASIC : HttpLoggingInterceptor.a.BODY);
            Retrofit.b bVar = new Retrofit.b();
            bVar.a(str2);
            bVar.a(GsonConverterFactory.a());
            OkHttpClient.b bVar2 = new OkHttpClient.b();
            bVar2.a((Interceptor) new LogInterceptor(str, str3, str4));
            bVar2.a((Interceptor) a);
            bVar.a(bVar2.a());
            logApiService = (LogApiService) bVar.a().a(LogApiService.class);
        }
    }

    @DexIgnore
    public final void setLogApiService(LogApiService logApiService2) {
        logApiService = logApiService2;
    }
}
