package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.db.Log;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogFlagConverter {
    @DexIgnore
    public final String logFlagEnumToString(Log.Flag flag) {
        wg6.b(flag, ServerSetting.VALUE);
        return flag.getValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r2 = com.misfit.frameworks.buttonservice.log.db.Log.Flag.Companion.fromValue(r2);
     */
    @DexIgnore
    public final Log.Flag stringToLogFlag(String str) {
        Log.Flag fromValue;
        return (str == null || fromValue == null) ? Log.Flag.ADD : fromValue;
    }
}
