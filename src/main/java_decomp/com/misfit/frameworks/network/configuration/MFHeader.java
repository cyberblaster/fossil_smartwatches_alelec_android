package com.misfit.frameworks.network.configuration;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFHeader {
    @DexIgnore
    public HashMap<String, String> extraInfoHeader; // = new HashMap<>();

    @DexIgnore
    public void addAll(HashMap<String, String> hashMap) {
        this.extraInfoHeader.putAll(hashMap);
    }

    @DexIgnore
    public void addHeader(String str, String str2) {
        this.extraInfoHeader.put(str, str2);
    }

    @DexIgnore
    public HashMap<String, String> getHeaderMap() {
        return this.extraInfoHeader;
    }
}
