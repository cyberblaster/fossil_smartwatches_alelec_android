package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ButtonLinkOwnership {
    ButtonLinkOwnershipUnknown(-1),
    ButtonLinkOwnershipNoLinking(0),
    ButtonLinkOwnershipLinkingOnlyOwnerAccount(1),
    ButtonLinkOwnershipLinkingManyAccount(2);
    
    @DexIgnore
    public int value;

    @DexIgnore
    public ButtonLinkOwnership(int i) {
        this.value = i;
    }

    @DexIgnore
    public static ButtonLinkOwnership fromInt(int i) {
        for (ButtonLinkOwnership buttonLinkOwnership : values()) {
            if (buttonLinkOwnership.getValue() == i) {
                return buttonLinkOwnership;
            }
        }
        return null;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
