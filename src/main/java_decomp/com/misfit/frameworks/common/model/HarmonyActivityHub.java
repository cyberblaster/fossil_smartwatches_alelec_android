package com.misfit.frameworks.common.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HarmonyActivityHub {
    @DexIgnore
    public String hubID;
    @DexIgnore
    public String id;
    @DexIgnore
    public boolean isCheck;
    @DexIgnore
    public String name;

    @DexIgnore
    public HarmonyActivityHub() {
        this.hubID = "";
        this.id = "";
        this.name = "";
        this.isCheck = false;
    }

    @DexIgnore
    public String getHubID() {
        return this.hubID;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public boolean isCheck() {
        return this.isCheck;
    }

    @DexIgnore
    public void setCheck(boolean z) {
        this.isCheck = z;
    }

    @DexIgnore
    public void setHubID(String str) {
        this.hubID = str;
    }

    @DexIgnore
    public void setID(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public HarmonyActivityHub(String str, String str2, String str3) {
        this.hubID = str;
        this.id = str2;
        this.name = str3;
        this.isCheck = false;
    }
}
