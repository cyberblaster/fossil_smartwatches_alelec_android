package com.portfolio.platform.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.fossil.f7;
import com.fossil.ky5;
import com.fossil.l0;
import com.fossil.ly5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.ColorPickerView;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ColorPickerDialog extends DialogFragment implements ColorPickerView.c, TextWatcher {
    @DexIgnore
    public static /* final */ int[] y; // = {-769226, -1499549, -54125, -6543440, -10011977, -12627531, -14575885, -16537100, -16728876, -16738680, -11751600, -7617718, -3285959, -5317, -16121, -26624, -8825528, -10453621, -6381922};
    @DexIgnore
    public ly5 a;
    @DexIgnore
    public FrameLayout b;
    @DexIgnore
    public int[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;
    @DexIgnore
    public ky5 i;
    @DexIgnore
    public LinearLayout j;
    @DexIgnore
    public SeekBar o;
    @DexIgnore
    public TextView p;
    @DexIgnore
    public ColorPickerView q;
    @DexIgnore
    public ColorPanelView r;
    @DexIgnore
    public EditText s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public /* final */ View.OnTouchListener x; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements SeekBar.OnSeekBarChangeListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            ky5 ky5;
            ColorPickerDialog.this.p.setText(String.format(Locale.ENGLISH, "%d%%", new Object[]{Integer.valueOf((int) ((((double) i) * 100.0d) / 255.0d))}));
            int i2 = 255 - i;
            int i3 = 0;
            while (true) {
                ky5 = ColorPickerDialog.this.i;
                int[] iArr = ky5.b;
                if (i3 >= iArr.length) {
                    break;
                }
                int i4 = iArr[i3];
                ColorPickerDialog.this.i.b[i3] = Color.argb(i2, Color.red(i4), Color.green(i4), Color.blue(i4));
                i3++;
            }
            ky5.notifyDataSetChanged();
            for (int i5 = 0; i5 < ColorPickerDialog.this.j.getChildCount(); i5++) {
                FrameLayout frameLayout = (FrameLayout) ColorPickerDialog.this.j.getChildAt(i5);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362127);
                ImageView imageView = (ImageView) frameLayout.findViewById(2131362124);
                if (frameLayout.getTag() == null) {
                    frameLayout.setTag(Integer.valueOf(colorPanelView.getBorderColor()));
                }
                int color = colorPanelView.getColor();
                int argb = Color.argb(i2, Color.red(color), Color.green(color), Color.blue(color));
                if (i2 <= 165) {
                    colorPanelView.setBorderColor(argb | -16777216);
                } else {
                    colorPanelView.setBorderColor(((Integer) frameLayout.getTag()).intValue());
                }
                if (colorPanelView.getTag() != null && ((Boolean) colorPanelView.getTag()).booleanValue()) {
                    if (i2 <= 165) {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    } else if (f7.a(argb) >= 0.65d) {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    } else {
                        imageView.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
                    }
                }
                colorPanelView.setColor(argb);
            }
            int red = Color.red(ColorPickerDialog.this.d);
            int green = Color.green(ColorPickerDialog.this.d);
            int blue = Color.blue(ColorPickerDialog.this.d);
            ColorPickerDialog.this.d = Color.argb(i2, red, green, blue);
        }

        @DexIgnore
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @DexIgnore
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnTouchListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            EditText editText = ColorPickerDialog.this.s;
            if (view == editText || !editText.hasFocus()) {
                return false;
            }
            ColorPickerDialog.this.s.clearFocus();
            ((InputMethodManager) ColorPickerDialog.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(ColorPickerDialog.this.s.getWindowToken(), 0);
            ColorPickerDialog.this.s.clearFocus();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements DialogInterface.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            colorPickerDialog.t(colorPickerDialog.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            ColorPickerDialog.this.b.removeAllViews();
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            int i = colorPickerDialog.e;
            if (i == 0) {
                colorPickerDialog.e = 1;
                ((Button) view).setText(colorPickerDialog.w != 0 ? ColorPickerDialog.this.w : 2131887127);
                ColorPickerDialog colorPickerDialog2 = ColorPickerDialog.this;
                colorPickerDialog2.b.addView(colorPickerDialog2.e1());
            } else if (i == 1) {
                colorPickerDialog.e = 0;
                ((Button) view).setText(colorPickerDialog.u != 0 ? ColorPickerDialog.this.u : 2131887251);
                ColorPickerDialog colorPickerDialog3 = ColorPickerDialog.this;
                colorPickerDialog3.b.addView(colorPickerDialog3.d1());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements View.OnClickListener {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onClick(View view) {
            int color = ColorPickerDialog.this.r.getColor();
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            int i = colorPickerDialog.d;
            if (color == i) {
                colorPickerDialog.t(i);
                ColorPickerDialog.this.dismiss();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements View.OnFocusChangeListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            if (z) {
                ((InputMethodManager) ColorPickerDialog.this.getActivity().getSystemService("input_method")).showSoftInput(ColorPickerDialog.this.s, 1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements ky5.a {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void a(int i) {
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            int i2 = colorPickerDialog.d;
            if (i2 == i) {
                colorPickerDialog.t(i2);
                ColorPickerDialog.this.dismiss();
                return;
            }
            colorPickerDialog.d = i;
            if (colorPickerDialog.g) {
                colorPickerDialog.r(colorPickerDialog.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public h(ColorPickerDialog colorPickerDialog, ColorPanelView colorPanelView, int i) {
            this.a = colorPanelView;
            this.b = i;
        }

        @DexIgnore
        public void run() {
            this.a.setColor(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView a;

        @DexIgnore
        public i(ColorPanelView colorPanelView) {
            this.a = colorPanelView;
        }

        @DexIgnore
        public void onClick(View view) {
            if (!(view.getTag() instanceof Boolean) || !((Boolean) view.getTag()).booleanValue()) {
                ColorPickerDialog.this.d = this.a.getColor();
                ColorPickerDialog.this.i.a();
                for (int i = 0; i < ColorPickerDialog.this.j.getChildCount(); i++) {
                    FrameLayout frameLayout = (FrameLayout) ColorPickerDialog.this.j.getChildAt(i);
                    ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362127);
                    ImageView imageView = (ImageView) frameLayout.findViewById(2131362124);
                    imageView.setImageResource(colorPanelView == view ? 2131230974 : 0);
                    if ((colorPanelView != view || f7.a(colorPanelView.getColor()) < 0.65d) && Color.alpha(colorPanelView.getColor()) > 165) {
                        imageView.setColorFilter((ColorFilter) null);
                    } else {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    }
                    colorPanelView.setTag(Boolean.valueOf(colorPanelView == view));
                }
                return;
            }
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            colorPickerDialog.t(colorPickerDialog.d);
            ColorPickerDialog.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView a;

        @DexIgnore
        public j(ColorPickerDialog colorPickerDialog, ColorPanelView colorPanelView) {
            this.a = colorPanelView;
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            this.a.c();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k {
        @DexIgnore
        public int a; // = 2131887241;
        @DexIgnore
        public int b; // = 2131887251;
        @DexIgnore
        public int c; // = 2131887127;
        @DexIgnore
        public int d; // = 2131887310;
        @DexIgnore
        public int e; // = 1;
        @DexIgnore
        public int[] f; // = ColorPickerDialog.y;
        @DexIgnore
        public int g; // = -16777216;
        @DexIgnore
        public int h; // = 0;
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public boolean j; // = true;
        @DexIgnore
        public boolean k; // = true;
        @DexIgnore
        public boolean l; // = true;
        @DexIgnore
        public int m; // = 1;

        @DexIgnore
        public k a(int i2) {
            this.g = i2;
            return this;
        }

        @DexIgnore
        public k b(int i2) {
            this.h = i2;
            return this;
        }

        @DexIgnore
        public k c(int i2) {
            this.e = i2;
            return this;
        }

        @DexIgnore
        public k a(boolean z) {
            this.j = z;
            return this;
        }

        @DexIgnore
        public k b(boolean z) {
            this.i = z;
            return this;
        }

        @DexIgnore
        public ColorPickerDialog a() {
            ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
            Bundle bundle = new Bundle();
            bundle.putInt("id", this.h);
            bundle.putInt("dialogType", this.e);
            bundle.putInt("color", this.g);
            bundle.putIntArray("presets", this.f);
            bundle.putBoolean("alpha", this.i);
            bundle.putBoolean("allowCustom", this.k);
            bundle.putBoolean("allowPresets", this.j);
            bundle.putInt("dialogTitle", this.a);
            bundle.putBoolean("showColorShades", this.l);
            bundle.putInt("colorShape", this.m);
            bundle.putInt("presetsButtonText", this.b);
            bundle.putInt("customButtonText", this.c);
            bundle.putInt("selectedButtonText", this.d);
            colorPickerDialog.setArguments(bundle);
            return colorPickerDialog;
        }
    }

    @DexIgnore
    public static k i1() {
        return new k();
    }

    @DexIgnore
    public final int W(String str) throws NumberFormatException {
        int i2;
        int i3;
        if (str.startsWith("#")) {
            str = str.substring(1);
        }
        int i4 = -1;
        int i5 = 255;
        if (str.length() == 0) {
            i2 = 0;
        } else if (str.length() <= 2) {
            i2 = Integer.parseInt(str, 16);
        } else {
            if (str.length() == 3) {
                i4 = Integer.parseInt(str.substring(0, 1), 16);
                i3 = Integer.parseInt(str.substring(1, 2), 16);
                i2 = Integer.parseInt(str.substring(2, 3), 16);
            } else if (str.length() == 4) {
                int parseInt = Integer.parseInt(str.substring(0, 2), 16);
                i2 = Integer.parseInt(str.substring(2, 4), 16);
                i3 = parseInt;
                i4 = 0;
            } else if (str.length() == 5) {
                i4 = Integer.parseInt(str.substring(0, 1), 16);
                i3 = Integer.parseInt(str.substring(1, 3), 16);
                i2 = Integer.parseInt(str.substring(3, 5), 16);
            } else if (str.length() == 6) {
                i4 = Integer.parseInt(str.substring(0, 2), 16);
                i3 = Integer.parseInt(str.substring(2, 4), 16);
                i2 = Integer.parseInt(str.substring(4, 6), 16);
            } else if (str.length() == 7) {
                int parseInt2 = Integer.parseInt(str.substring(0, 1), 16);
                int parseInt3 = Integer.parseInt(str.substring(1, 3), 16);
                int parseInt4 = Integer.parseInt(str.substring(3, 5), 16);
                i2 = Integer.parseInt(str.substring(5, 7), 16);
                i5 = parseInt2;
                i4 = parseInt3;
                i3 = parseInt4;
            } else if (str.length() == 8) {
                int parseInt5 = Integer.parseInt(str.substring(0, 2), 16);
                int parseInt6 = Integer.parseInt(str.substring(2, 4), 16);
                int parseInt7 = Integer.parseInt(str.substring(4, 6), 16);
                i2 = Integer.parseInt(str.substring(6, 8), 16);
                int i6 = parseInt5;
                i4 = parseInt6;
                i3 = parseInt7;
                i5 = i6;
            } else {
                i2 = -1;
                i3 = -1;
                i5 = -1;
            }
            return Color.argb(i5, i4, i3, i2);
        }
        i3 = 0;
        i4 = 0;
        return Color.argb(i5, i4, i3, i2);
    }

    @DexIgnore
    public void afterTextChanged(Editable editable) {
        int W;
        if (this.s.isFocused() && (W = W(editable.toString())) != this.q.getColor()) {
            this.v = true;
            this.q.a(W, true);
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public View d1() {
        View inflate = View.inflate(getActivity(), 2131558455, (ViewGroup) null);
        this.q = (ColorPickerView) inflate.findViewById(2131362128);
        this.r = (ColorPanelView) inflate.findViewById(2131362125);
        this.s = (EditText) inflate.findViewById(2131362129);
        try {
            TypedValue typedValue = new TypedValue();
            getActivity().obtainStyledAttributes(typedValue.data, new int[]{16842806}).recycle();
        } catch (Exception unused) {
        }
        this.q.setAlphaSliderVisible(this.t);
        this.q.a(this.d, true);
        this.r.setColor(this.d);
        u(this.d);
        if (!this.t) {
            this.s.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }
        this.r.setOnClickListener(new e());
        inflate.setOnTouchListener(this.x);
        this.q.setOnColorChangedListener(this);
        this.s.addTextChangedListener(this);
        this.s.setOnFocusChangeListener(new f());
        return inflate;
    }

    @DexIgnore
    public View e1() {
        View inflate = View.inflate(getActivity(), 2131558456, (ViewGroup) null);
        this.j = (LinearLayout) inflate.findViewById(2131362941);
        this.o = (SeekBar) inflate.findViewById(2131363050);
        this.p = (TextView) inflate.findViewById(2131363051);
        GridView gridView = (GridView) inflate.findViewById(2131362464);
        f1();
        if (this.g) {
            r(this.d);
        } else {
            this.j.setVisibility(8);
            inflate.findViewById(2131362940).setVisibility(8);
        }
        this.i = new ky5(new g(), this.c, getSelectedItemPosition(), this.h);
        gridView.setAdapter(this.i);
        if (this.t) {
            h1();
        } else {
            inflate.findViewById(2131363049).setVisibility(8);
            inflate.findViewById(2131363052).setVisibility(8);
        }
        return inflate;
    }

    @DexIgnore
    public final void f1() {
        int alpha = Color.alpha(this.d);
        this.c = getArguments().getIntArray("presets");
        if (this.c == null) {
            this.c = y;
        }
        boolean z = this.c == y;
        int[] iArr = this.c;
        this.c = Arrays.copyOf(iArr, iArr.length);
        if (alpha != 255) {
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.c;
                if (i2 >= iArr2.length) {
                    break;
                }
                int i3 = iArr2[i2];
                this.c[i2] = Color.argb(alpha, Color.red(i3), Color.green(i3), Color.blue(i3));
                i2++;
            }
        }
        this.c = b(this.c, this.d);
        int i4 = getArguments().getInt("color");
        if (i4 != this.d) {
            this.c = b(this.c, i4);
        }
        if (z) {
            int[] iArr3 = this.c;
            if (iArr3.length == 19) {
                this.c = a(iArr3, Color.argb(alpha, 0, 0, 0));
            }
        }
    }

    @DexIgnore
    public final void g1() {
        if (this.a != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.a.p(this.f);
            return;
        }
        ly5 activity = getActivity();
        if (activity instanceof ly5) {
            activity.p(this.f);
        }
        ly5 fragmentManager = getFragmentManager();
        if (fragmentManager instanceof ly5) {
            fragmentManager.d(this.f, this.d);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ColorPickerDialog", "fm=" + fragmentManager);
    }

    @DexIgnore
    public final int getSelectedItemPosition() {
        int i2 = 0;
        while (true) {
            int[] iArr = this.c;
            if (i2 >= iArr.length) {
                return -1;
            }
            if (iArr[i2] == this.d) {
                return i2;
            }
            i2++;
        }
    }

    @DexIgnore
    public final void h1() {
        int alpha = 255 - Color.alpha(this.d);
        this.o.setMax(255);
        this.o.setProgress(alpha);
        this.p.setText(String.format(Locale.ENGLISH, "%d%%", new Object[]{Integer.valueOf((int) ((((double) alpha) * 100.0d) / 255.0d))}));
        this.o.setOnSeekBarChangeListener(new a());
    }

    @DexIgnore
    public void onAttach(Context context) {
        ColorPickerDialog.super.onAttach(context);
        ly5 parentFragment = getParentFragment();
        if (parentFragment != null && (parentFragment instanceof ly5)) {
            this.a = parentFragment;
        }
        if (this.a == null && (context instanceof ly5)) {
            this.a = (ly5) context;
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        int i2;
        this.f = getArguments().getInt("id");
        this.t = getArguments().getBoolean("alpha");
        this.g = getArguments().getBoolean("showColorShades");
        this.h = getArguments().getInt("colorShape");
        if (bundle == null) {
            this.d = getArguments().getInt("color");
            this.e = getArguments().getInt("dialogType");
        } else {
            this.d = bundle.getInt("color");
            this.e = bundle.getInt("dialogType");
        }
        this.b = new FrameLayout(requireActivity());
        int i3 = this.e;
        if (i3 == 0) {
            this.b.addView(d1());
        } else if (i3 == 1) {
            this.b.addView(e1());
        }
        int i4 = getArguments().getInt("selectedButtonText");
        if (i4 == 0) {
            i4 = 2131887310;
        }
        l0.a aVar = new l0.a(requireActivity());
        aVar.b(this.b);
        aVar.b(i4, new c());
        int i5 = getArguments().getInt("dialogTitle");
        if (i5 != 0) {
            aVar.a(i5);
        }
        this.u = getArguments().getInt("presetsButtonText");
        this.w = getArguments().getInt("customButtonText");
        if (this.e == 0 && getArguments().getBoolean("allowPresets")) {
            i2 = this.u;
            if (i2 == 0) {
                i2 = 2131887127;
            }
        } else if (this.e != 1 || !getArguments().getBoolean("allowCustom")) {
            i2 = 0;
        } else {
            i2 = this.w;
            if (i2 == 0) {
                i2 = 2131887251;
            }
        }
        if (i2 != 0) {
            aVar.a(i2, (DialogInterface.OnClickListener) null);
        }
        return aVar.a();
    }

    @DexIgnore
    public void onDetach() {
        ColorPickerDialog.super.onDetach();
        this.a = null;
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        ColorPickerDialog.super.onDismiss(dialogInterface);
        g1();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("color", this.d);
        bundle.putInt("dialogType", this.e);
        ColorPickerDialog.super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void onStart() {
        ColorPickerDialog.super.onStart();
        l0 dialog = getDialog();
        dialog.getWindow().clearFlags(131080);
        dialog.getWindow().setSoftInputMode(4);
        Button b2 = dialog.b(-3);
        if (b2 != null) {
            b2.setOnClickListener(new d());
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void q(int i2) {
        this.d = i2;
        ColorPanelView colorPanelView = this.r;
        if (colorPanelView != null) {
            colorPanelView.setColor(i2);
        }
        if (!this.v && this.s != null) {
            u(i2);
            if (this.s.hasFocus()) {
                ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.s.getWindowToken(), 0);
                this.s.clearFocus();
            }
        }
        this.v = false;
    }

    @DexIgnore
    public void r(int i2) {
        int[] s2 = s(i2);
        if (this.j.getChildCount() != 0) {
            for (int i3 = 0; i3 < this.j.getChildCount(); i3++) {
                FrameLayout frameLayout = (FrameLayout) this.j.getChildAt(i3);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362127);
                colorPanelView.setColor(s2[i3]);
                colorPanelView.setTag(false);
                ((ImageView) frameLayout.findViewById(2131362124)).setImageDrawable((Drawable) null);
            }
            return;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(2131165314);
        for (int i4 : s2) {
            View inflate = View.inflate(getActivity(), this.h == 0 ? 2131558454 : 2131558453, (ViewGroup) null);
            ColorPanelView colorPanelView2 = (ColorPanelView) inflate.findViewById(2131362127);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) colorPanelView2.getLayoutParams();
            marginLayoutParams.rightMargin = dimensionPixelSize;
            marginLayoutParams.leftMargin = dimensionPixelSize;
            colorPanelView2.setLayoutParams(marginLayoutParams);
            colorPanelView2.setColor(i4);
            this.j.addView(inflate);
            colorPanelView2.post(new h(this, colorPanelView2, i4));
            colorPanelView2.setOnClickListener(new i(colorPanelView2));
            colorPanelView2.setOnLongClickListener(new j(this, colorPanelView2));
        }
    }

    @DexIgnore
    public final int[] s(int i2) {
        return new int[]{a(i2, 0.9d), a(i2, 0.7d), a(i2, 0.5d), a(i2, 0.333d), a(i2, 0.166d), a(i2, -0.125d), a(i2, -0.25d), a(i2, -0.375d), a(i2, -0.5d), a(i2, -0.675d), a(i2, -0.7d), a(i2, -0.775d)};
    }

    @DexIgnore
    public final void t(int i2) {
        if (this.a != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.a.d(this.f, i2);
            return;
        }
        ly5 activity = getActivity();
        if (activity instanceof ly5) {
            activity.d(this.f, i2);
            ly5 fragmentManager = getFragmentManager();
            if (fragmentManager instanceof ly5) {
                fragmentManager.d(this.f, i2);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ColorPickerDialog", "onColorSelected fm=" + fragmentManager);
            return;
        }
        throw new IllegalStateException("The activity must implement ColorPickerDialogListener");
    }

    @DexIgnore
    public final void u(int i2) {
        if (this.t) {
            this.s.setText(String.format("%08X", new Object[]{Integer.valueOf(i2)}));
            return;
        }
        this.s.setText(String.format("%06X", new Object[]{Integer.valueOf(i2 & 16777215)}));
    }

    @DexIgnore
    public final int[] b(int[] iArr, int i2) {
        boolean z;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z = false;
                break;
            } else if (iArr[i3] == i2) {
                z = true;
                break;
            } else {
                i3++;
            }
        }
        if (z) {
            return iArr;
        }
        int[] iArr2 = new int[(iArr.length + 1)];
        iArr2[0] = i2;
        System.arraycopy(iArr, 0, iArr2, 1, iArr2.length - 1);
        return iArr2;
    }

    @DexIgnore
    public final int a(int i2, double d2) {
        long parseLong = Long.parseLong(String.format("#%06X", new Object[]{Integer.valueOf(16777215 & i2)}).substring(1), 16);
        double d3 = 0.0d;
        int i3 = (d2 > 0.0d ? 1 : (d2 == 0.0d ? 0 : -1));
        if (i3 >= 0) {
            d3 = 255.0d;
        }
        if (i3 < 0) {
            d2 *= -1.0d;
        }
        long j2 = parseLong >> 16;
        long j3 = (parseLong >> 8) & 255;
        long j4 = parseLong & 255;
        return Color.argb(Color.alpha(i2), (int) (Math.round((d3 - ((double) j2)) * d2) + j2), (int) (Math.round((d3 - ((double) j3)) * d2) + j3), (int) (Math.round((d3 - ((double) j4)) * d2) + j4));
    }

    @DexIgnore
    public final int[] a(int[] iArr, int i2) {
        boolean z;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z = false;
                break;
            } else if (iArr[i3] == i2) {
                z = true;
                break;
            } else {
                i3++;
            }
        }
        if (z) {
            return iArr;
        }
        int[] iArr2 = new int[(iArr.length + 1)];
        iArr2[iArr2.length - 1] = i2;
        System.arraycopy(iArr, 0, iArr2, 0, iArr2.length - 1);
        return iArr2;
    }
}
