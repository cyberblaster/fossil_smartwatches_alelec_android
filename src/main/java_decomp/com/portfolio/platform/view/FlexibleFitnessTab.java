package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.o7;
import com.fossil.rc6;
import com.fossil.u0;
import com.fossil.wg6;
import com.fossil.x24;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleFitnessTab extends ConstraintLayout {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public View u;
    @DexIgnore
    public ImageView v;
    @DexIgnore
    public FlexibleTextView w;
    @DexIgnore
    public FlexibleTextView x;
    @DexIgnore
    public String y; // = "";
    @DexIgnore
    public int z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    private final void a(AttributeSet attributeSet) {
        Context context = getContext();
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558806, this, true) : null;
        if (inflate != null) {
            this.u = inflate.findViewById(2131362068);
            this.v = (ImageView) inflate.findViewById(2131362628);
            this.w = (FlexibleTextView) inflate.findViewById(2131362435);
            this.x = (FlexibleTextView) inflate.findViewById(2131362434);
            if (attributeSet != null) {
                Context context2 = getContext();
                TypedArray obtainStyledAttributes = context2 != null ? context2.obtainStyledAttributes(attributeSet, x24.FlexibleFitnessTab) : null;
                if (obtainStyledAttributes != null) {
                    String string = obtainStyledAttributes.getString(0);
                    if (string == null) {
                        string = "hybridInactiveTab";
                    }
                    wg6.a((Object) string, "styledAttrs.getString(R.\u2026d) ?: \"hybridInactiveTab\"");
                    String b = ThemeManager.l.a().b(string);
                    if (b == null) {
                        b = "";
                    }
                    this.y = b;
                    this.z = obtainStyledAttributes.getResourceId(1, 2131231081);
                    String string2 = obtainStyledAttributes.getString(2);
                    if (string2 == null) {
                        string2 = "";
                    }
                    this.A = string2;
                    ImageView imageView = this.v;
                    if (imageView != null) {
                        imageView.setImageResource(this.z);
                        Object r0 = this.x;
                        if (r0 != 0) {
                            r0.setText(this.A);
                            obtainStyledAttributes.recycle();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            View view = this.u;
            if (view != null) {
                view.setBackgroundResource(2131231198);
                d();
                return;
            }
            wg6.a();
            throw null;
        }
        throw new rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.view.FlexibleFitnessTab, android.view.ViewGroup] */
    private final void setTabBackgroundColor(int i) {
        Context context = getContext();
        if (context != null) {
            Drawable c = u0.c(context, 2131231198);
            if (c != null) {
                o7.b(o7.i(c), i);
                View view = this.u;
                if (view != null) {
                    Drawable background = view.getBackground();
                    if (background instanceof ShapeDrawable) {
                        Paint paint = ((ShapeDrawable) background).getPaint();
                        wg6.a((Object) paint, "drawable.paint");
                        paint.setColor(i);
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(i);
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(i);
                    } else if (background instanceof StateListDrawable) {
                        background.setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
                    } else if (background instanceof BitmapDrawable) {
                        background.mutate().setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void b(String str) {
        wg6.b(str, ServerSetting.VALUE);
        Object r0 = this.x;
        if (r0 != 0) {
            r0.setText(str);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void c(String str) {
        wg6.b(str, ServerSetting.VALUE);
        Object r0 = this.w;
        if (r0 != 0) {
            r0.setText(str);
        }
    }

    @DexIgnore
    public final void d(int i) {
        setTabBackgroundColor(i);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void c(int i) {
        ImageView imageView = this.v;
        if (imageView != null) {
            imageView.setColorFilter(i);
        }
        Object r0 = this.x;
        if (r0 != 0) {
            r0.setTextColor(i);
        }
        Object r02 = this.w;
        if (r02 != 0) {
            r02.setTextColor(i);
        }
    }

    @DexIgnore
    public final void d() {
        if (!TextUtils.isEmpty(this.y)) {
            setTabBackgroundColor(Color.parseColor(this.y));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }
}
