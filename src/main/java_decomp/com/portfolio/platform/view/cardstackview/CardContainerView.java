package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import com.fossil.bz5;
import com.fossil.dz5;
import com.fossil.fz5;
import com.fossil.gy5;
import com.fossil.j9;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CardContainerView extends FrameLayout {
    @DexIgnore
    public bz5 a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public ViewGroup h;
    @DexIgnore
    public ViewGroup i;
    @DexIgnore
    public View j;
    @DexIgnore
    public View o;
    @DexIgnore
    public View p;
    @DexIgnore
    public View q;
    @DexIgnore
    public c r;
    @DexIgnore
    public GestureDetector.SimpleOnGestureListener s;
    @DexIgnore
    public GestureDetector t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            c cVar = CardContainerView.this.r;
            if (cVar == null) {
                return true;
            }
            cVar.a();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[dz5.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[dz5.TopLeft.ordinal()] = 1;
            a[dz5.TopRight.ordinal()] = 2;
            a[dz5.BottomLeft.ordinal()] = 3;
            try {
                a[dz5.BottomRight.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(float f, float f2);

        @DexIgnore
        void a(Point point, fz5 fz5);

        @DexIgnore
        void b();
    }

    @DexIgnore
    public CardContainerView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(MotionEvent motionEvent) {
        this.d = motionEvent.getRawX();
        this.e = motionEvent.getRawY();
    }

    @DexIgnore
    public final void b(MotionEvent motionEvent) {
        this.f = true;
        d(motionEvent);
        h();
        g();
        c cVar = this.r;
        if (cVar != null) {
            cVar.a(getPercentX(), getPercentY());
        }
    }

    @DexIgnore
    public final void c(MotionEvent motionEvent) {
        float f2;
        if (this.f) {
            this.f = false;
            float rawX = motionEvent.getRawX();
            float rawY = motionEvent.getRawY();
            Point c2 = gy5.c(this.d, this.e, rawX, rawY);
            dz5 a2 = gy5.a(this.d, this.e, rawX, rawY);
            double b2 = gy5.b(this.d, this.e, rawX, rawY);
            fz5 fz5 = null;
            int i2 = b.a[a2.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            if (Math.cos(Math.toRadians(360.0d - Math.toDegrees(b2))) < 0.5d) {
                                fz5 = fz5.Bottom;
                            } else {
                                fz5 = fz5.Right;
                            }
                        }
                    } else if (Math.cos(Math.toRadians(Math.toDegrees(b2) + 180.0d)) < -0.5d) {
                        fz5 = fz5.Left;
                    } else {
                        fz5 = fz5.Bottom;
                    }
                } else if (Math.cos(Math.toRadians(Math.toDegrees(b2))) < 0.5d) {
                    fz5 = fz5.Top;
                } else {
                    fz5 = fz5.Right;
                }
            } else if (Math.cos(Math.toRadians(180.0d - Math.toDegrees(b2))) < -0.5d) {
                fz5 = fz5.Left;
            } else {
                fz5 = fz5.Top;
            }
            if (fz5 == fz5.Left || fz5 == fz5.Right) {
                f2 = getPercentX();
            } else {
                f2 = getPercentY();
            }
            float abs = Math.abs(f2);
            bz5 bz5 = this.a;
            if (abs <= bz5.b) {
                a();
                c cVar = this.r;
                if (cVar != null) {
                    cVar.b();
                }
            } else if (bz5.l.contains(fz5)) {
                c cVar2 = this.r;
                if (cVar2 != null) {
                    cVar2.a(c2, fz5);
                }
            } else {
                a();
                c cVar3 = this.r;
                if (cVar3 != null) {
                    cVar3.b();
                }
            }
        }
        this.d = motionEvent.getRawX();
        this.e = motionEvent.getRawY();
    }

    @DexIgnore
    public final void d(MotionEvent motionEvent) {
        setTranslationX((this.b + motionEvent.getRawX()) - this.d);
        setTranslationY((this.c + motionEvent.getRawY()) - this.e);
    }

    @DexIgnore
    public void e() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(0.0f);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(0.0f);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(0.0f);
        }
        View view4 = this.o;
        if (view4 != null) {
            view4.setAlpha(1.0f);
        }
    }

    @DexIgnore
    public void f() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(0.0f);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(0.0f);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(1.0f);
        }
        View view4 = this.o;
        if (view4 != null) {
            view4.setAlpha(0.0f);
        }
    }

    @DexIgnore
    public final void g() {
        float percentX = getPercentX();
        float percentY = getPercentY();
        List<fz5> list = this.a.l;
        if (list == fz5.HORIZONTAL) {
            a(percentX);
        } else if (list == fz5.VERTICAL) {
            b(percentY);
        } else if (list == fz5.FREEDOM_NO_BOTTOM) {
            if (Math.abs(percentX) >= Math.abs(percentY) || percentY >= 0.0f) {
                a(percentX);
                return;
            }
            f();
            setOverlayAlpha(Math.abs(percentY));
        } else if (list == fz5.FREEDOM) {
            if (Math.abs(percentX) > Math.abs(percentY)) {
                a(percentX);
            } else {
                b(percentY);
            }
        } else if (Math.abs(percentX) > Math.abs(percentY)) {
            if (percentX < 0.0f) {
                d();
            } else {
                e();
            }
            setOverlayAlpha(Math.abs(percentX));
        } else {
            if (percentY < 0.0f) {
                f();
            } else {
                c();
            }
            setOverlayAlpha(Math.abs(percentY));
        }
    }

    @DexIgnore
    public ViewGroup getContentContainer() {
        return this.h;
    }

    @DexIgnore
    public ViewGroup getOverlayContainer() {
        return this.i;
    }

    @DexIgnore
    public float getPercentX() {
        float translationX = ((getTranslationX() - this.b) * 2.0f) / ((float) getWidth());
        if (translationX > 1.0f) {
            translationX = 1.0f;
        }
        if (translationX < -1.0f) {
            return -1.0f;
        }
        return translationX;
    }

    @DexIgnore
    public float getPercentY() {
        float translationY = ((getTranslationY() - this.c) * 2.0f) / ((float) getHeight());
        if (translationY > 1.0f) {
            translationY = 1.0f;
        }
        if (translationY < -1.0f) {
            return -1.0f;
        }
        return translationY;
    }

    @DexIgnore
    public float getViewOriginX() {
        return this.b;
    }

    @DexIgnore
    public float getViewOriginY() {
        return this.c;
    }

    @DexIgnore
    public final void h() {
        setRotation(getPercentX() * 20.0f);
    }

    @DexIgnore
    public void onFinishInflate() {
        super.onFinishInflate();
        FrameLayout.inflate(getContext(), 2131558446, this);
        this.h = (ViewGroup) findViewById(2131361969);
        this.i = (ViewGroup) findViewById(2131361970);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.t.onTouchEvent(motionEvent);
        if (this.a.g && this.g) {
            int a2 = j9.a(motionEvent);
            if (a2 == 0) {
                a(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(true);
            } else if (a2 == 1) {
                c(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            } else if (a2 == 2) {
                b(motionEvent);
            } else if (a2 == 3) {
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
        return true;
    }

    @DexIgnore
    public void setCardStackOption(bz5 bz5) {
        this.a = bz5;
    }

    @DexIgnore
    public void setContainerEventListener(c cVar) {
        this.r = cVar;
        this.b = getTranslationX();
        this.c = getTranslationY();
    }

    @DexIgnore
    public void setDraggable(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public void setOverlayAlpha(AnimatorSet animatorSet) {
        if (animatorSet != null) {
            animatorSet.start();
        }
    }

    @DexIgnore
    public CardContainerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public void setOverlayAlpha(float f2) {
        this.i.setAlpha(f2);
    }

    @DexIgnore
    public CardContainerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = 0.0f;
        this.e = 0.0f;
        this.f = false;
        this.g = true;
        this.h = null;
        this.i = null;
        this.j = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = new a();
        this.t = new GestureDetector(getContext(), this.s);
    }

    @DexIgnore
    public final void a(float f2) {
        if (f2 < 0.0f) {
            d();
        } else {
            e();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    @DexIgnore
    public void d() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(1.0f);
        }
        View view2 = this.o;
        if (view2 != null) {
            view2.setAlpha(0.0f);
        }
        View view3 = this.p;
        if (view3 != null) {
            view3.setAlpha(0.0f);
        }
        View view4 = this.q;
        if (view4 != null) {
            view4.setAlpha(0.0f);
        }
    }

    @DexIgnore
    public final void a() {
        animate().translationX(this.b).translationY(this.c).setDuration(300).setInterpolator(new OvershootInterpolator(1.0f)).setListener((Animator.AnimatorListener) null).start();
    }

    @DexIgnore
    public final void b(float f2) {
        if (f2 < 0.0f) {
            f();
        } else {
            c();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    @DexIgnore
    public void b() {
        this.h.setAlpha(1.0f);
        this.i.setAlpha(0.0f);
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        View view = this.j;
        if (view != null) {
            this.i.removeView(view);
        }
        if (i2 != 0) {
            this.j = LayoutInflater.from(getContext()).inflate(i2, this.i, false);
            this.i.addView(this.j);
            this.j.setAlpha(0.0f);
        }
        View view2 = this.o;
        if (view2 != null) {
            this.i.removeView(view2);
        }
        if (i3 != 0) {
            this.o = LayoutInflater.from(getContext()).inflate(i3, this.i, false);
            this.i.addView(this.o);
            this.o.setAlpha(0.0f);
        }
        View view3 = this.p;
        if (view3 != null) {
            this.i.removeView(view3);
        }
        if (i4 != 0) {
            this.p = LayoutInflater.from(getContext()).inflate(i4, this.i, false);
            this.i.addView(this.p);
            this.p.setAlpha(0.0f);
        }
        View view4 = this.q;
        if (view4 != null) {
            this.i.removeView(view4);
        }
        if (i5 != 0) {
            this.q = LayoutInflater.from(getContext()).inflate(i5, this.i, false);
            this.i.addView(this.q);
            this.q.setAlpha(0.0f);
        }
    }

    @DexIgnore
    public void c() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(0.0f);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(1.0f);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(0.0f);
        }
        View view4 = this.o;
        if (view4 != null) {
            view4.setAlpha(0.0f);
        }
    }
}
