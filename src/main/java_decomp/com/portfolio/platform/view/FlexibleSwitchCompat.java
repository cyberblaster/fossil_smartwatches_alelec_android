package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.SwitchCompat;
import com.fossil.o7;
import com.fossil.wg6;
import com.fossil.x24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleSwitchCompat extends SwitchCompat {
    @DexIgnore
    public String T; // = "";
    @DexIgnore
    public String U; // = "";
    @DexIgnore
    public String V; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleSwitchCompat);
            String string = obtainStyledAttributes.getString(2);
            if (string == null) {
                string = "";
            }
            this.T = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "";
            }
            this.U = string2;
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 == null) {
                string3 = "";
            }
            this.V = string3;
            obtainStyledAttributes.recycle();
        }
        d();
        setElevation(0.0f);
    }

    @DexIgnore
    public final void d() {
        if (!TextUtils.isEmpty(this.T) && !TextUtils.isEmpty(this.U) && !TextUtils.isEmpty(this.V)) {
            String b = ThemeManager.l.a().b(this.T);
            String b2 = ThemeManager.l.a().b(this.U);
            if (!TextUtils.isEmpty(b) && !TextUtils.isEmpty(b2)) {
                o7.a(getTrackDrawable(), new ColorStateList(new int[][]{new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(b2), Color.parseColor(b)}));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }
}
