package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.fossil.wg6;
import com.fossil.x24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleProgressBar extends ProgressBar {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleProgressBar(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleProgressBar);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = "nonBrandSurface";
            }
            wg6.a((Object) string, "styledAttrs.getString(R.\u2026lor) ?: \"nonBrandSurface\"");
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "primaryText";
            }
            wg6.a((Object) string2, "styledAttrs.getString(R.\u2026s_color) ?: \"primaryText\"");
            this.a = ThemeManager.l.a().b(string);
            this.b = ThemeManager.l.a().b(string2);
            a();
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a() {
        String str = this.a;
        if (str != null) {
            setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(str)));
        }
        String str2 = this.b;
        if (str2 != null) {
            setProgressTintList(ColorStateList.valueOf(Color.parseColor(str2)));
        }
    }
}
