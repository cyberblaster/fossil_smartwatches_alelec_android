package com.portfolio.platform.view.blur;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import androidx.renderscript.RenderScript;
import com.fossil.ah;
import com.fossil.dh;
import com.fossil.qg6;
import com.fossil.tg;
import com.fossil.vg;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xj6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BlurView extends View {
    @DexIgnore
    public static int v;
    @DexIgnore
    public static /* final */ b w; // = new b();
    @DexIgnore
    public static Boolean x; // = null;
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Bitmap e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public Canvas g;
    @DexIgnore
    public RenderScript h;
    @DexIgnore
    public dh i;
    @DexIgnore
    public tg j;
    @DexIgnore
    public tg o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ Rect q; // = new Rect();
    @DexIgnore
    public /* final */ Rect r; // = new Rect();
    @DexIgnore
    public View s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener u; // = new c(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Boolean a() {
            return BlurView.x;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Boolean bool) {
            BlurView.x = bool;
        }

        @DexIgnore
        public final boolean a(Context context) {
            if (a() == null && context != null) {
                a(Boolean.valueOf((context.getApplicationInfo().flags & 2) != 0));
            }
            if (a() == Boolean.TRUE) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RuntimeException {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public /* final */ /* synthetic */ BlurView a;

        @DexIgnore
        public c(BlurView blurView) {
            this.a = blurView;
        }

        @DexIgnore
        public final boolean onPreDraw() {
            Canvas canvas;
            int[] iArr = new int[2];
            Bitmap b = this.a.f;
            View d = this.a.s;
            if (d != null && this.a.isShown() && this.a.a()) {
                boolean z = !wg6.a((Object) this.a.f, (Object) b);
                d.getLocationOnScreen(iArr);
                this.a.getLocationOnScreen(iArr);
                int i = (-iArr[0]) + iArr[0];
                int i2 = (-iArr[1]) + iArr[1];
                Bitmap a2 = this.a.e;
                if (a2 != null) {
                    a2.eraseColor(this.a.b & 16777215);
                    Canvas c = this.a.g;
                    if (c != null) {
                        int save = c.save();
                        this.a.p = true;
                        BlurView.v = BlurView.v + 1;
                        try {
                            Canvas c2 = this.a.g;
                            if (c2 != null) {
                                Bitmap a3 = this.a.e;
                                if (a3 != null) {
                                    float width = (((float) a3.getWidth()) * 1.0f) / ((float) this.a.getWidth());
                                    Bitmap a4 = this.a.e;
                                    if (a4 != null) {
                                        c2.scale(width, (1.0f * ((float) a4.getHeight())) / ((float) this.a.getHeight()));
                                        Canvas c3 = this.a.g;
                                        if (c3 != null) {
                                            c3.translate((float) (-i), (float) (-i2));
                                            if (d.getBackground() != null) {
                                                Drawable background = d.getBackground();
                                                Canvas c4 = this.a.g;
                                                if (c4 != null) {
                                                    background.draw(c4);
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            }
                                            d.draw(this.a.g);
                                            this.a.p = false;
                                            BlurView.v = BlurView.v - 1;
                                            canvas = this.a.g;
                                            if (canvas == null) {
                                                wg6.a();
                                                throw null;
                                            }
                                            canvas.restoreToCount(save);
                                            BlurView blurView = this.a;
                                            Bitmap a5 = blurView.e;
                                            if (a5 != null) {
                                                Bitmap b2 = this.a.f;
                                                if (b2 != null) {
                                                    blurView.a(a5, b2);
                                                    if (z || this.a.t) {
                                                        this.a.invalidate();
                                                    }
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } catch (b unused) {
                            this.a.p = false;
                            BlurView.v = BlurView.v - 1;
                            canvas = this.a.g;
                            if (canvas == null) {
                                wg6.a();
                                throw null;
                            }
                        } catch (Throwable th) {
                            this.a.p = false;
                            BlurView.v = BlurView.v - 1;
                            Canvas c5 = this.a.g;
                            if (c5 == null) {
                                wg6.a();
                                throw null;
                            }
                            c5.restoreToCount(save);
                            throw th;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            return true;
        }
    }

    /*
    static {
        try {
            ClassLoader classLoader = BlurView.class.getClassLoader();
            if (classLoader != null) {
                classLoader.loadClass("android.support.v8.renderscript.RenderScript");
                return;
            }
            wg6.a();
            throw null;
        } catch (ClassNotFoundException unused) {
            throw new RuntimeException("RenderScript support not enabled. Add \"android { defaultConfig { renderscriptSupportModeEnabled true }}\" in your build.gradle");
        }
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BlurView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.BlurView);
        Resources resources = context.getResources();
        wg6.a((Object) resources, "context.resources");
        this.c = obtainStyledAttributes.getDimension(2, TypedValue.applyDimension(1, 10.0f, resources.getDisplayMetrics()));
        this.a = obtainStyledAttributes.getFloat(0, 4.0f);
        this.b = obtainStyledAttributes.getColor(1, 0);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final View getActivityDecorView() {
        Context context = getContext();
        for (int i2 = 0; i2 < 4 && context != null && !(context instanceof Activity) && (context instanceof ContextWrapper); i2++) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (!(context instanceof Activity)) {
            return null;
        }
        Window window = ((Activity) context).getWindow();
        wg6.a((Object) window, "ctx.window");
        return window.getDecorView();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        wg6.b(canvas, "canvas");
        if (this.p) {
            throw w;
        } else if (v <= 0) {
            super.draw(canvas);
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.s = getActivityDecorView();
        View view = this.s;
        boolean z = false;
        if (view == null) {
            this.t = false;
        } else if (view != null) {
            view.getViewTreeObserver().addOnPreDrawListener(this.u);
            View view2 = this.s;
            if (view2 != null) {
                if (view2.getRootView() != getRootView()) {
                    z = true;
                }
                this.t = z;
                if (this.t) {
                    View view3 = this.s;
                    if (view3 != null) {
                        view3.postInvalidate();
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        View view = this.s;
        if (view != null) {
            if (view != null) {
                view.getViewTreeObserver().removeOnPreDrawListener(this.u);
            } else {
                wg6.a();
                throw null;
            }
        }
        b();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.onDraw(canvas);
        a(canvas, this.f, this.b);
    }

    @DexIgnore
    public final void b() {
        c();
        d();
    }

    @DexIgnore
    public final void c() {
        tg tgVar = this.j;
        if (tgVar != null) {
            if (tgVar != null) {
                tgVar.b();
                this.j = null;
            } else {
                wg6.a();
                throw null;
            }
        }
        tg tgVar2 = this.o;
        if (tgVar2 != null) {
            if (tgVar2 != null) {
                tgVar2.b();
                this.o = null;
            } else {
                wg6.a();
                throw null;
            }
        }
        Bitmap bitmap = this.e;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
                this.e = null;
            } else {
                wg6.a();
                throw null;
            }
        }
        Bitmap bitmap2 = this.f;
        if (bitmap2 == null) {
            return;
        }
        if (bitmap2 != null) {
            bitmap2.recycle();
            this.f = null;
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void d() {
        RenderScript renderScript = this.h;
        if (renderScript != null) {
            if (renderScript != null) {
                renderScript.a();
                this.h = null;
            } else {
                wg6.a();
                throw null;
            }
        }
        dh dhVar = this.i;
        if (dhVar == null) {
            return;
        }
        if (dhVar != null) {
            dhVar.b();
            this.i = null;
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        Bitmap bitmap;
        if (this.c == 0.0f) {
            b();
            return false;
        }
        float f2 = this.a;
        if (this.d || this.h == null) {
            if (this.h == null) {
                try {
                    this.h = RenderScript.a(getContext());
                    RenderScript renderScript = this.h;
                    if (renderScript != null) {
                        RenderScript renderScript2 = this.h;
                        if (renderScript2 != null) {
                            this.i = dh.a(renderScript, vg.h(renderScript2));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } catch (ah e2) {
                    if (y.a(getContext())) {
                        if (e2.getMessage() != null) {
                            String message = e2.getMessage();
                            if (message == null) {
                                wg6.a();
                                throw null;
                            } else if (xj6.c(message, "Error networkLoading RS jni library: java.lang.UnsatisfiedLinkError:", false, 2, (Object) null)) {
                                throw new RuntimeException("Error networkLoading RS jni library, Upgrade buildToolsVersion=\"24.0.2\" or higher may solve this issue");
                            }
                        }
                        throw e2;
                    }
                    d();
                    return false;
                }
            }
            this.d = false;
            float f3 = this.c / f2;
            if (f3 > 25.0f) {
                f2 = (f2 * f3) / 25.0f;
                f3 = 25.0f;
            }
            dh dhVar = this.i;
            if (dhVar != null) {
                dhVar.a(f3);
            } else {
                wg6.a();
                throw null;
            }
        }
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(1, (int) (((float) width) / f2));
        int max2 = Math.max(1, (int) (((float) height) / f2));
        if (!(this.g == null || (bitmap = this.f) == null)) {
            if (bitmap == null) {
                wg6.a();
                throw null;
            } else if (bitmap.getWidth() == max) {
                Bitmap bitmap2 = this.f;
                if (bitmap2 == null) {
                    wg6.a();
                    throw null;
                } else if (bitmap2.getHeight() == max2) {
                    return true;
                }
            }
        }
        c();
        try {
            this.e = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
            if (this.e == null) {
                c();
                return false;
            }
            Bitmap bitmap3 = this.e;
            if (bitmap3 != null) {
                this.g = new Canvas(bitmap3);
                RenderScript renderScript3 = this.h;
                if (renderScript3 != null) {
                    Bitmap bitmap4 = this.e;
                    if (bitmap4 != null) {
                        this.j = tg.a(renderScript3, bitmap4, tg.b.MIPMAP_NONE, 1);
                        RenderScript renderScript4 = this.h;
                        if (renderScript4 != null) {
                            tg tgVar = this.j;
                            if (tgVar != null) {
                                this.o = tg.a(renderScript4, tgVar.e());
                                this.f = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                                if (this.f != null) {
                                    return true;
                                }
                                c();
                                return false;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } catch (OutOfMemoryError unused) {
            c();
            return false;
        } catch (Throwable unused2) {
            c();
            return false;
        }
    }

    @DexIgnore
    public final void a(Bitmap bitmap, Bitmap bitmap2) {
        tg tgVar = this.j;
        if (tgVar != null) {
            tgVar.a(bitmap);
            dh dhVar = this.i;
            if (dhVar != null) {
                dhVar.c(this.j);
                dh dhVar2 = this.i;
                if (dhVar2 != null) {
                    dhVar2.b(this.o);
                    tg tgVar2 = this.o;
                    if (tgVar2 != null) {
                        tgVar2.b(bitmap2);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, Bitmap bitmap, int i2) {
        if (bitmap != null) {
            this.q.right = bitmap.getWidth();
            this.q.bottom = bitmap.getHeight();
            this.r.right = getWidth();
            this.r.bottom = getHeight();
            canvas.drawBitmap(bitmap, this.q, this.r, (Paint) null);
        }
        canvas.drawColor(i2);
    }
}
