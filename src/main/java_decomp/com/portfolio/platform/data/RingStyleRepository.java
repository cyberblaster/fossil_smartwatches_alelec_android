package com.portfolio.platform.data;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "RingStyleRepository";
    @DexIgnore
    public /* final */ FileRepository mFileRepository;
    @DexIgnore
    public /* final */ RingStyleDao mRingStyleDao;
    @DexIgnore
    public /* final */ RingStyleRemoteDataSource mRingStyleRemoveSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public RingStyleRepository(RingStyleDao ringStyleDao, RingStyleRemoteDataSource ringStyleRemoteDataSource, FileRepository fileRepository) {
        wg6.b(ringStyleDao, "mRingStyleDao");
        wg6.b(ringStyleRemoteDataSource, "mRingStyleRemoveSource");
        wg6.b(fileRepository, "mFileRepository");
        this.mRingStyleDao = ringStyleDao;
        this.mRingStyleRemoveSource = ringStyleRemoteDataSource;
        this.mFileRepository = fileRepository;
    }

    @DexIgnore
    private final void saveRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle) {
        this.mRingStyleDao.insertRingStyle(dianaComplicationRingStyle);
    }

    @DexIgnore
    public final void cleanUp() {
        this.mRingStyleDao.clearAllData();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01a6, code lost:
        r14 = r6.mFileRepository;
        r18 = r13.getData().getUrl();
        r4.L$0 = r6;
        r4.L$1 = r1;
        r4.Z$0 = r2;
        r4.L$2 = r3;
        r4.L$3 = r7;
        r4.L$4 = r8;
        r4.L$5 = r10;
        r4.L$6 = r5;
        r4.L$7 = r11;
        r4.L$8 = r13;
        r4.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01d5, code lost:
        if (com.portfolio.platform.data.source.FileRepository.downloadFromURL$default(r14, r18, (java.lang.String) null, r4, 2, (java.lang.Object) null) != r12) goto L_0x01d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01d7, code lost:
        return r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01d8, code lost:
        r14 = r1;
        r1 = r13;
        r13 = r2;
        r23 = r12;
        r12 = r3;
        r3 = r23;
        r24 = r10;
        r10 = r4;
        r4 = r11;
        r11 = r7;
        r7 = r8;
        r8 = r6;
        r6 = r24;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    public final Object downloadRingStyleList(String str, boolean z, xe6<? super cd6> xe6) {
        RingStyleRepository$downloadRingStyleList$Anon1 ringStyleRepository$downloadRingStyleList$Anon1;
        int i;
        Object obj;
        Iterator it;
        DianaComplicationRingStyle dianaComplicationRingStyle;
        ArrayList arrayList;
        String str2;
        RingStyleRepository ringStyleRepository;
        ArrayList arrayList2;
        ap4 ap4;
        DianaComplicationRingStyle dianaComplicationRingStyle2;
        boolean z2;
        ap4 ap42;
        RingStyleRepository ringStyleRepository2;
        DianaComplicationRingStyle dianaComplicationRingStyle3;
        ArrayList arrayList3;
        ArrayList arrayList4;
        Iterator it2;
        Object obj2;
        String str3;
        String str4;
        Object obj3;
        String str5 = str;
        boolean z3 = z;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof RingStyleRepository$downloadRingStyleList$Anon1) {
            ringStyleRepository$downloadRingStyleList$Anon1 = (RingStyleRepository$downloadRingStyleList$Anon1) xe62;
            int i2 = ringStyleRepository$downloadRingStyleList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                ringStyleRepository$downloadRingStyleList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj4 = ringStyleRepository$downloadRingStyleList$Anon1.result;
                Object a = ff6.a();
                i = ringStyleRepository$downloadRingStyleList$Anon1.label;
                boolean z4 = true;
                if (i != 0) {
                    nc6.a(obj4);
                    RingStyleRemoteDataSource ringStyleRemoteDataSource = this.mRingStyleRemoveSource;
                    ringStyleRepository$downloadRingStyleList$Anon1.L$0 = this;
                    ringStyleRepository$downloadRingStyleList$Anon1.L$1 = str5;
                    ringStyleRepository$downloadRingStyleList$Anon1.Z$0 = z3;
                    ringStyleRepository$downloadRingStyleList$Anon1.label = 1;
                    obj4 = ringStyleRemoteDataSource.getRingStyleList(str5, z3, ringStyleRepository$downloadRingStyleList$Anon1);
                    if (obj4 == a) {
                        return a;
                    }
                    ringStyleRepository = this;
                } else if (i == 1) {
                    boolean z5 = ringStyleRepository$downloadRingStyleList$Anon1.Z$0;
                    ringStyleRepository = (RingStyleRepository) ringStyleRepository$downloadRingStyleList$Anon1.L$0;
                    nc6.a(obj4);
                    z3 = z5;
                    str5 = (String) ringStyleRepository$downloadRingStyleList$Anon1.L$1;
                } else if (i == 2) {
                    dianaComplicationRingStyle2 = (DianaComplicationRingStyle) ringStyleRepository$downloadRingStyleList$Anon1.L$8;
                    ap42 = (ap4) ringStyleRepository$downloadRingStyleList$Anon1.L$2;
                    z2 = ringStyleRepository$downloadRingStyleList$Anon1.Z$0;
                    String str6 = (String) ringStyleRepository$downloadRingStyleList$Anon1.L$1;
                    nc6.a(obj4);
                    Object obj5 = a;
                    arrayList2 = (ArrayList) ringStyleRepository$downloadRingStyleList$Anon1.L$6;
                    DianaComplicationRingStyle dianaComplicationRingStyle4 = (DianaComplicationRingStyle) ringStyleRepository$downloadRingStyleList$Anon1.L$5;
                    ArrayList arrayList5 = (ArrayList) ringStyleRepository$downloadRingStyleList$Anon1.L$4;
                    String str7 = (String) ringStyleRepository$downloadRingStyleList$Anon1.L$3;
                    ringStyleRepository2 = (RingStyleRepository) ringStyleRepository$downloadRingStyleList$Anon1.L$0;
                    RingStyleRepository$downloadRingStyleList$Anon1 ringStyleRepository$downloadRingStyleList$Anon12 = ringStyleRepository$downloadRingStyleList$Anon1;
                    Iterator it3 = (Iterator) ringStyleRepository$downloadRingStyleList$Anon1.L$7;
                    FileRepository fileRepository = ringStyleRepository2.mFileRepository;
                    String previewUrl = dianaComplicationRingStyle2.getData().getPreviewUrl();
                    ringStyleRepository$downloadRingStyleList$Anon12.L$0 = ringStyleRepository2;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$1 = str6;
                    ringStyleRepository$downloadRingStyleList$Anon12.Z$0 = z2;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$2 = ap42;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$3 = str7;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$4 = arrayList5;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$5 = dianaComplicationRingStyle4;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$6 = arrayList2;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$7 = it3;
                    ringStyleRepository$downloadRingStyleList$Anon12.L$8 = dianaComplicationRingStyle2;
                    ringStyleRepository$downloadRingStyleList$Anon12.label = 3;
                    obj2 = obj5;
                    String str8 = previewUrl;
                    it2 = it3;
                    arrayList4 = arrayList2;
                    dianaComplicationRingStyle3 = dianaComplicationRingStyle4;
                    arrayList3 = arrayList5;
                    if (FileRepository.downloadFromURL$default(fileRepository, str8, (String) null, ringStyleRepository$downloadRingStyleList$Anon12, 2, (Object) null) == obj2) {
                        return obj2;
                    }
                    ringStyleRepository$downloadRingStyleList$Anon1 = ringStyleRepository$downloadRingStyleList$Anon12;
                    str4 = str6;
                    str3 = str7;
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadRingStyleList() - save data to local DB");
                    dianaComplicationRingStyle2.setSerial(str3);
                    wg6.a((Object) dianaComplicationRingStyle2, "item");
                    ringStyleRepository2.saveRingStyle(dianaComplicationRingStyle2);
                    str5 = str4;
                    ringStyleRepository = ringStyleRepository2;
                    ap4 = ap42;
                    z3 = z2;
                    str2 = str3;
                    obj = obj2;
                    it = it2;
                    arrayList2 = arrayList4;
                    arrayList = arrayList3;
                    dianaComplicationRingStyle = dianaComplicationRingStyle3;
                    while (true) {
                        if (it.hasNext()) {
                            break;
                            break;
                        }
                        break;
                    }
                    return cd6.a;
                    return obj2;
                } else if (i == 3) {
                    dianaComplicationRingStyle2 = (DianaComplicationRingStyle) ringStyleRepository$downloadRingStyleList$Anon1.L$8;
                    str3 = (String) ringStyleRepository$downloadRingStyleList$Anon1.L$3;
                    boolean z6 = ringStyleRepository$downloadRingStyleList$Anon1.Z$0;
                    str4 = (String) ringStyleRepository$downloadRingStyleList$Anon1.L$1;
                    ringStyleRepository2 = (RingStyleRepository) ringStyleRepository$downloadRingStyleList$Anon1.L$0;
                    nc6.a(obj4);
                    it2 = (Iterator) ringStyleRepository$downloadRingStyleList$Anon1.L$7;
                    arrayList4 = (ArrayList) ringStyleRepository$downloadRingStyleList$Anon1.L$6;
                    dianaComplicationRingStyle3 = (DianaComplicationRingStyle) ringStyleRepository$downloadRingStyleList$Anon1.L$5;
                    arrayList3 = (ArrayList) ringStyleRepository$downloadRingStyleList$Anon1.L$4;
                    ap42 = (ap4) ringStyleRepository$downloadRingStyleList$Anon1.L$2;
                    obj2 = a;
                    z2 = z6;
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadRingStyleList() - save data to local DB");
                    dianaComplicationRingStyle2.setSerial(str3);
                    wg6.a((Object) dianaComplicationRingStyle2, "item");
                    ringStyleRepository2.saveRingStyle(dianaComplicationRingStyle2);
                    str5 = str4;
                    ringStyleRepository = ringStyleRepository2;
                    ap4 = ap42;
                    z3 = z2;
                    str2 = str3;
                    obj = obj2;
                    it = it2;
                    arrayList2 = arrayList4;
                    arrayList = arrayList3;
                    dianaComplicationRingStyle = dianaComplicationRingStyle3;
                    while (true) {
                        if (it.hasNext()) {
                            DianaComplicationRingStyle dianaComplicationRingStyle5 = (DianaComplicationRingStyle) it.next();
                            String previewUrl2 = dianaComplicationRingStyle5.getData().getPreviewUrl();
                            if (!(previewUrl2 == null || xj6.a(previewUrl2))) {
                                String url = dianaComplicationRingStyle5.getData().getUrl();
                                if (!(url == null || xj6.a(url))) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj4;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadRingStyleList() success");
                    str2 = PortfolioApp.get.instance().e();
                    if (xj6.a(str2)) {
                        return cd6.a;
                    }
                    arrayList = (ArrayList) ((cp4) ap4).a();
                    dianaComplicationRingStyle = ringStyleRepository.mRingStyleDao.getRingStylesBySerial(str2);
                    if (dianaComplicationRingStyle != null) {
                        if (arrayList != null && (!arrayList.isEmpty())) {
                            Iterator it4 = arrayList.iterator();
                            while (true) {
                                if (!it4.hasNext()) {
                                    obj3 = null;
                                    break;
                                }
                                obj3 = it4.next();
                                DianaComplicationRingStyle dianaComplicationRingStyle6 = (DianaComplicationRingStyle) obj3;
                                if (hf6.a(wg6.a((Object) dianaComplicationRingStyle6.getCategory(), (Object) dianaComplicationRingStyle.getCategory()) && wg6.a((Object) dianaComplicationRingStyle6.getData(), (Object) dianaComplicationRingStyle.getData()) && wg6.a((Object) dianaComplicationRingStyle6.getMetaData(), (Object) dianaComplicationRingStyle.getMetaData())).booleanValue()) {
                                    break;
                                }
                            }
                            if (((DianaComplicationRingStyle) obj3) == null) {
                                z4 = true;
                            }
                        }
                        z4 = false;
                    }
                    if (!z4) {
                        return cd6.a;
                    }
                    if (arrayList != null) {
                        it = arrayList.iterator();
                        obj = a;
                        arrayList2 = arrayList;
                        while (true) {
                            if (it.hasNext()) {
                            }
                            break;
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e(TAG, "downloadRingStyleList() Failed, serverError = " + ((zo4) ap4).c());
                }
                return cd6.a;
            }
        }
        ringStyleRepository$downloadRingStyleList$Anon1 = new RingStyleRepository$downloadRingStyleList$Anon1(this, xe62);
        Object obj42 = ringStyleRepository$downloadRingStyleList$Anon1.result;
        Object a2 = ff6.a();
        i = ringStyleRepository$downloadRingStyleList$Anon1.label;
        boolean z42 = true;
        if (i != 0) {
        }
        ap4 = (ap4) obj42;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final List<DianaComplicationRingStyle> getRingStyles() {
        return this.mRingStyleDao.getRingStyles();
    }

    @DexIgnore
    public final DianaComplicationRingStyle getRingStylesBySerial(String str) {
        wg6.b(str, "serialNumber");
        return this.mRingStyleDao.getRingStylesBySerial(str);
    }
}
