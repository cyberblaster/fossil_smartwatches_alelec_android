package com.portfolio.platform.data;

import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Access {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ String l;
    @DexIgnore
    public /* final */ String m;
    @DexIgnore
    public /* final */ String n;
    @DexIgnore
    public /* final */ String o;

    @DexIgnore
    public Access(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14) {
        wg6.b(str, "a");
        wg6.b(str2, "b");
        wg6.b(str3, "c");
        wg6.b(str4, "d");
        wg6.b(str5, "e");
        wg6.b(str6, "f");
        wg6.b(str7, "g");
        wg6.b(str8, "h");
        wg6.b(str9, "i");
        wg6.b(str10, "k");
        wg6.b(str11, "l");
        wg6.b(str12, "m");
        wg6.b(str13, "n");
        wg6.b(str14, "o");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
        this.h = str8;
        this.i = str9;
        this.k = str10;
        this.l = str11;
        this.m = str12;
        this.n = str13;
        this.o = str14;
    }

    @DexIgnore
    public static /* synthetic */ Access copy$default(Access access, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, int i2, Object obj) {
        Access access2 = access;
        int i3 = i2;
        return access.copy((i3 & 1) != 0 ? access2.a : str, (i3 & 2) != 0 ? access2.b : str2, (i3 & 4) != 0 ? access2.c : str3, (i3 & 8) != 0 ? access2.d : str4, (i3 & 16) != 0 ? access2.e : str5, (i3 & 32) != 0 ? access2.f : str6, (i3 & 64) != 0 ? access2.g : str7, (i3 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? access2.h : str8, (i3 & 256) != 0 ? access2.i : str9, (i3 & 512) != 0 ? access2.k : str10, (i3 & 1024) != 0 ? access2.l : str11, (i3 & 2048) != 0 ? access2.m : str12, (i3 & 4096) != 0 ? access2.n : str13, (i3 & 8192) != 0 ? access2.o : str14);
    }

    @DexIgnore
    public final String component1() {
        return this.a;
    }

    @DexIgnore
    public final String component10() {
        return this.k;
    }

    @DexIgnore
    public final String component11() {
        return this.l;
    }

    @DexIgnore
    public final String component12() {
        return this.m;
    }

    @DexIgnore
    public final String component13() {
        return this.n;
    }

    @DexIgnore
    public final String component14() {
        return this.o;
    }

    @DexIgnore
    public final String component2() {
        return this.b;
    }

    @DexIgnore
    public final String component3() {
        return this.c;
    }

    @DexIgnore
    public final String component4() {
        return this.d;
    }

    @DexIgnore
    public final String component5() {
        return this.e;
    }

    @DexIgnore
    public final String component6() {
        return this.f;
    }

    @DexIgnore
    public final String component7() {
        return this.g;
    }

    @DexIgnore
    public final String component8() {
        return this.h;
    }

    @DexIgnore
    public final String component9() {
        return this.i;
    }

    @DexIgnore
    public final Access copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14) {
        String str15 = str;
        wg6.b(str15, "a");
        String str16 = str2;
        wg6.b(str16, "b");
        String str17 = str3;
        wg6.b(str17, "c");
        String str18 = str4;
        wg6.b(str18, "d");
        String str19 = str5;
        wg6.b(str19, "e");
        String str20 = str6;
        wg6.b(str20, "f");
        String str21 = str7;
        wg6.b(str21, "g");
        String str22 = str8;
        wg6.b(str22, "h");
        String str23 = str9;
        wg6.b(str23, "i");
        String str24 = str10;
        wg6.b(str24, "k");
        String str25 = str11;
        wg6.b(str25, "l");
        String str26 = str12;
        wg6.b(str26, "m");
        String str27 = str13;
        wg6.b(str27, "n");
        String str28 = str14;
        wg6.b(str28, "o");
        return new Access(str15, str16, str17, str18, str19, str20, str21, str22, str23, str24, str25, str26, str27, str28);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Access)) {
            return false;
        }
        Access access = (Access) obj;
        return wg6.a((Object) this.a, (Object) access.a) && wg6.a((Object) this.b, (Object) access.b) && wg6.a((Object) this.c, (Object) access.c) && wg6.a((Object) this.d, (Object) access.d) && wg6.a((Object) this.e, (Object) access.e) && wg6.a((Object) this.f, (Object) access.f) && wg6.a((Object) this.g, (Object) access.g) && wg6.a((Object) this.h, (Object) access.h) && wg6.a((Object) this.i, (Object) access.i) && wg6.a((Object) this.k, (Object) access.k) && wg6.a((Object) this.l, (Object) access.l) && wg6.a((Object) this.m, (Object) access.m) && wg6.a((Object) this.n, (Object) access.n) && wg6.a((Object) this.o, (Object) access.o);
    }

    @DexIgnore
    public final String getA() {
        return this.a;
    }

    @DexIgnore
    public final String getB() {
        return this.b;
    }

    @DexIgnore
    public final String getC() {
        return this.c;
    }

    @DexIgnore
    public final String getD() {
        return this.d;
    }

    @DexIgnore
    public final String getE() {
        return this.e;
    }

    @DexIgnore
    public final String getF() {
        return this.f;
    }

    @DexIgnore
    public final String getG() {
        return this.g;
    }

    @DexIgnore
    public final String getH() {
        return this.h;
    }

    @DexIgnore
    public final String getI() {
        return this.i;
    }

    @DexIgnore
    public final String getK() {
        return this.k;
    }

    @DexIgnore
    public final String getL() {
        return this.l;
    }

    @DexIgnore
    public final String getM() {
        return this.m;
    }

    @DexIgnore
    public final String getN() {
        return this.n;
    }

    @DexIgnore
    public final String getO() {
        return this.o;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.f;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.g;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.h;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.i;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.k;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.l;
        int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.m;
        int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.n;
        int hashCode13 = (hashCode12 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.o;
        if (str14 != null) {
            i2 = str14.hashCode();
        }
        return hashCode13 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Access(a=" + this.a + ", b=" + this.b + ", c=" + this.c + ", d=" + this.d + ", e=" + this.e + ", f=" + this.f + ", g=" + this.g + ", h=" + this.h + ", i=" + this.i + ", k=" + this.k + ", l=" + this.l + ", m=" + this.m + ", n=" + this.n + ", o=" + this.o + ")";
    }
}
