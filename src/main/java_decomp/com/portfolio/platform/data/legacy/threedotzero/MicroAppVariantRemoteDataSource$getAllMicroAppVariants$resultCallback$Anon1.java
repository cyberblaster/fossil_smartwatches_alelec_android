package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.dx6;
import com.fossil.fp4;
import com.fossil.ku3;
import com.fossil.rc6;
import com.fossil.rx6;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.model.Range;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1 implements dx6<ku3> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $microAppVariants;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRemoteDataSource this$0;

    @DexIgnore
    public MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(MicroAppVariantRemoteDataSource microAppVariantRemoteDataSource, String str, ArrayList arrayList, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRemoteDataSource;
        this.$serialNumber = str;
        this.$microAppVariants = arrayList;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    public void onFailure(Call<ku3> call, Throwable th) {
        wg6.b(call, "call");
        wg6.b(th, "throwable");
        String tag = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure");
        if (!this.$microAppVariants.isEmpty()) {
            String tag2 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants not null");
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
            if (getVariantListCallback != null) {
                ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback).onSuccess(this.$microAppVariants);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        String tag3 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants is null");
        if (th instanceof SocketTimeoutException) {
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
            if (getVariantListCallback2 != null) {
                getVariantListCallback2.onFail(MFNetworkReturnCode.CLIENT_TIMEOUT);
                return;
            }
            return;
        }
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
        if (getVariantListCallback3 != null) {
            getVariantListCallback3.onFail(601);
        }
    }

    @DexIgnore
    public void onResponse(Call<ku3> call, rx6<ku3> rx6) {
        wg6.b(call, "call");
        wg6.b(rx6, "response");
        if (rx6.d()) {
            ku3 ku3 = (ku3) rx6.a();
            String tag = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag, "getMicroAppGallery deviceSerial=" + this.$serialNumber + " onSuccess response=" + ku3);
            fp4 fp4 = new fp4();
            fp4.a(ku3);
            this.$microAppVariants.addAll(fp4.a());
            Range b = fp4.b();
            if (b == null || !b.isHasNext()) {
                String tag2 = MicroAppVariantRemoteDataSource.Companion.getTAG();
                MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=false");
                if (this.$microAppVariants.isEmpty()) {
                    MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
                    if (getVariantListCallback != null) {
                        getVariantListCallback.onFail(rx6.b());
                        return;
                    }
                    return;
                }
                MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
                if (getVariantListCallback2 != null) {
                    ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback2).onSuccess(this.$microAppVariants);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
            }
            String tag3 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=true");
            this.this$0.getAllMicroAppVariants$app_fossilRelease(this.$serialNumber, this.$major, this.$minor, b.getOffset() + b.getLimit(), b.getLimit(), this);
            return;
        }
        String tag4 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag4, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful");
        if (!this.$microAppVariants.isEmpty()) {
            String tag5 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag5, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants not null");
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
            if (getVariantListCallback3 != null) {
                ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback3).onSuccess(this.$microAppVariants);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        String tag6 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag6, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants is null");
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback4 = this.$callback;
        if (getVariantListCallback4 != null) {
            getVariantListCallback4.onFail(MFNetworkReturnCode.NOT_FOUND);
        }
    }
}
