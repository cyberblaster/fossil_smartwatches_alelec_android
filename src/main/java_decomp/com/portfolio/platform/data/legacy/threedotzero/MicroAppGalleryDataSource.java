package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MicroAppGalleryDataSource {

    @DexIgnore
    public interface GetMicroAppCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(MicroApp microApp);
    }

    @DexIgnore
    public interface GetMicroAppGalleryCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<? extends MicroApp> list);
    }

    @DexIgnore
    public void deleteListMicroApp(String str) {
        wg6.b(str, "serial");
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, GetMicroAppCallback getMicroAppCallback) {
        wg6.b(str, "deviceSerial");
        wg6.b(str2, "microAppId");
    }

    @DexIgnore
    public abstract void getMicroAppGallery(String str, GetMicroAppGalleryCallback getMicroAppGalleryCallback);

    @DexIgnore
    public void updateListMicroApp(List<? extends MicroApp> list) {
    }

    @DexIgnore
    public void updateMicroApp(MicroApp microApp, GetMicroAppCallback getMicroAppCallback) {
        wg6.b(microApp, "microApp");
    }
}
