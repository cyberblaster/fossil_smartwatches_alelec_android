package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory implements Factory<ActivitySummaryDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ActivitySummaryDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideActivitySummaryDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static ActivitySummaryDao proxyProvideActivitySummaryDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        ActivitySummaryDao provideActivitySummaryDao = portfolioDatabaseModule.provideActivitySummaryDao(fitnessDatabase);
        z76.a(provideActivitySummaryDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideActivitySummaryDao;
    }

    @DexIgnore
    public ActivitySummaryDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
