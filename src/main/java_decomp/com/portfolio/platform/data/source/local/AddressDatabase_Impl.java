package com.portfolio.platform.data.source.local;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressDatabase_Impl extends AddressDatabase {
    @DexIgnore
    public volatile AddressDao _addressDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `addressOfWeather` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `lat` REAL NOT NULL, `lng` REAL NOT NULL, `address` TEXT NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'a0a1d2ac0b494c20d42ebbc7bcd2b1a6')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `addressOfWeather`");
            if (AddressDatabase_Impl.this.mCallbacks != null) {
                int size = AddressDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) AddressDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (AddressDatabase_Impl.this.mCallbacks != null) {
                int size = AddressDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) AddressDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = AddressDatabase_Impl.this.mDatabase = iiVar;
            AddressDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (AddressDatabase_Impl.this.mCallbacks != null) {
                int size = AddressDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) AddressDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(4);
            hashMap.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap.put(Constants.LAT, new fi.a(Constants.LAT, "REAL", true, 0, (String) null, 1));
            hashMap.put("lng", new fi.a("lng", "REAL", true, 0, (String) null, 1));
            hashMap.put("address", new fi.a("address", "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("addressOfWeather", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "addressOfWeather");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "addressOfWeather(com.portfolio.platform.data.model.microapp.weather.AddressOfWeather).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public AddressDao addressDao() {
        AddressDao addressDao;
        if (this._addressDao != null) {
            return this._addressDao;
        }
        synchronized (this) {
            if (this._addressDao == null) {
                this._addressDao = new AddressDao_Impl(this);
            }
            addressDao = this._addressDao;
        }
        return addressDao;
    }

    @DexIgnore
    public void clearAllTables() {
        AddressDatabase_Impl.super.assertNotMainThread();
        ii a = AddressDatabase_Impl.super.getOpenHelper().a();
        try {
            AddressDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `addressOfWeather`");
            AddressDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            AddressDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"addressOfWeather"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "a0a1d2ac0b494c20d42ebbc7bcd2b1a6", "22c420623f1ce32408e58d9756808f61");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }
}
