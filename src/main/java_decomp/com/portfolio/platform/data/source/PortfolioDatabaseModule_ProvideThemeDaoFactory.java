package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideThemeDaoFactory implements Factory<ThemeDao> {
    @DexIgnore
    public /* final */ Provider<ThemeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideThemeDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ThemeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideThemeDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ThemeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideThemeDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ThemeDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ThemeDatabase> provider) {
        return proxyProvideThemeDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static ThemeDao proxyProvideThemeDao(PortfolioDatabaseModule portfolioDatabaseModule, ThemeDatabase themeDatabase) {
        ThemeDao provideThemeDao = portfolioDatabaseModule.provideThemeDao(themeDatabase);
        z76.a(provideThemeDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideThemeDao;
    }

    @DexIgnore
    public ThemeDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
