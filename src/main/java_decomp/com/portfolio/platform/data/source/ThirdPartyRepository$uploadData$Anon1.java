package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rl6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1", f = "ThirdPartyRepository.kt", l = {153, 154, 155, 156, 157, 158, 159}, m = "invokeSuspend")
public final class ThirdPartyRepository$uploadData$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$1", f = "ThirdPartyRepository.kt", l = {115}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSampleList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitSampleList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$gFitSampleList, this.$activeDeviceSerial, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List list = this.$gFitSampleList;
                String str = this.$activeDeviceSerial;
                this.L$0 = il6;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSampleToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$2", f = "ThirdPartyRepository.kt", l = {122}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends sf6 implements ig6<il6, xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitActiveTimeList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitActiveTimeList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$gFitActiveTimeList, this.$activeDeviceSerial, xe6);
            anon2_Level2.p$ = (il6) obj;
            return anon2_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List list = this.$gFitActiveTimeList;
                String str = this.$activeDeviceSerial;
                this.L$0 = il6;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitActiveTimeToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$3", f = "ThirdPartyRepository.kt", l = {129}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends sf6 implements ig6<il6, xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitHeartRateList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitHeartRateList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, this.$gFitHeartRateList, this.$activeDeviceSerial, xe6);
            anon3_Level2.p$ = (il6) obj;
            return anon3_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List list = this.$gFitHeartRateList;
                String str = this.$activeDeviceSerial;
                this.L$0 = il6;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitHeartRateToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$4", f = "ThirdPartyRepository.kt", l = {136}, m = "invokeSuspend")
    public static final class Anon4_Level2 extends sf6 implements ig6<il6, xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSleepList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4_Level2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitSleepList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon4_Level2 anon4_Level2 = new Anon4_Level2(this.this$0, this.$gFitSleepList, this.$activeDeviceSerial, xe6);
            anon4_Level2.p$ = (il6) obj;
            return anon4_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon4_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List list = this.$gFitSleepList;
                String str = this.$activeDeviceSerial;
                this.L$0 = il6;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSleepDataToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$1$5", f = "ThirdPartyRepository.kt", l = {145}, m = "invokeSuspend")
    public static final class Anon5_Level2 extends sf6 implements ig6<il6, xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitWorkoutSessionList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon5_Level2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitWorkoutSessionList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon5_Level2 anon5_Level2 = new Anon5_Level2(this.this$0, this.$gFitWorkoutSessionList, this.$activeDeviceSerial, xe6);
            anon5_Level2.p$ = (il6) obj;
            return anon5_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon5_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List list = this.$gFitWorkoutSessionList;
                String str = this.$activeDeviceSerial;
                this.L$0 = il6;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$uploadData$Anon1(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, xe6 xe6) {
        super(2, xe6);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1 = new ThirdPartyRepository$uploadData$Anon1(this.this$0, this.$pushPendingThirdPartyDataCallback, xe6);
        thirdPartyRepository$uploadData$Anon1.p$ = (il6) obj;
        return thirdPartyRepository$uploadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ThirdPartyRepository$uploadData$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x02a6, code lost:
        r18 = r11;
        r11 = r2;
        r2 = r6;
        r6 = r5;
        r5 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x02b2, code lost:
        if (r8 == null) goto L_0x02d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x02b4, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x02cd, code lost:
        if (r8.a(r0) != r1) goto L_0x02d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02cf, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x02d0, code lost:
        if (r7 == null) goto L_0x02ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x02d2, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x02eb, code lost:
        if (r7.a(r0) != r1) goto L_0x02ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x02ed, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x02ee, code lost:
        if (r6 == null) goto L_0x030c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x02f0, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0309, code lost:
        if (r6.a(r0) != r1) goto L_0x030c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x030b, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x030c, code lost:
        if (r5 == null) goto L_0x032a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x030e, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0327, code lost:
        if (r5.a(r0) != r1) goto L_0x032a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0329, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x032a, code lost:
        if (r4 == null) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x032c, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0345, code lost:
        if (r4.a(r0) != r1) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0347, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0348, code lost:
        if (r2 == null) goto L_0x0366;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x034a, code lost:
        r0.L$0 = r11;
        r0.L$1 = r10;
        r0.L$2 = r9;
        r0.L$3 = r8;
        r0.L$4 = r7;
        r0.L$5 = r6;
        r0.L$6 = r5;
        r0.L$7 = r4;
        r0.L$8 = r2;
        r0.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0363, code lost:
        if (r2.a(r0) != r1) goto L_0x0366;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0365, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0366, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End uploadData");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0371, code lost:
        r1 = r0.$pushPendingThirdPartyDataCallback;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0373, code lost:
        if (r1 == null) goto L_0x0378;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0375, code lost:
        r1.onComplete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x037a, code lost:
        return com.fossil.cd6.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        String str;
        rl6 rl6;
        rl6 rl62;
        rl6 rl63;
        rl6 rl64;
        rl6 rl65;
        rl6 rl66;
        rl6 rl67;
        rl6 rl68;
        rl6 rl69;
        il6 il62;
        rl6 rl610;
        rl6 rl611;
        rl6 rl612;
        rl6 rl613;
        Object a = ff6.a();
        rl6 rl614 = null;
        switch (this.label) {
            case 0:
                nc6.a(obj);
                il62 = this.p$;
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Start uploadData");
                str = this.this$0.getMPortfolioApp().e();
                if (str.length() > 0) {
                    if (this.this$0.mGoogleFitHelper.e()) {
                        List<GFitSample> allGFitSample = this.this$0.getMThirdPartyDatabase().getGFitSampleDao().getAllGFitSample();
                        if (!allGFitSample.isEmpty()) {
                            rl610 = ik6.a(il62, (af6) null, (ll6) null, new Anon1_Level2(this, allGFitSample, str, (xe6) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitSample is empty");
                            rl610 = null;
                        }
                        List<GFitActiveTime> allGFitActiveTime = this.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().getAllGFitActiveTime();
                        if (!allGFitActiveTime.isEmpty()) {
                            rl611 = ik6.a(il62, (af6) null, (ll6) null, new Anon2_Level2(this, allGFitActiveTime, str, (xe6) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitActiveTime is empty");
                            rl611 = null;
                        }
                        List<GFitHeartRate> allGFitHeartRate = this.this$0.getMThirdPartyDatabase().getGFitHeartRateDao().getAllGFitHeartRate();
                        if (!allGFitHeartRate.isEmpty()) {
                            rl612 = ik6.a(il62, (af6) null, (ll6) null, new Anon3_Level2(this, allGFitHeartRate, str, (xe6) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitHeartRate is empty");
                            rl612 = null;
                        }
                        List<GFitSleep> allGFitSleep = this.this$0.getMThirdPartyDatabase().getGFitSleepDao().getAllGFitSleep();
                        if (!allGFitSleep.isEmpty()) {
                            rl613 = ik6.a(il62, (af6) null, (ll6) null, new Anon4_Level2(this, allGFitSleep, str, (xe6) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitSleep is empty");
                            rl613 = null;
                        }
                        List<GFitWorkoutSession> allGFitWorkoutSession = this.this$0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().getAllGFitWorkoutSession();
                        if (!allGFitWorkoutSession.isEmpty()) {
                            rl67 = ik6.a(il62, this.this$0.mExceptionHandling, (ll6) null, new Anon5_Level2(this, allGFitWorkoutSession, str, (xe6) null), 2, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitWorkoutSession is empty");
                            rl67 = null;
                        }
                        rl6 rl615 = rl613;
                        rl6 = rl610;
                        rl69 = rl615;
                        rl6 rl616 = rl612;
                        rl62 = rl611;
                        rl63 = rl616;
                    } else {
                        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Google Fit is not connected");
                        rl69 = null;
                        rl63 = null;
                        rl62 = null;
                        rl6 = null;
                        rl67 = null;
                    }
                    if (rl6 == null) {
                        rl64 = rl69;
                        rl65 = rl67;
                        il6 = il62;
                        rl66 = null;
                        break;
                    } else {
                        this.L$0 = il62;
                        this.L$1 = str;
                        this.L$2 = rl6;
                        this.L$3 = rl62;
                        this.L$4 = rl63;
                        this.L$5 = rl69;
                        this.L$6 = rl67;
                        this.L$7 = null;
                        this.L$8 = null;
                        this.label = 1;
                        if (rl6.a(this) != a) {
                            rl68 = null;
                            break;
                        } else {
                            return a;
                        }
                    }
                }
                break;
            case 1:
                rl614 = (rl6) this.L$7;
                rl63 = (rl6) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                str = (String) this.L$1;
                nc6.a(obj);
                rl6 rl617 = (rl6) this.L$5;
                rl68 = (rl6) this.L$8;
                il62 = (il6) this.L$0;
                rl67 = (rl6) this.L$6;
                rl69 = rl617;
                break;
            case 2:
                rl66 = (rl6) this.L$8;
                rl614 = (rl6) this.L$7;
                rl65 = (rl6) this.L$6;
                rl64 = (rl6) this.L$5;
                rl63 = (rl6) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                str = (String) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 3:
                rl66 = (rl6) this.L$8;
                rl614 = (rl6) this.L$7;
                rl65 = (rl6) this.L$6;
                rl64 = (rl6) this.L$5;
                rl63 = (rl6) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                str = (String) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 4:
                rl66 = (rl6) this.L$8;
                rl614 = (rl6) this.L$7;
                rl65 = (rl6) this.L$6;
                rl64 = (rl6) this.L$5;
                rl63 = (rl6) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                str = (String) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 5:
                rl66 = (rl6) this.L$8;
                rl614 = (rl6) this.L$7;
                rl65 = (rl6) this.L$6;
                rl64 = (rl6) this.L$5;
                rl63 = (rl6) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                str = (String) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 6:
                rl66 = (rl6) this.L$8;
                rl614 = (rl6) this.L$7;
                rl65 = (rl6) this.L$6;
                rl64 = (rl6) this.L$5;
                rl63 = (rl6) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                str = (String) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 7:
                rl6 rl618 = (rl6) this.L$8;
                rl6 rl619 = (rl6) this.L$7;
                rl6 rl620 = (rl6) this.L$6;
                rl6 rl621 = (rl6) this.L$5;
                rl6 rl622 = (rl6) this.L$4;
                rl6 rl623 = (rl6) this.L$3;
                rl6 rl624 = (rl6) this.L$2;
                String str2 = (String) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
