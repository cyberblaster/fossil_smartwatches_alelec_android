package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.n44;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.fossil.x34;
import com.fossil.xe;
import com.fossil.yh;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateDailySummaryDao_Impl extends HeartRateDailySummaryDao {
    @DexIgnore
    public /* final */ x34 __dateShortStringConverter; // = new x34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<DailyHeartRateSummary> __insertionAdapterOfDailyHeartRateSummary;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllHeartRateSummaries;
    @DexIgnore
    public /* final */ n44 __restingConverter; // = new n44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<DailyHeartRateSummary> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `daily_heart_rate_summary` (`average`,`date`,`createdAt`,`updatedAt`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, DailyHeartRateSummary dailyHeartRateSummary) {
            miVar.a(1, (double) dailyHeartRateSummary.getAverage());
            String a = HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(dailyHeartRateSummary.getDate());
            if (a == null) {
                miVar.a(2);
            } else {
                miVar.a(2, a);
            }
            miVar.a(3, dailyHeartRateSummary.getCreatedAt());
            miVar.a(4, dailyHeartRateSummary.getUpdatedAt());
            miVar.a(5, (long) dailyHeartRateSummary.getMin());
            miVar.a(6, (long) dailyHeartRateSummary.getMax());
            miVar.a(7, (long) dailyHeartRateSummary.getMinuteCount());
            String a2 = HeartRateDailySummaryDao_Impl.this.__restingConverter.a(dailyHeartRateSummary.getResting());
            if (a2 == null) {
                miVar.a(8);
            } else {
                miVar.a(8, a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM daily_heart_rate_summary";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DailyHeartRateSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DailyHeartRateSummary> call() throws Exception {
            Cursor a = bi.a(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "average");
                int b2 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(a, "createdAt");
                int b4 = ai.b(a, "updatedAt");
                int b5 = ai.b(a, "min");
                int b6 = ai.b(a, "max");
                int b7 = ai.b(a, "minuteCount");
                int b8 = ai.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(a.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getLong(b3), a.getLong(b4), a.getInt(b5), a.getInt(b6), a.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(a.getString(b8))));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends xe.b<Integer, DailyHeartRateSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends yh<DailyHeartRateSummary> {
            @DexIgnore
            public Anon1_Level2(oh ohVar, rh rhVar, boolean z, String... strArr) {
                super(ohVar, rhVar, z, strArr);
            }

            @DexIgnore
            public List<DailyHeartRateSummary> convertRows(Cursor cursor) {
                Cursor cursor2 = cursor;
                int b = ai.b(cursor2, "average");
                int b2 = ai.b(cursor2, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(cursor2, "createdAt");
                int b4 = ai.b(cursor2, "updatedAt");
                int b5 = ai.b(cursor2, "min");
                int b6 = ai.b(cursor2, "max");
                int b7 = ai.b(cursor2, "minuteCount");
                int b8 = ai.b(cursor2, "resting");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(cursor2.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(cursor2.getString(b2)), cursor2.getLong(b3), cursor2.getLong(b4), cursor2.getInt(b5), cursor2.getInt(b6), cursor2.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(cursor2.getString(b8))));
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public yh<DailyHeartRateSummary> create() {
            return new Anon1_Level2(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, "daily_heart_rate_summary");
        }
    }

    @DexIgnore
    public HeartRateDailySummaryDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfDailyHeartRateSummary = new Anon1(ohVar);
        this.__preparedStmtOfDeleteAllHeartRateSummaries = new Anon2(ohVar);
    }

    @DexIgnore
    public void deleteAllHeartRateSummaries() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllHeartRateSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSummaries.release(acquire);
        }
    }

    @DexIgnore
    public List<DailyHeartRateSummary> getDailyHeartRateSummariesDesc(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "average");
            int b3 = ai.b(a3, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a3, "createdAt");
            int b5 = ai.b(a3, "updatedAt");
            int b6 = ai.b(a3, "min");
            int b7 = ai.b(a3, "max");
            int b8 = ai.b(a3, "minuteCount");
            int b9 = ai.b(a3, "resting");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                arrayList.add(new DailyHeartRateSummary(a3.getFloat(b2), this.__dateShortStringConverter.a(a3.getString(b3)), a3.getLong(b4), a3.getLong(b5), a3.getInt(b6), a3.getInt(b7), a3.getInt(b8), this.__restingConverter.a(a3.getString(b9))));
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<DailyHeartRateSummary>> getDailyHeartRateSummariesLiveData(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"daily_heart_rate_summary"}, false, new Anon3(b));
    }

    @DexIgnore
    public DailyHeartRateSummary getDailyHeartRateSummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary;
        rh b = rh.b("SELECT * FROM daily_heart_rate_summary WHERE date = ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "average");
            int b3 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a2, "createdAt");
            int b5 = ai.b(a2, "updatedAt");
            int b6 = ai.b(a2, "min");
            int b7 = ai.b(a2, "max");
            int b8 = ai.b(a2, "minuteCount");
            int b9 = ai.b(a2, "resting");
            if (a2.moveToFirst()) {
                dailyHeartRateSummary = new DailyHeartRateSummary(a2.getFloat(b2), this.__dateShortStringConverter.a(a2.getString(b3)), a2.getLong(b4), a2.getLong(b5), a2.getInt(b6), a2.getInt(b7), a2.getInt(b8), this.__restingConverter.a(a2.getString(b9)));
            } else {
                dailyHeartRateSummary = null;
            }
            return dailyHeartRateSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public Date getLastDate() {
        rh b = rh.b("SELECT date FROM daily_heart_rate_summary ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Date date = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                date = this.__dateShortStringConverter.a(a.getString(0));
            }
            return date;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public xe.b<Integer, DailyHeartRateSummary> getSummariesDataSource() {
        return new Anon4(rh.b("SELECT * FROM daily_heart_rate_summary ORDER BY date DESC", 0));
    }

    @DexIgnore
    public void insertDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(dailyHeartRateSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListDailyHeartRateSummary(List<DailyHeartRateSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
