package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$response$1", f = "SummariesRepository.kt", l = {417}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$response$Anon1(SummariesRepository summariesRepository, int i, int i2, int i3, String str, xe6 xe6) {
        super(1, xe6);
        this.this$0 = summariesRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new SummariesRepository$downloadRecommendedGoals$response$Anon1(this.this$0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SummariesRepository$downloadRecommendedGoals$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            int i2 = this.$age;
            int i3 = this.$weightInGrams;
            int i4 = this.$heightInCentimeters;
            String str = this.$gender;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getRecommendedGoalsRaw(i2, i3, i4, str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
