package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.d44;
import com.fossil.e44;
import com.fossil.f44;
import com.fossil.g44;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWorkoutSessionDao_Impl implements GFitWorkoutSessionDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<GFitWorkoutSession> __deletionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ d44 __gFitWOCaloriesConverter; // = new d44();
    @DexIgnore
    public /* final */ e44 __gFitWODistancesConverter; // = new e44();
    @DexIgnore
    public /* final */ f44 __gFitWOHeartRatesConverter; // = new f44();
    @DexIgnore
    public /* final */ g44 __gFitWOStepsConverter; // = new g44();
    @DexIgnore
    public /* final */ hh<GFitWorkoutSession> __insertionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<GFitWorkoutSession> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitWorkoutSession` (`id`,`startTime`,`endTime`,`workoutType`,`steps`,`calories`,`distances`,`heartRates`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GFitWorkoutSession gFitWorkoutSession) {
            miVar.a(1, (long) gFitWorkoutSession.getId());
            miVar.a(2, gFitWorkoutSession.getStartTime());
            miVar.a(3, gFitWorkoutSession.getEndTime());
            miVar.a(4, (long) gFitWorkoutSession.getWorkoutType());
            String a = GFitWorkoutSessionDao_Impl.this.__gFitWOStepsConverter.a(gFitWorkoutSession.getSteps());
            if (a == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a);
            }
            String a2 = GFitWorkoutSessionDao_Impl.this.__gFitWOCaloriesConverter.a(gFitWorkoutSession.getCalories());
            if (a2 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a2);
            }
            String a3 = GFitWorkoutSessionDao_Impl.this.__gFitWODistancesConverter.a(gFitWorkoutSession.getDistances());
            if (a3 == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a3);
            }
            String a4 = GFitWorkoutSessionDao_Impl.this.__gFitWOHeartRatesConverter.a(gFitWorkoutSession.getHeartRates());
            if (a4 == null) {
                miVar.a(8);
            } else {
                miVar.a(8, a4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<GFitWorkoutSession> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitWorkoutSession` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, GFitWorkoutSession gFitWorkoutSession) {
            miVar.a(1, (long) gFitWorkoutSession.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitWorkoutSession";
        }
    }

    @DexIgnore
    public GFitWorkoutSessionDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfGFitWorkoutSession = new Anon1(ohVar);
        this.__deletionAdapterOfGFitWorkoutSession = new Anon2(ohVar);
        this.__preparedStmtOfClearAll = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitWorkoutSession.handle(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitWorkoutSession> getAllGFitWorkoutSession() {
        rh b = rh.b("SELECT * FROM gFitWorkoutSession", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "startTime");
            int b4 = ai.b(a, "endTime");
            int b5 = ai.b(a, "workoutType");
            int b6 = ai.b(a, "steps");
            int b7 = ai.b(a, Constants.CALORIES);
            int b8 = ai.b(a, "distances");
            int b9 = ai.b(a, "heartRates");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitWorkoutSession gFitWorkoutSession = new GFitWorkoutSession(a.getLong(b3), a.getLong(b4), a.getInt(b5), this.__gFitWOStepsConverter.a(a.getString(b6)), this.__gFitWOCaloriesConverter.a(a.getString(b7)), this.__gFitWODistancesConverter.a(a.getString(b8)), this.__gFitWOHeartRatesConverter.a(a.getString(b9)));
                gFitWorkoutSession.setId(a.getInt(b2));
                arrayList.add(gFitWorkoutSession);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitWorkoutSession(List<GFitWorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
