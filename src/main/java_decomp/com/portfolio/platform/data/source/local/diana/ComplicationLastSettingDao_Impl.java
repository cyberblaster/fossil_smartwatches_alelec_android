package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSettingDao_Impl implements ComplicationLastSettingDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<ComplicationLastSetting> __insertionAdapterOfComplicationLastSetting;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteComplicationLastSettingByComplicationId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<ComplicationLastSetting> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complicationLastSetting` (`complicationId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, ComplicationLastSetting complicationLastSetting) {
            if (complicationLastSetting.getComplicationId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, complicationLastSetting.getComplicationId());
            }
            if (complicationLastSetting.getUpdatedAt() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, complicationLastSetting.getUpdatedAt());
            }
            if (complicationLastSetting.getSetting() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, complicationLastSetting.getSetting());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM complicationLastSetting WHERE complicationId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM complicationLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<ComplicationLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<ComplicationLastSetting> call() throws Exception {
            Cursor a = bi.a(ComplicationLastSettingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "complicationId");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new ComplicationLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ComplicationLastSettingDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfComplicationLastSetting = new Anon1(ohVar);
        this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId = new Anon2(ohVar);
        this.__preparedStmtOfCleanUp = new Anon3(ohVar);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteComplicationLastSettingByComplicationId(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.release(acquire);
        }
    }

    @DexIgnore
    public List<ComplicationLastSetting> getAllComplicationLastSetting() {
        rh b = rh.b("SELECT * FROM complicationLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "complicationId");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new ComplicationLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<ComplicationLastSetting>> getAllComplicationLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"complicationLastSetting"}, false, new Anon4(rh.b("SELECT * FROM complicationLastSetting", 0)));
    }

    @DexIgnore
    public ComplicationLastSetting getComplicationLastSetting(String str) {
        rh b = rh.b("SELECT * FROM complicationLastSetting WHERE complicationId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        ComplicationLastSetting complicationLastSetting = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "complicationId");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, MicroAppSetting.SETTING);
            if (a.moveToFirst()) {
                complicationLastSetting = new ComplicationLastSetting(a.getString(b2), a.getString(b3), a.getString(b4));
            }
            return complicationLastSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertComplicationLastSetting(ComplicationLastSetting complicationLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplicationLastSetting.insert(complicationLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
