package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSampleDao_Impl implements GFitSampleDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<GFitSample> __deletionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ hh<GFitSample> __insertionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<GFitSample> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSample` (`id`,`step`,`distance`,`calorie`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GFitSample gFitSample) {
            miVar.a(1, (long) gFitSample.getId());
            miVar.a(2, (long) gFitSample.getStep());
            miVar.a(3, (double) gFitSample.getDistance());
            miVar.a(4, (double) gFitSample.getCalorie());
            miVar.a(5, gFitSample.getStartTime());
            miVar.a(6, gFitSample.getEndTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<GFitSample> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitSample` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, GFitSample gFitSample) {
            miVar.a(1, (long) gFitSample.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitSample";
        }
    }

    @DexIgnore
    public GFitSampleDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfGFitSample = new Anon1(ohVar);
        this.__deletionAdapterOfGFitSample = new Anon2(ohVar);
        this.__preparedStmtOfClearAll = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitSample> getAllGFitSample() {
        rh b = rh.b("SELECT * FROM gFitSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "step");
            int b4 = ai.b(a, "distance");
            int b5 = ai.b(a, "calorie");
            int b6 = ai.b(a, "startTime");
            int b7 = ai.b(a, "endTime");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitSample gFitSample = new GFitSample(a.getInt(b3), a.getFloat(b4), a.getFloat(b5), a.getLong(b6), a.getLong(b7));
                gFitSample.setId(a.getInt(b2));
                arrayList.add(gFitSample);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitSample(GFitSample gFitSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(gFitSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
