package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDatabase_Impl extends GoalTrackingDatabase {
    @DexIgnore
    public volatile GoalTrackingDao _goalTrackingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `goalTrackingRaw` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `trackedAt` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `goalTrackingDay` (`pinType` INTEGER NOT NULL, `date` TEXT NOT NULL, `totalTracked` INTEGER NOT NULL, `goalTarget` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`date`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `goalSetting` (`id` INTEGER NOT NULL, `currentTarget` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ac5e072da94cafa67a74f62806ddb95f')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `goalTrackingRaw`");
            iiVar.b("DROP TABLE IF EXISTS `goalTrackingDay`");
            iiVar.b("DROP TABLE IF EXISTS `goalSetting`");
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = GoalTrackingDatabase_Impl.this.mDatabase = iiVar;
            GoalTrackingDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(7);
            hashMap.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put("trackedAt", new fi.a("trackedAt", "TEXT", true, 0, (String) null, 1));
            hashMap.put("timezoneOffsetInSecond", new fi.a("timezoneOffsetInSecond", "INTEGER", true, 0, (String) null, 1));
            hashMap.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 0, (String) null, 1));
            hashMap.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("goalTrackingRaw", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "goalTrackingRaw");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "goalTrackingRaw(com.portfolio.platform.data.model.goaltracking.GoalTrackingData).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(6);
            hashMap2.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 1, (String) null, 1));
            hashMap2.put("totalTracked", new fi.a("totalTracked", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("goalTarget", new fi.a("goalTarget", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            fi fiVar2 = new fi("goalTrackingDay", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, "goalTrackingDay");
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "goalTrackingDay(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap3.put("currentTarget", new fi.a("currentTarget", "INTEGER", true, 0, (String) null, 1));
            fi fiVar3 = new fi("goalSetting", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "goalSetting");
            if (fiVar3.equals(a3)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "goalSetting(com.portfolio.platform.data.model.GoalSetting).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        GoalTrackingDatabase_Impl.super.assertNotMainThread();
        ii a = GoalTrackingDatabase_Impl.super.getOpenHelper().a();
        try {
            GoalTrackingDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `goalTrackingRaw`");
            a.b("DELETE FROM `goalTrackingDay`");
            a.b("DELETE FROM `goalSetting`");
            GoalTrackingDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            GoalTrackingDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"goalTrackingRaw", "goalTrackingDay", "goalSetting"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(2), "ac5e072da94cafa67a74f62806ddb95f", "56ec29011feb0e074f1d2ba7ecbcbb17");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public GoalTrackingDao getGoalTrackingDao() {
        GoalTrackingDao goalTrackingDao;
        if (this._goalTrackingDao != null) {
            return this._goalTrackingDao;
        }
        synchronized (this) {
            if (this._goalTrackingDao == null) {
                this._goalTrackingDao = new GoalTrackingDao_Impl(this);
            }
            goalTrackingDao = this._goalTrackingDao;
        }
        return goalTrackingDao;
    }
}
