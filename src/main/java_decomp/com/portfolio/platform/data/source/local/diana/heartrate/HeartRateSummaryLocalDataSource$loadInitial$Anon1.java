package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.rm6;
import com.fossil.vk4;
import com.fossil.wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource$loadInitial$Anon1 implements vk4.b {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadInitial$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(vk4.b.a aVar) {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.this$0;
        heartRateSummaryLocalDataSource.calculateStartDate(heartRateSummaryLocalDataSource.mCreatedDate);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.this$0;
        Date mStartDate = heartRateSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        wg6.a((Object) aVar, "helperCallback");
        rm6 unused = heartRateSummaryLocalDataSource2.loadData(mStartDate, mEndDate, aVar);
    }
}
