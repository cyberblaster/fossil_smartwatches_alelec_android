package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ComplicationDao {
    @DexIgnore
    void clearAll();

    @DexIgnore
    List<Complication> getAllComplications();

    @DexIgnore
    LiveData<List<Complication>> getAllComplicationsAsLiveData();

    @DexIgnore
    Complication getComplicationById(String str);

    @DexIgnore
    List<Complication> getComplicationByIds(List<String> list);

    @DexIgnore
    List<Complication> queryComplicationByName(String str);

    @DexIgnore
    void upsertComplicationList(List<Complication> list);
}
