package com.portfolio.platform.data.source.local.diana.notification;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface NotificationSettingsDao {
    @DexIgnore
    void delete();

    @DexIgnore
    LiveData<List<NotificationSettingsModel>> getListNotificationSettings();

    @DexIgnore
    List<NotificationSettingsModel> getListNotificationSettingsNoLiveData();

    @DexIgnore
    LiveData<NotificationSettingsModel> getNotificationSettingsWithFieldIsCall(boolean z);

    @DexIgnore
    NotificationSettingsModel getNotificationSettingsWithIsCallNoLiveData(boolean z);

    @DexIgnore
    void insertListNotificationSettings(List<NotificationSettingsModel> list);

    @DexIgnore
    void insertNotificationSettings(NotificationSettingsModel notificationSettingsModel);
}
