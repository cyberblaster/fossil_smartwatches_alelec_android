package com.portfolio.platform.data.source.local.quickresponse;

import com.portfolio.platform.data.model.QuickResponseSender;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class QuickResponseSenderDao {
    @DexIgnore
    public abstract List<QuickResponseSender> getAllQuickResponseSender();

    @DexIgnore
    public abstract QuickResponseSender getQuickResponseSenderById(int i);

    @DexIgnore
    public abstract long insertQuickResponseSender(QuickResponseSender quickResponseSender);

    @DexIgnore
    public abstract void removeById(int i);
}
