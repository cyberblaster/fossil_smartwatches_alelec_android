package com.portfolio.platform.data.source.local.reminders;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindersSettingsDatabase_Impl extends RemindersSettingsDatabase {
    @DexIgnore
    public volatile InactivityNudgeTimeDao _inactivityNudgeTimeDao;
    @DexIgnore
    public volatile RemindTimeDao _remindTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `inactivityNudgeTimeModel` (`nudgeTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `nudgeTimeType` INTEGER NOT NULL, PRIMARY KEY(`nudgeTimeName`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `remindTimeModel` (`remindTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, PRIMARY KEY(`remindTimeName`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0ee434ba350bbb753b81bda456b5c107')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `inactivityNudgeTimeModel`");
            iiVar.b("DROP TABLE IF EXISTS `remindTimeModel`");
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = RemindersSettingsDatabase_Impl.this.mDatabase = iiVar;
            RemindersSettingsDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("nudgeTimeName", new fi.a("nudgeTimeName", "TEXT", true, 1, (String) null, 1));
            hashMap.put("minutes", new fi.a("minutes", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("nudgeTimeType", new fi.a("nudgeTimeType", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("inactivityNudgeTimeModel", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "inactivityNudgeTimeModel");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "inactivityNudgeTimeModel(com.portfolio.platform.data.InactivityNudgeTimeModel).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("remindTimeName", new fi.a("remindTimeName", "TEXT", true, 1, (String) null, 1));
            hashMap2.put("minutes", new fi.a("minutes", "INTEGER", true, 0, (String) null, 1));
            fi fiVar2 = new fi("remindTimeModel", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar, "remindTimeModel");
            if (fiVar2.equals(a2)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "remindTimeModel(com.portfolio.platform.data.RemindTimeModel).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        RemindersSettingsDatabase_Impl.super.assertNotMainThread();
        ii a = RemindersSettingsDatabase_Impl.super.getOpenHelper().a();
        try {
            RemindersSettingsDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `inactivityNudgeTimeModel`");
            a.b("DELETE FROM `remindTimeModel`");
            RemindersSettingsDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            RemindersSettingsDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"inactivityNudgeTimeModel", "remindTimeModel"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "0ee434ba350bbb753b81bda456b5c107", "7e66671f5f316f81e6558b840c854b68");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public InactivityNudgeTimeDao getInactivityNudgeTimeDao() {
        InactivityNudgeTimeDao inactivityNudgeTimeDao;
        if (this._inactivityNudgeTimeDao != null) {
            return this._inactivityNudgeTimeDao;
        }
        synchronized (this) {
            if (this._inactivityNudgeTimeDao == null) {
                this._inactivityNudgeTimeDao = new InactivityNudgeTimeDao_Impl(this);
            }
            inactivityNudgeTimeDao = this._inactivityNudgeTimeDao;
        }
        return inactivityNudgeTimeDao;
    }

    @DexIgnore
    public RemindTimeDao getRemindTimeDao() {
        RemindTimeDao remindTimeDao;
        if (this._remindTimeDao != null) {
            return this._remindTimeDao;
        }
        synchronized (this) {
            if (this._remindTimeDao == null) {
                this._remindTimeDao = new RemindTimeDao_Impl(this);
            }
            remindTimeDao = this._remindTimeDao;
        }
        return remindTimeDao;
    }
}
