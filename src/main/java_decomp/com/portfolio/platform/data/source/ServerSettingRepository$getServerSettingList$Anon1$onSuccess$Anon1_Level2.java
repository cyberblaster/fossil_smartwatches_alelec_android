package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1", f = "ServerSettingRepository.kt", l = {}, m = "invokeSuspend")
public final class ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingList $serverSettingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository$getServerSettingList$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(ServerSettingRepository$getServerSettingList$Anon1 serverSettingRepository$getServerSettingList$Anon1, ServerSettingList serverSettingList, xe6 xe6) {
        super(2, xe6);
        this.this$0 = serverSettingRepository$getServerSettingList$Anon1;
        this.$serverSettingList = serverSettingList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 = new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this.this$0, this.$serverSettingList, xe6);
        serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2.p$ = (il6) obj;
        return serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getServerSettingList - onSuccess - serverSettingList = [ " + this.$serverSettingList + " ]");
            ServerSettingList serverSettingList = this.$serverSettingList;
            if (serverSettingList != null) {
                List<ServerSetting> serverSettings = serverSettingList.getServerSettings();
                Boolean a = serverSettings != null ? hf6.a(!serverSettings.isEmpty()) : null;
                if (a == null) {
                    wg6.a();
                    throw null;
                } else if (a.booleanValue()) {
                    this.this$0.this$0.mServerSettingLocalDataSource.addOrUpdateServerSettingList(this.$serverSettingList.getServerSettings());
                }
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
