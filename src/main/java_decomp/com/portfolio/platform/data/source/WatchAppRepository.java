package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jm4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "WatchAppRepository";
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ WatchAppDao mWatchAppDao;
    @DexIgnore
    public /* final */ WatchAppRemoteDataSource mWatchAppRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public WatchAppRepository(WatchAppDao watchAppDao, WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        wg6.b(watchAppDao, "mWatchAppDao");
        wg6.b(watchAppRemoteDataSource, "mWatchAppRemoteDataSource");
        wg6.b(portfolioApp, "mPortfolioApp");
        this.mWatchAppDao = watchAppDao;
        this.mWatchAppRemoteDataSource = watchAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mWatchAppDao.clearAll();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadWatchApp(String str, xe6<? super cd6> xe6) {
        WatchAppRepository$downloadWatchApp$Anon1 watchAppRepository$downloadWatchApp$Anon1;
        int i;
        WatchAppRepository watchAppRepository;
        ap4 ap4;
        if (xe6 instanceof WatchAppRepository$downloadWatchApp$Anon1) {
            watchAppRepository$downloadWatchApp$Anon1 = (WatchAppRepository$downloadWatchApp$Anon1) xe6;
            int i2 = watchAppRepository$downloadWatchApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchAppRepository$downloadWatchApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchAppRepository$downloadWatchApp$Anon1.result;
                Object a = ff6.a();
                i = watchAppRepository$downloadWatchApp$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadWatchApp of " + str);
                    WatchAppRemoteDataSource watchAppRemoteDataSource = this.mWatchAppRemoteDataSource;
                    watchAppRepository$downloadWatchApp$Anon1.L$0 = this;
                    watchAppRepository$downloadWatchApp$Anon1.L$1 = str;
                    watchAppRepository$downloadWatchApp$Anon1.label = 1;
                    obj = watchAppRemoteDataSource.getAllWatchApp(str, watchAppRepository$downloadWatchApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchAppRepository = this;
                } else if (i == 1) {
                    str = (String) watchAppRepository$downloadWatchApp$Anon1.L$1;
                    watchAppRepository = (WatchAppRepository) watchAppRepository$downloadWatchApp$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadWatchApp of ");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    sb.append(" response ");
                    sb.append((List) cp4.a());
                    local2.d(TAG, sb.toString());
                    List<WatchApp> allWatchApp = watchAppRepository.mWatchAppDao.getAllWatchApp();
                    List list = (List) cp4.a();
                    if (list != null && (!wg6.a((Object) list, (Object) allWatchApp))) {
                        watchAppRepository.mWatchAppDao.clearAll();
                        watchAppRepository.mWatchAppDao.upsertWatchAppList(list);
                        FLogger.INSTANCE.getLocal().d(TAG, "downloadWatchApp - insert to database success");
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadWatchApp of ");
                    sb2.append(str);
                    sb2.append(" fail!!! error=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = zo4.c();
                    sb2.append(c != null ? c.getCode() : null);
                    local3.d(TAG, sb2.toString());
                }
                return cd6.a;
            }
        }
        watchAppRepository$downloadWatchApp$Anon1 = new WatchAppRepository$downloadWatchApp$Anon1(this, xe6);
        Object obj2 = watchAppRepository$downloadWatchApp$Anon1.result;
        Object a2 = ff6.a();
        i = watchAppRepository$downloadWatchApp$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final List<WatchApp> getAllWatchAppRaw() {
        return this.mWatchAppDao.getAllWatchApp();
    }

    @DexIgnore
    public final List<WatchApp> getWatchAppByIds(List<String> list) {
        wg6.b(list, "ids");
        if (!list.isEmpty()) {
            return yd6.a(this.mWatchAppDao.getWatchAppByIds(list), new WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1(list));
        }
        return new ArrayList();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final List<WatchApp> queryWatchAppByName(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (WatchApp next : this.mWatchAppDao.getAllWatchApp()) {
            String normalize = Normalizer.normalize(jm4.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            wg6.a((Object) normalize, "name");
            wg6.a((Object) normalize2, "searchQuery");
            if (yj6.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
