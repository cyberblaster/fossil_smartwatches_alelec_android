package com.portfolio.platform.data.source.local.dnd;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDScheduledTimeDao_Impl implements DNDScheduledTimeDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<DNDScheduledTimeModel> __insertionAdapterOfDNDScheduledTimeModel;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<DNDScheduledTimeModel> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dndScheduledTimeModel` (`scheduledTimeName`,`minutes`,`scheduledTimeType`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, DNDScheduledTimeModel dNDScheduledTimeModel) {
            if (dNDScheduledTimeModel.getScheduledTimeName() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, dNDScheduledTimeModel.getScheduledTimeName());
            }
            miVar.a(2, (long) dNDScheduledTimeModel.getMinutes());
            miVar.a(3, (long) dNDScheduledTimeModel.getScheduledTimeType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dndScheduledTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DNDScheduledTimeModel> call() throws Exception {
            Cursor a = bi.a(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "scheduledTimeName");
                int b2 = ai.b(a, "minutes");
                int b3 = ai.b(a, "scheduledTimeType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DNDScheduledTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<DNDScheduledTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public DNDScheduledTimeModel call() throws Exception {
            DNDScheduledTimeModel dNDScheduledTimeModel = null;
            Cursor a = bi.a(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "scheduledTimeName");
                int b2 = ai.b(a, "minutes");
                int b3 = ai.b(a, "scheduledTimeType");
                if (a.moveToFirst()) {
                    dNDScheduledTimeModel = new DNDScheduledTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3));
                }
                return dNDScheduledTimeModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DNDScheduledTimeDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfDNDScheduledTimeModel = new Anon1(ohVar);
        this.__preparedStmtOfDelete = new Anon2(ohVar);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public DNDScheduledTimeModel getDNDScheduledTimeModelWithFieldScheduledTimeType(int i) {
        rh b = rh.b("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        b.a(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        DNDScheduledTimeModel dNDScheduledTimeModel = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "scheduledTimeName");
            int b3 = ai.b(a, "minutes");
            int b4 = ai.b(a, "scheduledTimeType");
            if (a.moveToFirst()) {
                dNDScheduledTimeModel = new DNDScheduledTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4));
            }
            return dNDScheduledTimeModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<DNDScheduledTimeModel> getDNDScheduledTimeWithFieldScheduledTimeType(int i) {
        rh b = rh.b("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        b.a(1, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"dndScheduledTimeModel"}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<DNDScheduledTimeModel>> getListDNDScheduledTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"dndScheduledTimeModel"}, false, new Anon3(rh.b("SELECT * FROM dndScheduledTimeModel", 0)));
    }

    @DexIgnore
    public List<DNDScheduledTimeModel> getListDNDScheduledTimeModel() {
        rh b = rh.b("SELECT * FROM dndScheduledTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "scheduledTimeName");
            int b3 = ai.b(a, "minutes");
            int b4 = ai.b(a, "scheduledTimeType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DNDScheduledTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertDNDScheduledTime(DNDScheduledTimeModel dNDScheduledTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(dNDScheduledTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListDNDScheduledTime(List<DNDScheduledTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
