package com.portfolio.platform.data.source.local.thirdparty;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GFitHeartRateDao {
    @DexIgnore
    void clearAll();

    @DexIgnore
    void deleteListGFitHeartRate(List<GFitHeartRate> list);

    @DexIgnore
    List<GFitHeartRate> getAllGFitHeartRate();

    @DexIgnore
    void insertGFitHeartRate(GFitHeartRate gFitHeartRate);

    @DexIgnore
    void insertListGFitHeartRate(List<GFitHeartRate> list);
}
