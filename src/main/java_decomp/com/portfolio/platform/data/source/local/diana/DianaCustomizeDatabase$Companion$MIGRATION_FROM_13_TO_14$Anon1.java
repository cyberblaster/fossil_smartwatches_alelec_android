package com.portfolio.platform.data.source.local.diana;

import com.fossil.ai4;
import com.fossil.ii;
import com.fossil.wg6;
import com.fossil.xh;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1 extends xh {
    @DexIgnore
    public DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(ii iiVar) {
        wg6.b(iiVar, "database");
        FLogger.INSTANCE.getLocal().d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_13_TO_14 - start");
        iiVar.u();
        try {
            FLogger.INSTANCE.getLocal().d(DianaCustomizeDatabase.TAG, "Migrate WatchFace - start");
            iiVar.b("DROP TABLE IF EXISTS `DianaComplicationRingStyle`");
            iiVar.b("CREATE TABLE `DianaComplicationRingStyle` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("DROP TABLE IF EXISTS `watch_face_temp`");
            iiVar.b("CREATE TABLE `watch_face_temp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("INSERT INTO watch_face_temp(id,name, ringStyleItems, background, previewUrl, serial) SELECT id, name, ringStyleItems, background, previewUrl, serial FROM watch_face");
            int value = ai4.BACKGROUND.getValue();
            iiVar.b("ALTER TABLE `watch_face_temp` ADD `watchFaceType` INTEGER NOT NULL CONSTRAINT DefaultTypeValue DEFAULT(" + value + ')');
            iiVar.b("DROP TABLE `watch_face`");
            iiVar.b("ALTER TABLE watch_face_temp RENAME TO watch_face");
            FLogger.INSTANCE.getLocal().d(DianaCustomizeDatabase.TAG, "Migrate WatchFace - end");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_13_TO_14 - end with exception -- e=" + e);
            iiVar.b("DROP TABLE IF EXISTS `watch_face`");
            iiVar.b("DROP TABLE IF EXISTS `watch_face_temp`");
            iiVar.b("CREATE TABLE `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, `watchFaceType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        }
        iiVar.x();
        iiVar.y();
    }
}
