package com.portfolio.platform.data.source.local;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.wg6;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FitnessDataDao {
    @DexIgnore
    public abstract void deleteAllFitnessData();

    @DexIgnore
    public abstract void deleteFitnessData(List<FitnessDataWrapper> list);

    @DexIgnore
    public final List<FitnessDataWrapper> getFitnessData(Date date, Date date2) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        Date o = bk4.o(date);
        Date j = bk4.j(date2);
        wg6.a((Object) o, "startTime");
        DateTime dateTime = new DateTime(o.getTime());
        wg6.a((Object) j, "endTime");
        return getListFitnessData(dateTime, new DateTime(j.getTime()));
    }

    @DexIgnore
    public final LiveData<List<FitnessDataWrapper>> getFitnessDataLiveData(Date date, Date date2) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        Date o = bk4.o(date);
        Date j = bk4.j(date2);
        wg6.a((Object) o, "startTime");
        DateTime dateTime = new DateTime(o.getTime());
        wg6.a((Object) j, "endTime");
        return getListFitnessDataLiveData(dateTime, new DateTime(j.getTime()));
    }

    @DexIgnore
    public abstract List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract List<FitnessDataWrapper> getPendingFitnessData();

    @DexIgnore
    public abstract void insertFitnessDataList(List<FitnessDataWrapper> list);
}
