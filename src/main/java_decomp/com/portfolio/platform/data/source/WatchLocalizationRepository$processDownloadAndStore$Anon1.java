package com.portfolio.platform.data.source;

import com.fossil.FileHelper;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zq6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1", f = "WatchLocalizationRepository.kt", l = {66}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1 extends sf6 implements ig6<il6, xe6<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $path;
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, String str2, xe6 xe6) {
        super(2, xe6);
        this.this$0 = watchLocalizationRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1 = new WatchLocalizationRepository$processDownloadAndStore$Anon1(this.this$0, this.$url, this.$path, xe6);
        watchLocalizationRepository$processDownloadAndStore$Anon1.p$ = (il6) obj;
        return watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchLocalizationRepository$processDownloadAndStore$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 = new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = ResponseKt.a(watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ap4 ap4 = (ap4) obj;
        if (ap4 instanceof cp4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$p = this.this$0.TAG;
            local.d(access$getTAG$p, "start storing for url: " + this.$url + " to path: " + this.$path);
            FileHelper fileHelper = FileHelper.a;
            Object a2 = ((cp4) ap4).a();
            if (a2 == null) {
                wg6.a();
                throw null;
            } else if (fileHelper.a((zq6) a2, this.$path)) {
                return this.$path;
            } else {
                return null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String access$getTAG$p2 = this.this$0.TAG;
            local2.d(access$getTAG$p2, "download: " + this.$url + " | FAILED");
            return null;
        }
    }
}
