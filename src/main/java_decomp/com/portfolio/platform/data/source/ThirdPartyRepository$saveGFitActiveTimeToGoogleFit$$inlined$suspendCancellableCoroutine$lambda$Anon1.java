package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ew1;
import com.fossil.ff6;
import com.fossil.fw1;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lk6;
import com.fossil.ll6;
import com.fossil.mc6;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wv1;
import com.fossil.xe6;
import com.fossil.zl6;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1<R extends ew1> implements fw1<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitActiveTimeList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ wv1 $googleApiClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitActiveTimeList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2) {
                this.this$0 = anon1_Level2;
            }

            @DexIgnore
            public final void run() {
                this.this$0.this$0.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().deleteGFitActiveTime(this.this$0.this$0.$gFitActiveTime);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.this$0.getMThirdPartyDatabase().runInTransaction(new Anon1_Level3(this));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(GFitActiveTime gFitActiveTime, wv1 wv1, hh6 hh6, int i, lk6 lk6, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$gFitActiveTime = gFitActiveTime;
        this.$googleApiClient$inlined = wv1;
        this.$countSizeOfList$inlined = hh6;
        this.$sizeOfGFitActiveTimeList$inlined = i;
        this.$continuation$inlined = lk6;
        this.this$0 = thirdPartyRepository;
        this.$gFitActiveTimeList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onResult(Status status) {
        wg6.b(status, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Sending GFitActiveTime: Status = ");
        status.o();
        sb.append(status);
        sb.append(" - Status Message = ");
        sb.append(status.C());
        local.d(ThirdPartyRepository.TAG, sb.toString());
        if (status.F()) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new Anon1_Level2(this, (xe6) null), 3, (Object) null);
        }
        hh6 hh6 = this.$countSizeOfList$inlined;
        hh6.element++;
        if (hh6.element >= this.$sizeOfGFitActiveTimeList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitActiveTimeToGoogleFit");
            lk6 lk6 = this.$continuation$inlined;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
    }
}
