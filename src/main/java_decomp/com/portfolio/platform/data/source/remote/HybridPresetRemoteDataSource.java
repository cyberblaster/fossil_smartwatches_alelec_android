package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cp4;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.qj4;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "HybridPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public HybridPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deletePreset(HybridPreset hybridPreset, xe6<? super ap4<Void>> xe6) {
        HybridPresetRemoteDataSource$deletePreset$Anon1 hybridPresetRemoteDataSource$deletePreset$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof HybridPresetRemoteDataSource$deletePreset$Anon1) {
            hybridPresetRemoteDataSource$deletePreset$Anon1 = (HybridPresetRemoteDataSource$deletePreset$Anon1) xe6;
            int i2 = hybridPresetRemoteDataSource$deletePreset$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$deletePreset$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$deletePreset$Anon1.result;
                Object a = ff6.a();
                i = hybridPresetRemoteDataSource$deletePreset$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    fu3 fu3 = new fu3();
                    fu3.a(hybridPreset.getId());
                    ku3.a("_ids", fu3);
                    HybridPresetRemoteDataSource$deletePreset$response$Anon1 hybridPresetRemoteDataSource$deletePreset$response$Anon1 = new HybridPresetRemoteDataSource$deletePreset$response$Anon1(this, ku3, (xe6) null);
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$0 = this;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$1 = hybridPreset;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$2 = ku3;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$3 = fu3;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.label = 1;
                    obj = ResponseKt.a(hybridPresetRemoteDataSource$deletePreset$response$Anon1, hybridPresetRemoteDataSource$deletePreset$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    fu3 fu32 = (fu3) hybridPresetRemoteDataSource$deletePreset$Anon1.L$3;
                    ku3 ku32 = (ku3) hybridPresetRemoteDataSource$deletePreset$Anon1.L$2;
                    HybridPreset hybridPreset2 = (HybridPreset) hybridPresetRemoteDataSource$deletePreset$Anon1.L$1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$deletePreset$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new cp4((Object) null, false, 2, (qg6) null);
                }
                if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                }
                throw new kc6();
            }
        }
        hybridPresetRemoteDataSource$deletePreset$Anon1 = new HybridPresetRemoteDataSource$deletePreset$Anon1(this, xe6);
        Object obj2 = hybridPresetRemoteDataSource$deletePreset$Anon1.result;
        Object a2 = ff6.a();
        i = hybridPresetRemoteDataSource$deletePreset$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadHybridPresetList(String str, xe6<? super ap4<ArrayList<HybridPreset>>> xe6) {
        HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1) {
            hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 = (HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1) xe6;
            int i2 = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.result;
                Object a = ff6.a();
                i = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 hybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 = new HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1(this, str, (xe6) null);
                    hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$0 = this;
                    hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$1 = str;
                    hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(hybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1, hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    cp4 cp4 = (cp4) ap4;
                    if (!cp4.b()) {
                        Object a2 = cp4.a();
                        if (a2 != null) {
                            for (HybridPreset hybridPreset : ((ApiResponse) a2).get_items()) {
                                hybridPreset.setPinType(0);
                                hybridPreset.setSerialNumber(str);
                                arrayList.add(hybridPreset);
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    return new cp4(arrayList, cp4.b());
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 = new HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1(this, xe6);
        Object obj2 = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.result;
        Object a3 = ff6.a();
        i = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object downloadHybridRecommendPresetList(String str, xe6<? super ap4<ArrayList<HybridRecommendPreset>>> xe6) {
        HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1) {
            hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 = (HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1) xe6;
            int i2 = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.result;
                Object a = ff6.a();
                i = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadHybridRecommendPresetList " + str);
                    HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1 hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1 = new HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1(this, str, (xe6) null);
                    hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$0 = this;
                    hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$1 = str;
                    hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1, hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadHybridRecommendPresetList success ");
                    sb.append(arrayList);
                    sb.append(" isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    local2.d(TAG, sb.toString());
                    String u = bk4.u(new Date(System.currentTimeMillis()));
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        for (HybridRecommendPreset hybridRecommendPreset : ((ApiResponse) a2).get_items()) {
                            hybridRecommendPreset.setSerialNumber(str);
                            wg6.a((Object) u, "timestamp");
                            hybridRecommendPreset.setCreatedAt(u);
                            hybridRecommendPreset.setUpdatedAt(u);
                            arrayList.add(hybridRecommendPreset);
                        }
                        return new cp4(arrayList, cp4.b());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadHybridRecommendPresetList fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverError ");
                    sb2.append(zo4.c());
                    local3.d(TAG, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 = new HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1(this, xe6);
        Object obj2 = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.result;
        Object a3 = ff6.a();
        i = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object replaceHybridPresetList(List<HybridPreset> list, xe6<? super ap4<ArrayList<HybridPreset>>> xe6) {
        HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1) {
            hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 = (HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1) xe6;
            int i2 = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.result;
                Object a = ff6.a();
                i = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    du3 du3 = new du3();
                    du3.b(new qj4());
                    Gson a2 = du3.a();
                    ku3 ku3 = new ku3();
                    Object[] array = list.toArray(new HybridPreset[0]);
                    if (array != null) {
                        ku3.a(CloudLogWriter.ITEMS_PARAM, a2.b(array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "replaceHybridPresetList jsonObject " + ku3);
                        HybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1 hybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1 = new HybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1(this, ku3, (xe6) null);
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$0 = this;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$1 = list;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$2 = a2;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$3 = ku3;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(hybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1, hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$3;
                    Gson gson = (Gson) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$2;
                    List list2 = (List) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((cp4) ap4).a();
                    if (a3 != null) {
                        for (HybridPreset hybridPreset : ((ApiResponse) a3).get_items()) {
                            hybridPreset.setPinType(0);
                            arrayList.add(hybridPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "replaceHybridPresetList success " + arrayList);
                        return new cp4(arrayList, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("replaceHybridPresetList fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" serverError ");
                    sb.append(zo4.c());
                    local3.d(TAG, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 = new HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1(this, xe6);
        Object obj2 = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.result;
        Object a4 = ff6.a();
        i = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object upsertHybridPresetList(List<HybridPreset> list, xe6<? super ap4<ArrayList<HybridPreset>>> xe6) {
        HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1) {
            hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 = (HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1) xe6;
            int i2 = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.result;
                Object a = ff6.a();
                i = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    du3 du3 = new du3();
                    du3.b(new qj4());
                    Gson a2 = du3.a();
                    ku3 ku3 = new ku3();
                    Object[] array = list.toArray(new HybridPreset[0]);
                    if (array != null) {
                        ku3.a(CloudLogWriter.ITEMS_PARAM, a2.b(array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "upsertHybridPresetList jsonObject " + ku3);
                        HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1 hybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1 = new HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1(this, ku3, (xe6) null);
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$0 = this;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$1 = list;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$2 = a2;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$3 = ku3;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(hybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1, hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$3;
                    Gson gson = (Gson) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$2;
                    List list2 = (List) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((cp4) ap4).a();
                    if (a3 != null) {
                        for (HybridPreset hybridPreset : ((ApiResponse) a3).get_items()) {
                            hybridPreset.setPinType(0);
                            arrayList.add(hybridPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "upsertHybridPresetList success " + arrayList);
                        return new cp4(arrayList, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("upsertHybridPresetList fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" serverError ");
                    sb.append(zo4.c());
                    local3.d(TAG, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null, 12, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 = new HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1(this, xe6);
        Object obj2 = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.result;
        Object a4 = ff6.a();
        i = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
