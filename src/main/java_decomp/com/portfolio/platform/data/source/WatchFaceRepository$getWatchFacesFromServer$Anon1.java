package com.portfolio.platform.data.source;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.WatchFaceRepository", f = "WatchFaceRepository.kt", l = {29, 45, 52, 53, 54, 57, 58}, m = "getWatchFacesFromServer")
public final class WatchFaceRepository$getWatchFacesFromServer$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$10;
    @DexIgnore
    public Object L$11;
    @DexIgnore
    public Object L$12;
    @DexIgnore
    public Object L$13;
    @DexIgnore
    public Object L$14;
    @DexIgnore
    public Object L$15;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRepository$getWatchFacesFromServer$Anon1(WatchFaceRepository watchFaceRepository, xe6 xe6) {
        super(xe6);
        this.this$0 = watchFaceRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.getWatchFacesFromServer((String) null, this);
    }
}
