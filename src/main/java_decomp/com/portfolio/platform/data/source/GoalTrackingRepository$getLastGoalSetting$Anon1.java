package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.GoalSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$getLastGoalSetting$Anon1 extends tx5<Integer, GoalSetting> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    public GoalTrackingRepository$getLastGoalSetting$Anon1(GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    public Object createCall(xe6<? super rx6<GoalSetting>> xe6) {
        return this.this$0.mApiServiceV2.getGoalSetting(xe6);
    }

    @DexIgnore
    public LiveData<Integer> loadFromDb() {
        return this.this$0.mGoalTrackingDao.getLastGoalSettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getLastGoalSetting onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(GoalSetting goalSetting) {
        wg6.b(goalSetting, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = GoalTrackingRepository.Companion.getTAG();
        local.d(tag, "getLastGoalSetting saveCallResult goal: " + goalSetting);
        this.this$0.saveSettingToDB(goalSetting);
    }
}
