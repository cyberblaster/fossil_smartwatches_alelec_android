package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.fossil.af;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.ji;
import com.fossil.jl6;
import com.fossil.lc6;
import com.fossil.lh;
import com.fossil.ll6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wk4;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource extends af<Date, ActivitySummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public /* final */ ActivitySummaryDao mActivitySummaryDao;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ ik4 mFitnessHelper;
    @DexIgnore
    public vk4 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = wk4.a(this.mHelper);
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public List<lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ SummariesRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends lh.c {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = activitySummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wg6.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            wg6.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = bk4.c(instance);
            if (bk4.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            wg6.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitySummaryLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "ActivitySummaryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitySummaryLocalDataSource(SummariesRepository summariesRepository, ik4 ik4, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(ik4, "mFitnessHelper");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(activitySummaryDao, "mActivitySummaryDao");
        wg6.b(fitnessDatabase, "mFitnessDatabase");
        wg6.b(date, "mCreatedDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        wg6.b(calendar, "key");
        this.mSummariesRepository = summariesRepository;
        this.mFitnessHelper = ik4;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mActivitySummaryDao = activitySummaryDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new vk4(u04.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "sampleday", new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = bk4.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        wg6.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (bk4.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final void calculateSummaries(List<ActivitySummary> list) {
        int i;
        int i2;
        int i3;
        int i4;
        List<ActivitySummary> list2 = list;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "endCalendar");
            instance.setTime(((ActivitySummary) yd6.f(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar q = bk4.q(instance.getTime());
                wg6.a((Object) q, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                ActivitySummaryDao activitySummaryDao = this.mActivitySummaryDao;
                Date time = q.getTime();
                wg6.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                wg6.a((Object) time2, "endCalendar.time");
                ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummaryDao.getTotalValuesOfWeek(time, time2);
                i2 = (int) totalValuesOfWeek.getTotalStepsOfWeek();
                i = (int) totalValuesOfWeek.getTotalCaloriesOfWeek();
                i3 = totalValuesOfWeek.getTotalActiveTimeOfWeek();
            } else {
                i3 = 0;
                i2 = 0;
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "calendar");
            instance2.setTime(((ActivitySummary) yd6.d(list)).getDate());
            Calendar q2 = bk4.q(instance2.getTime());
            wg6.a((Object) q2, "DateHelper.getStartOfWeek(calendar.time)");
            q2.add(5, -1);
            int i5 = 0;
            int i6 = 0;
            double d = 0.0d;
            double d2 = 0.0d;
            int i7 = 0;
            for (T next : list) {
                int i8 = i6 + 1;
                if (i6 >= 0) {
                    ActivitySummary activitySummary = (ActivitySummary) next;
                    if (bk4.d(activitySummary.getDate(), q2.getTime())) {
                        i4 = i6;
                        list2.get(i5).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i7));
                        q2.add(5, -7);
                        i5 = i4;
                        d = 0.0d;
                        d2 = 0.0d;
                        i7 = 0;
                    } else {
                        i4 = i6;
                    }
                    d += activitySummary.getSteps();
                    d2 += activitySummary.getCalories();
                    i7 += activitySummary.getActiveTime();
                    if (i4 == list.size() - 1) {
                        d += (double) i2;
                        d2 += (double) i;
                        i7 += i3;
                    }
                    i6 = i8;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            list2.get(i5).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i7));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSummaries summaries.size=" + list.size());
    }

    @DexIgnore
    private final ActivitySummary dummySummary(ActivitySummary activitySummary, Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        ActivitySummary activitySummary2 = r1;
        ActivitySummary activitySummary3 = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), activitySummary.getTimezoneName(), activitySummary.getDstOffset(), 0.0d, 0.0d, 0.0d, qd6.d(0, 0, 0), 0, 0, 0, 0, 7680, (qg6) null);
        ActivitySummary activitySummary4 = activitySummary2;
        activitySummary4.setCreatedAt(DateTime.now());
        activitySummary4.setUpdatedAt(DateTime.now());
        return activitySummary4;
    }

    @DexIgnore
    private final List<ActivitySummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        Date date3 = date;
        Date date4 = date2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("getDataInDatabase - startDate=");
        sb.append(date3);
        sb.append(", endDate=");
        sb.append(date4);
        sb.append(" DBNAME=");
        ji openHelper = this.mFitnessDatabase.getOpenHelper();
        wg6.a((Object) openHelper, "mFitnessDatabase.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d(str, sb.toString());
        List<ActivitySummary> activitySummariesDesc = this.mActivitySummaryDao.getActivitySummariesDesc(bk4.b(date, date2) ? date4 : date3, date4);
        if (!activitySummariesDesc.isEmpty()) {
            Boolean t = bk4.t(((ActivitySummary) yd6.d(activitySummariesDesc)).getDate());
            wg6.a((Object) t, "DateHelper.isToday(summaries.first().getDate())");
            if (t.booleanValue()) {
                ((ActivitySummary) yd6.d(activitySummariesDesc)).setSteps(Math.max((double) this.mFitnessHelper.a(new Date()), ((ActivitySummary) yd6.d(activitySummariesDesc)).getSteps()));
            }
        }
        calculateSummaries(activitySummariesDesc);
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mActivitySummaryDao.getLastDate();
        if (lastDate == null) {
            lastDate = date3;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(activitySummariesDesc);
        ActivitySummary activitySummary = this.mActivitySummaryDao.getActivitySummary(date4);
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "endCalendar");
        instance.setTime(date4);
        if (activitySummary == null) {
            activitySummary = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), "", 0, 0.0d, 0.0d, 0.0d, qd6.d(0, 0, 0), 0, 0, 0, 0, 7680, (qg6) null);
            activitySummary.setActiveTimeGoal(30);
            activitySummary.setStepGoal(5000);
            activitySummary.setCaloriesGoal(140);
        }
        if (!bk4.b(date3, lastDate)) {
            date3 = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + activitySummariesDesc.size() + ", summaryParent=" + activitySummary + ", " + "lastDate=" + lastDate + ", startDateToFill=" + date3);
        while (bk4.c(date4, date3)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (bk4.d(((ActivitySummary) obj).getDate(), date4)) {
                    break;
                }
            }
            ActivitySummary activitySummary2 = (ActivitySummary) obj;
            if (activitySummary2 == null) {
                arrayList.add(dummySummary(activitySummary, date4));
            } else {
                arrayList.add(activitySummary2);
                arrayList2.remove(activitySummary2);
            }
            date4 = bk4.n(date4);
            wg6.a((Object) date4, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            ActivitySummary activitySummary3 = (ActivitySummary) yd6.d(arrayList);
            Boolean t2 = bk4.t(activitySummary3.getDate());
            wg6.a((Object) t2, "DateHelper.isToday(todaySummary.getDate())");
            if (t2.booleanValue()) {
                arrayList.add(0, new ActivitySummary(activitySummary3));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final void loadData(vk4.d dVar, Date date, Date date2, vk4.b.a aVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadData start=" + date + ", end=" + date2);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new ActivitySummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final vk4 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return ActivitySummaryLocalDataSource.super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(af.f<Date> fVar, af.a<Date, ActivitySummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (bk4.b((Date) fVar.a, this.mCreatedDate)) {
            Object obj = fVar.a;
            wg6.a(obj, "params.key");
            Date date = (Date) obj;
            Companion companion = Companion;
            Object obj2 = fVar.a;
            wg6.a(obj2, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) obj2, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date m = bk4.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : bk4.m(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + m + ", endQueryDate=" + date);
            wg6.a((Object) m, "startQueryDate");
            aVar.a(getDataInDatabase(m, date), calculateNextKey);
            if (bk4.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new lc6(this.mStartDate, this.mEndDate));
                this.mHelper.a(vk4.d.AFTER, (vk4.b) new ActivitySummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(af.f<Date> fVar, af.a<Date, ActivitySummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(af.e<Date> eVar, af.c<Date, ActivitySummary> cVar) {
        wg6.b(eVar, "params");
        wg6.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date m = bk4.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : bk4.m(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + m + ", endQueryDate=" + date);
        wg6.a((Object) m, "startQueryDate");
        cVar.a(getDataInDatabase(m, date), (Object) null, this.key.getTime());
        this.mHelper.a(vk4.d.INITIAL, (vk4.b) new ActivitySummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(vk4 vk4) {
        wg6.b(vk4, "<set-?>");
        this.mHelper = vk4;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wg6.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
