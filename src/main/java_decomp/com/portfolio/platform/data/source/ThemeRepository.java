package com.portfolio.platform.data.source;

import android.text.TextUtils;
import com.fossil.cd6;
import com.fossil.dg6;
import com.fossil.du3;
import com.fossil.ej6;
import com.fossil.ff6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yf6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.gson.ThemeDeserializer;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String DEFAULT_THEME_URI; // = "theme/default/config";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ PortfolioApp mApp;
    @DexIgnore
    public /* final */ ThemeDao mThemeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return ThemeRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ThemeRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "ThemeRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ThemeRepository(ThemeDao themeDao, PortfolioApp portfolioApp) {
        wg6.b(themeDao, "mThemeDao");
        wg6.b(portfolioApp, "mApp");
        this.mThemeDao = themeDao;
        this.mApp = portfolioApp;
    }

    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        com.fossil.yf6.a(r3, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0082, code lost:
        throw r0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    private final void loadTheme(String str) {
        Class<Theme> cls = Theme.class;
        du3 du3 = new du3();
        du3.a(cls, new ThemeDeserializer());
        Gson a = du3.a();
        BufferedReader bufferedReader = null;
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(new BufferedInputStream(this.mApp.getAssets().open(str)), ej6.a);
            BufferedReader bufferedReader2 = inputStreamReader instanceof BufferedReader ? (BufferedReader) inputStreamReader : new BufferedReader(inputStreamReader, 8192);
            String a2 = dg6.a(bufferedReader2);
            try {
                yf6.a(bufferedReader2, (Throwable) null);
                if (!TextUtils.isEmpty(a2)) {
                    Theme theme = (Theme) a.a(a2, cls);
                    ThemeDao themeDao = this.mThemeDao;
                    wg6.a((Object) theme, "theme");
                    themeDao.upsertTheme(theme);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "initializeLocalTheme - add theme " + theme);
                }
                bufferedReader2.close();
            } catch (Exception e) {
                e = e;
                bufferedReader = bufferedReader2;
                try {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, "Exception when parse theme file " + e);
                    if (bufferedReader == null) {
                        bufferedReader.close();
                    }
                } catch (Throwable th) {
                    th = th;
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = bufferedReader2;
                if (bufferedReader != null) {
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str32 = TAG;
            local22.d(str32, "Exception when parse theme file " + e);
            if (bufferedReader == null) {
            }
        }
    }

    @DexIgnore
    public final Object cleanUp(xe6<? super cd6> xe6) {
        this.mThemeDao.clearThemeTable();
        return cd6.a;
    }

    @DexIgnore
    public final Object deleteTheme(Theme theme, xe6<? super cd6> xe6) {
        this.mThemeDao.deleteTheme(theme);
        return cd6.a;
    }

    @DexIgnore
    public final Object getCurrentTheme(xe6<? super Theme> xe6) {
        return this.mThemeDao.getCurrentTheme();
    }

    @DexIgnore
    public final Object getCurrentThemeId(xe6<? super String> xe6) {
        return this.mThemeDao.getCurrentThemeId();
    }

    @DexIgnore
    public final Object getListStyleById(String str, xe6<? super ArrayList<Style>> xe6) {
        Theme themeById = this.mThemeDao.getThemeById(str);
        if (themeById != null) {
            return themeById.getStyles();
        }
        return null;
    }

    @DexIgnore
    public final Object getListTheme(xe6<? super List<Theme>> xe6) {
        return this.mThemeDao.getListTheme();
    }

    @DexIgnore
    public final Object getListThemeId(xe6<? super List<String>> xe6) {
        return this.mThemeDao.getListThemeId();
    }

    @DexIgnore
    public final Object getListThemeIdByType(String str, xe6<? super List<String>> xe6) {
        return this.mThemeDao.getListThemeIdByType(str);
    }

    @DexIgnore
    public final Object getNameById(String str, xe6<? super String> xe6) {
        return this.mThemeDao.getNameById(str);
    }

    @DexIgnore
    public final Object getThemeById(String str, xe6<? super Theme> xe6) {
        return this.mThemeDao.getThemeById(str);
    }

    @DexIgnore
    public final Object initializeLocalTheme(xe6<? super cd6> xe6) {
        loadTheme(DEFAULT_THEME_URI);
        Object currentThemeId = setCurrentThemeId(PortfolioApp.get.instance().m(), xe6);
        if (currentThemeId == ff6.a()) {
            return currentThemeId;
        }
        return cd6.a;
    }

    @DexIgnore
    public final Object setCurrentThemeId(String str, xe6<? super cd6> xe6) {
        this.mThemeDao.resetAllCurrentTheme();
        this.mThemeDao.setCurrentThemeId(str);
        return cd6.a;
    }

    @DexIgnore
    public final Object upsertUserTheme(Theme theme, xe6<? super cd6> xe6) {
        this.mThemeDao.upsertTheme(theme);
        return cd6.a;
    }
}
