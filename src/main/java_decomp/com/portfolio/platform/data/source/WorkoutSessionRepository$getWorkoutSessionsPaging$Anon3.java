package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.gg6;
import com.fossil.vk4;
import com.fossil.xg6;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository$getWorkoutSessionsPaging$Anon3 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessionsPaging$Anon3(WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory) {
        super(0);
        this.$sourceFactory = workoutSessionDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        vk4 mHelper;
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = (WorkoutSessionLocalDataSource) this.$sourceFactory.getSourceLiveData().a();
        if (workoutSessionLocalDataSource != null && (mHelper = workoutSessionLocalDataSource.getMHelper()) != null) {
            mHelper.b();
        }
    }
}
