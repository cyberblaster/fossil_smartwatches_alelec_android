package com.portfolio.platform.data.source.local.inapp;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationRepository_Factory implements Factory<InAppNotificationRepository> {
    @DexIgnore
    public /* final */ Provider<InAppNotificationDao> mInAppNotificationDaoProvider;

    @DexIgnore
    public InAppNotificationRepository_Factory(Provider<InAppNotificationDao> provider) {
        this.mInAppNotificationDaoProvider = provider;
    }

    @DexIgnore
    public static InAppNotificationRepository_Factory create(Provider<InAppNotificationDao> provider) {
        return new InAppNotificationRepository_Factory(provider);
    }

    @DexIgnore
    public static InAppNotificationRepository newInAppNotificationRepository(InAppNotificationDao inAppNotificationDao) {
        return new InAppNotificationRepository(inAppNotificationDao);
    }

    @DexIgnore
    public static InAppNotificationRepository provideInstance(Provider<InAppNotificationDao> provider) {
        return new InAppNotificationRepository(provider.get());
    }

    @DexIgnore
    public InAppNotificationRepository get() {
        return provideInstance(this.mInAppNotificationDaoProvider);
    }
}
