package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.g72;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lk6;
import com.fossil.ll6;
import com.fossil.mc3;
import com.fossil.mc6;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1<TResult> implements mc3<Void> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ g72 $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GFitSleep $gFitSLeepData;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitSLeepDataList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $listInsertGFitSuccessFul$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSleepData$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2) {
                this.this$0 = anon1_Level2;
            }

            @DexIgnore
            public final void run() {
                this.this$0.this$0.this$0.getMThirdPartyDatabase().getGFitSleepDao().deleteListGFitSleep(this.this$0.this$0.$listInsertGFitSuccessFul$inlined);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1, xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d(ThirdPartyRepository.TAG, "Delete " + this.this$0.$gFitSLeepData + " after pushing GoogleFit successful!");
                this.this$0.this$0.getMThirdPartyDatabase().runInTransaction(new Anon1_Level3(this));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(GFitSleep gFitSleep, g72 g72, GoogleSignInAccount googleSignInAccount, hh6 hh6, ArrayList arrayList, int i, lk6 lk6, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$gFitSLeepData = gFitSleep;
        this.$device$inlined = g72;
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfLists$inlined = hh6;
        this.$listInsertGFitSuccessFul$inlined = arrayList;
        this.$sizeOfListsOfGFitSleepData$inlined = i;
        this.$continuation$inlined = lk6;
        this.this$0 = thirdPartyRepository;
        this.$gFitSLeepDataList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onSuccess(Void voidR) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "Insert " + this.$gFitSLeepData + " to GoogleFit successful!");
        hh6 hh6 = this.$countSizeOfLists$inlined;
        hh6.element = hh6.element + 1;
        this.$listInsertGFitSuccessFul$inlined.add(this.$gFitSLeepData);
        if (this.$countSizeOfLists$inlined.element >= this.$sizeOfListsOfGFitSleepData$inlined && this.$continuation$inlined.isActive()) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new Anon1_Level2(this, (xe6) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitSleepDataToGoogleFit");
            lk6 lk6 = this.$continuation$inlined;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
    }
}
