package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.WatchAppSettingRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideWatchAppSettingRemoteDataSourceFactory implements Factory<WatchAppSettingRemoteDataSource> {
    @DexIgnore
    public /* final */ RepositoriesModule module;
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> serviceV2Provider;

    @DexIgnore
    public RepositoriesModule_ProvideWatchAppSettingRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.serviceV2Provider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideWatchAppSettingRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideWatchAppSettingRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static WatchAppSettingRemoteDataSource provideInstance(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return proxyProvideWatchAppSettingRemoteDataSource(repositoriesModule, provider.get());
    }

    @DexIgnore
    public static WatchAppSettingRemoteDataSource proxyProvideWatchAppSettingRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        WatchAppSettingRemoteDataSource provideWatchAppSettingRemoteDataSource = repositoriesModule.provideWatchAppSettingRemoteDataSource(apiServiceV2);
        z76.a(provideWatchAppSettingRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideWatchAppSettingRemoteDataSource;
    }

    @DexIgnore
    public WatchAppSettingRemoteDataSource get() {
        return provideInstance(this.module, this.serviceV2Provider);
    }
}
