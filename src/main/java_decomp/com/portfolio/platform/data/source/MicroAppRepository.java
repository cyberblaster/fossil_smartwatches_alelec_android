package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.jm4;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.w24;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yj6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ MicroAppDao mMicroAppDao;
    @DexIgnore
    public /* final */ MicroAppRemoteDataSource mMicroAppRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppRepository(MicroAppDao microAppDao, MicroAppRemoteDataSource microAppRemoteDataSource, PortfolioApp portfolioApp) {
        wg6.b(microAppDao, "mMicroAppDao");
        wg6.b(microAppRemoteDataSource, "mMicroAppRemoteDataSource");
        wg6.b(portfolioApp, "mPortfolioApp");
        this.mMicroAppDao = microAppDao;
        this.mMicroAppRemoteDataSource = microAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppDao.clearAllDeclarationFileTable();
        this.mMicroAppDao.clearAllMicroAppGalleryTable();
        this.mMicroAppDao.clearAllMicroAppSettingTable();
        this.mMicroAppDao.clearAllMicroAppVariantTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadAllMicroApp(String str, xe6<? super ap4<List<MicroApp>>> xe6) {
        MicroAppRepository$downloadAllMicroApp$Anon1 microAppRepository$downloadAllMicroApp$Anon1;
        int i;
        MicroAppRepository microAppRepository;
        ap4 ap4;
        if (xe6 instanceof MicroAppRepository$downloadAllMicroApp$Anon1) {
            microAppRepository$downloadAllMicroApp$Anon1 = (MicroAppRepository$downloadAllMicroApp$Anon1) xe6;
            int i2 = microAppRepository$downloadAllMicroApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRepository$downloadAllMicroApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRepository$downloadAllMicroApp$Anon1.result;
                Object a = ff6.a();
                i = microAppRepository$downloadAllMicroApp$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "downloadAllMicroApp - serial=" + str);
                    MicroAppRemoteDataSource microAppRemoteDataSource = this.mMicroAppRemoteDataSource;
                    microAppRepository$downloadAllMicroApp$Anon1.L$0 = this;
                    microAppRepository$downloadAllMicroApp$Anon1.L$1 = str;
                    microAppRepository$downloadAllMicroApp$Anon1.label = 1;
                    obj = microAppRemoteDataSource.getAllMicroApp(str, microAppRepository$downloadAllMicroApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    microAppRepository = this;
                } else if (i == 1) {
                    str = (String) microAppRepository$downloadAllMicroApp$Anon1.L$1;
                    microAppRepository = (MicroAppRepository) microAppRepository$downloadAllMicroApp$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                Integer num = null;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadAllMicroApp - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    local2.d(str3, sb.toString());
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        List list = (List) a2;
                        List<MicroApp> listMicroApp = microAppRepository.mMicroAppDao.getListMicroApp(str);
                        if ((!cp4.b() && (!list.isEmpty())) || (!wg6.a((Object) listMicroApp, (Object) list))) {
                            ArrayList arrayList = new ArrayList();
                            for (Object next : list) {
                                if (hf6.a(!yj6.a((CharSequence) w24.y.t(), (CharSequence) ((MicroApp) next).getId(), false, 2, (Object) null)).booleanValue()) {
                                    arrayList.add(next);
                                }
                            }
                            microAppRepository.mMicroAppDao.clearAllMicroApp();
                            microAppRepository.mMicroAppDao.upsertListMicroApp(arrayList);
                        }
                        return new cp4(list, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadAllMicroApp - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverError=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str4, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        microAppRepository$downloadAllMicroApp$Anon1 = new MicroAppRepository$downloadAllMicroApp$Anon1(this, xe6);
        Object obj2 = microAppRepository$downloadAllMicroApp$Anon1.result;
        Object a3 = ff6.a();
        i = microAppRepository$downloadAllMicroApp$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        Integer num2 = null;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadMicroAppVariant(String str, String str2, String str3, xe6<? super ap4<List<MicroAppVariant>>> xe6) {
        MicroAppRepository$downloadMicroAppVariant$Anon1 microAppRepository$downloadMicroAppVariant$Anon1;
        int i;
        MicroAppRepository microAppRepository;
        ap4 ap4;
        if (xe6 instanceof MicroAppRepository$downloadMicroAppVariant$Anon1) {
            microAppRepository$downloadMicroAppVariant$Anon1 = (MicroAppRepository$downloadMicroAppVariant$Anon1) xe6;
            int i2 = microAppRepository$downloadMicroAppVariant$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRepository$downloadMicroAppVariant$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRepository$downloadMicroAppVariant$Anon1.result;
                Object a = ff6.a();
                i = microAppRepository$downloadMicroAppVariant$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "downloadMicroAppVariant - serial=" + str + " major " + str2 + " minor " + str3);
                    MicroAppRemoteDataSource microAppRemoteDataSource = this.mMicroAppRemoteDataSource;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$0 = this;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$1 = str;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$2 = str2;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$3 = str3;
                    microAppRepository$downloadMicroAppVariant$Anon1.label = 1;
                    obj = microAppRemoteDataSource.getAllMicroAppVariant(str, str2, str3, microAppRepository$downloadMicroAppVariant$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    microAppRepository = this;
                } else if (i == 1) {
                    String str5 = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$3;
                    String str6 = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$2;
                    str = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$1;
                    microAppRepository = (MicroAppRepository) microAppRepository$downloadMicroAppVariant$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                Integer num = null;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadMicroAppVariant - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    local2.d(str7, sb.toString());
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        List<MicroAppVariant> list = (List) a2;
                        if (!cp4.b()) {
                            ArrayList arrayList = new ArrayList();
                            for (MicroAppVariant declarationFileList : list) {
                                hf6.a(arrayList.addAll(declarationFileList.getDeclarationFileList()));
                            }
                            microAppRepository.mMicroAppDao.upsertMicroAppVariantList(list);
                            microAppRepository.mMicroAppDao.upsertDeclarationFileList(arrayList);
                        }
                        return new cp4(list, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadMicroAppVariant - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" serverError=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str8, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        microAppRepository$downloadMicroAppVariant$Anon1 = new MicroAppRepository$downloadMicroAppVariant$Anon1(this, xe6);
        Object obj2 = microAppRepository$downloadMicroAppVariant$Anon1.result;
        Object a3 = ff6.a();
        i = microAppRepository$downloadMicroAppVariant$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        Integer num2 = null;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final List<MicroApp> getAllMicroApp(String str) {
        wg6.b(str, "serialNumber");
        return this.mMicroAppDao.getListMicroApp(str);
    }

    @DexIgnore
    public final List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        wg6.b(list, "ids");
        wg6.b(str, "serialNumber");
        return this.mMicroAppDao.getMicroAppByIds(list, str);
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, String str3, int i) {
        wg6.b(str, "serialNumber");
        wg6.b(str2, "microAppId");
        wg6.b(str3, "variantName");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final List<MicroApp> queryMicroAppByName(String str, String str2) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        wg6.b(str2, "serialNumber");
        ArrayList arrayList = new ArrayList();
        for (MicroApp next : this.mMicroAppDao.getListMicroApp(str2)) {
            String normalize = Normalizer.normalize(jm4.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            wg6.a((Object) normalize, "name");
            wg6.a((Object) normalize2, "searchQuery");
            if (yj6.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        wg6.b(str, "serialNumber");
        wg6.b(str2, "microAppId");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }
}
