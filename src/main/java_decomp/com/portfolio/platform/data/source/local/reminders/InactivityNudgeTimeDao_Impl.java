package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InactivityNudgeTimeDao_Impl implements InactivityNudgeTimeDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<InactivityNudgeTimeModel> __insertionAdapterOfInactivityNudgeTimeModel;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<InactivityNudgeTimeModel> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inactivityNudgeTimeModel` (`nudgeTimeName`,`minutes`,`nudgeTimeType`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, InactivityNudgeTimeModel inactivityNudgeTimeModel) {
            if (inactivityNudgeTimeModel.getNudgeTimeName() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, inactivityNudgeTimeModel.getNudgeTimeName());
            }
            miVar.a(2, (long) inactivityNudgeTimeModel.getMinutes());
            miVar.a(3, (long) inactivityNudgeTimeModel.getNudgeTimeType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM inactivityNudgeTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<InactivityNudgeTimeModel> call() throws Exception {
            Cursor a = bi.a(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "nudgeTimeName");
                int b2 = ai.b(a, "minutes");
                int b3 = ai.b(a, "nudgeTimeType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new InactivityNudgeTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<InactivityNudgeTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public InactivityNudgeTimeModel call() throws Exception {
            InactivityNudgeTimeModel inactivityNudgeTimeModel = null;
            Cursor a = bi.a(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "nudgeTimeName");
                int b2 = ai.b(a, "minutes");
                int b3 = ai.b(a, "nudgeTimeType");
                if (a.moveToFirst()) {
                    inactivityNudgeTimeModel = new InactivityNudgeTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3));
                }
                return inactivityNudgeTimeModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public InactivityNudgeTimeDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfInactivityNudgeTimeModel = new Anon1(ohVar);
        this.__preparedStmtOfDelete = new Anon2(ohVar);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public InactivityNudgeTimeModel getInactivityNudgeTimeModelWithFieldNudgeTimeType(int i) {
        rh b = rh.b("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        b.a(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        InactivityNudgeTimeModel inactivityNudgeTimeModel = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "nudgeTimeName");
            int b3 = ai.b(a, "minutes");
            int b4 = ai.b(a, "nudgeTimeType");
            if (a.moveToFirst()) {
                inactivityNudgeTimeModel = new InactivityNudgeTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4));
            }
            return inactivityNudgeTimeModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<InactivityNudgeTimeModel> getInactivityNudgeTimeWithFieldNudgeTimeType(int i) {
        rh b = rh.b("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        b.a(1, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"inactivityNudgeTimeModel"}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<InactivityNudgeTimeModel>> getListInactivityNudgeTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"inactivityNudgeTimeModel"}, false, new Anon3(rh.b("SELECT * FROM inactivityNudgeTimeModel", 0)));
    }

    @DexIgnore
    public List<InactivityNudgeTimeModel> getListInactivityNudgeTimeModel() {
        rh b = rh.b("SELECT * FROM inactivityNudgeTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "nudgeTimeName");
            int b3 = ai.b(a, "minutes");
            int b4 = ai.b(a, "nudgeTimeType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new InactivityNudgeTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertInactivityNudgeTime(InactivityNudgeTimeModel inactivityNudgeTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(inactivityNudgeTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListInactivityNudgeTime(List<InactivityNudgeTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
