package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSleepStatistic$Anon1 extends tx5<SleepStatistic, SleepStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$getSleepStatistic$Anon1(SleepSummariesRepository sleepSummariesRepository, boolean z) {
        this.this$0 = sleepSummariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public Object createCall(xe6<? super rx6<SleepStatistic>> xe6) {
        return this.this$0.mApiService.getSleepStatistic(xe6);
    }

    @DexIgnore
    public LiveData<SleepStatistic> loadFromDb() {
        return this.this$0.mSleepDao.getSleepStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getSleepStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(SleepStatistic sleepStatistic) {
        wg6.b(sleepStatistic, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getSleepStatistic - saveCallResult -- item=" + sleepStatistic);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2(this, sleepStatistic, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(SleepStatistic sleepStatistic) {
        return this.$shouldFetch;
    }
}
