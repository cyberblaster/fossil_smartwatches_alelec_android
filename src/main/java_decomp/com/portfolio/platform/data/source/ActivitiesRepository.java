package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.ik4;
import com.fossil.ji;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.sd;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitiesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ActivitySampleDao mActivitySampleDao;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ ik4 mFitnessHelper;
    @DexIgnore
    public /* final */ SampleRawDao mSampleRawDao;
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitiesRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingActivitiesCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<ActivitySample> list);
    }

    /*
    static {
        String simpleName = ActivitiesRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "ActivitiesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitiesRepository(ApiServiceV2 apiServiceV2, SampleRawDao sampleRawDao, ActivitySampleDao activitySampleDao, FitnessDatabase fitnessDatabase, FitnessDataDao fitnessDataDao, UserRepository userRepository, ik4 ik4) {
        wg6.b(apiServiceV2, "mApiService");
        wg6.b(sampleRawDao, "mSampleRawDao");
        wg6.b(activitySampleDao, "mActivitySampleDao");
        wg6.b(fitnessDatabase, "mFitnessDatabase");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(ik4, "mFitnessHelper");
        this.mApiService = apiServiceV2;
        this.mSampleRawDao = sampleRawDao;
        this.mActivitySampleDao = activitySampleDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mFitnessDataDao = fitnessDataDao;
        this.mUserRepository = userRepository;
        this.mFitnessHelper = ik4;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchActivitySamples$default(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, xe6 xe6, int i3, Object obj) {
        return activitiesRepository.fetchActivitySamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        this.mActivitySampleDao.deleteAllActivitySamples();
        this.mSampleRawDao.deleteAllActivitySamples();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    public final Object fetchActivitySamples(Date date, Date date2, int i, int i2, xe6<? super ap4<ApiResponse<Activity>>> xe6) {
        ActivitiesRepository$fetchActivitySamples$Anon1 activitiesRepository$fetchActivitySamples$Anon1;
        int i3;
        ap4 ap4;
        ap4 ap42;
        Object obj;
        int i4;
        ActivitiesRepository activitiesRepository;
        Date date3;
        int i5;
        String message;
        Date date4 = date;
        Date date5 = date2;
        xe6<? super ap4<ApiResponse<Activity>>> xe62 = xe6;
        if (xe62 instanceof ActivitiesRepository$fetchActivitySamples$Anon1) {
            activitiesRepository$fetchActivitySamples$Anon1 = (ActivitiesRepository$fetchActivitySamples$Anon1) xe62;
            int i6 = activitiesRepository$fetchActivitySamples$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                activitiesRepository$fetchActivitySamples$Anon1.label = i6 - Integer.MIN_VALUE;
                ActivitiesRepository$fetchActivitySamples$Anon1 activitiesRepository$fetchActivitySamples$Anon12 = activitiesRepository$fetchActivitySamples$Anon1;
                Object obj2 = activitiesRepository$fetchActivitySamples$Anon12.result;
                Object a = ff6.a();
                i3 = activitiesRepository$fetchActivitySamples$Anon12.label;
                if (i3 != 0) {
                    nc6.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchActivitySamples: start = ");
                    sb.append(date4);
                    sb.append(", end = ");
                    sb.append(date5);
                    sb.append(" DBNAME ");
                    ji openHelper = this.mFitnessDatabase.getOpenHelper();
                    wg6.a((Object) openHelper, "mFitnessDatabase.openHelper");
                    sb.append(openHelper.getDatabaseName());
                    local.d(str, sb.toString());
                    ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1 activitiesRepository$fetchActivitySamples$repoResponse$Anon1 = new ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1(this, date, date2, i, i2, (xe6) null);
                    activitiesRepository$fetchActivitySamples$Anon12.L$0 = this;
                    activitiesRepository$fetchActivitySamples$Anon12.L$1 = date4;
                    activitiesRepository$fetchActivitySamples$Anon12.L$2 = date5;
                    i5 = i;
                    activitiesRepository$fetchActivitySamples$Anon12.I$0 = i5;
                    int i7 = i2;
                    activitiesRepository$fetchActivitySamples$Anon12.I$1 = i7;
                    activitiesRepository$fetchActivitySamples$Anon12.label = 1;
                    Object a2 = ResponseKt.a(activitiesRepository$fetchActivitySamples$repoResponse$Anon1, activitiesRepository$fetchActivitySamples$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    activitiesRepository = this;
                } else if (i3 == 1) {
                    int i8 = activitiesRepository$fetchActivitySamples$Anon12.I$1;
                    i5 = activitiesRepository$fetchActivitySamples$Anon12.I$0;
                    date3 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$2;
                    nc6.a(obj2);
                    i4 = i8;
                    date4 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$1;
                    activitiesRepository = (ActivitiesRepository) activitiesRepository$fetchActivitySamples$Anon12.L$0;
                } else if (i3 == 2) {
                    List list = (List) activitiesRepository$fetchActivitySamples$Anon12.L$4;
                    ap42 = (ap4) activitiesRepository$fetchActivitySamples$Anon12.L$3;
                    int i9 = activitiesRepository$fetchActivitySamples$Anon12.I$1;
                    int i10 = activitiesRepository$fetchActivitySamples$Anon12.I$0;
                    Date date6 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$2;
                    Date date7 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$1;
                    ActivitiesRepository activitiesRepository2 = (ActivitiesRepository) activitiesRepository$fetchActivitySamples$Anon12.L$0;
                    try {
                        nc6.a(obj2);
                        obj = obj2;
                        return (ap4) obj;
                    } catch (Exception e) {
                        e = e;
                        ap4 = ap42;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj2;
                if (!(ap4 instanceof cp4)) {
                    if (((cp4) ap4).a() != null) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            for (Activity activitySample : ((ApiResponse) ((cp4) ap4).a()).get_items()) {
                                arrayList.add(activitySample.toActivitySample());
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("fetchActivitySamples: DBNAME=");
                            ji openHelper2 = activitiesRepository.mFitnessDatabase.getOpenHelper();
                            wg6.a((Object) openHelper2, "mFitnessDatabase.openHelper");
                            sb2.append(openHelper2.getDatabaseName());
                            local2.d(str2, sb2.toString());
                            if (!((cp4) ap4).b()) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str3 = TAG;
                                local3.d(str3, "fetchActivitySamples: new activity sample=" + arrayList);
                                activitiesRepository.mActivitySampleDao.upsertListActivitySample(arrayList);
                            }
                            if (((ApiResponse) ((cp4) ap4).a()).get_range() == null) {
                                return ap4;
                            }
                            Range range = ((ApiResponse) ((cp4) ap4).a()).get_range();
                            if (range == null) {
                                wg6.a();
                                throw null;
                            } else if (!range.isHasNext()) {
                                return ap4;
                            } else {
                                activitiesRepository$fetchActivitySamples$Anon12.L$0 = activitiesRepository;
                                activitiesRepository$fetchActivitySamples$Anon12.L$1 = date4;
                                activitiesRepository$fetchActivitySamples$Anon12.L$2 = date3;
                                activitiesRepository$fetchActivitySamples$Anon12.I$0 = i5;
                                activitiesRepository$fetchActivitySamples$Anon12.I$1 = i4;
                                activitiesRepository$fetchActivitySamples$Anon12.L$3 = ap4;
                                activitiesRepository$fetchActivitySamples$Anon12.L$4 = arrayList;
                                activitiesRepository$fetchActivitySamples$Anon12.label = 2;
                                obj = activitiesRepository.fetchActivitySamples(date4, date3, i5 + i4, i4, activitiesRepository$fetchActivitySamples$Anon12);
                                if (obj == a) {
                                    return a;
                                }
                                ap42 = ap4;
                                return (ap4) obj;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String str4 = TAG;
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("fetchActivitySamples exception=");
                            e.printStackTrace();
                            sb3.append(cd6.a);
                            local4.d(str4, sb3.toString());
                            return ap4;
                        }
                    }
                    return ap4;
                }
                String str5 = null;
                if (ap4 instanceof zo4) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str6 = TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("fetchActivitySamples Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb4.append(zo4.a());
                    sb4.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str5 = c2.getUserMessage();
                        }
                    } else {
                        str5 = message;
                    }
                    if (str5 == null) {
                        str5 = "";
                    }
                    sb4.append(str5);
                    local5.d(str6, sb4.toString());
                }
                return ap4;
            }
        }
        activitiesRepository$fetchActivitySamples$Anon1 = new ActivitiesRepository$fetchActivitySamples$Anon1(this, xe62);
        ActivitiesRepository$fetchActivitySamples$Anon1 activitiesRepository$fetchActivitySamples$Anon122 = activitiesRepository$fetchActivitySamples$Anon1;
        Object obj22 = activitiesRepository$fetchActivitySamples$Anon122.result;
        Object a3 = ff6.a();
        i3 = activitiesRepository$fetchActivitySamples$Anon122.label;
        if (i3 != 0) {
        }
        ap4 = (ap4) obj22;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final LiveData<yx5<List<ActivitySample>>> getActivityList(Date date, Date date2, boolean z) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getActivityList: start = " + date + ", end = " + date2 + " shouldFetch=" + z);
        Date o = bk4.o(date);
        Date j = bk4.j(date2);
        FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
        wg6.a((Object) o, "startDate");
        wg6.a((Object) j, "endDate");
        LiveData<yx5<List<ActivitySample>>> b = sd.b(fitnessDataDao.getFitnessDataLiveData(o, j), new ActivitiesRepository$getActivityList$Anon1(this, o, j, z, date2));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final List<SampleRaw> getActivityListByUAPinType(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getActivityListByUAPinType: uaPinType = " + i);
        return this.mSampleRawDao.getListActivitySampleByUaType(i);
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> getActivitySamplesInDate$app_fossilRelease(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendarStart");
        instance.setTime(date);
        bk4.d(instance);
        Object clone = instance.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            bk4.a(calendar);
            wg6.a((Object) calendar, "DateHelper.getEndOfDay(calendarEnd)");
            ActivitySampleDao activitySampleDao = this.mActivitySampleDao;
            Date time = instance.getTime();
            wg6.a((Object) time, "calendarStart.time");
            Date time2 = calendar.getTime();
            wg6.a((Object) time2, "calendarEnd.time");
            return activitySampleDao.getActivitySamplesLiveData(time, time2);
        }
        throw new rc6("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final List<SampleRaw> getPendingActivities(Date date, Date date2) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "start");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, "end");
        instance2.setTime(date2);
        SampleRawDao sampleRawDao = this.mSampleRawDao;
        Date o = bk4.o(instance.getTime());
        wg6.a((Object) o, "DateHelper.getStartOfDay(start.time)");
        Date j = bk4.j(instance2.getTime());
        wg6.a((Object) j, "DateHelper.getEndOfDay(end.time)");
        return sampleRawDao.getPendingActivitySamples(o, j);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object insert(List<SampleRaw> list, xe6<? super ap4<List<ActivitySample>>> xe6) {
        ActivitiesRepository$insert$Anon1 activitiesRepository$insert$Anon1;
        int i;
        ap4 ap4;
        List<Activity> list2;
        if (xe6 instanceof ActivitiesRepository$insert$Anon1) {
            activitiesRepository$insert$Anon1 = (ActivitiesRepository$insert$Anon1) xe6;
            int i2 = activitiesRepository$insert$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                activitiesRepository$insert$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = activitiesRepository$insert$Anon1.result;
                Object a = ff6.a();
                i = activitiesRepository$insert$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "insertActivities: sampleRawList =" + list.size());
                    MFUser currentUser = this.mUserRepository.getCurrentUser();
                    String userId = currentUser != null ? currentUser.getUserId() : null;
                    if (TextUtils.isEmpty(userId)) {
                        return new zo4(600, new ServerError(600, ""), (Throwable) null, (String) null, 8, (qg6) null);
                    }
                    fu3 fu3 = new fu3();
                    Gson gsonConverter = Activity.Companion.gsonConverter();
                    for (SampleRaw next : list) {
                        Activity.Companion companion = Activity.Companion;
                        if (userId != null) {
                            Activity activity = companion.toActivity(userId, next.toActivitySample());
                            JsonElement b = gsonConverter.b(activity);
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            local2.d(str3, "activity " + activity + " json " + b);
                            wg6.a((Object) b, "jsonTree");
                            if (!b.h()) {
                                fu3.a(b);
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    ku3 ku3 = new ku3();
                    ku3.a(CloudLogWriter.ITEMS_PARAM, fu3);
                    ActivitiesRepository$insert$repoResponse$Anon1 activitiesRepository$insert$repoResponse$Anon1 = new ActivitiesRepository$insert$repoResponse$Anon1(this, ku3, (xe6) null);
                    activitiesRepository$insert$Anon1.L$0 = this;
                    activitiesRepository$insert$Anon1.L$1 = list;
                    activitiesRepository$insert$Anon1.L$2 = userId;
                    activitiesRepository$insert$Anon1.L$3 = fu3;
                    activitiesRepository$insert$Anon1.L$4 = gsonConverter;
                    activitiesRepository$insert$Anon1.L$5 = ku3;
                    activitiesRepository$insert$Anon1.label = 1;
                    obj = ResponseKt.a(activitiesRepository$insert$repoResponse$Anon1, activitiesRepository$insert$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) activitiesRepository$insert$Anon1.L$5;
                    Gson gson = (Gson) activitiesRepository$insert$Anon1.L$4;
                    fu3 fu32 = (fu3) activitiesRepository$insert$Anon1.L$3;
                    String str4 = (String) activitiesRepository$insert$Anon1.L$2;
                    List list3 = (List) activitiesRepository$insert$Anon1.L$1;
                    ActivitiesRepository activitiesRepository = (ActivitiesRepository) activitiesRepository$insert$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    local3.d(str5, "insertActivity onResponse: response = " + ap4);
                    ArrayList arrayList = new ArrayList();
                    ApiResponse apiResponse = (ApiResponse) ((cp4) ap4).a();
                    if (!(apiResponse == null || (list2 = apiResponse.get_items()) == null)) {
                        for (Activity activitySample : list2) {
                            arrayList.add(activitySample.toActivitySample());
                        }
                    }
                    return new cp4(arrayList, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str6 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("insertActivity Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local4.d(str6, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        activitiesRepository$insert$Anon1 = new ActivitiesRepository$insert$Anon1(this, xe6);
        Object obj2 = activitiesRepository$insert$Anon1.result;
        Object a2 = ff6.a();
        i = activitiesRepository$insert$Anon1.label;
        String str7 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final void insertFromDevice(List<ActivitySample> list) {
        wg6.b(list, "activityList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: activityList = " + list.size());
        this.mActivitySampleDao.insertActivitySamples(list);
    }

    @DexIgnore
    public final Object pushPendingActivities(PushPendingActivitiesCallback pushPendingActivitiesCallback, xe6<? super cd6> xe6) {
        cd6 cd6;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushPendingActivities fitnessDb=" + this.mFitnessDatabase);
        List<SampleRaw> pendingActivitySamples = this.mSampleRawDao.getPendingActivitySamples();
        if (pendingActivitySamples.size() > 0) {
            Object saveActivitiesToServer = saveActivitiesToServer(pendingActivitySamples, pushPendingActivitiesCallback, xe6);
            if (saveActivitiesToServer == ff6.a()) {
                return saveActivitiesToServer;
            }
        } else {
            if (pushPendingActivitiesCallback != null) {
                pushPendingActivitiesCallback.onFail(MFNetworkReturnCode.NOT_FOUND);
                cd6 = cd6.a;
            } else {
                cd6 = null;
            }
            if (cd6 == ff6.a()) {
                return cd6;
            }
        }
        return cd6.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a0, code lost:
        if (r13.intValue() != 409000) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01b6, code lost:
        if (r7.intValue() != 409001) goto L_0x01b8;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x021d A[LOOP:1: B:69:0x0217->B:71:0x021d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0240  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final /* synthetic */ Object saveActivitiesToServer(List<SampleRaw> list, PushPendingActivitiesCallback pushPendingActivitiesCallback, xe6<? super cd6> xe6) {
        ActivitiesRepository activitiesRepository;
        ActivitiesRepository$saveActivitiesToServer$Anon1 activitiesRepository$saveActivitiesToServer$Anon1;
        int i;
        int i2;
        Object obj;
        Object obj2;
        List<SampleRaw> list2;
        ActivitiesRepository$saveActivitiesToServer$Anon1 activitiesRepository$saveActivitiesToServer$Anon12;
        int i3;
        List list3;
        int i4;
        List<SampleRaw> list4;
        PushPendingActivitiesCallback pushPendingActivitiesCallback2;
        ActivitiesRepository activitiesRepository2;
        ap4 ap4;
        ActivitiesRepository$saveActivitiesToServer$Anon1 activitiesRepository$saveActivitiesToServer$Anon13;
        List<SampleRaw> list5;
        Object obj3;
        ArrayList arrayList;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof ActivitiesRepository$saveActivitiesToServer$Anon1) {
            activitiesRepository$saveActivitiesToServer$Anon1 = (ActivitiesRepository$saveActivitiesToServer$Anon1) xe62;
            int i5 = activitiesRepository$saveActivitiesToServer$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                activitiesRepository$saveActivitiesToServer$Anon1.label = i5 - Integer.MIN_VALUE;
                activitiesRepository = this;
                Object obj4 = activitiesRepository$saveActivitiesToServer$Anon1.result;
                Object a = ff6.a();
                i = activitiesRepository$saveActivitiesToServer$Anon1.label;
                i2 = 1;
                if (i != 0) {
                    nc6.a(obj4);
                    list3 = new ArrayList();
                    activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon1;
                    activitiesRepository2 = activitiesRepository;
                    obj3 = a;
                    i3 = 0;
                    list5 = list;
                    pushPendingActivitiesCallback2 = pushPendingActivitiesCallback;
                } else if (i == 1) {
                    list4 = (List) activitiesRepository$saveActivitiesToServer$Anon1.L$4;
                    i4 = activitiesRepository$saveActivitiesToServer$Anon1.I$1;
                    list3 = (List) activitiesRepository$saveActivitiesToServer$Anon1.L$3;
                    i3 = activitiesRepository$saveActivitiesToServer$Anon1.I$0;
                    list2 = (List) activitiesRepository$saveActivitiesToServer$Anon1.L$1;
                    nc6.a(obj4);
                    obj = a;
                    pushPendingActivitiesCallback2 = (PushPendingActivitiesCallback) activitiesRepository$saveActivitiesToServer$Anon1.L$2;
                    activitiesRepository$saveActivitiesToServer$Anon12 = activitiesRepository$saveActivitiesToServer$Anon1;
                    activitiesRepository2 = (ActivitiesRepository) activitiesRepository$saveActivitiesToServer$Anon1.L$0;
                    obj2 = obj4;
                    ap4 = (ap4) obj2;
                    i3 += 100;
                    if (ap4 instanceof cp4) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.d(str, "saveActivitiesToServer success, bravo!!! startIndex=" + i3 + " endIndex=" + i4);
                        Object a2 = ((cp4) ap4).a();
                        if (a2 != null) {
                            activitiesRepository2.updateActivityPinType(list4, 0);
                            list3.addAll((List) a2);
                            if (i3 >= list2.size()) {
                                if (pushPendingActivitiesCallback2 != null) {
                                    pushPendingActivitiesCallback2.onSuccess(list3);
                                }
                                return cd6.a;
                            }
                        }
                        wg6.a();
                        throw null;
                    } else if (ap4 instanceof zo4) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("saveActivitiesToServer failed, errorCode=");
                        zo4 zo4 = (zo4) ap4;
                        sb.append(zo4.a());
                        sb.append(' ');
                        sb.append("startIndex=");
                        sb.append(i3);
                        sb.append(" endIndex=");
                        sb.append(i4);
                        local2.d(str2, sb.toString());
                        if (zo4.a() == 422) {
                            arrayList = new ArrayList();
                            if (!TextUtils.isEmpty(zo4.b())) {
                                try {
                                } catch (Exception e) {
                                    e = e;
                                }
                                Object a3 = new Gson().a(((zo4) ap4).b(), new ActivitiesRepository$saveActivitiesToServer$type$Anon1().getType());
                                wg6.a(a3, "Gson().fromJson(repoResponse.errorItems, type)");
                                List list6 = ((UpsertApiResponse) a3).get_items();
                                if (!list6.isEmpty()) {
                                    int size = list6.size();
                                    int i6 = 0;
                                    while (i6 < size) {
                                        Integer code = ((Activity) list6.get(i6)).getCode();
                                        if (code == null) {
                                        }
                                        Integer code2 = ((Activity) list6.get(i6)).getCode();
                                        if (code2 == null) {
                                        }
                                        if (!TextUtils.isEmpty(((Activity) list6.get(i6)).getId())) {
                                            SampleRaw sampleRaw = list4.get(i6);
                                            try {
                                                sampleRaw.setPinType(0);
                                                arrayList.add(sampleRaw);
                                            } catch (Exception e2) {
                                                e = e2;
                                            }
                                            i6++;
                                        } else {
                                            i6++;
                                        }
                                    }
                                }
                                activitiesRepository2.mSampleRawDao.upsertListActivitySample(arrayList);
                                ArrayList arrayList2 = new ArrayList(rd6.a(list4, 10));
                                for (SampleRaw activitySample : list4) {
                                    arrayList2.add(activitySample.toActivitySample());
                                }
                                list3.addAll(arrayList2);
                                if (i3 >= list2.size()) {
                                    if (pushPendingActivitiesCallback2 != null) {
                                        pushPendingActivitiesCallback2.onSuccess(list3);
                                    }
                                    return cd6.a;
                                }
                            }
                        }
                        if (i3 >= list2.size()) {
                            if (pushPendingActivitiesCallback2 != null) {
                                pushPendingActivitiesCallback2.onFail(zo4.a());
                            }
                            return cd6.a;
                        }
                    }
                    i2 = 1;
                    activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon12;
                    list5 = list2;
                    obj3 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (i3 < list5.size()) {
                    int i7 = i3 + 100;
                    if (i7 > list5.size()) {
                        i7 = list5.size();
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "saveActivitiesToServer startIndex=" + i3 + " endIndex=" + i7);
                    List<SampleRaw> subList = list5.subList(i3, i7);
                    activitiesRepository$saveActivitiesToServer$Anon13.L$0 = activitiesRepository2;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$1 = list5;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$2 = pushPendingActivitiesCallback2;
                    activitiesRepository$saveActivitiesToServer$Anon13.I$0 = i3;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$3 = list3;
                    activitiesRepository$saveActivitiesToServer$Anon13.I$1 = i7;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$4 = subList;
                    activitiesRepository$saveActivitiesToServer$Anon13.label = i2;
                    obj2 = activitiesRepository2.insert(subList, activitiesRepository$saveActivitiesToServer$Anon13);
                    if (obj2 == obj3) {
                        return obj3;
                    }
                    obj = obj3;
                    i4 = i7;
                    activitiesRepository$saveActivitiesToServer$Anon12 = activitiesRepository$saveActivitiesToServer$Anon13;
                    list4 = subList;
                    list2 = list5;
                    ap4 = (ap4) obj2;
                    i3 += 100;
                    if (ap4 instanceof cp4) {
                    }
                    i2 = 1;
                    activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon12;
                    list5 = list2;
                    obj3 = obj;
                    if (i3 < list5.size()) {
                    }
                    return obj3;
                }
                return cd6.a;
            }
        }
        activitiesRepository = this;
        activitiesRepository$saveActivitiesToServer$Anon1 = new ActivitiesRepository$saveActivitiesToServer$Anon1(activitiesRepository, xe62);
        Object obj42 = activitiesRepository$saveActivitiesToServer$Anon1.result;
        Object a4 = ff6.a();
        i = activitiesRepository$saveActivitiesToServer$Anon1.label;
        i2 = 1;
        if (i != 0) {
        }
        if (i3 < list5.size()) {
        }
        return cd6.a;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("saveActivitiesToServer ex=");
        e.printStackTrace();
        sb2.append(cd6.a);
        local4.d(str4, sb2.toString());
        activitiesRepository2.mSampleRawDao.upsertListActivitySample(arrayList);
        ArrayList arrayList22 = new ArrayList(rd6.a(list4, 10));
        while (r2.hasNext()) {
        }
        list3.addAll(arrayList22);
        if (i3 >= list2.size()) {
        }
        if (i3 >= list2.size()) {
        }
        i2 = 1;
        activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon12;
        list5 = list2;
        obj3 = obj;
        if (i3 < list5.size()) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final void updateActivityPinType(List<SampleRaw> list, int i) {
        wg6.b(list, "activitySampleList");
        for (SampleRaw pinType : list) {
            pinType.setPinType(i);
        }
        this.mSampleRawDao.upsertListActivitySample(list);
    }

    @DexIgnore
    public final void updateActivityUAPinType(List<SampleRaw> list, int i) {
        wg6.b(list, "activityList");
        for (SampleRaw pinType : list) {
            pinType.setPinType(i);
        }
        this.mSampleRawDao.upsertListActivitySample(list);
    }
}
