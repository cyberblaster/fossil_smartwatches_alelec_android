package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingRepository_Factory implements Factory<MicroAppLastSettingRepository> {
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingDao> mMicroAppLastSettingDaoProvider;

    @DexIgnore
    public MicroAppLastSettingRepository_Factory(Provider<MicroAppLastSettingDao> provider) {
        this.mMicroAppLastSettingDaoProvider = provider;
    }

    @DexIgnore
    public static MicroAppLastSettingRepository_Factory create(Provider<MicroAppLastSettingDao> provider) {
        return new MicroAppLastSettingRepository_Factory(provider);
    }

    @DexIgnore
    public static MicroAppLastSettingRepository newMicroAppLastSettingRepository(MicroAppLastSettingDao microAppLastSettingDao) {
        return new MicroAppLastSettingRepository(microAppLastSettingDao);
    }

    @DexIgnore
    public static MicroAppLastSettingRepository provideInstance(Provider<MicroAppLastSettingDao> provider) {
        return new MicroAppLastSettingRepository(provider.get());
    }

    @DexIgnore
    public MicroAppLastSettingRepository get() {
        return provideInstance(this.mMicroAppLastSettingDaoProvider);
    }
}
