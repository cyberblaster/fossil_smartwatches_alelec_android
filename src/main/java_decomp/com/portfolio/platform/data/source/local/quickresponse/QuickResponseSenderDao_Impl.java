package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.QuickResponseSender;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseSenderDao_Impl extends QuickResponseSenderDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<QuickResponseSender> __insertionAdapterOfQuickResponseSender;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<QuickResponseSender> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseSender` (`id`,`content`) VALUES (nullif(?, 0),?)";
        }

        @DexIgnore
        public void bind(mi miVar, QuickResponseSender quickResponseSender) {
            miVar.a(1, (long) quickResponseSender.getId());
            if (quickResponseSender.getContent() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, quickResponseSender.getContent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM quickResponseSender WHERE id = ?";
        }
    }

    @DexIgnore
    public QuickResponseSenderDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfQuickResponseSender = new Anon1(ohVar);
        this.__preparedStmtOfRemoveById = new Anon2(ohVar);
    }

    @DexIgnore
    public List<QuickResponseSender> getAllQuickResponseSender() {
        rh b = rh.b("SELECT * FROM quickResponseSender", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                QuickResponseSender quickResponseSender = new QuickResponseSender(a.getString(b3));
                quickResponseSender.setId(a.getInt(b2));
                arrayList.add(quickResponseSender);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public QuickResponseSender getQuickResponseSenderById(int i) {
        rh b = rh.b("SELECT * FROM quickResponseSender WHERE id = ?", 1);
        b.a(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        QuickResponseSender quickResponseSender = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "content");
            if (a.moveToFirst()) {
                QuickResponseSender quickResponseSender2 = new QuickResponseSender(a.getString(b3));
                quickResponseSender2.setId(a.getInt(b2));
                quickResponseSender = quickResponseSender2;
            }
            return quickResponseSender;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public long insertQuickResponseSender(QuickResponseSender quickResponseSender) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfQuickResponseSender.insertAndReturnId(quickResponseSender);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.a(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }
}
