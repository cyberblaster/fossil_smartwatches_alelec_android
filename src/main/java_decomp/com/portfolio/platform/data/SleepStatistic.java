package com.portfolio.platform.data;

import com.fossil.d;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepStatistic {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sleep_statistic";
    @DexIgnore
    @vu3("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @vu3("id")
    public /* final */ String id;
    @DexIgnore
    @vu3("sleepTimeBestDay")
    public /* final */ SleepDailyBest sleepTimeBestDay;
    @DexIgnore
    @vu3("sleepTimeBestStreak")
    public /* final */ SleepDailyBest sleepTimeBestStreak;
    @DexIgnore
    @vu3("totalDays")
    public /* final */ int totalDays;
    @DexIgnore
    @vu3("totalSleepMinutes")
    public /* final */ int totalSleepMinutes;
    @DexIgnore
    @vu3("totalSleepStateDistInMinute")
    public /* final */ List<Integer> totalSleepStateDistInMinute;
    @DexIgnore
    @vu3("totalSleeps")
    public /* final */ int totalSleeps;
    @DexIgnore
    @vu3("uid")
    public /* final */ String uid;
    @DexIgnore
    @vu3("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SleepDailyBest {
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ String sleepDailySummaryId;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public SleepDailyBest(String str, Date date2, int i) {
            wg6.b(str, "sleepDailySummaryId");
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            this.sleepDailySummaryId = str;
            this.date = date2;
            this.value = i;
        }

        @DexIgnore
        public static /* synthetic */ SleepDailyBest copy$default(SleepDailyBest sleepDailyBest, String str, Date date2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = sleepDailyBest.sleepDailySummaryId;
            }
            if ((i2 & 2) != 0) {
                date2 = sleepDailyBest.date;
            }
            if ((i2 & 4) != 0) {
                i = sleepDailyBest.value;
            }
            return sleepDailyBest.copy(str, date2, i);
        }

        @DexIgnore
        public final String component1() {
            return this.sleepDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final int component3() {
            return this.value;
        }

        @DexIgnore
        public final SleepDailyBest copy(String str, Date date2, int i) {
            wg6.b(str, "sleepDailySummaryId");
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            return new SleepDailyBest(str, date2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SleepDailyBest)) {
                return false;
            }
            SleepDailyBest sleepDailyBest = (SleepDailyBest) obj;
            return wg6.a((Object) this.sleepDailySummaryId, (Object) sleepDailyBest.sleepDailySummaryId) && wg6.a((Object) this.date, (Object) sleepDailyBest.date) && this.value == sleepDailyBest.value;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getSleepDailySummaryId() {
            return this.sleepDailySummaryId;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.sleepDailySummaryId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return ((hashCode + i) * 31) + d.a(this.value);
        }

        @DexIgnore
        public String toString() {
            return "SleepDailyBest(sleepDailySummaryId=" + this.sleepDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore
    public SleepStatistic(String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        wg6.b(str, "id");
        wg6.b(str2, "uid");
        wg6.b(list, "totalSleepStateDistInMinute");
        wg6.b(dateTime, "createdAt");
        wg6.b(dateTime2, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.sleepTimeBestDay = sleepDailyBest;
        this.sleepTimeBestStreak = sleepDailyBest2;
        this.totalDays = i;
        this.totalSleeps = i2;
        this.totalSleepMinutes = i3;
        this.totalSleepStateDistInMinute = list;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ SleepStatistic copy$default(SleepStatistic sleepStatistic, String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List list, DateTime dateTime, DateTime dateTime2, int i4, Object obj) {
        SleepStatistic sleepStatistic2 = sleepStatistic;
        int i5 = i4;
        return sleepStatistic.copy((i5 & 1) != 0 ? sleepStatistic2.id : str, (i5 & 2) != 0 ? sleepStatistic2.uid : str2, (i5 & 4) != 0 ? sleepStatistic2.sleepTimeBestDay : sleepDailyBest, (i5 & 8) != 0 ? sleepStatistic2.sleepTimeBestStreak : sleepDailyBest2, (i5 & 16) != 0 ? sleepStatistic2.totalDays : i, (i5 & 32) != 0 ? sleepStatistic2.totalSleeps : i2, (i5 & 64) != 0 ? sleepStatistic2.totalSleepMinutes : i3, (i5 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? sleepStatistic2.totalSleepStateDistInMinute : list, (i5 & 256) != 0 ? sleepStatistic2.createdAt : dateTime, (i5 & 512) != 0 ? sleepStatistic2.updatedAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component10() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component2() {
        return this.uid;
    }

    @DexIgnore
    public final SleepDailyBest component3() {
        return this.sleepTimeBestDay;
    }

    @DexIgnore
    public final SleepDailyBest component4() {
        return this.sleepTimeBestStreak;
    }

    @DexIgnore
    public final int component5() {
        return this.totalDays;
    }

    @DexIgnore
    public final int component6() {
        return this.totalSleeps;
    }

    @DexIgnore
    public final int component7() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final List<Integer> component8() {
        return this.totalSleepStateDistInMinute;
    }

    @DexIgnore
    public final DateTime component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final SleepStatistic copy(String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        wg6.b(str, "id");
        wg6.b(str2, "uid");
        List<Integer> list2 = list;
        wg6.b(list2, "totalSleepStateDistInMinute");
        DateTime dateTime3 = dateTime;
        wg6.b(dateTime3, "createdAt");
        DateTime dateTime4 = dateTime2;
        wg6.b(dateTime4, "updatedAt");
        return new SleepStatistic(str, str2, sleepDailyBest, sleepDailyBest2, i, i2, i3, list2, dateTime3, dateTime4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepStatistic)) {
            return false;
        }
        SleepStatistic sleepStatistic = (SleepStatistic) obj;
        return wg6.a((Object) this.id, (Object) sleepStatistic.id) && wg6.a((Object) this.uid, (Object) sleepStatistic.uid) && wg6.a((Object) this.sleepTimeBestDay, (Object) sleepStatistic.sleepTimeBestDay) && wg6.a((Object) this.sleepTimeBestStreak, (Object) sleepStatistic.sleepTimeBestStreak) && this.totalDays == sleepStatistic.totalDays && this.totalSleeps == sleepStatistic.totalSleeps && this.totalSleepMinutes == sleepStatistic.totalSleepMinutes && wg6.a((Object) this.totalSleepStateDistInMinute, (Object) sleepStatistic.totalSleepStateDistInMinute) && wg6.a((Object) this.createdAt, (Object) sleepStatistic.createdAt) && wg6.a((Object) this.updatedAt, (Object) sleepStatistic.updatedAt);
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final SleepDailyBest getSleepTimeBestDay() {
        return this.sleepTimeBestDay;
    }

    @DexIgnore
    public final SleepDailyBest getSleepTimeBestStreak() {
        return this.sleepTimeBestStreak;
    }

    @DexIgnore
    public final int getTotalDays() {
        return this.totalDays;
    }

    @DexIgnore
    public final int getTotalSleepMinutes() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final List<Integer> getTotalSleepStateDistInMinute() {
        return this.totalSleepStateDistInMinute;
    }

    @DexIgnore
    public final int getTotalSleeps() {
        return this.totalSleeps;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.uid;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        SleepDailyBest sleepDailyBest = this.sleepTimeBestDay;
        int hashCode3 = (hashCode2 + (sleepDailyBest != null ? sleepDailyBest.hashCode() : 0)) * 31;
        SleepDailyBest sleepDailyBest2 = this.sleepTimeBestStreak;
        int hashCode4 = (((((((hashCode3 + (sleepDailyBest2 != null ? sleepDailyBest2.hashCode() : 0)) * 31) + d.a(this.totalDays)) * 31) + d.a(this.totalSleeps)) * 31) + d.a(this.totalSleepMinutes)) * 31;
        List<Integer> list = this.totalSleepStateDistInMinute;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        DateTime dateTime = this.createdAt;
        int hashCode6 = (hashCode5 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public String toString() {
        return "SleepStatistic(id=" + this.id + ", uid=" + this.uid + ", sleepTimeBestDay=" + this.sleepTimeBestDay + ", sleepTimeBestStreak=" + this.sleepTimeBestStreak + ", totalDays=" + this.totalDays + ", totalSleeps=" + this.totalSleeps + ", totalSleepMinutes=" + this.totalSleepMinutes + ", totalSleepStateDistInMinute=" + this.totalSleepStateDistInMinute + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
