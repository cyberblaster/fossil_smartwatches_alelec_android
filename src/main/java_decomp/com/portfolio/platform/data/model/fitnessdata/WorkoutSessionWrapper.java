package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.d;
import com.fossil.di4;
import com.fossil.fi4;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.fitness.WorkoutStateChange;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionWrapper {
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public int duration;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public int id;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChangeWrapper> stateChanges;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public int type;

    @DexIgnore
    public WorkoutSessionWrapper(int i, DateTime dateTime, DateTime dateTime2, int i2, int i3, int i4, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        wg6.b(dateTime, "startTime");
        wg6.b(dateTime2, "endTime");
        wg6.b(stepWrapper, "step");
        wg6.b(calorieWrapper, "calorie");
        wg6.b(distanceWrapper, "distance");
        this.id = i;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.timezoneOffsetInSecond = i2;
        this.duration = i3;
        this.type = i4;
        this.step = stepWrapper;
        this.calorie = calorieWrapper;
        this.distance = distanceWrapper;
        this.heartRate = heartRateWrapper;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSessionWrapper copy$default(WorkoutSessionWrapper workoutSessionWrapper, int i, DateTime dateTime, DateTime dateTime2, int i2, int i3, int i4, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper, int i5, Object obj) {
        WorkoutSessionWrapper workoutSessionWrapper2 = workoutSessionWrapper;
        int i6 = i5;
        return workoutSessionWrapper.copy((i6 & 1) != 0 ? workoutSessionWrapper2.id : i, (i6 & 2) != 0 ? workoutSessionWrapper2.startTime : dateTime, (i6 & 4) != 0 ? workoutSessionWrapper2.endTime : dateTime2, (i6 & 8) != 0 ? workoutSessionWrapper2.timezoneOffsetInSecond : i2, (i6 & 16) != 0 ? workoutSessionWrapper2.duration : i3, (i6 & 32) != 0 ? workoutSessionWrapper2.type : i4, (i6 & 64) != 0 ? workoutSessionWrapper2.step : stepWrapper, (i6 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? workoutSessionWrapper2.calorie : calorieWrapper, (i6 & 256) != 0 ? workoutSessionWrapper2.distance : distanceWrapper, (i6 & 512) != 0 ? workoutSessionWrapper2.heartRate : heartRateWrapper);
    }

    @DexIgnore
    public final int component1() {
        return this.id;
    }

    @DexIgnore
    public final HeartRateWrapper component10() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.endTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component5() {
        return this.duration;
    }

    @DexIgnore
    public final int component6() {
        return this.type;
    }

    @DexIgnore
    public final StepWrapper component7() {
        return this.step;
    }

    @DexIgnore
    public final CalorieWrapper component8() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper component9() {
        return this.distance;
    }

    @DexIgnore
    public final WorkoutSessionWrapper copy(int i, DateTime dateTime, DateTime dateTime2, int i2, int i3, int i4, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        wg6.b(dateTime, "startTime");
        wg6.b(dateTime2, "endTime");
        StepWrapper stepWrapper2 = stepWrapper;
        wg6.b(stepWrapper2, "step");
        CalorieWrapper calorieWrapper2 = calorieWrapper;
        wg6.b(calorieWrapper2, "calorie");
        DistanceWrapper distanceWrapper2 = distanceWrapper;
        wg6.b(distanceWrapper2, "distance");
        return new WorkoutSessionWrapper(i, dateTime, dateTime2, i2, i3, i4, stepWrapper2, calorieWrapper2, distanceWrapper2, heartRateWrapper);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutSessionWrapper)) {
            return false;
        }
        WorkoutSessionWrapper workoutSessionWrapper = (WorkoutSessionWrapper) obj;
        return this.id == workoutSessionWrapper.id && wg6.a((Object) this.startTime, (Object) workoutSessionWrapper.startTime) && wg6.a((Object) this.endTime, (Object) workoutSessionWrapper.endTime) && this.timezoneOffsetInSecond == workoutSessionWrapper.timezoneOffsetInSecond && this.duration == workoutSessionWrapper.duration && this.type == workoutSessionWrapper.type && wg6.a((Object) this.step, (Object) workoutSessionWrapper.step) && wg6.a((Object) this.calorie, (Object) workoutSessionWrapper.calorie) && wg6.a((Object) this.distance, (Object) workoutSessionWrapper.distance) && wg6.a((Object) this.heartRate, (Object) workoutSessionWrapper.heartRate);
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.id) * 31;
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (a + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (((((((hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + d.a(this.timezoneOffsetInSecond)) * 31) + d.a(this.duration)) * 31) + d.a(this.type)) * 31;
        StepWrapper stepWrapper = this.step;
        int hashCode3 = (hashCode2 + (stepWrapper != null ? stepWrapper.hashCode() : 0)) * 31;
        CalorieWrapper calorieWrapper = this.calorie;
        int hashCode4 = (hashCode3 + (calorieWrapper != null ? calorieWrapper.hashCode() : 0)) * 31;
        DistanceWrapper distanceWrapper = this.distance;
        int hashCode5 = (hashCode4 + (distanceWrapper != null ? distanceWrapper.hashCode() : 0)) * 31;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        wg6.b(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        wg6.b(distanceWrapper, "<set-?>");
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setDuration(int i) {
        this.duration = i;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStateChanges(List<WorkoutStateChangeWrapper> list) {
        wg6.b(list, "<set-?>");
        this.stateChanges = list;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        wg6.b(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSessionWrapper(id=" + this.id + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", type=" + this.type + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WorkoutSessionWrapper(WorkoutSession workoutSession) {
        this(r2, r3, r0, r5, r6, r7, r8, r9, r10, r11);
        HeartRateWrapper heartRateWrapper;
        wg6.b(workoutSession, "workoutSession");
        int id2 = workoutSession.getId();
        DateTime dateTime = new DateTime(((long) workoutSession.getStarttime()) * 1000, DateTimeZone.forOffsetMillis(workoutSession.getTimezoneOffsetInSecond() * 1000));
        DateTime dateTime2 = new DateTime(((long) workoutSession.getEndtime()) * 1000, DateTimeZone.forOffsetMillis(workoutSession.getTimezoneOffsetInSecond() * 1000));
        int timezoneOffsetInSecond2 = workoutSession.getTimezoneOffsetInSecond();
        int duration2 = workoutSession.getDuration();
        int ordinal = fi4.Companion.a(workoutSession.getType().ordinal()).ordinal();
        Step step2 = workoutSession.getStep();
        wg6.a((Object) step2, "workoutSession.step");
        StepWrapper stepWrapper = new StepWrapper(step2);
        Calorie calorie2 = workoutSession.getCalorie();
        wg6.a((Object) calorie2, "workoutSession.calorie");
        CalorieWrapper calorieWrapper = new CalorieWrapper(calorie2);
        Distance distance2 = workoutSession.getDistance();
        wg6.a((Object) distance2, "workoutSession.distance");
        DistanceWrapper distanceWrapper = new DistanceWrapper(distance2);
        if (workoutSession.getHeartrate() != null) {
            HeartRate heartrate = workoutSession.getHeartrate();
            wg6.a((Object) heartrate, "workoutSession.heartrate");
            heartRateWrapper = new HeartRateWrapper(heartrate);
        } else {
            heartRateWrapper = null;
        }
        HeartRateWrapper heartRateWrapper2 = heartRateWrapper;
        ArrayList<WorkoutStateChange> stateChanges2 = workoutSession.getStateChanges();
        wg6.a((Object) stateChanges2, "workoutSession.stateChanges");
        for (WorkoutStateChange workoutStateChange : stateChanges2) {
            List<WorkoutStateChangeWrapper> list = this.stateChanges;
            di4.a aVar = di4.Companion;
            wg6.a((Object) workoutStateChange, "it");
            list.add(new WorkoutStateChangeWrapper(aVar.a(workoutStateChange.getState().ordinal()).ordinal(), workoutStateChange.getIndexInSecond()));
        }
    }
}
