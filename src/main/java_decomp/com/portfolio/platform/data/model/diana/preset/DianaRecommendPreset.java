package com.portfolio.platform.data.model.diana.preset;

import com.fossil.tu3;
import com.fossil.uu3;
import com.fossil.vu3;
import com.fossil.wg6;
import com.fossil.zi4;
import com.j256.ormlite.logger.Logger;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendPreset {
    @DexIgnore
    @tu3
    @uu3(DianaPresetComplicationSettingSerializer.class)
    @vu3("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("isDefault")
    public boolean isDefault;
    @DexIgnore
    @tu3
    @vu3("name")
    public String name;
    @DexIgnore
    @vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @vu3("updatedAt")
    public String updatedAt;
    @DexIgnore
    @vu3("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @tu3
    @uu3(DianaPresetWatchAppSettingSerializer.class)
    @vu3("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore
    public DianaRecommendPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, String str5, String str6) {
        wg6.b(str, "serialNumber");
        wg6.b(str2, "id");
        wg6.b(str3, "name");
        wg6.b(arrayList, "complications");
        wg6.b(arrayList2, "watchapps");
        wg6.b(str4, "watchFaceId");
        wg6.b(str5, "createdAt");
        wg6.b(str6, "updatedAt");
        this.serialNumber = str;
        this.id = str2;
        this.name = str3;
        this.isDefault = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
        this.createdAt = str5;
        this.updatedAt = str6;
    }

    @DexIgnore
    public static /* synthetic */ DianaRecommendPreset copy$default(DianaRecommendPreset dianaRecommendPreset, String str, String str2, String str3, boolean z, ArrayList arrayList, ArrayList arrayList2, String str4, String str5, String str6, int i, Object obj) {
        DianaRecommendPreset dianaRecommendPreset2 = dianaRecommendPreset;
        int i2 = i;
        return dianaRecommendPreset.copy((i2 & 1) != 0 ? dianaRecommendPreset2.serialNumber : str, (i2 & 2) != 0 ? dianaRecommendPreset2.id : str2, (i2 & 4) != 0 ? dianaRecommendPreset2.name : str3, (i2 & 8) != 0 ? dianaRecommendPreset2.isDefault : z, (i2 & 16) != 0 ? dianaRecommendPreset2.complications : arrayList, (i2 & 32) != 0 ? dianaRecommendPreset2.watchapps : arrayList2, (i2 & 64) != 0 ? dianaRecommendPreset2.watchFaceId : str4, (i2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? dianaRecommendPreset2.createdAt : str5, (i2 & 256) != 0 ? dianaRecommendPreset2.updatedAt : str6);
    }

    @DexIgnore
    public final DianaRecommendPreset clone() {
        return new DianaRecommendPreset(this.serialNumber, this.id, this.name, this.isDefault, zi4.b(this.complications), zi4.c(this.watchapps), this.watchFaceId, this.createdAt, this.updatedAt);
    }

    @DexIgnore
    public final String component1() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component2() {
        return this.id;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isDefault;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final String component8() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final DianaRecommendPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, String str5, String str6) {
        wg6.b(str, "serialNumber");
        wg6.b(str2, "id");
        wg6.b(str3, "name");
        ArrayList<DianaPresetComplicationSetting> arrayList3 = arrayList;
        wg6.b(arrayList3, "complications");
        ArrayList<DianaPresetWatchAppSetting> arrayList4 = arrayList2;
        wg6.b(arrayList4, "watchapps");
        String str7 = str4;
        wg6.b(str7, "watchFaceId");
        String str8 = str5;
        wg6.b(str8, "createdAt");
        String str9 = str6;
        wg6.b(str9, "updatedAt");
        return new DianaRecommendPreset(str, str2, str3, z, arrayList3, arrayList4, str7, str8, str9);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaRecommendPreset)) {
            return false;
        }
        DianaRecommendPreset dianaRecommendPreset = (DianaRecommendPreset) obj;
        return wg6.a((Object) this.serialNumber, (Object) dianaRecommendPreset.serialNumber) && wg6.a((Object) this.id, (Object) dianaRecommendPreset.id) && wg6.a((Object) this.name, (Object) dianaRecommendPreset.name) && this.isDefault == dianaRecommendPreset.isDefault && wg6.a((Object) this.complications, (Object) dianaRecommendPreset.complications) && wg6.a((Object) this.watchapps, (Object) dianaRecommendPreset.watchapps) && wg6.a((Object) this.watchFaceId, (Object) dianaRecommendPreset.watchFaceId) && wg6.a((Object) this.createdAt, (Object) dianaRecommendPreset.createdAt) && wg6.a((Object) this.updatedAt, (Object) dianaRecommendPreset.updatedAt);
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.serialNumber;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.id;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        int i2 = (hashCode3 + (z ? 1 : 0)) * 31;
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = (i2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = (hashCode4 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        String str4 = this.watchFaceId;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.createdAt;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.updatedAt;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode7 + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        wg6.b(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaRecommendPreset(serialNumber=" + this.serialNumber + ", id=" + this.id + ", name=" + this.name + ", isDefault=" + this.isDefault + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
