package com.portfolio.platform.data.model.diana.preset;

import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Data {
    @DexIgnore
    @vu3("previewUrl")
    public String previewUrl;
    @DexIgnore
    @vu3("url")
    public String url;

    @DexIgnore
    public Data(String str, String str2) {
        this.previewUrl = str;
        this.url = str2;
    }

    @DexIgnore
    public static /* synthetic */ Data copy$default(Data data, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = data.previewUrl;
        }
        if ((i & 2) != 0) {
            str2 = data.url;
        }
        return data.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String component2() {
        return this.url;
    }

    @DexIgnore
    public final Data copy(String str, String str2) {
        return new Data(str, str2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Data)) {
            return false;
        }
        Data data = (Data) obj;
        return wg6.a((Object) this.previewUrl, (Object) data.previewUrl) && wg6.a((Object) this.url, (Object) data.url);
    }

    @DexIgnore
    public final String getPreviewUrl() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String getUrl() {
        return this.url;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.previewUrl;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.url;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setPreviewUrl(String str) {
        this.previewUrl = str;
    }

    @DexIgnore
    public final void setUrl(String str) {
        this.url = str;
    }

    @DexIgnore
    public String toString() {
        return "Data(previewUrl=" + this.previewUrl + ", url=" + this.url + ")";
    }
}
