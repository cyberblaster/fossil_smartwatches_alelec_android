package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("addresses")
    public List<AddressWrapper> addresses;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public CommuteTimeWatchAppSetting createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new CommuteTimeWatchAppSetting(parcel);
        }

        @DexIgnore
        public CommuteTimeWatchAppSetting[] newArray(int i) {
            return new CommuteTimeWatchAppSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeWatchAppSetting() {
        this((List) null, 1, (qg6) null);
    }

    @DexIgnore
    public CommuteTimeWatchAppSetting(List<AddressWrapper> list) {
        wg6.b(list, "addresses");
        this.addresses = list;
    }

    @DexIgnore
    public static /* synthetic */ CommuteTimeWatchAppSetting copy$default(CommuteTimeWatchAppSetting commuteTimeWatchAppSetting, List<AddressWrapper> list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = commuteTimeWatchAppSetting.addresses;
        }
        return commuteTimeWatchAppSetting.copy(list);
    }

    @DexIgnore
    public final List<AddressWrapper> component1() {
        return this.addresses;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting copy(List<AddressWrapper> list) {
        wg6.b(list, "addresses");
        return new CommuteTimeWatchAppSetting(list);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof CommuteTimeWatchAppSetting) && wg6.a((Object) this.addresses, (Object) ((CommuteTimeWatchAppSetting) obj).addresses);
        }
        return true;
    }

    @DexIgnore
    public final AddressWrapper getAddressByName(String str) {
        wg6.b(str, "name");
        for (AddressWrapper next : this.addresses) {
            if (wg6.a((Object) next.getName(), (Object) str)) {
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public final List<AddressWrapper> getAddresses() {
        return this.addresses;
    }

    @DexIgnore
    public final ArrayList<String> getListAddressNameExceptOf(AddressWrapper addressWrapper) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (AddressWrapper next : this.addresses) {
            if (!wg6.a((Object) next.getId(), (Object) addressWrapper != null ? addressWrapper.getId() : null)) {
                arrayList.add(next.getName());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public int hashCode() {
        List<AddressWrapper> list = this.addresses;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setAddresses(List<AddressWrapper> list) {
        wg6.b(list, "<set-?>");
        this.addresses = list;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeWatchAppSetting(addresses=" + this.addresses + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeList(this.addresses);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ CommuteTimeWatchAppSetting(List list, int i, qg6 qg6) {
        this((List<AddressWrapper>) list);
        if ((i & 1) != 0) {
            list = qd6.d(new AddressWrapper(AddressWrapper.AddressType.HOME.getValue(), AddressWrapper.AddressType.HOME), new AddressWrapper(AddressWrapper.AddressType.WORK.getValue(), AddressWrapper.AddressType.WORK));
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CommuteTimeWatchAppSetting(Parcel parcel) {
        this((List<AddressWrapper>) r0);
        wg6.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, AddressWrapper.class.getClassLoader());
    }
}
