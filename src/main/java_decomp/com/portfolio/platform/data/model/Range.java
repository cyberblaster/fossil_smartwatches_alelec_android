package com.portfolio.platform.data.model;

import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Range {
    @DexIgnore
    @vu3("hasNext")
    public boolean hasNext;
    @DexIgnore
    @vu3("limit")
    public int limit;
    @DexIgnore
    @vu3("offset")
    public int offset;

    @DexIgnore
    public int getLimit() {
        return this.limit;
    }

    @DexIgnore
    public int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public boolean isHasNext() {
        return this.hasNext;
    }

    @DexIgnore
    public void setHasNext(boolean z) {
        this.hasNext = z;
    }

    @DexIgnore
    public void setOffset(int i) {
        this.offset = i;
    }
}
