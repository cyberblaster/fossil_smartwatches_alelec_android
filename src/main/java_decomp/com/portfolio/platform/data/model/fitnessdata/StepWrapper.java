package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.d;
import com.fossil.fitness.Step;
import com.fossil.wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StepWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public int total;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public StepWrapper(int i, List<Short> list, int i2) {
        wg6.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = i2;
    }

    @DexIgnore
    public static /* synthetic */ StepWrapper copy$default(StepWrapper stepWrapper, int i, List<Short> list, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = stepWrapper.resolutionInSecond;
        }
        if ((i3 & 2) != 0) {
            list = stepWrapper.values;
        }
        if ((i3 & 4) != 0) {
            i2 = stepWrapper.total;
        }
        return stepWrapper.copy(i, list, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> component2() {
        return this.values;
    }

    @DexIgnore
    public final int component3() {
        return this.total;
    }

    @DexIgnore
    public final StepWrapper copy(int i, List<Short> list, int i2) {
        wg6.b(list, "values");
        return new StepWrapper(i, list, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StepWrapper)) {
            return false;
        }
        StepWrapper stepWrapper = (StepWrapper) obj;
        return this.resolutionInSecond == stepWrapper.resolutionInSecond && wg6.a((Object) this.values, (Object) stepWrapper.values) && this.total == stepWrapper.total;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolutionInSecond) * 31;
        List<Short> list = this.values;
        return ((a + (list != null ? list.hashCode() : 0)) * 31) + d.a(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        wg6.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "StepWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public StepWrapper(Step step) {
        this(r0, r1, step.getTotal());
        wg6.b(step, "step");
        int resolutionInSecond2 = step.getResolutionInSecond();
        ArrayList values2 = step.getValues();
        wg6.a((Object) values2, "step.values");
    }
}
