package com.portfolio.platform.data.model.diana;

import com.fossil.wg6;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSetting {
    @DexIgnore
    public String complicationId;
    @DexIgnore
    public String setting;
    @DexIgnore
    public String updatedAt;

    @DexIgnore
    public ComplicationLastSetting(String str, String str2, String str3) {
        wg6.b(str, "complicationId");
        wg6.b(str2, "updatedAt");
        wg6.b(str3, MicroAppSetting.SETTING);
        this.complicationId = str;
        this.updatedAt = str2;
        this.setting = str3;
    }

    @DexIgnore
    public static /* synthetic */ ComplicationLastSetting copy$default(ComplicationLastSetting complicationLastSetting, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = complicationLastSetting.complicationId;
        }
        if ((i & 2) != 0) {
            str2 = complicationLastSetting.updatedAt;
        }
        if ((i & 4) != 0) {
            str3 = complicationLastSetting.setting;
        }
        return complicationLastSetting.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.complicationId;
    }

    @DexIgnore
    public final String component2() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final ComplicationLastSetting copy(String str, String str2, String str3) {
        wg6.b(str, "complicationId");
        wg6.b(str2, "updatedAt");
        wg6.b(str3, MicroAppSetting.SETTING);
        return new ComplicationLastSetting(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ComplicationLastSetting)) {
            return false;
        }
        ComplicationLastSetting complicationLastSetting = (ComplicationLastSetting) obj;
        return wg6.a((Object) this.complicationId, (Object) complicationLastSetting.complicationId) && wg6.a((Object) this.updatedAt, (Object) complicationLastSetting.updatedAt) && wg6.a((Object) this.setting, (Object) complicationLastSetting.setting);
    }

    @DexIgnore
    public final String getComplicationId() {
        return this.complicationId;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.complicationId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.updatedAt;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.setting;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setComplicationId(String str) {
        wg6.b(str, "<set-?>");
        this.complicationId = str;
    }

    @DexIgnore
    public final void setSetting(String str) {
        wg6.b(str, "<set-?>");
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationLastSetting(complicationId=" + this.complicationId + ", updatedAt=" + this.updatedAt + ", setting=" + this.setting + ")";
    }
}
