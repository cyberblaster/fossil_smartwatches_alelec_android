package com.portfolio.platform.data.model.diana.preset;

import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyle {
    @DexIgnore
    @vu3("data")
    public Data data;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("metadata")
    public MetaData metadata;

    @DexIgnore
    public RingStyle(String str, Data data2, MetaData metaData) {
        wg6.b(str, "id");
        wg6.b(data2, "data");
        this.id = str;
        this.data = data2;
        this.metadata = metaData;
    }

    @DexIgnore
    public static /* synthetic */ RingStyle copy$default(RingStyle ringStyle, String str, Data data2, MetaData metaData, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringStyle.id;
        }
        if ((i & 2) != 0) {
            data2 = ringStyle.data;
        }
        if ((i & 4) != 0) {
            metaData = ringStyle.metadata;
        }
        return ringStyle.copy(str, data2, metaData);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final Data component2() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component3() {
        return this.metadata;
    }

    @DexIgnore
    public final RingStyle copy(String str, Data data2, MetaData metaData) {
        wg6.b(str, "id");
        wg6.b(data2, "data");
        return new RingStyle(str, data2, metaData);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RingStyle)) {
            return false;
        }
        RingStyle ringStyle = (RingStyle) obj;
        return wg6.a((Object) this.id, (Object) ringStyle.id) && wg6.a((Object) this.data, (Object) ringStyle.data) && wg6.a((Object) this.metadata, (Object) ringStyle.metadata);
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Data data2 = this.data;
        int hashCode2 = (hashCode + (data2 != null ? data2.hashCode() : 0)) * 31;
        MetaData metaData = this.metadata;
        if (metaData != null) {
            i = metaData.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setData(Data data2) {
        wg6.b(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetadata(MetaData metaData) {
        this.metadata = metaData;
    }

    @DexIgnore
    public String toString() {
        return "RingStyle(id=" + this.id + ", data=" + this.data + ", metadata=" + this.metadata + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RingStyle(String str, Data data2, MetaData metaData, int i, qg6 qg6) {
        this(str, data2, (i & 4) != 0 ? null : metaData);
    }
}
