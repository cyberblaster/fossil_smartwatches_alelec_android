package com.portfolio.platform.data.model.diana.workout;

import com.fossil.b;
import com.fossil.d;
import com.fossil.qg6;
import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSpeed {
    @DexIgnore
    public double average;
    @DexIgnore
    public int resolution;
    @DexIgnore
    public List<Double> values;

    @DexIgnore
    public WorkoutSpeed(int i, List<Double> list, double d) {
        wg6.b(list, "values");
        this.resolution = i;
        this.values = list;
        this.average = d;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSpeed copy$default(WorkoutSpeed workoutSpeed, int i, List<Double> list, double d, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutSpeed.resolution;
        }
        if ((i2 & 2) != 0) {
            list = workoutSpeed.values;
        }
        if ((i2 & 4) != 0) {
            d = workoutSpeed.average;
        }
        return workoutSpeed.copy(i, list, d);
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Double> component2() {
        return this.values;
    }

    @DexIgnore
    public final double component3() {
        return this.average;
    }

    @DexIgnore
    public final WorkoutSpeed copy(int i, List<Double> list, double d) {
        wg6.b(list, "values");
        return new WorkoutSpeed(i, list, d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutSpeed)) {
            return false;
        }
        WorkoutSpeed workoutSpeed = (WorkoutSpeed) obj;
        return this.resolution == workoutSpeed.resolution && wg6.a((Object) this.values, (Object) workoutSpeed.values) && Double.compare(this.average, workoutSpeed.average) == 0;
    }

    @DexIgnore
    public final double getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Double> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolution) * 31;
        List<Double> list = this.values;
        return ((a + (list != null ? list.hashCode() : 0)) * 31) + b.a(this.average);
    }

    @DexIgnore
    public final void setAverage(double d) {
        this.average = d;
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setValues(List<Double> list) {
        wg6.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSpeed(resolution=" + this.resolution + ", values=" + this.values + ", average=" + this.average + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutSpeed(int i, List list, double d, int i2, qg6 qg6) {
        this(i, list, (i2 & 4) != 0 ? 0.0d : d);
    }
}
