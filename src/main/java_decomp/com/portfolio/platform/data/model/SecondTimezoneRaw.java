package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.d;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneRaw implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("cityCode")
    public String cityCode;
    @DexIgnore
    @vu3("cityName")
    public String cityName;
    @DexIgnore
    @vu3("countryName")
    public String countryName;
    @DexIgnore
    @vu3("timeZoneId")
    public String timeZoneId;
    @DexIgnore
    @vu3("timeZoneName")
    public String timeZoneName;
    @DexIgnore
    @vu3("timeZoneOffset")
    public int timezoneOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SecondTimezoneRaw> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public SecondTimezoneRaw createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new SecondTimezoneRaw(parcel);
        }

        @DexIgnore
        public SecondTimezoneRaw[] newArray(int i) {
            return new SecondTimezoneRaw[i];
        }
    }

    @DexIgnore
    public SecondTimezoneRaw() {
        this((String) null, (String) null, (String) null, (String) null, 0, (String) null, 63, (qg6) null);
    }

    @DexIgnore
    public SecondTimezoneRaw(String str, String str2, String str3, String str4, int i, String str5) {
        wg6.b(str, "cityName");
        wg6.b(str2, "countryName");
        wg6.b(str3, "timeZoneName");
        wg6.b(str4, "timeZoneId");
        wg6.b(str5, "cityCode");
        this.cityName = str;
        this.countryName = str2;
        this.timeZoneName = str3;
        this.timeZoneId = str4;
        this.timezoneOffset = i;
        this.cityCode = str5;
    }

    @DexIgnore
    public static /* synthetic */ SecondTimezoneRaw copy$default(SecondTimezoneRaw secondTimezoneRaw, String str, String str2, String str3, String str4, int i, String str5, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = secondTimezoneRaw.cityName;
        }
        if ((i2 & 2) != 0) {
            str2 = secondTimezoneRaw.countryName;
        }
        String str6 = str2;
        if ((i2 & 4) != 0) {
            str3 = secondTimezoneRaw.timeZoneName;
        }
        String str7 = str3;
        if ((i2 & 8) != 0) {
            str4 = secondTimezoneRaw.timeZoneId;
        }
        String str8 = str4;
        if ((i2 & 16) != 0) {
            i = secondTimezoneRaw.timezoneOffset;
        }
        int i3 = i;
        if ((i2 & 32) != 0) {
            str5 = secondTimezoneRaw.cityCode;
        }
        return secondTimezoneRaw.copy(str, str6, str7, str8, i3, str5);
    }

    @DexIgnore
    public final String component1() {
        return this.cityName;
    }

    @DexIgnore
    public final String component2() {
        return this.countryName;
    }

    @DexIgnore
    public final String component3() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final String component4() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final int component5() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component6() {
        return this.cityCode;
    }

    @DexIgnore
    public final SecondTimezoneRaw copy(String str, String str2, String str3, String str4, int i, String str5) {
        wg6.b(str, "cityName");
        wg6.b(str2, "countryName");
        wg6.b(str3, "timeZoneName");
        wg6.b(str4, "timeZoneId");
        wg6.b(str5, "cityCode");
        return new SecondTimezoneRaw(str, str2, str3, str4, i, str5);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SecondTimezoneRaw)) {
            return false;
        }
        SecondTimezoneRaw secondTimezoneRaw = (SecondTimezoneRaw) obj;
        return wg6.a((Object) this.cityName, (Object) secondTimezoneRaw.cityName) && wg6.a((Object) this.countryName, (Object) secondTimezoneRaw.countryName) && wg6.a((Object) this.timeZoneName, (Object) secondTimezoneRaw.timeZoneName) && wg6.a((Object) this.timeZoneId, (Object) secondTimezoneRaw.timeZoneId) && this.timezoneOffset == secondTimezoneRaw.timezoneOffset && wg6.a((Object) this.cityCode, (Object) secondTimezoneRaw.cityCode);
    }

    @DexIgnore
    public final String getCityCode() {
        return this.cityCode;
    }

    @DexIgnore
    public final String getCityName() {
        return this.cityName;
    }

    @DexIgnore
    public final String getCountryName() {
        return this.countryName;
    }

    @DexIgnore
    public final String getTimeZoneId() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final String getTimeZoneName() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.cityName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.countryName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.timeZoneName;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.timeZoneId;
        int hashCode4 = (((hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31) + d.a(this.timezoneOffset)) * 31;
        String str5 = this.cityCode;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setCityCode(String str) {
        wg6.b(str, "<set-?>");
        this.cityCode = str;
    }

    @DexIgnore
    public final void setCityName(String str) {
        wg6.b(str, "<set-?>");
        this.cityName = str;
    }

    @DexIgnore
    public final void setCountryName(String str) {
        wg6.b(str, "<set-?>");
        this.countryName = str;
    }

    @DexIgnore
    public final void setTimeZoneId(String str) {
        wg6.b(str, "<set-?>");
        this.timeZoneId = str;
    }

    @DexIgnore
    public final void setTimeZoneName(String str) {
        wg6.b(str, "<set-?>");
        this.timeZoneName = str;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public String toString() {
        return "SecondTimezoneRaw(cityName=" + this.cityName + ", countryName=" + this.countryName + ", timeZoneName=" + this.timeZoneName + ", timeZoneId=" + this.timeZoneId + ", timezoneOffset=" + this.timezoneOffset + ", cityCode=" + this.cityCode + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.cityName);
        parcel.writeString(this.countryName);
        parcel.writeString(this.timeZoneName);
        parcel.writeString(this.timeZoneId);
        parcel.writeInt(this.timezoneOffset);
        parcel.writeString(this.cityCode);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ SecondTimezoneRaw(String str, String str2, String str3, String str4, int i, String str5, int i2, qg6 qg6) {
        this(r13, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? "" : str3, (i2 & 8) != 0 ? "" : str4, (i2 & 16) != 0 ? 0 : i, (i2 & 32) != 0 ? "" : str5);
        String str6 = (i2 & 1) != 0 ? "" : str;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SecondTimezoneRaw(Parcel parcel) {
        this(r3, r4, r5, r6, r7, r8);
        String str;
        String str2;
        String str3;
        String str4;
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        String str5 = readString != null ? readString : "";
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
        String readString4 = parcel.readString();
        if (readString4 != null) {
            str3 = readString4;
        } else {
            str3 = "";
        }
        int readInt = parcel.readInt();
        String readString5 = parcel.readString();
        if (readString5 != null) {
            str4 = readString5;
        } else {
            str4 = "";
        }
    }
}
