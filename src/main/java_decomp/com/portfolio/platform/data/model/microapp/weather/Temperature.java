package com.portfolio.platform.data.model.microapp.weather;

import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Temperature {
    @DexIgnore
    @vu3("currently")
    public float currently;
    @DexIgnore
    @vu3("max")
    public float max;
    @DexIgnore
    @vu3("min")
    public float min;
    @DexIgnore
    @vu3("unit")
    public String unit;

    @DexIgnore
    public float getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public float getMax() {
        return this.max;
    }

    @DexIgnore
    public float getMin() {
        return this.min;
    }

    @DexIgnore
    public String getUnit() {
        return this.unit;
    }
}
