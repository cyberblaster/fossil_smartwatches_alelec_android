package com.portfolio.platform.data.model.room.microapp;

import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroApp {
    @DexIgnore
    @vu3("categoryIds")
    public ArrayList<String> categories;
    @DexIgnore
    @vu3("englishDescription")
    public String description;
    @DexIgnore
    @vu3("description")
    public String descriptionKey;
    @DexIgnore
    @vu3("icon")
    public String icon;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("englishName")
    public String name;
    @DexIgnore
    @vu3("name")
    public String nameKey;
    @DexIgnore
    public String serialNumber;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[MicroAppInstruction.MicroAppID.values().length];

        /*
        static {
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
            $EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
        }
        */
    }

    @DexIgnore
    public MicroApp(String str, String str2, String str3, String str4, ArrayList<String> arrayList, String str5, String str6, String str7) {
        wg6.b(str, "id");
        wg6.b(str2, "name");
        wg6.b(str3, "nameKey");
        wg6.b(str4, "serialNumber");
        wg6.b(arrayList, HelpRequest.INCLUDE_CATEGORIES);
        wg6.b(str5, "description");
        wg6.b(str6, "descriptionKey");
        this.id = str;
        this.name = str2;
        this.nameKey = str3;
        this.serialNumber = str4;
        this.categories = arrayList;
        this.description = str5;
        this.descriptionKey = str6;
        this.icon = str7;
    }

    @DexIgnore
    public static /* synthetic */ MicroApp copy$default(MicroApp microApp, String str, String str2, String str3, String str4, ArrayList arrayList, String str5, String str6, String str7, int i, Object obj) {
        MicroApp microApp2 = microApp;
        int i2 = i;
        return microApp.copy((i2 & 1) != 0 ? microApp2.id : str, (i2 & 2) != 0 ? microApp2.name : str2, (i2 & 4) != 0 ? microApp2.nameKey : str3, (i2 & 8) != 0 ? microApp2.serialNumber : str4, (i2 & 16) != 0 ? microApp2.categories : arrayList, (i2 & 32) != 0 ? microApp2.description : str5, (i2 & 64) != 0 ? microApp2.descriptionKey : str6, (i2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? microApp2.icon : str7);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final String component4() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<String> component5() {
        return this.categories;
    }

    @DexIgnore
    public final String component6() {
        return this.description;
    }

    @DexIgnore
    public final String component7() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String component8() {
        return this.icon;
    }

    @DexIgnore
    public final MicroApp copy(String str, String str2, String str3, String str4, ArrayList<String> arrayList, String str5, String str6, String str7) {
        wg6.b(str, "id");
        wg6.b(str2, "name");
        wg6.b(str3, "nameKey");
        wg6.b(str4, "serialNumber");
        wg6.b(arrayList, HelpRequest.INCLUDE_CATEGORIES);
        String str8 = str5;
        wg6.b(str8, "description");
        String str9 = str6;
        wg6.b(str9, "descriptionKey");
        return new MicroApp(str, str2, str3, str4, arrayList, str8, str9, str7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroApp)) {
            return false;
        }
        MicroApp microApp = (MicroApp) obj;
        return wg6.a((Object) this.id, (Object) microApp.id) && wg6.a((Object) this.name, (Object) microApp.name) && wg6.a((Object) this.nameKey, (Object) microApp.nameKey) && wg6.a((Object) this.serialNumber, (Object) microApp.serialNumber) && wg6.a((Object) this.categories, (Object) microApp.categories) && wg6.a((Object) this.description, (Object) microApp.description) && wg6.a((Object) this.descriptionKey, (Object) microApp.descriptionKey) && wg6.a((Object) this.icon, (Object) microApp.icon);
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final int getDefaultIconId() {
        switch (WhenMappings.$EnumSwitchMapping$0[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.id).ordinal()]) {
            case 1:
                return 2131231124;
            case 2:
                return 2131231188;
            case 3:
                return 2131231180;
            case 4:
                return 2131231181;
            case 5:
                return 2131231186;
            case 6:
                return 2131231184;
            case 7:
                return 2131231183;
            case 8:
                return 2131231185;
            case 9:
                return 2131231187;
            case 10:
                return 2131231191;
            case 11:
                return 2131231192;
            case 12:
                return 2131231179;
            case 13:
                return 2131231193;
            case 14:
                return 2131231189;
            case 15:
                return 2131231190;
            default:
                return 2131231124;
        }
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.nameKey;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.serialNumber;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        ArrayList<String> arrayList = this.categories;
        int hashCode5 = (hashCode4 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        String str5 = this.description;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.descriptionKey;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.icon;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode7 + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setDescription(String str) {
        wg6.b(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        wg6.b(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        wg6.b(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroApp(id=" + this.id + ", name=" + this.name + ", nameKey=" + this.nameKey + ", serialNumber=" + this.serialNumber + ", categories=" + this.categories + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", icon=" + this.icon + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroApp(String str, String str2, String str3, String str4, ArrayList arrayList, String str5, String str6, String str7, int i, qg6 qg6) {
        this(str, str2, str3, str4, arrayList, str5, str6, (i & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? "" : str7);
    }
}
