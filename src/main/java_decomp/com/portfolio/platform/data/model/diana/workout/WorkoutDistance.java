package com.portfolio.platform.data.model.diana.workout;

import com.fossil.b;
import com.fossil.d;
import com.fossil.wg6;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDistance {
    @DexIgnore
    public int resolution;
    @DexIgnore
    public double total;
    @DexIgnore
    public List<Double> values;

    @DexIgnore
    public WorkoutDistance(int i, List<Double> list, double d) {
        this.resolution = i;
        this.values = list;
        this.total = d;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutDistance copy$default(WorkoutDistance workoutDistance, int i, List<Double> list, double d, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutDistance.resolution;
        }
        if ((i2 & 2) != 0) {
            list = workoutDistance.values;
        }
        if ((i2 & 4) != 0) {
            d = workoutDistance.total;
        }
        return workoutDistance.copy(i, list, d);
    }

    @DexIgnore
    public final WorkoutSpeed calculateSpeed() {
        ArrayList arrayList = new ArrayList();
        List<Double> list = this.values;
        double d = 0.0d;
        int i = 0;
        if (list != null) {
            for (Number doubleValue : list) {
                double doubleValue2 = doubleValue.doubleValue();
                arrayList.add(Double.valueOf(doubleValue2 / ((double) this.resolution)));
                d += doubleValue2;
                i++;
            }
        }
        if (i == 0) {
            return null;
        }
        return new WorkoutSpeed(this.resolution, arrayList, d / ((double) i));
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Double> component2() {
        return this.values;
    }

    @DexIgnore
    public final double component3() {
        return this.total;
    }

    @DexIgnore
    public final WorkoutDistance copy(int i, List<Double> list, double d) {
        return new WorkoutDistance(i, list, d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutDistance)) {
            return false;
        }
        WorkoutDistance workoutDistance = (WorkoutDistance) obj;
        return this.resolution == workoutDistance.resolution && wg6.a((Object) this.values, (Object) workoutDistance.values) && Double.compare(this.total, workoutDistance.total) == 0;
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final double getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Double> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolution) * 31;
        List<Double> list = this.values;
        return ((a + (list != null ? list.hashCode() : 0)) * 31) + b.a(this.total);
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setTotal(double d) {
        this.total = d;
    }

    @DexIgnore
    public final void setValues(List<Double> list) {
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutDistance(resolution=" + this.resolution + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutDistance(DistanceWrapper distanceWrapper) {
        this(distanceWrapper.getResolutionInSecond(), distanceWrapper.getValues(), distanceWrapper.getTotal());
        wg6.b(distanceWrapper, "distance");
    }
}
