package com.portfolio.platform.data.model.diana.preset;

import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaComplicationRingStyle {
    @DexIgnore
    @vu3("category")
    public String category;
    @DexIgnore
    @vu3("data")
    public Data data;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("metadata")
    public MetaData metaData;
    @DexIgnore
    @vu3("name")
    public String name;
    @DexIgnore
    public String serial;

    @DexIgnore
    public DianaComplicationRingStyle(String str, String str2, String str3, Data data2, MetaData metaData2, String str4) {
        wg6.b(str, "id");
        wg6.b(str2, "category");
        wg6.b(str3, "name");
        wg6.b(data2, "data");
        wg6.b(metaData2, "metaData");
        wg6.b(str4, "serial");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metaData = metaData2;
        this.serial = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaComplicationRingStyle copy$default(DianaComplicationRingStyle dianaComplicationRingStyle, String str, String str2, String str3, Data data2, MetaData metaData2, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaComplicationRingStyle.id;
        }
        if ((i & 2) != 0) {
            str2 = dianaComplicationRingStyle.category;
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            str3 = dianaComplicationRingStyle.name;
        }
        String str6 = str3;
        if ((i & 8) != 0) {
            data2 = dianaComplicationRingStyle.data;
        }
        Data data3 = data2;
        if ((i & 16) != 0) {
            metaData2 = dianaComplicationRingStyle.metaData;
        }
        MetaData metaData3 = metaData2;
        if ((i & 32) != 0) {
            str4 = dianaComplicationRingStyle.serial;
        }
        return dianaComplicationRingStyle.copy(str, str5, str6, data3, metaData3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.category;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final Data component4() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component5() {
        return this.metaData;
    }

    @DexIgnore
    public final String component6() {
        return this.serial;
    }

    @DexIgnore
    public final DianaComplicationRingStyle copy(String str, String str2, String str3, Data data2, MetaData metaData2, String str4) {
        wg6.b(str, "id");
        wg6.b(str2, "category");
        wg6.b(str3, "name");
        wg6.b(data2, "data");
        wg6.b(metaData2, "metaData");
        wg6.b(str4, "serial");
        return new DianaComplicationRingStyle(str, str2, str3, data2, metaData2, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaComplicationRingStyle)) {
            return false;
        }
        DianaComplicationRingStyle dianaComplicationRingStyle = (DianaComplicationRingStyle) obj;
        return wg6.a((Object) this.id, (Object) dianaComplicationRingStyle.id) && wg6.a((Object) this.category, (Object) dianaComplicationRingStyle.category) && wg6.a((Object) this.name, (Object) dianaComplicationRingStyle.name) && wg6.a((Object) this.data, (Object) dianaComplicationRingStyle.data) && wg6.a((Object) this.metaData, (Object) dianaComplicationRingStyle.metaData) && wg6.a((Object) this.serial, (Object) dianaComplicationRingStyle.serial);
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.category;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Data data2 = this.data;
        int hashCode4 = (hashCode3 + (data2 != null ? data2.hashCode() : 0)) * 31;
        MetaData metaData2 = this.metaData;
        int hashCode5 = (hashCode4 + (metaData2 != null ? metaData2.hashCode() : 0)) * 31;
        String str4 = this.serial;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setCategory(String str) {
        wg6.b(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setData(Data data2) {
        wg6.b(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        wg6.b(metaData2, "<set-?>");
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerial(String str) {
        wg6.b(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaComplicationRingStyle(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", data=" + this.data + ", metaData=" + this.metaData + ", serial=" + this.serial + ")";
    }
}
