package com.portfolio.platform.data.model.diana.workout;

import com.fossil.d;
import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutLocation {
    @DexIgnore
    public int resolution;
    @DexIgnore
    public List<Location> values;

    @DexIgnore
    public WorkoutLocation(int i, List<Location> list) {
        wg6.b(list, "values");
        this.resolution = i;
        this.values = list;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutLocation copy$default(WorkoutLocation workoutLocation, int i, List<Location> list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutLocation.resolution;
        }
        if ((i2 & 2) != 0) {
            list = workoutLocation.values;
        }
        return workoutLocation.copy(i, list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Location> component2() {
        return this.values;
    }

    @DexIgnore
    public final WorkoutLocation copy(int i, List<Location> list) {
        wg6.b(list, "values");
        return new WorkoutLocation(i, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutLocation)) {
            return false;
        }
        WorkoutLocation workoutLocation = (WorkoutLocation) obj;
        return this.resolution == workoutLocation.resolution && wg6.a((Object) this.values, (Object) workoutLocation.values);
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Location> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolution) * 31;
        List<Location> list = this.values;
        return a + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setValues(List<Location> list) {
        wg6.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutLocation(resolution=" + this.resolution + ", values=" + this.values + ")";
    }
}
