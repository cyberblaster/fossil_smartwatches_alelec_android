package com.portfolio.platform.data.model.ua;

import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UADataSource {
    @DexIgnore
    @vu3("active")
    public Boolean active;
    @DexIgnore
    @vu3("advertised_name")
    public String advertisedName;
    @DexIgnore
    @vu3("bluetooth_device_address")
    public String bluetoothDeviceAddress;
    @DexIgnore
    @vu3("_embedded")
    public UAEmbedded embedded;
    @DexIgnore
    @vu3("firmware_version")
    public String firmwareVersion;
    @DexIgnore
    @vu3("hardware_version")
    public String hardwareVersion;
    @DexIgnore
    @vu3("_links")
    public UALinks link;
    @DexIgnore
    @vu3("name")
    public String name;
    @DexIgnore
    @vu3("serial_number")
    public String serialNumber;

    @DexIgnore
    public final Boolean getActive() {
        return this.active;
    }

    @DexIgnore
    public final String getAdvertisedName() {
        return this.advertisedName;
    }

    @DexIgnore
    public final String getBluetoothDeviceAddress() {
        return this.bluetoothDeviceAddress;
    }

    @DexIgnore
    public final UAEmbedded getEmbedded() {
        return this.embedded;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final String getHardwareVersion() {
        return this.hardwareVersion;
    }

    @DexIgnore
    public final UALinks getLink() {
        return this.link;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final void setActive(Boolean bool) {
        this.active = bool;
    }

    @DexIgnore
    public final void setAdvertisedName(String str) {
        this.advertisedName = str;
    }

    @DexIgnore
    public final void setBluetoothDeviceAddress(String str) {
        this.bluetoothDeviceAddress = str;
    }

    @DexIgnore
    public final void setEmbedded(UAEmbedded uAEmbedded) {
        this.embedded = uAEmbedded;
    }

    @DexIgnore
    public final void setFirmwareVersion(String str) {
        this.firmwareVersion = str;
    }

    @DexIgnore
    public final void setHardwareVersion(String str) {
        this.hardwareVersion = str;
    }

    @DexIgnore
    public final void setLink(UALinks uALinks) {
        this.link = uALinks;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        this.serialNumber = str;
    }
}
