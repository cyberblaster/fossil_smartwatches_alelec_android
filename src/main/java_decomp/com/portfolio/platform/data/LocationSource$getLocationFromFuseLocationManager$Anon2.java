package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wv2;
import com.fossil.xe6;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.LocationSource$getLocationFromFuseLocationManager$2", f = "LocationSource.kt", l = {134}, m = "invokeSuspend")
public final class LocationSource$getLocationFromFuseLocationManager$Anon2 extends sf6 implements ig6<il6, xe6<? super Location>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ wv2 $fusedLocationClient;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocationFromFuseLocationManager$Anon2(LocationSource locationSource, wv2 wv2, LocationRequest locationRequest, xe6 xe6) {
        super(2, xe6);
        this.this$0 = locationSource;
        this.$fusedLocationClient = wv2;
        this.$locationRequest = locationRequest;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        LocationSource$getLocationFromFuseLocationManager$Anon2 locationSource$getLocationFromFuseLocationManager$Anon2 = new LocationSource$getLocationFromFuseLocationManager$Anon2(this.this$0, this.$fusedLocationClient, this.$locationRequest, xe6);
        locationSource$getLocationFromFuseLocationManager$Anon2.p$ = (il6) obj;
        return locationSource$getLocationFromFuseLocationManager$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LocationSource$getLocationFromFuseLocationManager$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            LocationSource locationSource = this.this$0;
            wv2 wv2 = this.$fusedLocationClient;
            wg6.a((Object) wv2, "fusedLocationClient");
            LocationRequest locationRequest = this.$locationRequest;
            this.L$0 = il6;
            this.label = 1;
            obj = locationSource.requestLocationUpdates(wv2, locationRequest, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
