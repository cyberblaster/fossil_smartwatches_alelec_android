package com.portfolio.platform.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationType {
    @DexIgnore
    public static /* final */ int ACTIVITY_DETAILS; // = 1;
    @DexIgnore
    public static /* final */ int HEART_RATE_DETAILS; // = 2;
    @DexIgnore
    public static /* final */ InAppNotificationType INSTANCE; // = new InAppNotificationType();
    @DexIgnore
    public static /* final */ int NONE_DETAILS; // = 0;
    @DexIgnore
    public static /* final */ int SLEEP_DETAILS; // = 4;
    @DexIgnore
    public static /* final */ int WORKOUT_DETAILS; // = 3;
}
