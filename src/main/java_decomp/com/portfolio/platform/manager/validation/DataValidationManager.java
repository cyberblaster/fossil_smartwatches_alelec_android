package com.portfolio.platform.manager.validation;

import com.fossil.ai4;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DataValidationManager {
    @DexIgnore
    public /* final */ DianaPresetRepository a;
    @DexIgnore
    public /* final */ an4 b;
    @DexIgnore
    public /* final */ WatchFaceRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.validation.DataValidationManager", f = "DataValidationManager.kt", l = {37}, m = "validateDianaPreset")
    public static final class b extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DataValidationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DataValidationManager dataValidationManager, xe6 xe6) {
            super(xe6);
            this.this$0 = dataValidationManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DataValidationManager(DianaPresetRepository dianaPresetRepository, an4 an4, WatchFaceRepository watchFaceRepository) {
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(an4, "mSharePreferencesManager");
        wg6.b(watchFaceRepository, "mWatchFaceRepository");
        this.a = dianaPresetRepository;
        this.b = an4;
        this.c = watchFaceRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object a(String str, xe6<? super cd6> xe6) {
        b bVar;
        int i;
        DataValidationManager dataValidationManager;
        if (xe6 instanceof b) {
            bVar = (b) xe6;
            int i2 = bVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                bVar.label = i2 - Integer.MIN_VALUE;
                Object obj = bVar.result;
                Object a2 = ff6.a();
                i = bVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    ArrayList<DianaPreset> presetList = this.a.getPresetList(str);
                    List<WatchFace> watchFacesWithType = this.c.getWatchFacesWithType(str, ai4.BACKGROUND);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DataValidationManager", "invalidateDianaPreset serial " + str + " presetList size " + presetList.size() + " watchFaceListSize " + watchFacesWithType.size());
                    if (presetList.isEmpty() || watchFacesWithType.isEmpty()) {
                        return cd6.a;
                    }
                    fh6 fh6 = new fh6();
                    fh6.element = false;
                    for (DianaPreset dianaPreset : presetList) {
                        if (this.c.getWatchFaceWithId(dianaPreset.getWatchFaceId()) == null) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d("DataValidationManager", "invalidateDianaPreset watchface " + dianaPreset.getWatchFaceId() + " is not exist, choose first watch face " + watchFacesWithType.get(0) + " for preset " + dianaPreset.getName());
                            dianaPreset.setWatchFaceId(watchFacesWithType.get(0).getId());
                            fh6.element = true;
                        }
                    }
                    if (fh6.element) {
                        FLogger.INSTANCE.getLocal().d("DataValidationManager", "invalidateDianaPreset preset is outdated, force sync to device");
                        DianaPresetRepository dianaPresetRepository = this.a;
                        bVar.L$0 = this;
                        bVar.L$1 = str;
                        bVar.L$2 = presetList;
                        bVar.L$3 = watchFacesWithType;
                        bVar.L$4 = fh6;
                        bVar.label = 1;
                        if (dianaPresetRepository.upsertPresetList(presetList, bVar) == a2) {
                            return a2;
                        }
                        dataValidationManager = this;
                    }
                    return cd6.a;
                } else if (i == 1) {
                    fh6 fh62 = (fh6) bVar.L$4;
                    List list = (List) bVar.L$3;
                    ArrayList arrayList = (ArrayList) bVar.L$2;
                    str = (String) bVar.L$1;
                    dataValidationManager = (DataValidationManager) bVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                dataValidationManager.b.a(str, 0, false);
                return cd6.a;
            }
        }
        bVar = new b(this, xe6);
        Object obj2 = bVar.result;
        Object a22 = ff6.a();
        i = bVar.label;
        if (i != 0) {
        }
        dataValidationManager.b.a(str, 0, false);
        return cd6.a;
    }
}
