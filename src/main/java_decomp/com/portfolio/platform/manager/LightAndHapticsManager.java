package com.portfolio.platform.manager;

import android.os.Handler;
import android.os.Looper;
import android.provider.Telephony;
import android.text.TextUtils;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ej6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.tm4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wg6;
import com.fossil.wx5;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.zl6;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.HybridNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppFilterHistory;
import com.portfolio.platform.data.AppType;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.MessageComparator;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.data.model.NotificationPriority;
import com.portfolio.platform.data.source.DeviceRepository;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.zip.CRC32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LightAndHapticsManager {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static LightAndHapticsManager h;
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public an4 b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<LightAndHaptics> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ Runnable f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(LightAndHapticsManager lightAndHapticsManager) {
            LightAndHapticsManager.h = lightAndHapticsManager;
        }

        @DexIgnore
        public final LightAndHapticsManager b() {
            return LightAndHapticsManager.h;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final LightAndHapticsManager a() {
            if (b() == null) {
                a(new LightAndHapticsManager((qg6) null));
            }
            LightAndHapticsManager b = b();
            if (b != null) {
                return b;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$info, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                LightAndHapticsManager lightAndHapticsManager = this.this$0;
                String packageName = this.$info.getPackageName();
                wg6.a((Object) packageName, "info.packageName");
                LightAndHaptics a = lightAndHapticsManager.a(packageName, this.$info.getBody());
                if (a != null) {
                    a.setNotificationType(NotificationType.APP_FILTER);
                    this.this$0.a(a);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstEmail$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$info, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = LightAndHapticsManager.g;
                local.e(e, "Check again email with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics a = this.this$0.a(AppType.ALL_EMAIL.name(), this.$info.getBody());
                if (a != null) {
                    this.this$0.a(a);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstPhone$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$info, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = LightAndHapticsManager.g;
                local.e(e, "Check again phone with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics a = this.this$0.a(AppType.ALL_CALLS.name(), this.$info.getSenderInfo());
                if (a != null) {
                    a.setNotificationType(NotificationType.CALL);
                    this.this$0.a(a);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$info, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = LightAndHapticsManager.g;
                local.e(e, "Check again sms with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics a = this.this$0.a(AppType.ALL_SMS.name(), this.$info.getBody());
                if (a != null) {
                    a.setNotificationType(NotificationType.SMS);
                    this.this$0.a(a);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager a;

        @DexIgnore
        public f(LightAndHapticsManager lightAndHapticsManager) {
            this.a = lightAndHapticsManager;
        }

        @DexIgnore
        public final void run() {
            this.a.b();
        }
    }

    /*
    static {
        String simpleName = LightAndHapticsManager.class.getSimpleName();
        wg6.a((Object) simpleName, "LightAndHapticsManager::class.java.simpleName");
        g = simpleName;
        wg6.a((Object) Arrays.asList(new CalibrationEnums.HandId[]{CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR}), "Arrays.asList(Calibratio\u2026brationEnums.HandId.HOUR)");
        wg6.a((Object) Arrays.asList(new CalibrationEnums.HandId[]{CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR, CalibrationEnums.HandId.SUB_EYE}), "Arrays.asList(Calibratio\u2026tionEnums.HandId.SUB_EYE)");
    }
    */

    @DexIgnore
    public LightAndHapticsManager() {
        PortfolioApp.get.instance().g().a(this);
        this.c = new PriorityBlockingQueue<>(5, new MessageComparator());
        this.e = new Handler(Looper.getMainLooper());
        this.f = new f(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0095 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x017c A[RETURN] */
    public final boolean c(NotificationInfo notificationInfo) {
        List<ContactGroup> list;
        List<ContactGroup> list2;
        LightAndHaptics lightAndHaptics = null;
        if (notificationInfo.getSource() == NotificationSource.TEXT) {
            list2 = wx5.a().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
            if (!TextUtils.isEmpty(notificationInfo.getSenderInfo())) {
                wx5 a2 = wx5.a();
                String senderInfo = notificationInfo.getSenderInfo();
                if (senderInfo != null) {
                    list = a2.b(senderInfo, MFDeviceFamily.DEVICE_FAMILY_SAM);
                    if (list2 == null && list != null && list.isEmpty() && list2.isEmpty()) {
                        if (a()) {
                            b(notificationInfo);
                            FLogger.INSTANCE.getLocal().d(g, ".Inside checkAgainstContacts, NO assigned contact with this phone number, NO ALL-TEXT, let Message fire");
                        }
                        return false;
                    } else if (list != null && list2 == null) {
                        return false;
                    } else {
                        if (list == null || !(!list.isEmpty())) {
                            NotificationSource source = notificationInfo.getSource();
                            if (source != null) {
                                int i2 = tm4.a[source.ordinal()];
                                if (i2 == 1) {
                                    NotificationSource source2 = notificationInfo.getSource();
                                    wg6.a((Object) source2, "info.source");
                                    lightAndHaptics = a("-5678", (List<? extends ContactGroup>) list2, source2);
                                    FLogger.INSTANCE.getLocal().d(g, "ALL TEXT with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
                                } else if (i2 == 2) {
                                    NotificationSource source3 = notificationInfo.getSource();
                                    wg6.a((Object) source3, "info.source");
                                    lightAndHaptics = a("-1234", (List<? extends ContactGroup>) list2, source3);
                                    FLogger.INSTANCE.getLocal().d(g, "ALL CALL with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
                                }
                            }
                        } else {
                            String senderInfo2 = notificationInfo.getSenderInfo();
                            NotificationSource source4 = notificationInfo.getSource();
                            wg6.a((Object) source4, "info.source");
                            lightAndHaptics = a(senderInfo2, (List<? extends ContactGroup>) list, source4);
                        }
                        FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
                        if (lightAndHaptics == null) {
                            return false;
                        }
                        a(lightAndHaptics);
                        return true;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            if (notificationInfo.getSource() == NotificationSource.CALL) {
                list2 = wx5.a().a("-1234", MFDeviceFamily.DEVICE_FAMILY_SAM);
                if (!TextUtils.isEmpty(notificationInfo.getSenderInfo())) {
                    wx5 a3 = wx5.a();
                    String senderInfo3 = notificationInfo.getSenderInfo();
                    if (senderInfo3 != null) {
                        list = a3.a(senderInfo3, MFDeviceFamily.DEVICE_FAMILY_SAM);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                list2 = null;
                list = null;
            }
            if (list2 == null) {
            }
            if (list != null) {
            }
            if (list == null || !(!list.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
            if (lightAndHaptics == null) {
            }
        }
        list = null;
        if (list2 == null) {
        }
        if (list != null) {
        }
        if (list == null || !(!list.isEmpty())) {
        }
        FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + lightAndHaptics);
        if (lightAndHaptics == null) {
        }
    }

    @DexIgnore
    public final rm6 f(NotificationInfo notificationInfo) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new e(this, notificationInfo, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(NotificationInfo notificationInfo) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        if (!TextUtils.isEmpty(notificationInfo.getPackageName())) {
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new b(this, notificationInfo, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final rm6 d(NotificationInfo notificationInfo) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new c(this, notificationInfo, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final rm6 e(NotificationInfo notificationInfo) {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new d(this, notificationInfo, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(NotificationInfo notificationInfo) {
        wg6.b(notificationInfo, Constants.INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "notification info : " + notificationInfo.getSource());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local2.d(str2, "notification body : " + notificationInfo.getBody());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local3.d(str3, "notification type : " + notificationInfo.getSource());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = g;
        local4.e(str4, "notification - info=" + notificationInfo);
        String packageName = notificationInfo.getPackageName();
        wg6.a((Object) packageName, "info.packageName");
        if (!a(packageName)) {
            if (notificationInfo.getSource() == NotificationSource.CALL || notificationInfo.getSource() == NotificationSource.TEXT || notificationInfo.getSource() == NotificationSource.MAIL) {
                if (!c(notificationInfo)) {
                    if (notificationInfo.getSource() == NotificationSource.CALL) {
                        e(notificationInfo);
                    } else if (notificationInfo.getSource() == NotificationSource.TEXT) {
                        f(notificationInfo);
                    } else if (notificationInfo.getSource() == NotificationSource.MAIL) {
                        d(notificationInfo);
                    }
                }
            } else if (notificationInfo.getSource() == NotificationSource.OS) {
                b(notificationInfo);
            }
        }
    }

    @DexIgnore
    public final LightAndHaptics b(String str) {
        Contact a2 = wx5.a().a(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (a2 == null || !a2.isUseSms()) {
            return null;
        }
        NotificationType notificationType = NotificationType.SMS;
        String displayName = a2.getDisplayName();
        ContactGroup contactGroup = a2.getContactGroup();
        wg6.a((Object) contactGroup, "contact.contactGroup");
        return new LightAndHaptics("", notificationType, displayName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
    }

    @DexIgnore
    public /* synthetic */ LightAndHapticsManager(qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Check the  : " + this.c.size());
        if (!this.c.isEmpty()) {
            this.d = true;
            c();
            return;
        }
        this.d = false;
    }

    @DexIgnore
    public final void b(LightAndHaptics lightAndHaptics) {
        FLogger.INSTANCE.getLocal().d(g, "doPlayHandsNotification");
        NotificationType type = lightAndHaptics.getType();
        if (type != null) {
            int i2 = tm4.c[type.ordinal()];
            if (i2 == 1) {
                PortfolioApp instance = PortfolioApp.get.instance();
                String e2 = PortfolioApp.get.instance().e();
                NotificationBaseObj.ANotificationType aNotificationType = NotificationBaseObj.ANotificationType.INCOMING_CALL;
                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                String senderName = lightAndHaptics.getSenderName();
                wg6.a((Object) senderName, "item.senderName");
                String senderName2 = lightAndHaptics.getSenderName();
                wg6.a((Object) senderName2, "item.senderName");
                String senderName3 = lightAndHaptics.getSenderName();
                wg6.a((Object) senderName3, "item.senderName");
                DianaNotificationObj dianaNotificationObj = r5;
                DianaNotificationObj dianaNotificationObj2 = new DianaNotificationObj(0, aNotificationType, phone_incoming_call, senderName, senderName2, -1, senderName3, qd6.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), (Long) null, (NotificationBaseObj.NotificationControlActionStatus) null, (NotificationBaseObj.NotificationControlActionType) null, 1792, (qg6) null);
                instance.b(e2, (NotificationBaseObj) dianaNotificationObj);
            } else if (i2 == 2) {
                PortfolioApp instance2 = PortfolioApp.get.instance();
                String e3 = PortfolioApp.get.instance().e();
                NotificationBaseObj.ANotificationType aNotificationType2 = NotificationBaseObj.ANotificationType.TEXT;
                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                String senderName4 = lightAndHaptics.getSenderName();
                wg6.a((Object) senderName4, "item.senderName");
                String senderName5 = lightAndHaptics.getSenderName();
                wg6.a((Object) senderName5, "item.senderName");
                String senderName6 = lightAndHaptics.getSenderName();
                wg6.a((Object) senderName6, "item.senderName");
                DianaNotificationObj dianaNotificationObj3 = r5;
                DianaNotificationObj dianaNotificationObj4 = new DianaNotificationObj(0, aNotificationType2, messages, senderName4, senderName5, -1, senderName6, qd6.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), (Long) null, (NotificationBaseObj.NotificationControlActionStatus) null, (NotificationBaseObj.NotificationControlActionType) null, 1792, (qg6) null);
                instance2.b(e3, (NotificationBaseObj) dianaNotificationObj3);
            } else if (i2 == 3) {
                String senderName7 = lightAndHaptics.getSenderName();
                String packageName = senderName7 == null || xj6.a(senderName7) ? lightAndHaptics.getPackageName() : lightAndHaptics.getSenderName();
                CRC32 crc32 = new CRC32();
                String packageName2 = lightAndHaptics.getPackageName();
                wg6.a((Object) packageName2, "item.packageName");
                Charset charset = ej6.a;
                if (packageName2 != null) {
                    byte[] bytes = packageName2.getBytes(charset);
                    wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    crc32.update(bytes);
                    crc32.getValue();
                    wg6.a((Object) packageName, "appName");
                    String packageName3 = lightAndHaptics.getPackageName();
                    wg6.a((Object) packageName3, "item.packageName");
                    FNotification fNotification = new FNotification(packageName, packageName3, "", NotificationBaseObj.ANotificationType.NOTIFICATION);
                    PortfolioApp instance3 = PortfolioApp.get.instance();
                    String e4 = PortfolioApp.get.instance().e();
                    NotificationBaseObj.ANotificationType aNotificationType3 = NotificationBaseObj.ANotificationType.NOTIFICATION;
                    String packageName4 = lightAndHaptics.getPackageName();
                    wg6.a((Object) packageName4, "item.packageName");
                    String packageName5 = lightAndHaptics.getPackageName();
                    wg6.a((Object) packageName5, "item.packageName");
                    String packageName6 = lightAndHaptics.getPackageName();
                    wg6.a((Object) packageName6, "item.packageName");
                    instance3.b(e4, (NotificationBaseObj) new HybridNotificationObj(0, aNotificationType3, fNotification, packageName4, packageName5, -1, packageName6, qd6.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
    }

    @DexIgnore
    public final void a(NotificationInfo notificationInfo, boolean z) {
        wg6.b(notificationInfo, Constants.INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        LightAndHaptics b2 = b(notificationInfo.getSenderInfo());
        if (b2 == null && z) {
            String packageName = notificationInfo.getPackageName();
            wg6.a((Object) packageName, "info.packageName");
            if (a(packageName)) {
                String packageName2 = notificationInfo.getPackageName();
                wg6.a((Object) packageName2, "info.packageName");
                b2 = a(packageName2, notificationInfo.getBody());
            }
            if (b2 == null) {
                List<ContactGroup> b3 = wx5.a().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
                NotificationSource source = notificationInfo.getSource();
                wg6.a((Object) source, "info.source");
                b2 = a("-5678", (List<? extends ContactGroup>) b3, source);
            }
        }
        if (b2 != null) {
            a(b2);
        }
    }

    @DexIgnore
    public final void a(LightAndHaptics lightAndHaptics) {
        if (this.c.size() < 5) {
            this.c.add(lightAndHaptics);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "addEventToQueue : " + lightAndHaptics.getType());
            if (!this.d) {
                this.d = true;
                c();
            }
        }
    }

    @DexIgnore
    public final void c() {
        if (!this.c.isEmpty()) {
            try {
                LightAndHaptics lightAndHaptics = (LightAndHaptics) this.c.remove();
                wg6.a((Object) lightAndHaptics, "nextItem");
                b(lightAndHaptics);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Total duration: " + 500);
                this.e.postDelayed(this.f, 500);
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "exception when startQueue " + e2);
            }
        } else {
            this.d = false;
        }
    }

    @DexIgnore
    public final LightAndHaptics a(String str, List<? extends ContactGroup> list, NotificationSource notificationSource) {
        if (list != null && (!list.isEmpty())) {
            ContactGroup contactGroup = list.get(0);
            Contact contactWithPhoneNumber = contactGroup.getContactWithPhoneNumber(str);
            if (contactWithPhoneNumber == null) {
                return null;
            }
            String firstName = contactWithPhoneNumber.getFirstName();
            if (!TextUtils.isEmpty(contactWithPhoneNumber.getLastName()) && !xj6.b(contactWithPhoneNumber.getLastName(), "null", true)) {
                firstName = firstName + " " + contactWithPhoneNumber.getLastName();
            }
            String str2 = firstName;
            int i2 = tm4.b[notificationSource.ordinal()];
            if (i2 == 1) {
                return new LightAndHaptics("", NotificationType.CALL, str2, contactGroup.getHour(), NotificationPriority.ENTOURAGE_CALL);
            }
            if (i2 == 2) {
                return new LightAndHaptics("", NotificationType.SMS, str2, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
            }
        }
        return null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean a(String str) {
        if (TextUtils.equals(Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), str)) {
            an4 an4 = this.b;
            if (an4 == null) {
                wg6.d("mSharePref");
                throw null;
            } else if (an4.N()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final LightAndHaptics a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local.d(str3, "Attepting to match app " + str);
        AppFilter a2 = NotificationAppHelper.b.a(str, MFDeviceFamily.DEVICE_FAMILY_SAM);
        if (a2 == null) {
            return null;
        }
        AppFilterHistory appFilterHistory = new AppFilterHistory("");
        appFilterHistory.setColor(a2.getColor());
        appFilterHistory.setHaptic(a2.getHaptic());
        appFilterHistory.setTitle(a2.getName());
        appFilterHistory.setSubTitle(str2);
        appFilterHistory.setType(a2.getType());
        NotificationType notificationType = NotificationType.APP_FILTER;
        if (wg6.a((Object) str, (Object) AppType.ALL_CALLS.name())) {
            notificationType = NotificationType.CALL;
        } else if (wg6.a((Object) str, (Object) AppType.ALL_SMS.name())) {
            notificationType = NotificationType.SMS;
        } else if (wg6.a((Object) str, (Object) AppType.ALL_EMAIL.name())) {
            notificationType = NotificationType.EMAIL;
        }
        return new LightAndHaptics(str, notificationType, "", a2.getHour(), NotificationPriority.APP_FILTER);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean a() {
        for (AppFilter appFilter : zm4.p.a().a().getAllAppFilters()) {
            wg6.a((Object) appFilter, "item");
            if (xj6.b(appFilter.getType(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), true)) {
                FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return true");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return false");
        return false;
    }
}
