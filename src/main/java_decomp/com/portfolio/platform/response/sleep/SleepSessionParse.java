package com.portfolio.platform.response.sleep;

import com.fossil.ph4;
import com.fossil.wg6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionParse {
    @DexIgnore
    public /* final */ DateTime bookmarkTime;
    @DexIgnore
    public /* final */ DateTime createdAt;
    @DexIgnore
    public /* final */ Date date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ DateTime editedEndTime;
    @DexIgnore
    public /* final */ Integer editedSleepMinutes;
    @DexIgnore
    public /* final */ List<Integer> editedSleepStateDistInMinute;
    @DexIgnore
    public /* final */ DateTime editedStartTime;
    @DexIgnore
    public /* final */ SleepSessionHeartRate heartRate;
    @DexIgnore
    public /* final */ DateTime realEndTime;
    @DexIgnore
    public /* final */ int realSleepMinutes;
    @DexIgnore
    public /* final */ List<Integer> realSleepStateDistInMinute;
    @DexIgnore
    public /* final */ DateTime realStartTime;
    @DexIgnore
    public /* final */ double sleepQuality;
    @DexIgnore
    public /* final */ List<int[]> sleepStates;
    @DexIgnore
    public /* final */ String source;
    @DexIgnore
    public /* final */ DateTime syncTime;
    @DexIgnore
    public /* final */ int timezoneOffset;
    @DexIgnore
    public /* final */ DateTime updatedAt;

    @DexIgnore
    public SleepSessionParse(DateTime dateTime, DateTime dateTime2, Date date2, int i, String str, String str2, DateTime dateTime3, DateTime dateTime4, DateTime dateTime5, DateTime dateTime6, int i2, List<Integer> list, DateTime dateTime7, DateTime dateTime8, Integer num, List<Integer> list2, SleepSessionHeartRate sleepSessionHeartRate, double d, List<int[]> list3) {
        DateTime dateTime9 = dateTime5;
        DateTime dateTime10 = dateTime6;
        List<Integer> list4 = list;
        List<int[]> list5 = list3;
        wg6.b(dateTime, "createdAt");
        wg6.b(dateTime2, "updatedAt");
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        wg6.b(str, "source");
        wg6.b(dateTime9, "realStartTime");
        wg6.b(dateTime10, "realEndTime");
        wg6.b(list4, "realSleepStateDistInMinute");
        wg6.b(list5, "sleepStates");
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
        this.date = date2;
        this.timezoneOffset = i;
        this.source = str;
        this.deviceSerialNumber = str2;
        this.syncTime = dateTime3;
        this.bookmarkTime = dateTime4;
        this.realStartTime = dateTime9;
        this.realEndTime = dateTime10;
        this.realSleepMinutes = i2;
        this.realSleepStateDistInMinute = list4;
        this.editedStartTime = dateTime7;
        this.editedEndTime = dateTime8;
        this.editedSleepMinutes = num;
        this.editedSleepStateDistInMinute = list2;
        this.heartRate = sleepSessionHeartRate;
        this.sleepQuality = d;
        this.sleepStates = list5;
    }

    @DexIgnore
    public final MFSleepSession getMfSleepSessionBySleepSessionParse() {
        List<Integer> list = this.editedSleepStateDistInMinute;
        Integer num = null;
        SleepDistribution sleepDistribution = list != null ? new SleepDistribution(list) : null;
        SleepDistribution sleepDistribution2 = new SleepDistribution(this.realSleepStateDistInMinute);
        ArrayList arrayList = new ArrayList();
        int size = this.sleepStates.size();
        for (int i = 0; i < size; i++) {
            int[] iArr = this.sleepStates.get(i);
            if (iArr.length > 1) {
                arrayList.add(new WrapperSleepStateChange(iArr[0], (long) iArr[1]));
            }
        }
        String a = new Gson().a(arrayList);
        DateTime dateTime = this.editedStartTime;
        Integer valueOf = dateTime != null ? Integer.valueOf((int) (dateTime.getMillis() / ((long) 1000))) : null;
        DateTime dateTime2 = this.editedEndTime;
        Integer valueOf2 = dateTime2 != null ? Integer.valueOf((int) (dateTime2.getMillis() / ((long) 1000))) : null;
        DateTime dateTime3 = this.bookmarkTime;
        Long valueOf3 = dateTime3 != null ? Long.valueOf(dateTime3.getMillis() / ((long) 1000)) : null;
        DateTime dateTime4 = this.syncTime;
        Long valueOf4 = dateTime4 != null ? Long.valueOf(dateTime4.getMillis() / ((long) 1000)) : null;
        long time = this.date.getTime();
        Date date2 = this.date;
        int i2 = this.timezoneOffset;
        String str = this.deviceSerialNumber;
        Integer valueOf5 = valueOf4 != null ? Integer.valueOf((int) valueOf4.longValue()) : null;
        if (valueOf3 != null) {
            num = Integer.valueOf((int) valueOf3.longValue());
        }
        double d = this.sleepQuality;
        int ordinal = ph4.fromString(this.source).ordinal();
        long millis = this.realStartTime.getMillis();
        long j = (long) 1000;
        wg6.a((Object) a, "sleepStatesGson");
        return new MFSleepSession(time, date2, i2, str, valueOf5, num, d, ordinal, (int) (millis / j), (int) (this.realEndTime.getMillis() / j), this.realSleepMinutes, sleepDistribution2, valueOf, valueOf2, this.editedSleepMinutes, sleepDistribution, a, this.heartRate, this.createdAt, this.updatedAt);
    }
}
