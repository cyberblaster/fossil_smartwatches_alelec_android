package com.portfolio.platform.cloudimage;

import android.os.AsyncTask;
import com.fossil.hh6;
import com.fossil.jh6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UnzippingTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + UnzippingTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public OnUnzipFinishListener listener;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class UnzipTask extends AsyncTask<Void, Void, Boolean> {
            @DexIgnore
            public /* final */ String destinationUnzipPath;
            @DexIgnore
            public /* final */ OnUnzipFinishListener listener;
            @DexIgnore
            public /* final */ String zipFilePath;

            @DexIgnore
            public UnzipTask(String str, String str2, OnUnzipFinishListener onUnzipFinishListener) {
                wg6.b(str, "zipFilePath");
                wg6.b(str2, "destinationUnzipPath");
                this.zipFilePath = str;
                this.destinationUnzipPath = str2;
                this.listener = onUnzipFinishListener;
            }

            @DexIgnore
            public Boolean doInBackground(Void... voidArr) {
                FileOutputStream fileOutputStream;
                wg6.b(voidArr, "params");
                if (!new File(this.zipFilePath).exists()) {
                    return false;
                }
                byte[] bArr = new byte[2048];
                UnzippingTaskHelper.Companion.createDirectory$app_fossilRelease(this.destinationUnzipPath);
                FileInputStream fileInputStream = new FileInputStream(this.zipFilePath);
                ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
                try {
                    jh6 jh6 = new jh6();
                    while (true) {
                        jh6.element = zipInputStream.getNextEntry();
                        if (((ZipEntry) jh6.element) != null) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String tAG$app_fossilRelease = UnzippingTaskHelper.Companion.getTAG$app_fossilRelease();
                            StringBuilder sb = new StringBuilder();
                            sb.append("Unzipping ");
                            ZipEntry zipEntry = (ZipEntry) jh6.element;
                            if (zipEntry != null) {
                                sb.append(zipEntry.getName());
                                local.d(tAG$app_fossilRelease, sb.toString());
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(this.destinationUnzipPath);
                                sb2.append(ZendeskConfig.SLASH);
                                ZipEntry zipEntry2 = (ZipEntry) jh6.element;
                                if (zipEntry2 != null) {
                                    sb2.append(zipEntry2.getName());
                                    fileOutputStream = new FileOutputStream(sb2.toString());
                                    hh6 hh6 = new hh6();
                                    while (true) {
                                        try {
                                            hh6.element = zipInputStream.read(bArr);
                                            if (!(hh6.element > 0)) {
                                                break;
                                            }
                                            fileOutputStream.write(bArr, 0, hh6.element);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            zipInputStream.closeEntry();
                                        }
                                    }
                                    zipInputStream.closeEntry();
                                    fileOutputStream.close();
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String tAG$app_fossilRelease2 = UnzippingTaskHelper.Companion.getTAG$app_fossilRelease();
                            local2.d(tAG$app_fossilRelease2, "Unzipping completed, path = " + this.destinationUnzipPath);
                            zipInputStream.close();
                            fileInputStream.close();
                            return true;
                        }
                    }
                } catch (Exception e2) {
                    try {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String tAG$app_fossilRelease3 = UnzippingTaskHelper.Companion.getTAG$app_fossilRelease();
                        local3.e(tAG$app_fossilRelease3, "Unzipping failed, ex = " + e2);
                    } catch (Throwable unused) {
                    }
                    zipInputStream.close();
                    fileInputStream.close();
                    return false;
                } catch (Throwable th) {
                    zipInputStream.closeEntry();
                    fileOutputStream.close();
                    throw th;
                }
            }

            @DexIgnore
            public void onPostExecute(Boolean bool) {
                super.onPostExecute(bool);
                if (bool == null) {
                    wg6.a();
                    throw null;
                } else if (bool.booleanValue()) {
                    OnUnzipFinishListener onUnzipFinishListener = this.listener;
                    if (onUnzipFinishListener != null) {
                        onUnzipFinishListener.onUnzipSuccess(this.zipFilePath, this.destinationUnzipPath);
                    }
                } else {
                    OnUnzipFinishListener onUnzipFinishListener2 = this.listener;
                    if (onUnzipFinishListener2 != null) {
                        onUnzipFinishListener2.onUnzipFail(this.zipFilePath, this.destinationUnzipPath);
                    }
                }
            }
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final void createDirectory$app_fossilRelease(String str) {
            wg6.b(str, "destination");
            File file = new File(str);
            if (!file.isDirectory()) {
                file.mkdirs();
            }
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return UnzippingTaskHelper.TAG;
        }

        @DexIgnore
        public final UnzippingTaskHelper newInstance() {
            return new UnzippingTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface OnUnzipFinishListener {
        @DexIgnore
        void onUnzipFail(String str, String str2);

        @DexIgnore
        void onUnzipSuccess(String str, String str2);
    }

    @DexIgnore
    public static final UnzippingTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    public final void execute() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "execute() called with destinationUnzipPath = [" + this.destinationUnzipPath + ']');
        String str2 = this.zipFilePath;
        if (str2 != null) {
            String str3 = this.destinationUnzipPath;
            if (str3 != null) {
                new Companion.UnzipTask(str2, str3, this.listener).execute(new Void[0]);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void init(String str, String str2) {
        wg6.b(str, "zipFilePath");
        wg6.b(str2, "destinationUnzipPath");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
    }

    @DexIgnore
    public final void setOnUnzipFinishListener(OnUnzipFinishListener onUnzipFinishListener) {
        wg6.b(onUnzipFinishListener, "listener");
        this.listener = onUnzipFinishListener;
    }
}
