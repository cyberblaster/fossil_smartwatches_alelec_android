package com.portfolio.platform.cloudimage;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$2", f = "URLRequestTaskHelper.kt", l = {}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ AssetsDeviceResponse $assetsDeviceResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, AssetsDeviceResponse assetsDeviceResponse, xe6 xe6) {
        super(2, xe6);
        this.this$0 = uRLRequestTaskHelper;
        this.$assetsDeviceResponse = assetsDeviceResponse;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(this.this$0, this.$assetsDeviceResponse, xe6);
        uRLRequestTaskHelper$execute$Anon2.p$ = (il6) obj;
        return uRLRequestTaskHelper$execute$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((URLRequestTaskHelper$execute$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            URLRequestTaskHelper.OnNextTaskListener listener$app_fossilRelease = this.this$0.getListener$app_fossilRelease();
            if (listener$app_fossilRelease == null) {
                return null;
            }
            String zipFilePath$app_fossilRelease = this.this$0.getZipFilePath$app_fossilRelease();
            String destinationUnzipPath$app_fossilRelease = this.this$0.getDestinationUnzipPath$app_fossilRelease();
            AssetsDeviceResponse assetsDeviceResponse = this.$assetsDeviceResponse;
            wg6.a((Object) assetsDeviceResponse, "assetsDeviceResponse");
            listener$app_fossilRelease.downloadFile(zipFilePath$app_fossilRelease, destinationUnzipPath$app_fossilRelease, assetsDeviceResponse);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
