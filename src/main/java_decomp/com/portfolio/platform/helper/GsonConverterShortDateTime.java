package com.portfolio.platform.helper;

import com.fossil.bk4;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.lu3;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GsonConverterShortDateTime implements hu3<DateTime> {
    @DexIgnore
    public DateTime deserialize(JsonElement jsonElement, Type type, gu3 gu3) throws lu3 {
        String f = jsonElement.f();
        if (f.isEmpty()) {
            return null;
        }
        return bk4.b(f);
    }
}
