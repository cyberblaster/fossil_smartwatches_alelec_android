package com.portfolio.platform.migration;

import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.s24;
import com.fossil.sf6;
import com.fossil.t24;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MigrationHelper {
    @DexIgnore
    public /* final */ an4 a;
    @DexIgnore
    public /* final */ t24 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$1", f = "MigrationHelper.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $version;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(MigrationHelper migrationHelper, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = migrationHelper;
            this.$version = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$version, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                String n = this.this$0.a.n();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MigrationHelper", "start migration for " + this.$version + " lastVersion " + n);
                this.this$0.b.d();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public MigrationHelper(an4 an4, t24 t24, s24 s24) {
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(t24, "mMigrationManager");
        wg6.b(s24, "mLegacyMigrationManager");
        this.a = an4;
        this.b = t24;
    }

    @DexIgnore
    public final boolean a(String str) {
        wg6.b(str, "version");
        return this.a.m(str);
    }

    @DexIgnore
    public final rm6 b(String str) {
        wg6.b(str, "version");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new b(this, str, (xe6) null), 3, (Object) null);
    }
}
