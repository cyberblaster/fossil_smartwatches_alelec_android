package com.portfolio.platform.util;

import com.fossil.af6;
import com.fossil.aj6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.hg6;
import com.fossil.hk6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.ix5$c$a;
import com.fossil.jf6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rl6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.ti6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.fossil.zm4;
import com.fossil.zn4;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.response.ResponseKt;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceUtils {
    @DexIgnore
    public static DeviceUtils e;
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a((qg6) null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public an4 b;
    @DexIgnore
    public GuestApiService c;
    @DexIgnore
    public FirmwareFileRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(DeviceUtils deviceUtils) {
            DeviceUtils.e = deviceUtils;
        }

        @DexIgnore
        public final DeviceUtils b() {
            return DeviceUtils.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final synchronized DeviceUtils a() {
            DeviceUtils b;
            if (DeviceUtils.g.b() == null) {
                DeviceUtils.g.a(new DeviceUtils((qg6) null));
            }
            b = DeviceUtils.g.b();
            if (b == null) {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.util.DeviceUtils");
            }
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {76, 81}, m = "downloadActiveDeviceFirmware")
    public static final class b extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DeviceUtils deviceUtils, xe6 xe6) {
            super(xe6);
            this.this$0 = deviceUtils;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2", f = "DeviceUtils.kt", l = {97}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ap4 $repoResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DeviceUtils deviceUtils, ap4 ap4, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceUtils;
            this.$repoResponse = ap4;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$repoResponse, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0097  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
        public final Object invokeSuspend(Object obj) {
            c cVar;
            boolean z;
            il6 il6;
            List list;
            zn4 zn4;
            fh6 fh6;
            ti6 ti6;
            Iterator it;
            fh6 fh62;
            Object obj2;
            c cVar2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                ApiResponse apiResponse = (ApiResponse) ((cp4) this.$repoResponse).a();
                List<Firmware> list2 = apiResponse != null ? apiResponse.get_items() : null;
                if (list2 != null) {
                    zn4 e = zm4.p.a().e();
                    fh6 fh63 = new fh6();
                    fh63.element = true;
                    for (Firmware a2 : list2) {
                        e.a(a2);
                    }
                    ti6 c = aj6.c(yd6.b(list2), new ix5$c$a(this, il62));
                    il6 = il62;
                    list = list2;
                    fh62 = fh63;
                    ti6 = c;
                    cVar = this;
                    zn4 = e;
                    it = c.iterator();
                }
                return cd6.a;
            } else if (i == 1) {
                fh62 = (fh6) this.L$8;
                rl6 rl6 = (rl6) this.L$7;
                it = (Iterator) this.L$5;
                ti6 = (ti6) this.L$4;
                fh6 = (fh6) this.L$3;
                zn4 = (zn4) this.L$2;
                list = (List) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj2 = a;
                cVar2 = this;
                if (!((Boolean) obj).booleanValue()) {
                    cVar = cVar2;
                    a = obj2;
                    z = true;
                    fh62.element = z;
                    fh62 = fh6;
                }
                cVar = cVar2;
                a = obj2;
                z = false;
                fh62.element = z;
                fh62 = fh6;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!it.hasNext()) {
                Object next = it.next();
                rl6 rl62 = (rl6) next;
                if (fh62.element) {
                    cVar.L$0 = il6;
                    cVar.L$1 = list;
                    cVar.L$2 = zn4;
                    cVar.L$3 = fh62;
                    cVar.L$4 = ti6;
                    cVar.L$5 = it;
                    cVar.L$6 = next;
                    cVar.L$7 = rl62;
                    cVar.L$8 = fh62;
                    cVar.label = 1;
                    Object a3 = rl62.a(cVar);
                    if (a3 == a) {
                        return a;
                    }
                    obj2 = a;
                    cVar2 = cVar;
                    obj = a3;
                    fh6 = fh62;
                    if (!((Boolean) obj).booleanValue()) {
                        cVar = cVar2;
                        a = obj2;
                        z = false;
                        fh62.element = z;
                        fh62 = fh6;
                        if (!it.hasNext()) {
                        }
                    }
                    cVar = cVar2;
                    a = obj2;
                    z = true;
                    fh62.element = z;
                    fh62 = fh6;
                    if (!it.hasNext()) {
                    }
                    return a;
                }
                fh6 = fh62;
                z = false;
                fh62.element = z;
                fh62 = fh6;
                if (!it.hasNext()) {
                }
            }
            if (!fh62.element) {
                FLogger.INSTANCE.getLocal().e(DeviceUtils.f, "downloadActiveDeviceFirmware - download detail fw FAILED in one or more parts. Retry later.");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$repoResponse$1", f = "DeviceUtils.kt", l = {76}, m = "invokeSuspend")
    public static final class d extends sf6 implements hg6<xe6<? super rx6<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DeviceUtils deviceUtils, String str, xe6 xe6) {
            super(1, xe6);
            this.this$0 = deviceUtils;
            this.$model = str;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new d(this.this$0, this.$model, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((d) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                GuestApiService a2 = this.this$0.a();
                String h = PortfolioApp.get.instance().h();
                String str = this.$model;
                wg6.a((Object) str, "model");
                this.label = 1;
                obj = a2.getFirmwares(h, str, "android", this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {130}, m = "downloadDetailFirmware")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(DeviceUtils deviceUtils, xe6 xe6) {
            super(xe6);
            this.this$0 = deviceUtils;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((Firmware) null, (xe6<? super Boolean>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils$isDeviceDianaEV1Java$1", f = "DeviceUtils.kt", l = {}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DeviceUtils deviceUtils, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceUtils;
            this.$deviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$deviceSerial, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return hf6.a(this.this$0.a(this.$deviceSerial));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {197}, m = "isLatestFirmware")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(DeviceUtils deviceUtils, xe6 xe6) {
            super(xe6);
            this.this$0 = deviceUtils;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, (Device) null, this);
        }
    }

    /*
    static {
        String simpleName = DeviceUtils.class.getSimpleName();
        wg6.a((Object) simpleName, "DeviceUtils::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public DeviceUtils() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final GuestApiService a() {
        GuestApiService guestApiService = this.c;
        if (guestApiService != null) {
            return guestApiService;
        }
        wg6.d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final boolean b(String str) {
        wg6.b(str, "deviceSerial");
        return ((Boolean) hk6.a((af6) null, new f(this, str, (xe6) null), 1, (Object) null)).booleanValue();
    }

    @DexIgnore
    public /* synthetic */ DeviceUtils(qg6 qg6) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x017b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object a(xe6<? super cd6> xe6) {
        DeviceUtils deviceUtils;
        b bVar;
        int i;
        Object obj;
        Iterator it;
        String str;
        ArrayList arrayList;
        DeviceUtils deviceUtils2;
        DeviceUtils deviceUtils3;
        ArrayList arrayList2;
        String str2;
        Iterator it2;
        Object obj2;
        String str3;
        ap4 ap4;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof b) {
            bVar = (b) xe62;
            int i2 = bVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                bVar.label = i2 - Integer.MIN_VALUE;
                deviceUtils = this;
                Object obj3 = bVar.result;
                Object a2 = ff6.a();
                i = bVar.label;
                int i3 = 1;
                if (i != 0) {
                    nc6.a(obj3);
                    FLogger.INSTANCE.getLocal().d(f, "downloadActiveDeviceFirmware");
                    if (zm4.p.a().n().b() == null) {
                        return cd6.a;
                    }
                    ArrayList arrayList3 = new ArrayList();
                    str = PortfolioApp.get.instance().e();
                    if (str.length() > 0) {
                        List<String> c2 = DeviceHelper.o.c(str);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str4 = f;
                        local.d(str4, "active device skus " + c2);
                        if (!c2.isEmpty()) {
                            arrayList3.addAll(c2);
                        } else {
                            String[] b2 = DeviceHelper.o.b();
                            arrayList3.addAll(Arrays.asList((String[]) Arrays.copyOf(b2, b2.length)));
                        }
                    } else {
                        String[] b3 = DeviceHelper.o.b();
                        arrayList3.addAll(Arrays.asList((String[]) Arrays.copyOf(b3, b3.length)));
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str5 = f;
                    local2.d(str5, "download firmware for models " + arrayList3);
                    it = arrayList3.iterator();
                    obj = a2;
                    arrayList = arrayList3;
                    deviceUtils2 = deviceUtils;
                } else if (i == 1) {
                    it2 = (Iterator) bVar.L$4;
                    nc6.a(obj3);
                    DeviceUtils deviceUtils4 = (DeviceUtils) bVar.L$0;
                    obj2 = a2;
                    str3 = (String) bVar.L$3;
                    str2 = (String) bVar.L$2;
                    arrayList2 = (ArrayList) bVar.L$1;
                    deviceUtils3 = deviceUtils4;
                    ap4 = (ap4) obj3;
                    if (ap4 instanceof cp4) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str6 = f;
                        StringBuilder sb = new StringBuilder();
                        sb.append("downloadActiveDeviceFirmware - get latest fw SUCCESS isFromCache ");
                        cp4 cp4 = (cp4) ap4;
                        sb.append(cp4.b());
                        local3.d(str6, sb.toString());
                        if (!cp4.b()) {
                            dl6 b4 = zl6.b();
                            c cVar = new c(deviceUtils3, ap4, (xe6) null);
                            bVar.L$0 = deviceUtils3;
                            bVar.L$1 = arrayList2;
                            bVar.L$2 = str2;
                            bVar.L$3 = str3;
                            bVar.L$4 = it2;
                            bVar.L$5 = ap4;
                            bVar.label = 2;
                            if (gk6.a(b4, cVar, bVar) == obj2) {
                                return obj2;
                            }
                            a2 = obj2;
                            deviceUtils2 = deviceUtils3;
                            ArrayList arrayList4 = arrayList2;
                            obj = a2;
                            arrayList = arrayList4;
                            String str7 = str2;
                            it = it2;
                            str = str7;
                            i3 = 1;
                            return obj2;
                        }
                    } else if (ap4 instanceof zo4) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str8 = f;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("downloadActiveDeviceFirmware - get latest firmwares FAILED!!! {code=");
                        zo4 zo4 = (zo4) ap4;
                        sb2.append(zo4.a());
                        sb2.append(", message=");
                        ServerError c3 = zo4.c();
                        sb2.append(c3 != null ? c3.getMessage() : null);
                        sb2.append('}');
                        local4.e(str8, sb2.toString());
                    }
                    arrayList = arrayList2;
                    deviceUtils2 = deviceUtils3;
                    obj = obj2;
                    String str9 = str2;
                    it = it2;
                    str = str9;
                    i3 = 1;
                } else if (i == 2) {
                    ap4 ap42 = (ap4) bVar.L$5;
                    it2 = (Iterator) bVar.L$4;
                    String str10 = (String) bVar.L$3;
                    str2 = (String) bVar.L$2;
                    arrayList2 = (ArrayList) bVar.L$1;
                    deviceUtils3 = (DeviceUtils) bVar.L$0;
                    nc6.a(obj3);
                    deviceUtils2 = deviceUtils3;
                    ArrayList arrayList42 = arrayList2;
                    obj = a2;
                    arrayList = arrayList42;
                    String str72 = str2;
                    it = it2;
                    str = str72;
                    i3 = 1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (it.hasNext()) {
                    String str11 = (String) it.next();
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str12 = f;
                    local5.d(str12, "downloadActiveDeviceFirmware - get latest fw by model=" + str11);
                    d dVar = new d(deviceUtils2, str11, (xe6) null);
                    bVar.L$0 = deviceUtils2;
                    bVar.L$1 = arrayList;
                    bVar.L$2 = str;
                    bVar.L$3 = str11;
                    bVar.L$4 = it;
                    bVar.label = i3;
                    Object a3 = ResponseKt.a(dVar, bVar);
                    if (a3 == obj) {
                        return obj;
                    }
                    String str13 = str11;
                    deviceUtils3 = deviceUtils2;
                    obj3 = a3;
                    obj2 = obj;
                    arrayList2 = arrayList;
                    str3 = str13;
                    Iterator it3 = it;
                    str2 = str;
                    it2 = it3;
                    ap4 = (ap4) obj3;
                    if (ap4 instanceof cp4) {
                    }
                    arrayList = arrayList2;
                    deviceUtils2 = deviceUtils3;
                    obj = obj2;
                    String str92 = str2;
                    it = it2;
                    str = str92;
                    i3 = 1;
                    if (it.hasNext()) {
                    }
                    return obj;
                }
                return cd6.a;
            }
        }
        deviceUtils = this;
        bVar = new b(deviceUtils, xe62);
        Object obj32 = bVar.result;
        Object a22 = ff6.a();
        i = bVar.label;
        int i32 = 1;
        if (i != 0) {
        }
        if (it.hasNext()) {
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(Firmware firmware, xe6<? super Boolean> xe6) {
        e eVar;
        Object obj;
        int i;
        zn4 zn4;
        Firmware firmware2;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                boolean z = true;
                if (i != 0) {
                    nc6.a(obj);
                    zn4 e2 = zm4.p.a().e();
                    Firmware a3 = e2.a(firmware.getDeviceModel());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = f;
                    local.d(str, "downloadDetailFirmware - latestFw=" + firmware + ", localFw=" + a3 + ", from URL=" + firmware.getDownloadUrl());
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    String e3 = PortfolioApp.get.instance().e();
                    String str2 = f;
                    remote.i(component, session, e3, str2, "[FW Download] START DL detail FW of " + firmware.getVersionNumber());
                    if (a3 != null && xj6.b(a3.getVersionNumber(), firmware.getVersionNumber(), true)) {
                        FirmwareFileRepository firmwareFileRepository = this.d;
                        if (firmwareFileRepository != null) {
                            String versionNumber = a3.getVersionNumber();
                            wg6.a((Object) versionNumber, "localFirmware.versionNumber");
                            String checksum = a3.getChecksum();
                            wg6.a((Object) checksum, "localFirmware.checksum");
                            if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String str3 = f;
                                local2.d(str3, "Local fw of model=" + a3.getDeviceModel() + " is already latest, no need to re-download");
                                return hf6.a(z);
                            }
                        } else {
                            wg6.d("mFirmwareFileRepository");
                            throw null;
                        }
                    }
                    FirmwareFileRepository firmwareFileRepository2 = this.d;
                    if (firmwareFileRepository2 != null) {
                        String versionNumber2 = firmware.getVersionNumber();
                        wg6.a((Object) versionNumber2, "latestFirmware.versionNumber");
                        String downloadUrl = firmware.getDownloadUrl();
                        wg6.a((Object) downloadUrl, "latestFirmware.downloadUrl");
                        String checksum2 = firmware.getChecksum();
                        wg6.a((Object) checksum2, "latestFirmware.checksum");
                        eVar.L$0 = this;
                        eVar.L$1 = firmware;
                        eVar.L$2 = e2;
                        eVar.L$3 = a3;
                        eVar.label = 1;
                        Object downloadFirmware = firmwareFileRepository2.downloadFirmware(versionNumber2, downloadUrl, checksum2, eVar);
                        if (downloadFirmware == a2) {
                            return a2;
                        }
                        firmware2 = firmware;
                        zn4 = e2;
                        obj = downloadFirmware;
                    } else {
                        wg6.d("mFirmwareFileRepository");
                        throw null;
                    }
                } else if (i == 1) {
                    Firmware firmware3 = (Firmware) eVar.L$3;
                    zn4 = (zn4) eVar.L$2;
                    firmware2 = (Firmware) eVar.L$1;
                    DeviceUtils deviceUtils = (DeviceUtils) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (((File) obj) == null) {
                    zn4.a(firmware2);
                    IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                    FLogger.Component component2 = FLogger.Component.APP;
                    FLogger.Session session2 = FLogger.Session.OTHER;
                    String e4 = PortfolioApp.get.instance().e();
                    String str4 = f;
                    remote2.i(component2, session2, e4, str4, "[FW Download] DONE DL detail FW of " + firmware2.getVersionNumber());
                } else {
                    z = false;
                }
                return hf6.a(z);
            }
        }
        eVar = new e(this, xe6);
        obj = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        boolean z2 = true;
        if (i != 0) {
        }
        if (((File) obj) == null) {
        }
        return hf6.a(z2);
    }

    @DexIgnore
    public final boolean a(String str) {
        String firmwareRevision;
        wg6.b(str, "deviceSerial");
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
            return false;
        }
        DeviceRepository deviceRepository = this.a;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
            if (deviceBySerial == null || (firmwareRevision = deviceBySerial.getFirmwareRevision()) == null) {
                return false;
            }
            return xj6.c(firmwareRevision, "DN0.0.0.", true);
        }
        wg6.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object a(String str, Device device, xe6<? super Boolean> xe6) {
        g gVar;
        int i;
        Device device2;
        Firmware firmware;
        String str2 = str;
        Device device3 = device;
        xe6<? super Boolean> xe62 = xe6;
        if (xe62 instanceof g) {
            gVar = (g) xe62;
            int i2 = gVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                gVar.label = i2 - Integer.MIN_VALUE;
                Object obj = gVar.result;
                Object a2 = ff6.a();
                i = gVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("isLatestFirmware - deviceSerial=");
                    sb.append(str2);
                    sb.append(", activeDevice=");
                    sb.append(device3);
                    sb.append(", isSkipOTA=");
                    an4 an4 = this.b;
                    String str4 = null;
                    if (an4 != null) {
                        sb.append(an4.V());
                        local.d(str3, sb.toString());
                        an4 an42 = this.b;
                        if (an42 == null) {
                            wg6.d("mSharePrefs");
                            throw null;
                        } else if (an42.V()) {
                            return hf6.a(true);
                        } else {
                            if (device3 == null) {
                                DeviceRepository deviceRepository = this.a;
                                if (deviceRepository != null) {
                                    device2 = deviceRepository.getDeviceBySerial(str2);
                                } else {
                                    wg6.d("mDeviceRepository");
                                    throw null;
                                }
                            } else {
                                device2 = device3;
                            }
                            if (device2 == null) {
                                return hf6.a(true);
                            }
                            String firmwareRevision = device2.getFirmwareRevision();
                            if (!xj6.b("release", "release", true)) {
                                an4 an43 = this.b;
                                if (an43 == null) {
                                    wg6.d("mSharePrefs");
                                    throw null;
                                } else if (an43.D()) {
                                    an4 an44 = this.b;
                                    if (an44 != null) {
                                        firmware = an44.a(device2.getSku());
                                        if (firmware == null) {
                                            firmware = zm4.p.a().e().a(device2.getSku());
                                        }
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String str5 = f;
                                        local2.d(str5, "isLatestFirmware - latestFw=" + firmware);
                                        if (firmware != null) {
                                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                            String str6 = f;
                                            local3.e(str6, "isLatestFirmware - Error when update firmware, can't find latest fw of model=" + device2.getSku());
                                            gVar.L$0 = this;
                                            gVar.L$1 = str2;
                                            gVar.L$2 = device3;
                                            gVar.L$3 = device2;
                                            gVar.L$4 = firmwareRevision;
                                            gVar.L$5 = firmware;
                                            gVar.label = 1;
                                            if (a((xe6<? super cd6>) gVar) == a2) {
                                                return a2;
                                            }
                                        } else {
                                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                            String str7 = f;
                                            local4.d(str7, "isLatestFirmware - CheckFirmwareActiveDeviceUseCase currentDeviceFw=" + firmwareRevision + ", latestFw=" + firmware.getVersionNumber());
                                            return hf6.a(xj6.b(firmware.getVersionNumber(), firmwareRevision, true));
                                        }
                                    } else {
                                        wg6.d("mSharePrefs");
                                        throw null;
                                    }
                                }
                            }
                            firmware = zm4.p.a().e().a(device2.getSku());
                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                            FLogger.Component component = FLogger.Component.APP;
                            FLogger.Session session = FLogger.Session.OTHER;
                            String e2 = PortfolioApp.get.instance().e();
                            String str8 = f;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("[Check FW] Latest FW of SKU ");
                            sb2.append(device2.getSku());
                            sb2.append(" in DB ");
                            if (firmware != null) {
                                str4 = firmware.getVersionNumber();
                            }
                            sb2.append(str4);
                            remote.i(component, session, e2, str8, sb2.toString());
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            String str52 = f;
                            local22.d(str52, "isLatestFirmware - latestFw=" + firmware);
                            if (firmware != null) {
                            }
                        }
                    } else {
                        wg6.d("mSharePrefs");
                        throw null;
                    }
                } else if (i == 1) {
                    Firmware firmware2 = (Firmware) gVar.L$5;
                    String str9 = (String) gVar.L$4;
                    Device device4 = (Device) gVar.L$3;
                    Device device5 = (Device) gVar.L$2;
                    String str10 = (String) gVar.L$1;
                    DeviceUtils deviceUtils = (DeviceUtils) gVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return hf6.a(true);
            }
        }
        gVar = new g(this, xe62);
        Object obj2 = gVar.result;
        Object a22 = ff6.a();
        i = gVar.label;
        if (i != 0) {
        }
        return hf6.a(true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b8  */
    public final lc6<String, String> a(String str, Device device) {
        Firmware firmware;
        wg6.b(str, "deviceSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = f;
        local.d(str2, "fetchFirmwaresInfo - deviceSerial=" + str + ", activeDevice=" + device);
        String str3 = null;
        if (device == null) {
            DeviceRepository deviceRepository = this.a;
            if (deviceRepository != null) {
                device = deviceRepository.getDeviceBySerial(str);
            } else {
                wg6.d("mDeviceRepository");
                throw null;
            }
        }
        if (device == null) {
            return new lc6<>(null, null);
        }
        String firmwareRevision = device.getFirmwareRevision();
        if (!xj6.b("release", "release", true)) {
            an4 an4 = this.b;
            if (an4 == null) {
                wg6.d("mSharePrefs");
                throw null;
            } else if (an4.D()) {
                an4 an42 = this.b;
                if (an42 != null) {
                    firmware = an42.a(device.getSku());
                    if (firmware == null) {
                        firmware = zm4.p.a().e().a(device.getSku());
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = f;
                    local2.d(str4, "fetchFirmwaresInfo - latestFw=" + firmware);
                    if (firmware != null) {
                        str3 = firmware.getVersionNumber();
                    }
                    return new lc6<>(firmwareRevision, str3);
                }
                wg6.d("mSharePrefs");
                throw null;
            }
        }
        firmware = zm4.p.a().e().a(device.getSku());
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str42 = f;
        local22.d(str42, "fetchFirmwaresInfo - latestFw=" + firmware);
        if (firmware != null) {
        }
        return new lc6<>(firmwareRevision, str3);
    }
}
