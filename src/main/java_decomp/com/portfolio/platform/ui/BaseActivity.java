package com.portfolio.platform.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bn4;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.gy5;
import com.fossil.hc;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.n6;
import com.fossil.nc6;
import com.fossil.os4;
import com.fossil.pw6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.t24;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zm4;
import com.fossil.zx5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.ProgressDialogFragment;
import java.lang.reflect.Method;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class BaseActivity extends AppCompatActivity implements AlertDialogFragment.g, pw6.a {
    @DexIgnore
    public static /* final */ a A; // = new a((qg6) null);
    @DexIgnore
    public static IButtonConnectivity z;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Handler d; // = new Handler();
    @DexIgnore
    public View e;
    @DexIgnore
    public TextView f;
    @DexIgnore
    public TextView g;
    @DexIgnore
    public TextView h;
    @DexIgnore
    public TextView i;
    @DexIgnore
    public TextView j;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public UserRepository p;
    @DexIgnore
    public an4 q;
    @DexIgnore
    public DeviceRepository r;
    @DexIgnore
    public t24 s;
    @DexIgnore
    public os4 t;
    @DexIgnore
    public String u;
    @DexIgnore
    public ProgressDialogFragment v;
    @DexIgnore
    public /* final */ d w; // = new d(this);
    @DexIgnore
    public /* final */ e x; // = new e(this);
    @DexIgnore
    public /* final */ Runnable y; // = new b(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final IButtonConnectivity a() {
            return BaseActivity.z;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            BaseActivity.z = iButtonConnectivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements m24.e<os4.e, os4.b> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(os4.e eVar) {
                wg6.b(eVar, "responseValue");
                this.a.a.b(String.valueOf(eVar.a()));
            }

            @DexIgnore
            public void a(os4.b bVar) {
                wg6.b(bVar, "errorValue");
                this.a.a.b("Disconnected");
            }
        }

        @DexIgnore
        public b(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v4, types: [com.fossil.os4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.ui.BaseActivity$b$a] */
        public final void run() {
            if (!TextUtils.isEmpty(this.a.u)) {
                Object c = this.a.c();
                String a2 = this.a.u;
                if (a2 != null) {
                    c.a(new os4.d(a2), new a(this));
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore
        public c(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public final void run() {
            try {
                if (this.a.v == null) {
                    ProgressDialogFragment b = this.a.getSupportFragmentManager().b("ProgressDialogFragment");
                    if (b != null) {
                        this.a.v = b;
                    }
                }
                if (this.a.v != null) {
                    FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog dismissAllowingStateLoss");
                    ProgressDialogFragment b2 = this.a.v;
                    if (b2 != null) {
                        b2.dismissAllowingStateLoss();
                        this.a.v = null;
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f = this.a.f();
                local.d(f, "Exception when dismiss progress dialog=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1", f = "BaseActivity.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = dVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    PortfolioApp.inner inner = PortfolioApp.get;
                    IButtonConnectivity a = BaseActivity.A.a();
                    if (a != null) {
                        inner.b(a);
                        try {
                            MFUser currentUser = this.this$0.a.d().getCurrentUser();
                            if (!(currentUser == null || BaseActivity.A.a() == null)) {
                                IButtonConnectivity a2 = BaseActivity.A.a();
                                if (a2 != null) {
                                    a2.updateUserId(currentUser.getUserId());
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            this.this$0.a.n();
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String f = this.this$0.a.f();
                            local.e(f, ".onServiceConnected(), ex=" + e);
                            e.printStackTrace();
                        }
                        return cd6.a;
                    }
                    wg6.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public d(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            wg6.b(componentName, "name");
            wg6.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.f(), "Button service connected");
            BaseActivity.A.a(IButtonConnectivity.Stub.asInterface(iBinder));
            this.a.b(true);
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            wg6.b(componentName, "name");
            FLogger.INSTANCE.getLocal().d(this.a.f(), "Button service disconnected");
            this.a.b(false);
            BaseActivity.A.a((IButtonConnectivity) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore
        public e(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            wg6.b(componentName, "name");
            wg6.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.f(), "Misfit service connected");
            MFDeviceService.b bVar = (MFDeviceService.b) iBinder;
            this.a.a(bVar.a());
            PortfolioApp.get.b(bVar);
            this.a.c(true);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            wg6.b(componentName, "name");
            this.a.c(false);
            this.a.a((MFDeviceService) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore
        public f(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public final void run() {
            FragmentManager supportFragmentManager = this.a.getSupportFragmentManager();
            wg6.a((Object) supportFragmentManager, "supportFragmentManager");
            if (supportFragmentManager.t() > 1) {
                this.a.getSupportFragmentManager().E();
            } else {
                this.a.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public g(BaseActivity baseActivity, String str, boolean z) {
            this.a = baseActivity;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
        public final void run() {
            try {
                if (!this.a.isDestroyed()) {
                    if (!this.a.isFinishing()) {
                        this.a.v = ProgressDialogFragment.c.a(this.b);
                        ProgressDialogFragment b2 = this.a.v;
                        if (b2 != null) {
                            b2.setCancelable(this.c);
                            hc b3 = this.a.getSupportFragmentManager().b();
                            wg6.a((Object) b3, "supportFragmentManager.beginTransaction()");
                            ProgressDialogFragment b4 = this.a.v;
                            if (b4 != null) {
                                b3.a(b4, "ProgressDialogFragment");
                                b3.b();
                                return;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a.f(), "Activity is destroy or finishing, no need to show dialog");
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f = this.a.f();
                local.d(f, "Exception when showing progress dialog=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1", f = "BaseActivity.kt", l = {695}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Device $activeDevice$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(xe6 xe6, h hVar, Device device) {
                super(2, xe6);
                this.this$0 = hVar;
                this.$activeDevice$inlined = device;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(xe6, this.this$0, this.$activeDevice$inlined);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    TextView f = this.this$0.this$0.f;
                    if (f != null) {
                        f.setText(this.$activeDevice$inlined.getDeviceId());
                        TextView d = this.this$0.this$0.g;
                        if (d != null) {
                            d.setText(String.valueOf(this.$activeDevice$inlined.getBatteryLevel()));
                            TextView e = this.this$0.this$0.i;
                            if (e != null) {
                                e.setText(this.$activeDevice$inlined.getFirmwareRevision());
                                this.this$0.this$0.u = this.$activeDevice$inlined.getDeviceId();
                                return cd6.a;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(BaseActivity baseActivity, xe6 xe6) {
            super(2, xe6);
            this.this$0 = baseActivity;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                DeviceRepository b = this.this$0.b();
                String a3 = this.this$0.u;
                if (a3 != null) {
                    Device deviceBySerial = b.getDeviceBySerial(a3);
                    if (deviceBySerial != null) {
                        cn6 c = zl6.c();
                        a aVar = new a((xe6) null, this, deviceBySerial);
                        this.L$0 = il6;
                        this.L$1 = deviceBySerial;
                        this.L$2 = deviceBySerial;
                        this.label = 1;
                        if (gk6.a(c, aVar, this) == a2) {
                            return a2;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                Device device = (Device) this.L$2;
                Device device2 = (Device) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser b2 = zm4.p.a().n().b();
            String accessTokenExpiresAt = b2 != null ? b2.getAccessTokenExpiresAt() : null;
            TextView c2 = this.this$0.j;
            if (c2 != null) {
                if (accessTokenExpiresAt == null) {
                    accessTokenExpiresAt = "";
                }
                c2.setText(accessTokenExpiresAt);
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public BaseActivity() {
        String simpleName = getClass().getSimpleName();
        wg6.a((Object) simpleName, "this.javaClass.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
    }

    @DexIgnore
    public final void a(MFDeviceService mFDeviceService) {
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public void finish() {
        BaseActivity.super.finish();
        if (i()) {
            overridePendingTransition(2130772015, 2130772018);
        }
    }

    @DexIgnore
    public final int g() {
        try {
            Method method = Context.class.getMethod("getThemeResId", new Class[0]);
            wg6.a((Object) method, "currentClass.getMethod(\"getThemeResId\")");
            method.setAccessible(true);
            Object invoke = method.invoke(this, new Object[0]);
            if (invoke != null) {
                return ((Integer) invoke).intValue();
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Int");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(this.a, "Failed to get theme resource ID");
            return 0;
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog");
        this.d.post(new c(this));
    }

    @DexIgnore
    public final boolean i() {
        return g() == 2131951631;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public final void j() {
        try {
            n6.c(this);
        } catch (IllegalArgumentException unused) {
            finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public final void k() {
        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public final void l() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getPackageName(), (String) null));
        startActivity(intent);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public final void m() {
        if (Build.VERSION.SDK_INT >= 29) {
            startActivity(new Intent("android.settings.panel.action.INTERNET_CONNECTIVITY"));
        } else {
            startActivity(new Intent("android.settings.SETTINGS"));
        }
    }

    @DexIgnore
    public final synchronized void n() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("needToUpdateBLEWhenUpgradeLegacy - isNeedToUpdateBLE=");
        an4 an4 = this.q;
        if (an4 != null) {
            sb.append(an4.M());
            local.d(str, sb.toString());
            an4 an42 = this.q;
            if (an42 == null) {
                wg6.d("mSharePrefs");
                throw null;
            } else if (an42.M()) {
                try {
                    t24 t24 = this.s;
                    if (t24 != null) {
                        t24.c();
                    } else {
                        wg6.d("mMigrationManager");
                        throw null;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = this.a;
                    local2.e(str2, "needToUpdateBLEWhenUpgradeLegacy - e=" + e2);
                }
            }
        } else {
            wg6.d("mSharePrefs");
            throw null;
        }
        return;
    }

    @DexIgnore
    public final void o() {
        View view = this.e;
        if (view != null && this.o) {
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(this.e);
                    this.o = false;
                    this.d.removeCallbacks(this.y);
                } else {
                    throw new rc6("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        try {
            os4 os4 = this.t;
            if (os4 != null) {
                os4.g();
            } else {
                wg6.d("mGetRssi");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public void onBackPressed() {
        runOnUiThread(new f(this));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.ui.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity] */
    public void onCreate(Bundle bundle) {
        PortfolioApp.get.instance().g().a((BaseActivity) this);
        BaseActivity.super.onCreate(bundle);
        if (i()) {
            overridePendingTransition(2130772016, 2130772017);
        }
        bn4.o.a().b();
        Window window = getWindow();
        wg6.a((Object) window, "window");
        View decorView = window.getDecorView();
        wg6.a((Object) decorView, "window.decorView");
        decorView.setSystemUiVisibility(3328);
        AnalyticsHelper.f.c();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        wg6.b(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            j();
        }
        return BaseActivity.super.onOptionsItemSelected(menuItem);
    }

    @DexIgnore
    public void onPause() {
        BaseActivity.super.onPause();
        try {
            PortfolioApp.get.c(this);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.e(str, "Inside " + this.a + ".onPause - exception=" + e2);
        }
        if (!PortfolioApp.get.instance().F()) {
            an4 an4 = this.q;
            if (an4 == null) {
                wg6.d("mSharePrefs");
                throw null;
            } else if (an4.K()) {
                o();
            }
        }
    }

    @DexIgnore
    public void onResume() {
        BaseActivity.super.onResume();
        PortfolioApp.get.b((Object) this);
        a(false);
        if (PortfolioApp.get.f()) {
            bn4.o.a().b();
        }
        if (!PortfolioApp.get.instance().F()) {
            an4 an4 = this.q;
            if (an4 == null) {
                wg6.d("mSharePrefs");
                throw null;
            } else if (an4.K()) {
                a();
            }
        }
    }

    @DexIgnore
    public void onStart() {
        BaseActivity.super.onStart();
        FLogger.INSTANCE.getLocal().d(this.a, "onStart()");
    }

    @DexIgnore
    public void onStop() {
        BaseActivity.super.onStop();
        FLogger.INSTANCE.getLocal().d(this.a, "onStop()");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public void onTrimMemory(int i2) {
        BaseActivity.super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final void p() {
        a(this, false, (String) null, 2, (Object) null);
    }

    @DexIgnore
    public final void q() {
        this.u = PortfolioApp.get.instance().e();
        if (!TextUtils.isEmpty(this.u)) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
            b("Disconnected");
        }
    }

    @DexIgnore
    public void setContentView(int i2) {
        BaseActivity.super.setContentView(i2);
    }

    @DexIgnore
    public void startActivityForResult(Intent intent, int i2) {
        if (intent == null) {
            intent = new Intent();
        }
        BaseActivity.super.startActivityForResult(intent, i2);
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.c = z2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.b = z2;
    }

    @DexIgnore
    public final UserRepository d() {
        UserRepository userRepository = this.p;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final int e() {
        Resources resources = PortfolioApp.get.instance().getResources();
        int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return resources.getDimensionPixelSize(identifier);
        }
        return 0;
    }

    @DexIgnore
    public final String f() {
        return this.a;
    }

    @DexIgnore
    public final DeviceRepository b() {
        DeviceRepository deviceRepository = this.r;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wg6.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final os4 c() {
        os4 os4 = this.t;
        if (os4 != null) {
            return os4;
        }
        wg6.d("mGetRssi");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity] */
    public final <T extends Service> void b(Class<? extends T>... clsArr) {
        wg6.b(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.a, "unbindServices()");
        for (Class<? extends T> cls : clsArr) {
            if (wg6.a((Object) cls, (Object) MFDeviceService.class)) {
                if (this.b) {
                    FLogger.INSTANCE.getLocal().d(this.a, "Unbinding from mIsMisfitServiceBound");
                    zx5.a.a((Context) this, (ServiceConnection) this.x);
                    this.b = false;
                }
            } else if (wg6.a((Object) cls, (Object) ButtonService.class) && this.c) {
                FLogger.INSTANCE.getLocal().d(this.a, "Unbinding from mButtonServiceBound");
                zx5.a.a((Context) this, (ServiceConnection) this.w);
                this.c = false;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity] */
    public <T extends Service> void a(Class<? extends T>... clsArr) {
        wg6.b(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.a, "executeServices()");
        for (Class<? extends T> cls : clsArr) {
            if (wg6.a((Object) cls, (Object) MFDeviceService.class)) {
                zx5.a.b(this, MFDeviceService.class, this.x, 0);
            } else if (wg6.a((Object) cls, (Object) ButtonService.class)) {
                zx5.a.b(this, ButtonService.class, this.w, 1);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    public final void a() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "displayDeviceInfoView() called: isDeviceInfoViewAdded = " + this.o);
        if (!this.o) {
            Object systemService = getSystemService("layout_inflater");
            if (systemService != null) {
                this.e = ((LayoutInflater) systemService).inflate(2131558802, (ViewGroup) null);
                View view = this.e;
                if (view != null) {
                    view.setY((float) e());
                    View view2 = this.e;
                    if (view2 != null) {
                        this.f = (TextView) view2.findViewById(2131363142);
                        View view3 = this.e;
                        if (view3 != null) {
                            this.g = (TextView) view3.findViewById(2131363131);
                            View view4 = this.e;
                            if (view4 != null) {
                                this.h = (TextView) view4.findViewById(2131363140);
                                View view5 = this.e;
                                if (view5 != null) {
                                    this.i = (TextView) view5.findViewById(2131363135);
                                    View view6 = this.e;
                                    if (view6 != null) {
                                        TextView textView = (TextView) view6.findViewById(2131363144);
                                        View view7 = this.e;
                                        if (view7 != null) {
                                            TextView textView2 = (TextView) view7.findViewById(2131363137);
                                            View view8 = this.e;
                                            if (view8 != null) {
                                                TextView textView3 = (TextView) view8.findViewById(2131363133);
                                                View view9 = this.e;
                                                if (view9 != null) {
                                                    this.j = (TextView) view9.findViewById(2131363072);
                                                    getWindow().addContentView(this.e, new ConstraintLayout.LayoutParams((int) gy5.a(170, (Context) this), (int) gy5.a(130, (Context) this)));
                                                    this.o = true;
                                                    q();
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type android.view.LayoutInflater");
            }
        }
        os4 os4 = this.t;
        if (os4 != null) {
            os4.f();
        } else {
            wg6.d("mGetRssi");
            throw null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "rssiInfo");
        TextView textView = this.h;
        if (textView != null) {
            textView.setText(str);
            this.d.postDelayed(this.y, 1000);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, Explore.COLUMN_TITLE);
        a(false, str);
    }

    @DexIgnore
    public static /* synthetic */ void a(BaseActivity baseActivity, boolean z2, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                str = "";
            }
            baseActivity.a(z2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showLoadingDialog");
    }

    @DexIgnore
    public final void a(boolean z2, String str) {
        wg6.b(str, Explore.COLUMN_TITLE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "showLoadingDialog: cancelable = " + z2);
        h();
        this.d.post(new g(this, str, z2));
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "Inside .onDialogFragmentResult tag=" + str);
        if (str.hashCode() == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131362354) {
            k();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity] */
    @TargetApi(23)
    public final void a(boolean z2) {
        if (Build.VERSION.SDK_INT >= 23) {
            Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(Integer.MIN_VALUE);
            wg6.a((Object) window, "window");
            View decorView = window.getDecorView();
            wg6.a((Object) decorView, "window.decorView");
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (z2) {
                decorView.setSystemUiVisibility(systemUiVisibility & -8193);
            } else {
                decorView.setSystemUiVisibility(systemUiVisibility | 8192);
            }
        }
    }

    @DexIgnore
    public final void a(Fragment fragment, int i2) {
        wg6.b(fragment, "fragment");
        a(fragment, (String) null, i2);
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i2) {
        wg6.b(fragment, "fragment");
        hc b2 = getSupportFragmentManager().b();
        b2.b(i2, fragment, str);
        b2.a(str);
        b2.b();
    }
}
