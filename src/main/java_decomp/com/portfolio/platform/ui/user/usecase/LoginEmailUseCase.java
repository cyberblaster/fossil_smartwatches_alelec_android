package com.portfolio.platform.ui.user.usecase;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.kt4;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginEmailUseCase extends m24<kt4.c, kt4.d, kt4.b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            wg6.b(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(String str, String str2) {
            wg6.b(str, "email");
            wg6.b(str2, "password");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public d(Auth auth) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.LoginEmailUseCase", f = "LoginEmailUseCase.kt", l = {24}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ LoginEmailUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(LoginEmailUseCase loginEmailUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = loginEmailUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((kt4.c) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = LoginEmailUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "LoginEmailUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public LoginEmailUseCase(UserRepository userRepository) {
        wg6.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public Object a(kt4.c cVar, xe6<Object> xe6) {
        e eVar;
        int i;
        ap4 ap4;
        String str;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    if (cVar == null) {
                        return new b(600, "");
                    }
                    String a3 = cVar.a();
                    if (a3 != null) {
                        String lowerCase = a3.toLowerCase();
                        wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        UserRepository userRepository = this.d;
                        String b2 = cVar.b();
                        eVar.L$0 = this;
                        eVar.L$1 = cVar;
                        eVar.L$2 = lowerCase;
                        eVar.label = 1;
                        obj = userRepository.loginEmail(lowerCase, b2, eVar);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                } else if (i == 1) {
                    String str2 = (String) eVar.L$2;
                    c cVar2 = (c) eVar.L$1;
                    LoginEmailUseCase loginEmailUseCase = (LoginEmailUseCase) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new d((Auth) ((cp4) ap4).a());
                }
                if (!(ap4 instanceof zo4)) {
                    return new b(600, "");
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .run failed with http code=");
                zo4 zo4 = (zo4) ap4;
                sb.append(zo4.a());
                local.d(str3, sb.toString());
                int a4 = zo4.a();
                ServerError c2 = zo4.c();
                if (c2 == null || (str = c2.getMessage()) == null) {
                    str = "";
                }
                return new b(a4, str);
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
