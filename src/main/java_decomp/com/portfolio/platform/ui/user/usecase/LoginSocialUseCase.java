package com.portfolio.platform.ui.user.usecase;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.nt4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginSocialUseCase extends m24<nt4.c, nt4.d, nt4.b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public b(int i, String str) {
            wg6.b(str, "errorMessage");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(String str, String str2, String str3) {
            wg6.b(str, Constants.SERVICE);
            wg6.b(str2, "token");
            wg6.b(str3, "clientId");
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public d(Auth auth) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase", f = "LoginSocialUseCase.kt", l = {23}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ LoginSocialUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(LoginSocialUseCase loginSocialUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = loginSocialUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((nt4.c) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = LoginSocialUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "LoginSocialUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public LoginSocialUseCase(UserRepository userRepository) {
        wg6.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public Object a(nt4.c cVar, xe6<Object> xe6) {
        e eVar;
        int i;
        ap4 ap4;
        String str;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    if (cVar == null) {
                        return new b(600, "");
                    }
                    UserRepository userRepository = this.d;
                    String b2 = cVar.b();
                    String c2 = cVar.c();
                    String a3 = cVar.a();
                    eVar.L$0 = this;
                    eVar.L$1 = cVar;
                    eVar.label = 1;
                    obj = userRepository.loginWithSocial(b2, c2, a3, eVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    c cVar2 = (c) eVar.L$1;
                    LoginSocialUseCase loginSocialUseCase = (LoginSocialUseCase) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new d((Auth) ((cp4) ap4).a());
                }
                if (!(ap4 instanceof zo4)) {
                    return new b(600, "");
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .run failed with http code=");
                zo4 zo4 = (zo4) ap4;
                sb.append(zo4.a());
                local.d(str2, sb.toString());
                int a4 = zo4.a();
                ServerError c3 = zo4.c();
                if (c3 == null || (str = c3.getMessage()) == null) {
                    str = "";
                }
                return new b(a4, str);
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
