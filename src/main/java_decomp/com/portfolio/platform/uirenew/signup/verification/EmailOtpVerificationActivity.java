package com.portfolio.platform.uirenew.signup.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.au5;
import com.fossil.eu5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EmailOtpVerificationActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public au5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            wg6.b(context, "context");
            wg6.b(signUpEmailAuth, "emailAuth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EmailOtpVerificationActivity", "start verify Otp with Email Auth =" + signUpEmailAuth);
            Intent intent = new Intent(context, EmailOtpVerificationActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        EmailOtpVerificationFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = EmailOtpVerificationFragment.u.a();
            a((Fragment) b, f(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new eu5(this, b)).a(this);
            Intent intent = getIntent();
            if (intent != null && intent.hasExtra("EMAIL_AUTH")) {
                FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent emailAddress");
                au5 au5 = this.B;
                if (au5 != null) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("EMAIL_AUTH");
                    wg6.a((Object) parcelableExtra, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                    au5.a((SignUpEmailAuth) parcelableExtra);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationContract.View");
    }
}
