package com.portfolio.platform.uirenew.welcome;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.hu5;
import com.fossil.iu5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SplashScreenFragment extends BaseFragment implements iu5 {
    @DexIgnore
    public static /* final */ a h; // = new a((qg6) null);
    @DexIgnore
    public hu5 f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SplashScreenFragment a() {
            return new SplashScreenFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wg6.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
            activity.finish();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void k0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558615, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(2131362606);
        String b = ThemeManager.l.a().b("primaryText");
        if (b != null) {
            wg6.a((Object) imageView, "ivLogo");
            imageView.setImageTintList(ColorStateList.valueOf(Color.parseColor(b)));
        }
        return inflate;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        SplashScreenFragment.super.onPause();
        hu5 hu5 = this.f;
        if (hu5 != null) {
            hu5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        SplashScreenFragment.super.onResume();
        hu5 hu5 = this.f;
        if (hu5 != null) {
            hu5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void x0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void a(hu5 hu5) {
        wg6.b(hu5, "presenter");
        this.f = hu5;
    }
}
