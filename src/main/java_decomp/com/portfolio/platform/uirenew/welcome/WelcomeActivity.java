package com.portfolio.platform.uirenew.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.bw5;
import com.fossil.cw5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zx5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WelcomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public bw5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            context.startActivity(new Intent(context, WelcomeActivity.class));
        }

        @DexIgnore
        public final void b(Context context) {
            wg6.b(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        WelcomeFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = new WelcomeFragment();
            a((Fragment) b, "WelcomeFragment", 2131362119);
        }
        PortfolioApp.get.instance().g().a(new cw5(b)).a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.uirenew.welcome.WelcomeActivity, com.portfolio.platform.ui.BaseActivity] */
    public void onStart() {
        super.onStart();
        zx5.a.a(this, MFDeviceService.class, Constants.STOP_FOREGROUND_ACTION);
        zx5.a.a(this, ButtonService.class, Constants.STOP_FOREGROUND_ACTION);
    }
}
