package com.portfolio.platform.uirenew.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.AlarmPresenter;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rv4;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public AlarmPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
            wg6.b(context, "context");
            wg6.b(str, "deviceId");
            wg6.b(arrayList, "currentAlarms");
            Intent intent = new Intent(context, AlarmActivity.class);
            intent.putExtra("EXTRA_DEVICE_ID", str);
            intent.putExtra("EXTRA_ALARM", alarm);
            intent.putParcelableArrayListExtra("EXTRA_ALARMS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.alarm.AlarmActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        String stringExtra = getIntent().getStringExtra("EXTRA_DEVICE_ID");
        Alarm alarm = (Alarm) getIntent().getParcelableExtra("EXTRA_ALARM");
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_ALARMS");
        AlarmFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = AlarmFragment.q.b();
            a((Fragment) b, AlarmFragment.q.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            wg6.a((Object) stringExtra, "deviceId");
            wg6.a((Object) parcelableArrayListExtra, "alarms");
            g.a(new rv4(b, stringExtra, parcelableArrayListExtra, alarm)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmContract.View");
    }
}
