package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ik6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.ui4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yv4;
import com.fossil.yv4$b$a;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAlarm extends m24<yv4.d, yv4.e, yv4.c> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public d e;
    @DexIgnore
    public /* final */ b f; // = new b();
    @DexIgnore
    public /* final */ AlarmsRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteAlarm.h;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm] */
        /* JADX WARNING: type inference failed for: r8v8, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && DeleteAlarm.this.d()) {
                DeleteAlarm.this.a(false);
                FLogger.INSTANCE.getLocal().d(DeleteAlarm.i.a(), "onReceive");
                Alarm a2 = DeleteAlarm.this.e().a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(DeleteAlarm.i.a(), "onReceive success");
                    rm6 unused = ik6.b(DeleteAlarm.this.b(), (af6) null, (ll6) null, new yv4$b$a(this, a2, (xe6) null), 3, (Object) null);
                    DeleteAlarm.this.a(new e(a2));
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = DeleteAlarm.i.a();
                local.d(a3, "onReceive error - errorCode=" + intExtra);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                DeleteAlarm.this.a(new c(a2, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public c(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            wg6.b(arrayList, "errorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public d(String str, List<Alarm> list, Alarm alarm) {
            wg6.b(str, "deviceId");
            wg6.b(list, "alarms");
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.d {
        @DexIgnore
        public e(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm", f = "DeleteAlarm.kt", l = {87}, m = "run")
    public static final class f extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAlarm this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DeleteAlarm deleteAlarm, xe6 xe6) {
            super(xe6);
            this.this$0 = deleteAlarm;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((yv4.d) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        String simpleName = DeleteAlarm.class.getSimpleName();
        wg6.a((Object) simpleName, "DeleteAlarm::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public DeleteAlarm(AlarmsRepository alarmsRepository) {
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.g = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return h;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final d e() {
        d dVar = this.e;
        if (dVar != null) {
            return dVar;
        }
        wg6.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(yv4.d dVar, xe6<Object> xe6) {
        f fVar;
        int i2;
        Alarm alarm;
        if (xe6 instanceof f) {
            fVar = (f) xe6;
            int i3 = fVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                fVar.label = i3 - Integer.MIN_VALUE;
                Object obj = fVar.result;
                Object a2 = ff6.a();
                i2 = fVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(h, "executeUseCase");
                    if (dVar != null) {
                        this.e = dVar;
                        this.d = true;
                        Alarm a3 = dVar.a();
                        if (a3.isActive()) {
                            ArrayList arrayList = new ArrayList();
                            for (Alarm next : dVar.b()) {
                                Alarm alarmById = this.g.getAlarmById(next.getUri());
                                if (alarmById == null) {
                                    arrayList.add(next);
                                } else if (!wg6.a((Object) alarmById.getUri(), (Object) a3.getUri())) {
                                    alarmById.setMinute(next.getMinute());
                                    alarmById.setDays(next.getDays());
                                    alarmById.setHour(next.getHour());
                                    alarmById.setActive(next.isActive());
                                    alarmById.setCreatedAt(next.getCreatedAt());
                                    alarmById.setRepeated(next.isRepeated());
                                    arrayList.add(alarmById);
                                }
                            }
                            a((List<Alarm>) arrayList, dVar.c());
                            return cd6.a;
                        }
                        AlarmsRepository alarmsRepository = this.g;
                        fVar.L$0 = this;
                        fVar.L$1 = dVar;
                        fVar.L$2 = a3;
                        fVar.label = 1;
                        if (alarmsRepository.deleteAlarm(a3, fVar) == a2) {
                            return a2;
                        }
                        alarm = a3;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    alarm = (Alarm) fVar.L$2;
                    d dVar2 = (d) fVar.L$1;
                    DeleteAlarm deleteAlarm = (DeleteAlarm) fVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return new e(alarm);
            }
        }
        fVar = new f(this, xe6);
        Object obj2 = fVar.result;
        Object a22 = ff6.a();
        i2 = fVar.label;
        if (i2 != 0) {
        }
        return new e(alarm);
    }

    @DexIgnore
    public final void a(List<Alarm> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "setAlarms - alarms=" + list);
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((Alarm) next).isActive()) {
                arrayList.add(next);
            }
        }
        PortfolioApp.get.instance().a(str, (List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) ui4.a(arrayList));
    }
}
