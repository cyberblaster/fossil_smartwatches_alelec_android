package com.portfolio.platform.uirenew;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.c06;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.p64;
import com.fossil.pr5;
import com.fossil.qg3;
import com.fossil.qg6;
import com.fossil.qr5;
import com.fossil.vd;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import com.portfolio.platform.view.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BirthdayFragment extends BaseBottomSheetDialogFragment implements qr5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public pr5 j;
    @DexIgnore
    public ax5<p64> o;
    @DexIgnore
    public c06 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return BirthdayFragment.r;
        }

        @DexIgnore
        public final BirthdayFragment b() {
            return new BirthdayFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ BirthdayFragment a;

        @DexIgnore
        public b(BirthdayFragment birthdayFragment) {
            this.a = birthdayFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BirthdayFragment.s.a();
            local.d(a2, "Month was changed from " + i + " to " + i2);
            BirthdayFragment.a(this.a).b(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ BirthdayFragment a;

        @DexIgnore
        public c(BirthdayFragment birthdayFragment) {
            this.a = birthdayFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BirthdayFragment.s.a();
            local.d(a2, "Day was changed from " + i + " to " + i2);
            BirthdayFragment.a(this.a).a(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ BirthdayFragment a;

        @DexIgnore
        public d(BirthdayFragment birthdayFragment) {
            this.a = birthdayFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BirthdayFragment.s.a();
            local.d(a2, "Year was changed from " + i + " to " + i2);
            BirthdayFragment.a(this.a).c(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ BirthdayFragment a;

        @DexIgnore
        public e(BirthdayFragment birthdayFragment) {
            this.a = birthdayFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            BirthdayFragment.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ BirthdayFragment a;

        @DexIgnore
        public f(BirthdayFragment birthdayFragment) {
            this.a = birthdayFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.dismiss();
        }
    }

    /*
    static {
        String simpleName = BirthdayFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "BirthdayFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pr5 a(BirthdayFragment birthdayFragment) {
        pr5 pr5 = birthdayFragment.j;
        if (pr5 != null) {
            return pr5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i, int i2) {
        NumberPicker numberPicker;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "Update day range: from " + i + " to " + i2);
        p64 g1 = g1();
        if (g1 != null && (numberPicker = g1.s) != null) {
            wg6.a((Object) numberPicker, "npDay");
            numberPicker.setMinValue(i);
            numberPicker.setMaxValue(i2);
        }
    }

    @DexIgnore
    public void c(Date date) {
        wg6.b(date, "birthday");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProfileSetupFragment", "birthDay: " + date);
        c06 c06 = this.p;
        if (c06 != null) {
            c06.a().a(date);
            dismiss();
            return;
        }
        wg6.d("mUserBirthDayViewModel");
        throw null;
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final p64 g1() {
        ax5<p64> ax5 = this.o;
        if (ax5 != null) {
            return ax5.a();
        }
        return null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 2131951973);
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(r, "onCreateDialog");
        View inflate = View.inflate(getContext(), 2131558507, (ViewGroup) null);
        wg6.a((Object) inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        Context context = getContext();
        if (context != null) {
            qg3 qg3 = new qg3(context, getTheme());
            qg3.setContentView(inflate);
            qg3.setCanceledOnTouchOutside(true);
            return qg3;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        p64 a2 = kb.a(layoutInflater, 2131558507, viewGroup, false);
        wg6.a((Object) a2, "DataBindingUtil.inflate(\u2026rthday, container, false)");
        p64 p64 = a2;
        this.o = new ax5<>(this, p64);
        return p64.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onResume() {
        BirthdayFragment.super.onResume();
        pr5 pr5 = this.j;
        if (pr5 != null) {
            pr5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        BirthdayFragment.super.onStop();
        pr5 pr5 = this.j;
        if (pr5 != null) {
            pr5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v11, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v12, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        Object r2;
        Object r22;
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        wg6.b(view, "view");
        BirthdayFragment.super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c06 a2 = vd.a(activity).a(c06.class);
            wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
            this.p = a2;
            p64 g1 = g1();
            if (!(g1 == null || (numberPicker3 = g1.t) == null)) {
                DateFormatSymbols instance = DateFormatSymbols.getInstance(Locale.getDefault());
                wg6.a((Object) instance, "DateFormatSymbols.getInstance(Locale.getDefault())");
                numberPicker3.setDisplayedValues(instance.getShortMonths());
                numberPicker3.setOnValueChangedListener(new b(this));
            }
            p64 g12 = g1();
            if (!(g12 == null || (numberPicker2 = g12.s) == null)) {
                numberPicker2.setOnValueChangedListener(new c(this));
            }
            p64 g13 = g1();
            if (!(g13 == null || (numberPicker = g13.u) == null)) {
                numberPicker.setOnValueChangedListener(new d(this));
            }
            p64 g14 = g1();
            if (!(g14 == null || (r22 = g14.r) == 0)) {
                r22.setOnClickListener(new e(this));
            }
            p64 g15 = g1();
            if (g15 != null && (r2 = g15.q) != 0) {
                r2.setOnClickListener(new f(this));
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(pr5 pr5) {
        wg6.b(pr5, "presenter");
        this.j = pr5;
    }

    @DexIgnore
    public void a(lc6<Integer, Integer> lc6, lc6<Integer, Integer> lc62, lc6<Integer, Integer> lc63) {
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        NumberPicker numberPicker4;
        NumberPicker numberPicker5;
        NumberPicker numberPicker6;
        wg6.b(lc6, "dayRange");
        wg6.b(lc62, "monthRange");
        wg6.b(lc63, "yearRange");
        p64 g1 = g1();
        if (!(g1 == null || (numberPicker6 = g1.u) == null)) {
            wg6.a((Object) numberPicker6, "npYear");
            numberPicker6.setMinValue(lc63.getFirst().intValue());
            numberPicker6.setMaxValue(lc63.getSecond().intValue());
        }
        p64 g12 = g1();
        if (!(g12 == null || (numberPicker5 = g12.t) == null)) {
            wg6.a((Object) numberPicker5, "npMonth");
            numberPicker5.setMinValue(lc62.getFirst().intValue());
            numberPicker5.setMaxValue(lc62.getSecond().intValue());
        }
        p64 g13 = g1();
        if (!(g13 == null || (numberPicker4 = g13.s) == null)) {
            wg6.a((Object) numberPicker4, "npDay");
            numberPicker4.setMinValue(lc6.getFirst().intValue());
            numberPicker4.setMaxValue(lc6.getSecond().intValue());
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("DAY");
            int i2 = arguments.getInt("MONTH");
            int i3 = arguments.getInt("YEAR");
            p64 g14 = g1();
            if (!(g14 == null || (numberPicker3 = g14.s) == null)) {
                numberPicker3.setValue(i);
            }
            p64 g15 = g1();
            if (!(g15 == null || (numberPicker2 = g15.t) == null)) {
                numberPicker2.setValue(i2);
            }
            p64 g16 = g1();
            if (!(g16 == null || (numberPicker = g16.u) == null)) {
                numberPicker.setValue(i3);
            }
            pr5 pr5 = this.j;
            if (pr5 != null) {
                pr5.a(i);
                pr5 pr52 = this.j;
                if (pr52 != null) {
                    pr52.b(i2);
                    pr5 pr53 = this.j;
                    if (pr53 != null) {
                        pr53.c(i3);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }
}
