package com.portfolio.platform.uirenew.pairing;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.as5;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.ox5;
import com.fossil.qg6;
import com.fossil.vc4;
import com.fossil.wg6;
import com.fossil.zr5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingInstructionsFragment extends BasePermissionFragment implements as5 {
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<vc4> g;
    @DexIgnore
    public zr5 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final PairingInstructionsFragment a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            PairingInstructionsFragment pairingInstructionsFragment = new PairingInstructionsFragment();
            pairingInstructionsFragment.setArguments(bundle);
            return pairingInstructionsFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingInstructionsFragment a;

        @DexIgnore
        public b(PairingInstructionsFragment pairingInstructionsFragment, String str) {
            this.a = pairingInstructionsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingInstructionsFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingInstructionsFragment a;

        @DexIgnore
        public c(PairingInstructionsFragment pairingInstructionsFragment, String str) {
            this.a = pairingInstructionsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                if (this.a.i) {
                    HomeActivity.a aVar = HomeActivity.C;
                    wg6.a((Object) activity, "fragmentActivity");
                    HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
                }
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingInstructionsFragment a;

        @DexIgnore
        public d(PairingInstructionsFragment pairingInstructionsFragment, String str) {
            this.a = pairingInstructionsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore
    public static final /* synthetic */ zr5 b(PairingInstructionsFragment pairingInstructionsFragment) {
        zr5 zr5 = pairingInstructionsFragment.h;
        if (zr5 != null) {
            return zr5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g() {
        DashBar dashBar;
        ax5<vc4> ax5 = this.g;
        if (ax5 != null) {
            vc4 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.u) != null) {
                ox5.a aVar = ox5.a;
                wg6.a((Object) dashBar, "this");
                aVar.e(dashBar, this.i, 500);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean i1() {
        FragmentActivity activity;
        zr5 zr5 = this.h;
        if (zr5 == null) {
            wg6.d("mPresenter");
            throw null;
        } else if (zr5.i() || (activity = getActivity()) == null) {
            return false;
        } else {
            activity.finish();
            return false;
        }
    }

    @DexIgnore
    public void j0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingActivity.a aVar = PairingActivity.D;
            wg6.a((Object) activity, "fragmentActivity");
            zr5 zr5 = this.h;
            if (zr5 != null) {
                aVar.a(activity, zr5.i());
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        vc4 a2 = kb.a(layoutInflater, 2131558592, viewGroup, false, e1());
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        PairingInstructionsFragment.super.onPause();
        zr5 zr5 = this.h;
        if (zr5 != null) {
            zr5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        PairingInstructionsFragment.super.onResume();
        zr5 zr5 = this.h;
        if (zr5 != null) {
            zr5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886705);
        ax5<vc4> ax5 = this.g;
        if (ax5 != null) {
            vc4 a3 = ax5.a();
            if (a3 != null) {
                a3.q.setOnClickListener(new b(this, a2));
                a3.r.setOnClickListener(new c(this, a2));
                a3.t.setOnClickListener(new d(this, a2));
                Object r1 = a3.s;
                wg6.a((Object) r1, "binding.ftvWearOs");
                r1.setText(a2);
                AppCompatTextView appCompatTextView = a3.s;
                wg6.a((Object) appCompatTextView, "binding.ftvWearOs");
                appCompatTextView.setTypeface(appCompatTextView.getTypeface(), 1);
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.i = arguments.getBoolean("IS_ONBOARDING_FLOW");
                zr5 zr5 = this.h;
                if (zr5 != null) {
                    zr5.a(this.i);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void v(boolean z) {
        ImageView imageView;
        if (isActive()) {
            ax5<vc4> ax5 = this.g;
            if (ax5 != null) {
                vc4 a2 = ax5.a();
                if (a2 != null && (imageView = a2.t) != null) {
                    wg6.a((Object) imageView, "it");
                    imageView.setVisibility(!z ? 4 : 0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(zr5 zr5) {
        wg6.b(zr5, "presenter");
        this.h = zr5;
    }
}
