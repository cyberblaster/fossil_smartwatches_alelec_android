package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.d15;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.hx5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.ws5;
import com.fossil.xe6;
import com.fossil.xs5;
import com.fossil.ys5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetDianaDeviceSettingUseCase extends m24<xs5, ys5, ws5> {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ WatchAppRepository e;
    @DexIgnore
    public /* final */ ComplicationRepository f;
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ CategoryRepository h;
    @DexIgnore
    public /* final */ WatchFaceRepository i;
    @DexIgnore
    public /* final */ RingStyleRepository j;
    @DexIgnore
    public /* final */ d15 k;
    @DexIgnore
    public /* final */ mz4 l;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase m;
    @DexIgnore
    public /* final */ an4 n;
    @DexIgnore
    public /* final */ WatchLocalizationRepository o;
    @DexIgnore
    public /* final */ AlarmsRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1", f = "GetDianaDeviceSettingUseCase.kt", l = {65, 66, 67, 68, 69, 70, 71, 72, 73, 80, 96, 114, 124}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GetDianaDeviceSettingUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = getDianaDeviceSettingUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v24, types: [com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x0330, code lost:
            if (r9.element == false) goto L_0x034d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x0332, code lost:
            r1 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r6.L$0 = r4;
            r6.L$1 = r3;
            r6.L$2 = r0;
            r6.L$3 = r5;
            r6.L$4 = r9;
            r6.label = 12;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x034a, code lost:
            if (r1.upsertPresetList(r3, r6) != r7) goto L_0x034d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x034c, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x034d, code lost:
            r1 = com.fossil.zi4.a(r0.getComplications(), new com.google.gson.Gson());
            r2 = com.portfolio.platform.PortfolioApp.get.instance();
            r9 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x0366, code lost:
            if (r9 == null) goto L_0x040a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0368, code lost:
            r2.a(r1, r9);
            r2 = com.fossil.zi4.b(r0.getWatchapps(), new com.google.gson.Gson());
            r9 = com.portfolio.platform.PortfolioApp.get.instance();
            r10 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:0x0384, code lost:
            if (r10 == null) goto L_0x0406;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x0386, code lost:
            r9.a(r2, r10);
            r9 = com.fossil.NotificationAppHelper.b;
            r10 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.f(r6.this$0);
            r11 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.e(r6.this$0);
            r12 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.g(r6.this$0);
            r13 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.j(r6.this$0);
            r6.L$0 = r4;
            r6.L$1 = r3;
            r6.L$2 = r0;
            r6.L$3 = r5;
            r6.L$4 = r1;
            r6.L$5 = r2;
            r6.label = 13;
            r0 = r9.a(r10, r11, r12, r13, r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x03be, code lost:
            if (r0 != r7) goto L_0x03c1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x03c0, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x03c1, code lost:
            r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings((java.util.List) r0, java.lang.System.currentTimeMillis());
            r0 = com.portfolio.platform.PortfolioApp.get.instance();
            r2 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x03d8, code lost:
            if (r2 == null) goto L_0x0402;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x03da, code lost:
            r0.a(r1, r2);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.e(), "start set localization");
            com.portfolio.platform.PortfolioApp.get.instance().R();
            r6.this$0.a(new com.fossil.ys5());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:115:0x0401, code lost:
            return com.fossil.cd6.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:0x0402, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x0405, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x0406, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x0409, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x040a, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:121:0x040d, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x040e, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x0411, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x0412, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x0415, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x0416, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x0419, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x041a, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x041d, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x041e, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x0421, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x0422, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x0425, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:134:0x0426, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:135:0x0429, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x042a, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x042d, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00d9, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.b(r6.this$0);
            r6.L$0 = r0;
            r6.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00e8, code lost:
            if (r3.downloadCategories(r6) != r7) goto L_0x00eb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ea, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00eb, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.c(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f7, code lost:
            if (r4 == null) goto L_0x042a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00f9, code lost:
            r6.L$0 = r0;
            r6.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0102, code lost:
            if (r3.downloadAllComplication(r4, r6) != r7) goto L_0x0105;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0104, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0105, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.k(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0111, code lost:
            if (r4 == null) goto L_0x0426;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0113, code lost:
            r6.L$0 = r0;
            r6.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x011c, code lost:
            if (r3.downloadWatchApp(r4, r6) != r7) goto L_0x011f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x011e, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x011f, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x012b, code lost:
            if (r4 == null) goto L_0x0422;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x012d, code lost:
            r6.L$0 = r0;
            r6.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0136, code lost:
            if (r3.downloadPresetList(r4, r6) != r7) goto L_0x0139;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0138, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0139, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0145, code lost:
            if (r4 == null) goto L_0x041e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0147, code lost:
            r6.L$0 = r0;
            r6.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0150, code lost:
            if (r3.downloadRecommendPresetList(r4, r6) != r7) goto L_0x0153;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0152, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0153, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.l(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x015f, code lost:
            if (r4 == null) goto L_0x041a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0161, code lost:
            r6.L$0 = r0;
            r6.label = 7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x016a, code lost:
            if (r3.getWatchFacesFromServer(r4, r6) != r7) goto L_0x016d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x016c, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x016d, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.h(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x0179, code lost:
            if (r4 == null) goto L_0x0416;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x017b, code lost:
            r6.L$0 = r0;
            r6.label = 8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0185, code lost:
            if (r3.downloadRingStyleList(r4, true, r6) != r7) goto L_0x0188;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0187, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0188, code lost:
            r3 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.a(r6.this$0);
            r6.L$0 = r0;
            r6.label = 9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0198, code lost:
            if (r3.downloadAlarms(r6) != r7) goto L_0x007b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x019a, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x019b, code lost:
            r0 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x01a7, code lost:
            if (r4 == null) goto L_0x0412;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x01a9, code lost:
            r0 = r0.getPresetList(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x01b1, code lost:
            if (r0.isEmpty() == false) goto L_0x01f6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x01b3, code lost:
            r4 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x01bf, code lost:
            if (r5 == null) goto L_0x01f9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x01c1, code lost:
            r4 = r4.getRecommendPresetList(r5);
            r5 = r4.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x01cd, code lost:
            if (r5.hasNext() == false) goto L_0x01df;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x01cf, code lost:
            r0.add(com.portfolio.platform.data.model.diana.preset.DianaPreset.Companion.cloneFromDefaultPreset(r5.next()));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x01df, code lost:
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r6.L$0 = r3;
            r6.L$1 = r0;
            r6.L$2 = r4;
            r6.label = 10;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x01f3, code lost:
            if (r5.upsertPresetList(r0, r6) != r7) goto L_0x01f6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x01f5, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x01f6, code lost:
            r4 = r3;
            r3 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x01f9, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x01fc, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x01fd, code lost:
            r0 = r3.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x0205, code lost:
            if (r0.hasNext() == false) goto L_0x021d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x0207, code lost:
            r5 = r0.next();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x021a, code lost:
            if (com.fossil.hf6.a(((com.portfolio.platform.data.model.diana.preset.DianaPreset) r5).isActive()).booleanValue() == false) goto L_0x0201;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x021d, code lost:
            r5 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x021e, code lost:
            r5 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x0220, code lost:
            if (r5 != null) goto L_0x0248;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x0226, code lost:
            if (r3.isEmpty() == false) goto L_0x0248;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x0228, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.e(), "activePreset is null, preset list is empty?????");
            r6.this$0.a(new com.fossil.ws5(600, ""));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x0247, code lost:
            return com.fossil.cd6.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x0248, code lost:
            if (r5 != null) goto L_0x0288;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x024a, code lost:
            r0 = r3.get(0);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.e(), "Active preset is null ,pick " + r0);
            r0.setActive(true);
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.d(r6.this$0);
            r6.L$0 = r4;
            r6.L$1 = r3;
            r6.L$2 = r0;
            r6.label = 11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x0285, code lost:
            if (r5.upsertPreset(r0, r6) != r7) goto L_0x0289;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x0287, code lost:
            return r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x0288, code lost:
            r0 = r5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0289, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.e(), "activePreset=" + r0);
            r5 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.l(r6.this$0);
            r9 = com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.i(r6.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x02b3, code lost:
            if (r9 == null) goto L_0x040e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x02b5, code lost:
            r5 = r5.getWatchFacesWithType(r9, com.fossil.ai4.BACKGROUND);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x02c0, code lost:
            if ((!r5.isEmpty()) == false) goto L_0x034d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x02c2, code lost:
            r9 = new com.fossil.fh6();
            r9.element = false;
            r10 = r3.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x02d1, code lost:
            if (r10.hasNext() == false) goto L_0x032e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x02d3, code lost:
            r11 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10.next();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x02e7, code lost:
            if (com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.l(r6.this$0).getWatchFaceWithId(r11.getWatchFaceId()) != null) goto L_0x02cd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x02e9, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.e(), "Watchface " + r11.getWatchFaceId() + " is not exist, choose first watch face " + r5.get(0) + " for user");
            r11.setWatchFaceId(r5.get(0).getId());
            r9.element = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x007b, code lost:
            r3 = r0;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            ArrayList<DianaPreset> arrayList;
            DianaPreset dianaPreset;
            il6 il62;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il62 = this.p$;
                    WatchLocalizationRepository m = this.this$0.o;
                    this.L$0 = il62;
                    this.label = 1;
                    if (m.getWatchLocalizationFromServer(false, this) == a) {
                        return a;
                    }
                    break;
                case 1:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 4:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 5:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 6:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 7:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 8:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 9:
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 10:
                    List list = (List) this.L$2;
                    ArrayList<DianaPreset> arrayList2 = (ArrayList) this.L$1;
                    il6 il63 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 11:
                    dianaPreset = (DianaPreset) this.L$2;
                    arrayList = (ArrayList) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 12:
                    fh6 fh6 = (fh6) this.L$4;
                    nc6.a(obj);
                    List<WatchFace> list2 = (List) this.L$3;
                    dianaPreset = (DianaPreset) this.L$2;
                    il6 = (il6) this.L$0;
                    arrayList = (ArrayList) this.L$1;
                    break;
                case 13:
                    WatchAppMappingSettings watchAppMappingSettings = (WatchAppMappingSettings) this.L$5;
                    ComplicationAppMappingSettings complicationAppMappingSettings = (ComplicationAppMappingSettings) this.L$4;
                    List list3 = (List) this.L$3;
                    DianaPreset dianaPreset2 = (DianaPreset) this.L$2;
                    ArrayList arrayList3 = (ArrayList) this.L$1;
                    il6 il64 = (il6) this.L$0;
                    nc6.a(obj);
                    Object obj2 = obj;
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = GetDianaDeviceSettingUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "GetDianaDeviceSettingUse\u2026se::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public GetDianaDeviceSettingUseCase(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, CategoryRepository categoryRepository, WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository, d15 d15, mz4 mz4, NotificationSettingsDatabase notificationSettingsDatabase, an4 an4, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository) {
        wg6.b(watchAppRepository, "mWatchAppRepository");
        wg6.b(complicationRepository, "mComplicationRepository");
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(categoryRepository, "mCategoryRepository");
        wg6.b(watchFaceRepository, "mWatchFaceRepository");
        wg6.b(ringStyleRepository, "mRingStyleRepository");
        wg6.b(d15, "mGetApp");
        wg6.b(mz4, "mGetAllContactGroups");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wg6.b(an4, "mSharePref");
        wg6.b(watchLocalizationRepository, "mWatchLocalizationRepository");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.e = watchAppRepository;
        this.f = complicationRepository;
        this.g = dianaPresetRepository;
        this.h = categoryRepository;
        this.i = watchFaceRepository;
        this.j = ringStyleRepository;
        this.k = d15;
        this.l = mz4;
        this.m = notificationSettingsDatabase;
        this.n = an4;
        this.o = watchLocalizationRepository;
        this.p = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return q;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final rm6 d() {
        return ik6.b(b(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public Object a(xs5 xs5, xe6<? super cd6> xe6) {
        if (xs5 == null) {
            Object a2 = a(new ws5(600, ""));
            if (a2 == ff6.a()) {
                return a2;
            }
            return cd6.a;
        }
        this.d = xs5.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "download device setting of " + this.d);
        if (!hx5.b(PortfolioApp.get.instance())) {
            Object a3 = a(new ws5(601, ""));
            if (a3 == ff6.a()) {
                return a3;
            }
            return cd6.a;
        }
        if (!TextUtils.isEmpty(this.d)) {
            DeviceHelper.a aVar = DeviceHelper.o;
            String str2 = this.d;
            if (str2 == null) {
                wg6.a();
                throw null;
            } else if (aVar.e(str2)) {
                Object d2 = d();
                if (d2 == ff6.a()) {
                    return d2;
                }
                return cd6.a;
            }
        }
        Object a4 = a(new ws5(600, ""));
        if (a4 == ff6.a()) {
            return a4;
        }
        return cd6.a;
    }
}
