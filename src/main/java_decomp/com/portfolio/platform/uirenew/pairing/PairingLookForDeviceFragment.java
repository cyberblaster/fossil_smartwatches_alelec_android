package com.portfolio.platform.uirenew.pairing;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.ks5;
import com.fossil.ls5;
import com.fossil.ox5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xc4;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.scanning.BasePairingSubFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingLookForDeviceFragment extends BasePairingSubFragment implements ls5 {
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public ax5<xc4> g;
    @DexIgnore
    public ks5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final PairingLookForDeviceFragment a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            PairingLookForDeviceFragment pairingLookForDeviceFragment = new PairingLookForDeviceFragment();
            pairingLookForDeviceFragment.setArguments(bundle);
            return pairingLookForDeviceFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity a;

        @DexIgnore
        public b(FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.a, 2130772011);
            Object r0 = (FlexibleTextView) this.a.findViewById(2131362442);
            if (r0 != 0) {
                r0.startAnimation(loadAnimation);
            }
            Object r02 = (FlexibleTextView) this.a.findViewById(2131362323);
            if (r02 != 0) {
                r02.startAnimation(loadAnimation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingLookForDeviceFragment a;

        @DexIgnore
        public c(PairingLookForDeviceFragment pairingLookForDeviceFragment) {
            this.a = pairingLookForDeviceFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingLookForDeviceFragment.a(this.a).a(1);
        }
    }

    @DexIgnore
    public static final /* synthetic */ ks5 a(PairingLookForDeviceFragment pairingLookForDeviceFragment) {
        ks5 ks5 = pairingLookForDeviceFragment.h;
        if (ks5 != null) {
            return ks5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        FragmentActivity activity;
        if (z || (activity = getActivity()) == null) {
            return null;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(activity, 2130772005);
        if (loadAnimation == null) {
            return loadAnimation;
        }
        loadAnimation.setAnimationListener(new b(activity));
        return loadAnimation;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        xc4 a2 = kb.a(layoutInflater, 2131558593, viewGroup, false, e1());
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v6, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        Object r4;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<xc4> ax5 = this.g;
        if (ax5 != null) {
            xc4 a2 = ax5.a();
            if (!(a2 == null || (r4 = a2.q) == 0)) {
                r4.setOnClickListener(new c(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                ax5<xc4> ax52 = this.g;
                if (ax52 != null) {
                    xc4 a3 = ax52.a();
                    if (a3 != null && (dashBar = a3.r) != null) {
                        ox5.a aVar = ox5.a;
                        wg6.a((Object) dashBar, "this");
                        aVar.d(dashBar, z, 500);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ks5 ks5) {
        wg6.b(ks5, "presenter");
        this.h = ks5;
    }
}
