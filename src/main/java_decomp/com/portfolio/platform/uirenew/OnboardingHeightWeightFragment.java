package com.portfolio.platform.uirenew;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.nc4;
import com.fossil.ox5;
import com.fossil.pq5;
import com.fossil.qg6;
import com.fossil.qq5;
import com.fossil.uy5;
import com.fossil.wg6;
import com.fossil.yt4;
import com.fossil.yz5;
import com.fossil.zh4;
import com.fossil.zj4;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OnboardingHeightWeightFragment extends BaseFragment implements qq5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public pq5 f;
    @DexIgnore
    public ax5<nc4> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return OnboardingHeightWeightFragment.i;
        }

        @DexIgnore
        public final OnboardingHeightWeightFragment b() {
            return new OnboardingHeightWeightFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment a;

        @DexIgnore
        public c(OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public void a(TabLayout.g gVar) {
            wg6.b(gVar, "tab");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void b(TabLayout.g gVar) {
            wg6.b(gVar, "tab");
            CharSequence e = gVar.e();
            zh4 zh4 = zh4.METRIC;
            if (wg6.a((Object) e, (Object) PortfolioApp.get.instance().getString(2131886711))) {
                zh4 = zh4.IMPERIAL;
            }
            OnboardingHeightWeightFragment.a(this.a).a(zh4);
        }

        @DexIgnore
        public void c(TabLayout.g gVar) {
            wg6.b(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment a;

        @DexIgnore
        public e(OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public void a(TabLayout.g gVar) {
            wg6.b(gVar, "tab");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void b(TabLayout.g gVar) {
            wg6.b(gVar, "tab");
            CharSequence e = gVar.e();
            zh4 zh4 = zh4.METRIC;
            if (wg6.a((Object) e, (Object) PortfolioApp.get.instance().getString(2131886713))) {
                zh4 = zh4.IMPERIAL;
            }
            OnboardingHeightWeightFragment.a(this.a).b(zh4);
        }

        @DexIgnore
        public void c(TabLayout.g gVar) {
            wg6.b(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment a;

        @DexIgnore
        public f(OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            OnboardingHeightWeightFragment.a(this.a).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment a;

        @DexIgnore
        public g(OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            OnboardingHeightWeightFragment.a(this.a).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment a;

        @DexIgnore
        public h(OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            OnboardingHeightWeightFragment.a(this.a).a(false);
        }
    }

    /*
    static {
        String simpleName = OnboardingHeightWeightFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "OnboardingHeightWeightFr\u2026::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pq5 a(OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
        pq5 pq5 = onboardingHeightWeightFragment.f;
        if (pq5 != null) {
            return pq5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i2, zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        int i3 = yt4.a[zh4.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "updateData weight=" + i2 + " metric");
            ax5<nc4> ax5 = this.g;
            if (ax5 != null) {
                nc4 a2 = ax5.a();
                if (a2 != null) {
                    TabLayout.g b2 = a2.x.b(0);
                    if (b2 != null) {
                        b2.h();
                    }
                    a2.v.setUnit(zh4.METRIC);
                    a2.v.setFormatter(new ProfileFormatter(4));
                    a2.v.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            ax5<nc4> ax52 = this.g;
            if (ax52 != null) {
                nc4 a3 = ax52.a();
                if (a3 != null) {
                    TabLayout.g b3 = a3.x.b(1);
                    if (b3 != null) {
                        b3.h();
                    }
                    float f2 = zj4.f((float) i2);
                    a3.v.setUnit(zh4.IMPERIAL);
                    a3.v.setFormatter(new ProfileFormatter(4));
                    a3.v.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g() {
        DashBar dashBar;
        ax5<nc4> ax5 = this.g;
        if (ax5 != null) {
            nc4 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.s) != null) {
                ox5.a aVar = ox5.a;
                wg6.a((Object) dashBar, "this");
                aVar.a(dashBar, 500);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return i;
    }

    @DexIgnore
    public void i() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void k() {
        if (isActive()) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886718);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            X(a2);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        OnboardingHeightWeightFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.g = new ax5<>(this, kb.a(layoutInflater, 2131558587, viewGroup, false, e1()));
        ax5<nc4> ax5 = this.g;
        if (ax5 != null) {
            nc4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        OnboardingHeightWeightFragment.super.onPause();
        pq5 pq5 = this.f;
        if (pq5 != null) {
            pq5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        OnboardingHeightWeightFragment.super.onResume();
        pq5 pq5 = this.f;
        if (pq5 != null) {
            pq5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<nc4> ax5 = this.g;
        if (ax5 != null) {
            nc4 a2 = ax5.a();
            if (a2 != null) {
                a2.u.setValuePickerListener(new b(a2, this));
                a2.w.a(new c(this));
                a2.v.setValuePickerListener(new d(a2, this));
                a2.x.a(new e(this));
                a2.r.setOnClickListener(new f(this));
                a2.y.setOnClickListener(new g(this));
                a2.q.setOnClickListener(new h(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void q0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
            wg6.a((Object) activity, "it");
            PairingInstructionsActivity.a.a(aVar, activity, true, false, 4, (Object) null);
            activity.finish();
        }
    }

    @DexIgnore
    public void a(pq5 pq5) {
        wg6.b(pq5, "presenter");
        this.f = pq5;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements yz5 {
        @DexIgnore
        public /* final */ /* synthetic */ nc4 a;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment b;

        @DexIgnore
        public b(nc4 nc4, OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = nc4;
            this.b = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public void a(int i) {
            if (this.a.u.getUnit() == zh4.METRIC) {
                OnboardingHeightWeightFragment.a(this.b).a(i);
                return;
            }
            OnboardingHeightWeightFragment.a(this.b).a(Math.round(zj4.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        public void b(int i) {
        }

        @DexIgnore
        public void a(boolean z) {
            TabLayout tabLayout = this.a.w;
            wg6.a((Object) tabLayout, "it.tlHeightUnit");
            uy5.a(tabLayout, !z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements yz5 {
        @DexIgnore
        public /* final */ /* synthetic */ nc4 a;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightFragment b;

        @DexIgnore
        public d(nc4 nc4, OnboardingHeightWeightFragment onboardingHeightWeightFragment) {
            this.a = nc4;
            this.b = onboardingHeightWeightFragment;
        }

        @DexIgnore
        public void a(int i) {
            if (this.a.v.getUnit() == zh4.METRIC) {
                OnboardingHeightWeightFragment.a(this.b).b(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            OnboardingHeightWeightFragment.a(this.b).b(Math.round(zj4.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        public void b(int i) {
        }

        @DexIgnore
        public void a(boolean z) {
            TabLayout tabLayout = this.a.x;
            wg6.a((Object) tabLayout, "it.tlWeightUnit");
            uy5.a(tabLayout, !z);
        }
    }

    @DexIgnore
    public void a(int i2, zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        int i3 = yt4.b[zh4.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "updateData height=" + i2 + " metric");
            ax5<nc4> ax5 = this.g;
            if (ax5 != null) {
                nc4 a2 = ax5.a();
                if (a2 != null) {
                    TabLayout.g b2 = a2.w.b(0);
                    if (b2 != null) {
                        b2.h();
                    }
                    a2.u.setUnit(zh4.METRIC);
                    a2.u.setFormatter(new ProfileFormatter(-1));
                    a2.u.a(100, 251, i2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            ax5<nc4> ax52 = this.g;
            if (ax52 != null) {
                nc4 a3 = ax52.a();
                if (a3 != null) {
                    TabLayout.g b3 = a3.w.b(1);
                    if (b3 != null) {
                        b3.h();
                    }
                    lc6<Integer, Integer> b4 = zj4.b((float) i2);
                    a3.u.setUnit(zh4.IMPERIAL);
                    a3.u.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.u;
                    Integer first = b4.getFirst();
                    wg6.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = zj4.a(first.intValue());
                    Integer second = b4.getSecond();
                    wg6.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }
}
