package com.portfolio.platform.uirenew;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.hc;
import com.fossil.jb;
import com.fossil.jl4;
import com.fossil.kl4;
import com.fossil.o34;
import com.fossil.pw6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseFragment extends Fragment implements pw6.a, View.OnKeyListener, jl4.b {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public AnalyticsHelper a;
    @DexIgnore
    public /* final */ jb b; // = new o34(this);
    @DexIgnore
    public kl4 c;
    @DexIgnore
    public HashMap d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = BaseFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "BaseFragment::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public final void W(String str) {
        wg6.b(str, "view");
        kl4 b2 = AnalyticsHelper.f.b();
        b2.b(str);
        this.c = b2;
        kl4 kl4 = this.c;
        if (kl4 != null) {
            kl4.a((jl4.b) this);
        }
    }

    @DexIgnore
    public final void X(String str) {
        wg6.b(str, Explore.COLUMN_TITLE);
        FLogger.INSTANCE.getLocal().d(e, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.a(str);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void Y(String str) {
        wg6.b(str, "content");
        LayoutInflater layoutInflater = getLayoutInflater();
        wg6.a((Object) layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558796, activity != null ? (ViewGroup) activity.findViewById(2131362139) : null);
        View findViewById = inflate.findViewById(2131362312);
        wg6.a((Object) findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, MFNetworkReturnCode.RESPONSE_OK);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public void Z0() {
        FLogger.INSTANCE.getLocal().d(e, "Tracer started");
        PortfolioApp.get.instance().a(this.c);
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(e, "hideProgressDialog");
        if (getActivity() != null) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.h();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(e, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.p();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public void b1() {
        FLogger.INSTANCE.getLocal().d(e, "Tracer ended");
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.d;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final jb e1() {
        return this.b;
    }

    @DexIgnore
    public final AnalyticsHelper f1() {
        AnalyticsHelper analyticsHelper = this.a;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wg6.d("mAnalyticHelper");
        throw null;
    }

    @DexIgnore
    public final kl4 g1() {
        return this.c;
    }

    @DexIgnore
    public String h1() {
        return e;
    }

    @DexIgnore
    public boolean i1() {
        if (!isActive() || getParentFragment() == null || !(getParentFragment() instanceof BaseFragment)) {
            return false;
        }
        BaseFragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            return parentFragment.i1();
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        BaseFragment.super.onCreate(bundle);
        this.a = AnalyticsHelper.f.c();
    }

    @DexIgnore
    public void onDestroyView() {
        kl4 kl4 = this.c;
        if (kl4 != null) {
            kl4.c();
        }
        BaseFragment.super.onDestroyView();
        d1();
    }

    @DexIgnore
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        wg6.b(view, "view");
        wg6.b(keyEvent, "keyEvent");
        if (keyEvent.getAction() != 1 || i != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(e, "onKey KEYCODE_BACK");
        return i1();
    }

    @DexIgnore
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        wg6.b(strArr, "permissions");
        wg6.b(iArr, "grantResults");
        BaseFragment.super.onRequestPermissionsResult(i, strArr, iArr);
        pw6.a(i, strArr, iArr, new Object[]{this});
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        BaseFragment.super.onViewCreated(view, bundle);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131362851);
        if (viewGroup != null) {
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                viewGroup.setBackgroundColor(Color.parseColor(b2));
            }
        }
        view.setOnKeyListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i) {
        wg6.b(fragment, "fragment");
        wg6.b(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        hc b2 = childFragmentManager.b();
        wg6.a((Object) b2, "fragmentManager.beginTransaction()");
        b2.a((String) null);
        b2.a(i, fragment, str);
        b2.b();
    }

    @DexIgnore
    public final void b(Fragment fragment, String str, int i) {
        wg6.b(fragment, "fragment");
        wg6.b(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        hc b2 = childFragmentManager.b();
        wg6.a((Object) b2, "fragmentManager.beginTransaction()");
        Fragment b3 = childFragmentManager.b(i);
        if (b3 != null) {
            b2.d(b3);
        }
        b2.a((String) null);
        b2.b(i, fragment, str);
        b2.b();
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        wg6.b(list, "perms");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "onPermissionsDenied: perm = " + str);
        }
    }

    @DexIgnore
    public final void a(Intent intent, String str) {
        wg6.b(intent, "browserIntent");
        wg6.b(str, "tag");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            FLogger.INSTANCE.getLocal().e(str, "Exception when start url with no browser app");
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        wg6.b(list, "perms");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "onPermissionsGranted: perm = " + str);
        }
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        wg6.b(str, Constants.EVENT);
        wg6.b(map, "values");
        AnalyticsHelper analyticsHelper = this.a;
        if (analyticsHelper != null) {
            analyticsHelper.a(str, (Map<String, ? extends Object>) map);
        } else {
            wg6.d("mAnalyticHelper");
            throw null;
        }
    }
}
