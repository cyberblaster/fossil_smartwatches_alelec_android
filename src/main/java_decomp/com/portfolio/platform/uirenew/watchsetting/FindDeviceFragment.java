package com.portfolio.platform.uirenew.watchsetting;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.az2;
import com.fossil.bv5;
import com.fossil.fr;
import com.fossil.gy5;
import com.fossil.ix2;
import com.fossil.jm4;
import com.fossil.jx2;
import com.fossil.kb;
import com.fossil.lk4;
import com.fossil.lx2;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.px2;
import com.fossil.qg6;
import com.fossil.qv5;
import com.fossil.r94;
import com.fossil.rc6;
import com.fossil.rv5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wq;
import com.fossil.yy2;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDeviceFragment extends BasePermissionFragment implements rv5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a u; // = new a((qg6) null);
    @DexIgnore
    public qv5 g;
    @DexIgnore
    public ax5<r94> h;
    @DexIgnore
    public jx2 i;
    @DexIgnore
    public View j;
    @DexIgnore
    public Bitmap o;
    @DexIgnore
    public fr p;
    @DexIgnore
    public /* final */ String q; // = ThemeManager.l.a().b("primaryText");
    @DexIgnore
    public /* final */ String r; // = ThemeManager.l.a().b("success");
    @DexIgnore
    public /* final */ String s; // = ThemeManager.l.a().b("secondaryText");
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final FindDeviceFragment a() {
            return new FindDeviceFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment a;

        @DexIgnore
        public b(FindDeviceFragment findDeviceFragment) {
            this.a = findDeviceFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements lx2 {
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment a;

        @DexIgnore
        public c(FindDeviceFragment findDeviceFragment) {
            this.a = findDeviceFragment;
        }

        @DexIgnore
        public final void a(jx2 jx2) {
            this.a.i = jx2;
            wg6.a((Object) jx2, "googleMap");
            px2 c = jx2.c();
            wg6.a((Object) c, "googleMap.uiSettings");
            c.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ r94 b;

        @DexIgnore
        public d(FindDeviceFragment findDeviceFragment, r94 r94) {
            this.a = findDeviceFragment;
            this.b = r94;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleSwitchCompat, android.view.View] */
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            gy5.a((View) this.b.z);
            ConstraintLayout constraintLayout = this.b.r;
            wg6.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(z ? 0 : 4);
            FindDeviceFragment.a(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r94 a;
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment b;

        @DexIgnore
        public e(r94 r94, FindDeviceFragment findDeviceFragment, bv5.c cVar) {
            this.a = r94;
            this.b = findDeviceFragment;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            FindDeviceFragment.b(this.b).a(str2).a(this.a.x);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment b;

        @DexIgnore
        public f(View view, String str, FindDeviceFragment findDeviceFragment, bv5.c cVar) {
            this.a = view;
            this.b = findDeviceFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v9, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void onImageCallback(String str, String str2) {
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            if (this.b.isActive()) {
                View findViewById = this.a.findViewById(2131362507);
                if (findViewById != null) {
                    ((ImageView) findViewById).setImageBitmap(lk4.a(str2));
                    int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165440);
                    this.a.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
                    View view = this.a;
                    view.layout(0, 0, view.getMeasuredWidth(), this.a.getMeasuredHeight());
                    this.b.o = lk4.a(this.a);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.ImageView");
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ qv5 a(FindDeviceFragment findDeviceFragment) {
        qv5 qv5 = findDeviceFragment.g;
        if (qv5 != null) {
            return qv5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ fr b(FindDeviceFragment findDeviceFragment) {
        fr frVar = findDeviceFragment.p;
        if (frVar != null) {
            return frVar;
        }
        wg6.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void B(String str) {
        Object r0;
        wg6.b(str, "address");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showAddress: address = " + str);
        ax5<r94> ax5 = this.h;
        if (ax5 != null) {
            r94 a2 = ax5.a();
            if (a2 != null && (r0 = a2.t) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void X0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationNotFound");
        r94 j1 = j1();
        if (j1 != null) {
            Object r1 = j1.z;
            wg6.a((Object) r1, "binding.swLocate");
            r1.setEnabled(false);
            FlexibleSwitchCompat flexibleSwitchCompat = j1.z;
            wg6.a((Object) flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            String str = this.s;
            if (str != null) {
                j1.v.setTextColor(Color.parseColor(str));
            }
            ConstraintLayout constraintLayout = j1.r;
            wg6.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            Object r0 = j1.s;
            wg6.a((Object) r0, "binding.ftvError");
            r0.setVisibility(0);
            qv5 qv5 = this.g;
            if (qv5 != null) {
                qv5.b(false);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void c(boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocationEnable: enable = " + z + ", needWarning = " + z2);
        ax5<r94> ax5 = this.h;
        if (ax5 != null) {
            r94 a2 = ax5.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.z;
                wg6.a((Object) flexibleSwitchCompat, "binding.swLocate");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void i0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationPermissionError");
        r94 j1 = j1();
        if (j1 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = j1.z;
            wg6.a((Object) flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = j1.r;
            wg6.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            qv5 qv5 = this.g;
            if (qv5 != null) {
                qv5.b(false);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final r94 j1() {
        ax5<r94> ax5 = this.h;
        if (ax5 != null) {
            return ax5.a();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void l(int i2) {
        ax5<r94> ax5 = this.h;
        if (ax5 != null) {
            r94 a2 = ax5.a();
            if (a2 != null) {
                float b2 = DeviceHelper.o.b(i2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDeviceFragment", "showProximity - rssi: " + i2 + ", distanceInFt: " + b2);
                if (b2 >= 0.0f && b2 <= 5.0f) {
                    Object r8 = a2.B;
                    wg6.a((Object) r8, "binding.tvLocationStatus");
                    r8.setText(jm4.a(getContext(), 2131886909));
                    Object r82 = a2.B;
                    wg6.a((Object) r82, "binding.tvLocationStatus");
                    r82.setEnabled(true);
                    String str = this.r;
                    if (str != null) {
                        a2.B.setTextColor(Color.parseColor(str));
                        a2.B.setBackgroundColor(Color.parseColor(str));
                        Object r83 = a2.B;
                        wg6.a((Object) r83, "binding.tvLocationStatus");
                        Drawable background = r83.getBackground();
                        wg6.a((Object) background, "binding.tvLocationStatus.background");
                        background.setAlpha(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                    }
                } else if (b2 >= 5.0f && b2 <= 10.0f) {
                    Object r84 = a2.B;
                    wg6.a((Object) r84, "binding.tvLocationStatus");
                    r84.setText(jm4.a(getContext(), 2131886907));
                    Object r85 = a2.B;
                    wg6.a((Object) r85, "binding.tvLocationStatus");
                    r85.setEnabled(true);
                    String str2 = this.r;
                    if (str2 != null) {
                        a2.B.setTextColor(Color.parseColor(str2));
                        a2.B.setBackgroundColor(Color.parseColor(str2));
                        Object r86 = a2.B;
                        wg6.a((Object) r86, "binding.tvLocationStatus");
                        Drawable background2 = r86.getBackground();
                        wg6.a((Object) background2, "binding.tvLocationStatus.background");
                        background2.setAlpha(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                    }
                } else if (b2 < 10.0f || b2 > 30.0f) {
                    Object r87 = a2.B;
                    wg6.a((Object) r87, "binding.tvLocationStatus");
                    r87.setText(jm4.a(getContext(), 2131886906));
                    Object r88 = a2.B;
                    wg6.a((Object) r88, "binding.tvLocationStatus");
                    r88.setEnabled(false);
                    String str3 = this.s;
                    if (str3 != null) {
                        a2.B.setTextColor(Color.parseColor(str3));
                        a2.B.setBackgroundColor(Color.parseColor(str3));
                        Object r89 = a2.B;
                        wg6.a((Object) r89, "binding.tvLocationStatus");
                        Drawable background3 = r89.getBackground();
                        wg6.a((Object) background3, "binding.tvLocationStatus.background");
                        background3.setAlpha(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                    }
                } else {
                    Object r810 = a2.B;
                    wg6.a((Object) r810, "binding.tvLocationStatus");
                    r810.setText(jm4.a(getContext(), 2131886908));
                    Object r811 = a2.B;
                    wg6.a((Object) r811, "binding.tvLocationStatus");
                    r811.setEnabled(true);
                    String str4 = this.r;
                    if (str4 != null) {
                        a2.B.setTextColor(Color.parseColor(str4));
                        a2.B.setBackgroundColor(Color.parseColor(str4));
                        Object r812 = a2.B;
                        wg6.a((Object) r812, "binding.tvLocationStatus");
                        Drawable background4 = r812.getBackground();
                        wg6.a((Object) background4, "binding.tvLocationStatus.background");
                        background4.setAlpha(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                    }
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        fr a2 = wq.a(this);
        wg6.a((Object) a2, "Glide.with(this)");
        this.p = a2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v8, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r6v13, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String b2;
        wg6.b(layoutInflater, "inflater");
        r94 a2 = kb.a(layoutInflater, 2131558549, viewGroup, false, e1());
        this.j = layoutInflater.inflate(2131558699, (ViewGroup) null);
        View view = this.j;
        if (view != null) {
            int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165440);
            view.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            this.o = lk4.a(view);
        }
        a2.w.setOnClickListener(new b(this));
        SupportMapFragment b3 = getChildFragmentManager().b(2131362202);
        if (b3 != null) {
            b3.a(new c(this));
            a2.z.setOnCheckedChangeListener(new d(this, a2));
            ConstraintLayout constraintLayout = a2.q;
            if (!(constraintLayout == null || (b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND)) == null)) {
                constraintLayout.setBackgroundColor(Color.parseColor(b2));
            }
            this.h = new ax5<>(this, a2);
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        throw new rc6("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onPause");
        FindDeviceFragment.super.onPause();
        qv5 qv5 = this.g;
        if (qv5 != null) {
            qv5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onResume");
        FindDeviceFragment.super.onResume();
        qv5 qv5 = this.g;
        if (qv5 != null) {
            qv5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(bv5.c cVar) {
        wg6.b(cVar, "watchSetting");
        if (isActive()) {
            ax5<r94> ax5 = this.h;
            if (ax5 != null) {
                r94 a2 = ax5.a();
                if (a2 != null) {
                    String deviceId = cVar.a().getDeviceId();
                    Object r3 = a2.A;
                    wg6.a((Object) r3, "it.tvDeviceName");
                    r3.setText(cVar.b());
                    a2.A.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    Object r32 = a2.s;
                    wg6.a((Object) r32, "it.ftvError");
                    nh6 nh6 = nh6.a;
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886912);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026chNameHasntBeenConnected)");
                    Object[] objArr = {cVar.b()};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    r32.setText(format);
                    boolean d2 = cVar.d();
                    if (d2) {
                        if (cVar.e()) {
                            String str = this.r;
                            if (str != null) {
                                a2.A.setTextColor(Color.parseColor(str));
                            }
                            Object r33 = a2.A;
                            wg6.a((Object) r33, "it.tvDeviceName");
                            r33.setAlpha(1.0f);
                            a2.A.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, w6.c(PortfolioApp.get.instance(), 2131231087), (Drawable) null);
                        } else {
                            String str2 = this.q;
                            if (str2 != null) {
                                a2.A.setTextColor(Color.parseColor(str2));
                            }
                            Object r1 = a2.A;
                            wg6.a((Object) r1, "it.tvDeviceName");
                            r1.setAlpha(0.4f);
                            Object r12 = a2.B;
                            wg6.a((Object) r12, "it.tvLocationStatus");
                            r12.setText(jm4.a(getContext(), 2131886906));
                            Object r13 = a2.B;
                            wg6.a((Object) r13, "it.tvLocationStatus");
                            r13.setEnabled(false);
                            String str3 = this.s;
                            if (str3 != null) {
                                a2.B.setTextColor(Color.parseColor(str3));
                                a2.B.setBackgroundColor(Color.parseColor(str3));
                                Object r14 = a2.B;
                                wg6.a((Object) r14, "it.tvLocationStatus");
                                Drawable background = r14.getBackground();
                                wg6.a((Object) background, "it.tvLocationStatus.background");
                                background.setAlpha(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                            }
                        }
                    } else if (!d2) {
                        String str4 = this.q;
                        if (str4 != null) {
                            a2.A.setTextColor(Color.parseColor(str4));
                        }
                        Object r15 = a2.A;
                        wg6.a((Object) r15, "it.tvDeviceName");
                        r15.setAlpha(0.4f);
                        Object r16 = a2.B;
                        wg6.a((Object) r16, "it.tvLocationStatus");
                        r16.setText(jm4.a(getContext(), 2131886906));
                        Object r17 = a2.B;
                        wg6.a((Object) r17, "it.tvLocationStatus");
                        r17.setEnabled(false);
                        String str5 = this.s;
                        if (str5 != null) {
                            a2.B.setTextColor(Color.parseColor(str5));
                            a2.B.setBackgroundColor(Color.parseColor(str5));
                            Object r18 = a2.B;
                            wg6.a((Object) r18, "it.tvLocationStatus");
                            Drawable background2 = r18.getBackground();
                            wg6.a((Object) background2, "it.tvLocationStatus.background");
                            background2.setAlpha(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                        }
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.x;
                    wg6.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.b(deviceId, DeviceHelper.ImageStyle.SMALL)).setImageCallback(new e(a2, this, cVar)).download();
                    View view = this.j;
                    if (view != null) {
                        new CloudImageHelper().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE).setImageCallback(new f(view, deviceId, this, cVar)).download();
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(qv5 qv5) {
        wg6.b(qv5, "presenter");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "setPresenter: presenter = " + qv5);
        this.g = qv5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        r94 j1 = j1();
        if (j1 != null) {
            Object r0 = j1.s;
            wg6.a((Object) r0, "binding.ftvError");
            r0.setVisibility(8);
            if (d2 != null && d3 != null) {
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                jx2 jx2 = this.i;
                if (jx2 != null) {
                    jx2.b(ix2.a(latLng, 13.0f));
                    jx2.a(ix2.a(18.0f));
                    jx2.a();
                    Bitmap bitmap = this.o;
                    if (bitmap != null) {
                        az2 az2 = new az2();
                        az2.e(jm4.a((Context) PortfolioApp.get.instance(), 2131886905));
                        az2.a(yy2.a(bitmap));
                        az2.a(latLng);
                        jx2.a(az2);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(long j2) {
        Object r0;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showTime: time = " + j2);
        ax5<r94> ax5 = this.h;
        if (ax5 != null) {
            r94 a2 = ax5.a();
            if (a2 != null && (r0 = a2.u) != 0) {
                r0.setText(DateUtils.getRelativeTimeSpanString(j2));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showErrorDialog");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
        r94 j1 = j1();
        if (j1 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = j1.z;
            wg6.a((Object) flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = j1.r;
            wg6.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            qv5 qv5 = this.g;
            if (qv5 != null) {
                qv5.b(false);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }
}
