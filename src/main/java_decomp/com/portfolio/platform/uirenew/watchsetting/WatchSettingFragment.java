package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.WatchSettingFragmentBinding;
import com.fossil.av5;
import com.fossil.ax5;
import com.fossil.bv5;
import com.fossil.fr;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.l24;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.uh4;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wq;
import com.fossil.xe4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingFragment extends BasePermissionFragment implements AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public ax5<xe4> g;
    @DexIgnore
    public bv5 h;
    @DexIgnore
    public String i; // = ThemeManager.l.a().b("primaryText");
    @DexIgnore
    public fr j;
    @DexIgnore
    public w04 o;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WatchSettingFragment.q;
        }

        @DexIgnore
        public final WatchSettingFragment b() {
            return new WatchSettingFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<l24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public b(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(l24.a aVar) {
            if (aVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingFragment.r.a();
                local.d(a2, "loadingState start " + aVar.a() + " stop " + aVar.b());
                if (aVar.a()) {
                    this.a.k();
                }
                if (aVar.b()) {
                    this.a.i();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<l24.b> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public c(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(l24.b bVar) {
            if (!bVar.a().isEmpty()) {
                WatchSettingFragment watchSettingFragment = this.a;
                Object[] array = bVar.a().toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    watchSettingFragment.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<bv5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public d(WatchSettingFragment watchSettingFragment, String str) {
            this.a = watchSettingFragment;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bv5.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingFragment.r.a();
                local.d(a2, "onUIState changed, modelWrapper=" + bVar);
                if (bVar.k() != null) {
                    WatchSettingFragment watchSettingFragment = this.a;
                    bv5.c k = bVar.k();
                    if (k != null) {
                        watchSettingFragment.a(k);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.a()) {
                    this.a.o();
                }
                if (bVar.l() != null) {
                    WatchSettingFragment watchSettingFragment2 = this.a;
                    String l = bVar.l();
                    if (l != null) {
                        watchSettingFragment2.f0(l);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                Integer m = bVar.m();
                if (m != null) {
                    m.intValue();
                    WatchSettingFragment watchSettingFragment3 = this.a;
                    Integer m2 = bVar.m();
                    if (m2 != null) {
                        watchSettingFragment3.r(m2.intValue());
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.f() != null) {
                    WatchSettingFragment watchSettingFragment4 = this.a;
                    lc6<Integer, String> f = bVar.f();
                    if (f != null) {
                        int intValue = f.getFirst().intValue();
                        lc6<Integer, String> f2 = bVar.f();
                        if (f2 != null) {
                            watchSettingFragment4.a(intValue, f2.getSecond());
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.d()) {
                    this.a.b0(this.b);
                }
                if (bVar.c() != null) {
                    WatchSettingFragment watchSettingFragment5 = this.a;
                    String c = bVar.c();
                    if (c != null) {
                        watchSettingFragment5.a0(c);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.i() != null) {
                    WatchSettingFragment watchSettingFragment6 = this.a;
                    String i = bVar.i();
                    if (i != null) {
                        watchSettingFragment6.e0(i);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.e() != null) {
                    WatchSettingFragment watchSettingFragment7 = this.a;
                    String e = bVar.e();
                    if (e != null) {
                        watchSettingFragment7.c0(e);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.b() != null) {
                    WatchSettingFragment watchSettingFragment8 = this.a;
                    String b2 = bVar.b();
                    if (b2 != null) {
                        watchSettingFragment8.Z(b2);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.h() != null) {
                    WatchSettingFragment watchSettingFragment9 = this.a;
                    String h = bVar.h();
                    if (h != null) {
                        watchSettingFragment9.d0(h);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (bVar.g()) {
                    this.a.c();
                }
                if (bVar.j()) {
                    this.a.k1();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public e(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.b(childFragmentManager, WatchSettingFragment.a(this.a).g());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public f(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingFragment.r.a();
            local.d(a2, "getWatchSerial=" + WatchSettingFragment.a(this.a).k());
            if (this.a.isActive() && !TextUtils.isEmpty(WatchSettingFragment.a(this.a).k())) {
                FindDeviceActivity.a aVar = FindDeviceActivity.C;
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    String k = WatchSettingFragment.a(this.a).k();
                    if (k != null) {
                        aVar.a(activity, k);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public g(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.isActive() && !TextUtils.isEmpty(WatchSettingFragment.a(this.a).k())) {
                CalibrationActivity.a aVar = CalibrationActivity.C;
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public h(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(100);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public i(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(50);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public j(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(25);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public k(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public l(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragmentBinding a;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public m(WatchSettingFragmentBinding watchSettingFragmentBinding, WatchSettingFragment watchSettingFragment, bv5.c cVar) {
            this.a = watchSettingFragmentBinding;
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            this.b.j1().a(str2).a(this.a.x);
        }
    }

    /*
    static {
        String simpleName = WatchSettingFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchSettingFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ bv5 a(WatchSettingFragment watchSettingFragment) {
        bv5 bv5 = watchSettingFragment.h;
        if (bv5 != null) {
            return bv5;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void Z(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.b(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void a0(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.e(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void b0(String str) {
        wg6.b(str, "serial");
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                TroubleshootingActivity.a.a(aVar, context, str, false, 4, (Object) null);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    public final void c0(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.d(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void d0(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.c(str, childFragmentManager);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void e0(String str) {
        wg6.b(str, "serial");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.f(str, childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void f0(String str) {
        wg6.b(str, "lastSync");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.d(str2, "updateLastSync, lastSync=" + str);
        if (isActive()) {
            ax5<xe4> ax5 = this.g;
            if (ax5 != null) {
                WatchSettingFragmentBinding a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.E;
                    wg6.a((Object) r1, "it.tvLastSyncValue");
                    r1.setText(str);
                    Object r5 = a2.E;
                    wg6.a((Object) r5, "it.tvLastSyncValue");
                    r5.setSelected(true);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public String h1() {
        return q;
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(q, "stopLoading");
        a();
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final fr j1() {
        fr frVar = this.j;
        if (frVar != null) {
            return frVar;
        }
        wg6.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(q, "startLoading");
        b();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void k1() {
        Object r0;
        ax5<xe4> ax5 = this.g;
        if (ax5 != null) {
            WatchSettingFragmentBinding a2 = ax5.a();
            if (a2 != null && (r0 = a2.s) != 0) {
                r0.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886976));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void o() {
        FragmentActivity activity;
        FLogger.INSTANCE.getLocal().d(q, "finish");
        if (isActive() && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v19, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v20, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v21, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v23, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v24, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v25, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        WatchSettingFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        WatchSettingFragmentBinding a2 = kb.a(layoutInflater, 2131558625, viewGroup, false, e1());
        PortfolioApp.get.instance().g().a(new av5()).a(this);
        w04 w04 = this.o;
        if (w04 != null) {
            bv5 a3 = vd.a(this, w04).a(bv5.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026ingViewModel::class.java)");
            this.h = a3;
            bv5 bv5 = this.h;
            if (bv5 != null) {
                bv5.n();
                Bundle arguments = getArguments();
                Object obj = arguments != null ? arguments.get("SERIAL") : null;
                if (obj != null) {
                    String str = (String) obj;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = q;
                    local.d(str2, "serial=" + str);
                    bv5 bv52 = this.h;
                    if (bv52 != null) {
                        bv52.i(str);
                        bv5 bv53 = this.h;
                        if (bv53 != null) {
                            bv53.b().a(getViewLifecycleOwner(), new b(this));
                            bv5 bv54 = this.h;
                            if (bv54 != null) {
                                bv54.d().a(getViewLifecycleOwner(), new c(this));
                                bv5 bv55 = this.h;
                                if (bv55 != null) {
                                    bv55.j().a(getViewLifecycleOwner(), new d(this, str));
                                    bv5 bv56 = this.h;
                                    if (bv56 != null) {
                                        if (bv56.q()) {
                                            Object r8 = a2.H;
                                            wg6.a((Object) r8, "bindingLocal.tvVibration");
                                            r8.setVisibility(0);
                                            Object r82 = a2.v;
                                            wg6.a((Object) r82, "bindingLocal.fbVibrationLow");
                                            r82.setVisibility(0);
                                            Object r83 = a2.w;
                                            wg6.a((Object) r83, "bindingLocal.fbVibrationMedium");
                                            r83.setVisibility(0);
                                            Object r84 = a2.u;
                                            wg6.a((Object) r84, "bindingLocal.fbVibrationHigh");
                                            r84.setVisibility(0);
                                        } else {
                                            Object r85 = a2.H;
                                            wg6.a((Object) r85, "bindingLocal.tvVibration");
                                            r85.setVisibility(8);
                                            Object r86 = a2.v;
                                            wg6.a((Object) r86, "bindingLocal.fbVibrationLow");
                                            r86.setVisibility(8);
                                            Object r87 = a2.w;
                                            wg6.a((Object) r87, "bindingLocal.fbVibrationMedium");
                                            r87.setVisibility(8);
                                            Object r88 = a2.u;
                                            wg6.a((Object) r88, "bindingLocal.fbVibrationHigh");
                                            r88.setVisibility(8);
                                        }
                                        this.g = new ax5<>(this, a2);
                                        ax5<xe4> ax5 = this.g;
                                        if (ax5 != null) {
                                            WatchSettingFragmentBinding a4 = ax5.a();
                                            if (a4 != null) {
                                                wg6.a((Object) a4, "mBinding.get()!!");
                                                return a4.d();
                                            }
                                            wg6.a();
                                            throw null;
                                        }
                                        wg6.d("mBinding");
                                        throw null;
                                    }
                                    wg6.d("mViewModel");
                                    throw null;
                                }
                                wg6.d("mViewModel");
                                throw null;
                            }
                            wg6.d("mViewModel");
                            throw null;
                        }
                        wg6.d("mViewModel");
                        throw null;
                    }
                    wg6.d("mViewModel");
                    throw null;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.String");
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        WatchSettingFragment.super.onPause();
        bv5 bv5 = this.h;
        if (bv5 != null) {
            bv5.o();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        WatchSettingFragment.super.onResume();
        bv5 bv5 = this.h;
        if (bv5 != null) {
            bv5.n();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v9, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        String b2;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        fr a2 = wq.a(this);
        wg6.a((Object) a2, "Glide.with(this)");
        this.j = a2;
        ax5<xe4> ax5 = this.g;
        if (ax5 != null) {
            WatchSettingFragmentBinding a3 = ax5.a();
            if (a3 != null) {
                a3.F.setOnClickListener(new e(this));
                a3.C.setOnClickListener(new f(this));
                a3.z.setOnClickListener(new g(this));
                ConstraintLayout constraintLayout = a3.t;
                if (!(constraintLayout == null || (b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND)) == null)) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                }
                a3.u.setOnClickListener(new h(this));
                a3.w.setOnClickListener(new i(this));
                a3.v.setOnClickListener(new j(this));
                a3.r.setOnClickListener(new k(this));
                a3.s.setOnClickListener(new l(this));
            }
            W("watch_setting_view");
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void r(int i2) {
        ax5<xe4> ax5 = this.g;
        if (ax5 != null) {
            WatchSettingFragmentBinding a2 = ax5.a();
            if (a2 != null) {
                a2.u.a("flexible_button_secondary");
                a2.w.a("flexible_button_secondary");
                a2.v.a("flexible_button_secondary");
                if (i2 == 25) {
                    a2.v.a("flexible_button_primary");
                } else if (i2 == 50) {
                    a2.w.a("flexible_button_primary");
                } else if (i2 == 100) {
                    a2.u.a("flexible_button_primary");
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v13, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v14, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v22, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v23, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v24, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r10v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v35, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v28, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v29, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v31, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v32, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v33, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r10v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(bv5.c cVar) {
        wg6.b(cVar, "watchSetting");
        FLogger.INSTANCE.getLocal().d(q, "updateDeviceInfo");
        if (isActive()) {
            ax5<xe4> ax5 = this.g;
            if (ax5 != null) {
                WatchSettingFragmentBinding a2 = ax5.a();
                if (a2 != null) {
                    Object r2 = a2.serial_value;
                    wg6.a((Object) r2, "it.tvSerialValue");
                    r2.setText(cVar.a().getDeviceId());
                    Object r22 = a2.fw_version_value;
                    wg6.a((Object) r22, "it.tvFwVersionValue");
                    r22.setText(cVar.a().getFirmwareRevision());
                    Object r23 = a2.B;
                    wg6.a((Object) r23, "it.tvDeviceName");
                    r23.setText(cVar.b());
                    boolean d2 = cVar.d();
                    if (d2) {
                        Boolean c2 = cVar.c();
                        if (c2 != null && c2.booleanValue()) {
                            Integer vibrationStrength = cVar.a().getVibrationStrength();
                            if (vibrationStrength != null) {
                                r(vibrationStrength.intValue());
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        if (cVar.e()) {
                            Object r24 = a2.B;
                            wg6.a((Object) r24, "it.tvDeviceName");
                            r24.setAlpha(1.0f);
                            a2.B.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, w6.c(PortfolioApp.get.instance(), 2131231087), (Drawable) null);
                            Object r25 = a2.A;
                            wg6.a((Object) r25, "it.tvConnectionStatus");
                            r25.setAlpha(1.0f);
                            Object r26 = a2.A;
                            wg6.a((Object) r26, "it.tvConnectionStatus");
                            r26.setText(a(true, cVar.a().getBatteryLevel()));
                            if (cVar.a().getBatteryLevel() >= 0) {
                                int batteryLevel = cVar.a().getBatteryLevel();
                                a2.A.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, w6.c(PortfolioApp.get.instance(), (batteryLevel >= 0 && 25 >= batteryLevel) ? 2131231049 : (25 <= batteryLevel && 50 >= batteryLevel) ? 2131231051 : (50 <= batteryLevel && 75 >= batteryLevel) ? 2131231053 : 2131231047), (Drawable) null);
                            }
                            Object r1 = a2.q;
                            wg6.a((Object) r1, "it.btActive");
                            r1.setVisibility(0);
                            Object r12 = a2.s;
                            wg6.a((Object) r12, "it.btConnect");
                            r12.setVisibility(8);
                            Object r13 = a2.z;
                            wg6.a((Object) r13, "it.tvCalibration");
                            r13.setAlpha(1.0f);
                            Object r14 = a2.u;
                            wg6.a((Object) r14, "it.fbVibrationHigh");
                            r14.setEnabled(true);
                            Object r15 = a2.v;
                            wg6.a((Object) r15, "it.fbVibrationLow");
                            r15.setEnabled(true);
                            Object r16 = a2.w;
                            wg6.a((Object) r16, "it.fbVibrationMedium");
                            r16.setEnabled(true);
                            Object r17 = a2.z;
                            wg6.a((Object) r17, "it.tvCalibration");
                            r17.setEnabled(true);
                        } else {
                            a2.B.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                            String str = this.i;
                            if (str != null) {
                                a2.B.setTextColor(Color.parseColor(str));
                            }
                            Object r18 = a2.B;
                            wg6.a((Object) r18, "it.tvDeviceName");
                            r18.setAlpha(0.4f);
                            Object r19 = a2.A;
                            wg6.a((Object) r19, "it.tvConnectionStatus");
                            r19.setText(a(false, cVar.a().getBatteryLevel()));
                            Object r110 = a2.A;
                            wg6.a((Object) r110, "it.tvConnectionStatus");
                            r110.setAlpha(0.4f);
                            Object r111 = a2.q;
                            wg6.a((Object) r111, "it.btActive");
                            r111.setVisibility(8);
                            Object r112 = a2.s;
                            wg6.a((Object) r112, "it.btConnect");
                            r112.setVisibility(0);
                            Object r113 = a2.s;
                            wg6.a((Object) r113, "it.btConnect");
                            r113.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886976));
                            Object r114 = a2.z;
                            wg6.a((Object) r114, "it.tvCalibration");
                            r114.setAlpha(0.4f);
                            Object r115 = a2.z;
                            wg6.a((Object) r115, "it.tvCalibration");
                            r115.setEnabled(false);
                        }
                    } else if (!d2) {
                        a2.u.a("flexible_button_secondary");
                        a2.w.a("flexible_button_secondary");
                        a2.v.a("flexible_button_secondary");
                        a2.B.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                        String str2 = this.i;
                        if (str2 != null) {
                            a2.B.setTextColor(Color.parseColor(str2));
                        }
                        Object r116 = a2.B;
                        wg6.a((Object) r116, "it.tvDeviceName");
                        r116.setAlpha(0.4f);
                        Object r117 = a2.A;
                        wg6.a((Object) r117, "it.tvConnectionStatus");
                        r117.setText(a(false, cVar.a().getBatteryLevel()));
                        Object r118 = a2.A;
                        wg6.a((Object) r118, "it.tvConnectionStatus");
                        r118.setAlpha(0.4f);
                        Object r119 = a2.q;
                        wg6.a((Object) r119, "it.btActive");
                        r119.setVisibility(8);
                        Object r120 = a2.s;
                        wg6.a((Object) r120, "it.btConnect");
                        r120.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886977));
                        Object r121 = a2.z;
                        wg6.a((Object) r121, "it.tvCalibration");
                        r121.setAlpha(0.4f);
                        Object r122 = a2.z;
                        wg6.a((Object) r122, "it.tvCalibration");
                        r122.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(cVar.a().getDeviceId()).setSerialPrefix(DeviceHelper.o.b(cVar.a().getDeviceId())).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.x;
                    wg6.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.b(cVar.a().getDeviceId(), DeviceHelper.ImageStyle.SMALL)).setImageCallback(new m(a2, this, cVar)).download();
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        wg6.b(str, "message");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        switch (str.hashCode()) {
            case -2051261777:
                if (str.equals("REMOVE_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == 2131363105) {
                        bv5 bv5 = this.h;
                        if (bv5 != null) {
                            bv5.g(stringExtra);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363190) {
                        bv5 bv52 = this.h;
                        if (bv52 != null) {
                            bv52.c(stringExtra);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -1138109835:
                if (str.equals("SWITCH_DEVICE_ERASE_FAIL")) {
                    String stringExtra2 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra2 != null && i2 == 2131363190) {
                        bv5 bv53 = this.h;
                        if (bv53 != null) {
                            bv53.e(stringExtra2);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -693701870:
                if (str.equals("CONFIRM_REMOVE_DEVICE")) {
                    if (i2 == 2131363190) {
                        bv5 bv54 = this.h;
                        if (bv54 != null) {
                            bv54.m();
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -454228492:
                if (str.equals("REMOVE_DEVICE_SYNC_FAIL")) {
                    String stringExtra3 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra3 != null && i2 == 2131363190) {
                        bv5 bv55 = this.h;
                        if (bv55 != null) {
                            bv55.b(stringExtra3);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    String stringExtra4 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra4 != null && i2 == 2131363190) {
                        bv5 bv56 = this.h;
                        if (bv56 != null) {
                            bv56.f(stringExtra4);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra5 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra5 == null) {
                        return;
                    }
                    if (i2 == 2131363105) {
                        bv5 bv57 = this.h;
                        if (bv57 != null) {
                            bv57.h(stringExtra5);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363190) {
                        bv5 bv58 = this.h;
                        if (bv58 != null) {
                            bv58.d(stringExtra5);
                            return;
                        } else {
                            wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
        }
        super.a(str, i2, intent);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(boolean z, int i2) {
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), z ? 2131886949 : 2131886926);
        if (!z || i2 <= 0) {
            wg6.a((Object) a2, "connectedString");
            return a2;
        }
        return a2 + ", " + i2 + '%';
    }
}
