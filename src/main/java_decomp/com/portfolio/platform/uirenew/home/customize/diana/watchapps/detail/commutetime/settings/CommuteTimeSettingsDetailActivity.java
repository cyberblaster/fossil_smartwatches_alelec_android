package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.CommuteTimeSettingsDetailFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle, int i) {
            wg6.b(fragment, "fragment");
            wg6.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDetailActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_ADDRESS", bundle);
            fragment.startActivityForResult(intent, i);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (getSupportFragmentManager().b(2131362119) == null) {
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_ADDRESS");
            a((Fragment) CommuteTimeSettingsDetailFragment.q.a((AddressWrapper) bundleExtra.getParcelable("KEY_SELECTED_ADDRESS"), bundleExtra.getStringArrayList("KEY_LIST_ADDRESS"), bundleExtra.getBoolean("KEY_HAVING_MAP_RESULT")), CommuteTimeSettingsDetailFragment.q.a(), 2131362119);
        }
    }
}
