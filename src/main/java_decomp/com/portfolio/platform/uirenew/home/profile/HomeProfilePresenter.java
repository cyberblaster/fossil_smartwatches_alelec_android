package com.portfolio.platform.uirenew.home.profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.dt4;
import com.fossil.ff6;
import com.fossil.ft4;
import com.fossil.gk6;
import com.fossil.ht4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.lk4;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mj5;
import com.fossil.nc6;
import com.fossil.nj5;
import com.fossil.oj5;
import com.fossil.pj5;
import com.fossil.pj5$g$a;
import com.fossil.pj5$g$b;
import com.fossil.pj5$h$a;
import com.fossil.pj5$l$a;
import com.fossil.pj5$m$a;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xp6;
import com.fossil.yx5;
import com.fossil.zp6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.HomeProfileFragment;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeProfilePresenter extends nj5 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase A;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
    @DexIgnore
    public /* final */ ArrayList<pj5.b> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Device> g; // = new ArrayList<>();
    @DexIgnore
    public MFUser h;
    @DexIgnore
    public LiveData<yx5<ActivityStatistic>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<ActivitySummary>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<SleepStatistic>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ LiveData<List<Device>> l; // = this.w.getAllDeviceAsLiveData();
    @DexIgnore
    public ActivityStatistic.ActivityDailyBest m;
    @DexIgnore
    public long n;
    @DexIgnore
    public /* final */ xp6 o; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ Handler p; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Runnable q; // = new f(this);
    @DexIgnore
    public /* final */ e r; // = new e(this);
    @DexIgnore
    public /* final */ oj5 s;
    @DexIgnore
    public /* final */ PortfolioApp t;
    @DexIgnore
    public /* final */ dt4 u;
    @DexIgnore
    public /* final */ UpdateUser v;
    @DexIgnore
    public /* final */ DeviceRepository w;
    @DexIgnore
    public /* final */ UserRepository x;
    @DexIgnore
    public /* final */ SummariesRepository y;
    @DexIgnore
    public /* final */ SleepSummariesRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomeProfilePresenter.B;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b(String str, boolean z, String str2, int i, boolean z2, boolean z3) {
            wg6.b(str, "serial");
            wg6.b(str2, "deviceName");
            this.a = str;
            this.b = z;
            this.c = str2;
            this.d = i;
            this.e = z2;
            this.f = z3;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final boolean d() {
            return this.e;
        }

        @DexIgnore
        public final boolean e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && this.b == bVar.b && wg6.a((Object) this.c, (Object) bVar.c) && this.d == bVar.d && this.e == bVar.e && this.f == bVar.f;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i2 = (hashCode + (z ? 1 : 0)) * 31;
            String str2 = this.c;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int a2 = (((i2 + i) * 31) + com.fossil.d.a(this.d)) * 31;
            boolean z2 = this.e;
            if (z2) {
                z2 = true;
            }
            int i3 = (a2 + (z2 ? 1 : 0)) * 31;
            boolean z3 = this.f;
            if (z3) {
                z3 = true;
            }
            return i3 + (z3 ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "DeviceWrapper(serial=" + this.a + ", isConnected=" + this.b + ", deviceName=" + this.c + ", batteryLevel=" + this.d + ", isActive=" + this.e + ", isLatestFw=" + this.f + ")";
        }

        @DexIgnore
        public final int a() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<dt4.a, m24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public c(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wg6.b(aVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dt4.a aVar) {
            wg6.b(aVar, "responseValue");
            MFUser a2 = aVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HomeProfilePresenter.C.a();
            local.d(a3, "loadUser " + a2);
            if (a2 != null) {
                this.a.a(a2);
                this.a.m().a(a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<ht4.d, ht4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public d(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.m().d();
            this.a.m().O();
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.m().d();
            this.a.m().a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public e(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
            boolean z = true;
            if (xj6.b(stringExtra, this.a.t.e(), true) && (!this.a.k().isEmpty())) {
                Iterator<T> it = this.a.k().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((b) t).c(), (Object) stringExtra)) {
                        break;
                    }
                }
                b bVar = (b) t;
                if (bVar == null) {
                    return;
                }
                if (intExtra == ConnectionStateChange.GATT_ON.ordinal() || intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                    if (intExtra != ConnectionStateChange.GATT_ON.ordinal()) {
                        z = false;
                    }
                    bVar.a(z);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = HomeProfilePresenter.C.a();
                    local2.d(a3, "active device status change connected=" + bVar.e());
                    this.a.m().c(this.a.k());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public f(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public final void run() {
            if (this.a.m().isActive() && (!this.a.k().isEmpty())) {
                this.a.m().K();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1", f = "HomeProfilePresenter.kt", l = {337, 343}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Uri $imageUri;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(HomeProfilePresenter homeProfilePresenter, Uri uri, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
            this.$imageUri = uri;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$imageUri, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            MFUser mFUser;
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 b = this.this$0.c();
                pj5$g$b pj5_g_b = new pj5$g$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, pj5_g_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                mFUser = (MFUser) this.L$2;
                Bitmap bitmap = (Bitmap) this.L$1;
                il6 il62 = (il6) this.L$0;
                try {
                    nc6.a(obj);
                    mFUser.setProfilePicture((String) obj);
                    this.this$0.b(this.this$0.l());
                } catch (Exception e) {
                    e.printStackTrace();
                    this.this$0.m().z();
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Bitmap bitmap2 = (Bitmap) obj;
            MFUser l = this.this$0.l();
            if (l != null) {
                dl6 b2 = this.this$0.c();
                pj5$g$a pj5_g_a = new pj5$g$a(bitmap2, (xe6) null);
                this.L$0 = il6;
                this.L$1 = bitmap2;
                this.L$2 = l;
                this.label = 2;
                obj = gk6.a(b2, pj5_g_a, this);
                if (obj == a) {
                    return a;
                }
                mFUser = l;
                mFUser.setProfilePicture((String) obj);
            }
            this.this$0.b(this.this$0.l());
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1", f = "HomeProfilePresenter.kt", l = {357}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(HomeProfilePresenter homeProfilePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                oj5 m = this.this$0.m();
                if (m != null) {
                    FragmentActivity activity = ((HomeProfileFragment) m).getActivity();
                    if (activity != null) {
                        dl6 a2 = this.this$0.b();
                        pj5$h$a pj5_h_a = new pj5$h$a(activity, (xe6) null);
                        this.L$0 = il6;
                        this.L$1 = activity;
                        this.L$2 = activity;
                        this.label = 1;
                        obj = gk6.a(a2, pj5_h_a, this);
                        if (obj == a) {
                            return a;
                        }
                    }
                    return cd6.a;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
            } else if (i == 1) {
                FragmentActivity fragmentActivity = (FragmentActivity) this.L$2;
                FragmentActivity fragmentActivity2 = (FragmentActivity) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Intent intent = (Intent) obj;
            if (intent != null) {
                ((HomeProfileFragment) this.this$0.m()).startActivityForResult(intent, 1234);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ld<yx5<? extends ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public i(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<ActivitySummary> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "start - mActivitySummaryLiveData -- resource=" + yx5);
            ActivitySummary activitySummary = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    activitySummary = yx5.d();
                }
                this.a.n = activitySummary != null ? (long) activitySummary.getSteps() : 0;
                HomeProfilePresenter homeProfilePresenter = this.a;
                homeProfilePresenter.a(homeProfilePresenter.m, this.a.n);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ld<yx5<? extends ActivityStatistic>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public j(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<ActivityStatistic> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "start - mActivityStatisticLiveData -- resource=" + yx5);
            ActivityStatistic.ActivityDailyBest activityDailyBest = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                ActivityStatistic d = yx5 != null ? yx5.d() : null;
                this.a.m().a(d);
                HomeProfilePresenter homeProfilePresenter = this.a;
                if (d != null) {
                    activityDailyBest = d.getStepsBestDay();
                }
                homeProfilePresenter.m = activityDailyBest;
                HomeProfilePresenter homeProfilePresenter2 = this.a;
                homeProfilePresenter2.a(homeProfilePresenter2.m, this.a.n);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ld<yx5<? extends SleepStatistic>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public k(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<SleepStatistic> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "start - mSleepStatisticLiveData -- resource=" + yx5);
            SleepStatistic sleepStatistic = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    SleepStatistic d = yx5.d();
                }
                oj5 m = this.a.m();
                if (yx5 != null) {
                    sleepStatistic = yx5.d();
                }
                m.a(sleepStatistic);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ld<List<? extends Device>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public l(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<Device> list) {
            ArrayList arrayList = new ArrayList();
            this.a.g.clear();
            ArrayList e = this.a.g;
            if (list != null) {
                e.addAll(list);
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new pj5$l$a(this, list, arrayList, (xe6) null), 3, (Object) null);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public m(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.C.a(), ".Inside updateUser onSuccess");
            if (this.a.m().isActive()) {
                this.a.m().d();
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new pj5$m$a(this, (xe6) null), 3, (Object) null);
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, ".Inside updateUser onError errorCode=" + cVar.a());
            if (this.a.m().isActive()) {
                this.a.m().d();
                this.a.m().a(cVar.a(), "");
            }
        }
    }

    /*
    static {
        String simpleName = HomeProfilePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "HomeProfilePresenter::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public HomeProfilePresenter(oj5 oj5, PortfolioApp portfolioApp, dt4 dt4, UpdateUser updateUser, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        wg6.b(oj5, "mView");
        wg6.b(portfolioApp, "mApp");
        wg6.b(dt4, "mGetUser");
        wg6.b(updateUser, "mUpdateUser");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wg6.b(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        this.s = oj5;
        this.t = portfolioApp;
        this.u = dt4;
        this.v = updateUser;
        this.w = deviceRepository;
        this.x = userRepository;
        this.y = summariesRepository;
        this.z = sleepSummariesRepository;
        this.A = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final MFUser l() {
        return this.h;
    }

    @DexIgnore
    public final oj5 m() {
        return this.s;
    }

    @DexIgnore
    public final void n() {
        this.p.removeCallbacksAndMessages((Object) null);
        this.p.postDelayed(this.q, 60000);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.fossil.dt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$c, com.portfolio.platform.CoroutineUseCase$e] */
    public final void o() {
        this.u.a(null, new c(this));
    }

    @DexIgnore
    public void p() {
        this.s.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$m] */
    public final void b(MFUser mFUser) {
        if (mFUser != null) {
            this.s.e();
            this.v.a(new UpdateUser.b(mFUser), new m(this));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void f() {
        Object r0 = this.t;
        e eVar = this.r;
        r0.registerReceiver(eVar, new IntentFilter(this.t.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        o();
        this.j = this.y.getSummary(new Date());
        this.i = this.y.getActivityStatistic(true);
        this.k = this.z.getSleepStatistic(true);
        LiveData<yx5<ActivitySummary>> liveData = this.j;
        oj5 oj5 = this.s;
        if (oj5 != null) {
            liveData.a((HomeProfileFragment) oj5, new i(this));
            this.i.a(this.s, new j(this));
            this.k.a(this.s, new k(this));
            this.l.a(this.s, new l(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void g() {
        LiveData<yx5<ActivitySummary>> liveData = this.j;
        oj5 oj5 = this.s;
        if (oj5 != null) {
            liveData.a((HomeProfileFragment) oj5);
            this.l.a(this.s);
            this.i.a(this.s);
            this.k.a(this.s);
            this.p.removeCallbacksAndMessages((Object) null);
            try {
                this.t.unregisterReceiver(this.r);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = B;
                local.d(str, "stop with " + e2);
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$d, com.portfolio.platform.CoroutineUseCase$e] */
    public void i() {
        this.s.e();
        Object r0 = this.A;
        oj5 oj5 = this.s;
        if (oj5 != null) {
            FragmentActivity activity = ((HomeProfileFragment) oj5).getActivity();
            if (activity != null) {
                r0.a(new DeleteLogoutUserUseCase.b(1, new WeakReference(activity)), new d(this));
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.app.Activity");
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
    }

    @DexIgnore
    public void j() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final ArrayList<pj5.b> k() {
        return this.f;
    }

    @DexIgnore
    public void b(Intent intent) {
        wg6.b(intent, "intent");
        if (this.s.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && xj6.b(stringExtra, PortfolioApp.get.instance().e(), true)) {
                this.s.K();
                n();
            }
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        this.h = mFUser;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(Intent intent) {
        Uri a2 = lk4.a(intent, (Context) PortfolioApp.get.instance());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .onActivityResult imageUri=");
        if (a2 != null) {
            sb.append(a2);
            local.d(str, sb.toString());
            if (PortfolioApp.get.instance().a(intent, a2)) {
                rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new g(this, a2, (xe6) null), 3, (Object) null);
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a(ActivityStatistic.ActivityDailyBest activityDailyBest, long j2) {
        if (j2 > (activityDailyBest != null ? (long) activityDailyBest.getValue() : 0)) {
            this.s.a(new mj5(new Date(), j2));
        }
    }
}
