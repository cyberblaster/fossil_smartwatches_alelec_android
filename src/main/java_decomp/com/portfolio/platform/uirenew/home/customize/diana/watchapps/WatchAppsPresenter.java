package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl4;
import com.fossil.dl6;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.l75;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m75;
import com.fossil.nc6;
import com.fossil.p75$b$a;
import com.fossil.p75$d$a;
import com.fossil.p75$f$a;
import com.fossil.p75$j$a;
import com.fossil.pc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.qj4;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xx5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppsPresenter extends l75 {
    @DexIgnore
    public DianaCustomizeViewModel e;
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<WatchApp>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<lc6<String, Boolean>>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<pc6<String, Boolean, Parcelable>> j; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<Category> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> l;
    @DexIgnore
    public /* final */ ld<String> m;
    @DexIgnore
    public /* final */ LiveData<List<WatchApp>> n;
    @DexIgnore
    public /* final */ ld<List<WatchApp>> o;
    @DexIgnore
    public /* final */ LiveData<List<lc6<String, Boolean>>> p;
    @DexIgnore
    public /* final */ ld<List<lc6<String, Boolean>>> q;
    @DexIgnore
    public /* final */ ld<pc6<String, Boolean, Parcelable>> r;
    @DexIgnore
    public /* final */ m75 s;
    @DexIgnore
    public /* final */ CategoryRepository t;
    @DexIgnore
    public /* final */ an4 u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1", f = "WatchAppsPresenter.kt", l = {295}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchAppsPresenter watchAppsPresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppsPresenter;
            this.$id = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$id, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            boolean z;
            Object a = ff6.a();
            int i = this.label;
            Parcelable parcelable = null;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                z = dl4.c.f(this.$id);
                if (z) {
                    dl6 a2 = this.this$0.b();
                    p75$b$a p75_b_a = new p75$b$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = null;
                    this.Z$0 = z;
                    this.label = 1;
                    obj = gk6.a(a2, p75_b_a, this);
                    if (obj == a) {
                        return a;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.j.a(new pc6(this.$id, hf6.a(z), parcelable));
                return cd6.a;
            } else if (i == 1) {
                boolean z2 = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                z = z2;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) obj;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.j.a(new pc6(this.$id, hf6.a(z), parcelable));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public c(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.s.e(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public d(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(WatchApp watchApp) {
            String str = (String) this.a.g.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("transform from selected WatchApp to category");
            sb.append(" currentCategory=");
            sb.append(str);
            sb.append(" watchAppCategories=");
            sb.append(watchApp != null ? watchApp.getCategories() : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (watchApp != null) {
                ArrayList<String> categories = watchApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.g.a(categories.get(0));
                } else {
                    this.a.g.a(str);
                }
            } else {
                WatchAppsPresenter watchAppsPresenter = this.a;
                rm6 unused = ik6.b(watchAppsPresenter.e(), (af6) null, (ll6) null, new p75$d$a(watchAppsPresenter, (xe6) null), 3, (Object) null);
            }
            return this.a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<List<? extends lc6<? extends String, ? extends Boolean>>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public e(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0076, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION) != false) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0095, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE) != false) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0097, code lost:
            r0 = com.fossil.nh6.a;
            r0 = com.fossil.jm4.a((android.content.Context) com.portfolio.platform.PortfolioApp.get.instance(), 2131886979);
            com.fossil.wg6.a((java.lang.Object) r0, "LanguageHelper.getString\u2026nLocationServicesToAllow)");
            r3 = new java.lang.Object[]{com.portfolio.platform.PortfolioApp.get.instance().i()};
            r0 = java.lang.String.format(r0, java.util.Arrays.copyOf(r3, r3.length));
            com.fossil.wg6.a((java.lang.Object) r0, "java.lang.String.format(format, *args)");
         */
        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<lc6<String, Boolean>> list) {
            lc6 lc6;
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        lc6 = null;
                        break;
                    }
                    lc6 = it.next();
                    if (!((Boolean) lc6.getSecond()).booleanValue()) {
                        break;
                    }
                }
                lc6 lc62 = (lc6) lc6;
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onLiveDataChanged permissionsRequired " + lc62 + ' ');
                String str = "";
                int i = 0;
                if (lc62 != null) {
                    String str2 = (String) lc62.getFirst();
                    switch (str2.hashCode()) {
                        case 385352715:
                            break;
                        case 564039755:
                            if (str2.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                                str = jm4.a((Context) PortfolioApp.get.instance(), 2131887041);
                                break;
                            }
                            break;
                        case 766697727:
                            break;
                        case 2009556792:
                            if (str2.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                                str = jm4.a((Context) PortfolioApp.get.instance(), 2131887359);
                                break;
                            }
                            break;
                    }
                    m75 j = this.a.s;
                    if (!(list instanceof Collection) || !list.isEmpty()) {
                        for (lc6 second : list) {
                            if (((Boolean) second.getSecond()).booleanValue() && (i = i + 1) < 0) {
                                qd6.b();
                                throw null;
                            }
                        }
                    }
                    int size = list.size();
                    String a2 = xx5.a.a((String) lc62.getFirst());
                    wg6.a((Object) str, "content");
                    j.a(i, size, a2, str);
                    return;
                }
                this.a.s.a(0, 0, str, str);
                WatchApp watchApp = (WatchApp) WatchAppsPresenter.f(this.a).h().a();
                if (watchApp != null) {
                    m75 j2 = this.a.s;
                    String a3 = jm4.a(PortfolioApp.get.instance(), watchApp.getDescriptionKey(), watchApp.getDescription());
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    j2.H(a3);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public f(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<lc6<String, Boolean>>> apply(WatchApp watchApp) {
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new p75$f$a(this, watchApp, (xe6) null), 3, (Object) null);
            return this.a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<pc6<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public g(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public final void onChanged(pc6<String, Boolean, ? extends Parcelable> pc6) {
            String str;
            String str2;
            String str3;
            if (pc6 != null) {
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onLiveDataChanged setting of " + pc6.getFirst() + " isSettingRequired " + pc6.getSecond().booleanValue() + " setting " + ((Parcelable) pc6.getThird()) + ' ');
                String str4 = "";
                if (pc6.getSecond().booleanValue()) {
                    Parcelable parcelable = (Parcelable) pc6.getThird();
                    if (parcelable == null) {
                        str = str4;
                        str4 = dl4.c.a(pc6.getFirst());
                    } else {
                        try {
                            String first = pc6.getFirst();
                            int hashCode = first.hashCode();
                            if (hashCode == -829740640) {
                                if (first.equals("commute-time")) {
                                    str2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886196);
                                    wg6.a((Object) str2, "LanguageHelper.getString\u2026Title__SavedDestinations)");
                                }
                                str = str4;
                            } else if (hashCode != 1223440372) {
                                str2 = str4;
                            } else {
                                if (first.equals("weather")) {
                                    List<WeatherLocationWrapper> locations = ((WeatherWatchAppSetting) parcelable).getLocations();
                                    str = str4;
                                    for (WeatherLocationWrapper next : locations) {
                                        try {
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str);
                                            if (locations.indexOf(next) < qd6.a(locations)) {
                                                str3 = next.getName() + "; ";
                                            } else {
                                                str3 = next.getName();
                                            }
                                            sb.append(str3);
                                            str = sb.toString();
                                        } catch (Exception e) {
                                            e = e;
                                            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                                            this.a.s.a(true, pc6.getFirst(), str4, str);
                                            return;
                                        }
                                    }
                                }
                                str = str4;
                            }
                            str = str2;
                        } catch (Exception e2) {
                            e = e2;
                            str = str4;
                            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                            this.a.s.a(true, pc6.getFirst(), str4, str);
                            return;
                        }
                    }
                    this.a.s.a(true, pc6.getFirst(), str4, str);
                    return;
                }
                this.a.s.a(false, pc6.getFirst(), str4, (String) null);
                WatchApp watchApp = (WatchApp) WatchAppsPresenter.f(this.a).h().a();
                if (watchApp != null) {
                    m75 j = this.a.s;
                    String a2 = jm4.a(PortfolioApp.get.instance(), watchApp.getDescriptionKey(), watchApp.getDescription());
                    wg6.a((Object) a2, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    j.H(a2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<List<? extends WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public h(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WatchApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged WatchApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (list != null) {
                this.a.s.v(list);
                WatchApp watchApp = (WatchApp) WatchAppsPresenter.f(this.a).h().a();
                if (watchApp != null) {
                    this.a.s.b(watchApp);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public i(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<WatchApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "transform from category to list watchapps with category=" + str);
            DianaCustomizeViewModel f = WatchAppsPresenter.f(this.a);
            wg6.a((Object) str, "category");
            List<WatchApp> c = f.c(str);
            ArrayList arrayList = new ArrayList();
            DianaPreset dianaPreset = (DianaPreset) WatchAppsPresenter.f(this.a).a().a();
            WatchApp watchApp = (WatchApp) WatchAppsPresenter.f(this.a).h().a();
            String watchappId = watchApp != null ? watchApp.getWatchappId() : null;
            if (dianaPreset != null) {
                for (WatchApp next : c) {
                    Iterator<T> it = dianaPreset.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                        boolean z = true;
                        if (!wg6.a((Object) dianaPresetWatchAppSetting.getId(), (Object) next.getWatchappId()) || !(!wg6.a((Object) dianaPresetWatchAppSetting.getId(), (Object) watchappId))) {
                            z = false;
                            continue;
                        }
                        if (z) {
                            break;
                        }
                    }
                    if (((DianaPresetWatchAppSetting) t) == null || wg6.a((Object) next.getWatchappId(), (Object) "empty")) {
                        arrayList.add(next);
                    }
                }
            }
            this.a.h.a(arrayList);
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1", f = "WatchAppsPresenter.kt", l = {252}, m = "invokeSuspend")
    public static final class j extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(WatchAppsPresenter watchAppsPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            j jVar = new j(this.this$0, xe6);
            jVar.p$ = (il6) obj;
            return jVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((j) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                p75$j$a p75_j_a = new p75$j$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, p75_j_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) obj) {
                if (!WatchAppsPresenter.f(this.this$0).c(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.k.clear();
            this.this$0.k.addAll(arrayList);
            this.this$0.s.a(this.this$0.k);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements ld<WatchApp> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public k(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(WatchApp watchApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged selectedWatchApp value=" + watchApp);
            this.a.f.a(watchApp);
            if (watchApp != null) {
                this.a.s.w(dl4.c.e(watchApp.getWatchappId()));
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public WatchAppsPresenter(m75 m75, CategoryRepository categoryRepository, an4 an4) {
        wg6.b(m75, "mView");
        wg6.b(categoryRepository, "mCategoryRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.s = m75;
        this.t = categoryRepository;
        this.u = an4;
        new Gson();
        LiveData<String> b2 = sd.b(this.f, new d(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.l = b2;
        this.m = new c(this);
        LiveData<List<WatchApp>> b3 = sd.b(this.l, new i(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.n = b3;
        this.o = new h(this);
        LiveData<List<lc6<String, Boolean>>> b4 = sd.b(this.f, new f(this));
        wg6.a((Object) b4, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.p = b4;
        this.q = new e(this);
        this.r = new g(this);
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel f(WatchAppsPresenter watchAppsPresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = watchAppsPresenter.e;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final rm6 b(String str) {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onStart");
        this.l.a(this.m);
        this.n.a(this.o);
        this.p.a(this.q);
        this.j.a(this.r);
        if (this.k.isEmpty()) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new j(this, (xe6) null), 3, (Object) null);
        } else {
            this.s.a(this.k);
        }
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            LiveData<WatchApp> h2 = dianaCustomizeViewModel.h();
            m75 m75 = this.s;
            if (m75 != null) {
                h2.a((WatchAppsFragment) m75, new k(this));
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            LiveData<WatchApp> h2 = dianaCustomizeViewModel.h();
            m75 m75 = this.s;
            if (m75 != null) {
                h2.a((WatchAppsFragment) m75);
                this.h.b(this.o);
                this.g.b(this.m);
                this.i.b(this.q);
                this.j.b(this.r);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        T t2;
        String str;
        T t3;
        String str2;
        String str3;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        T t4 = null;
        if (dianaCustomizeViewModel != null) {
            DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wg6.a((Object) ((DianaPresetWatchAppSetting) t2).getPosition(), (Object) "top")) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                if (dianaPresetWatchAppSetting == null || (str = dianaPresetWatchAppSetting.getId()) == null) {
                    str = "empty";
                }
                Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it2.next();
                    if (wg6.a((Object) ((DianaPresetWatchAppSetting) t3).getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t3;
                if (dianaPresetWatchAppSetting2 == null || (str2 = dianaPresetWatchAppSetting2.getId()) == null) {
                    str2 = "empty";
                }
                Iterator<T> it3 = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (wg6.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) "bottom")) {
                        t4 = next;
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) t4;
                if (dianaPresetWatchAppSetting3 == null || (str3 = dianaPresetWatchAppSetting3.getId()) == null) {
                    str3 = "empty";
                }
                this.s.a(str, str2, str3);
                return;
            }
            this.s.a("empty", "empty", "empty");
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    public void i() {
        String str;
        ArrayList<DianaPresetWatchAppSetting> watchapps;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        T t2 = null;
        if (dianaCustomizeViewModel != null) {
            WatchApp watchApp = (WatchApp) dianaCustomizeViewModel.h().a();
            if (watchApp != null) {
                String component1 = watchApp.component1();
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel2.a().a();
                    if (!(dianaPreset == null || (watchapps = dianaPreset.getWatchapps()) == null)) {
                        Iterator<T> it = watchapps.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            T next = it.next();
                            if (wg6.a((Object) component1, (Object) ((DianaPresetWatchAppSetting) next).getId())) {
                                t2 = next;
                                break;
                            }
                        }
                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                        if (dianaPresetWatchAppSetting != null) {
                            str = dianaPresetWatchAppSetting.getSettings();
                        }
                    }
                    str = "";
                    String watchappId = watchApp.getWatchappId();
                    int hashCode = watchappId.hashCode();
                    if (hashCode != -829740640) {
                        if (hashCode == 1223440372 && watchappId.equals("weather")) {
                            this.s.O(str);
                        }
                    } else if (watchappId.equals("commute-time")) {
                        this.s.c(str);
                    }
                } else {
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            WatchApp watchApp = (WatchApp) dianaCustomizeViewModel.h().a();
            if (watchApp != null) {
                String component1 = watchApp.component1();
                if (dl4.c.e(component1)) {
                    this.s.d(component1);
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095  */
    public void k() {
        Object obj;
        boolean a2;
        List list = (List) this.i.a();
        if (list != null) {
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!((Boolean) ((lc6) obj).getSecond()).booleanValue()) {
                    break;
                }
            }
            lc6 lc6 = (lc6) obj;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "processRequiredPermission " + lc6);
            if (lc6 != null) {
                String str = (String) lc6.component1();
                boolean z = false;
                int hashCode = str.hashCode();
                if (hashCode != 564039755) {
                    if (hashCode == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        a2 = xx5.a.e();
                    }
                    if (z) {
                        this.s.D((String) lc6.getFirst());
                        return;
                    } else {
                        this.s.f((String) lc6.getFirst());
                        return;
                    }
                } else {
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        a2 = xx5.a.a((Context) PortfolioApp.get.instance());
                    }
                    if (z) {
                    }
                }
                z = !a2;
                if (z) {
                }
            }
        }
    }

    @DexIgnore
    public void l() {
        this.s.a(this);
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        wg6.b(dianaCustomizeViewModel, "viewModel");
        this.e = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void a(Category category) {
        wg6.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "category change " + category);
        this.g.a(category.getId());
    }

    @DexIgnore
    public void a(String str) {
        T t2;
        wg6.b(str, "watchappId");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            WatchApp d2 = dianaCustomizeViewModel.d(str);
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onUserChooseWatchApp " + d2);
            if (d2 != null) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel2.a().a();
                    if (dianaPreset != null) {
                        DianaPreset clone = dianaPreset.clone();
                        ArrayList arrayList = new ArrayList();
                        DianaCustomizeViewModel dianaCustomizeViewModel3 = this.e;
                        if (dianaCustomizeViewModel3 != null) {
                            Object a2 = dianaCustomizeViewModel3.i().a();
                            if (a2 != null) {
                                wg6.a(a2, "mDianaCustomizeViewModel\u2026ctedWatchAppPos().value!!");
                                String str2 = (String) a2;
                                DianaCustomizeViewModel dianaCustomizeViewModel4 = this.e;
                                if (dianaCustomizeViewModel4 != null) {
                                    if (!dianaCustomizeViewModel4.i(str)) {
                                        Iterator<DianaPresetWatchAppSetting> it = clone.getWatchapps().iterator();
                                        while (it.hasNext()) {
                                            DianaPresetWatchAppSetting next = it.next();
                                            if (wg6.a((Object) next.getPosition(), (Object) str2)) {
                                                arrayList.add(new DianaPresetWatchAppSetting(str2, str, next.getLocalUpdateAt()));
                                            } else {
                                                arrayList.add(next);
                                            }
                                        }
                                        clone.getWatchapps().clear();
                                        clone.getWatchapps().addAll(arrayList);
                                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Update current preset=" + clone);
                                        DianaCustomizeViewModel dianaCustomizeViewModel5 = this.e;
                                        if (dianaCustomizeViewModel5 != null) {
                                            dianaCustomizeViewModel5.a(clone);
                                        } else {
                                            wg6.d("mDianaCustomizeViewModel");
                                            throw null;
                                        }
                                    } else {
                                        Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                t2 = null;
                                                break;
                                            }
                                            t2 = it2.next();
                                            if (wg6.a((Object) ((DianaPresetWatchAppSetting) t2).getId(), (Object) str)) {
                                                break;
                                            }
                                        }
                                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                                        if (dianaPresetWatchAppSetting != null) {
                                            DianaCustomizeViewModel dianaCustomizeViewModel6 = this.e;
                                            if (dianaCustomizeViewModel6 != null) {
                                                dianaCustomizeViewModel6.k(dianaPresetWatchAppSetting.getPosition());
                                            } else {
                                                wg6.d("mDianaCustomizeViewModel");
                                                throw null;
                                            }
                                        }
                                    }
                                    String watchappId = d2.getWatchappId();
                                    if (watchappId.hashCode() == -829740640 && watchappId.equals("commute-time") && !this.u.h()) {
                                        this.u.o(true);
                                        this.s.d("commute-time");
                                        return;
                                    }
                                    return;
                                }
                                wg6.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    return;
                }
                wg6.d("mDianaCustomizeViewModel");
                throw null;
            }
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(String str, Parcelable parcelable) {
        T t2;
        wg6.b(str, "watchAppId");
        wg6.b(parcelable, MicroAppSetting.SETTING);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
            if (dianaPreset != null) {
                DianaPreset clone = dianaPreset.clone();
                Iterator<T> it = clone.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wg6.a((Object) ((DianaPresetWatchAppSetting) t2).getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                du3 du3 = new du3();
                du3.b(new qj4());
                Gson a2 = du3.a();
                if (dianaPresetWatchAppSetting != null) {
                    dianaPresetWatchAppSetting.setSettings(a2.a(parcelable));
                }
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "update current preset with new setting " + parcelable + " of " + str);
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.a(clone);
                } else {
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }
}
