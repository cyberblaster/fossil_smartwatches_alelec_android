package com.portfolio.platform.uirenew.home.details.sleep;

import android.graphics.RectF;
import android.os.Bundle;
import android.util.Pair;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.aj6;
import com.fossil.bj5$d$a;
import com.fossil.bj5$d$b;
import com.fossil.bj5$d$c;
import com.fossil.bj5$e$a;
import com.fossil.bj5$f$a;
import com.fossil.bj5$g$a;
import com.fossil.bj5$h$a;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ej5;
import com.fossil.ff6;
import com.fossil.fj5;
import com.fossil.gk6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.iz5;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.ti6;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.xg6;
import com.fossil.xi5;
import com.fossil.yd6;
import com.fossil.yi5;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDetailPresenter extends xi5 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<lc6<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE k; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
    @DexIgnore
    public List<MFSleepDay> l; // = new ArrayList();
    @DexIgnore
    public List<MFSleepSession> m; // = new ArrayList();
    @DexIgnore
    public MFSleepDay n;
    @DexIgnore
    public List<MFSleepSession> o;
    @DexIgnore
    public LiveData<yx5<List<MFSleepDay>>> p;
    @DexIgnore
    public LiveData<yx5<List<MFSleepSession>>> q;
    @DexIgnore
    public /* final */ yi5 r;
    @DexIgnore
    public /* final */ SleepSummariesRepository s;
    @DexIgnore
    public /* final */ SleepSessionsRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<MFSleepSession, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke((MFSleepSession) obj));
        }

        @DexIgnore
        public final boolean invoke(MFSleepSession mFSleepSession) {
            wg6.b(mFSleepSession, "it");
            return bk4.d(mFSleepSession.getDay(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter a;

        @DexIgnore
        public c(SleepDetailPresenter sleepDetailPresenter) {
            this.a = sleepDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<MFSleepSession>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.t.getSleepSessionList((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1", f = "SleepDetailPresenter.kt", l = {149, 173, 174}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(SleepDetailPresenter sleepDetailPresenter, Date date, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$date, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x015d  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x019e A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x019f  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x01b0  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01c2  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x01eb  */
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            MFSleepDay mFSleepDay;
            List list;
            lc6 lc6;
            il6 il6;
            Boolean bool;
            Pair<Date, Date> pair;
            Object obj3;
            boolean z;
            il6 il62;
            Pair<Date, Date> a;
            Object obj4;
            SleepDetailPresenter sleepDetailPresenter;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il62 = this.p$;
                if (this.this$0.e == null) {
                    sleepDetailPresenter = this.this$0;
                    dl6 a3 = sleepDetailPresenter.b();
                    bj5$d$a bj5_d_a = new bj5$d$a((xe6) null);
                    this.L$0 = il62;
                    this.L$1 = sleepDetailPresenter;
                    this.label = 1;
                    obj4 = gk6.a(a3, bj5_d_a, this);
                    if (obj4 == a2) {
                        return a2;
                    }
                }
                il6 = il62;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SleepDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
                this.this$0.f = this.$date;
                z = bk4.c(this.this$0.e, this.$date);
                Boolean t = bk4.t(this.$date);
                yi5 n = this.this$0.r;
                Date date = this.$date;
                wg6.a((Object) t, "isToday");
                n.a(date, z, t.booleanValue(), !bk4.c(new Date(), this.$date));
                a = bk4.a(this.$date, this.this$0.e);
                wg6.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                lc6 = (lc6) this.this$0.g.a();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new lc6(a.first, a.second));
                if (lc6 == null || !bk4.d((Date) lc6.getFirst(), (Date) a.first) || !bk4.d((Date) lc6.getSecond(), (Date) a.second)) {
                    this.this$0.i = false;
                    this.this$0.j = false;
                    this.this$0.g.a(new lc6(a.first, a.second));
                    return cd6.a;
                }
                dl6 a4 = this.this$0.b();
                bj5$d$c bj5_d_c = new bj5$d$c(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = t;
                this.L$2 = a;
                this.L$3 = lc6;
                this.label = 2;
                obj3 = gk6.a(a4, bj5_d_c, this);
                if (obj3 == a2) {
                    return a2;
                }
                pair = a;
                bool = t;
                MFSleepDay mFSleepDay2 = (MFSleepDay) obj3;
                dl6 a5 = this.this$0.b();
                bj5$d$b bj5_d_b = new bj5$d$b(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair;
                this.L$3 = lc6;
                this.L$4 = mFSleepDay2;
                this.label = 3;
                obj2 = gk6.a(a5, bj5_d_b, this);
                if (obj2 != a2) {
                }
            } else if (i == 1) {
                sleepDetailPresenter = (SleepDetailPresenter) this.L$1;
                il62 = (il6) this.L$0;
                nc6.a(obj);
                obj4 = obj;
            } else if (i == 2) {
                bool = (Boolean) this.L$1;
                boolean z2 = this.Z$0;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                lc6 = (lc6) this.L$3;
                pair = (Pair) this.L$2;
                z = z2;
                obj3 = obj;
                MFSleepDay mFSleepDay22 = (MFSleepDay) obj3;
                dl6 a52 = this.this$0.b();
                bj5$d$b bj5_d_b2 = new bj5$d$b(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair;
                this.L$3 = lc6;
                this.L$4 = mFSleepDay22;
                this.label = 3;
                obj2 = gk6.a(a52, bj5_d_b2, this);
                if (obj2 != a2) {
                    return a2;
                }
                mFSleepDay = mFSleepDay22;
                list = (List) obj2;
                if (!wg6.a((Object) this.this$0.n, (Object) mFSleepDay)) {
                }
                if (!wg6.a((Object) this.this$0.o, (Object) list)) {
                }
                this.this$0.r.a(mFSleepDay);
                rm6 unused = this.this$0.l();
                rm6 unused2 = this.this$0.m();
                return cd6.a;
            } else if (i == 3) {
                mFSleepDay = (MFSleepDay) this.L$4;
                lc6 lc62 = (lc6) this.L$3;
                Pair pair2 = (Pair) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                obj2 = obj;
                list = (List) obj2;
                if (!wg6.a((Object) this.this$0.n, (Object) mFSleepDay)) {
                    this.this$0.n = mFSleepDay;
                }
                if (!wg6.a((Object) this.this$0.o, (Object) list)) {
                    this.this$0.o = list;
                }
                this.this$0.r.a(mFSleepDay);
                if (this.this$0.i && this.this$0.j) {
                    rm6 unused3 = this.this$0.l();
                    rm6 unused4 = this.this$0.m();
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            sleepDetailPresenter.e = (Date) obj4;
            il6 = il62;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("SleepDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
            this.this$0.f = this.$date;
            z = bk4.c(this.this$0.e, this.$date);
            Boolean t2 = bk4.t(this.$date);
            yi5 n2 = this.this$0.r;
            Date date2 = this.$date;
            wg6.a((Object) t2, "isToday");
            n2.a(date2, z, t2.booleanValue(), !bk4.c(new Date(), this.$date));
            a = bk4.a(this.$date, this.this$0.e);
            wg6.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            lc6 = (lc6) this.this$0.g.a();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("SleepDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new lc6(a.first, a.second));
            if (lc6 == null || !bk4.d((Date) lc6.getFirst(), (Date) a.first) || !bk4.d((Date) lc6.getSecond(), (Date) a.second)) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showDetailChart$1", f = "SleepDetailPresenter.kt", l = {327}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(SleepDetailPresenter sleepDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                bj5$e$a bj5_e_a = new bj5$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bj5_e_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailPresenter", "showDetailChart - data=" + list);
            if (!list.isEmpty()) {
                this.this$0.r.p(list);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$1", f = "SleepDetailPresenter.kt", l = {124}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(SleepDetailPresenter sleepDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                bj5$f$a bj5_f_a = new bj5$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bj5_f_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = (ArrayList) obj;
            DeviceHelper.a aVar = DeviceHelper.o;
            String b = this.this$0.h;
            if (b != null) {
                if (aVar.f(b)) {
                    if (arrayList.isEmpty()) {
                        arrayList.add(new ej5.a((ArrayList) null, 0, 0, 0, 15, (qg6) null));
                    }
                    this.this$0.r.d(arrayList);
                } else if (arrayList.isEmpty()) {
                    this.this$0.r.c0();
                } else {
                    this.this$0.r.d(arrayList);
                }
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<yx5<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter a;

        @DexIgnore
        public g(SleepDetailPresenter sleepDetailPresenter) {
            this.a = sleepDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<MFSleepDay>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepDetailPresenter", sb.toString());
            if (a2 == wh4.NETWORK_LOADING || a2 == wh4.SUCCESS) {
                this.a.l = list;
                this.a.i = true;
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new bj5$g$a(this, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<yx5<? extends List<MFSleepSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter a;

        @DexIgnore
        public h(SleepDetailPresenter sleepDetailPresenter) {
            this.a = sleepDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<MFSleepSession>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sessionTransformations -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepDetailPresenter", sb.toString());
            if (a2 == wh4.NETWORK_LOADING || a2 == wh4.SUCCESS) {
                this.a.m = list;
                this.a.j = true;
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new bj5$h$a(this, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter a;

        @DexIgnore
        public i(SleepDetailPresenter sleepDetailPresenter) {
            this.a = sleepDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<MFSleepDay>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.s.getSleepSummaries((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SleepDetailPresenter(yi5 yi5, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        wg6.b(yi5, "mView");
        wg6.b(sleepSummariesRepository, "mSummariesRepository");
        wg6.b(sleepSessionsRepository, "mSessionsRepository");
        this.r = yi5;
        this.s = sleepSummariesRepository;
        this.t = sleepSessionsRepository;
        LiveData<yx5<List<MFSleepDay>>> b2 = sd.b(this.g, new i(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.p = b2;
        LiveData<yx5<List<MFSleepSession>>> b3 = sd.b(this.g, new c(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.q = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "start");
        this.h = PortfolioApp.get.instance().e();
        LiveData<yx5<List<MFSleepDay>>> liveData = this.p;
        yi5 yi5 = this.r;
        if (yi5 != null) {
            liveData.a((SleepDetailFragment) yi5, new g(this));
            this.q.a(this.r, new h(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "stop");
        LiveData<yx5<List<MFSleepDay>>> liveData = this.p;
        yi5 yi5 = this.r;
        if (yi5 != null) {
            liveData.a((SleepDetailFragment) yi5);
            this.q.a(this.r);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.k;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        Date m2 = bk4.m(this.f);
        wg6.a((Object) m2, "DateHelper.getNextDate(mDate)");
        a(m2);
    }

    @DexIgnore
    public void j() {
        Date n2 = bk4.n(this.f);
        wg6.a((Object) n2, "DateHelper.getPrevDate(mDate)");
        a(n2);
    }

    @DexIgnore
    public void k() {
        this.r.a(this);
    }

    @DexIgnore
    public final rm6 l() {
        return ik6.b(e(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final rm6 m() {
        return ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final List<fj5.b> b(List<MFSleepSession> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSleepSessionsToDetailChart - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        int a2 = ik4.d.a(this.n);
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                MFSleepSession mFSleepSession = (MFSleepSession) it.next();
                BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
                ArrayList arrayList2 = new ArrayList();
                List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
                ArrayList arrayList3 = new ArrayList();
                SleepDistribution sleepState = mFSleepSession.getSleepState();
                int realStartTime = mFSleepSession.getRealStartTime();
                int totalMinuteBySleepDistribution = sleepState.getTotalMinuteBySleepDistribution();
                if (sleepStateChange != null) {
                    for (WrapperSleepStateChange wrapperSleepStateChange : sleepStateChange) {
                        BarChart.b bVar = new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 31, (qg6) null);
                        Iterator<T> it2 = it;
                        bVar.a((int) wrapperSleepStateChange.index);
                        bVar.c(realStartTime);
                        bVar.b(totalMinuteBySleepDistribution);
                        int i2 = wrapperSleepStateChange.state;
                        if (i2 == 0) {
                            bVar.a(BarChart.f.LOWEST);
                        } else if (i2 == 1) {
                            bVar.a(BarChart.f.DEFAULT);
                        } else if (i2 == 2) {
                            bVar.a(BarChart.f.HIGHEST);
                        }
                        arrayList3.add(bVar);
                        it = it2;
                    }
                }
                Iterator<T> it3 = it;
                arrayList2.add(arrayList3);
                ArrayList a3 = arrayList2.size() != 0 ? arrayList2 : qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})});
                ArrayList<BarChart.a> a4 = cVar.a();
                BarChart.a aVar = r3;
                BarChart.a aVar2 = new BarChart.a(a2, a3, 0, false, 12, (qg6) null);
                a4.add(aVar);
                cVar.b(a2);
                cVar.a(a2);
                int awake = sleepState.getAwake();
                int light = sleepState.getLight();
                int deep = sleepState.getDeep();
                if (totalMinuteBySleepDistribution > 0) {
                    float f2 = (float) totalMinuteBySleepDistribution;
                    float f3 = ((float) awake) / f2;
                    float f4 = ((float) light) / f2;
                    arrayList.add(new fj5.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession.getTimezoneOffset()));
                }
                it = it3;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, date, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wg6.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    public final ArrayList<ej5.a> a(List<MFSleepSession> list) {
        short s2;
        short s3;
        short s4;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("extractHeartRateDataFromSleepSession - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        ArrayList<ej5.a> arrayList = new ArrayList<>();
        if (list != null) {
            for (MFSleepSession mFSleepSession : list) {
                List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
                try {
                    SleepSessionHeartRate heartRate = mFSleepSession.getHeartRate();
                    if (heartRate != null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("extractHeartRateDataFromSleepSession - sleepStates.size=");
                        sb2.append(sleepStateChange != null ? Integer.valueOf(sleepStateChange.size()) : null);
                        sb2.append(", heartRateData.size=");
                        sb2.append(heartRate.getValues().size());
                        local2.d("SleepDetailPresenter", sb2.toString());
                        ArrayList arrayList2 = new ArrayList();
                        int resolutionInSecond = heartRate.getResolutionInSecond();
                        int i2 = 0;
                        short s5 = Short.MAX_VALUE;
                        short s6 = Short.MIN_VALUE;
                        int i3 = 0;
                        for (T next : heartRate.getValues()) {
                            int i4 = i3 + 1;
                            if (i3 >= 0) {
                                short shortValue = ((Number) next).shortValue();
                                if (s5 > shortValue && shortValue != ((short) i2)) {
                                    s5 = shortValue;
                                }
                                if (s6 < shortValue) {
                                    s6 = shortValue;
                                }
                                iz5 iz5 = new iz5(0, 0, 0, 7, (qg6) null);
                                iz5.b((i3 * resolutionInSecond) / 60);
                                iz5.c(shortValue);
                                if (sleepStateChange != null) {
                                    int size = sleepStateChange.size();
                                    int i5 = 0;
                                    while (true) {
                                        if (i5 >= size) {
                                            break;
                                        }
                                        if (i5 < qd6.a(sleepStateChange)) {
                                            s4 = s5;
                                            s3 = s6;
                                            if (sleepStateChange.get(i5).index <= ((long) iz5.e()) && ((long) iz5.e()) < sleepStateChange.get(i5 + 1).index) {
                                                iz5.a(sleepStateChange.get(i5).state);
                                                break;
                                            }
                                        } else {
                                            s4 = s5;
                                            s3 = s6;
                                            iz5.a(sleepStateChange.get(i5).state);
                                        }
                                        i5++;
                                        s5 = s4;
                                        s6 = s3;
                                    }
                                }
                                s4 = s5;
                                s3 = s6;
                                arrayList2.add(iz5);
                                i3 = i4;
                                s5 = s4;
                                s6 = s3;
                                i2 = 0;
                            } else {
                                qd6.c();
                                throw null;
                            }
                        }
                        if (s5 == Short.MAX_VALUE) {
                            s2 = Short.MIN_VALUE;
                            s5 = 0;
                        } else {
                            s2 = Short.MIN_VALUE;
                        }
                        if (s6 == s2) {
                            s6 = 100;
                        }
                        try {
                            arrayList.add(new ej5.a(arrayList2, mFSleepSession.getRealSleepStateDistInMinute().getTotalMinuteBySleepDistribution(), s5, s6));
                        } catch (Exception e2) {
                            e = e2;
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("SleepDetailPresenter", "extractHeartRateDataFromSleepSession - e=" + e);
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<MFSleepSession> a(Date date, List<MFSleepSession> list) {
        ti6<T> b2;
        ti6<T> a2;
        if (list == null || (b2 = yd6.b(list)) == null || (a2 = aj6.a(b2, new b(date))) == null) {
            return null;
        }
        return aj6.g(a2);
    }

    @DexIgnore
    public final MFSleepDay b(Date date, List<MFSleepDay> list) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailPresenter", "findSleepSummary - date=" + date);
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (bk4.d(instance.getTime(), ((MFSleepDay) next).getDate())) {
                t2 = next;
                break;
            }
        }
        return (MFSleepDay) t2;
    }
}
