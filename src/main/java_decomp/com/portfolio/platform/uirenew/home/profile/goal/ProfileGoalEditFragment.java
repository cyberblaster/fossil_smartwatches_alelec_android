package com.portfolio.platform.uirenew.home.profile.goal;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.jd4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.mj6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rk5;
import com.fossil.sh4;
import com.fossil.sk5;
import com.fossil.tk4;
import com.fossil.tk5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileGoalEditFragment extends BaseFragment implements sk5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public rk5 f;
    @DexIgnore
    public ax5<jd4> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public sh4 i; // = sh4.TOTAL_STEPS;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ProfileGoalEditFragment.o;
        }

        @DexIgnore
        public final ProfileGoalEditFragment b() {
            return new ProfileGoalEditFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ jd4 b;

        @DexIgnore
        public b(ProfileGoalEditFragment profileGoalEditFragment, jd4 jd4) {
            this.a = profileGoalEditFragment;
            this.b = jd4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r7v10, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                boolean z = true;
                int i = 0;
                if (!(editable.length() == 0)) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.b.w.setText(String.valueOf(parseInt));
                    }
                    int i2 = 16;
                    if (parseInt > 16) {
                        this.b.w.setText(String.valueOf(16));
                        lx5 lx5 = lx5.c;
                        FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                        wg6.a((Object) childFragmentManager, "childFragmentManager");
                        lx5.a(childFragmentManager, this.a.i, 16);
                    } else {
                        i2 = parseInt;
                    }
                    ProfileGoalEditFragment profileGoalEditFragment = this.a;
                    FlexibleEditText flexibleEditText = this.b.x;
                    wg6.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        wg6.a((Object) text, "binding.fetSleepMinuteValue.text!!");
                        if (text.length() != 0) {
                            z = false;
                        }
                        if (!z) {
                            FlexibleEditText flexibleEditText2 = this.b.x;
                            wg6.a((Object) flexibleEditText2, "binding.fetSleepMinuteValue");
                            i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                        }
                        this.a.j1().a(profileGoalEditFragment.e(i, i2), this.a.i);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                this.b.w.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ jd4 b;

        @DexIgnore
        public c(ProfileGoalEditFragment profileGoalEditFragment, jd4 jd4) {
            this.a = profileGoalEditFragment;
            this.b = jd4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r6v13, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                boolean z = true;
                int i = 0;
                if (!(editable.length() == 0)) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.b.x.setText(String.valueOf(parseInt));
                    }
                    if (this.a.i == sh4.TOTAL_SLEEP) {
                        int i2 = 59;
                        if (parseInt > 59) {
                            this.b.x.setText(String.valueOf(59));
                        } else {
                            i2 = parseInt;
                        }
                        ProfileGoalEditFragment profileGoalEditFragment = this.a;
                        FlexibleEditText flexibleEditText = this.b.x;
                        wg6.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                        Editable text = flexibleEditText.getText();
                        if (text != null) {
                            wg6.a((Object) text, "binding.fetSleepMinuteValue.text!!");
                            if (text.length() != 0) {
                                z = false;
                            }
                            if (!z) {
                                FlexibleEditText flexibleEditText2 = this.b.w;
                                wg6.a((Object) flexibleEditText2, "binding.fetSleepHourValue");
                                i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                            }
                            parseInt = profileGoalEditFragment.e(i2, i);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    this.a.j1().a(parseInt, this.a.i);
                    return;
                }
                this.b.x.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;

        @DexIgnore
        public d(ProfileGoalEditFragment profileGoalEditFragment) {
            this.a = profileGoalEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;

        @DexIgnore
        public e(ProfileGoalEditFragment profileGoalEditFragment) {
            this.a = profileGoalEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileGoalEditFragment profileGoalEditFragment = this.a;
            if (view != null) {
                profileGoalEditFragment.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;

        @DexIgnore
        public f(ProfileGoalEditFragment profileGoalEditFragment) {
            this.a = profileGoalEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileGoalEditFragment profileGoalEditFragment = this.a;
            if (view != null) {
                profileGoalEditFragment.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;

        @DexIgnore
        public g(ProfileGoalEditFragment profileGoalEditFragment) {
            this.a = profileGoalEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileGoalEditFragment profileGoalEditFragment = this.a;
            if (view != null) {
                profileGoalEditFragment.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;

        @DexIgnore
        public h(ProfileGoalEditFragment profileGoalEditFragment) {
            this.a = profileGoalEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileGoalEditFragment profileGoalEditFragment = this.a;
            if (view != null) {
                profileGoalEditFragment.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ jd4 b;

        @DexIgnore
        public i(ProfileGoalEditFragment profileGoalEditFragment, jd4 jd4) {
            this.a = profileGoalEditFragment;
            this.b = jd4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onFocusChange(View view, boolean z) {
            if (this.a.i == sh4.TOTAL_SLEEP) {
                ProfileGoalEditFragment profileGoalEditFragment = this.a;
                Object r0 = this.b.x;
                wg6.a((Object) r0, "binding.fetSleepMinuteValue");
                profileGoalEditFragment.e(z, r0.isFocused());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ jd4 b;

        @DexIgnore
        public j(ProfileGoalEditFragment profileGoalEditFragment, jd4 jd4) {
            this.a = profileGoalEditFragment;
            this.b = jd4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onFocusChange(View view, boolean z) {
            ProfileGoalEditFragment profileGoalEditFragment = this.a;
            Object r0 = this.b.w;
            wg6.a((Object) r0, "binding.fetSleepHourValue");
            profileGoalEditFragment.e(r0.isFocused(), z);
            if (z) {
                FlexibleEditText flexibleEditText = this.b.w;
                wg6.a((Object) flexibleEditText, "binding.fetSleepHourValue");
                if (Integer.parseInt(String.valueOf(flexibleEditText.getText())) == 16) {
                    this.a.j1().j();
                    lx5 lx5 = lx5.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    wg6.a((Object) childFragmentManager, "childFragmentManager");
                    lx5.a(childFragmentManager, sh4.TOTAL_SLEEP, 16);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ jd4 b;

        @DexIgnore
        public k(ProfileGoalEditFragment profileGoalEditFragment, jd4 jd4) {
            this.a = profileGoalEditFragment;
            this.b = jd4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                if (!(editable.length() == 0)) {
                    int a2 = tk4.a(editable.toString());
                    if (a2 <= 9 && editable.length() > 1) {
                        this.b.v.setText(String.valueOf(a2));
                    }
                    this.a.r(a2);
                    return;
                }
                this.b.v.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r3v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r2v10, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence != null) {
                if (!(charSequence.length() == 0)) {
                    this.b.v.removeTextChangedListener(this);
                    String obj = charSequence.toString();
                    if (yj6.a((CharSequence) obj, (CharSequence) ",", false, 2, (Object) null)) {
                        obj = new mj6(",").replace((CharSequence) obj, "");
                    }
                    this.b.v.setText(tk4.c(Integer.parseInt(obj)));
                    AppCompatEditText appCompatEditText = this.b.v;
                    wg6.a((Object) appCompatEditText, "binding.fetGoalsValue");
                    Editable text = appCompatEditText.getText();
                    if (text != null) {
                        appCompatEditText.setSelection(text.length());
                        this.b.v.addTextChangedListener(this);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                this.b.v.setText(String.valueOf(0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ jd4 a;

        @DexIgnore
        public l(jd4 jd4) {
            this.a = jd4;
        }

        @DexIgnore
        public final void run() {
            this.a.H.fullScroll(130);
        }
    }

    /*
    static {
        String simpleName = ProfileGoalEditFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "ProfileGoalEditFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public void E0() {
        if (isActive()) {
            a();
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.S(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r9v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v8, types: [android.view.View, com.portfolio.platform.view.FlexibleEditText] */
    /* JADX WARNING: type inference failed for: r9v11, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v12, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v13, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v14, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v22, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r9v23, types: [android.view.View, com.portfolio.platform.view.FlexibleEditText] */
    public final void R(boolean z) {
        jd4 a2;
        FLogger.INSTANCE.getLocal().d(o, "showSleepGoalEdit");
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Object instance = PortfolioApp.get.instance();
            Object systemService = instance.getSystemService("input_method");
            if (systemService != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) systemService;
                if (z) {
                    LinearLayout linearLayout = a2.F;
                    wg6.a((Object) linearLayout, "llSleepGoalValue");
                    linearLayout.setVisibility(0);
                    LinearLayout linearLayout2 = a2.E;
                    wg6.a((Object) linearLayout2, "llGoalsValue");
                    linearLayout2.setVisibility(8);
                    a2.w.setTextColor(w6.a(instance, 2131099813));
                    a2.x.setTextColor(w6.a(instance, 2131099760));
                    Object r9 = a2.w;
                    wg6.a((Object) r9, "fetSleepHourValue");
                    r9.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    Object r92 = a2.x;
                    wg6.a((Object) r92, "fetSleepMinuteValue");
                    r92.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    Object r93 = a2.B;
                    wg6.a((Object) r93, "ftvSleepHourUnit");
                    r93.setText(getString(2131886870));
                    Object r94 = a2.C;
                    wg6.a((Object) r94, "ftvSleepMinuteUnit");
                    r94.setText(getString(2131886871));
                    int mValue = (this.h ? a2.s : a2.r).getMValue() / 60;
                    CustomEditGoalView customEditGoalView = this.h ? a2.s : a2.r;
                    a2.w.setText(String.valueOf(mValue));
                    a2.x.setText(String.valueOf(customEditGoalView.getMValue() % 60));
                    inputMethodManager.showSoftInput(a2.w, 1);
                } else {
                    LinearLayout linearLayout3 = a2.F;
                    wg6.a((Object) linearLayout3, "llSleepGoalValue");
                    linearLayout3.setVisibility(8);
                    LinearLayout linearLayout4 = a2.E;
                    wg6.a((Object) linearLayout4, "llGoalsValue");
                    linearLayout4.setVisibility(0);
                    AppCompatEditText appCompatEditText = a2.v;
                    wg6.a((Object) appCompatEditText, "fetGoalsValue");
                    Editable text = appCompatEditText.getText();
                    if (text != null) {
                        appCompatEditText.setSelection(text.length());
                        a2.v.requestFocus();
                        inputMethodManager.showSoftInput(a2.v, 1);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                a2.H.post(new l(a2));
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
        }
    }

    @DexIgnore
    public void V0() {
        a();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v8, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v17, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v18, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v16, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v26, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v27, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v10, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v37, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v41, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v42, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r12v43, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r12v44, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v17, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void b(sh4 sh4) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && sh4 != null) {
            int i2 = tk5.b[sh4.ordinal()];
            if (i2 == 1) {
                Object r12 = a2.z;
                wg6.a((Object) r12, "ftvGoalTitle");
                r12.setText(getString(2131886417));
                Object r122 = a2.y;
                wg6.a((Object) r122, "ftvDesc");
                r122.setText(getString(2131886866));
                String b2 = ThemeManager.l.a().b("dianaActiveCaloriesTab");
                if (b2 != null) {
                    a2.v.setTextColor(Color.parseColor(b2));
                }
                Object r123 = a2.v;
                wg6.a((Object) r123, "fetGoalsValue");
                r123.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                a2.v.setText(String.valueOf((this.h ? a2.r : a2.u).getValue()));
                Object r124 = a2.A;
                wg6.a((Object) r124, "ftvGoalsUnit");
                r124.setVisibility(8);
                R(false);
            } else if (i2 == 2) {
                Object r125 = a2.z;
                wg6.a((Object) r125, "ftvGoalTitle");
                r125.setText(getString(2131886425));
                Object r126 = a2.y;
                wg6.a((Object) r126, "ftvDesc");
                r126.setText(getString(2131886868));
                String b3 = ThemeManager.l.a().b("dianaActiveMinutesTab");
                if (b3 != null) {
                    a2.v.setTextColor(Color.parseColor(b3));
                }
                Object r127 = a2.v;
                wg6.a((Object) r127, "fetGoalsValue");
                r127.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(5)});
                a2.v.setText(String.valueOf(a2.u.getValue()));
                Object r128 = a2.A;
                wg6.a((Object) r128, "ftvGoalsUnit");
                r128.setVisibility(8);
                R(false);
            } else if (i2 == 3) {
                Object r129 = a2.z;
                wg6.a((Object) r129, "ftvGoalTitle");
                r129.setText(getString(2131886878));
                Object r1210 = a2.y;
                wg6.a((Object) r1210, "ftvDesc");
                r1210.setText(getString(2131886879));
                String b4 = ThemeManager.l.a().b("hybridStepsTab");
                if (b4 != null) {
                    a2.v.setTextColor(Color.parseColor(b4));
                }
                Object r1211 = a2.v;
                wg6.a((Object) r1211, "fetGoalsValue");
                r1211.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                a2.v.setText(String.valueOf(a2.t.getValue()));
                Object r1212 = a2.A;
                wg6.a((Object) r1212, "ftvGoalsUnit");
                r1212.setVisibility(8);
                R(false);
            } else if (i2 == 4) {
                Object r1213 = a2.z;
                wg6.a((Object) r1213, "ftvGoalTitle");
                r1213.setText(getString(2131886534));
                Object r1214 = a2.y;
                wg6.a((Object) r1214, "ftvDesc");
                r1214.setText(getString(2131886872));
                String b5 = ThemeManager.l.a().b("dianaSleepTab");
                if (b5 != null) {
                    a2.v.setTextColor(Color.parseColor(b5));
                }
                R(true);
            } else if (i2 == 5) {
                Object r1215 = a2.z;
                wg6.a((Object) r1215, "ftvGoalTitle");
                r1215.setText(getString(2131886508));
                Object r1216 = a2.y;
                wg6.a((Object) r1216, "ftvDesc");
                r1216.setText(getString(2131886884));
                String b6 = ThemeManager.l.a().b("hybridGoalTrackingTab");
                if (b6 != null) {
                    a2.v.setTextColor(Color.parseColor(b6));
                }
                Object r1217 = a2.v;
                wg6.a((Object) r1217, "fetGoalsValue");
                r1217.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                a2.v.setText(String.valueOf(a2.s.getValue()));
                Object r1218 = a2.A;
                wg6.a((Object) r1218, "ftvGoalsUnit");
                r1218.setVisibility(0);
                Object r1219 = a2.A;
                wg6.a((Object) r1219, "ftvGoalsUnit");
                r1219.setText(getString(2131886883));
                R(false);
            }
        }
    }

    @DexIgnore
    public void d(int i2, String str) {
        wg6.b(str, "message");
        a();
        lx5 lx5 = lx5.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        lx5.a(i2, str, childFragmentManager);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void e(boolean z, boolean z2) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            String b2 = ThemeManager.l.a().b("dianaSleepTab");
            String b3 = ThemeManager.l.a().b("nonBrandCoolGray");
            int parseColor = Color.parseColor(b2);
            int parseColor2 = Color.parseColor(b3);
            if (z) {
                a2.w.setTextColor(parseColor);
                a2.x.setTextColor(parseColor2);
            } else if (z2) {
                a2.w.setTextColor(parseColor2);
                a2.x.setTextColor(parseColor);
            } else {
                a2.w.setTextColor(parseColor);
                a2.x.setTextColor(parseColor);
            }
        }
    }

    @DexIgnore
    public boolean i1() {
        if (getActivity() == null) {
            return true;
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "activity!!");
            if (activity.isFinishing()) {
                return true;
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                wg6.a((Object) activity2, "activity!!");
                if (activity2.isDestroyed()) {
                    return true;
                }
                rk5 rk5 = this.f;
                if (rk5 != null) {
                    rk5.h();
                    return true;
                }
                wg6.d("mPresenter");
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void j(int i2) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            a2.w.setText(String.valueOf(i2 / 60));
            a2.x.setText(String.valueOf(i2 % 60));
            rk5 rk5 = this.f;
            if (rk5 != null) {
                rk5.a(i2, sh4.TOTAL_SLEEP);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final rk5 j1() {
        rk5 rk5 = this.f;
        if (rk5 != null) {
            return rk5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void k(int i2) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && !this.h) {
            a2.s.setValue(i2);
        }
    }

    @DexIgnore
    public void m(int i2) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (this.h) {
                a2.s.setValue(i2);
            } else {
                a2.r.setValue(i2);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.CustomEditGoalView, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.CustomEditGoalView, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.view.CustomEditGoalView, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.CustomEditGoalView, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r4v7, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v8, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v10, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v11, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v12, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String b2;
        wg6.b(layoutInflater, "inflater");
        boolean z = false;
        jd4 a2 = kb.a(layoutInflater, 2131558599, viewGroup, false, e1());
        rk5 rk5 = this.f;
        if (rk5 != null) {
            if (rk5 != null) {
                if (rk5.i() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    z = true;
                }
                this.h = z;
                if (this.h) {
                    a2.t.setType(sh4.TOTAL_STEPS);
                    a2.u.setType(sh4.ACTIVE_TIME);
                    a2.r.setType(sh4.CALORIES);
                    a2.s.setType(sh4.TOTAL_SLEEP);
                } else {
                    a2.t.setType(sh4.TOTAL_STEPS);
                    a2.u.setType(sh4.CALORIES);
                    a2.r.setType(sh4.TOTAL_SLEEP);
                    a2.s.setType(sh4.GOAL_TRACKING);
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        ConstraintLayout constraintLayout = a2.q;
        if (!(constraintLayout == null || (b2 = ThemeManager.l.a().b("nonBrandSurface")) == null)) {
            constraintLayout.setBackgroundColor(Color.parseColor(b2));
        }
        a2.t.setOnClickListener(new e(this));
        a2.u.setOnClickListener(new f(this));
        a2.r.setOnClickListener(new g(this));
        a2.s.setOnClickListener(new h(this));
        Object r4 = a2.w;
        wg6.a((Object) r4, "binding.fetSleepHourValue");
        r4.setOnFocusChangeListener(new i(this, a2));
        Object r42 = a2.x;
        wg6.a((Object) r42, "binding.fetSleepMinuteValue");
        r42.setOnFocusChangeListener(new j(this, a2));
        a2.v.addTextChangedListener(new k(this, a2));
        a2.w.addTextChangedListener(new b(this, a2));
        a2.x.addTextChangedListener(new c(this, a2));
        a2.D.setOnClickListener(new d(this));
        this.g = new ax5<>(this, a2);
        W("set_goal_view");
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        FLogger.INSTANCE.getLocal().d(o, "onResume");
        ProfileGoalEditFragment.super.onResume();
        rk5 rk5 = this.f;
        if (rk5 != null) {
            rk5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        ProfileGoalEditFragment.super.onStop();
        rk5 rk5 = this.f;
        if (rk5 != null) {
            rk5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    /* JADX WARNING: type inference failed for: r9v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r9 <= 999) goto L_0x0040;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006e  */
    public final void r(int i2) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            int i3 = tk5.a[this.i.ordinal()];
            int i4 = 999;
            boolean z = true;
            if (i3 != 1) {
                if (i3 != 2) {
                    if (i3 != 3) {
                        if (i3 == 4) {
                        }
                    } else if (i2 > 480) {
                        i4 = 480;
                        if (!z) {
                            rk5 rk5 = this.f;
                            if (rk5 != null) {
                                rk5.a(i4, this.i);
                                a2.v.setText(tk4.c(i4));
                                lx5 lx5 = lx5.c;
                                FragmentManager childFragmentManager = getChildFragmentManager();
                                wg6.a((Object) childFragmentManager, "childFragmentManager");
                                lx5.a(childFragmentManager, this.i, i4);
                                return;
                            }
                            wg6.d("mPresenter");
                            throw null;
                        }
                        rk5 rk52 = this.f;
                        if (rk52 != null) {
                            rk52.a(i2, this.i);
                            return;
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                } else if (i2 > 4800) {
                    i4 = 4800;
                    if (!z) {
                    }
                }
            } else if (i2 > 50000) {
                i4 = 50000;
                if (!z) {
                }
            }
            i4 = 0;
            z = false;
            if (!z) {
            }
        }
    }

    @DexIgnore
    public void z0() {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            LinearLayout linearLayout = a2.E;
            wg6.a((Object) linearLayout, "llGoalsValue");
            linearLayout.setVisibility(4);
            LinearLayout linearLayout2 = a2.F;
            wg6.a((Object) linearLayout2, "llSleepGoalValue");
            linearLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public final void a(sh4 sh4) {
        jd4 a2;
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (sh4 != null) {
                this.i = sh4;
            }
            CustomEditGoalView customEditGoalView = a2.t;
            wg6.a((Object) customEditGoalView, "cegvTopLeft");
            boolean z = true;
            customEditGoalView.setSelected(a2.t.getMGoalType() == sh4);
            CustomEditGoalView customEditGoalView2 = a2.u;
            wg6.a((Object) customEditGoalView2, "cegvTopRight");
            customEditGoalView2.setSelected(a2.u.getMGoalType() == sh4);
            CustomEditGoalView customEditGoalView3 = a2.r;
            wg6.a((Object) customEditGoalView3, "cegvBottomLeft");
            customEditGoalView3.setSelected(a2.r.getMGoalType() == sh4);
            CustomEditGoalView customEditGoalView4 = a2.s;
            wg6.a((Object) customEditGoalView4, "cegvBottomRight");
            if (a2.s.getMGoalType() != sh4) {
                z = false;
            }
            customEditGoalView4.setSelected(z);
            b(sh4);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final int e(int i2, int i3) {
        jd4 a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = o;
        local.d(str, "updateSleepGoal minute: " + i2 + " hour: " + i3);
        ax5<jd4> ax5 = this.g;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return 0;
        }
        int i4 = i2 + (i3 * 60);
        if (i4 <= 960) {
            return i4;
        }
        a2.x.setText(ShareWebViewClient.RESP_SUCC_CODE);
        a2.w.setText(String.valueOf(16));
        return 960;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        wg6.b(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            if (hashCode != -1375614559) {
                if (hashCode == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
                    BaseActivity activity2 = getActivity();
                    if (activity2 != null) {
                        activity2.m();
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            } else if (!str.equals("UNSAVED_CHANGE")) {
            } else {
                if (i2 == 2131363190) {
                    b();
                    rk5 rk5 = this.f;
                    if (rk5 != null) {
                        rk5.k();
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else if (i2 == 2131363105 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            }
        }
    }

    @DexIgnore
    public void a(rk5 rk5) {
        wg6.b(rk5, "presenter");
        this.f = rk5;
    }

    @DexIgnore
    public void a(ActivitySettings activitySettings) {
        jd4 a2;
        wg6.b(activitySettings, "currentSettings");
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            a2.t.setValue(activitySettings.getCurrentStepGoal());
            if (this.h) {
                a2.u.setValue(activitySettings.getCurrentActiveTimeGoal());
                a2.r.setValue(activitySettings.getCurrentCaloriesGoal());
            } else {
                a2.u.setValue(activitySettings.getCurrentCaloriesGoal());
            }
            a(this.i);
        }
    }

    @DexIgnore
    public void a(int i2, sh4 sh4) {
        jd4 a2;
        wg6.b(sh4, "type");
        ax5<jd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            int i3 = tk5.c[sh4.ordinal()];
            if (i3 == 1) {
                a2.t.setValue(i2);
            } else if (i3 != 2) {
                if (i3 == 3) {
                    a2.u.setValue(i2);
                } else if (i3 != 4) {
                    if (i3 == 5) {
                        a2.s.setValue(i2);
                    }
                } else if (this.h) {
                    a2.s.setValue(i2);
                } else {
                    a2.r.setValue(i2);
                }
            } else if (this.h) {
                a2.r.setValue(i2);
            } else {
                a2.u.setValue(i2);
            }
        }
    }
}
