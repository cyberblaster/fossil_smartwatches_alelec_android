package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.fossil.ax5;
import com.fossil.h94;
import com.fossil.jb;
import com.fossil.jh6;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.o34;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y05;
import com.fossil.z05;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DoNotDisturbScheduledTimeFragment extends BaseBottomSheetDialogFragment implements z05 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public /* final */ jb j; // = new o34(this);
    @DexIgnore
    public ax5<h94> o;
    @DexIgnore
    public y05 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DoNotDisturbScheduledTimeFragment.r;
        }

        @DexIgnore
        public final DoNotDisturbScheduledTimeFragment b() {
            return new DoNotDisturbScheduledTimeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ h94 b;

        @DexIgnore
        public b(DoNotDisturbScheduledTimeFragment doNotDisturbScheduledTimeFragment, h94 h94) {
            this.a = doNotDisturbScheduledTimeFragment;
            this.b = h94;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            y05 a2 = DoNotDisturbScheduledTimeFragment.a(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.u;
            wg6.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.t;
            wg6.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ h94 b;

        @DexIgnore
        public c(DoNotDisturbScheduledTimeFragment doNotDisturbScheduledTimeFragment, h94 h94) {
            this.a = doNotDisturbScheduledTimeFragment;
            this.b = h94;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            y05 a2 = DoNotDisturbScheduledTimeFragment.a(this.a);
            NumberPicker numberPicker2 = this.b.s;
            wg6.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.t;
            wg6.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ h94 b;

        @DexIgnore
        public d(DoNotDisturbScheduledTimeFragment doNotDisturbScheduledTimeFragment, h94 h94) {
            this.a = doNotDisturbScheduledTimeFragment;
            this.b = h94;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            y05 a2 = DoNotDisturbScheduledTimeFragment.a(this.a);
            NumberPicker numberPicker2 = this.b.s;
            wg6.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.u;
            wg6.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimeFragment a;

        @DexIgnore
        public e(DoNotDisturbScheduledTimeFragment doNotDisturbScheduledTimeFragment) {
            this.a = doNotDisturbScheduledTimeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            DoNotDisturbScheduledTimeFragment.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ h94 a;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 b;

        @DexIgnore
        public f(h94 h94, jh6 jh6) {
            this.a = h94;
            this.b = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.a.v;
            wg6.a((Object) constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                CoordinatorLayout.e layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior d = layoutParams.d();
                    if (d != null) {
                        d.e(3);
                        h94 h94 = this.a;
                        wg6.a((Object) h94, "it");
                        View d2 = h94.d();
                        wg6.a((Object) d2, "it.root");
                        d2.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.b.element);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                throw new rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new rc6("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = DoNotDisturbScheduledTimeFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "DoNotDisturbScheduledTim\u2026nt::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ y05 a(DoNotDisturbScheduledTimeFragment doNotDisturbScheduledTimeFragment) {
        y05 y05 = doNotDisturbScheduledTimeFragment.p;
        if (y05 != null) {
            return y05;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i) {
        int i2;
        int i3 = i / 60;
        int i4 = i % 60;
        if (i3 >= 12) {
            i2 = 1;
            i3 -= 12;
        } else {
            i2 = 0;
        }
        if (i3 == 0) {
            i3 = 12;
        }
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.s;
                wg6.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.u;
                wg6.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i4);
                NumberPicker numberPicker3 = a2.t;
                wg6.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        h94 a2 = kb.a(layoutInflater, 2131558544, viewGroup, false, this.j);
        a2.q.setOnClickListener(new e(this));
        wg6.a((Object) a2, "binding");
        a(a2);
        this.o = new ax5<>(this, a2);
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onPause() {
        y05 y05 = this.p;
        if (y05 != null) {
            y05.g();
            DoNotDisturbScheduledTimeFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        DoNotDisturbScheduledTimeFragment.super.onResume();
        y05 y05 = this.p;
        if (y05 != null) {
            y05.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        DoNotDisturbScheduledTimeFragment.super.onViewCreated(view, bundle);
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null) {
                jh6 jh6 = new jh6();
                jh6.element = null;
                jh6.element = new f(a2, jh6);
                wg6.a((Object) a2, "it");
                View d2 = a2.d();
                wg6.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) jh6.element);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void r(int i) {
        y05 y05 = this.p;
        if (y05 != null) {
            y05.a(i);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(y05 y05) {
        wg6.b(y05, "presenter");
        this.p = y05;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(h94 h94) {
        String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            h94.v.setBackgroundColor(Color.parseColor(b2));
        }
        NumberPicker numberPicker = h94.s;
        wg6.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = h94.s;
        wg6.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        h94.s.setOnValueChangedListener(new b(this, h94));
        NumberPicker numberPicker3 = h94.u;
        wg6.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = h94.u;
        wg6.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        h94.u.setOnValueChangedListener(new c(this, h94));
        String[] strArr = {jm4.a((Context) PortfolioApp.get.instance(), 2131886102), jm4.a((Context) PortfolioApp.get.instance(), 2131886104)};
        NumberPicker numberPicker5 = h94.t;
        wg6.a((Object) numberPicker5, "binding.numberPickerThree");
        numberPicker5.setMinValue(0);
        NumberPicker numberPicker6 = h94.t;
        wg6.a((Object) numberPicker6, "binding.numberPickerThree");
        numberPicker6.setMaxValue(1);
        h94.t.setDisplayedValues(strArr);
        h94.t.setOnValueChangedListener(new d(this, h94));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(String str) {
        Object r0;
        wg6.b(str, Explore.COLUMN_TITLE);
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null && (r0 = a2.r) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
