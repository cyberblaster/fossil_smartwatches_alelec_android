package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Intent;
import android.view.View;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.WatchFaceHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.j45;
import com.fossil.jm4;
import com.fossil.k45;
import com.fossil.l45$b$a;
import com.fossil.l45$c$a;
import com.fossil.l45$d$a;
import com.fossil.l45$e$a;
import com.fossil.l45$g$a;
import com.fossil.l45$g$b;
import com.fossil.l45$h$a;
import com.fossil.l45$h$b;
import com.fossil.l45$h$c;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.m35;
import com.fossil.nc6;
import com.fossil.o35;
import com.fossil.om4;
import com.fossil.qc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.u8;
import com.fossil.u85;
import com.fossil.uh4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.xp6;
import com.fossil.zl6;
import com.fossil.zp6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDianaCustomizePresenter extends j45 {
    @DexIgnore
    public /* final */ CustomizeRealDataRepository A;
    @DexIgnore
    public /* final */ UserRepository B;
    @DexIgnore
    public /* final */ WatchFaceRepository C;
    @DexIgnore
    public /* final */ an4 D;
    @DexIgnore
    public LiveData<List<DianaPreset>> e; // = new MutableLiveData();
    @DexIgnore
    public LiveData<List<CustomizeRealData>> f; // = this.A.getAllRealDataAsLiveData();
    @DexIgnore
    public /* final */ ArrayList<Complication> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WatchApp> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<DianaPreset> i; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public DianaPreset j;
    @DexIgnore
    public MutableLiveData<String> k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> m; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public int n; // = 1;
    @DexIgnore
    public MFUser o;
    @DexIgnore
    public xp6 p; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public DianaComplicationRingStyle q;
    @DexIgnore
    public lc6<Boolean, ? extends List<DianaPreset>> r; // = qc6.a(false, null);
    @DexIgnore
    public boolean s;
    @DexIgnore
    public /* final */ k45 t;
    @DexIgnore
    public /* final */ WatchAppRepository u;
    @DexIgnore
    public /* final */ ComplicationRepository v;
    @DexIgnore
    public /* final */ RingStyleRepository w;
    @DexIgnore
    public /* final */ DianaPresetRepository x;
    @DexIgnore
    public /* final */ SetDianaPresetToWatchUseCase y;
    @DexIgnore
    public /* final */ om4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {336}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HomeDianaCustomizePresenter homeDianaCustomizePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                Iterator it = this.this$0.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (hf6.a(((DianaPreset) obj2).isActive()).booleanValue()) {
                        break;
                    }
                }
                DianaPreset dianaPreset = (DianaPreset) obj2;
                if (dianaPreset != null) {
                    DianaPreset cloneFrom = DianaPreset.Companion.cloneFrom(dianaPreset);
                    dl6 d = this.this$0.c();
                    l45$b$a l45_b_a = new l45$b$a(cloneFrom, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = dianaPreset;
                    this.L$2 = dianaPreset;
                    this.L$3 = cloneFrom;
                    this.label = 1;
                    if (gk6.a(d, l45_b_a, this) == a) {
                        return a;
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                DianaPreset dianaPreset2 = (DianaPreset) this.L$3;
                DianaPreset dianaPreset3 = (DianaPreset) this.L$2;
                DianaPreset dianaPreset4 = (DianaPreset) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.t.v();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<u85.d, u85.b> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset a;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter b;

        @DexIgnore
        public c(DianaPreset dianaPreset, HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str) {
            this.a = dianaPreset;
            this.b = homeDianaCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.b.e(), (af6) null, (ll6) null, new l45$c$a(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(SetDianaPresetToWatchUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.b.t.n();
            int b2 = bVar.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.a());
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                k45 t = this.b.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    t.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.b.t.j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DianaPreset dianaPreset, xe6 xe6, HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str) {
            super(2, xe6);
            this.$preset = dianaPreset;
            this.this$0 = homeDianaCustomizePresenter;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.$preset, xe6, this.this$0, this.$nextActivePresetId$inlined);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 d = this.this$0.c();
                l45$d$a l45_d_a = new l45$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(d, l45_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.t.d(this.this$0.j() - 1);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$renameCurrentPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {228}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$presetId, this.$name, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                Iterator it = this.this$0.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (hf6.a(wg6.a((Object) ((DianaPreset) obj2).getId(), (Object) this.$presetId)).booleanValue()) {
                        break;
                    }
                }
                DianaPreset dianaPreset = (DianaPreset) obj2;
                DianaPreset clone = dianaPreset != null ? dianaPreset.clone() : null;
                if (clone != null) {
                    clone.setName(this.$name);
                    dl6 d = this.this$0.c();
                    l45$e$a l45_e_a = new l45$e$a(clone, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = clone;
                    this.L$2 = clone;
                    this.label = 1;
                    if (gk6.a(d, l45_e_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                DianaPreset dianaPreset2 = (DianaPreset) this.L$2;
                DianaPreset dianaPreset3 = (DianaPreset) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements m24.e<u85.d, u85.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter a;

        @DexIgnore
        public f(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            this.a = homeDianaCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.t.n();
            this.a.t.f(0);
        }

        @DexIgnore
        public void a(SetDianaPresetToWatchUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.t.n();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.a());
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                k45 t = this.a.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    t.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.t.j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1", f = "HomeDianaCustomizePresenter.kt", l = {348, 354}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(HomeDianaCustomizePresenter homeDianaCustomizePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            List list;
            List list2;
            HomeDianaCustomizePresenter homeDianaCustomizePresenter;
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 c = this.this$0.b();
                l45$g$b l45_g_b = new l45$g$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(c, l45_g_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                homeDianaCustomizePresenter = (HomeDianaCustomizePresenter) this.L$2;
                list2 = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                homeDianaCustomizePresenter.q = (DianaComplicationRingStyle) obj;
                list = list2;
                this.this$0.t.a(list, this.this$0.q);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list = (List) obj;
            if (this.this$0.q == null) {
                HomeDianaCustomizePresenter homeDianaCustomizePresenter2 = this.this$0;
                dl6 b = zl6.b();
                l45$g$a l45_g_a = new l45$g$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = list;
                this.L$2 = homeDianaCustomizePresenter2;
                this.label = 2;
                Object a2 = gk6.a(b, l45_g_a, this);
                if (a2 == a) {
                    return a;
                }
                homeDianaCustomizePresenter = homeDianaCustomizePresenter2;
                Object obj2 = a2;
                list2 = list;
                obj = obj2;
                homeDianaCustomizePresenter.q = (DianaComplicationRingStyle) obj;
                list = list2;
            }
            this.this$0.t.a(list, this.this$0.q);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1", f = "HomeDianaCustomizePresenter.kt", l = {95}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(HomeDianaCustomizePresenter homeDianaCustomizePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.g.isEmpty() || this.this$0.h.isEmpty()) {
                    FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps");
                    dl6 c = this.this$0.b();
                    l45$h$a l45_h_a = new l45$h$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(c, l45_h_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LiveData h = this.this$0.f;
            k45 t = this.this$0.t;
            if (t != null) {
                h.a((HomeDianaCustomizeFragment) t, new l45$h$b(this));
                this.this$0.k.a(this.this$0.t, new l45$h$c(this));
                this.this$0.y.f();
                BleCommandResultManager.d.a(CommunicateMode.SET_PRESET_APPS_DATA);
                return cd6.a;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HomeDianaCustomizePresenter(k45 k45, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, RingStyleRepository ringStyleRepository, DianaPresetRepository dianaPresetRepository, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, om4 om4, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, an4 an4, PortfolioApp portfolioApp) {
        wg6.b(k45, "mView");
        wg6.b(watchAppRepository, "mWatchAppRepository");
        wg6.b(complicationRepository, "mComplicationRepository");
        wg6.b(ringStyleRepository, "mRingStyleRepository");
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(setDianaPresetToWatchUseCase, "mSetDianaPresetToWatchUseCase");
        wg6.b(om4, "mCustomizeRealDataManager");
        wg6.b(customizeRealDataRepository, "mCustomizeRealDataRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(watchFaceRepository, "mWatchFaceRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(portfolioApp, "mApp");
        this.t = k45;
        this.u = watchAppRepository;
        this.v = complicationRepository;
        this.w = ringStyleRepository;
        this.x = dianaPresetRepository;
        this.y = setDianaPresetToWatchUseCase;
        this.z = om4;
        this.A = customizeRealDataRepository;
        this.B = userRepository;
        this.C = watchFaceRepository;
        this.D = an4;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps first time");
        this.k = portfolioApp.f();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$c, com.portfolio.platform.CoroutineUseCase$e] */
    public final void b(String str) {
        T t2;
        DianaPreset dianaPreset;
        List list = (List) this.e.a();
        if (list != null) {
            Boolean.valueOf(!list.isEmpty());
        }
        Iterator<T> it = this.i.iterator();
        while (true) {
            t2 = null;
            if (!it.hasNext()) {
                dianaPreset = null;
                break;
            }
            dianaPreset = it.next();
            if (wg6.a((Object) dianaPreset.getId(), (Object) str)) {
                break;
            }
        }
        DianaPreset dianaPreset2 = (DianaPreset) dianaPreset;
        if (dianaPreset2 != null) {
            Iterator<T> it2 = this.i.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                T next = it2.next();
                if (((DianaPreset) next).isActive()) {
                    t2 = next;
                    break;
                }
            }
            DianaPreset dianaPreset3 = (DianaPreset) t2;
            this.t.m();
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + dianaPreset3 + " set preset " + dianaPreset2 + " as active first");
            this.y.a(new SetDianaPresetToWatchUseCase.c(dianaPreset2), new c(dianaPreset3, this, str));
        }
    }

    @DexIgnore
    public final void c(int i2) {
        this.l = i2;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            MutableLiveData<String> mutableLiveData = this.k;
            k45 k45 = this.t;
            if (k45 != null) {
                mutableLiveData.a((HomeDianaCustomizeFragment) k45);
                this.e.a(this.t);
                this.f.a(this.t);
                this.y.g();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "stop fail due to " + e2);
        }
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> i() {
        return this.m;
    }

    @DexIgnore
    public final int j() {
        return this.l;
    }

    @DexIgnore
    public void k() {
        this.t.a(this);
    }

    @DexIgnore
    public final void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.i.size());
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new g(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final m35 a(DianaPreset dianaPreset) {
        WatchApp watchApp;
        String str;
        Complication complication;
        String str2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "convertPresetToDianaPresetConfigWrapper watchAppsSize " + this.h.size() + " compsSize " + this.g.size());
        ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
        ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        WatchFace watchFaceWithId = this.C.getWatchFaceWithId(dianaPreset.getWatchFaceId());
        WatchFaceWrapper b2 = watchFaceWithId != null ? WatchFaceHelper.b(watchFaceWithId, complications) : null;
        Iterator<DianaPresetComplicationSetting> it = complications.iterator();
        while (it.hasNext()) {
            DianaPresetComplicationSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.g.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    complication = null;
                    break;
                }
                complication = it2.next();
                if (wg6.a((Object) complication.getComplicationId(), (Object) component2)) {
                    break;
                }
            }
            Complication complication2 = (Complication) complication;
            if (complication2 != null) {
                String complicationId = complication2.getComplicationId();
                String icon = complication2.getIcon();
                if (icon != null) {
                    str2 = icon;
                } else {
                    str2 = "";
                }
                arrayList.add(new o35(complicationId, str2, jm4.a(PortfolioApp.get.instance(), complication2.getNameKey(), complication2.getName()), component1, this.z.a(this.o, this.m, complication2.getComplicationId(), dianaPreset)));
            } else {
                DianaPreset dianaPreset2 = dianaPreset;
            }
        }
        DianaPreset dianaPreset3 = dianaPreset;
        Iterator<DianaPresetWatchAppSetting> it3 = watchapps.iterator();
        while (it3.hasNext()) {
            DianaPresetWatchAppSetting next2 = it3.next();
            String component12 = next2.component1();
            String component22 = next2.component2();
            Iterator<T> it4 = this.h.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    watchApp = null;
                    break;
                }
                watchApp = it4.next();
                if (wg6.a((Object) watchApp.getWatchappId(), (Object) component22)) {
                    break;
                }
            }
            WatchApp watchApp2 = (WatchApp) watchApp;
            if (watchApp2 != null) {
                String watchappId = watchApp2.getWatchappId();
                String icon2 = watchApp2.getIcon();
                if (icon2 != null) {
                    str = icon2;
                } else {
                    str = "";
                }
                arrayList2.add(new o35(watchappId, str, jm4.a(PortfolioApp.get.instance(), watchApp2.getNameKey(), watchApp2.getName()), component12, (String) null, 16, (qg6) null));
            }
        }
        return new m35(dianaPreset.getId(), dianaPreset.getName(), arrayList, arrayList2, dianaPreset.isActive(), b2);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.n = i2;
        if (this.n == 1 && this.r.getFirst().booleanValue()) {
            List list = (List) this.r.getSecond();
            if (list != null) {
                a((List<DianaPreset>) list);
            }
            this.r = qc6.a(false, null);
        }
    }

    @DexIgnore
    public void a(int i2) {
        boolean z2 = i2 == this.i.size();
        if (this.i.size() > i2) {
            this.l = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "changePresetPosition " + this.l);
            this.j = this.i.get(this.l);
            DianaPreset dianaPreset = this.j;
            if (dianaPreset != null) {
                this.t.b(dianaPreset.isActive(), z2);
            }
        } else {
            this.t.b(false, z2);
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HomeDianaCustomizePresenter", "viewPos " + i2 + " presenterPos " + this.l + " size " + this.i.size());
    }

    @DexIgnore
    public void a(String str, String str2) {
        wg6.b(str, "name");
        wg6.b(str2, "presetId");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, str2, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$f, com.portfolio.platform.CoroutineUseCase$e] */
    public void a(boolean z2) {
        DianaPreset dianaPreset;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.j);
        Set<Integer> a2 = xm4.d.a(this.j);
        if (z2) {
            this.D.q(true);
            this.D.r(true);
        }
        xm4 xm4 = xm4.d;
        k45 k45 = this.t;
        if (k45 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } else if (xm4.a(((HomeDianaCustomizeFragment) k45).getContext(), a2) && (dianaPreset = this.j) != null) {
            this.t.m();
            this.y.a(new SetDianaPresetToWatchUseCase.c(dianaPreset), new f(this));
        }
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "nextActivePresetId");
        DianaPreset dianaPreset = this.j;
        if (dianaPreset == null) {
            return;
        }
        if (dianaPreset.isActive()) {
            b(str);
        } else {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(dianaPreset, (xe6) null, this, str), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.s = false;
        if (i2 == 100 && i3 == -1) {
            this.t.e(0);
        }
        if (i2 == 111) {
            xm4 xm4 = xm4.d;
            k45 k45 = this.t;
            if (k45 == null) {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
            } else if (xm4.a(((HomeDianaCustomizeFragment) k45).getContext(), 1)) {
                a(false);
            }
        }
    }

    @DexIgnore
    public void a(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2) {
        wg6.b(list, "views");
        wg6.b(list2, "customizeWidgetViews");
        if (!this.s) {
            this.s = true;
            this.t.a(m35, list, list2);
        }
    }

    @DexIgnore
    public final void a(List<DianaPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "doShowingPreset");
        this.i.clear();
        this.i.addAll(list);
        int size = this.i.size();
        int i2 = this.l;
        if (size > i2 && i2 > 0) {
            this.j = this.i.get(i2);
        }
        l();
    }
}
