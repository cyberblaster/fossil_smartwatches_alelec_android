package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.de4;
import com.fossil.fg5;
import com.fossil.fj5;
import com.fossil.gg5;
import com.fossil.kb;
import com.fossil.oj3;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewDayFragment extends BaseFragment implements gg5 {
    @DexIgnore
    public ax5<de4> f;
    @DexIgnore
    public fg5 g;
    @DexIgnore
    public fj5 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ String j; // = ThemeManager.l.a().b("nonBrandSurface");
    @DexIgnore
    public /* final */ String o; // = ThemeManager.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String p; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            Date date = new Date();
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            Date date = new Date();
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ de4 b;

        @DexIgnore
        public d(SleepOverviewDayFragment sleepOverviewDayFragment, de4 de4) {
            this.a = sleepOverviewDayFragment;
            this.b = de4;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            SleepOverviewDayFragment.super.a(i, f, i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayFragment", "onPageScrolled " + i);
            if (!TextUtils.isEmpty(this.a.p)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepOverviewDayFragment", "set icon color " + this.a.p);
                int parseColor = Color.parseColor(this.a.p);
                TabLayout.g b4 = this.b.r.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.o) && this.a.i != i) {
                int parseColor2 = Color.parseColor(this.a.o);
                TabLayout.g b5 = this.b.r.b(this.a.i);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.i = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayFragment a;

        @DexIgnore
        public e(SleepOverviewDayFragment sleepOverviewDayFragment) {
            this.a = sleepOverviewDayFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.o) && !TextUtils.isEmpty(this.a.p)) {
                int parseColor = Color.parseColor(this.a.o);
                int parseColor2 = Color.parseColor(this.a.p);
                gVar.b(2131231004);
                if (i == this.a.i) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "SleepOverviewDayFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        de4 a2;
        wg6.b(layoutInflater, "inflater");
        SleepOverviewDayFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onCreateView");
        de4 a3 = kb.a(layoutInflater, 2131558612, viewGroup, false, e1());
        wg6.a((Object) a3, "binding");
        a(a3);
        this.f = new ax5<>(this, a3);
        ax5<de4> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        SleepOverviewDayFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onResume");
        fg5 fg5 = this.g;
        if (fg5 != null) {
            fg5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        SleepOverviewDayFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onStop");
        fg5 fg5 = this.g;
        if (fg5 != null) {
            fg5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void s(List<fj5.b> list) {
        de4 a2;
        TabLayout tabLayout;
        de4 a3;
        TabLayout tabLayout2;
        de4 a4;
        Object r0;
        de4 a5;
        Object r5;
        wg6.b(list, "listOfSleepSessionUIData");
        if (list.isEmpty()) {
            ax5<de4> ax5 = this.f;
            if (ax5 != null && (a5 = ax5.a()) != null && (r5 = a5.v) != 0) {
                r5.setVisibility(0);
                return;
            }
            return;
        }
        ax5<de4> ax52 = this.f;
        if (!(ax52 == null || (a4 = ax52.a()) == null || (r0 = a4.v) == 0)) {
            r0.setVisibility(8);
        }
        if (list.size() > 1) {
            ax5<de4> ax53 = this.f;
            if (!(ax53 == null || (a3 = ax53.a()) == null || (tabLayout2 = a3.r) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            ax5<de4> ax54 = this.f;
            if (!(ax54 == null || (a2 = ax54.a()) == null || (tabLayout = a2.r) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        fj5 fj5 = this.h;
        if (fj5 != null) {
            fj5.a(list);
        }
    }

    @DexIgnore
    public void a(fg5 fg5) {
        wg6.b(fg5, "presenter");
        this.g = fg5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(de4 de4) {
        de4.s.setOnClickListener(b.a);
        String str = this.j;
        if (str != null) {
            de4.v.setBackgroundColor(Color.parseColor(str));
            de4.q.setBackgroundColor(Color.parseColor(str));
        }
        de4.t.setOnClickListener(c.a);
        this.h = new fj5(new ArrayList());
        ViewPager2 viewPager2 = de4.u;
        wg6.a((Object) viewPager2, "binding.rvSleeps");
        viewPager2.setAdapter(this.h);
        de4.u.a(new d(this, de4));
        new oj3(de4.r, de4.u, new e(this)).a();
        ViewPager2 viewPager22 = de4.u;
        wg6.a((Object) viewPager22, "binding.rvSleeps");
        viewPager22.setCurrentItem(this.i);
    }
}
