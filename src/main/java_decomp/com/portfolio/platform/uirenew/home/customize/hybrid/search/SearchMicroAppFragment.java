package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.fossil.aa5;
import com.fossil.ax5;
import com.fossil.ba5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.nh6;
import com.fossil.nk4;
import com.fossil.pb4;
import com.fossil.qg6;
import com.fossil.qu4;
import com.fossil.rc6;
import com.fossil.tq4;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchMicroAppFragment extends BaseFragment implements ba5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<pb4> f;
    @DexIgnore
    public qu4 g;
    @DexIgnore
    public aa5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SearchMicroAppFragment.j;
        }

        @DexIgnore
        public final SearchMicroAppFragment b() {
            return new SearchMicroAppFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppFragment a;

        @DexIgnore
        public b(SearchMicroAppFragment searchMicroAppFragment) {
            this.a = searchMicroAppFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            pb4 a2 = this.a.k1().a();
            if (a2 != null) {
                a2.s.setText("");
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppFragment a;

        @DexIgnore
        public c(SearchMicroAppFragment searchMicroAppFragment) {
            this.a = searchMicroAppFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            Object r3;
            ImageView imageView2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SearchMicroAppFragment.o.a();
            local.d(a2, "onTextChanged " + charSequence);
            if (TextUtils.isEmpty(charSequence)) {
                pb4 a3 = this.a.k1().a();
                if (!(a3 == null || (imageView2 = a3.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.a.b("");
                this.a.l1().h();
                return;
            }
            pb4 a4 = this.a.k1().a();
            if (!(a4 == null || (r3 = a4.v) == 0)) {
                r3.setVisibility(8);
            }
            pb4 a5 = this.a.k1().a();
            if (!(a5 == null || (imageView = a5.r) == null)) {
                imageView.setVisibility(0);
            }
            this.a.b(String.valueOf(charSequence));
            this.a.l1().a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppFragment a;

        @DexIgnore
        public d(SearchMicroAppFragment searchMicroAppFragment) {
            this.a = searchMicroAppFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements qu4.d {
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppFragment a;

        @DexIgnore
        public e(SearchMicroAppFragment searchMicroAppFragment) {
            this.a = searchMicroAppFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void a(String str) {
            wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            pb4 a2 = this.a.k1().a();
            if (a2 != null) {
                Object r1 = a2.v;
                wg6.a((Object) r1, "it.tvNotFound");
                r1.setVisibility(0);
                Object r0 = a2.v;
                wg6.a((Object) r0, "it.tvNotFound");
                nh6 nh6 = nh6.a;
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886579);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object[] objArr = {str};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r0.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements qu4.e {
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppFragment a;

        @DexIgnore
        public f(SearchMicroAppFragment searchMicroAppFragment) {
            this.a = searchMicroAppFragment;
        }

        @DexIgnore
        public void a(MicroApp microApp) {
            wg6.b(microApp, "item");
            this.a.l1().a(microApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ pb4 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(pb4 pb4, long j) {
            this.a = pb4;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r4v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r4v10, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        public void onTransitionStart(Transition transition) {
            Object r4 = this.a.q;
            wg6.a((Object) r4, "binding.btnCancel");
            if (r4.getAlpha() == 0.0f) {
                this.a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.a.q.animate().setDuration(this.b).alpha(0.0f);
            }
        }
    }

    /*
    static {
        String simpleName = SearchMicroAppFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "SearchMicroAppFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void b(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        qu4 qu4 = this.g;
        if (qu4 != null) {
            qu4.a(str);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void d(List<lc6<MicroApp, String>> list) {
        wg6.b(list, "recentSearchResult");
        qu4 qu4 = this.g;
        if (qu4 != null) {
            qu4.a(list);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return j;
    }

    @DexIgnore
    public boolean i1() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: com.portfolio.platform.view.FlexibleButton} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: android.view.View} */
    /* JADX WARNING: Multi-variable type inference failed */
    public void j1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            nk4 nk4 = nk4.a;
            ax5<pb4> ax5 = this.f;
            View view = null;
            if (ax5 != null) {
                pb4 a2 = ax5.a();
                if (a2 != null) {
                    view = a2.q;
                }
                if (view != null) {
                    wg6.a((Object) activity, "it");
                    nk4.a(view, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.view.View");
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final ax5<pb4> k1() {
        ax5<pb4> ax5 = this.f;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final aa5 l1() {
        aa5 aa5 = this.h;
        if (aa5 != null) {
            return aa5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        SearchMicroAppFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558575, viewGroup, false, e1()));
        ax5<pb4> ax5 = this.f;
        if (ax5 != null) {
            pb4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        SearchMicroAppFragment.super.onResume();
        aa5 aa5 = this.h;
        if (aa5 != null) {
            aa5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        SearchMicroAppFragment.super.onStop();
        aa5 aa5 = this.h;
        if (aa5 != null) {
            aa5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.view.View, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r5v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "it");
            a(activity, 550);
        }
        this.g = new qu4();
        ax5<pb4> ax5 = this.f;
        if (ax5 != null) {
            pb4 a2 = ax5.a();
            if (a2 != null) {
                pb4 pb4 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = pb4.u;
                wg6.a((Object) recyclerViewEmptySupport, "this.rvResults");
                qu4 qu4 = this.g;
                if (qu4 != null) {
                    recyclerViewEmptySupport.setAdapter(qu4);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = pb4.u;
                    wg6.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = pb4.u;
                    Object r1 = pb4.v;
                    wg6.a((Object) r1, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(r1);
                    ImageView imageView = pb4.r;
                    wg6.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    pb4.r.setOnClickListener(new b(this));
                    pb4.s.addTextChangedListener(new c(this));
                    pb4.q.setOnClickListener(new d(this));
                    qu4 qu42 = this.g;
                    if (qu42 != null) {
                        qu42.a((qu4.d) new e(this));
                        qu4 qu43 = this.g;
                        if (qu43 != null) {
                            qu43.a((qu4.e) new f(this));
                        } else {
                            wg6.d("mAdapter");
                            throw null;
                        }
                    } else {
                        wg6.d("mAdapter");
                        throw null;
                    }
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u() {
        qu4 qu4 = this.g;
        if (qu4 != null) {
            qu4.b((List<lc6<MicroApp, String>>) null);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = tq4.a.a(j2);
        Window window = fragmentActivity.getWindow();
        wg6.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        ax5<pb4> ax5 = this.f;
        if (ax5 != null) {
            pb4 a3 = ax5.a();
            if (a3 != null) {
                wg6.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(List<lc6<MicroApp, String>> list) {
        wg6.b(list, "results");
        qu4 qu4 = this.g;
        if (qu4 != null) {
            qu4.b(list);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public final TransitionSet a(TransitionSet transitionSet, long j2, pb4 pb4) {
        Object r0 = pb4.q;
        wg6.a((Object) r0, "binding.btnCancel");
        r0.setAlpha(0.0f);
        return transitionSet.addListener(new g(pb4, j2));
    }

    @DexIgnore
    public void a(aa5 aa5) {
        wg6.b(aa5, "presenter");
        this.h = aa5;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: com.portfolio.platform.view.FlexibleButton} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: android.view.View} */
    /* JADX WARNING: Multi-variable type inference failed */
    public void a(MicroApp microApp) {
        wg6.b(microApp, "selectedMicroApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            nk4 nk4 = nk4.a;
            ax5<pb4> ax5 = this.f;
            View view = null;
            if (ax5 != null) {
                pb4 a2 = ax5.a();
                if (a2 != null) {
                    view = a2.q;
                }
                if (view != null) {
                    wg6.a((Object) activity, "it");
                    nk4.a(view, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_MICRO_APP_RESULT_ID", microApp.getId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.view.View");
            }
            wg6.d("mBinding");
            throw null;
        }
    }
}
