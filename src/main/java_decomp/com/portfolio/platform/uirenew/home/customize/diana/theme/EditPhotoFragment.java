package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.a75;
import com.fossil.ax5;
import com.fossil.b75;
import com.fossil.cl4;
import com.fossil.d75;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.j94;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lw4;
import com.fossil.lx5;
import com.fossil.o35;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoFragment extends BasePermissionFragment implements d75.d, CropImageView.i, CropImageView.g, FragmentManager.h, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public EditPhotoViewModel g;
    @DexIgnore
    public ax5<j94> h;
    @DexIgnore
    public Uri i;
    @DexIgnore
    public d75 j;
    @DexIgnore
    public /* final */ ArrayList<FilterType> o;
    @DexIgnore
    public FilterType p;
    @DexIgnore
    public w04 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final EditPhotoFragment a(Uri uri, ArrayList<o35> arrayList) {
            wg6.b(uri, "uri");
            wg6.b(arrayList, "complications");
            EditPhotoFragment editPhotoFragment = new EditPhotoFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("MAGE_URI_ARG", uri);
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", arrayList);
            editPhotoFragment.setArguments(bundle);
            return editPhotoFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<b75.e> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public b(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(EditPhotoViewModel.e eVar) {
            TextView textView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EditPhotoFragment", "Crop image: result = " + eVar);
            this.a.a();
            j94 b = this.a.m1();
            if (!(b == null || (textView = b.u) == null)) {
                textView.setClickable(true);
            }
            if (eVar.a() && this.a.getActivity() != null) {
                if (EditPhotoFragment.f(this.a).c() != null) {
                    EditPhotoFragment editPhotoFragment = this.a;
                    PreviewFragment.a aVar = PreviewFragment.j;
                    ArrayList<o35> c = EditPhotoFragment.f(editPhotoFragment).c();
                    if (c != null) {
                        editPhotoFragment.a(aVar.a(c, this.a.p), "PreviewFragment", 2131362119);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "Can not initialize PreviewFragment without complications");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<b75.b> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public c(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(EditPhotoViewModel.b bVar) {
            List<FilterResult> a2 = bVar.a();
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(a2));
            this.a.a();
            if (!a2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (FilterResult next : bVar.a()) {
                    Bitmap preview = next.getPreview();
                    wg6.a((Object) preview, "filter.preview");
                    Bitmap a3 = lw4.a(preview);
                    wg6.a((Object) a3, "circleBitmap");
                    FilterType type = next.getType();
                    wg6.a((Object) type, "filter.type");
                    arrayList.add(new d75.b(a3, type));
                }
                this.a.p = ((d75.b) arrayList.get(0)).b();
                d75 c = this.a.j;
                if (c != null) {
                    c.a((List<d75.b>) arrayList);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<b75.a> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public d(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(EditPhotoViewModel.a aVar) {
            CropImageView cropImageView;
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(aVar.a()));
            FilterResult a2 = aVar.a();
            j94 b = this.a.m1();
            if (!(b == null || (cropImageView = b.q) == null)) {
                cropImageView.a(a2.getPreview());
            }
            EditPhotoFragment editPhotoFragment = this.a;
            FilterType type = a2.getType();
            wg6.a((Object) type, "it.type");
            editPhotoFragment.p = type;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public e(EditPhotoFragment editPhotoFragment, j94 j94) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public f(EditPhotoFragment editPhotoFragment, j94 j94) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.i != null) {
                this.a.k1();
            } else {
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Can not crop image cause of null imageURI");
            }
        }
    }

    @DexIgnore
    public EditPhotoFragment() {
        ArrayList<FilterType> arrayList = new ArrayList<>();
        arrayList.add(FilterType.ATKINSON_DITHERING);
        arrayList.add(FilterType.BURKES_DITHERING);
        arrayList.add(FilterType.DIRECT_MAPPING);
        arrayList.add(FilterType.JAJUNI_DITHERING);
        arrayList.add(FilterType.ORDERED_DITHERING);
        arrayList.add(FilterType.SIERRA_DITHERING);
        this.o = arrayList;
        FilterType filterType = this.o.get(0);
        wg6.a((Object) filterType, "mFilterList[0]");
        this.p = filterType;
    }

    @DexIgnore
    public static final /* synthetic */ EditPhotoViewModel f(EditPhotoFragment editPhotoFragment) {
        EditPhotoViewModel editPhotoViewModel = editPhotoFragment.g;
        if (editPhotoViewModel != null) {
            return editPhotoViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void j1() {
        EditPhotoViewModel editPhotoViewModel = this.g;
        if (editPhotoViewModel != null) {
            editPhotoViewModel.d().a(getViewLifecycleOwner(), new b(this));
            EditPhotoViewModel editPhotoViewModel2 = this.g;
            if (editPhotoViewModel2 != null) {
                editPhotoViewModel2.b().a(getViewLifecycleOwner(), new c(this));
                EditPhotoViewModel editPhotoViewModel3 = this.g;
                if (editPhotoViewModel3 != null) {
                    editPhotoViewModel3.a().a(getViewLifecycleOwner(), new d(this));
                } else {
                    wg6.d("mViewModel");
                    throw null;
                }
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void k1() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "cropImage()");
        EditPhotoViewModel.d l1 = l1();
        if (l1 != null) {
            b();
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.a(l1, this.p);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final EditPhotoViewModel.d l1() {
        CropImageView cropImageView;
        ConstraintLayout constraintLayout;
        j94 m1 = m1();
        if (!(m1 == null || (constraintLayout = m1.r) == null)) {
            constraintLayout.invalidate();
        }
        j94 m12 = m1();
        if (m12 == null || (cropImageView = m12.q) == null) {
            return null;
        }
        wg6.a((Object) cropImageView, "it");
        if (cropImageView.getInitializeBitmap() == null) {
            return null;
        }
        cropImageView.clearAnimation();
        float[] fArr = new float[8];
        float[] cropPoints = cropImageView.getCropPoints();
        wg6.a((Object) cropPoints, "it.cropPoints");
        int length = cropPoints.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            fArr[i3] = cropPoints[i2] / ((float) cropImageView.getLoadedSampleSize());
            i2++;
            i3++;
        }
        Bitmap initializeBitmap = cropImageView.getInitializeBitmap();
        wg6.a((Object) initializeBitmap, "it.initializeBitmap");
        int rotatedDegrees = cropImageView.getRotatedDegrees();
        boolean c2 = cropImageView.c();
        Object obj = cropImageView.getAspectRatio().first;
        wg6.a(obj, "it.aspectRatio.first");
        int intValue = ((Number) obj).intValue();
        Object obj2 = cropImageView.getAspectRatio().second;
        wg6.a(obj2, "it.aspectRatio.second");
        return new EditPhotoViewModel.d(initializeBitmap, fArr, rotatedDegrees, c2, intValue, ((Number) obj2).intValue(), cropImageView.d(), cropImageView.e());
    }

    @DexIgnore
    public final j94 m1() {
        ax5<j94> ax5 = this.h;
        if (ax5 != null) {
            return ax5.a();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n1() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "updateFilterList()");
        EditPhotoViewModel.d l1 = l1();
        if (l1 != null) {
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.a((b75.d) l1, this.o);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        EditPhotoFragment.super.onActivityCreated(bundle);
        if (bundle != null) {
            b(bundle);
        }
    }

    @DexIgnore
    public void onBackStackChanged() {
        CropImageView cropImageView;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onBackStackChanged()");
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        if (childFragmentManager.t() == 0) {
            ax5<j94> ax5 = this.h;
            if (ax5 != null) {
                j94 a2 = ax5.a();
                if (a2 != null) {
                    a2.q.setOnSetImageUriCompleteListener(this);
                    CropImageView cropImageView2 = a2.q;
                    if (cropImageView2 != null) {
                        cropImageView2.setOnSetCropOverlayReleasedListener(this);
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        ax5<j94> ax52 = this.h;
        if (ax52 != null) {
            j94 a3 = ax52.a();
            if (a3 != null && (cropImageView = a3.q) != null) {
                cropImageView.a();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().g().a(new a75()).a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            w04 w04 = this.q;
            if (w04 != null) {
                EditPhotoViewModel a2 = vd.a(activity, w04).a(EditPhotoViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026otoViewModel::class.java)");
                this.g = a2;
                Bundle arguments = getArguments();
                if (arguments != null) {
                    wg6.a((Object) arguments, "it");
                    b(arguments);
                }
                getChildFragmentManager().a(this);
                return;
            }
            wg6.d("viewModelFactory");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        j94 a2 = kb.a(LayoutInflater.from(getContext()), 2131558545, (ViewGroup) null, true, e1());
        this.h = new ax5<>(this, a2);
        this.j = new d75((ArrayList) null, this, 1, (qg6) null);
        ax5<j94> ax5 = this.h;
        if (ax5 != null) {
            j94 a3 = ax5.a();
            if (a3 != null) {
                TextView textView = a3.t;
                wg6.a((Object) textView, "fragmentBinding.tvCancel");
                cl4.a(textView, new e(this, a2));
                TextView textView2 = a3.u;
                wg6.a((Object) textView2, "fragmentBinding.tvPreview");
                cl4.a(textView2, new f(this, a2));
                if (this.i != null) {
                    a3.q.setOnSetImageUriCompleteListener(this);
                    CropImageView cropImageView = a3.q;
                    if (cropImageView != null) {
                        cropImageView.setOnSetCropOverlayReleasedListener(this);
                    }
                    b();
                    a3.q.a(this.i, this.p);
                } else {
                    a();
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator((RecyclerView.j) null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.j);
            }
            j1();
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        d75 d75 = this.j;
        if (d75 != null) {
            d75.a((d75.d) null);
        }
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        EditPhotoFragment.super.onSaveInstanceState(bundle);
        bundle.putParcelable("MAGE_URI_ARG", this.i);
        EditPhotoViewModel editPhotoViewModel = this.g;
        if (editPhotoViewModel != null) {
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", editPhotoViewModel.c());
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "initialize()");
        ArrayList arrayList = null;
        if (bundle.containsKey("MAGE_URI_ARG")) {
            Bundle arguments = getArguments();
            this.i = arguments != null ? (Uri) arguments.getParcelable("MAGE_URI_ARG") : null;
        }
        if (bundle.containsKey("COMPLICATIONS_ARG")) {
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getParcelableArrayList("COMPLICATIONS_ARG");
                }
                editPhotoViewModel.a(arrayList);
                return;
            }
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(d75.b bVar) {
        j94 m1;
        CropImageView cropImageView;
        Bitmap initializeBitmap;
        wg6.b(bVar, "imageFilter");
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(bVar.b()));
        if (this.p != bVar.b() && (m1 = m1()) != null && (cropImageView = m1.q) != null && (initializeBitmap = cropImageView.getInitializeBitmap()) != null) {
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.a(initializeBitmap, bVar.b());
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(CropImageView cropImageView, Uri uri, Exception exc) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onSetImageUriComplete, uri = " + uri + ", error = " + exc);
        if (isActive()) {
            if (exc == null) {
                n1();
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("EditPhotoFragment", "error = " + exc);
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.E(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(Rect rect) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onCropOverlayReleased()");
        n1();
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onDialogFragmentResult tag = " + str);
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() == 407101428 && str.equals("PROCESS_IMAGE_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        super.a(str, i2, intent);
    }
}
