package com.portfolio.platform.uirenew.home.dashboard.activity;

import androidx.lifecycle.LiveData;
import com.fossil.ac5;
import com.fossil.af6;
import com.fossil.bc5$b$a;
import com.fossil.bc5$b$b;
import com.fossil.bc5$c$a;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gg6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zb5;
import com.fossil.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardActivityPresenter extends zb5 implements vk4.a {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public zh4 g;
    @DexIgnore
    public /* final */ ac5 h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ ActivitySummaryDao k;
    @DexIgnore
    public /* final */ FitnessDatabase l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ u04 n;
    @DexIgnore
    public /* final */ ik4 o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$initDataSource$1", f = "DashboardActivityPresenter.kt", l = {90}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActivityPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DashboardActivityPresenter dashboardActivityPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardActivityPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            LiveData pagedList;
            Object a = ff6.a();
            int i = this.label;
            LiveData liveData = null;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                bc5$b$b bc5_b_b = new bc5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bc5_b_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                Date d = bk4.d(mFUser.getCreatedAt());
                DashboardActivityPresenter dashboardActivityPresenter = this.this$0;
                SummariesRepository j = dashboardActivityPresenter.i;
                SummariesRepository j2 = this.this$0.i;
                ik4 i2 = this.this$0.o;
                FitnessDataRepository g = this.this$0.j;
                ActivitySummaryDao c = this.this$0.k;
                FitnessDatabase h = this.this$0.l;
                wg6.a((Object) d, "createdDate");
                dashboardActivityPresenter.f = j.getSummariesPaging(j2, i2, g, c, h, d, this.this$0.n, this.this$0);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("listing ");
                sb.append(this.this$0.f);
                sb.append(" pageList ");
                Listing b = this.this$0.f;
                if (b != null) {
                    liveData = b.getPagedList();
                }
                sb.append(liveData);
                local.d("DashboardActivityPresenter", sb.toString());
                Listing b2 = this.this$0.f;
                if (!(b2 == null || (pagedList = b2.getPagedList()) == null)) {
                    ac5 l = this.this$0.h;
                    if (l != null) {
                        pagedList.a((DashboardActivityFragment) l, new bc5$b$a(this));
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                    }
                }
                ac5 l2 = this.this$0.h;
                zh4 distanceUnit = mFUser.getDistanceUnit();
                if (distanceUnit == null) {
                    distanceUnit = zh4.METRIC;
                }
                l2.b(distanceUnit);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1", f = "DashboardActivityPresenter.kt", l = {49}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActivityPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DashboardActivityPresenter dashboardActivityPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardActivityPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                bc5$c$a bc5_c_a = new bc5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bc5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                if (this.this$0.g == null) {
                    this.this$0.g = mFUser.getDistanceUnit();
                } else {
                    zh4 distanceUnit = mFUser.getDistanceUnit();
                    if (!(distanceUnit == null || this.this$0.g == distanceUnit)) {
                        this.this$0.g = distanceUnit;
                        this.this$0.h.b(distanceUnit);
                    }
                }
            }
            FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "start");
            if (!bk4.t(this.this$0.e).booleanValue()) {
                this.this$0.e = new Date();
                Listing b = this.this$0.f;
                if (b != null) {
                    b.getRefresh();
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DashboardActivityPresenter(ac5 ac5, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, u04 u04, ik4 ik4) {
        wg6.b(ac5, "mView");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(activitySummaryDao, "mActivitySummaryDao");
        wg6.b(fitnessDatabase, "mFitnessDatabase");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(u04, "mAppExecutors");
        wg6.b(ik4, "mFitnessHelper");
        this.h = ac5;
        this.i = summariesRepository;
        this.j = fitnessDataRepository;
        this.k = activitySummaryDao;
        this.l = fitnessDatabase;
        this.m = userRepository;
        this.n = u04;
        this.o = ik4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        LiveData<cf<ActivitySummary>> pagedList;
        try {
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                ac5 ac5 = this.h;
                if (ac5 != null) {
                    pagedList.a((DashboardActivityFragment) ac5);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                }
            }
            this.i.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("DashboardActivityPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        gg6<cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        wg6.b(gVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActivityPresenter", "onStatusChange status=" + gVar);
        if (gVar.a()) {
            this.h.f();
        }
    }
}
