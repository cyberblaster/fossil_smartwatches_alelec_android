package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.m15;
import com.fossil.mu4;
import com.fossil.n15;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.w6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.xb4;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationAllowCallsAndMessagesFragment;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsAndAppsAssignedFragment extends BasePermissionFragment implements n15, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public m15 g;
    @DexIgnore
    public ax5<xb4> h;
    @DexIgnore
    public mu4 i;
    @DexIgnore
    public NotificationAllowCallsAndMessagesFragment j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsAndAppsAssignedFragment.p;
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedFragment b() {
            return new NotificationContactsAndAppsAssignedFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NotificationAllowCallsAndMessagesFragment.b {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public c(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public void a(int i, boolean z, boolean z2) {
            NotificationContactsAndAppsAssignedFragment.b(this.a).a(i, z, z2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public d(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public e(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.k1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public f(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsAndAppsAssignedFragment.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public g(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsAndAppsAssignedFragment.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public h(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsAndAppsAssignedFragment.b(this.a).l();
        }
    }

    /*
    static {
        String simpleName = NotificationContactsAndAppsAssignedFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationContactsAndA\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ m15 b(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
        m15 m15 = notificationContactsAndAppsAssignedFragment.g;
        if (m15 != null) {
            return m15;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void c(int i2, ArrayList<wx4> arrayList) {
        wg6.b(arrayList, "contactWrappersSelected");
        NotificationHybridContactActivity.C.a(this, i2, arrayList);
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void i(int i2) {
        Object r0;
        ax5<xb4> ax5 = this.h;
        if (ax5 != null) {
            xb4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.r) != 0) {
                nh6 nh6 = nh6.a;
                String a3 = jm4.a(getContext(), 2131886164);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026ct_Title__AssignToNumber)");
                Object[] objArr = {Integer.valueOf(i2)};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r0.setText(format);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean i1() {
        j1();
        return true;
    }

    @DexIgnore
    public void j() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void j1() {
        ax5<xb4> ax5 = this.h;
        if (ax5 == null) {
            wg6.d("mBinding");
            throw null;
        } else if (ax5.a() != null) {
            m15 m15 = this.g;
            if (m15 == null) {
                wg6.d("mPresenter");
                throw null;
            } else if (!m15.i()) {
                close();
            } else {
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.b(childFragmentManager);
            }
        }
    }

    @DexIgnore
    public final void k1() {
        mu4 mu4 = this.i;
        if (mu4 == null) {
            wg6.d("mAdapter");
            throw null;
        } else if (mu4.c()) {
            m15 m15 = this.g;
            if (m15 != null) {
                m15.m();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        NotificationContactsAndAppsAssignedFragment.super.onActivityResult(i2, i3, intent);
        if (i2 != 4567) {
            if (i2 != 5678) {
                if (i2 == 6789 && i3 == -1 && intent != null) {
                    Serializable serializableExtra = intent.getSerializableExtra("CONTACT_DATA");
                    if (serializableExtra != null) {
                        ArrayList arrayList = (ArrayList) serializableExtra;
                        m15 m15 = this.g;
                        if (m15 != null) {
                            m15.b(arrayList);
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                    }
                }
            } else if (i3 == -1 && intent != null) {
                Serializable serializableExtra2 = intent.getSerializableExtra("CONTACT_DATA");
                if (serializableExtra2 != null) {
                    ArrayList arrayList2 = (ArrayList) serializableExtra2;
                    m15 m152 = this.g;
                    if (m152 != null) {
                        m152.a((ArrayList<wx4>) arrayList2);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                }
            }
        } else if (i3 == -1 && intent != null) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("APP_DATA");
            m15 m153 = this.g;
            if (m153 != null) {
                wg6.a((Object) stringArrayListExtra, "stringAppsSelected");
                m153.c(stringArrayListExtra);
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        xb4 a2 = kb.a(layoutInflater, 2131558580, viewGroup, false, e1());
        this.j = getChildFragmentManager().b(NotificationAllowCallsAndMessagesFragment.x.a());
        if (this.j == null) {
            this.j = NotificationAllowCallsAndMessagesFragment.x.b();
        }
        NotificationAllowCallsAndMessagesFragment notificationAllowCallsAndMessagesFragment = this.j;
        if (notificationAllowCallsAndMessagesFragment != null) {
            notificationAllowCallsAndMessagesFragment.a(new c(this));
        }
        Object r4 = a2.q;
        wg6.a((Object) r4, "binding.ftvAssignSection");
        r4.setVisibility(8);
        RecyclerView recyclerView = a2.y;
        wg6.a((Object) recyclerView, "binding.rvAssign");
        recyclerView.setVisibility(8);
        ImageView imageView = a2.t;
        wg6.a((Object) imageView, "binding.ivDone");
        Context context = getContext();
        if (context != null) {
            imageView.setBackground(w6.c(context, 2131230913));
            a2.s.setOnClickListener(new d(this));
            a2.t.setOnClickListener(new e(this));
            a2.w.setOnClickListener(new f(this));
            a2.v.setOnClickListener(new g(this));
            a2.u.setOnClickListener(new h(this));
            mu4 mu4 = new mu4();
            mu4.a((mu4.c) new b(this));
            this.i = mu4;
            RecyclerView recyclerView2 = a2.y;
            recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
            recyclerView2.setHasFixedSize(true);
            mu4 mu42 = this.i;
            if (mu42 != null) {
                recyclerView2.setAdapter(mu42);
                String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
                if (!TextUtils.isEmpty(b2)) {
                    int parseColor = Color.parseColor(b2);
                    a2.z.setBackgroundColor(parseColor);
                    a2.A.setBackgroundColor(parseColor);
                    a2.B.setBackgroundColor(parseColor);
                }
                this.h = new ax5<>(this, a2);
                wg6.a((Object) a2, "binding");
                return a2.d();
            }
            wg6.d("mAdapter");
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        m15 m15 = this.g;
        if (m15 != null) {
            m15.g();
            NotificationContactsAndAppsAssignedFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationContactsAndAppsAssignedFragment.super.onResume();
        m15 m15 = this.g;
        if (m15 != null) {
            m15.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void p(boolean z) {
        ImageView imageView;
        if (isActive()) {
            ax5<xb4> ax5 = this.h;
            if (ax5 != null) {
                xb4 a2 = ax5.a();
                if (a2 != null && (imageView = a2.t) != null) {
                    wg6.a((Object) imageView, "doneButton");
                    imageView.setEnabled(z);
                    imageView.setClickable(z);
                    if (z) {
                        Context context = getContext();
                        if (context != null) {
                            imageView.setBackground(w6.c(context, 2131100008));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        Context context2 = getContext();
                        if (context2 != null) {
                            imageView.setBackground(w6.c(context2, 2131099823));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(int i2, ArrayList<String> arrayList) {
        wg6.b(arrayList, "stringAppsSelected");
        NotificationHybridAppActivity.C.a(this, i2, arrayList);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void e(List<Object> list) {
        wg6.b(list, "contactAndAppData");
        mu4 mu4 = this.i;
        if (mu4 != null) {
            mu4.a((List<? extends Object>) list);
            if (!isActive()) {
                return;
            }
            if (list.isEmpty()) {
                ax5<xb4> ax5 = this.h;
                if (ax5 != null) {
                    xb4 a2 = ax5.a();
                    if (a2 != null) {
                        Object r1 = a2.q;
                        wg6.a((Object) r1, "it.ftvAssignSection");
                        r1.setVisibility(8);
                        RecyclerView recyclerView = a2.y;
                        wg6.a((Object) recyclerView, "it.rvAssign");
                        recyclerView.setVisibility(8);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            ax5<xb4> ax52 = this.h;
            if (ax52 != null) {
                xb4 a3 = ax52.a();
                if (a3 != null) {
                    Object r12 = a3.q;
                    wg6.a((Object) r12, "it.ftvAssignSection");
                    r12.setVisibility(0);
                    RecyclerView recyclerView2 = a3.y;
                    wg6.a((Object) recyclerView2, "it.rvAssign");
                    recyclerView2.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public void a(m15 m15) {
        wg6.b(m15, "presenter");
        this.g = m15;
    }

    @DexIgnore
    public void a(int i2, ArrayList<wx4> arrayList) {
        wg6.b(arrayList, "contactWrappersSelected");
        NotificationHybridEveryoneActivity.C.a(this, i2, arrayList);
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            return;
        }
        if (i2 == 2131363105) {
            close();
        } else if (i2 == 2131363190) {
            k1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements mu4.c {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedFragment a;

        @DexIgnore
        public b(NotificationContactsAndAppsAssignedFragment notificationContactsAndAppsAssignedFragment) {
            this.a = notificationContactsAndAppsAssignedFragment;
        }

        @DexIgnore
        public void a(wx4 wx4) {
            wg6.b(wx4, "contactWrapper");
            NotificationAllowCallsAndMessagesFragment a2 = this.a.j;
            if (a2 != null) {
                Contact contact = wx4.getContact();
                Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Contact contact2 = wx4.getContact();
                    Boolean valueOf2 = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                    if (valueOf2 != null) {
                        boolean booleanValue = valueOf2.booleanValue();
                        Contact contact3 = wx4.getContact();
                        Boolean valueOf3 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                        if (valueOf3 != null) {
                            a2.a(intValue, booleanValue, valueOf3.booleanValue());
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            NotificationAllowCallsAndMessagesFragment a3 = this.a.j;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, NotificationAllowCallsAndMessagesFragment.x.a());
            }
        }

        @DexIgnore
        public void a() {
            NotificationContactsAndAppsAssignedFragment.b(this.a).h();
        }
    }
}
