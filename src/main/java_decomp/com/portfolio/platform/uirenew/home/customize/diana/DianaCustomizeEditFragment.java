package com.portfolio.platform.uirenew.home.customize.diana;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.cd6;
import com.fossil.f94;
import com.fossil.hm4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lk4;
import com.fossil.lx5;
import com.fossil.m35;
import com.fossil.o35;
import com.fossil.q35;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sq4;
import com.fossil.tq4;
import com.fossil.u35;
import com.fossil.uz5;
import com.fossil.v5;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xm4;
import com.fossil.y04;
import com.fossil.y35;
import com.fossil.z35;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeEditFragment extends BasePermissionFragment implements z35, View.OnClickListener, CustomizeWidget.c, AlertDialogFragment.g, hm4.b {
    @DexIgnore
    public y35 g;
    @DexIgnore
    public ax5<f94> h;
    @DexIgnore
    public /* final */ ArrayList<Fragment> i; // = new ArrayList<>();
    @DexIgnore
    public ComplicationsFragment j;
    @DexIgnore
    public WatchAppsFragment o;
    @DexIgnore
    public CustomizeThemeFragment p;
    @DexIgnore
    public int q; // = 1;
    @DexIgnore
    public Integer r;
    @DexIgnore
    public ValueAnimator s;
    @DexIgnore
    public ComplicationsPresenter t;
    @DexIgnore
    public WatchAppsPresenter u;
    @DexIgnore
    public CustomizeThemePresenter v;
    @DexIgnore
    public w04 w;
    @DexIgnore
    public DianaCustomizeViewModel x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public b(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                wg6.a((Object) activity, "it");
                int intExtra = activity.getIntent().getIntExtra("KEY_CUSTOMIZE_TAB", 1);
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_COMPLICATION_POS_SELECTED");
                String stringExtra3 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
                wg6.a((Object) stringExtra, "presetId");
                wg6.a((Object) stringExtra2, "complicationPos");
                aVar.a(activity, stringExtra, intExtra, stringExtra2, stringExtra3);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onTransitionStart(Transition transition) {
            ViewPropertyAnimator duration;
            ViewPropertyAnimator duration2;
            ViewPropertyAnimator duration3;
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            f94 f94 = (f94) DianaCustomizeEditFragment.a(this.a).a();
            if (f94 != null) {
                tq4 tq4 = tq4.a;
                Object a2 = DianaCustomizeEditFragment.a(this.a).a();
                if (a2 != null) {
                    CardView cardView = ((f94) a2).s;
                    wg6.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    tq4.a((View) cardView);
                    ViewPropertyAnimator animate = f94.F.animate();
                    if (!(animate == null || (duration3 = animate.setDuration(500)) == null)) {
                        duration3.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate2 = f94.E.animate();
                    if (!(animate2 == null || (duration2 = animate2.setDuration(500)) == null)) {
                        duration2.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate3 = f94.D.animate();
                    if (animate3 != null && (duration = animate3.setDuration(500)) != null) {
                        duration.alpha(1.0f);
                        return;
                    }
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ f94 a;

        @DexIgnore
        public c(f94 f94) {
            this.a = f94;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            v5 v5Var = new v5();
            v5Var.c(this.a.z);
            wg6.a((Object) valueAnimator, ServerSetting.VALUE);
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                v5Var.a(2131362467, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    v5Var.a(2131362469, ((Float) animatedValue2).floatValue() + ((float) 1));
                    v5Var.a(this.a.z);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Float");
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ f94 a;

        @DexIgnore
        public d(f94 f94) {
            this.a = f94;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            v5 v5Var = new v5();
            v5Var.c(this.a.z);
            wg6.a((Object) valueAnimator, ServerSetting.VALUE);
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                v5Var.a(2131362467, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    v5Var.a(2131362469, ((Float) animatedValue2).floatValue() - ((float) 1));
                    v5Var.a(this.a.z);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Float");
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ f94 a;

        @DexIgnore
        public e(f94 f94) {
            this.a = f94;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            v5 v5Var = new v5();
            v5Var.c(this.a.z);
            wg6.a((Object) valueAnimator, ServerSetting.VALUE);
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                v5Var.a(2131362467, ((Float) animatedValue).floatValue());
                v5Var.a(this.a.z);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ f94 a;

        @DexIgnore
        public f(f94 f94) {
            this.a = f94;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            v5 v5Var = new v5();
            v5Var.c(this.a.z);
            wg6.a((Object) valueAnimator, ServerSetting.VALUE);
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                v5Var.a(2131362467, ((Float) animatedValue).floatValue());
                v5Var.a(this.a.z);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public g(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(false, "top", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(false, "top", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(false, str, "top");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(false, "top", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "top");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public h(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(false, "left", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(false, "left", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(false, str, "left");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(false, "left", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "left");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public i(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(false, "right", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(false, "right", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(false, str, "right");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(false, "right", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "right");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public j(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(false, "bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(false, "bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(false, str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(false, "bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "bottom");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public k(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(true, "top", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(true, "top", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(true, str, "top");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(true, "top", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "top");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public l(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(true, "middle", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(true, "middle", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(true, str, "middle");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(true, "middle", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "middle");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public m(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a(true, "bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.a(true, "bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.c(true, str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.b(true, "bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "bottom");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public final void Z(String str) {
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.N.setSelectedWc(false);
                            a2.K.setSelectedWc(true);
                            a2.M.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals("top")) {
                            a2.N.setSelectedWc(true);
                            a2.K.setSelectedWc(false);
                            a2.M.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals("left")) {
                            a2.N.setSelectedWc(false);
                            a2.K.setSelectedWc(false);
                            a2.M.setSelectedWc(true);
                            a2.L.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.N.setSelectedWc(false);
                            a2.K.setSelectedWc(false);
                            a2.M.setSelectedWc(false);
                            a2.L.setSelectedWc(true);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view) {
    }

    @DexIgnore
    public final void a0(String str) {
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.J.setSelectedWc(true);
                            a2.I.setSelectedWc(false);
                            a2.H.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.J.setSelectedWc(false);
                        a2.I.setSelectedWc(true);
                        a2.H.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.J.setSelectedWc(false);
                    a2.I.setSelectedWc(false);
                    a2.H.setSelectedWc(true);
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(View view) {
    }

    @DexIgnore
    public void c(View view) {
    }

    @DexIgnore
    public void d(View view) {
    }

    @DexIgnore
    public void d(String str) {
        wg6.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.B;
            wg6.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void e(boolean z) {
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                Object r4 = a2.t;
                wg6.a((Object) r4, "it.ftvSetToWatch");
                r4.setEnabled(true);
                Object r42 = a2.t;
                wg6.a((Object) r42, "it.ftvSetToWatch");
                r42.setClickable(true);
                a2.t.setBackgroundResource(2131231304);
                return;
            }
            Object r43 = a2.t;
            wg6.a((Object) r43, "it.ftvSetToWatch");
            r43.setClickable(false);
            Object r44 = a2.t;
            wg6.a((Object) r44, "it.ftvSetToWatch");
            r44.setEnabled(false);
            a2.t.setBackgroundResource(2131231305);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(boolean z) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (z) {
                activity.setResult(-1);
            } else {
                activity.setResult(0);
            }
            activity.finishAfterTransition();
        }
    }

    @DexIgnore
    public void g(int i2) {
        int i3 = i2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "initTab - initTab=" + i3 + " - mPreTab=" + this.r);
        this.q = i3;
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                v5 v5Var = new v5();
                v5Var.c(a2.z);
                if (i3 == 0) {
                    Integer num = this.r;
                    if (num != null && num.intValue() == 1) {
                        y35 y35 = this.g;
                        if (y35 != null) {
                            y35.i();
                            a2.v.setImageBitmap(lk4.a((View) a2.q));
                            this.s = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
                            ValueAnimator valueAnimator = this.s;
                            if (valueAnimator != null) {
                                valueAnimator.addUpdateListener(new c(a2));
                            }
                            ValueAnimator valueAnimator2 = this.s;
                            if (valueAnimator2 != null) {
                                valueAnimator2.start();
                            }
                            this.r = null;
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    } else {
                        v5Var.a(2131362467, 1.0f);
                        v5Var.a(a2.z);
                    }
                    ConstraintLayout constraintLayout = a2.r;
                    wg6.a((Object) constraintLayout, "it.clWatchApps");
                    constraintLayout.setVisibility(4);
                    View view = a2.x;
                    wg6.a((Object) view, "it.lineCenter");
                    view.setAlpha(0.0f);
                    View view2 = a2.w;
                    wg6.a((Object) view2, "it.lineBottom");
                    view2.setAlpha(0.0f);
                    View view3 = a2.y;
                    wg6.a((Object) view3, "it.lineTop");
                    view3.setAlpha(0.0f);
                } else if (i3 == 1) {
                    Integer num2 = this.r;
                    if (num2 != null && num2.intValue() == 0) {
                        a2.v.setImageBitmap(lk4.a((View) a2.q));
                        this.s = ValueAnimator.ofFloat(new float[]{2.0f, 1.0f});
                        ValueAnimator valueAnimator3 = this.s;
                        if (valueAnimator3 != null) {
                            valueAnimator3.addUpdateListener(new d(a2));
                        }
                        ValueAnimator valueAnimator4 = this.s;
                        if (valueAnimator4 != null) {
                            valueAnimator4.start();
                        }
                        this.r = null;
                    } else if (num2 != null && num2.intValue() == 2) {
                        this.s = ValueAnimator.ofFloat(new float[]{0.25f, 1.0f});
                        ValueAnimator valueAnimator5 = this.s;
                        if (valueAnimator5 != null) {
                            valueAnimator5.addUpdateListener(new e(a2));
                        }
                        ValueAnimator valueAnimator6 = this.s;
                        if (valueAnimator6 != null) {
                            valueAnimator6.start();
                        }
                        this.r = null;
                    } else {
                        v5Var.a(2131362467, 1.0f);
                        v5Var.a(a2.z);
                    }
                    ConstraintLayout constraintLayout2 = a2.r;
                    wg6.a((Object) constraintLayout2, "it.clWatchApps");
                    constraintLayout2.setVisibility(4);
                    View view4 = a2.x;
                    wg6.a((Object) view4, "it.lineCenter");
                    view4.setAlpha(0.0f);
                    View view5 = a2.w;
                    wg6.a((Object) view5, "it.lineBottom");
                    view5.setAlpha(0.0f);
                    View view6 = a2.y;
                    wg6.a((Object) view6, "it.lineTop");
                    view6.setAlpha(0.0f);
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.x;
                    if (dianaCustomizeViewModel != null) {
                        String str = (String) dianaCustomizeViewModel.f().a();
                        if (str != null) {
                            wg6.a((Object) str, "pos");
                            Z(str);
                        }
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                } else if (i3 == 2) {
                    Integer num3 = this.r;
                    if (num3 != null && num3.intValue() == 1) {
                        v5Var.a(2131362469, 0.0f);
                        v5Var.a(a2.z);
                        this.s = ValueAnimator.ofFloat(new float[]{1.0f, 0.25f});
                        ValueAnimator valueAnimator7 = this.s;
                        if (valueAnimator7 != null) {
                            valueAnimator7.addUpdateListener(new f(a2));
                        }
                        ValueAnimator valueAnimator8 = this.s;
                        if (valueAnimator8 != null) {
                            valueAnimator8.start();
                        }
                        this.r = null;
                    } else {
                        v5Var.a(2131362467, 0.25f);
                        v5Var.a(a2.z);
                    }
                    ConstraintLayout constraintLayout3 = a2.r;
                    wg6.a((Object) constraintLayout3, "it.clWatchApps");
                    constraintLayout3.setVisibility(0);
                    View view7 = a2.x;
                    wg6.a((Object) view7, "it.lineCenter");
                    view7.setAlpha(1.0f);
                    View view8 = a2.w;
                    wg6.a((Object) view8, "it.lineBottom");
                    view8.setAlpha(1.0f);
                    View view9 = a2.y;
                    wg6.a((Object) view9, "it.lineTop");
                    view9.setAlpha(1.0f);
                    DianaCustomizeViewModel dianaCustomizeViewModel2 = this.x;
                    if (dianaCustomizeViewModel2 != null) {
                        String str2 = (String) dianaCustomizeViewModel2.i().a();
                        if (str2 != null) {
                            wg6.a((Object) str2, "pos");
                            a0(str2);
                        }
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                }
                r(i2);
                y35 y352 = this.g;
                if (y352 != null) {
                    y352.a(i3);
                    ViewPager2 viewPager2 = a2.A;
                    wg6.a((Object) viewPager2, "it.rvPreset");
                    viewPager2.setCurrentItem(i3);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public boolean i1() {
        y35 y35 = this.g;
        if (y35 != null) {
            y35.h();
            return false;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void j1() {
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                a2.N.g();
                a2.L.g();
                a2.K.g();
                a2.M.g();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void k1() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.j = getChildFragmentManager().b("ComplicationsFragment");
        this.o = getChildFragmentManager().b("WatchAppsFragment");
        this.p = getChildFragmentManager().b("CustomizeThemeFragment");
        if (this.j == null) {
            this.j = new ComplicationsFragment();
        }
        if (this.o == null) {
            this.o = new WatchAppsFragment();
        }
        if (this.p == null) {
            this.p = new CustomizeThemeFragment();
        }
        CustomizeThemeFragment customizeThemeFragment = this.p;
        if (customizeThemeFragment != null) {
            this.i.add(customizeThemeFragment);
        }
        ComplicationsFragment complicationsFragment = this.j;
        if (complicationsFragment != null) {
            this.i.add(complicationsFragment);
        }
        WatchAppsFragment watchAppsFragment = this.o;
        if (watchAppsFragment != null) {
            this.i.add(watchAppsFragment);
        }
        y04 g2 = PortfolioApp.get.instance().g();
        ComplicationsFragment complicationsFragment2 = this.j;
        if (complicationsFragment2 != null) {
            WatchAppsFragment watchAppsFragment2 = this.o;
            if (watchAppsFragment2 != null) {
                CustomizeThemeFragment customizeThemeFragment2 = this.p;
                if (customizeThemeFragment2 != null) {
                    g2.a(new q35(complicationsFragment2, watchAppsFragment2, customizeThemeFragment2)).a(this);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeContract.View");
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsContract.View");
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsContract.View");
    }

    @DexIgnore
    public final void l1() {
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.N;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", "top");
                wg6.a((Object) putExtra, "Intent().putExtra(Custom\u2026plicationAppPos.TOP_FACE)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_COMPLICATION", putExtra, new u35(new g(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget2 = a2.M;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "left");
                wg6.a((Object) putExtra2, "Intent().putExtra(Custom\u2026licationAppPos.LEFT_FACE)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_COMPLICATION", putExtra2, new u35(new h(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget3 = a2.L;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "right");
                wg6.a((Object) putExtra3, "Intent().putExtra(Custom\u2026icationAppPos.RIGHT_FACE)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_COMPLICATION", putExtra3, new u35(new i(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget4 = a2.K;
                Intent putExtra4 = new Intent().putExtra("KEY_POSITION", "bottom");
                wg6.a((Object) putExtra4, "Intent().putExtra(Custom\u2026cationAppPos.BOTTOM_FACE)");
                CustomizeWidget.a(customizeWidget4, "SWAP_PRESET_COMPLICATION", putExtra4, new u35(new j(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget5 = a2.J;
                Intent putExtra5 = new Intent().putExtra("KEY_POSITION", "top");
                wg6.a((Object) putExtra5, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget5, "SWAP_PRESET_WATCH_APP", putExtra5, new u35(new k(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget6 = a2.I;
                Intent putExtra6 = new Intent().putExtra("KEY_POSITION", "middle");
                wg6.a((Object) putExtra6, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget6, "SWAP_PRESET_WATCH_APP", putExtra6, new u35(new l(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget7 = a2.H;
                Intent putExtra7 = new Intent().putExtra("KEY_POSITION", "bottom");
                wg6.a((Object) putExtra7, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget7, "SWAP_PRESET_WATCH_APP", putExtra7, new u35(new m(this)), (CustomizeWidget.b) null, 8, (Object) null);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void m() {
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886580);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        X(a2);
    }

    @DexIgnore
    public void n() {
        a();
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        DianaCustomizeEditFragment.super.onActivityCreated(bundle);
        DianaCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = activity;
            w04 w04 = this.w;
            if (w04 != null) {
                DianaCustomizeViewModel a2 = vd.a(dianaCustomizeEditActivity, w04).a(DianaCustomizeViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.x = a2;
                y35 y35 = this.g;
                if (y35 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.x;
                    if (dianaCustomizeViewModel != null) {
                        y35.a(dianaCustomizeViewModel);
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        DianaCustomizeEditFragment.super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && xm4.d.a(getContext(), 1)) {
            y35 y35 = this.g;
            if (y35 != null) {
                y35.a(false);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            switch (view.getId()) {
                case 2131362426:
                    y35 y35 = this.g;
                    if (y35 != null) {
                        y35.a(true);
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363105:
                    y35 y352 = this.g;
                    if (y352 != null) {
                        y352.h();
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363328:
                    y35 y353 = this.g;
                    if (y353 != null) {
                        y353.b("bottom");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363329:
                    y35 y354 = this.g;
                    if (y354 != null) {
                        y354.b("middle");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363330:
                    y35 y355 = this.g;
                    if (y355 != null) {
                        y355.b("top");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363331:
                    y35 y356 = this.g;
                    if (y356 != null) {
                        y356.a("bottom");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363335:
                    y35 y357 = this.g;
                    if (y357 != null) {
                        y357.a("right");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363338:
                    y35 y358 = this.g;
                    if (y358 != null) {
                        y358.a("left");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                case 2131363339:
                    y35 y359 = this.g;
                    if (y359 != null) {
                        y359.a("top");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        f94 a2 = kb.a(layoutInflater, 2131558543, viewGroup, false, e1());
        k1();
        this.h = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        DianaCustomizeEditFragment.super.onPause();
        y35 y35 = this.g;
        if (y35 != null) {
            y35.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        DianaCustomizeEditFragment.super.onResume();
        y35 y35 = this.g;
        if (y35 != null) {
            y35.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v9, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v10, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v11, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v12, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v13, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v14, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v15, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v16, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            wg6.a((Object) window, "activity.window");
            window.setEnterTransition(tq4.a.a());
            Window window2 = activity.getWindow();
            wg6.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(tq4.a.a(PortfolioApp.get.instance()));
            Intent intent = activity.getIntent();
            wg6.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new sq4(intent, PortfolioApp.get.instance()));
            a(activity);
            postponeEnterTransition();
        }
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                String b3 = ThemeManager.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2)) {
                    a2.s.setBackgroundColor(Color.parseColor(b2));
                }
                if (!TextUtils.isEmpty(b3)) {
                    a2.G.setBackgroundColor(Color.parseColor(b3));
                }
                a2.N.a(this);
                a2.K.a(this);
                a2.M.a(this);
                a2.L.a(this);
                a2.N.setOnClickListener(this);
                a2.K.setOnClickListener(this);
                a2.M.setOnClickListener(this);
                a2.L.setOnClickListener(this);
                a2.J.setOnClickListener(this);
                a2.I.setOnClickListener(this);
                a2.H.setOnClickListener(this);
                a2.B.setOnClickListener(this);
                a2.t.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.A;
                wg6.a((Object) viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new uz5(getChildFragmentManager(), this.i));
                if (a2.A.getChildAt(0) != null) {
                    RecyclerView childAt = a2.A.getChildAt(0);
                    if (childAt != null) {
                        childAt.setItemViewCacheSize(2);
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.A;
                wg6.a((Object) viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                hm4 hm4 = new hm4();
                wg6.a((Object) a2, "it");
                hm4.a(a2.d(), this);
            }
            l1();
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void p() {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886367));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886365));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886366));
        fVar.b(2131363105);
        fVar.b(2131363190);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public void q() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wg6.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.get.instance().e(), false, 4, (Object) null);
        }
    }

    @DexIgnore
    public final void r(int i2) {
        if (i2 == 1) {
            W("set_complication_view");
        } else if (i2 == 2) {
            W("set_watch_apps_view");
        }
    }

    @DexIgnore
    public void s(String str) {
        wg6.b(str, "complicationsPosition");
        if (this.q == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedComplication data=" + str);
            Z(str);
        }
    }

    @DexIgnore
    public void u(String str) {
        wg6.b(str, "buttonsPosition");
        if (this.q == 2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
            a0(str);
        }
    }

    @DexIgnore
    public final void b(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.J.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.I.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.H.e();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.K.e();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals("top")) {
                            a2.N.e();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals("left")) {
                            a2.M.e();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.L.e();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals("top")) {
                            a2.J.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.I.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.H.e();
                }
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                y35 y35 = this.g;
                if (y35 != null) {
                    y35.d(str, str2);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                switch (str2.hashCode()) {
                    case -1383228885:
                        if (str2.equals("bottom")) {
                            a2.K.e();
                            break;
                        }
                        break;
                    case 115029:
                        if (str2.equals("top")) {
                            a2.N.e();
                            break;
                        }
                        break;
                    case 3317767:
                        if (str2.equals("left")) {
                            a2.M.e();
                            break;
                        }
                        break;
                    case 108511772:
                        if (str2.equals("right")) {
                            a2.L.e();
                            break;
                        }
                        break;
                }
                a2.N.setDragMode(false);
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                a2.K.setDragMode(false);
                y35 y352 = this.g;
                if (y352 != null) {
                    y352.c(str, str2);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public static final /* synthetic */ ax5 a(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
        ax5<f94> ax5 = dianaCustomizeEditFragment.h;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(View view, Boolean bool) {
        a(view, bool.booleanValue());
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        wg6.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public void a(View view, boolean z) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "onHorizontalFling");
    }

    @DexIgnore
    public final boolean a(boolean z, String str, View view, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 == null) {
                return true;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.J.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.I.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.H.e();
                }
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                WatchAppsFragment watchAppsFragment = this.o;
                if (watchAppsFragment != null) {
                    watchAppsFragment.j1();
                }
                y35 y35 = this.g;
                if (y35 != null) {
                    y35.b(str2, str);
                    return true;
                }
                wg6.d("mPresenter");
                throw null;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.K.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals("top")) {
                        a2.N.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals("left")) {
                        a2.M.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.L.e();
                        break;
                    }
                    break;
            }
            a2.N.setDragMode(false);
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            a2.K.setDragMode(false);
            ComplicationsFragment complicationsFragment = this.j;
            if (complicationsFragment != null) {
                complicationsFragment.j1();
            }
            y35 y352 = this.g;
            if (y352 != null) {
                y352.a(str2, str);
                return true;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public final void c(boolean z, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.J.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.I.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.H.e();
                }
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                WatchAppsFragment watchAppsFragment = this.o;
                if (watchAppsFragment != null) {
                    watchAppsFragment.j1();
                    return;
                }
                return;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.K.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals("top")) {
                        a2.N.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals("left")) {
                        a2.M.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.L.e();
                        break;
                    }
                    break;
            }
            a2.N.setDragMode(false);
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            a2.K.setDragMode(false);
            ComplicationsFragment complicationsFragment = this.j;
            if (complicationsFragment != null) {
                complicationsFragment.j1();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.J.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.I.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.H.d();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.K.d();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals("top")) {
                            a2.N.d();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals("left")) {
                            a2.M.d();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.L.d();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(WatchFaceWrapper watchFaceWrapper) {
        Drawable leftComplication;
        Drawable bottomComplication;
        Drawable rightComplication;
        Drawable topComplication;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "initCustomizeThemeUI " + watchFaceWrapper);
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                a2.N.setBackgroundRes(2131230895);
                a2.L.setBackgroundRes(2131230895);
                a2.K.setBackgroundRes(2131230895);
                a2.M.setBackgroundRes(2131230895);
                if (!(watchFaceWrapper == null || (topComplication = watchFaceWrapper.getTopComplication()) == null)) {
                    a2.N.setBackgroundDrawableCus(topComplication);
                }
                if (!(watchFaceWrapper == null || (rightComplication = watchFaceWrapper.getRightComplication()) == null)) {
                    a2.L.setBackgroundDrawableCus(rightComplication);
                }
                if (!(watchFaceWrapper == null || (bottomComplication = watchFaceWrapper.getBottomComplication()) == null)) {
                    a2.K.setBackgroundDrawableCus(bottomComplication);
                }
                if (!(watchFaceWrapper == null || (leftComplication = watchFaceWrapper.getLeftComplication()) == null)) {
                    a2.M.setBackgroundDrawableCus(leftComplication);
                }
                a2.N.setSelectedWc(false);
                a2.K.setSelectedWc(false);
                a2.M.setSelectedWc(false);
                a2.L.setSelectedWc(false);
                startPostponedEnterTransition();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(WatchFaceWrapper watchFaceWrapper) {
        if (this.q == 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedCustomizeTheme watchFaceWrapper=" + watchFaceWrapper);
            c(watchFaceWrapper);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(m35 m35, DianaComplicationRingStyle dianaComplicationRingStyle) {
        Object obj;
        Object obj2;
        Object obj3;
        String str;
        String str2;
        String str3;
        String str4;
        WatchFaceWrapper.MetaData leftMetaData;
        WatchFaceWrapper.MetaData bottomMetaData;
        WatchFaceWrapper.MetaData rightMetaData;
        WatchFaceWrapper.MetaData topMetaData;
        WatchFaceWrapper.MetaData leftMetaData2;
        WatchFaceWrapper.MetaData bottomMetaData2;
        WatchFaceWrapper.MetaData rightMetaData2;
        WatchFaceWrapper.MetaData topMetaData2;
        Drawable background;
        MetaData metaData;
        MetaData metaData2;
        MetaData metaData3;
        MetaData metaData4;
        wg6.b(m35, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("showCurrentPreset watchFaceId ");
        WatchFaceWrapper f2 = m35.f();
        sb.append(f2 != null ? f2.getId() : null);
        sb.append(" complications=");
        sb.append(m35.a());
        sb.append(" watchApps=");
        sb.append(m35.e());
        sb.append(" defaultRingStyle ");
        sb.append(dianaComplicationRingStyle);
        local.d("DianaCustomizeEditFragment", sb.toString());
        ax5<f94> ax5 = this.h;
        if (ax5 != null) {
            f94 a2 = ax5.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(m35.a());
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(m35.e());
                Object r5 = a2.C;
                wg6.a((Object) r5, "it.tvPresetName");
                r5.setText(m35.d());
                WatchFaceWrapper f3 = m35.f();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    o35 o35 = (o35) it.next();
                    String c2 = o35.c();
                    if (c2 != null) {
                        switch (c2.hashCode()) {
                            case -1383228885:
                                if (!c2.equals("bottom")) {
                                    break;
                                } else {
                                    a2.K.b(o35.a());
                                    a2.K.setBottomContent(o35.d());
                                    a2.K.h();
                                    CustomizeWidget customizeWidget = a2.K;
                                    wg6.a((Object) customizeWidget, "it.wcBottom");
                                    a(customizeWidget, o35.a());
                                    break;
                                }
                            case 115029:
                                if (!c2.equals("top")) {
                                    break;
                                } else {
                                    a2.N.b(o35.a());
                                    a2.N.setBottomContent(o35.d());
                                    a2.N.h();
                                    CustomizeWidget customizeWidget2 = a2.N;
                                    wg6.a((Object) customizeWidget2, "it.wcTop");
                                    a(customizeWidget2, o35.a());
                                    break;
                                }
                            case 3317767:
                                if (!c2.equals("left")) {
                                    break;
                                } else {
                                    a2.M.b(o35.a());
                                    a2.M.setBottomContent(o35.d());
                                    a2.M.h();
                                    CustomizeWidget customizeWidget3 = a2.M;
                                    wg6.a((Object) customizeWidget3, "it.wcStart");
                                    a(customizeWidget3, o35.a());
                                    break;
                                }
                            case 108511772:
                                if (!c2.equals("right")) {
                                    break;
                                } else {
                                    a2.L.b(o35.a());
                                    a2.L.setBottomContent(o35.d());
                                    a2.L.h();
                                    CustomizeWidget customizeWidget4 = a2.L;
                                    wg6.a((Object) customizeWidget4, "it.wcEnd");
                                    a(customizeWidget4, o35.a());
                                    break;
                                }
                        }
                    }
                }
                Iterator it2 = arrayList2.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        obj = it2.next();
                        if (wg6.a((Object) ((o35) obj).c(), (Object) "top")) {
                        }
                    } else {
                        obj = null;
                    }
                }
                o35 o352 = (o35) obj;
                Iterator it3 = arrayList2.iterator();
                while (true) {
                    if (it3.hasNext()) {
                        obj2 = it3.next();
                        if (wg6.a((Object) ((o35) obj2).c(), (Object) "middle")) {
                        }
                    } else {
                        obj2 = null;
                    }
                }
                o35 o353 = (o35) obj2;
                Iterator it4 = arrayList2.iterator();
                while (true) {
                    if (it4.hasNext()) {
                        obj3 = it4.next();
                        if (wg6.a((Object) ((o35) obj3).c(), (Object) "bottom")) {
                        }
                    } else {
                        obj3 = null;
                    }
                }
                CustomizeWidget customizeWidget5 = a2.J;
                wg6.a((Object) customizeWidget5, "it.waTop");
                FlexibleTextView flexibleTextView = a2.F;
                wg6.a((Object) flexibleTextView, "it.tvWaTop");
                a(customizeWidget5, flexibleTextView, o352);
                CustomizeWidget customizeWidget6 = a2.I;
                wg6.a((Object) customizeWidget6, "it.waMiddle");
                FlexibleTextView flexibleTextView2 = a2.E;
                wg6.a((Object) flexibleTextView2, "it.tvWaMiddle");
                a(customizeWidget6, flexibleTextView2, o353);
                CustomizeWidget customizeWidget7 = a2.H;
                wg6.a((Object) customizeWidget7, "it.waBottom");
                FlexibleTextView flexibleTextView3 = a2.D;
                wg6.a((Object) flexibleTextView3, "it.tvWaBottom");
                a(customizeWidget7, flexibleTextView3, (o35) obj3);
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "showCurrentPreset watchFaceWrapper " + f3);
                if (dianaComplicationRingStyle == null || (metaData4 = dianaComplicationRingStyle.getMetaData()) == null || (str = metaData4.getSelectedBackgroundColor()) == null) {
                    str = "#242424";
                }
                int parseColor = Color.parseColor(str);
                if (dianaComplicationRingStyle == null || (metaData3 = dianaComplicationRingStyle.getMetaData()) == null || (str2 = metaData3.getUnselectedBackgroundColor()) == null) {
                    str2 = "#242424";
                }
                int parseColor2 = Color.parseColor(str2);
                if (dianaComplicationRingStyle == null || (metaData2 = dianaComplicationRingStyle.getMetaData()) == null || (str3 = metaData2.getSelectedForegroundColor()) == null) {
                    str3 = "#242424";
                }
                int parseColor3 = Color.parseColor(str3);
                if (dianaComplicationRingStyle == null || (metaData = dianaComplicationRingStyle.getMetaData()) == null || (str4 = metaData.getUnselectedForegroundColor()) == null) {
                    str4 = "#242424";
                }
                int parseColor4 = Color.parseColor(str4);
                if (!(f3 == null || (background = f3.getBackground()) == null)) {
                    a2.u.setImageDrawable(background);
                    cd6 cd6 = cd6.a;
                }
                j1();
                if (this.q == 0) {
                    if (f3 == null || (topMetaData2 = f3.getTopMetaData()) == null) {
                        a2.N.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                        cd6 cd62 = cd6.a;
                    } else {
                        a2.N.a((Integer) null, (Integer) null, topMetaData2.getUnselectedForegroundColor(), (Integer) null);
                        cd6 cd63 = cd6.a;
                    }
                    if (f3 == null || (rightMetaData2 = f3.getRightMetaData()) == null) {
                        a2.L.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                        cd6 cd64 = cd6.a;
                    } else {
                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "set right watch face unselectedForegroundColor " + rightMetaData2.getUnselectedForegroundColor());
                        a2.L.a((Integer) null, (Integer) null, rightMetaData2.getUnselectedForegroundColor(), (Integer) null);
                        cd6 cd65 = cd6.a;
                    }
                    if (f3 == null || (bottomMetaData2 = f3.getBottomMetaData()) == null) {
                        a2.K.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                        cd6 cd66 = cd6.a;
                    } else {
                        a2.K.a((Integer) null, (Integer) null, bottomMetaData2.getUnselectedForegroundColor(), (Integer) null);
                        cd6 cd67 = cd6.a;
                    }
                    if (f3 == null || (leftMetaData2 = f3.getLeftMetaData()) == null) {
                        a2.M.a((Integer) null, (Integer) null, Integer.valueOf(parseColor4), (Integer) null);
                        cd6 cd68 = cd6.a;
                    } else {
                        a2.M.a((Integer) null, (Integer) null, leftMetaData2.getUnselectedForegroundColor(), (Integer) null);
                        cd6 cd69 = cd6.a;
                    }
                    c(f3);
                } else {
                    if (f3 == null || (topMetaData = f3.getTopMetaData()) == null) {
                        a2.N.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                        cd6 cd610 = cd6.a;
                    } else {
                        a2.N.a(topMetaData.getSelectedForegroundColor(), topMetaData.getSelectedBackgroundColor(), topMetaData.getUnselectedForegroundColor(), topMetaData.getUnselectedBackgroundColor());
                        cd6 cd611 = cd6.a;
                    }
                    if (f3 == null || (rightMetaData = f3.getRightMetaData()) == null) {
                        a2.L.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                        cd6 cd612 = cd6.a;
                    } else {
                        a2.L.a(rightMetaData.getSelectedForegroundColor(), rightMetaData.getSelectedBackgroundColor(), rightMetaData.getUnselectedForegroundColor(), rightMetaData.getUnselectedBackgroundColor());
                        cd6 cd613 = cd6.a;
                    }
                    if (f3 == null || (bottomMetaData = f3.getBottomMetaData()) == null) {
                        a2.K.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                        cd6 cd614 = cd6.a;
                    } else {
                        a2.K.a(bottomMetaData.getSelectedForegroundColor(), bottomMetaData.getSelectedBackgroundColor(), bottomMetaData.getUnselectedForegroundColor(), bottomMetaData.getUnselectedBackgroundColor());
                        cd6 cd615 = cd6.a;
                    }
                    if (f3 == null || (leftMetaData = f3.getLeftMetaData()) == null) {
                        a2.M.a(Integer.valueOf(parseColor3), Integer.valueOf(parseColor), Integer.valueOf(parseColor4), Integer.valueOf(parseColor2));
                        cd6 cd616 = cd6.a;
                    } else {
                        a2.M.a(leftMetaData.getSelectedForegroundColor(), leftMetaData.getSelectedBackgroundColor(), leftMetaData.getUnselectedForegroundColor(), leftMetaData.getUnselectedBackgroundColor());
                        cd6 cd617 = cd6.a;
                    }
                }
                cd6 cd618 = cd6.a;
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(CustomizeWidget customizeWidget, FlexibleTextView r3, o35 o35) {
        if (o35 != null) {
            r3.setText(o35.b());
            customizeWidget.d(o35.a());
            customizeWidget.h();
            b(customizeWidget, o35.a());
            return;
        }
        r3.setText("");
        customizeWidget.d("empty");
        customizeWidget.h();
        b(customizeWidget, "empty");
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(y35 y35) {
        wg6.b(y35, "presenter");
        this.g = y35;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(String str, String str2, String str3, boolean z) {
        wg6.b(str, "message");
        wg6.b(str2, "id");
        wg6.b(str3, "pos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            bundle.putBoolean("TO_COMPLICATION", z);
            AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
            fVar.a(2131363129, str);
            fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886360));
            fVar.b(2131363190);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode == 291193711 && str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING") && i2 == 2131363190 && intent != null) {
                    String stringExtra = intent.getStringExtra("TO_POS");
                    String stringExtra2 = intent.getStringExtra("TO_ID");
                    boolean booleanExtra = intent.getBooleanExtra("TO_COMPLICATION", true);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
                    if (this.q == 1) {
                        if (booleanExtra) {
                            y35 y35 = this.g;
                            if (y35 != null) {
                                wg6.a((Object) stringExtra, "toPos");
                                y35.a(stringExtra);
                                return;
                            }
                            wg6.d("mPresenter");
                            throw null;
                        }
                    } else if (!booleanExtra) {
                        y35 y352 = this.g;
                        if (y352 != null) {
                            wg6.a((Object) stringExtra, "toPos");
                            y352.b(stringExtra);
                            return;
                        }
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
            } else {
                if (i2 == 2131363105) {
                    f(false);
                } else if (i2 == 2131363190) {
                    y35 y353 = this.g;
                    if (y353 != null) {
                        y353.a(true);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION") && i2 == 2131363190 && intent != null) {
            String stringExtra3 = intent.getStringExtra("TO_ID");
            xm4 xm4 = xm4.d;
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) stringExtra3, "complicationId");
                xm4.a(context, stringExtra3);
                return;
            }
            wg6.a();
            throw null;
        }
    }
}
