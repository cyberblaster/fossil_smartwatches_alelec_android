package com.portfolio.platform.uirenew.home.dashboard.goaltracking;

import androidx.lifecycle.LiveData;
import com.fossil.ae5;
import com.fossil.af6;
import com.fossil.be5$b$a;
import com.fossil.be5$b$b;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gg6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardGoalTrackingPresenter extends zd5 implements vk4.a {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<GoalTrackingSummary> f;
    @DexIgnore
    public /* final */ ae5 g;
    @DexIgnore
    public /* final */ GoalTrackingRepository h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ GoalTrackingDao j;
    @DexIgnore
    public /* final */ GoalTrackingDatabase k;
    @DexIgnore
    public /* final */ u04 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1", f = "DashboardGoalTrackingPresenter.kt", l = {61}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardGoalTrackingPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardGoalTrackingPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            LiveData pagedList;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                be5$b$b be5_b_b = new be5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, be5_b_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                Date d = bk4.d(mFUser.getCreatedAt());
                DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter = this.this$0;
                GoalTrackingRepository e = dashboardGoalTrackingPresenter.h;
                GoalTrackingRepository e2 = this.this$0.h;
                GoalTrackingDao c = this.this$0.j;
                GoalTrackingDatabase d2 = this.this$0.k;
                wg6.a((Object) d, "createdDate");
                dashboardGoalTrackingPresenter.f = e.getSummariesPaging(e2, c, d2, d, this.this$0.l, this.this$0);
                Listing f = this.this$0.f;
                if (!(f == null || (pagedList = f.getPagedList()) == null)) {
                    ae5 k = this.this$0.k();
                    if (k != null) {
                        pagedList.a((DashboardGoalTrackingFragment) k, new be5$b$a(this));
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                    }
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DashboardGoalTrackingPresenter(ae5 ae5, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, u04 u04) {
        wg6.b(ae5, "mView");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(u04, "mAppExecutors");
        this.g = ae5;
        this.h = goalTrackingRepository;
        this.i = userRepository;
        this.j = goalTrackingDao;
        this.k = goalTrackingDatabase;
        this.l = u04;
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        LiveData<cf<GoalTrackingSummary>> pagedList;
        try {
            Listing<GoalTrackingSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                ae5 ae5 = this.g;
                if (ae5 != null) {
                    pagedList.a((DashboardGoalTrackingFragment) ae5);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("DashboardGoalTrackingPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        gg6<cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "retry all failed request");
        Listing<GoalTrackingSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }

    @DexIgnore
    public final ae5 k() {
        return this.g;
    }

    @DexIgnore
    public void l() {
        this.g.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "start");
        if (!bk4.t(this.e).booleanValue()) {
            this.e = new Date();
            Listing<GoalTrackingSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "stop");
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        wg6.b(gVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardGoalTrackingPresenter", "onStatusChange status=" + gVar);
        if (gVar.a()) {
            this.g.f();
        }
    }
}
