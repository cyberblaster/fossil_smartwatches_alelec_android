package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.cf;
import com.fossil.fi5;
import com.fossil.gi5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.ou4;
import com.fossil.qg6;
import com.fossil.r24;
import com.fossil.rc6;
import com.fossil.tk4;
import com.fossil.tz5;
import com.fossil.ut4;
import com.fossil.wg6;
import com.fossil.x94;
import com.fossil.yk4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleProgressBar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailFragment extends BaseFragment implements gi5, View.OnClickListener, ou4.b, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public ou4 g;
    @DexIgnore
    public ax5<x94> h;
    @DexIgnore
    public fi5 i;
    @DexIgnore
    public tz5 j;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ String w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final GoalTrackingDetailFragment a(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            GoalTrackingDetailFragment goalTrackingDetailFragment = new GoalTrackingDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            goalTrackingDetailFragment.setArguments(bundle);
            return goalTrackingDetailFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends tz5 {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailFragment e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerViewEmptySupport recyclerViewEmptySupport, LinearLayoutManager linearLayoutManager, GoalTrackingDetailFragment goalTrackingDetailFragment, LinearLayoutManager linearLayoutManager2, x94 x94) {
            super(linearLayoutManager);
            this.e = goalTrackingDetailFragment;
        }

        @DexIgnore
        public void a(int i) {
            fi5 a = this.e.i;
            if (a != null) {
                a.i();
            }
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    @DexIgnore
    public GoalTrackingDetailFragment() {
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        this.r = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("backgroundDashboard");
        Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("secondaryText");
        this.s = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("primaryText");
        this.t = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("nonBrandDisableCalendarDay");
        this.u = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.v = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
        String b8 = ThemeManager.l.a().b("nonBrandSurface");
        this.w = b8 == null ? "#FFFFFF" : b8;
    }

    @DexIgnore
    public void c(cf<GoalTrackingData> cfVar) {
        ou4 ou4;
        wg6.b(cfVar, "goalTrackingDataList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailData - goalTrackingDataList=" + cfVar);
        ax5<x94> ax5 = this.h;
        if (ax5 != null && ax5.a() != null && (ou4 = this.g) != null) {
            ou4.b(cfVar);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f() {
        tz5 tz5 = this.j;
        if (tz5 != null) {
            tz5.a();
        }
    }

    @DexIgnore
    public String h1() {
        return "GoalTrackingDetailFragment";
    }

    @DexIgnore
    public final void j1() {
        x94 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        ax5<x94> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayGoalChart = a2.t) != null) {
            overviewDayGoalChart.a("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r11v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void onClick(View view) {
        int i2;
        int i3;
        int i4;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("GoalTrackingDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131361881:
                    Boolean t2 = bk4.t(this.f);
                    wg6.a((Object) t2, "DateHelper.isToday(mDate)");
                    if (t2.booleanValue()) {
                        Calendar instance = Calendar.getInstance();
                        int i5 = instance.get(10);
                        int i6 = instance.get(12);
                        i2 = instance.get(9);
                        if (i5 == 0) {
                            i3 = i6;
                            i4 = 12;
                        } else {
                            i4 = i5;
                            i3 = i6;
                        }
                    } else {
                        i4 = 12;
                        i3 = 0;
                        i2 = 0;
                    }
                    lx5 lx5 = lx5.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    wg6.a((Object) childFragmentManager, "childFragmentManager");
                    String[] strArr = new String[2];
                    String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886102);
                    wg6.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    if (a2 != null) {
                        String upperCase = a2.toUpperCase();
                        wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        strArr[0] = upperCase;
                        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886104);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                        if (a3 != null) {
                            String upperCase2 = a3.toUpperCase();
                            wg6.a((Object) upperCase2, "(this as java.lang.String).toUpperCase()");
                            strArr[1] = upperCase2;
                            lx5.a(childFragmentManager, i4, i3, i2, strArr);
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                case 2131362542:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362543:
                    fi5 fi5 = this.i;
                    if (fi5 != null) {
                        fi5.k();
                        return;
                    }
                    return;
                case 2131362609:
                    fi5 fi52 = this.i;
                    if (fi52 != null) {
                        fi52.j();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        x94 a2;
        wg6.b(layoutInflater, "inflater");
        GoalTrackingDetailFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        x94 a3 = kb.a(layoutInflater, 2131558552, viewGroup, false, e1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.f = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.f = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        this.g = new ou4(this, new r24());
        wg6.a((Object) a3, "binding");
        a(a3);
        fi5 fi5 = this.i;
        if (fi5 != null) {
            fi5.b(this.f);
        }
        this.h = new ax5<>(this, a3);
        j1();
        ax5<x94> ax5 = this.h;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public void onDestroy() {
        GoalTrackingDetailFragment.super.onDestroy();
        fi5 fi5 = this.i;
        if (fi5 != null) {
            fi5.h();
        }
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        GoalTrackingDetailFragment.super.onPause();
        fi5 fi5 = this.i;
        if (fi5 != null) {
            fi5.g();
        }
    }

    @DexIgnore
    public void onResume() {
        GoalTrackingDetailFragment.super.onResume();
        j1();
        fi5 fi5 = this.i;
        if (fi5 != null) {
            fi5.c(this.f);
        }
        fi5 fi52 = this.i;
        if (fi52 != null) {
            fi52.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        fi5 fi5 = this.i;
        if (fi5 != null) {
            fi5.a(bundle);
        }
        GoalTrackingDetailFragment.super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r10v2, types: [com.portfolio.platform.view.FlexibleTextView, android.view.View, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v10, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v21, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public final void a(x94 x94) {
        x94.A.setOnClickListener(this);
        x94.B.setOnClickListener(this);
        x94.C.setOnClickListener(this);
        if (!TextUtils.isEmpty(this.w)) {
            int parseColor = Color.parseColor(this.w);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231027);
            if (drawable != null) {
                drawable.setTint(parseColor);
                x94.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        x94.q.setOnClickListener(this);
        this.o = ThemeManager.l.a().b("hybridGoalTrackingTab");
        this.p = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.q = ThemeManager.l.a().b("onHybridGoalTrackingTab");
        x94.r.setBackgroundColor(this.r);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        RecyclerViewEmptySupport recyclerViewEmptySupport = x94.G;
        wg6.a((Object) recyclerViewEmptySupport, "it");
        recyclerViewEmptySupport.setLayoutManager(linearLayoutManager);
        LinearLayoutManager layoutManager = recyclerViewEmptySupport.getLayoutManager();
        if (layoutManager != null) {
            this.j = new b(recyclerViewEmptySupport, layoutManager, this, linearLayoutManager, x94);
            recyclerViewEmptySupport.setAdapter(this.g);
            Object r10 = x94.H;
            wg6.a((Object) r10, "binding.tvNotFound");
            recyclerViewEmptySupport.setEmptyView(r10);
            tz5 tz5 = this.j;
            if (tz5 != null) {
                recyclerViewEmptySupport.addOnScrollListener(tz5);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v11, types: [java.io.Serializable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public void a(String str, int i2, Intent intent) {
        GoalTrackingData goalTrackingData;
        fi5 fi5;
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        Bundle bundle = null;
        if (hashCode != 193266439) {
            if (hashCode == 1983205541 && str.equals("GOAL_TRACKING_ADD") && i2 == 2131362204) {
                if (intent != null) {
                    bundle = intent.getSerializableExtra("EXTRA_NUMBER_PICKER_RESULTS");
                }
                if (bundle != null) {
                    HashMap hashMap = (HashMap) bundle;
                    Integer num = (Integer) hashMap.get(2131362748);
                    Integer num2 = (Integer) hashMap.get(2131362749);
                    Integer num3 = (Integer) hashMap.get(2131362751);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("GoalTrackingDetailFragment", "onDialogFragmentResult GOAL_TRACKING_ADD: hour=" + num + ", minute=" + num2 + ", suffix=" + num3);
                    Calendar instance = Calendar.getInstance();
                    wg6.a((Object) instance, "calendar");
                    instance.setTime(this.f);
                    if (num != null && num2 != null && num3 != null) {
                        if (num.intValue() == 12) {
                            num = 0;
                        }
                        instance.set(9, num3.intValue());
                        instance.set(10, num.intValue());
                        instance.set(12, num2.intValue());
                        Calendar instance2 = Calendar.getInstance();
                        wg6.a((Object) instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        wg6.a((Object) time, "Calendar.getInstance().time");
                        long time2 = time.getTime();
                        Date time3 = instance.getTime();
                        wg6.a((Object) time3, "calendar.time");
                        if (time2 > time3.getTime()) {
                            fi5 fi52 = this.i;
                            if (fi52 != null) {
                                Date time4 = instance.getTime();
                                wg6.a((Object) time4, "calendar.time");
                                fi52.a(time4);
                                return;
                            }
                            return;
                        }
                        FragmentManager fragmentManager = getFragmentManager();
                        if (fragmentManager != null) {
                            lx5 lx5 = lx5.c;
                            wg6.a((Object) fragmentManager, "this");
                            lx5.n(fragmentManager);
                            return;
                        }
                        return;
                    }
                    return;
                }
                throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Int, kotlin.Int>");
            }
        } else if (str.equals("GOAL_TRACKING_DELETE") && i2 == 2131363190) {
            if (intent != null) {
                bundle = intent.getExtras();
            }
            if (bundle != null && (goalTrackingData = (GoalTrackingData) bundle.getSerializable("GOAL_TRACKING_DELETE_BUNDLE")) != null && (fi5 = this.i) != null) {
                fi5.a(goalTrackingData);
            }
        }
    }

    @DexIgnore
    public void a(GoalTrackingData goalTrackingData) {
        wg6.b(goalTrackingData, "item");
        lx5 lx5 = lx5.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        lx5.a(childFragmentManager, goalTrackingData);
    }

    @DexIgnore
    public void a(fi5 fi5) {
        wg6.b(fi5, "presenter");
        this.i = fi5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        x94 a2;
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3);
        this.f = date;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        int i2 = instance.get(7);
        ax5<x94> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Object r2 = a2.w;
            wg6.a((Object) r2, "binding.ftvDayOfMonth");
            r2.setText(String.valueOf(instance.get(5)));
            if (z) {
                Object r6 = a2.B;
                wg6.a((Object) r6, "binding.ivBackDate");
                r6.setVisibility(4);
            } else {
                Object r62 = a2.B;
                wg6.a((Object) r62, "binding.ivBackDate");
                r62.setVisibility(0);
            }
            if (z2 || z3) {
                Object r8 = a2.C;
                wg6.a((Object) r8, "binding.ivNextDate");
                r8.setVisibility(8);
                if (z2) {
                    Object r5 = a2.x;
                    wg6.a((Object) r5, "binding.ftvDayOfWeek");
                    r5.setText(jm4.a(getContext(), 2131886449));
                    return;
                }
                Object r63 = a2.x;
                wg6.a((Object) r63, "binding.ftvDayOfWeek");
                r63.setText(yk4.b.b(i2));
                return;
            }
            Object r7 = a2.C;
            wg6.a((Object) r7, "binding.ivNextDate");
            r7.setVisibility(0);
            Object r64 = a2.x;
            wg6.a((Object) r64, "binding.ftvDayOfWeek");
            r64.setText(yk4.b.b(i2));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r11v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v15, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v16, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v18, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v19, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v25, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v26, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v30, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v31, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v35, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v37, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v38, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v35, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v41, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v42, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v43, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v44, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v46, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v47, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void a(GoalTrackingSummary goalTrackingSummary) {
        x94 a2;
        int i2;
        int i3;
        GoalTrackingSummary goalTrackingSummary2 = goalTrackingSummary;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetail - goalTrackingSummary=" + goalTrackingSummary2);
        ax5<x94> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            wg6.a((Object) a2, "binding");
            View d = a2.d();
            wg6.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (goalTrackingSummary2 != null) {
                i2 = goalTrackingSummary.getTotalTracked();
                i3 = goalTrackingSummary.getGoalTarget();
            } else {
                i3 = 0;
                i2 = 0;
            }
            Object r6 = a2.v;
            wg6.a((Object) r6, "binding.ftvDailyValue");
            r6.setText(tk4.b((float) i2, 1));
            Object r62 = a2.u;
            wg6.a((Object) r62, "binding.ftvDailyUnit");
            r62.setText("/ " + i3 + " " + " " + jm4.a((Context) PortfolioApp.get.instance(), 2131886512));
            int i4 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.x.setTextColor(this.r);
                a2.w.setTextColor(this.r);
                a2.u.setTextColor(this.r);
                a2.v.setTextColor(this.r);
                Object r5 = a2.C;
                wg6.a((Object) r5, "binding.ivNextDate");
                r5.setSelected(true);
                Object r52 = a2.B;
                wg6.a((Object) r52, "binding.ivBackDate");
                r52.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                wg6.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                Object r4 = a2.x;
                wg6.a((Object) r4, "binding.ftvDayOfWeek");
                r4.setSelected(true);
                Object r42 = a2.w;
                wg6.a((Object) r42, "binding.ftvDayOfMonth");
                r42.setSelected(true);
                View view = a2.D;
                wg6.a((Object) view, "binding.line");
                view.setSelected(true);
                Object r43 = a2.v;
                wg6.a((Object) r43, "binding.ftvDailyValue");
                r43.setSelected(true);
                Object r44 = a2.u;
                wg6.a((Object) r44, "binding.ftvDailyUnit");
                r44.setSelected(true);
                Object r45 = a2.H;
                wg6.a((Object) r45, "binding.tvNotFound");
                r45.setVisibility(4);
                String str = this.q;
                if (str != null) {
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.w.setTextColor(Color.parseColor(str));
                    a2.v.setTextColor(Color.parseColor(str));
                    a2.u.setTextColor(Color.parseColor(str));
                    a2.D.setBackgroundColor(Color.parseColor(str));
                    a2.C.setColorFilter(Color.parseColor(str));
                    a2.B.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.o;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.w.setTextColor(this.t);
                a2.x.setTextColor(this.s);
                a2.u.setTextColor(this.v);
                View view2 = a2.D;
                wg6.a((Object) view2, "binding.line");
                view2.setSelected(false);
                Object r53 = a2.C;
                wg6.a((Object) r53, "binding.ivNextDate");
                r53.setSelected(false);
                Object r54 = a2.B;
                wg6.a((Object) r54, "binding.ivBackDate");
                r54.setSelected(false);
                a2.v.setTextColor(this.t);
                Object r46 = a2.H;
                wg6.a((Object) r46, "binding.tvNotFound");
                r46.setVisibility(4);
                int i5 = this.v;
                a2.D.setBackgroundColor(i5);
                a2.C.setColorFilter(i5);
                a2.B.setColorFilter(i5);
                String str3 = this.p;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.w.setTextColor(this.t);
                a2.x.setTextColor(this.s);
                a2.v.setTextColor(this.u);
                a2.u.setTextColor(this.u);
                Object r55 = a2.C;
                wg6.a((Object) r55, "binding.ivNextDate");
                r55.setSelected(false);
                Object r56 = a2.B;
                wg6.a((Object) r56, "binding.ivBackDate");
                r56.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                wg6.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                Object r47 = a2.x;
                wg6.a((Object) r47, "binding.ftvDayOfWeek");
                r47.setSelected(false);
                Object r48 = a2.w;
                wg6.a((Object) r48, "binding.ftvDayOfMonth");
                r48.setSelected(false);
                View view3 = a2.D;
                wg6.a((Object) view3, "binding.line");
                view3.setSelected(false);
                Object r49 = a2.v;
                wg6.a((Object) r49, "binding.ftvDailyValue");
                r49.setSelected(false);
                Object r410 = a2.u;
                wg6.a((Object) r410, "binding.ftvDailyUnit");
                r410.setSelected(false);
                Object r411 = a2.H;
                wg6.a((Object) r411, "binding.tvNotFound");
                r411.setVisibility(0);
                int i6 = this.v;
                a2.D.setBackgroundColor(i6);
                a2.C.setColorFilter(i6);
                a2.B.setColorFilter(i6);
                String str4 = this.p;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.E;
                wg6.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                Object r412 = a2.z;
                wg6.a((Object) r412, "binding.ftvProgressValue");
                r412.setText(jm4.a(context, 2131887067));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.E;
                wg6.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                Object r413 = a2.z;
                wg6.a((Object) r413, "binding.ftvProgressValue");
                r413.setText(i4 + "%");
            }
            Object r2 = a2.y;
            wg6.a((Object) r2, "binding.ftvGoalValue");
            nh6 nh6 = nh6.a;
            String a3 = jm4.a(context, 2131886514);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026age_Title__OfNumberTimes)");
            Object[] objArr = {Integer.valueOf(i3)};
            String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            r2.setText(format);
        }
    }

    @DexIgnore
    public void a(ut4 ut4, ArrayList<String> arrayList) {
        x94 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        wg6.b(ut4, "baseModel");
        wg6.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailChart - baseModel=" + ut4);
        ax5<x94> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayGoalChart = a2.t) != null) {
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) yk4.b.a(), false, 2, (Object) null);
            }
            overviewDayGoalChart.a(ut4);
        }
    }
}
