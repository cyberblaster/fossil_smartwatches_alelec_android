package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.dl4;
import com.fossil.gu4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.l75;
import com.fossil.lx5;
import com.fossil.m34;
import com.fossil.m75;
import com.fossil.nh6;
import com.fossil.o75;
import com.fossil.pw6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vd;
import com.fossil.ve4;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppsFragment extends BaseFragment implements m75, AlertDialogFragment.g {
    @DexIgnore
    public ax5<ve4> f;
    @DexIgnore
    public l75 g;
    @DexIgnore
    public gu4 h;
    @DexIgnore
    public m34 i;
    @DexIgnore
    public w04 j;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m34.b {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsFragment a;

        @DexIgnore
        public b(WatchAppsFragment watchAppsFragment) {
            this.a = watchAppsFragment;
        }

        @DexIgnore
        public void a(WatchApp watchApp) {
            wg6.b(watchApp, "watchApp");
            WatchAppsFragment.a(this.a).a(watchApp.getWatchappId());
        }

        @DexIgnore
        public void b(WatchApp watchApp) {
            wg6.b(watchApp, "watchApp");
            this.a.c(watchApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements gu4.c {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsFragment a;

        @DexIgnore
        public c(WatchAppsFragment watchAppsFragment) {
            this.a = watchAppsFragment;
        }

        @DexIgnore
        public void a() {
            WatchAppsFragment.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            wg6.b(category, "category");
            WatchAppsFragment.a(this.a).a(category);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsFragment a;

        @DexIgnore
        public d(WatchAppsFragment watchAppsFragment) {
            this.a = watchAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchAppsFragment.a(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsFragment a;

        @DexIgnore
        public e(WatchAppsFragment watchAppsFragment) {
            this.a = watchAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchAppsFragment.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsFragment a;

        @DexIgnore
        public f(WatchAppsFragment watchAppsFragment) {
            this.a = watchAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchAppsFragment.a(this.a).j();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ l75 a(WatchAppsFragment watchAppsFragment) {
        l75 l75 = watchAppsFragment.g;
        if (l75 != null) {
            return l75;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void D(String str) {
        wg6.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        lx5 lx5 = lx5.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager, "childFragmentManager");
                        lx5.z(childFragmentManager);
                        return;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        lx5 lx52 = lx5.c;
                        FragmentManager childFragmentManager2 = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager2, "childFragmentManager");
                        lx52.J(childFragmentManager2);
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        lx5 lx53 = lx5.c;
                        FragmentManager childFragmentManager3 = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager3, "childFragmentManager");
                        lx53.K(childFragmentManager3);
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        lx5 lx54 = lx5.c;
                        FragmentManager childFragmentManager4 = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager4, "childFragmentManager");
                        lx54.y(childFragmentManager4);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void H(String str) {
        wg6.b(str, "content");
        ax5<ve4> ax5 = this.f;
        if (ax5 != null) {
            ve4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.v;
                wg6.a((Object) r0, "it.tvWatchappsDetail");
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void O(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        WeatherSettingActivity.C.a(this, str);
    }

    @DexIgnore
    public void b(WatchApp watchApp) {
        if (watchApp != null) {
            m34 m34 = this.i;
            if (m34 != null) {
                m34.b(watchApp.getWatchappId());
                d(watchApp);
                return;
            }
            wg6.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(WatchApp watchApp) {
        String str;
        List<String> b2 = dl4.c.b(watchApp.getWatchappId());
        String str2 = InAppPermission.NOTIFICATION_ACCESS;
        if (b2.contains(str2)) {
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887129);
            wg6.a((Object) str, "LanguageHelper.getString\u2026ification_access_warning)");
        } else {
            nh6 nh6 = nh6.a;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886603);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026iresLocationSettingsToBe)");
            Object[] objArr = {jm4.a(PortfolioApp.get.instance(), watchApp.getNameKey(), watchApp.getName())};
            str = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) str, "java.lang.String.format(format, *args)");
            str2 = "LOCATION_PERMISSION_TAG";
        }
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.a(2131363129, str);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886931));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886376));
        fVar.a(2131363190);
        fVar.b(2131363105);
        fVar.b(2131363190);
        fVar.a(getChildFragmentManager(), str2);
    }

    @DexIgnore
    public void d(String str) {
        wg6.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.B;
            wg6.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e(String str) {
        wg6.b(str, "category");
        ax5<ve4> ax5 = this.f;
        if (ax5 != null) {
            ve4 a2 = ax5.a();
            if (a2 != null) {
                gu4 gu4 = this.h;
                if (gu4 != null) {
                    int a3 = gu4.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        gu4 gu42 = this.h;
                        if (gu42 != null) {
                            gu42.a(a3);
                            a2.r.smoothScrollToPosition(a3);
                            return;
                        }
                        wg6.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                wg6.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        wg6.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        xx5.a aVar = xx5.a;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            wg6.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        xx5.a.a((Fragment) this, (int) MFNetworkReturnCode.RESPONSE_OK, "android.permission.ACCESS_BACKGROUND_LOCATION");
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        xx5.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public String h1() {
        return "WatchAppsFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final void j1() {
        if (isActive()) {
            m34 m34 = this.i;
            if (m34 != null) {
                m34.c();
            } else {
                wg6.d("mWatchAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        WatchAppsFragment.super.onActivityCreated(bundle);
        DianaCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = activity;
            w04 w04 = this.j;
            if (w04 != null) {
                DianaCustomizeViewModel a2 = vd.a(dianaCustomizeEditActivity, w04).a(DianaCustomizeViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = a2;
                l75 l75 = this.g;
                if (l75 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        l75.a(dianaCustomizeViewModel);
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        WatchAppsFragment.super.onActivityResult(i2, i3, intent);
        if (i2 != 101) {
            if (i2 != 105) {
                if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) intent.getParcelableExtra("COMMUTE_TIME_WATCH_APP_SETTING")) != null) {
                    l75 l75 = this.g;
                    if (l75 != null) {
                        l75.a("commute-time", commuteTimeWatchAppSetting);
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            } else if (i3 == -1 && intent != null && (weatherWatchAppSetting = (WeatherWatchAppSetting) intent.getParcelableExtra("WEATHER_WATCH_APP_SETTING")) != null) {
                l75 l752 = this.g;
                if (l752 != null) {
                    l752.a("weather", weatherWatchAppSetting);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("SEARCH_WATCH_APP_RESULT_ID");
            if (!TextUtils.isEmpty(stringExtra)) {
                l75 l753 = this.g;
                if (l753 != null) {
                    wg6.a((Object) stringExtra, "selectedWatchAppId");
                    l753.a(stringExtra);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ve4 a2 = kb.a(layoutInflater, 2131558624, viewGroup, false, e1());
        PortfolioApp.get.instance().g().a(new o75(this)).a(this);
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        l75 l75 = this.g;
        if (l75 != null) {
            l75.g();
            WatchAppsFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        WatchAppsFragment.super.onResume();
        l75 l75 = this.g;
        if (l75 != null) {
            l75.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        m34 m34 = new m34((ArrayList) null, (m34.b) null, 3, (qg6) null);
        m34.a((m34.b) new b(this));
        this.i = m34;
        gu4 gu4 = new gu4((ArrayList) null, (gu4.c) null, 3, (qg6) null);
        gu4.a((gu4.c) new c(this));
        this.h = gu4;
        ax5<ve4> ax5 = this.f;
        if (ax5 != null) {
            ve4 a2 = ax5.a();
            if (a2 != null) {
                String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b2)) {
                    a2.q.setBackgroundColor(Color.parseColor(b2));
                }
                RecyclerView recyclerView = a2.r;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                gu4 gu42 = this.h;
                if (gu42 != null) {
                    recyclerView.setAdapter(gu42);
                    RecyclerView recyclerView2 = a2.s;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    m34 m342 = this.i;
                    if (m342 != null) {
                        recyclerView2.setAdapter(m342);
                        a2.w.setOnClickListener(new d(this));
                        a2.x.setOnClickListener(new e(this));
                        a2.u.setOnClickListener(new f(this));
                        return;
                    }
                    wg6.d("mWatchAppAdapter");
                    throw null;
                }
                wg6.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void v(List<WatchApp> list) {
        wg6.b(list, "watchApps");
        m34 m34 = this.i;
        if (m34 != null) {
            m34.a(list);
        } else {
            wg6.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w(boolean z) {
        if (isActive()) {
            ax5<ve4> ax5 = this.f;
            if (ax5 != null) {
                ve4 a2 = ax5.a();
                if (a2 != null) {
                    a2.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, z ? 2131231107 : 0, 0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d(WatchApp watchApp) {
        ax5<ve4> ax5 = this.f;
        if (ax5 != null) {
            ve4 a2 = ax5.a();
            if (a2 != null) {
                Object r2 = a2.u;
                wg6.a((Object) r2, "binding.tvSelectedWatchapps");
                r2.setText(jm4.a(PortfolioApp.get.instance(), watchApp.getNameKey(), watchApp.getName()));
                Object r22 = a2.v;
                wg6.a((Object) r22, "binding.tvWatchappsDetail");
                r22.setText(jm4.a(PortfolioApp.get.instance(), watchApp.getDescriptionKey(), watchApp.getDescription()));
                m34 m34 = this.i;
                if (m34 != null) {
                    int a3 = m34.a(watchApp.getWatchappId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "updateDetailComplication watchAppId=" + watchApp.getWatchappId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.s.scrollToPosition(a3);
                        return;
                    }
                    return;
                }
                wg6.d("mWatchAppAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(l75 l75) {
        wg6.b(l75, "presenter");
        this.g = l75;
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "onPermissionsGranted:" + i2 + ':' + list.size());
    }

    @DexIgnore
    public void a(List<Category> list) {
        wg6.b(list, HelpRequest.INCLUDE_CATEGORIES);
        gu4 gu4 = this.h;
        if (gu4 != null) {
            gu4.a(list);
        } else {
            wg6.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(boolean z, String str, String str2, String str3) {
        wg6.b(str, "watchAppId");
        wg6.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "updateSetting of watchAppId " + str + " requestContent " + str2 + " setting " + str3);
        ax5<ve4> ax5 = this.f;
        if (ax5 != null) {
            ve4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.t;
                wg6.a((Object) r0, "it.tvPermissionOrder");
                r0.setVisibility(8);
                Object r02 = a2.w;
                wg6.a((Object) r02, "it.tvWatchappsPermission");
                r02.setVisibility(8);
                if (z) {
                    Object r4 = a2.x;
                    wg6.a((Object) r4, "it.tvWatchappsSetting");
                    r4.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        Object r42 = a2.x;
                        wg6.a((Object) r42, "it.tvWatchappsSetting");
                        r42.setText(str3);
                        return;
                    }
                    Object r43 = a2.x;
                    wg6.a((Object) r43, "it.tvWatchappsSetting");
                    r43.setText(str2);
                    return;
                }
                Object r44 = a2.x;
                wg6.a((Object) r44, "it.tvWatchappsSetting");
                r44.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        CommuteTimeWatchAppSettingsActivity.B.a(this, str);
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        wg6.b(str, "topWatchApp");
        wg6.b(str2, "middleWatchApp");
        wg6.b(str3, "bottomWatchApp");
        WatchAppSearchActivity.C.a(this, str, str2, str3);
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "onPermissionsDenied:" + i2 + ':' + list.size());
        if (pw6.a(this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        D(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    D(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(int i2, int i3, String str, String str2) {
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsFragment", "showPermissionRequired current " + i2 + " total " + i3 + " title " + str + " content " + str2);
            ax5<ve4> ax5 = this.f;
            if (ax5 != null) {
                ve4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.x;
                    wg6.a((Object) r1, "it.tvWatchappsSetting");
                    r1.setVisibility(8);
                    if (i3 == 0 || i2 == i3) {
                        Object r6 = a2.w;
                        wg6.a((Object) r6, "it.tvWatchappsPermission");
                        r6.setVisibility(8);
                        Object r62 = a2.t;
                        wg6.a((Object) r62, "it.tvPermissionOrder");
                        r62.setVisibility(8);
                        return;
                    }
                    Object r2 = a2.w;
                    wg6.a((Object) r2, "it.tvWatchappsPermission");
                    r2.setVisibility(0);
                    Object r22 = a2.w;
                    wg6.a((Object) r22, "it.tvWatchappsPermission");
                    r22.setText(str);
                    if (str2.length() > 0) {
                        Object r8 = a2.v;
                        wg6.a((Object) r8, "it.tvWatchappsDetail");
                        r8.setText(str2);
                    }
                    if (i3 > 1) {
                        Object r82 = a2.t;
                        wg6.a((Object) r82, "it.tvPermissionOrder");
                        r82.setVisibility(0);
                        Object r83 = a2.t;
                        wg6.a((Object) r83, "it.tvPermissionOrder");
                        nh6 nh6 = nh6.a;
                        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887240);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                        Object[] objArr = {Integer.valueOf(i2), Integer.valueOf(i3)};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        r83.setText(format);
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        BaseActivity activity;
        BaseActivity activity2;
        BaseActivity activity3;
        wg6.b(str, "tag");
        if (wg6.a((Object) str, (Object) "REQUEST_NOTIFICATION_ACCESS")) {
            if (i2 == 2131363105) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        } else if (wg6.a((Object) str, (Object) lx5.c.a())) {
            if (i2 == 2131363190 && (activity3 = getActivity()) != null) {
                if (!pw6.a(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    xx5.a.a((Fragment) this, (int) MFNetworkReturnCode.RESPONSE_OK, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity3 != null) {
                    activity3.l();
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (wg6.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363190 && (activity2 = getActivity()) != null) {
                if (activity2 != null) {
                    activity2.l();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (wg6.a((Object) str, (Object) "LOCATION_PERMISSION_TAG")) {
            if (i2 != 2131363190 || (activity = getActivity()) == null) {
                return;
            }
            if (activity != null) {
                activity.l();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (wg6.a((Object) str, (Object) InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363190) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }
}
