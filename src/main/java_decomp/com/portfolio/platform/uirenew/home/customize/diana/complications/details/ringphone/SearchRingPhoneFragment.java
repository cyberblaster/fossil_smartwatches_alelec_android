package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.i34;
import com.fossil.j54;
import com.fossil.kb;
import com.fossil.q55;
import com.fossil.qg6;
import com.fossil.r55;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhoneFragment extends BaseFragment implements r55, i34.b {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<j54> f;
    @DexIgnore
    public i34 g;
    @DexIgnore
    public q55 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SearchRingPhoneFragment.j;
        }

        @DexIgnore
        public final SearchRingPhoneFragment b() {
            return new SearchRingPhoneFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SearchRingPhoneFragment a;

        @DexIgnore
        public b(SearchRingPhoneFragment searchRingPhoneFragment) {
            this.a = searchRingPhoneFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    /*
    static {
        String simpleName = SearchRingPhoneFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "SearchRingPhoneFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return j;
    }

    @DexIgnore
    public boolean i1() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return false;
        }
        q55 q55 = this.h;
        if (q55 != null) {
            q55.i();
            Intent intent = new Intent();
            q55 q552 = this.h;
            if (q552 != null) {
                intent.putExtra("KEY_SELECTED_RINGPHONE", q552.h());
                activity.setResult(-1, intent);
                activity.finish();
                return false;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        SearchRingPhoneFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558434, viewGroup, false, e1()));
        ax5<j54> ax5 = this.f;
        if (ax5 != null) {
            j54 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        SearchRingPhoneFragment.super.onPause();
        q55 q55 = this.h;
        if (q55 != null) {
            q55.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        SearchRingPhoneFragment.super.onResume();
        q55 q55 = this.h;
        if (q55 != null) {
            q55.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<j54> ax5 = this.f;
        if (ax5 != null) {
            j54 a2 = ax5.a();
            if (a2 != null) {
                this.g = new i34(this);
                RecyclerView recyclerView = a2.s;
                wg6.a((Object) recyclerView, "it.rvRingphones");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.s;
                wg6.a((Object) recyclerView2, "it.rvRingphones");
                i34 i34 = this.g;
                if (i34 != null) {
                    recyclerView2.setAdapter(i34);
                    a2.q.setOnClickListener(new b(this));
                    return;
                }
                wg6.d("mSearchRingPhoneAdapter");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void r(List<Ringtone> list) {
        wg6.b(list, "data");
        i34 i34 = this.g;
        if (i34 != null) {
            q55 q55 = this.h;
            if (q55 != null) {
                i34.a(list, q55.h());
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            wg6.d("mSearchRingPhoneAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(Ringtone ringtone) {
        wg6.b(ringtone, Constants.RINGTONE);
        q55 q55 = this.h;
        if (q55 != null) {
            q55.a(ringtone);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(q55 q55) {
        wg6.b(q55, "presenter");
        this.h = q55;
    }
}
