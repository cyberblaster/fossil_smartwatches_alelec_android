package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.j55;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.CommuteTimeSettingsFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public CommuteTimeSettingsPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wg6.b(fragment, "fragment");
            wg6.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(2131558428);
        CommuteTimeSettingsFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = CommuteTimeSettingsFragment.q.a();
            a((Fragment) b, CommuteTimeSettingsFragment.q.b(), 2131362119);
        }
        PortfolioApp.get.instance().g().a(new j55(b)).a(this);
        if (getIntent() != null) {
            str = getIntent().getStringExtra(Constants.USER_SETTING);
            wg6.a((Object) str, "intent.getStringExtra(Constants.JSON_KEY_SETTINGS)");
        } else {
            str = "";
        }
        CommuteTimeSettingsPresenter commuteTimeSettingsPresenter = this.B;
        if (commuteTimeSettingsPresenter != null) {
            commuteTimeSettingsPresenter.c(str);
        } else {
            wg6.d("mCommuteTimeSettingsPresenter");
            throw null;
        }
    }
}
