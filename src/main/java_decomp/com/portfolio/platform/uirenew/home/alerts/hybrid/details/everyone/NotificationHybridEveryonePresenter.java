package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import com.fossil.a35;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.d35;
import com.fossil.dl6;
import com.fossil.e35;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.i35$b$a;
import com.fossil.i35$c$a;
import com.fossil.i35$c$b;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.z24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppType;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridEveryonePresenter extends d35 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((qg6) null);
    @DexIgnore
    public /* final */ List<wx4> e; // = new ArrayList();
    @DexIgnore
    public /* final */ e35 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<wx4> h;
    @DexIgnore
    public /* final */ z24 i;
    @DexIgnore
    public /* final */ a35 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridEveryonePresenter.k;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1", f = "NotificationHybridEveryonePresenter.kt", l = {138}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryonePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationHybridEveryonePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                i35$b$a i35_b_a = new i35$b$a((xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, i35_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1", f = "NotificationHybridEveryonePresenter.kt", l = {42}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryonePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationHybridEveryonePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (!PortfolioApp.get.instance().w().P()) {
                    dl6 a2 = this.this$0.b();
                    i35$c$a i35_c_a = new i35$c$a((xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(a2, i35_c_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.m().isEmpty()) {
                this.this$0.i.a(this.this$0.j, null, new i35$c$b(this));
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationHybridEveryonePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationHybridEveryo\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridEveryonePresenter(e35 e35, int i2, ArrayList<wx4> arrayList, z24 z24, a35 a35) {
        wg6.b(e35, "mView");
        wg6.b(arrayList, "mContactWrappersSelected");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(a35, "mGetAllHybridContactGroups");
        this.f = e35;
        this.g = i2;
        this.h = arrayList;
        this.i = z24;
        this.j = a35;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    public int h() {
        return this.g;
    }

    @DexIgnore
    public void i() {
        if (!this.e.isEmpty()) {
            boolean z = false;
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -100) {
                    size--;
                } else {
                    if (this.e.get(size).getCurrentHandGroup() != this.g) {
                        this.f.a(this.e.get(size));
                    } else {
                        this.e.remove(size);
                    }
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_CALLS.packageName;
                wg6.a((Object) str, "AppType.ALL_CALLS.packageName");
                this.e.add(a(-100, str));
            }
        } else {
            String str2 = AppType.ALL_CALLS.packageName;
            wg6.a((Object) str2, "AppType.ALL_CALLS.packageName");
            this.e.add(a(-100, str2));
        }
        this.f.b(this.e, this.g);
    }

    @DexIgnore
    public void j() {
        if (!this.e.isEmpty()) {
            boolean z = false;
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -200) {
                    size--;
                } else {
                    if (this.e.get(size).getCurrentHandGroup() != this.g) {
                        this.f.a(this.e.get(size));
                    } else {
                        this.e.remove(size);
                    }
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_SMS.packageName;
                wg6.a((Object) str, "AppType.ALL_SMS.packageName");
                this.e.add(a(-200, str));
            }
        } else {
            String str2 = AppType.ALL_SMS.packageName;
            wg6.a((Object) str2, "AppType.ALL_SMS.packageName");
            this.e.add(a(-200, str2));
        }
        this.f.b(this.e, this.g);
    }

    @DexIgnore
    public void k() {
        ArrayList arrayList = new ArrayList();
        for (wx4 wx4 : this.e) {
            if (wx4.isAdded() && wx4.getCurrentHandGroup() == this.g) {
                arrayList.add(wx4);
            }
        }
        this.f.a((ArrayList<wx4>) arrayList);
    }

    @DexIgnore
    public void l() {
        if (!PortfolioApp.get.instance().w().P()) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final List<wx4> m() {
        return this.e;
    }

    @DexIgnore
    public void n() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(wx4 wx4) {
        wg6.b(wx4, "contactWrapper");
        for (wx4 wx42 : this.e) {
            Contact contact = wx42.getContact();
            Integer num = null;
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = wx4.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (wg6.a((Object) valueOf, (Object) num)) {
                wx42.setCurrentHandGroup(this.g);
                wx42.setAdded(true);
            }
        }
        this.f.b(this.e, this.g);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, "start");
        xm4 xm4 = xm4.d;
        e35 e35 = this.f;
        if (e35 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneFragment");
        } else if (xm4.a(xm4, ((NotificationHybridEveryoneFragment) e35).getContext(), "NOTIFICATION_CONTACTS", false, false, false, 28, (Object) null)) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final wx4 a(int i2, String str) {
        Contact contact = new Contact();
        contact.setContactId(i2);
        contact.setFirstName(str);
        wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
        wx4.setHasPhoneNumber(true);
        wx4.setAdded(true);
        wx4.setCurrentHandGroup(this.g);
        if (i2 == -100) {
            Contact contact2 = wx4.getContact();
            if (contact2 != null) {
                contact2.setUseCall(true);
            }
            Contact contact3 = wx4.getContact();
            if (contact3 != null) {
                contact3.setUseSms(false);
            }
        } else {
            Contact contact4 = wx4.getContact();
            if (contact4 != null) {
                contact4.setUseCall(false);
            }
            Contact contact5 = wx4.getContact();
            if (contact5 != null) {
                contact5.setUseSms(true);
            }
        }
        return wx4;
    }
}
