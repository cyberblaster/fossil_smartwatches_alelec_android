package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import com.fossil.a05;
import com.fossil.ax5;
import com.fossil.b05;
import com.fossil.h94;
import com.fossil.jb;
import com.fossil.jh6;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.o34;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InactivityNudgeTimeFragment extends BaseBottomSheetDialogFragment implements b05 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public /* final */ jb j; // = new o34(this);
    @DexIgnore
    public ax5<h94> o;
    @DexIgnore
    public a05 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return InactivityNudgeTimeFragment.r;
        }

        @DexIgnore
        public final InactivityNudgeTimeFragment b() {
            return new InactivityNudgeTimeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ h94 a;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimeFragment b;

        @DexIgnore
        public b(h94 h94, InactivityNudgeTimeFragment inactivityNudgeTimeFragment, boolean z) {
            this.a = h94;
            this.b = inactivityNudgeTimeFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            a05 a2 = InactivityNudgeTimeFragment.a(this.b);
            NumberPicker numberPicker2 = this.a.s;
            wg6.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.a.t;
            wg6.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ h94 a;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimeFragment b;

        @DexIgnore
        public c(h94 h94, InactivityNudgeTimeFragment inactivityNudgeTimeFragment, boolean z) {
            this.a = h94;
            this.b = inactivityNudgeTimeFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            a05 a2 = InactivityNudgeTimeFragment.a(this.b);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.a.u;
            wg6.a((Object) numberPicker2, "binding.numberPickerTwo");
            a2.a(valueOf, String.valueOf(numberPicker2.getValue()), false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ h94 a;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimeFragment b;

        @DexIgnore
        public d(h94 h94, InactivityNudgeTimeFragment inactivityNudgeTimeFragment, boolean z) {
            this.a = h94;
            this.b = inactivityNudgeTimeFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            a05 a2 = InactivityNudgeTimeFragment.a(this.b);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.a.u;
            wg6.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.a.t;
            wg6.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ h94 a;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimeFragment b;

        @DexIgnore
        public e(h94 h94, InactivityNudgeTimeFragment inactivityNudgeTimeFragment, boolean z) {
            this.a = h94;
            this.b = inactivityNudgeTimeFragment;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            a05 a2 = InactivityNudgeTimeFragment.a(this.b);
            NumberPicker numberPicker2 = this.a.s;
            wg6.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.a.u;
            wg6.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimeFragment a;

        @DexIgnore
        public f(InactivityNudgeTimeFragment inactivityNudgeTimeFragment) {
            this.a = inactivityNudgeTimeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            InactivityNudgeTimeFragment.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ h94 a;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 b;

        @DexIgnore
        public g(h94 h94, jh6 jh6) {
            this.a = h94;
            this.b = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.a.v;
            wg6.a((Object) constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                CoordinatorLayout.e layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior d = layoutParams.d();
                    if (d != null) {
                        d.e(3);
                        h94 h94 = this.a;
                        wg6.a((Object) h94, "it");
                        View d2 = h94.d();
                        wg6.a((Object) d2, "it.root");
                        d2.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.b.element);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                throw new rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new rc6("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = InactivityNudgeTimeFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "InactivityNudgeTimeFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ a05 a(InactivityNudgeTimeFragment inactivityNudgeTimeFragment) {
        a05 a05 = inactivityNudgeTimeFragment.p;
        if (a05 != null) {
            return a05;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void M(boolean z) {
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.u;
                wg6.a((Object) numberPicker, "binding.numberPickerTwo");
                numberPicker.setMinValue(0);
                NumberPicker numberPicker2 = a2.u;
                wg6.a((Object) numberPicker2, "binding.numberPickerTwo");
                numberPicker2.setMaxValue(59);
                a2.u.setOnValueChangedListener(new b(a2, this, z));
                if (z) {
                    NumberPicker numberPicker3 = a2.s;
                    wg6.a((Object) numberPicker3, "binding.numberPickerOne");
                    numberPicker3.setMinValue(0);
                    NumberPicker numberPicker4 = a2.s;
                    wg6.a((Object) numberPicker4, "binding.numberPickerOne");
                    numberPicker4.setMaxValue(23);
                    a2.s.setOnValueChangedListener(new c(a2, this, z));
                    NumberPicker numberPicker5 = a2.t;
                    wg6.a((Object) numberPicker5, "binding.numberPickerThree");
                    numberPicker5.setVisibility(8);
                    return;
                }
                NumberPicker numberPicker6 = a2.s;
                wg6.a((Object) numberPicker6, "binding.numberPickerOne");
                numberPicker6.setMinValue(1);
                NumberPicker numberPicker7 = a2.s;
                wg6.a((Object) numberPicker7, "binding.numberPickerOne");
                numberPicker7.setMaxValue(12);
                a2.s.setOnValueChangedListener(new d(a2, this, z));
                String[] strArr = {jm4.a((Context) PortfolioApp.get.instance(), 2131886102), jm4.a((Context) PortfolioApp.get.instance(), 2131886104)};
                NumberPicker numberPicker8 = a2.t;
                wg6.a((Object) numberPicker8, "binding.numberPickerThree");
                numberPicker8.setVisibility(0);
                NumberPicker numberPicker9 = a2.t;
                wg6.a((Object) numberPicker9, "binding.numberPickerThree");
                numberPicker9.setMinValue(0);
                NumberPicker numberPicker10 = a2.t;
                wg6.a((Object) numberPicker10, "binding.numberPickerThree");
                numberPicker10.setMaxValue(1);
                a2.t.setDisplayedValues(strArr);
                a2.t.setOnValueChangedListener(new e(a2, this, z));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        h94 a2 = kb.a(layoutInflater, 2131558544, viewGroup, false, this.j);
        String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            a2.v.setBackgroundColor(Color.parseColor(b2));
        }
        a2.q.setOnClickListener(new f(this));
        this.o = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onPause() {
        a05 a05 = this.p;
        if (a05 != null) {
            a05.g();
            InactivityNudgeTimeFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        InactivityNudgeTimeFragment.super.onResume();
        a05 a05 = this.p;
        if (a05 != null) {
            a05.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        InactivityNudgeTimeFragment.super.onViewCreated(view, bundle);
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null) {
                jh6 jh6 = new jh6();
                jh6.element = null;
                jh6.element = new g(a2, jh6);
                wg6.a((Object) a2, "it");
                View d2 = a2.d();
                wg6.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) jh6.element);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void r(int i) {
        a05 a05 = this.p;
        if (a05 != null) {
            a05.a(i);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void x(String str) {
        wg6.b(str, "description");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                wg6.a((Object) fragmentManager, "fragmentManager!!");
                lx5.a(fragmentManager, str);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(a05 a05) {
        wg6.b(a05, "presenter");
        this.p = a05;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(String str) {
        Object r0;
        wg6.b(str, Explore.COLUMN_TITLE);
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null && (r0 = a2.r) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i, boolean z) {
        int i2 = i / 60;
        int i3 = i % 60;
        int i4 = 0;
        if (!z) {
            if (i2 >= 12) {
                i4 = 1;
                i2 -= 12;
            }
            if (i2 == 0) {
                i2 = 12;
            }
        }
        ax5<h94> ax5 = this.o;
        if (ax5 != null) {
            h94 a2 = ax5.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.s;
                wg6.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i2);
                NumberPicker numberPicker2 = a2.u;
                wg6.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i3);
                NumberPicker numberPicker3 = a2.t;
                wg6.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i4);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
