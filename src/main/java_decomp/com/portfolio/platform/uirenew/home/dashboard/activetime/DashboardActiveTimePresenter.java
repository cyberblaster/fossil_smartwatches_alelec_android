package com.portfolio.platform.uirenew.home.dashboard.activetime;

import androidx.lifecycle.LiveData;
import com.fossil.ab5;
import com.fossil.af6;
import com.fossil.bb5$b$a;
import com.fossil.bb5$b$b;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gg6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.za5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardActiveTimePresenter extends za5 implements vk4.a {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public /* final */ ab5 g;
    @DexIgnore
    public /* final */ SummariesRepository h;
    @DexIgnore
    public /* final */ FitnessDataRepository i;
    @DexIgnore
    public /* final */ ActivitySummaryDao j;
    @DexIgnore
    public /* final */ FitnessDatabase k;
    @DexIgnore
    public /* final */ UserRepository l;
    @DexIgnore
    public /* final */ u04 m;
    @DexIgnore
    public /* final */ ik4 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1", f = "DashboardActiveTimePresenter.kt", l = {63}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActiveTimePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DashboardActiveTimePresenter dashboardActiveTimePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardActiveTimePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            LiveData pagedList;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                bb5$b$b bb5_b_b = new bb5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bb5_b_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                Date d = bk4.d(mFUser.getCreatedAt());
                DashboardActiveTimePresenter dashboardActiveTimePresenter = this.this$0;
                SummariesRepository h = dashboardActiveTimePresenter.h;
                SummariesRepository h2 = this.this$0.h;
                ik4 g = this.this$0.n;
                FitnessDataRepository e = this.this$0.i;
                ActivitySummaryDao c = this.this$0.j;
                FitnessDatabase f = this.this$0.k;
                wg6.a((Object) d, "createdDate");
                dashboardActiveTimePresenter.f = h.getSummariesPaging(h2, g, e, c, f, d, this.this$0.m, this.this$0);
                Listing b = this.this$0.f;
                if (!(b == null || (pagedList = b.getPagedList()) == null)) {
                    ab5 j = this.this$0.g;
                    if (j != null) {
                        pagedList.a((DashboardActiveTimeFragment) j, new bb5$b$a(this));
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                    }
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DashboardActiveTimePresenter(ab5 ab5, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, u04 u04, ik4 ik4) {
        wg6.b(ab5, "mView");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(activitySummaryDao, "mActivitySummaryDao");
        wg6.b(fitnessDatabase, "mFitnessDatabase");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(u04, "mAppExecutors");
        wg6.b(ik4, "mFitnessHelper");
        this.g = ab5;
        this.h = summariesRepository;
        this.i = fitnessDataRepository;
        this.j = activitySummaryDao;
        this.k = fitnessDatabase;
        this.l = userRepository;
        this.m = u04;
        this.n = ik4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "start");
        if (!bk4.t(this.e).booleanValue()) {
            this.e = new Date();
            Listing<ActivitySummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "stop");
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        LiveData<cf<ActivitySummary>> pagedList;
        try {
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                ab5 ab5 = this.g;
                if (ab5 != null) {
                    pagedList.a((DashboardActiveTimeFragment) ab5);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("DashboardActiveTimePresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        gg6<cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        wg6.b(gVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimePresenter", "onStatusChange status=" + gVar);
        if (gVar.a()) {
            this.g.f();
        }
    }
}
