package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.ao5;
import com.fossil.ax5;
import com.fossil.bo5;
import com.fossil.h84;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.ly5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRingChartFragment extends BaseFragment implements ly5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String o;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static String q;
    @DexIgnore
    public static String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public CustomizeRingChartViewModel g;
    @DexIgnore
    public ax5<h84> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CustomizeRingChartFragment.p;
        }

        @DexIgnore
        public final String b() {
            return CustomizeRingChartFragment.o;
        }

        @DexIgnore
        public final String c() {
            return CustomizeRingChartFragment.q;
        }

        @DexIgnore
        public final String d() {
            return CustomizeRingChartFragment.r;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<bo5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartFragment a;

        @DexIgnore
        public b(CustomizeRingChartFragment customizeRingChartFragment) {
            this.a = customizeRingChartFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CustomizeRingChartViewModel.b bVar) {
            if (bVar != null) {
                Integer d = bVar.d();
                if (d != null) {
                    this.a.u(d.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.t(c.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.s(b.intValue());
                }
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.r(a2.intValue());
                }
                Integer f = bVar.f();
                if (f != null) {
                    this.a.w(f.intValue());
                }
                Integer e = bVar.e();
                if (e != null) {
                    this.a.v(e.intValue());
                }
                Integer h = bVar.h();
                if (h != null) {
                    this.a.y(h.intValue());
                }
                Integer g = bVar.g();
                if (g != null) {
                    this.a.x(g.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartFragment a;

        @DexIgnore
        public c(CustomizeRingChartFragment customizeRingChartFragment) {
            this.a = customizeRingChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 401);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartFragment a;

        @DexIgnore
        public d(CustomizeRingChartFragment customizeRingChartFragment) {
            this.a = customizeRingChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, (int) Action.ActivityTracker.TAG_ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartFragment a;

        @DexIgnore
        public e(CustomizeRingChartFragment customizeRingChartFragment) {
            this.a = customizeRingChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, (int) MFNetworkReturnCode.WRONG_PASSWORD);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartFragment a;

        @DexIgnore
        public f(CustomizeRingChartFragment customizeRingChartFragment) {
            this.a = customizeRingChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, (int) MFNetworkReturnCode.NOT_FOUND);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartFragment a;

        @DexIgnore
        public g(CustomizeRingChartFragment customizeRingChartFragment) {
            this.a = customizeRingChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.g(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = CustomizeRingChartFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeRingChartFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363190) {
            CustomizeRingChartViewModel customizeRingChartViewModel = this.g;
            if (customizeRingChartViewModel != null) {
                customizeRingChartViewModel.a(UserCustomizeThemeFragment.p.a(), o, p, q, r);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf(16777215 & i3)};
        String format = String.format("#%06X", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3 + " hexColor=" + format);
        CustomizeRingChartViewModel customizeRingChartViewModel = this.g;
        if (customizeRingChartViewModel != null) {
            customizeRingChartViewModel.a(i2, Color.parseColor(format));
            switch (i2) {
                case 401:
                    o = format;
                    return;
                case Action.ActivityTracker.TAG_ACTIVITY:
                    p = format;
                    return;
                case MFNetworkReturnCode.WRONG_PASSWORD:
                    q = format;
                    return;
                case MFNetworkReturnCode.NOT_FOUND:
                    r = format;
                    return;
                default:
                    return;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        h84 a2 = kb.a(LayoutInflater.from(getContext()), 2131558531, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new ao5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            CustomizeRingChartViewModel a3 = vd.a(this, w04).a(CustomizeRingChartViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            this.g = a3;
            CustomizeRingChartViewModel customizeRingChartViewModel = this.g;
            if (customizeRingChartViewModel != null) {
                customizeRingChartViewModel.b().a(getViewLifecycleOwner(), new b(this));
                CustomizeRingChartViewModel customizeRingChartViewModel2 = this.g;
                if (customizeRingChartViewModel2 != null) {
                    customizeRingChartViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        CustomizeRingChartFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        o = null;
        p = null;
        q = null;
        r = null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CustomizeRingChartFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        CustomizeRingChartViewModel customizeRingChartViewModel = this.g;
        if (customizeRingChartViewModel != null) {
            customizeRingChartViewModel.c();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.w.setProgress(1.0f);
                a2.v.setProgress(1.0f);
                a2.x.setProgress(1.0f);
                a2.y.setProgress(1.0f);
                a2.s.setOnClickListener(new c(this));
                a2.r.setOnClickListener(new d(this));
                a2.t.setOnClickListener(new e(this));
                a2.u.setOnClickListener(new f(this));
                a2.q.setOnClickListener(new g(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void r(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.v.setProgressRingColorPreview(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.z.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.w.setProgressRingColorPreview(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void u(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.A.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void v(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.x.setProgressRingColorPreview(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void w(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.B.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void x(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.y.setProgressRingColorPreview(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void y(int i2) {
        ax5<h84> ax5 = this.h;
        if (ax5 != null) {
            h84 a2 = ax5.a();
            if (a2 != null) {
                a2.C.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
