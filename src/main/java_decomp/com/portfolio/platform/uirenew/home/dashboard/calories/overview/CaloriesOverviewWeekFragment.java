package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.b74;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.ud5;
import com.fossil.ut4;
import com.fossil.vd5;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewWeekFragment extends BaseFragment implements vd5 {
    @DexIgnore
    public ax5<b74> f;
    @DexIgnore
    public ud5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "CaloriesOverviewWeekFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        b74 a2;
        OverviewWeekChart overviewWeekChart;
        ax5<b74> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.q) != null) {
            ud5 ud5 = this.g;
            if ((ud5 != null ? ud5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewWeekChart.a("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.a("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b74 a2;
        wg6.b(layoutInflater, "inflater");
        CaloriesOverviewWeekFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onCreateView");
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558513, viewGroup, false, e1()));
        j1();
        ax5<b74> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CaloriesOverviewWeekFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onResume");
        j1();
        ud5 ud5 = this.g;
        if (ud5 != null) {
            ud5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        CaloriesOverviewWeekFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onStop");
        ud5 ud5 = this.g;
        if (ud5 != null) {
            ud5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(ut4 ut4) {
        b74 a2;
        OverviewWeekChart overviewWeekChart;
        wg6.b(ut4, "baseModel");
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "showWeekDetails");
        ax5<b74> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            ut4.a aVar = ut4.a;
            wg6.a((Object) overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            wg6.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewWeekChart.a(ut4);
        }
    }

    @DexIgnore
    public void a(ud5 ud5) {
        wg6.b(ud5, "presenter");
        this.g = ud5;
    }
}
