package com.portfolio.platform.uirenew.home.profile.opt;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.OptInFragmentBinding;
import com.fossil.ax5;
import com.fossil.jk3;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.pc4;
import com.fossil.qg6;
import com.fossil.ul5;
import com.fossil.vl5;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileOptInFragment extends BaseFragment implements vl5 {
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public ax5<pc4> f;
    @DexIgnore
    public ul5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ProfileOptInFragment a() {
            return new ProfileOptInFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInFragment a;

        @DexIgnore
        public b(ProfileOptInFragment profileOptInFragment) {
            this.a = profileOptInFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ OptInFragmentBinding b;

        @DexIgnore
        public c(ProfileOptInFragment profileOptInFragment, OptInFragmentBinding optInFragmentBinding) {
            this.a = profileOptInFragment;
            this.b = optInFragmentBinding;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        public final void onClick(View view) {
            ProfileOptInFragment profileOptInFragment = this.a;
            Object r0 = this.b.q;
            wg6.a((Object) r0, "binding.anonymousSwitch");
            profileOptInFragment.c("Usage_Data", r0.isChecked());
            AnalyticsHelper a2 = this.a.f1();
            Object r02 = this.b.q;
            wg6.a((Object) r02, "binding.anonymousSwitch");
            a2.c(r02.isChecked());
            ul5 b2 = ProfileOptInFragment.b(this.a);
            Object r03 = this.b.q;
            wg6.a((Object) r03, "binding.anonymousSwitch");
            b2.a(r03.isChecked());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ OptInFragmentBinding b;

        @DexIgnore
        public d(ProfileOptInFragment profileOptInFragment, OptInFragmentBinding optInFragmentBinding) {
            this.a = profileOptInFragment;
            this.b = optInFragmentBinding;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        public final void onClick(View view) {
            ProfileOptInFragment profileOptInFragment = this.a;
            Object r0 = this.b.u;
            wg6.a((Object) r0, "binding.scSubcribeEmail");
            profileOptInFragment.c("Emails", r0.isChecked());
            AnalyticsHelper a2 = this.a.f1();
            Object r02 = this.b.u;
            wg6.a((Object) r02, "binding.scSubcribeEmail");
            a2.b(r02.isChecked());
            ul5 b2 = ProfileOptInFragment.b(this.a);
            Object r03 = this.b.u;
            wg6.a((Object) r03, "binding.scSubcribeEmail");
            b2.b(r03.isChecked());
        }
    }

    /*
    static {
        wg6.a((Object) ProfileOptInFragment.class.getSimpleName(), "ProfileOptInFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ ul5 b(ProfileOptInFragment profileOptInFragment) {
        ul5 ul5 = profileOptInFragment.g;
        if (ul5 != null) {
            return ul5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void H(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<pc4> ax5 = this.f;
        if (ax5 != null) {
            OptInFragmentBinding a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.q) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void K(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<pc4> ax5 = this.f;
        if (ax5 != null) {
            OptInFragmentBinding a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.u) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void Q0() {
        a();
    }

    @DexIgnore
    public final void c(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("Item", str);
        hashMap.put("Optin", z ? "Yes" : "No");
        a("profile_optin", (Map<String, String>) hashMap);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f0() {
        String string = getString(2131886679);
        wg6.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        X(string);
    }

    @DexIgnore
    public void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r5v7, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        OptInFragmentBinding a2 = kb.a(LayoutInflater.from(getContext()), 2131558588, (ViewGroup) null, false);
        a2.s.setOnClickListener(new b(this));
        Object r6 = a2.r;
        wg6.a((Object) r6, "binding.ftvDescriptionSubcribeEmail");
        nh6 nh6 = nh6.a;
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886967);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026_GetTipsAboutBrandsTools)");
        Object[] objArr = {PortfolioApp.get.instance().i()};
        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        r6.setText(format);
        a2.q.setOnClickListener(new c(this, a2));
        a2.u.setOnClickListener(new d(this, a2));
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ProfileOptInFragment.super.onPause();
        ul5 ul5 = this.g;
        if (ul5 != null) {
            ul5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        ProfileOptInFragment.super.onResume();
        ul5 ul5 = this.g;
        if (ul5 != null) {
            ul5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2, String str) {
        wg6.b(str, "message");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(ul5 ul5) {
        wg6.b(ul5, "presenter");
        jk3.a(ul5);
        wg6.a((Object) ul5, "checkNotNull(presenter)");
        this.g = ul5;
    }
}
