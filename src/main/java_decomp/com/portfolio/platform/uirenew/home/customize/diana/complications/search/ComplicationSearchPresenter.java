package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.i65;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.j65;
import com.fossil.jh6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m65$a$a;
import com.fossil.m65$b$a;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationSearchPresenter extends i65 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ArrayList<Complication> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Complication> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ j65 l;
    @DexIgnore
    public /* final */ ComplicationRepository m;
    @DexIgnore
    public /* final */ an4 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $results;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationSearchPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ComplicationSearchPresenter complicationSearchPresenter, String str, jh6 jh6, xe6 xe6) {
            super(2, xe6);
            this.this$0 = complicationSearchPresenter;
            this.$query = str;
            this.$results = jh6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$query, this.$results, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(T t) {
            jh6 jh6;
            T a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(t);
                il6 il6 = this.p$;
                if (this.$query.length() > 0) {
                    jh6 jh62 = this.$results;
                    dl6 a2 = this.this$0.c();
                    m65$a$a m65_a_a = new m65$a$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = jh62;
                    this.label = 1;
                    t = gk6.a(a2, m65_a_a, this);
                    if (t == a) {
                        return a;
                    }
                    jh6 = jh62;
                }
                this.this$0.i().b((List<lc6<Complication, String>>) this.this$0.a((List<Complication>) (List) this.$results.element));
                this.this$0.i = this.$query;
                return cd6.a;
            } else if (i == 1) {
                jh6 = (jh6) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(t);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            jh6.element = (List) t;
            this.this$0.i().b((List<lc6<Complication, String>>) this.this$0.a((List<Complication>) (List) this.$results.element));
            this.this$0.i = this.$query;
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1", f = "ComplicationSearchPresenter.kt", l = {41}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationSearchPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ComplicationSearchPresenter complicationSearchPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = complicationSearchPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                m65$b$a m65_b_a = new m65$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, m65_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j();
            return cd6.a;
        }
    }

    @DexIgnore
    public ComplicationSearchPresenter(j65 j65, ComplicationRepository complicationRepository, an4 an4) {
        wg6.b(j65, "mView");
        wg6.b(complicationRepository, "mComplicationRepository");
        wg6.b(an4, "sharedPreferencesManager");
        this.l = j65;
        this.m = complicationRepository;
        this.n = an4;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.i = "";
        this.l.u();
        j();
    }

    @DexIgnore
    public final j65 i() {
        return this.l;
    }

    @DexIgnore
    public final void j() {
        if (this.k.isEmpty()) {
            this.l.b(a((List<Complication>) yd6.d(this.j)));
        } else {
            this.l.d(a((List<Complication>) yd6.d(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            j65 j65 = this.l;
            String str = this.i;
            if (str != null) {
                j65.b(str);
                String str2 = this.i;
                if (str2 != null) {
                    a(str2);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void k() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4) {
        wg6.b(str, "topComplication");
        wg6.b(str2, "bottomComplication");
        wg6.b(str3, "rightComplication");
        wg6.b(str4, "leftComplication");
        this.e = str;
        this.f = str2;
        this.h = str3;
        this.g = str4;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        jh6 jh6 = new jh6();
        jh6.element = new ArrayList();
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new a(this, str, jh6, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(Complication complication) {
        wg6.b(complication, "selectedComplication");
        List<String> d = this.n.d();
        wg6.a((Object) d, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
        if (!d.contains(complication.getComplicationId())) {
            d.add(0, complication.getComplicationId());
            if (d.size() > 5) {
                d = d.subList(0, 5);
            }
            this.n.b(d);
        }
        this.l.a(complication);
    }

    @DexIgnore
    public final List<lc6<Complication, String>> a(List<Complication> list) {
        ArrayList arrayList = new ArrayList();
        for (Complication next : list) {
            String complicationId = next.getComplicationId();
            if (wg6.a((Object) complicationId, (Object) this.e)) {
                arrayList.add(new lc6(next, "top"));
            } else if (wg6.a((Object) complicationId, (Object) this.f)) {
                arrayList.add(new lc6(next, "bottom"));
            } else if (wg6.a((Object) complicationId, (Object) this.h)) {
                arrayList.add(new lc6(next, "right"));
            } else if (wg6.a((Object) complicationId, (Object) this.g)) {
                arrayList.add(new lc6(next, "left"));
            } else {
                arrayList.add(new lc6(next, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString("top", this.e);
            bundle.putString("bottom", this.f);
            bundle.putString("left", this.g);
            bundle.putString("right", this.h);
        }
        return bundle;
    }
}
