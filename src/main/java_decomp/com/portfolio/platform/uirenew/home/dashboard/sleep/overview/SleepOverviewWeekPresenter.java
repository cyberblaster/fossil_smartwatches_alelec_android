package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.graphics.RectF;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ug5;
import com.fossil.vg5;
import com.fossil.wg5$b$a;
import com.fossil.wg5$b$b;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewWeekPresenter extends ug5 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE f; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.l.e());
    @DexIgnore
    public LiveData<yx5<List<MFSleepDay>>> g; // = new MutableLiveData();
    @DexIgnore
    public BarChart.c h; // = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
    @DexIgnore
    public /* final */ vg5 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ SleepSummariesRepository k;
    @DexIgnore
    public /* final */ PortfolioApp l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1", f = "SleepOverviewWeekPresenter.kt", l = {57}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SleepOverviewWeekPresenter sleepOverviewWeekPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepOverviewWeekPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00f6  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0103  */
        public final Object invokeSuspend(Object obj) {
            vg5 g;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                this.this$0.i();
                if (this.this$0.e == null || !bk4.t(SleepOverviewWeekPresenter.d(this.this$0)).booleanValue()) {
                    this.this$0.e = new Date();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("SleepOverviewWeekPresenter", "start - mDate=" + SleepOverviewWeekPresenter.d(this.this$0));
                    dl6 a2 = this.this$0.b();
                    wg5$b$b wg5_b_b = new wg5$b$b(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    obj = gk6.a(a2, wg5_b_b, this);
                    if (obj == a) {
                        return a;
                    }
                }
                LiveData e = this.this$0.g;
                g = this.this$0.i;
                if (g == null) {
                    e.a((SleepOverviewWeekFragment) g, new wg5$b$a(this));
                    return cd6.a;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("SleepOverviewWeekPresenter", "start - startDate=" + ((Date) lc6.getFirst()) + ", endDate=" + ((Date) lc6.getSecond()));
            Date date = (Date) lc6.getFirst();
            Date date2 = (Date) lc6.getSecond();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("SleepOverviewWeekPresenter", "start - startDate=" + date + ", endDate=" + date2);
            SleepOverviewWeekPresenter sleepOverviewWeekPresenter = this.this$0;
            sleepOverviewWeekPresenter.g = sleepOverviewWeekPresenter.k.getSleepSummaries(date, date2, false);
            LiveData e2 = this.this$0.g;
            g = this.this$0.i;
            if (g == null) {
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SleepOverviewWeekPresenter(vg5 vg5, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        wg6.b(vg5, "mView");
        wg6.b(userRepository, "userRepository");
        wg6.b(sleepSummariesRepository, "mSummariesRepository");
        wg6.b(portfolioApp, "mApp");
        this.i = vg5;
        this.j = userRepository;
        this.k = sleepSummariesRepository;
        this.l = portfolioApp;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
        Date date = sleepOverviewWeekPresenter.e;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void j() {
        this.i.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "start");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "stop");
        try {
            LiveData<yx5<List<MFSleepDay>>> liveData = this.g;
            vg5 vg5 = this.i;
            if (vg5 != null) {
                liveData.a((SleepOverviewWeekFragment) vg5);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.f;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<MFSleepDay> list) {
        int i2;
        T t;
        Date date2 = date;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date2);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
        Calendar instance = Calendar.getInstance(Locale.US);
        wg6.a((Object) instance, "calendar");
        instance.setTime(date2);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            wg6.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (bk4.d(((MFSleepDay) t).getDate(), instance.getTime())) {
                        break;
                    }
                }
                MFSleepDay mFSleepDay = (MFSleepDay) t;
                if (mFSleepDay != null) {
                    int a2 = ik4.d.a(mFSleepDay);
                    int sleepMinutes = mFSleepDay.getSleepMinutes();
                    i4 = Math.max(Math.max(a2, sleepMinutes), i4);
                    cVar.a().add(new BarChart.a(a2, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, sleepMinutes, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
                    i5 = a2;
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 <= 0) {
            i4 = i5 > 0 ? i5 : 480;
        }
        cVar.b(i4);
        return cVar;
    }

    @DexIgnore
    public final lc6<Date, Date> a(Date date) {
        Date b2 = bk4.b(date, 6);
        MFUser currentUser = this.j.getCurrentUser();
        if (currentUser != null) {
            Date d = bk4.d(currentUser.getCreatedAt());
            if (!bk4.b(b2, d)) {
                b2 = d;
            }
        }
        return new lc6<>(b2, date);
    }
}
