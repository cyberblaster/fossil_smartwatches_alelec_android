package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.content.Context;
import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.uf5;
import com.fossil.vf5;
import com.fossil.wf5$b$a;
import com.fossil.wf5$b$b;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewWeekPresenter extends uf5 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public LiveData<yx5<List<DailyHeartRateSummary>>> f;
    @DexIgnore
    public List<String> g; // = new ArrayList();
    @DexIgnore
    public /* final */ vf5 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1", f = "HeartRateOverviewWeekPresenter.kt", l = {48}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00b2  */
        public final Object invokeSuspend(Object obj) {
            LiveData d;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.e == null || !bk4.t(HeartRateOverviewWeekPresenter.c(this.this$0)).booleanValue()) {
                    this.this$0.e = new Date();
                    dl6 a2 = this.this$0.b();
                    wf5$b$b wf5_b_b = new wf5$b$b(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    obj = gk6.a(a2, wf5_b_b, this);
                    if (obj == a) {
                        return a;
                    }
                }
                this.this$0.h();
                d = this.this$0.f;
                if (d != null) {
                    vf5 g = this.this$0.h;
                    if (g != null) {
                        d.a((HeartRateOverviewWeekFragment) g, new wf5$b$a(this));
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewWeekPresenter", "start - startDate=" + ((Date) lc6.getFirst()) + ", endDate=" + ((Date) lc6.getSecond()));
            HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter = this.this$0;
            heartRateOverviewWeekPresenter.f = heartRateOverviewWeekPresenter.j.getHeartRateSummaries((Date) lc6.getFirst(), (Date) lc6.getSecond(), false);
            this.this$0.h();
            d = this.this$0.f;
            if (d != null) {
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewWeekPresenter(vf5 vf5, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        wg6.b(vf5, "mView");
        wg6.b(userRepository, "userRepository");
        wg6.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.h = vf5;
        this.i = userRepository;
        this.j = heartRateSummaryRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date c(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
        Date date = heartRateOverviewWeekPresenter.e;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewWeekPresenter", "start");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        MFLogger.d("HeartRateOverviewWeekPresenter", "stop");
        LiveData<yx5<List<DailyHeartRateSummary>>> liveData = this.f;
        if (liveData != null) {
            vf5 vf5 = this.h;
            if (vf5 != null) {
                liveData.a((HeartRateOverviewWeekFragment) vf5);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v19, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v22, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v25, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void h() {
        this.g.clear();
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        Date date = this.e;
        if (date != null) {
            instance.setTime(date);
            instance.add(5, -6);
            for (int i2 = 1; i2 <= 7; i2++) {
                Boolean t = bk4.t(instance.getTime());
                wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
                if (!t.booleanValue()) {
                    switch (instance.get(7)) {
                        case 1:
                            List<String> list = this.g;
                            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886537);
                            wg6.a((Object) a2, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                            list.add(a2);
                            break;
                        case 2:
                            List<String> list2 = this.g;
                            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886536);
                            wg6.a((Object) a3, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                            list2.add(a3);
                            break;
                        case 3:
                            List<String> list3 = this.g;
                            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886539);
                            wg6.a((Object) a4, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                            list3.add(a4);
                            break;
                        case 4:
                            List<String> list4 = this.g;
                            String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886541);
                            wg6.a((Object) a5, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                            list4.add(a5);
                            break;
                        case 5:
                            List<String> list5 = this.g;
                            String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886540);
                            wg6.a((Object) a6, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                            list5.add(a6);
                            break;
                        case 6:
                            List<String> list6 = this.g;
                            String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886535);
                            wg6.a((Object) a7, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                            list6.add(a7);
                            break;
                        case 7:
                            List<String> list7 = this.g;
                            String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886538);
                            wg6.a((Object) a8, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                            list7.add(a8);
                            break;
                    }
                } else {
                    List<String> list8 = this.g;
                    String a9 = jm4.a((Context) PortfolioApp.get.instance(), 2131886435);
                    wg6.a((Object) a9, "LanguageHelper.getString\u2026_Sleep7days_Label__Today)");
                    list8.add(a9);
                }
                instance.add(5, 1);
            }
            return;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.h.a(this);
    }

    @DexIgnore
    public final lc6<Date, Date> a(Date date) {
        Date b2 = bk4.b(date, 6);
        MFUser currentUser = this.i.getCurrentUser();
        if (currentUser != null) {
            Date d = bk4.d(currentUser.getCreatedAt());
            if (!bk4.b(b2, d)) {
                b2 = d;
            }
        }
        return new lc6<>(b2, date);
    }
}
