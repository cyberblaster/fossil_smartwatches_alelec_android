package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse;

import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.tz4;
import com.fossil.tz4$b$a;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseEditDialogViewModel extends td {
    @DexIgnore
    public MutableLiveData<tz4.a> a; // = new MutableLiveData<>();
    @DexIgnore
    public a b; // = new a((Integer) null, (Boolean) null, 3, (qg6) null);
    @DexIgnore
    public /* final */ QuickResponseRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Boolean b;

        @DexIgnore
        public a() {
            this((Integer) null, (Boolean) null, 3, (qg6) null);
        }

        @DexIgnore
        public a(Integer num, Boolean bool) {
            this.a = num;
            this.b = bool;
        }

        @DexIgnore
        public final Boolean a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.a;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Integer num, Boolean bool, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : bool);
        }

        @DexIgnore
        public final void a(Integer num, Boolean bool) {
            this.a = num;
            this.b = bool;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseEditDialogViewModel$updateResponse$1", f = "QuickResponseEditDialogViewModel.kt", l = {33}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseMessage $qr;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseEditDialogViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(QuickResponseEditDialogViewModel quickResponseEditDialogViewModel, QuickResponseMessage quickResponseMessage, xe6 xe6) {
            super(2, xe6);
            this.this$0 = quickResponseEditDialogViewModel;
            this.$qr = quickResponseMessage;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$qr, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                tz4$b$a tz4_b_a = new tz4$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, tz4_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public QuickResponseEditDialogViewModel(QuickResponseRepository quickResponseRepository) {
        wg6.b(quickResponseRepository, "mQRRepository");
        this.c = quickResponseRepository;
    }

    @DexIgnore
    public final MutableLiveData<tz4.a> b() {
        return this.a;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final void a(String str, int i) {
        wg6.b(str, "text");
        int length = str.length();
        this.b.a(Integer.valueOf(length), Boolean.valueOf(length >= i));
        a();
    }

    @DexIgnore
    public final void a(QuickResponseMessage quickResponseMessage) {
        wg6.b(quickResponseMessage, "qr");
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new b(this, quickResponseMessage, (xe6) null), 3, (Object) null);
    }
}
