package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.i25;
import com.fossil.l25;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridAppActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public l25 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<String> arrayList) {
            wg6.b(fragment, "fragment");
            wg6.b(arrayList, "stringAppsSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridAppActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putStringArrayListExtra("LIST_APPS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 4567);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        wg6.a((Object) NotificationHybridAppActivity.class.getSimpleName(), "NotificationHybridAppAct\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        NotificationHybridAppFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = NotificationHybridAppFragment.j.b();
            a((Fragment) b, NotificationHybridAppFragment.j.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new i25(b, getIntent().getIntExtra("HAND_NUMBER", 0), getIntent().getStringArrayListExtra("LIST_APPS_SELECTED"))).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppContract.View");
    }
}
