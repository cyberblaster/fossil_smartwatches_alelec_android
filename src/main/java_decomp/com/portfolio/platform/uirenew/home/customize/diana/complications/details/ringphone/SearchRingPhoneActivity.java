package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.s55;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public SearchRingPhonePresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wg6.b(fragment, "context");
            wg6.b(str, "selectedRingtone");
            Intent intent = new Intent(fragment.getContext(), SearchRingPhoneActivity.class);
            intent.putExtra("KEY_SELECTED_RINGPHONE", str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 104);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        Ringtone ringtone;
        super.onCreate(bundle);
        setContentView(2131558439);
        SearchRingPhoneFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = SearchRingPhoneFragment.o.b();
            a((Fragment) b, SearchRingPhoneFragment.o.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new s55(b)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                SearchRingPhonePresenter searchRingPhonePresenter = this.B;
                if (searchRingPhonePresenter != null) {
                    String stringExtra = intent.getStringExtra("KEY_SELECTED_RINGPHONE");
                    wg6.a((Object) stringExtra, "it.getStringExtra(KEY_SELECTED_RINGPHONE)");
                    searchRingPhonePresenter.a(stringExtra);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null && (ringtone = (Ringtone) bundle.getParcelable("KEY_SELECTED_RINGPHONE")) != null) {
                SearchRingPhonePresenter searchRingPhonePresenter2 = this.B;
                if (searchRingPhonePresenter2 != null) {
                    searchRingPhonePresenter2.b(ringtone);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        SearchRingPhonePresenter searchRingPhonePresenter = this.B;
        if (searchRingPhonePresenter != null) {
            bundle.putParcelable("KEY_SELECTED_RINGPHONE", searchRingPhonePresenter.h());
            SearchRingPhoneActivity.super.onSaveInstanceState(bundle);
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }
}
