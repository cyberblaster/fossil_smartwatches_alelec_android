package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.jb;
import com.fossil.kb;
import com.fossil.o34;
import com.fossil.qg6;
import com.fossil.rb4;
import com.fossil.wg6;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAllowCallsAndMessagesFragment extends BaseBottomSheetDialogFragment {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a((qg6) null);
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public /* final */ jb s; // = new o34(this);
    @DexIgnore
    public ax5<rb4> t;
    @DexIgnore
    public b u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationAllowCallsAndMessagesFragment.w;
        }

        @DexIgnore
        public final NotificationAllowCallsAndMessagesFragment b() {
            return new NotificationAllowCallsAndMessagesFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z, boolean z2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAllowCallsAndMessagesFragment a;

        @DexIgnore
        public c(NotificationAllowCallsAndMessagesFragment notificationAllowCallsAndMessagesFragment) {
            this.a = notificationAllowCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.a.getDialog();
            if (dialog != null) {
                NotificationAllowCallsAndMessagesFragment notificationAllowCallsAndMessagesFragment = this.a;
                wg6.a((Object) dialog, "it");
                notificationAllowCallsAndMessagesFragment.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAllowCallsAndMessagesFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ rb4 b;

        @DexIgnore
        public d(NotificationAllowCallsAndMessagesFragment notificationAllowCallsAndMessagesFragment, rb4 rb4) {
            this.a = notificationAllowCallsAndMessagesFragment;
            this.b = rb4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r3v5, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r3v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r3v9, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        public final void onClick(View view) {
            Object r3 = this.b.u;
            wg6.a((Object) r3, "binding.ivCallsMessageCheck");
            if (r3.getVisibility() == 4) {
                Object r32 = this.b.u;
                wg6.a((Object) r32, "binding.ivCallsMessageCheck");
                r32.setVisibility(0);
                Object r33 = this.b.t;
                wg6.a((Object) r33, "binding.ivCallsCheck");
                r33.setVisibility(4);
                Object r34 = this.b.v;
                wg6.a((Object) r34, "binding.ivMessagesCheck");
                r34.setVisibility(4);
                this.a.p = true;
                this.a.o = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAllowCallsAndMessagesFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ rb4 b;

        @DexIgnore
        public e(NotificationAllowCallsAndMessagesFragment notificationAllowCallsAndMessagesFragment, rb4 rb4) {
            this.a = notificationAllowCallsAndMessagesFragment;
            this.b = rb4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v9, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        public final void onClick(View view) {
            Object r4 = this.b.t;
            wg6.a((Object) r4, "binding.ivCallsCheck");
            if (r4.getVisibility() == 4) {
                Object r42 = this.b.t;
                wg6.a((Object) r42, "binding.ivCallsCheck");
                r42.setVisibility(0);
                Object r43 = this.b.u;
                wg6.a((Object) r43, "binding.ivCallsMessageCheck");
                r43.setVisibility(4);
                Object r44 = this.b.v;
                wg6.a((Object) r44, "binding.ivMessagesCheck");
                r44.setVisibility(4);
                this.a.o = true;
                this.a.p = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAllowCallsAndMessagesFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ rb4 b;

        @DexIgnore
        public f(NotificationAllowCallsAndMessagesFragment notificationAllowCallsAndMessagesFragment, rb4 rb4) {
            this.a = notificationAllowCallsAndMessagesFragment;
            this.b = rb4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r4v9, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        public final void onClick(View view) {
            Object r4 = this.b.v;
            wg6.a((Object) r4, "binding.ivMessagesCheck");
            if (r4.getVisibility() == 4) {
                Object r42 = this.b.v;
                wg6.a((Object) r42, "binding.ivMessagesCheck");
                r42.setVisibility(0);
                Object r43 = this.b.u;
                wg6.a((Object) r43, "binding.ivCallsMessageCheck");
                r43.setVisibility(4);
                Object r44 = this.b.t;
                wg6.a((Object) r44, "binding.ivCallsCheck");
                r44.setVisibility(4);
                this.a.p = true;
                this.a.o = false;
            }
        }
    }

    /*
    static {
        String simpleName = NotificationAllowCallsAndMessagesFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationAllowCallsAn\u2026nt::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v9, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v3, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v14, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public final void g1() {
        if (this.p && this.o) {
            ax5<rb4> ax5 = this.t;
            if (ax5 != null) {
                rb4 a2 = ax5.a();
                if (a2 != null) {
                    Object r5 = a2.u;
                    wg6.a((Object) r5, "it.ivCallsMessageCheck");
                    r5.setVisibility(0);
                    Object r1 = a2.t;
                    wg6.a((Object) r1, "it.ivCallsCheck");
                    r1.setVisibility(4);
                    Object r0 = a2.v;
                    wg6.a((Object) r0, "it.ivMessagesCheck");
                    r0.setVisibility(4);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        } else if (this.o) {
            ax5<rb4> ax52 = this.t;
            if (ax52 != null) {
                rb4 a3 = ax52.a();
                if (a3 != null) {
                    Object r52 = a3.u;
                    wg6.a((Object) r52, "it.ivCallsMessageCheck");
                    r52.setVisibility(4);
                    Object r4 = a3.t;
                    wg6.a((Object) r4, "it.ivCallsCheck");
                    r4.setVisibility(0);
                    Object r02 = a3.v;
                    wg6.a((Object) r02, "it.ivMessagesCheck");
                    r02.setVisibility(4);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        } else {
            ax5<rb4> ax53 = this.t;
            if (ax53 != null) {
                rb4 a4 = ax53.a();
                if (a4 != null) {
                    Object r53 = a4.u;
                    wg6.a((Object) r53, "it.ivCallsMessageCheck");
                    r53.setVisibility(4);
                    Object r42 = a4.t;
                    wg6.a((Object) r42, "it.ivCallsCheck");
                    r42.setVisibility(4);
                    Object r03 = a4.v;
                    wg6.a((Object) r03, "it.ivMessagesCheck");
                    r03.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        rb4 a2 = kb.a(layoutInflater, 2131558576, viewGroup, false, this.s);
        a2.w.setOnClickListener(new c(this));
        this.t = new ax5<>(this, a2);
        g1();
        a2.r.setOnClickListener(new d(this, a2));
        a2.q.setOnClickListener(new e(this, a2));
        a2.s.setOnClickListener(new f(this, a2));
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        b bVar;
        wg6.b(dialogInterface, "dialog");
        if (!((this.q == this.o && this.r == this.p) || (bVar = this.u) == null)) {
            bVar.a(this.j, this.o, this.p);
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    public void onResume() {
        NotificationAllowCallsAndMessagesFragment.super.onResume();
        g1();
    }

    @DexIgnore
    public final void a(int i, boolean z, boolean z2) {
        this.j = i;
        this.o = z;
        this.p = z2;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "onDismissListener");
        this.u = bVar;
    }
}
