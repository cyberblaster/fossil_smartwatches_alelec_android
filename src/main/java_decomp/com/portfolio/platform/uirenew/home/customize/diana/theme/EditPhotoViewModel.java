package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.b75;
import com.fossil.b75$f$a;
import com.fossil.b75$g$a;
import com.fossil.b75$h$a;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.o35;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoViewModel extends td {
    @DexIgnore
    public MutableLiveData<b75.e> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<b75.b> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<b75.a> c; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<o35> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ FilterResult a;

        @DexIgnore
        public a(FilterResult filterResult) {
            wg6.b(filterResult, "filters");
            this.a = filterResult;
        }

        @DexIgnore
        public final FilterResult a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && wg6.a((Object) this.a, (Object) ((a) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            FilterResult filterResult = this.a;
            if (filterResult != null) {
                return filterResult.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "ApplyFiltersResult(filters=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ List<FilterResult> a;

        @DexIgnore
        public b(List<FilterResult> list) {
            wg6.b(list, "filters");
            this.a = list;
        }

        @DexIgnore
        public final List<FilterResult> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && wg6.a((Object) this.a, (Object) ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<FilterResult> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "BuildFiltersResult(filters=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ float[] b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public /* final */ boolean h;

        @DexIgnore
        public d(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
            wg6.b(bitmap, "bitmap");
            wg6.b(fArr, "points");
            this.a = bitmap;
            this.b = fArr;
            this.c = i;
            this.d = z;
            this.e = i2;
            this.f = i3;
            this.g = z2;
            this.h = z3;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.f;
        }

        @DexIgnore
        public final Bitmap c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final boolean e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.g;
        }

        @DexIgnore
        public final boolean g() {
            return this.h;
        }

        @DexIgnore
        public final float[] h() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public e() {
            this(false, (String) null, 3, (qg6) null);
        }

        @DexIgnore
        public e(boolean z, String str) {
            wg6.b(str, "data");
            this.a = z;
            this.b = str;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof e)) {
                return false;
            }
            e eVar = (e) obj;
            return this.a == eVar.a && wg6.a((Object) this.b, (Object) eVar.b);
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.a;
            if (z) {
                z = true;
            }
            int i = (z ? 1 : 0) * true;
            String str = this.b;
            return i + (str != null ? str.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "CropImageResult(isSuccess=" + this.a + ", data=" + this.b + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ e(boolean z, String str, int i, qg6 qg6) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? "" : str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1", f = "EditPhotoViewModel.kt", l = {74}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filter;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(EditPhotoViewModel editPhotoViewModel, Bitmap bitmap, FilterType filterType, xe6 xe6) {
            super(2, xe6);
            this.this$0 = editPhotoViewModel;
            this.$bitmap = bitmap;
            this.$filter = filterType;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$bitmap, this.$filter, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                b75$f$a b75_f_a = new b75$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, b75_f_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1", f = "EditPhotoViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ d $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $filterList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(EditPhotoViewModel editPhotoViewModel, d dVar, ArrayList arrayList, xe6 xe6) {
            super(2, xe6);
            this.this$0 = editPhotoViewModel;
            this.$cropImageInfo = dVar;
            this.$filterList = arrayList;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$cropImageInfo, this.$filterList, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                b75$g$a b75_g_a = new b75$g$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, b75_g_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            wg6.a(obj, "withContext(IO) {\n      \u2026          }\n            }");
            this.this$0.b().a(new b((ArrayList) obj));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1", f = "EditPhotoViewModel.kt", l = {38}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ d $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filterType;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(EditPhotoViewModel editPhotoViewModel, d dVar, FilterType filterType, Context context, xe6 xe6) {
            super(2, xe6);
            this.this$0 = editPhotoViewModel;
            this.$cropImageInfo = dVar;
            this.$filterType = filterType;
            this.$context = context;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, this.$cropImageInfo, this.$filterType, this.$context, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                b75$h$a b75_h_a = new b75$h$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, b75_h_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    /*
    static {
        new c((qg6) null);
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public final MutableLiveData<b75.a> a() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<b75.b> b() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<o35> c() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<b75.e> d() {
        return this.a;
    }

    @DexIgnore
    public void onCleared() {
        jl6.a(ud.a(this), (CancellationException) null, 1, (Object) null);
        EditPhotoViewModel.super.onCleared();
    }

    @DexIgnore
    public final void a(ArrayList<o35> arrayList) {
        this.d = arrayList;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(d dVar, FilterType filterType) {
        wg6.b(dVar, "cropImageInfo");
        wg6.b(filterType, "filterType");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoViewModel", "cropImage(), filterType = " + filterType);
        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new h(this, dVar, filterType, applicationContext, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(b75.d dVar, ArrayList<FilterType> arrayList) {
        wg6.b(dVar, "cropImageInfo");
        wg6.b(arrayList, "filterList");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "buildFilterList()");
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new g(this, dVar, arrayList, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Bitmap bitmap, FilterType filterType) {
        wg6.b(bitmap, "bitmap");
        wg6.b(filterType, "filter");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "applyFilter()");
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new f(this, bitmap, filterType, (xe6) null), 3, (Object) null);
    }
}
