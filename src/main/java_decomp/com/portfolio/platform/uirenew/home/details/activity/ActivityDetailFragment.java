package com.portfolio.platform.uirenew.home.details.activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.bl4;
import com.fossil.cf;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lh5;
import com.fossil.mh5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sh5;
import com.fossil.ut4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.yk4;
import com.fossil.z54;
import com.fossil.zg5;
import com.fossil.zh4;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDetailFragment extends BaseFragment implements mh5, View.OnClickListener {
    @DexIgnore
    public static /* final */ a x; // = new a((qg6) null);
    @DexIgnore
    public sh5 f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public ax5<z54> h;
    @DexIgnore
    public lh5 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ActivityDetailFragment a(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            ActivityDetailFragment activityDetailFragment = new ActivityDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            activityDetailFragment.setArguments(bundle);
            return activityDetailFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ cf b;

        @DexIgnore
        public b(ActivityDetailFragment activityDetailFragment, boolean z, cf cfVar, zh4 zh4) {
            this.a = z;
            this.b = cfVar;
        }

        @DexIgnore
        public boolean a(AppBarLayout appBarLayout) {
            wg6.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public ActivityDetailFragment() {
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        this.q = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("backgroundDashboard");
        this.r = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("secondaryText");
        this.s = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("primaryText");
        this.t = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("nonBrandDisableCalendarDay");
        this.u = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.v = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "ActivityDetailFragment";
    }

    @DexIgnore
    public final void j1() {
        z54 a2;
        OverviewDayChart overviewDayChart;
        ax5<z54> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.t) != null) {
            lh5 lh5 = this.i;
            if ((lh5 != null ? lh5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewDayChart.a("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("ActivityDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362542:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362543:
                    lh5 lh5 = this.i;
                    if (lh5 != null) {
                        lh5.k();
                        return;
                    }
                    return;
                case 2131362609:
                    lh5 lh52 = this.i;
                    if (lh52 != null) {
                        lh52.j();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        z54 a2;
        wg6.b(layoutInflater, "inflater");
        ActivityDetailFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        z54 a3 = kb.a(layoutInflater, 2131558499, viewGroup, false, e1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.g = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.g = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        wg6.a((Object) a3, "binding");
        a(a3);
        lh5 lh5 = this.i;
        if (lh5 != null) {
            lh5.a(this.g);
        }
        this.h = new ax5<>(this, a3);
        j1();
        ax5<z54> ax5 = this.h;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public void onDestroyView() {
        lh5 lh5 = this.i;
        if (lh5 != null) {
            lh5.i();
        }
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ActivityDetailFragment.super.onPause();
        lh5 lh5 = this.i;
        if (lh5 != null) {
            lh5.g();
        }
    }

    @DexIgnore
    public void onResume() {
        ActivityDetailFragment.super.onResume();
        lh5 lh5 = this.i;
        if (lh5 != null) {
            lh5.b(this.g);
        }
        lh5 lh52 = this.i;
        if (lh52 != null) {
            lh52.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        lh5 lh5 = this.i;
        if (lh5 != null) {
            lh5.a(bundle);
        }
        ActivityDetailFragment.super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a(z54 z54) {
        String str;
        z54.C.setOnClickListener(this);
        z54.D.setOnClickListener(this);
        z54.E.setOnClickListener(this);
        lh5 lh5 = this.i;
        if ((lh5 != null ? lh5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            str = ThemeManager.l.a().b("dianaStepsTab");
        } else {
            str = ThemeManager.l.a().b("hybridStepsTab");
        }
        this.j = str;
        this.o = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.p = ThemeManager.l.a().b("onDianaStepsTab");
        this.f = new sh5(sh5.c.STEPS, zh4.IMPERIAL, new WorkoutSessionDifference(), this.j);
        z54.G.setBackgroundColor(this.r);
        z54.r.setBackgroundColor(this.q);
        RecyclerView recyclerView = z54.J;
        wg6.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.f);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = w6.c(recyclerView.getContext(), 2131230877);
        if (c != null) {
            zg5 zg5 = new zg5(linearLayoutManager.Q(), false, false, 6, (qg6) null);
            wg6.a((Object) c, ResourceManager.DRAWABLE);
            zg5.a(c);
            recyclerView.addItemDecoration(zg5);
        }
    }

    @DexIgnore
    public void a(lh5 lh5) {
        wg6.b(lh5, "presenter");
        this.i = lh5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        z54 a2;
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3);
        this.g = date;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        int i2 = instance.get(7);
        ax5<z54> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            a2.q.a(true, true);
            Object r2 = a2.w;
            wg6.a((Object) r2, "binding.ftvDayOfMonth");
            r2.setText(String.valueOf(instance.get(5)));
            if (z) {
                Object r6 = a2.D;
                wg6.a((Object) r6, "binding.ivBackDate");
                r6.setVisibility(4);
            } else {
                Object r62 = a2.D;
                wg6.a((Object) r62, "binding.ivBackDate");
                r62.setVisibility(0);
            }
            if (z2 || z3) {
                Object r8 = a2.E;
                wg6.a((Object) r8, "binding.ivNextDate");
                r8.setVisibility(8);
                if (z2) {
                    Object r5 = a2.x;
                    wg6.a((Object) r5, "binding.ftvDayOfWeek");
                    r5.setText(jm4.a(getContext(), 2131886449));
                    return;
                }
                Object r63 = a2.x;
                wg6.a((Object) r63, "binding.ftvDayOfWeek");
                r63.setText(yk4.b.b(i2));
                return;
            }
            Object r7 = a2.E;
            wg6.a((Object) r7, "binding.ivNextDate");
            r7.setVisibility(0);
            Object r64 = a2.x;
            wg6.a((Object) r64, "binding.ftvDayOfWeek");
            r64.setText(yk4.b.b(i2));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v6, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r8v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v12, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v13, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r8v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v17, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v16, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v17, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r8v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v23, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r8v24, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v30, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v31, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r15v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v29, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(zh4 zh4, ActivitySummary activitySummary) {
        z54 a2;
        double d;
        int i2;
        int i3;
        int i4;
        String str;
        zh4 zh42 = zh4;
        ActivitySummary activitySummary2 = activitySummary;
        wg6.b(zh42, MFUser.DISTANCE_UNIT);
        FLogger.INSTANCE.getLocal().d("ActivityDetailFragment", "showDayDetail - distanceUnit=" + zh42 + ", activitySummary=" + activitySummary2);
        ax5<z54> ax5 = this.h;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return;
        }
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        Context context = d2.getContext();
        if (activitySummary2 != null) {
            i3 = (int) activitySummary.getSteps();
            i2 = activitySummary.getStepGoal();
            d = activitySummary.getDistance();
        } else {
            d = 0.0d;
            i3 = 0;
            i2 = 0;
        }
        if (i3 > 0) {
            Object r15 = a2.v;
            wg6.a((Object) r15, "binding.ftvDailyValue");
            r15.setText(bl4.a.b(Integer.valueOf(i3)));
            Object r7 = a2.u;
            wg6.a((Object) r7, "binding.ftvDailyUnit");
            String a3 = jm4.a(context, 2131886465);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
            if (a3 != null) {
                String lowerCase = a3.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                r7.setText(lowerCase);
                Object r2 = a2.y;
                wg6.a((Object) r2, "binding.ftvEst");
                if (zh42 == zh4.IMPERIAL) {
                    StringBuilder sb = new StringBuilder();
                    nh6 nh6 = nh6.a;
                    String a4 = jm4.a(context, 2131886466);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                    i4 = i3;
                    Object[] objArr = {bl4.a.a(Float.valueOf((float) d), zh42)};
                    String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    sb.append(format);
                    sb.append(" ");
                    sb.append(PortfolioApp.get.instance().getString(2131886616));
                    str = sb.toString();
                } else {
                    i4 = i3;
                    StringBuilder sb2 = new StringBuilder();
                    nh6 nh62 = nh6.a;
                    String a5 = jm4.a(context, 2131886466);
                    wg6.a((Object) a5, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                    Object[] objArr2 = {bl4.a.a(Float.valueOf((float) d), zh42)};
                    String format2 = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                    wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                    sb2.append(format2);
                    sb2.append(" ");
                    sb2.append(PortfolioApp.get.instance().getString(2131886615));
                    str = sb2.toString();
                }
                r2.setText(str);
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        } else {
            i4 = i3;
            Object r0 = a2.v;
            wg6.a((Object) r0, "binding.ftvDailyValue");
            r0.setText("");
            Object r02 = a2.u;
            wg6.a((Object) r02, "binding.ftvDailyUnit");
            String a6 = jm4.a(context, 2131886494);
            wg6.a((Object) a6, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
            if (a6 != null) {
                String upperCase = a6.toUpperCase();
                wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                r02.setText(upperCase);
                Object r03 = a2.y;
                wg6.a((Object) r03, "binding.ftvEst");
                r03.setText("");
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        int i5 = i2 > 0 ? (i4 * 100) / i2 : -1;
        int i6 = i4;
        if (i6 < i2 || i2 <= 0) {
            if (i6 > 0) {
                a2.w.setTextColor(this.t);
                a2.x.setTextColor(this.s);
                a2.u.setTextColor(this.v);
                a2.v.setTextColor(this.t);
                a2.y.setTextColor(this.t);
                View view = a2.F;
                wg6.a((Object) view, "binding.line");
                view.setSelected(false);
                Object r8 = a2.E;
                wg6.a((Object) r8, "binding.ivNextDate");
                r8.setSelected(false);
                Object r72 = a2.D;
                wg6.a((Object) r72, "binding.ivBackDate");
                r72.setSelected(false);
                int i7 = this.v;
                a2.F.setBackgroundColor(i7);
                a2.E.setColorFilter(i7);
                a2.D.setColorFilter(i7);
                String str2 = this.o;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else {
                a2.w.setTextColor(this.t);
                a2.x.setTextColor(this.s);
                a2.v.setTextColor(this.u);
                a2.u.setTextColor(this.u);
                Object r82 = a2.E;
                wg6.a((Object) r82, "binding.ivNextDate");
                r82.setSelected(false);
                Object r83 = a2.D;
                wg6.a((Object) r83, "binding.ivBackDate");
                r83.setSelected(false);
                ConstraintLayout constraintLayout = a2.s;
                wg6.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(false);
                Object r4 = a2.x;
                wg6.a((Object) r4, "binding.ftvDayOfWeek");
                r4.setSelected(false);
                Object r42 = a2.w;
                wg6.a((Object) r42, "binding.ftvDayOfMonth");
                r42.setSelected(false);
                View view2 = a2.F;
                wg6.a((Object) view2, "binding.line");
                view2.setSelected(false);
                Object r22 = a2.v;
                wg6.a((Object) r22, "binding.ftvDailyValue");
                r22.setSelected(false);
                Object r23 = a2.u;
                wg6.a((Object) r23, "binding.ftvDailyUnit");
                r23.setSelected(false);
                Object r24 = a2.y;
                wg6.a((Object) r24, "binding.ftvEst");
                r24.setSelected(false);
                int i8 = this.v;
                a2.F.setBackgroundColor(i8);
                a2.E.setColorFilter(i8);
                a2.D.setColorFilter(i8);
                String str3 = this.o;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            }
        } else {
            a2.x.setTextColor(this.q);
            a2.w.setTextColor(this.q);
            a2.u.setTextColor(this.q);
            a2.v.setTextColor(this.q);
            a2.y.setTextColor(this.q);
            Object r84 = a2.E;
            wg6.a((Object) r84, "binding.ivNextDate");
            r84.setSelected(true);
            Object r85 = a2.D;
            wg6.a((Object) r85, "binding.ivBackDate");
            r85.setSelected(true);
            ConstraintLayout constraintLayout2 = a2.s;
            wg6.a((Object) constraintLayout2, "binding.clOverviewDay");
            constraintLayout2.setSelected(true);
            Object r43 = a2.x;
            wg6.a((Object) r43, "binding.ftvDayOfWeek");
            r43.setSelected(true);
            Object r44 = a2.w;
            wg6.a((Object) r44, "binding.ftvDayOfMonth");
            r44.setSelected(true);
            View view3 = a2.F;
            wg6.a((Object) view3, "binding.line");
            view3.setSelected(true);
            Object r25 = a2.v;
            wg6.a((Object) r25, "binding.ftvDailyValue");
            r25.setSelected(true);
            Object r26 = a2.u;
            wg6.a((Object) r26, "binding.ftvDailyUnit");
            r26.setSelected(true);
            Object r27 = a2.y;
            wg6.a((Object) r27, "binding.ftvEst");
            r27.setSelected(true);
            String str4 = this.p;
            if (str4 != null) {
                a2.x.setTextColor(Color.parseColor(str4));
                a2.w.setTextColor(Color.parseColor(str4));
                a2.v.setTextColor(Color.parseColor(str4));
                a2.u.setTextColor(Color.parseColor(str4));
                a2.y.setTextColor(Color.parseColor(str4));
                a2.F.setBackgroundColor(Color.parseColor(str4));
                a2.E.setColorFilter(Color.parseColor(str4));
                a2.D.setColorFilter(Color.parseColor(str4));
            }
            String str5 = this.j;
            if (str5 != null) {
                a2.s.setBackgroundColor(Color.parseColor(str5));
            }
        }
        if (i5 == -1) {
            FlexibleProgressBar flexibleProgressBar = a2.H;
            wg6.a((Object) flexibleProgressBar, "binding.pbGoal");
            flexibleProgressBar.setProgress(0);
            Object r04 = a2.B;
            wg6.a((Object) r04, "binding.ftvProgressValue");
            r04.setText(jm4.a(context, 2131887067));
        } else {
            FlexibleProgressBar flexibleProgressBar2 = a2.H;
            wg6.a((Object) flexibleProgressBar2, "binding.pbGoal");
            flexibleProgressBar2.setProgress(i5);
            Object r05 = a2.B;
            wg6.a((Object) r05, "binding.ftvProgressValue");
            r05.setText(i5 + "%");
        }
        Object r06 = a2.z;
        wg6.a((Object) r06, "binding.ftvGoalValue");
        nh6 nh63 = nh6.a;
        String a7 = jm4.a(context, 2131886499);
        wg6.a((Object) a7, "LanguageHelper.getString\u2026age_Title__OfNumberSteps)");
        Object[] objArr3 = {bl4.a.b(Integer.valueOf(i2))};
        String format3 = String.format(a7, Arrays.copyOf(objArr3, objArr3.length));
        wg6.a((Object) format3, "java.lang.String.format(format, *args)");
        r06.setText(format3);
    }

    @DexIgnore
    public void a(ut4 ut4, ArrayList<String> arrayList) {
        z54 a2;
        OverviewDayChart overviewDayChart;
        wg6.b(ut4, "baseModel");
        wg6.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityDetailFragment", "showDayDetailChart - baseModel=" + ut4);
        ax5<z54> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.t) != null) {
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) yk4.b.a(), false, 2, (Object) null);
            }
            overviewDayChart.a(ut4);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(boolean z, zh4 zh4, cf<WorkoutSession> cfVar) {
        z54 a2;
        wg6.b(zh4, MFUser.DISTANCE_UNIT);
        wg6.b(cfVar, "workoutSessions");
        ax5<z54> ax5 = this.h;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.G;
                wg6.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                if (!cfVar.isEmpty()) {
                    Object r3 = a2.A;
                    wg6.a((Object) r3, "it.ftvNoWorkoutRecorded");
                    r3.setVisibility(8);
                    RecyclerView recyclerView = a2.J;
                    wg6.a((Object) recyclerView, "it.rvWorkout");
                    recyclerView.setVisibility(0);
                    sh5 sh5 = this.f;
                    if (sh5 != null) {
                        sh5.a(zh4, cfVar);
                    }
                } else {
                    Object r32 = a2.A;
                    wg6.a((Object) r32, "it.ftvNoWorkoutRecorded");
                    r32.setVisibility(0);
                    RecyclerView recyclerView2 = a2.J;
                    wg6.a((Object) recyclerView2, "it.rvWorkout");
                    recyclerView2.setVisibility(8);
                    sh5 sh52 = this.f;
                    if (sh52 != null) {
                        sh52.a(zh4, cfVar);
                    }
                }
            } else {
                LinearLayout linearLayout2 = a2.G;
                wg6.a((Object) linearLayout2, "it.llWorkout");
                linearLayout2.setVisibility(8);
            }
            AppBarLayout appBarLayout = a2.q;
            wg6.a((Object) appBarLayout, "it.appBarLayout");
            CoordinatorLayout.e layoutParams = appBarLayout.getLayoutParams();
            if (layoutParams != null) {
                CoordinatorLayout.e eVar = layoutParams;
                AppBarLayout.Behavior d = eVar.d();
                if (d == null) {
                    d = new AppBarLayout.Behavior();
                }
                d.setDragCallback(new b(this, z, cfVar, zh4));
                eVar.a(d);
                return;
            }
            throw new rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        }
    }
}
