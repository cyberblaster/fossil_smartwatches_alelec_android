package com.portfolio.platform.uirenew.home.customize.tutorial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.CustomizeTutorialFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeTutorialActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wg6.b(context, "context");
            wg6.b(str, "watchAppId");
            Intent intent = new Intent(context, CustomizeTutorialActivity.class);
            intent.putExtra("EXTRA_WATCHAPP_ID", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (getSupportFragmentManager().b(2131362119) == null) {
            String stringExtra = getIntent().getStringExtra("EXTRA_WATCHAPP_ID");
            CustomizeTutorialFragment.a aVar = CustomizeTutorialFragment.j;
            wg6.a((Object) stringExtra, "watchAppId");
            a((Fragment) aVar.a(stringExtra), "CustomizeTutorialFragment", 2131362119);
        }
    }
}
