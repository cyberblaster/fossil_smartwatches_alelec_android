package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import android.content.Intent;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.v85;
import com.fossil.vi4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zi4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetHybridPresetToWatchUseCase extends m24<v85.c, v85.d, v85.b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public c e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ HybridPresetRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            wg6.b(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ HybridPreset a;

        @DexIgnore
        public c(HybridPreset hybridPreset) {
            wg6.b(hybridPreset, "mPreset");
            this.a = hybridPreset;
        }

        @DexIgnore
        public final HybridPreset a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (SetHybridPresetToWatchUseCase.this.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_LINK_MAPPING) {
                    SetHybridPresetToWatchUseCase.this.a(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - success");
                        SetHybridPresetToWatchUseCase.this.f();
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - failed");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    SetHybridPresetToWatchUseCase.this.a(new b(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1", f = "SetHybridPresetToWatchUseCase.kt", l = {80}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SetHybridPresetToWatchUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = setHybridPresetToWatchUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            HybridPreset hybridPreset;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                HybridPreset a2 = SetHybridPresetToWatchUseCase.c(this.this$0).a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
                a2.setActive(true);
                HybridPresetRepository a3 = this.this$0.h;
                this.L$0 = il6;
                this.L$1 = a2;
                this.label = 1;
                if (a3.upsertHybridPreset(a2, this) == a) {
                    return a;
                }
                hybridPreset = a2;
            } else if (i == 1) {
                hybridPreset = (HybridPreset) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            String u = bk4.u(instance.getTime());
            Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting next = it.next();
                if (!vi4.a(next.getSettings())) {
                    SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase = this.this$0;
                    String appId = next.getAppId();
                    String settings = next.getSettings();
                    if (settings != null) {
                        setHybridPresetToWatchUseCase.a(appId, settings);
                        MicroAppLastSettingRepository b = this.this$0.j;
                        String appId2 = next.getAppId();
                        wg6.a((Object) u, "updatedAt");
                        String settings2 = next.getSettings();
                        if (settings2 != null) {
                            b.upsertMicroAppLastSetting(new MicroAppLastSetting(appId2, u, settings2));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            this.this$0.a(new d());
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SetHybridPresetToWatchUseCase(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(hybridPresetRepository, "mHybridPresetRepository");
        wg6.b(microAppRepository, "mMicroAppRepository");
        wg6.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        this.g = deviceRepository;
        this.h = hybridPresetRepository;
        this.i = microAppRepository;
        this.j = microAppLastSettingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ c c(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase) {
        c cVar = setHybridPresetToWatchUseCase.e;
        if (cVar != null) {
            return cVar;
        }
        wg6.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public String c() {
        return "SetHybridPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final rm6 f() {
        return ik6.b(b(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x007b, code lost:
        if (com.fossil.hf6.a(com.portfolio.platform.PortfolioApp.get.instance().c(r6, (java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping>) r7)) != null) goto L_0x0094;
     */
    @DexIgnore
    public Object a(v85.c cVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "executeUseCase");
        if (cVar != null) {
            this.e = cVar;
            this.d = true;
            String e2 = PortfolioApp.get.instance().e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("execute with preset ");
            c cVar2 = this.e;
            if (cVar2 != null) {
                sb.append(cVar2.a());
                local.d("SetHybridPresetToWatchUseCase", sb.toString());
                c cVar3 = this.e;
                if (cVar3 != null) {
                    List<MicroAppMapping> a2 = zi4.a(cVar3.a(), e2, this.g, this.i);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("SetHybridPresetToWatchUseCase", "set preset to watch with bleMappings " + a2);
                } else {
                    wg6.d("mRequestValues");
                    throw null;
                }
            } else {
                wg6.d("mRequestValues");
                throw null;
            }
        }
        a(new b(-1, new ArrayList()));
        return new Object();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        try {
            if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                PortfolioApp.get.instance().o(((SecondTimezoneSetting) new Gson().a(str2, SecondTimezoneSetting.class)).getTimeZoneId());
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SetHybridPresetToWatchUseCase", "setAutoSettingToWatch exception " + e2);
        }
    }
}
