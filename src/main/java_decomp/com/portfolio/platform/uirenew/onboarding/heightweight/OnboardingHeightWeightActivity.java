package com.portfolio.platform.uirenew.onboarding.heightweight;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rq5;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.OnboardingHeightWeightFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OnboardingHeightWeightActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public OnboardingHeightWeightPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            context.startActivity(new Intent(context, OnboardingHeightWeightActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        OnboardingHeightWeightFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = OnboardingHeightWeightFragment.j.b();
            a((Fragment) b, OnboardingHeightWeightFragment.j.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new rq5(b)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightContract.View");
    }
}
