package com.portfolio.platform.uirenew;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.e34;
import com.fossil.kb;
import com.fossil.oj3;
import com.fossil.ox5;
import com.fossil.p94;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xp5;
import com.fossil.yp5;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ExploreWatchFragment extends BaseFragment implements yp5 {
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public xp5 f;
    @DexIgnore
    public e34 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ String j; // = ThemeManager.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String o; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public ax5<p94> p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ExploreWatchFragment a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            ExploreWatchFragment exploreWatchFragment = new ExploreWatchFragment();
            exploreWatchFragment.setArguments(bundle);
            return exploreWatchFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchFragment a;

        @DexIgnore
        public b(ExploreWatchFragment exploreWatchFragment) {
            this.a = exploreWatchFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.j) && !TextUtils.isEmpty(this.a.o)) {
                int parseColor = Color.parseColor(this.a.j);
                int parseColor2 = Color.parseColor(this.a.o);
                gVar.b(2131231004);
                if (i == this.a.i) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ p94 a;
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchFragment b;

        @DexIgnore
        public c(p94 p94, ExploreWatchFragment exploreWatchFragment) {
            this.a = p94;
            this.b = exploreWatchFragment;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            ExploreWatchFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.o)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ExploreWatchFragment", "set icon color " + this.b.o);
                int parseColor = Color.parseColor(this.b.o);
                TabLayout.g b4 = this.a.r.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.j) && this.b.i != i) {
                int parseColor2 = Color.parseColor(this.b.j);
                TabLayout.g b5 = this.a.r.b(this.b.i);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.i = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchFragment a;

        @DexIgnore
        public d(ExploreWatchFragment exploreWatchFragment) {
            this.a = exploreWatchFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ExploreWatchFragment.c(this.a).h();
        }
    }

    @DexIgnore
    public ExploreWatchFragment() {
        ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    }

    @DexIgnore
    public static final /* synthetic */ xp5 c(ExploreWatchFragment exploreWatchFragment) {
        xp5 xp5 = exploreWatchFragment.f;
        if (xp5 != null) {
            return xp5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wg6.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g() {
        DashBar dashBar;
        ax5<p94> ax5 = this.p;
        if (ax5 != null) {
            p94 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.s) != null) {
                ox5.a aVar = ox5.a;
                wg6.a((Object) dashBar, "this");
                aVar.a(dashBar, this.h, 500);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "ExploreWatchFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ExploreWatchFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.p = new ax5<>(this, kb.a(layoutInflater, 2131558548, viewGroup, false, e1()));
        ax5<p94> ax5 = this.p;
        if (ax5 != null) {
            p94 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ExploreWatchFragment.super.onPause();
        e34 e34 = this.g;
        if (e34 != null) {
            e34.c();
            xp5 xp5 = this.f;
            if (xp5 != null) {
                xp5.g();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        ExploreWatchFragment.super.onResume();
        xp5 xp5 = this.f;
        if (xp5 != null) {
            xp5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v11, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        this.g = new e34();
        ax5<p94> ax5 = this.p;
        if (ax5 != null) {
            p94 a2 = ax5.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.t;
                wg6.a((Object) viewPager2, "binding.vpExplore");
                e34 e34 = this.g;
                if (e34 != null) {
                    viewPager2.setAdapter(e34);
                    if (a2.t.getChildAt(0) != null) {
                        RecyclerView childAt = a2.t.getChildAt(0);
                        if (childAt != null) {
                            childAt.setOverScrollMode(2);
                        } else {
                            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    TabLayout tabLayout = a2.r;
                    ViewPager2 viewPager22 = a2.t;
                    if (viewPager22 != null) {
                        new oj3(tabLayout, viewPager22, new b(this)).a();
                        a2.t.a(new c(a2, this));
                        a2.q.setOnClickListener(new d(this));
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.h = arguments.getBoolean("IS_ONBOARDING_FLOW");
                xp5 xp5 = this.f;
                if (xp5 != null) {
                    xp5.a(this.h);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u(List<? extends Explore> list) {
        wg6.b(list, "data");
        e34 e34 = this.g;
        if (e34 != null) {
            e34.a(list);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(xp5 xp5) {
        wg6.b(xp5, "presenter");
        this.f = xp5;
    }
}
