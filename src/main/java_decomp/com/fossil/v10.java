package com.fossil;

import com.fossil.a20;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v10 implements m10 {
    @DexIgnore
    public static /* final */ Set<a20.c> b; // = new a();
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends HashSet<a20.c> {
        @DexIgnore
        public a() {
            add(a20.c.START);
            add(a20.c.RESUME);
            add(a20.c.PAUSE);
            add(a20.c.STOP);
        }
    }

    @DexIgnore
    public v10(int i) {
        this.a = i;
    }

    @DexIgnore
    public boolean a(a20 a20) {
        boolean z = b.contains(a20.c) && a20.a.e == null;
        boolean z2 = Math.abs(a20.a.c.hashCode() % this.a) != 0;
        if (!z || !z2) {
            return false;
        }
        return true;
    }
}
