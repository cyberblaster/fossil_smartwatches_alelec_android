package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gn extends dn<ym> {
    @DexIgnore
    public static /* final */ String e; // = tl.a("NetworkNotRoamingCtrlr");

    @DexIgnore
    public gn(Context context, to toVar) {
        super(pn.a(context, toVar).c());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.b() == ul.NOT_ROAMING;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(ym ymVar) {
        if (Build.VERSION.SDK_INT < 24) {
            tl.a().a(e, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
            return !ymVar.a();
        } else if (!ymVar.a() || !ymVar.c()) {
            return true;
        } else {
            return false;
        }
    }
}
