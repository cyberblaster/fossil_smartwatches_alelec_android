package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jw6 implements wv6 {
    @DexIgnore
    public boolean a; // = false;
    @DexIgnore
    public /* final */ Map<String, iw6> b; // = new HashMap();
    @DexIgnore
    public /* final */ LinkedBlockingQueue<dw6> c; // = new LinkedBlockingQueue<>();

    @DexIgnore
    public synchronized xv6 a(String str) {
        iw6 iw6;
        iw6 = this.b.get(str);
        if (iw6 == null) {
            iw6 = new iw6(str, this.c, this.a);
            this.b.put(str, iw6);
        }
        return iw6;
    }

    @DexIgnore
    public LinkedBlockingQueue<dw6> b() {
        return this.c;
    }

    @DexIgnore
    public List<iw6> c() {
        return new ArrayList(this.b.values());
    }

    @DexIgnore
    public void d() {
        this.a = true;
    }

    @DexIgnore
    public void a() {
        this.b.clear();
        this.c.clear();
    }
}
