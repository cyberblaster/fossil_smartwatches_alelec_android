package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.util.DeviceUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1", f = "UpdateFirmwarePresenter.kt", l = {213, 216}, m = "invokeSuspend")
public final class br5$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ br5$b$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(br5$b$a br5_b_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = br5_b_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return hf6.a(DeviceUtils.g.a().b(this.this$0.this$0.b));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1", f = "UpdateFirmwarePresenter.kt", l = {216}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ br5$b$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(br5$b$a br5_b_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = br5_b_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                DeviceUtils a2 = DeviceUtils.g.a();
                UpdateFirmwarePresenter.b bVar = this.this$0.this$0;
                String str = bVar.b;
                Device device = bVar.c;
                this.L$0 = il6;
                this.label = 1;
                obj = a2.a(str, device, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public br5$b$a(UpdateFirmwarePresenter.b bVar, DownloadFirmwareByDeviceModelUsecase.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        br5$b$a br5_b_a = new br5$b$a(this.this$0, this.$responseValue, xe6);
        br5_b_a.p$ = (il6) obj;
        return br5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((br5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b0  */
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        String str;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il62 = this.p$;
            str = this.$responseValue.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = UpdateFirmwarePresenter.r.a();
            local.d(a3, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str);
            dl6 b2 = this.this$0.a.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il62;
            this.L$1 = str;
            this.label = 1;
            Object a4 = gk6.a(b2, aVar, this);
            if (a4 == a2) {
                return a2;
            }
            Object obj2 = a4;
            il6 = il62;
            obj = obj2;
        } else if (i == 1) {
            str = (String) this.L$1;
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            String str2 = (String) this.L$1;
            il6 il63 = (il6) this.L$0;
            nc6.a(obj);
            if (!((Boolean) obj).booleanValue()) {
                this.this$0.a.o().D0();
            } else {
                UpdateFirmwarePresenter.b bVar = this.this$0;
                bVar.a.c(bVar.c);
            }
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        if (!booleanValue) {
            dl6 b3 = this.this$0.a.b();
            b bVar2 = new b(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = str;
            this.Z$0 = booleanValue;
            this.label = 2;
            obj = gk6.a(b3, bVar2, this);
            if (obj == a2) {
                return a2;
            }
            if (!((Boolean) obj).booleanValue()) {
            }
            return cd6.a;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String a5 = UpdateFirmwarePresenter.r.a();
        local2.e(a5, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str + " but device is DianaEV1!!!");
        return cd6.a;
    }
}
