package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s94 extends r94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E; // = new SparseIntArray();
    @DexIgnore
    public long C;

    /*
    static {
        E.put(2131362542, 1);
        E.put(2131363218, 2);
        E.put(2131362566, 3);
        E.put(2131363139, 4);
        E.put(2131363198, 5);
        E.put(2131363173, 6);
        E.put(2131362385, 7);
        E.put(2131362986, 8);
        E.put(2131362341, 9);
        E.put(2131362046, 10);
        E.put(2131362042, 11);
        E.put(2131362380, 12);
        E.put(2131362381, 13);
        E.put(2131362378, 14);
        E.put(2131362379, 15);
    }
    */

    @DexIgnore
    public s94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 16, D, E));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public s94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[11], objArr[10], objArr[9], objArr[14], objArr[15], objArr[12], objArr[13], objArr[7], objArr[1], objArr[3], objArr[0], objArr[8], objArr[4], objArr[6], objArr[5], objArr[2]);
        this.C = -1;
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
