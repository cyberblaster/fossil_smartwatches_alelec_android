package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.fossil.a86;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c86 {
    @DexIgnore
    public static volatile c86 l;
    @DexIgnore
    public static /* final */ l86 m; // = new b86();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Map<Class<? extends i86>, i86> b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ f86<c86> d;
    @DexIgnore
    public /* final */ f86<?> e;
    @DexIgnore
    public /* final */ j96 f;
    @DexIgnore
    public a86 g;
    @DexIgnore
    public WeakReference<Activity> h;
    @DexIgnore
    public AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ l86 j;
    @DexIgnore
    public /* final */ boolean k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends a86.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Activity activity, Bundle bundle) {
            c86.this.a(activity);
        }

        @DexIgnore
        public void c(Activity activity) {
            c86.this.a(activity);
        }

        @DexIgnore
        public void d(Activity activity) {
            c86.this.a(activity);
        }
    }

    @DexIgnore
    public c86(Context context, Map<Class<? extends i86>, i86> map, aa6 aa6, Handler handler, l86 l86, boolean z, f86 f86, j96 j96, Activity activity) {
        this.a = context;
        this.b = map;
        this.c = aa6;
        this.j = l86;
        this.k = z;
        this.d = f86;
        this.e = a(map.size());
        this.f = j96;
        a(activity);
    }

    @DexIgnore
    public static c86 d(c86 c86) {
        if (l == null) {
            synchronized (c86.class) {
                if (l == null) {
                    c(c86);
                }
            }
        }
        return l;
    }

    @DexIgnore
    public static l86 g() {
        if (l == null) {
            return m;
        }
        return l.j;
    }

    @DexIgnore
    public static boolean h() {
        if (l == null) {
            return false;
        }
        return l.k;
    }

    @DexIgnore
    public static c86 i() {
        if (l != null) {
            return l;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    @DexIgnore
    public String c() {
        return "io.fabric.sdk.android:fabric";
    }

    @DexIgnore
    public String e() {
        return "1.4.8.32";
    }

    @DexIgnore
    public final void f() {
        this.g = new a86(this.a);
        this.g.a(new a());
        b(this.a);
    }

    @DexIgnore
    public static void c(c86 c86) {
        l = c86;
        c86.f();
    }

    @DexIgnore
    public void b(Context context) {
        StringBuilder sb;
        Future<Map<String, k86>> a2 = a(context);
        Collection<i86> d2 = d();
        m86 m86 = new m86(a2, d2);
        ArrayList<i86> arrayList = new ArrayList<>(d2);
        Collections.sort(arrayList);
        m86.a(context, this, f86.a, this.f);
        for (i86 a3 : arrayList) {
            a3.a(context, this, this.e, this.f);
        }
        m86.l();
        if (g().isLoggable("Fabric", 3)) {
            sb = new StringBuilder("Initializing ");
            sb.append(c());
            sb.append(" [Version: ");
            sb.append(e());
            sb.append("], with the following kits:\n");
        } else {
            sb = null;
        }
        for (i86 i86 : arrayList) {
            i86.b.a((ba6) m86.b);
            a(this.b, i86);
            i86.l();
            if (sb != null) {
                sb.append(i86.h());
                sb.append(" [Version: ");
                sb.append(i86.j());
                sb.append("]\n");
            }
        }
        if (sb != null) {
            g().d("Fabric", sb.toString());
        }
    }

    @DexIgnore
    public static c86 a(Context context, i86... i86Arr) {
        if (l == null) {
            synchronized (c86.class) {
                if (l == null) {
                    c cVar = new c(context);
                    cVar.a(i86Arr);
                    c(cVar.a());
                }
            }
        }
        return l;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements f86 {
        @DexIgnore
        public /* final */ CountDownLatch b; // = new CountDownLatch(this.c);
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public b(int i) {
            this.c = i;
        }

        @DexIgnore
        public void a(Object obj) {
            this.b.countDown();
            if (this.b.getCount() == 0) {
                c86.this.i.set(true);
                c86.this.d.a(c86.this);
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            c86.this.d.a(exc);
        }
    }

    @DexIgnore
    public static Activity d(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    @DexIgnore
    public c86 a(Activity activity) {
        this.h = new WeakReference<>(activity);
        return this;
    }

    @DexIgnore
    public Collection<i86> d() {
        return this.b.values();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public i86[] b;
        @DexIgnore
        public aa6 c;
        @DexIgnore
        public Handler d;
        @DexIgnore
        public l86 e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public f86<c86> i;

        @DexIgnore
        public c(Context context) {
            if (context != null) {
                this.a = context;
                return;
            }
            throw new IllegalArgumentException("Context must not be null.");
        }

        @DexIgnore
        public c a(i86... i86Arr) {
            if (this.b == null) {
                if (!c96.a(this.a).a()) {
                    ArrayList arrayList = new ArrayList();
                    boolean z = false;
                    for (i86 i86 : i86Arr) {
                        String h2 = i86.h();
                        char c2 = 65535;
                        int hashCode = h2.hashCode();
                        if (hashCode != 607220212) {
                            if (hashCode == 1830452504 && h2.equals("com.crashlytics.sdk.android:crashlytics")) {
                                c2 = 0;
                            }
                        } else if (h2.equals("com.crashlytics.sdk.android:answers")) {
                            c2 = 1;
                        }
                        if (c2 == 0 || c2 == 1) {
                            arrayList.add(i86);
                        } else if (!z) {
                            c86.g().w("Fabric", "Fabric will not initialize any kits when Firebase automatic data collection is disabled; to use Third-party kits with automatic data collection disabled, initialize these kits via non-Fabric means.");
                            z = true;
                        }
                    }
                    i86Arr = (i86[]) arrayList.toArray(new i86[0]);
                }
                this.b = i86Arr;
                return this;
            }
            throw new IllegalStateException("Kits already set.");
        }

        @DexIgnore
        public c a(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public c86 a() {
            Map map;
            if (this.c == null) {
                this.c = aa6.a();
            }
            if (this.d == null) {
                this.d = new Handler(Looper.getMainLooper());
            }
            if (this.e == null) {
                if (this.f) {
                    this.e = new b86(3);
                } else {
                    this.e = new b86();
                }
            }
            if (this.h == null) {
                this.h = this.a.getPackageName();
            }
            if (this.i == null) {
                this.i = f86.a;
            }
            i86[] i86Arr = this.b;
            if (i86Arr == null) {
                map = new HashMap();
            } else {
                map = c86.b((Collection<? extends i86>) Arrays.asList(i86Arr));
            }
            Map map2 = map;
            Context applicationContext = this.a.getApplicationContext();
            return new c86(applicationContext, map2, this.c, this.d, this.e, this.f, this.i, new j96(applicationContext, this.h, this.g, map2.values()), c86.d(this.a));
        }
    }

    @DexIgnore
    public Activity a() {
        WeakReference<Activity> weakReference = this.h;
        if (weakReference != null) {
            return (Activity) weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public void a(Map<Class<? extends i86>, i86> map, i86 i86) {
        t96 t96 = i86.f;
        if (t96 != null) {
            for (Class cls : t96.value()) {
                if (cls.isInterface()) {
                    for (i86 next : map.values()) {
                        if (cls.isAssignableFrom(next.getClass())) {
                            i86.b.a((ba6) next.b);
                        }
                    }
                } else if (map.get(cls) != null) {
                    i86.b.a((ba6) map.get(cls).b);
                } else {
                    throw new ca6("Referenced Kit was null, does the kit exist?");
                }
            }
        }
    }

    @DexIgnore
    public static <T extends i86> T a(Class<T> cls) {
        return (i86) i().b.get(cls);
    }

    @DexIgnore
    public static void a(Map<Class<? extends i86>, i86> map, Collection<? extends i86> collection) {
        for (i86 i86 : collection) {
            map.put(i86.getClass(), i86);
            if (i86 instanceof j86) {
                a(map, ((j86) i86).a());
            }
        }
    }

    @DexIgnore
    public ExecutorService b() {
        return this.c;
    }

    @DexIgnore
    public static Map<Class<? extends i86>, i86> b(Collection<? extends i86> collection) {
        HashMap hashMap = new HashMap(collection.size());
        a((Map<Class<? extends i86>, i86>) hashMap, collection);
        return hashMap;
    }

    @DexIgnore
    public f86<?> a(int i2) {
        return new b(i2);
    }

    @DexIgnore
    public Future<Map<String, k86>> a(Context context) {
        return b().submit(new e86(context.getPackageCodePath()));
    }
}
