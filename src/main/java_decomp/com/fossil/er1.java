package com.fossil;

import android.app.job.JobParameters;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class er1 implements Runnable {
    @DexIgnore
    public /* final */ JobInfoSchedulerService a;
    @DexIgnore
    public /* final */ JobParameters b;

    @DexIgnore
    public er1(JobInfoSchedulerService jobInfoSchedulerService, JobParameters jobParameters) {
        this.a = jobInfoSchedulerService;
        this.b = jobParameters;
    }

    @DexIgnore
    public static Runnable a(JobInfoSchedulerService jobInfoSchedulerService, JobParameters jobParameters) {
        return new er1(jobInfoSchedulerService, jobParameters);
    }

    @DexIgnore
    public void run() {
        this.a.jobFinished(this.b, false);
    }
}
