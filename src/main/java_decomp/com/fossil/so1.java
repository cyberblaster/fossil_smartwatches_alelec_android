package com.fossil;

import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so1 extends yw3<so1, b> implements to1 {
    @DexIgnore
    public static /* final */ so1 e; // = new so1();
    @DexIgnore
    public static volatile gx3<so1> f;
    @DexIgnore
    public zw3.c<yo1> d; // = yw3.i();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<so1, b> implements to1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b a(yo1 yo1) {
            d();
            so1.a((so1) this.b, yo1);
            return this;
        }

        @DexIgnore
        public b() {
            super(so1.e);
        }
    }

    /*
    static {
        e.g();
    }
    */

    @DexIgnore
    public static b k() {
        return (b) e.c();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new so1();
            case 2:
                return e;
            case 3:
                this.d.l();
                return null;
            case 4:
                return new b((a) null);
            case 5:
                this.d = ((yw3.k) obj).a(this.d, ((so1) obj2).d);
                yw3.i iVar = yw3.i.a;
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                boolean z = false;
                while (!z) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 10) {
                                if (!this.d.m()) {
                                    this.d = yw3.a(this.d);
                                }
                                this.d.add((yo1) tw3.a(yo1.l(), ww3));
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (f == null) {
                    synchronized (so1.class) {
                        if (f == null) {
                            f = new yw3.c(e);
                        }
                    }
                }
                return f;
            default:
                throw new UnsupportedOperationException();
        }
        return e;
    }

    @DexIgnore
    public int d() {
        int i = this.c;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.d.size(); i3++) {
            i2 += uw3.b(1, (dx3) this.d.get(i3));
        }
        this.c = i2;
        return i2;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        for (int i = 0; i < this.d.size(); i++) {
            uw3.a(1, (dx3) this.d.get(i));
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(so1 so1, yo1 yo1) {
        if (yo1 != null) {
            if (!so1.d.m()) {
                so1.d = yw3.a(so1.d);
            }
            so1.d.add(yo1);
            return;
        }
        throw new NullPointerException();
    }
}
