package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yv3 implements xv3 {
    @DexIgnore
    public static /* final */ Logger e; // = Logger.getLogger(yv3.class.getName());
    @DexIgnore
    public /* final */ Map<String, fw3> a;
    @DexIgnore
    public /* final */ Map<Integer, fw3> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ vv3 d;

    @DexIgnore
    public yv3(String str, vv3 vv3) {
        this.a = Collections.synchronizedMap(new HashMap());
        this.b = Collections.synchronizedMap(new HashMap());
        this.c = str;
        this.d = vv3;
    }

    @DexIgnore
    public fw3 a(String str) {
        synchronized (this.a) {
            if (!this.a.containsKey(str)) {
                a(str, 0);
            }
        }
        return this.a.get(str);
    }

    @DexIgnore
    public yv3(vv3 vv3) {
        this("/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto", vv3);
    }

    @DexIgnore
    public fw3 a(int i) {
        synchronized (this.b) {
            if (!this.b.containsKey(Integer.valueOf(i))) {
                List list = uv3.a().get(Integer.valueOf(i));
                if (list.size() == 1 && "001".equals(list.get(0))) {
                    a("001", i);
                }
            }
        }
        return this.b.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void a(String str, int i) {
        boolean equals = "001".equals(str);
        String valueOf = String.valueOf(String.valueOf(this.c));
        String valueOf2 = String.valueOf(String.valueOf(equals ? String.valueOf(i) : str));
        StringBuilder sb = new StringBuilder(valueOf.length() + 1 + valueOf2.length());
        sb.append(valueOf);
        sb.append("_");
        sb.append(valueOf2);
        String sb2 = sb.toString();
        InputStream a2 = this.d.a(sb2);
        if (a2 == null) {
            Logger logger = e;
            Level level = Level.SEVERE;
            String valueOf3 = String.valueOf(sb2);
            logger.log(level, valueOf3.length() != 0 ? "missing metadata: ".concat(valueOf3) : new String("missing metadata: "));
            String valueOf4 = String.valueOf(sb2);
            throw new IllegalStateException(valueOf4.length() != 0 ? "missing metadata: ".concat(valueOf4) : new String("missing metadata: "));
        }
        try {
            fw3[] fw3Arr = a(new ObjectInputStream(a2)).a;
            if (fw3Arr.length == 0) {
                Logger logger2 = e;
                Level level2 = Level.SEVERE;
                String valueOf5 = String.valueOf(sb2);
                logger2.log(level2, valueOf5.length() != 0 ? "empty metadata: ".concat(valueOf5) : new String("empty metadata: "));
                String valueOf6 = String.valueOf(sb2);
                throw new IllegalStateException(valueOf6.length() != 0 ? "empty metadata: ".concat(valueOf6) : new String("empty metadata: "));
            }
            if (fw3Arr.length > 1) {
                Logger logger3 = e;
                Level level3 = Level.WARNING;
                String valueOf7 = String.valueOf(sb2);
                logger3.log(level3, valueOf7.length() != 0 ? "invalid metadata (too many entries): ".concat(valueOf7) : new String("invalid metadata (too many entries): "));
            }
            fw3 fw3 = fw3Arr[0];
            if (equals) {
                this.b.put(Integer.valueOf(i), fw3);
            } else {
                this.a.put(str, fw3);
            }
        } catch (IOException e2) {
            Logger logger4 = e;
            Level level4 = Level.SEVERE;
            String valueOf8 = String.valueOf(sb2);
            logger4.log(level4, valueOf8.length() != 0 ? "cannot load/parse metadata: ".concat(valueOf8) : new String("cannot load/parse metadata: "), e2);
            String valueOf9 = String.valueOf(sb2);
            throw new RuntimeException(valueOf9.length() != 0 ? "cannot load/parse metadata: ".concat(valueOf9) : new String("cannot load/parse metadata: "), e2);
        }
    }

    @DexIgnore
    public static gw3 a(ObjectInputStream objectInputStream) {
        gw3 gw3 = new gw3();
        try {
            gw3.a(wv3.a(objectInputStream, 16384));
            try {
                objectInputStream.close();
            } catch (IOException e2) {
                e.log(Level.WARNING, "error closing input stream (ignored)", e2);
            }
        } catch (IOException e3) {
            e.log(Level.WARNING, "error reading input (ignored)", e3);
            objectInputStream.close();
        } catch (Throwable th) {
            try {
                objectInputStream.close();
            } catch (IOException e4) {
                e.log(Level.WARNING, "error closing input stream (ignored)", e4);
            }
            throw th;
        }
        return gw3;
    }
}
