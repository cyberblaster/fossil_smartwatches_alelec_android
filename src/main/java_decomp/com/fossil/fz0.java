package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz0 extends j61 {
    @DexIgnore
    public int A; // = 23;
    @DexIgnore
    public /* final */ int B;

    @DexIgnore
    public fz0(int i, ue1 ue1) {
        super(lx0.REQUEST_MTU, ue1);
        this.B = i;
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.A = ((l71) ok0).j;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.EXCHANGED_MTU, (Object) Integer.valueOf(this.A)), 7));
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.REQUESTED_MTU, (Object) Integer.valueOf(this.B));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.EXCHANGED_MTU, (Object) Integer.valueOf(this.A));
    }

    @DexIgnore
    public ok0 l() {
        return new l71(this.B, this.y.v);
    }
}
