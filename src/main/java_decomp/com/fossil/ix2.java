package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix2 {
    @DexIgnore
    public static qx2 a;

    @DexIgnore
    public static qx2 a() {
        qx2 qx2 = a;
        w12.a(qx2, (Object) "CameraUpdateFactory is not initialized");
        return qx2;
    }

    @DexIgnore
    public static void a(qx2 qx2) {
        w12.a(qx2);
        a = qx2;
    }

    @DexIgnore
    public static hx2 a(float f) {
        try {
            return new hx2(a().a(f));
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public static hx2 a(LatLng latLng, float f) {
        try {
            return new hx2(a().a(latLng, f));
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }
}
