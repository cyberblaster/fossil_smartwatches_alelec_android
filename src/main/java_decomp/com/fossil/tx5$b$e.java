package com.fossil;

import com.fossil.yx5;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.util.NetworkBoundResource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
public final class tx5$b$e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ap4 $response;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkBoundResource.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ld<S> {
        @DexIgnore
        public /* final */ /* synthetic */ tx5$b$e a;

        @DexIgnore
        public a(tx5$b$e tx5_b_e) {
            this.a = tx5_b_e;
        }

        @DexIgnore
        public final void onChanged(ResultType resulttype) {
            String str;
            tx5$b$e tx5_b_e = this.a;
            NetworkBoundResource networkBoundResource = tx5_b_e.this$0.this$0;
            yx5.a aVar = yx5.e;
            int a2 = ((zo4) tx5_b_e.$response).a();
            ServerError c = ((zo4) this.a.$response).c();
            if (c == null || (str = c.getUserMessage()) == null) {
                ServerError c2 = ((zo4) this.a.$response).c();
                str = c2 != null ? c2.getMessage() : null;
            }
            if (str == null) {
                str = "";
            }
            networkBoundResource.setValue(aVar.a(a2, str, resulttype));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tx5$b$e(NetworkBoundResource.b bVar, ap4 ap4, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$response = ap4;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        tx5$b$e tx5_b_e = new tx5$b$e(this.this$0, this.$response, xe6);
        tx5_b_e.p$ = (il6) obj;
        return tx5_b_e;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((tx5$b$e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.result.a(this.this$0.$dbSource, new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
