package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fo1<T> {
    @DexIgnore
    public static <T> fo1<T> a(T t) {
        return new eo1((Integer) null, t, go1.DEFAULT);
    }

    @DexIgnore
    public abstract Integer a();

    @DexIgnore
    public abstract T b();

    @DexIgnore
    public abstract go1 c();
}
