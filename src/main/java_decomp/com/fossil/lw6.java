package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lw6 implements mw6 {
    @DexIgnore
    public static /* final */ lw6 b; // = new lw6();
    @DexIgnore
    public static String c; // = "1.6.99";
    @DexIgnore
    public static /* final */ String d; // = gw6.class.getName();
    @DexIgnore
    public /* final */ wv6 a; // = new gw6();

    @DexIgnore
    public static final lw6 c() {
        return b;
    }

    @DexIgnore
    public wv6 a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return d;
    }
}
