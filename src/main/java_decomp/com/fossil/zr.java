package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zr<T, Z> {
    @DexIgnore
    rt<Z> a(T t, int i, int i2, xr xrVar) throws IOException;

    @DexIgnore
    boolean a(T t, xr xrVar) throws IOException;
}
