package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zx6 implements fx6<zq6, Boolean> {
    @DexIgnore
    public static /* final */ zx6 a; // = new zx6();

    @DexIgnore
    public Boolean a(zq6 zq6) throws IOException {
        return Boolean.valueOf(zq6.string());
    }
}
