package com.fossil;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.j256.ormlite.field.FieldType;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d36 implements SharedPreferences {
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ String[] b; // = {FieldType.FOREIGN_ID_FIELD_SUFFIX, "key", "type", ServerSetting.VALUE};
    @DexIgnore
    public /* final */ HashMap<String, Object> c; // = new HashMap<>();
    @DexIgnore
    public a d; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements SharedPreferences.Editor {
        @DexIgnore
        public Map<String, Object> a; // = new HashMap();
        @DexIgnore
        public Set<String> b; // = new HashSet();
        @DexIgnore
        public boolean c; // = false;
        @DexIgnore
        public ContentResolver d;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.d = contentResolver;
        }

        @DexIgnore
        public void apply() {
        }

        @DexIgnore
        public SharedPreferences.Editor clear() {
            this.c = true;
            return this;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0097  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0099  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00ae  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x003f A[SYNTHETIC] */
        public boolean commit() {
            int i;
            boolean z;
            String str;
            ContentValues contentValues = new ContentValues();
            if (this.c) {
                this.d.delete(l26.a, (String) null, (String[]) null);
                this.c = false;
            }
            for (String str2 : this.b) {
                this.d.delete(l26.a, "key = ?", new String[]{str2});
            }
            for (Map.Entry next : this.a.entrySet()) {
                Object value = next.getValue();
                if (value == null) {
                    str = "unresolve failed, null value";
                } else {
                    if (value instanceof Integer) {
                        i = 1;
                    } else if (value instanceof Long) {
                        i = 2;
                    } else if (value instanceof String) {
                        i = 3;
                    } else if (value instanceof Boolean) {
                        i = 4;
                    } else if (value instanceof Float) {
                        i = 5;
                    } else if (value instanceof Double) {
                        i = 6;
                    } else {
                        str = "unresolve failed, unknown type=" + value.getClass().toString();
                    }
                    if (i != 0) {
                        z = false;
                    } else {
                        contentValues.put("type", Integer.valueOf(i));
                        contentValues.put(ServerSetting.VALUE, value.toString());
                        z = true;
                    }
                    if (!z) {
                        this.d.update(l26.a, contentValues, "key = ?", new String[]{(String) next.getKey()});
                    }
                }
                h26.a("MicroMsg.SDK.PluginProvider.Resolver", str);
                i = 0;
                if (i != 0) {
                }
                if (!z) {
                }
            }
            return true;
        }

        @DexIgnore
        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            this.a.put(str, Boolean.valueOf(z));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putFloat(String str, float f) {
            this.a.put(str, Float.valueOf(f));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putInt(String str, int i) {
            this.a.put(str, Integer.valueOf(i));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putLong(String str, long j) {
            this.a.put(str, Long.valueOf(j));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putString(String str, String str2) {
            this.a.put(str, str2);
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            return null;
        }

        @DexIgnore
        public SharedPreferences.Editor remove(String str) {
            this.b.add(str);
            return this;
        }
    }

    @DexIgnore
    public d36(Context context) {
        this.a = context.getContentResolver();
    }

    @DexIgnore
    public final Object a(String str) {
        try {
            Cursor query = this.a.query(l26.a, this.b, "key = ?", new String[]{str}, (String) null);
            if (query == null) {
                return null;
            }
            Object a2 = query.moveToFirst() ? k26.a(query.getInt(query.getColumnIndex("type")), query.getString(query.getColumnIndex(ServerSetting.VALUE))) : null;
            query.close();
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public boolean contains(String str) {
        return a(str) != null;
    }

    @DexIgnore
    public SharedPreferences.Editor edit() {
        if (this.d == null) {
            this.d = new a(this.a);
        }
        return this.d;
    }

    @DexIgnore
    public Map<String, ?> getAll() {
        try {
            Cursor query = this.a.query(l26.a, this.b, (String) null, (String[]) null, (String) null);
            if (query == null) {
                return null;
            }
            int columnIndex = query.getColumnIndex("key");
            int columnIndex2 = query.getColumnIndex("type");
            int columnIndex3 = query.getColumnIndex(ServerSetting.VALUE);
            while (query.moveToNext()) {
                this.c.put(query.getString(columnIndex), k26.a(query.getInt(columnIndex2), query.getString(columnIndex3)));
            }
            query.close();
            return this.c;
        } catch (Exception e) {
            e.printStackTrace();
            return this.c;
        }
    }

    @DexIgnore
    public boolean getBoolean(String str, boolean z) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Boolean)) ? z : ((Boolean) a2).booleanValue();
    }

    @DexIgnore
    public float getFloat(String str, float f) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Float)) ? f : ((Float) a2).floatValue();
    }

    @DexIgnore
    public int getInt(String str, int i) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Integer)) ? i : ((Integer) a2).intValue();
    }

    @DexIgnore
    public long getLong(String str, long j) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Long)) ? j : ((Long) a2).longValue();
    }

    @DexIgnore
    public String getString(String str, String str2) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof String)) ? str2 : (String) a2;
    }

    @DexIgnore
    public Set<String> getStringSet(String str, Set<String> set) {
        return null;
    }

    @DexIgnore
    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    @DexIgnore
    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }
}
