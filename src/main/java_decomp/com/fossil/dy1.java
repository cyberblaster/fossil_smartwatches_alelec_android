package com.fossil;

import android.os.Looper;
import android.os.Message;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy1 extends bb2 {
    @DexIgnore
    public /* final */ /* synthetic */ ay1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dy1(ay1 ay1, Looper looper) {
        super(looper);
        this.a = ay1;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            this.a.n();
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GoogleApiClientImpl", sb.toString());
        } else {
            this.a.l();
        }
    }
}
