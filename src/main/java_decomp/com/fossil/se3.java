package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se3 implements Parcelable.Creator<re3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        int i = 0;
        byte[] bArr = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                i = f22.q(parcel, a);
            } else if (a2 == 3) {
                str = f22.e(parcel, a);
            } else if (a2 == 4) {
                bArr = f22.b(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                str2 = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new re3(i, str, bArr, str2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new re3[i];
    }
}
