package com.fossil;

import android.util.Log;
import com.fossil.fs;
import com.fossil.jv;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zu implements jv<File, ByteBuffer> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fs<ByteBuffer> {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public a(File file) {
            this.a = file;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super ByteBuffer> aVar) {
            try {
                aVar.a(h00.a(this.a));
            } catch (IOException e) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements kv<File, ByteBuffer> {
        @DexIgnore
        public jv<File, ByteBuffer> a(nv nvVar) {
            return new zu();
        }
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public jv.a<ByteBuffer> a(File file, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(file), new a(file));
    }
}
