package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h80 extends o80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<h80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public h80 createFromParcel(Parcel parcel) {
            return new h80(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new h80[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m22createFromParcel(Parcel parcel) {
            return new h80(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public h80(de0 de0) {
        super(q80.COMMUTE, (ce0) null, de0, 2);
    }

    @DexIgnore
    public JSONObject b() {
        JSONObject put = super.b().put("commuteApp._.config.destinations", cw0.a(getDataConfig().getDestinations()));
        wg6.a(put, "super.getDataConfigJSONO\u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    public final de0 getDataConfig() {
        ee0 ee0 = this.c;
        if (ee0 != null) {
            return (de0) ee0;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public h80(de0 de0, ce0 ce0) {
        super(q80.COMMUTE, ce0, de0);
    }

    @DexIgnore
    public /* synthetic */ h80(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
