package com.fossil;

import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb2 extends OutputStream {
    @DexIgnore
    public final String toString() {
        return "ByteStreams.nullOutputStream()";
    }

    @DexIgnore
    public final void write(int i) {
    }

    @DexIgnore
    public final void write(byte[] bArr) {
        mb2.a(bArr);
    }

    @DexIgnore
    public final void write(byte[] bArr, int i, int i2) {
        mb2.a(bArr);
    }
}
