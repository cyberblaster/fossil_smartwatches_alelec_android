package com.fossil;

import java.io.IOException;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx6<T> implements fx6<T, RequestBody> {
    @DexIgnore
    public static /* final */ yx6<Object> a; // = new yx6<>();
    @DexIgnore
    public static /* final */ uq6 b; // = uq6.b("text/plain; charset=UTF-8");

    @DexIgnore
    public RequestBody a(T t) throws IOException {
        return RequestBody.a(b, String.valueOf(t));
    }
}
