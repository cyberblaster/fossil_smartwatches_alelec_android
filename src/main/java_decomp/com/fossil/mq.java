package com.fossil;

import com.facebook.internal.Utility;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mq extends eq {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ SSLSocketFactory b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends FilterInputStream {
        @DexIgnore
        public /* final */ HttpURLConnection a;

        @DexIgnore
        public a(HttpURLConnection httpURLConnection) {
            super(mq.b(httpURLConnection));
            this.a = httpURLConnection;
        }

        @DexIgnore
        public void close() throws IOException {
            super.close();
            this.a.disconnect();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        String a(String str);
    }

    @DexIgnore
    public mq() {
        this((b) null);
    }

    @DexIgnore
    public static boolean a(int i, int i2) {
        return (i == 4 || (100 <= i2 && i2 < 200) || i2 == 204 || i2 == 304) ? false : true;
    }

    @DexIgnore
    public kq b(up<?> upVar, Map<String, String> map) throws IOException, hp {
        String str;
        String url = upVar.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.putAll(upVar.getHeaders());
        b bVar = this.a;
        if (bVar != null) {
            str = bVar.a(url);
            if (str == null) {
                throw new IOException("URL blocked by rewriter: " + url);
            }
        } else {
            str = url;
        }
        HttpURLConnection a2 = a(new URL(str), upVar);
        boolean z = false;
        try {
            for (String str2 : hashMap.keySet()) {
                a2.setRequestProperty(str2, (String) hashMap.get(str2));
            }
            b(a2, upVar);
            int responseCode = a2.getResponseCode();
            if (responseCode == -1) {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            } else if (!a(upVar.getMethod(), responseCode)) {
                kq kqVar = new kq(responseCode, a((Map<String, List<String>>) a2.getHeaderFields()));
                a2.disconnect();
                return kqVar;
            } else {
                z = true;
                return new kq(responseCode, a((Map<String, List<String>>) a2.getHeaderFields()), a2.getContentLength(), new a(a2));
            }
        } catch (Throwable th) {
            if (!z) {
                a2.disconnect();
            }
            throw th;
        }
    }

    @DexIgnore
    public mq(b bVar) {
        this(bVar, (SSLSocketFactory) null);
    }

    @DexIgnore
    public static List<np> a(Map<String, List<String>> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            if (next.getKey() != null) {
                for (String npVar : (List) next.getValue()) {
                    arrayList.add(new np((String) next.getKey(), npVar));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public mq(b bVar, SSLSocketFactory sSLSocketFactory) {
        this.a = bVar;
        this.b = sSLSocketFactory;
    }

    @DexIgnore
    public HttpURLConnection a(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }

    @DexIgnore
    public final HttpURLConnection a(URL url, up<?> upVar) throws IOException {
        SSLSocketFactory sSLSocketFactory;
        HttpURLConnection a2 = a(url);
        int timeoutMs = upVar.getTimeoutMs();
        a2.setConnectTimeout(timeoutMs);
        a2.setReadTimeout(timeoutMs);
        a2.setUseCaches(false);
        a2.setDoInput(true);
        if (Utility.URL_SCHEME.equals(url.getProtocol()) && (sSLSocketFactory = this.b) != null) {
            ((HttpsURLConnection) a2).setSSLSocketFactory(sSLSocketFactory);
        }
        return a2;
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, up<?> upVar) throws IOException, hp {
        byte[] body = upVar.getBody();
        if (body != null) {
            a(httpURLConnection, upVar, body);
        }
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, up<?> upVar, byte[] bArr) throws IOException {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey("Content-Type")) {
            httpURLConnection.setRequestProperty("Content-Type", upVar.getBodyContentType());
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(bArr);
        dataOutputStream.close();
    }

    @DexIgnore
    public static InputStream b(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException unused) {
            return httpURLConnection.getErrorStream();
        }
    }

    @DexIgnore
    public static void b(HttpURLConnection httpURLConnection, up<?> upVar) throws IOException, hp {
        switch (upVar.getMethod()) {
            case -1:
                byte[] postBody = upVar.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setRequestMethod("POST");
                    a(httpURLConnection, upVar, postBody);
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                a(httpURLConnection, upVar);
                return;
            case 2:
                httpURLConnection.setRequestMethod("PUT");
                a(httpURLConnection, upVar);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                return;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                return;
            case 7:
                httpURLConnection.setRequestMethod("PATCH");
                a(httpURLConnection, upVar);
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }
}
