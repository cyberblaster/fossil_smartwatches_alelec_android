package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.FitnessData;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ym0 extends if1 {
    @DexIgnore
    public /* final */ boolean B; // = true;
    @DexIgnore
    public FitnessData[] C; // = new FitnessData[0];
    @DexIgnore
    public byte[][] D;
    @DexIgnore
    public long E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public float H; // = 0.5f;
    @DexIgnore
    public float I; // = 0.5f;
    @DexIgnore
    public /* final */ boolean J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ int M;
    @DexIgnore
    public /* final */ ArrayList<hl1> N;
    @DexIgnore
    public /* final */ h60 O;
    @DexIgnore
    public /* final */ HashMap<io0, Object> P;

    @DexIgnore
    public ym0(ue1 ue1, q41 q41, h60 h60, HashMap<io0, Object> hashMap) {
        super(ue1, q41, eh1.SYNC_FLOW, (String) null, 8);
        this.O = h60;
        this.P = hashMap;
        Boolean bool = (Boolean) this.P.get(io0.SKIP_LIST);
        this.J = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) this.P.get(io0.SKIP_ERASE);
        this.K = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) this.P.get(io0.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.L = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) this.P.get(io0.NUMBER_OF_FILE_REQUIRED);
        this.M = num != null ? num.intValue() : 0;
        this.N = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.FILE_CONFIG, hl1.TRANSFER_DATA}));
    }

    @DexIgnore
    public boolean b() {
        return this.B;
    }

    @DexIgnore
    public Object d() {
        return this.C;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.N;
    }

    @DexIgnore
    public void h() {
        if (fm0.f.b(this.x.a())) {
            this.H = 1.0f;
            this.I = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            n();
            return;
        }
        if1.a((if1) this, (qv0) new te0(lk1.b.a(this.w.t, w31.ACTIVITY_FILE), this.w, 0, 4), (hg6) new pd1(this), (hg6) new lf1(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        JSONObject put = super.i().put(s60.BIOMETRIC_PROFILE.b(), this.O.d()).put(cw0.a((Enum<?>) io0.SKIP_LIST), this.J).put(cw0.a((Enum<?>) io0.SKIP_ERASE), this.K).put(cw0.a((Enum<?>) io0.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.L).put(cw0.a((Enum<?>) io0.NUMBER_OF_FILE_REQUIRED), this.M);
        wg6.a(put, "super.optionDescription(\u2026me, numberOfFileRequired)");
        return put;
    }

    @DexIgnore
    public final void m() {
        if1.a((if1) this, (qv0) new te0(lk1.b.a(this.w.t, w31.HARDWARE_LOG), this.w, 0, 4), (hg6) new hh1(this), (hg6) new bj1(this), (ig6) null, (hg6) new vk1(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public final void n() {
        if1.a((if1) this, (if1) new hx0(this.w, this.x, this.O, this.P, this.z), (hg6) new sh0(this), (hg6) new lj0(this), (ig6) new fl0(this), (hg6) null, (hg6) null, 48, (Object) null);
    }
}
