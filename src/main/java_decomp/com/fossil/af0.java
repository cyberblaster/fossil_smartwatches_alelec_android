package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum af0 {
    CONFIGURATION_FILE((byte) 0),
    REMOTE_ACTIVITY((byte) 8);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public af0(byte b) {
        this.a = b;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
