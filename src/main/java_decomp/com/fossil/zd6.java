package com.fossil;

import java.util.ListIterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zd6 implements ListIterator, ph6 {
    @DexIgnore
    public static /* final */ zd6 a; // = new zd6();

    @DexIgnore
    public /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean hasNext() {
        return false;
    }

    @DexIgnore
    public boolean hasPrevious() {
        return false;
    }

    @DexIgnore
    public int nextIndex() {
        return 0;
    }

    @DexIgnore
    public int previousIndex() {
        return -1;
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public Void next() {
        throw new NoSuchElementException();
    }

    @DexIgnore
    public Void previous() {
        throw new NoSuchElementException();
    }
}
