package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class y44 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon11 a;
    @DexIgnore
    private /* final */ /* synthetic */ String b;
    @DexIgnore
    private /* final */ /* synthetic */ List c;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetListCallback d;

    @DexIgnore
    public /* synthetic */ y44(PresetRepository.Anon11 anon11, String str, List list, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        this.a = anon11;
        this.b = str;
        this.c = list;
        this.d = getRecommendedPresetListCallback;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c, this.d);
    }
}
