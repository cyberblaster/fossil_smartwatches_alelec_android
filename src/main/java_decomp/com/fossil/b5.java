package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface b5<T> {
    @DexIgnore
    T a();

    @DexIgnore
    void a(T[] tArr, int i);

    @DexIgnore
    boolean a(T t);
}
