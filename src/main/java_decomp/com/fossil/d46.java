package com.fossil;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;

    @DexIgnore
    public d46(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void run() {
        try {
            new Thread(new j46(this.a, (Map<String, Integer>) null, (r36) null), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            q36.m.a(th);
            q36.a(this.a, th);
        }
    }
}
