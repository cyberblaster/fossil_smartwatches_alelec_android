package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om6 extends jk6 {
    @DexIgnore
    public /* final */ hg6<Throwable, cd6> a;

    @DexIgnore
    public om6(hg6<? super Throwable, cd6> hg6) {
        wg6.b(hg6, "handler");
        this.a = hg6;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.a.invoke(th);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancel[" + ol6.a((Object) this.a) + '@' + ol6.b(this) + ']';
    }
}
