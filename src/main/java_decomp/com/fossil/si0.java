package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class si0 extends em0 {
    @DexIgnore
    public static /* final */ mn1 CREATOR; // = new mn1((qg6) null);
    @DexIgnore
    public ah0 c;

    @DexIgnore
    public si0(byte b) {
        super(xh0.TIME_SYNC_EVENT, b);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a = super.a();
        bm0 bm0 = bm0.VALUE;
        ah0 ah0 = this.c;
        return cw0.a(a, bm0, (Object) ah0 != null ? ah0.a() : null);
    }

    @DexIgnore
    public byte[] b() {
        this.c = new ah0(System.currentTimeMillis());
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        ah0 ah0 = this.c;
        if (ah0 != null) {
            ByteBuffer putInt = order.putInt((int) ah0.a);
            ah0 ah02 = this.c;
            if (ah02 != null) {
                ByteBuffer putShort = putInt.putShort((short) ((int) ah02.b));
                ah0 ah03 = this.c;
                if (ah03 != null) {
                    byte[] array = putShort.putShort((short) ah03.c).array();
                    wg6.a(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
                    return array;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ si0(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
