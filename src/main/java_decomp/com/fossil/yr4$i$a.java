package com.fossil;

import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1", f = "SwitchActiveDeviceUseCase.kt", l = {74}, m = "invokeSuspend")
public final class yr4$i$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase.i this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yr4$i$a(SwitchActiveDeviceUseCase.i iVar, String str, MisfitDeviceProfile misfitDeviceProfile, xe6 xe6) {
        super(2, xe6);
        this.this$0 = iVar;
        this.$serial = str;
        this.$currentDeviceProfile = misfitDeviceProfile;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        yr4$i$a yr4_i_a = new yr4$i$a(this.this$0, this.$serial, this.$currentDeviceProfile, xe6);
        yr4_i_a.p$ = (il6) obj;
        return yr4_i_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((yr4$i$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0.a;
            String str = this.$serial;
            wg6.a((Object) str, "serial");
            MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                this.L$0 = il6;
                this.label = 1;
                obj = switchActiveDeviceUseCase.a(str, misfitDeviceProfile, this);
                if (obj == a) {
                    return a;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        lc6 lc6 = (lc6) obj;
        boolean booleanValue = ((Boolean) lc6.component1()).booleanValue();
        int intValue = ((Number) lc6.component2()).intValue();
        PortfolioApp instance = PortfolioApp.get.instance();
        String str2 = this.$serial;
        wg6.a((Object) str2, "serial");
        instance.a(str2, booleanValue, intValue);
        return cd6.a;
    }
}
