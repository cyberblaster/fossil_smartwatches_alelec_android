package com.fossil;

import android.content.Context;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vb6 {
    @DexIgnore
    public /* final */ AtomicReference<yb6> a;
    @DexIgnore
    public /* final */ CountDownLatch b;
    @DexIgnore
    public xb6 c;
    @DexIgnore
    public boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ vb6 a; // = new vb6();
    }

    @DexIgnore
    public static vb6 d() {
        return b.a;
    }

    @DexIgnore
    public synchronized vb6 a(i86 i86, j96 j96, va6 va6, String str, String str2, String str3, c96 c96) {
        i86 i862 = i86;
        synchronized (this) {
            if (this.d) {
                return this;
            }
            if (this.c == null) {
                Context d2 = i86.d();
                String d3 = j96.d();
                String d4 = new x86().d(d2);
                String g = j96.g();
                n96 n96 = new n96();
                pb6 pb6 = new pb6();
                nb6 nb6 = new nb6(i862);
                String c2 = z86.c(d2);
                String str4 = str3;
                qb6 qb6 = new qb6(i862, str4, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", new Object[]{d3}), va6);
                String h = j96.h();
                String str5 = h;
                String str6 = str2;
                String str7 = str;
                i86 i863 = i86;
                this.c = new ob6(i863, new bc6(d4, str5, j96.i(), j96.j(), j96.e(), z86.a(z86.n(d2)), str6, str7, d96.determineFrom(g).getId(), c2), n96, pb6, nb6, qb6, c96);
            }
            this.d = true;
            return this;
        }
    }

    @DexIgnore
    public synchronized boolean b() {
        yb6 a2;
        a2 = this.c.a();
        a(a2);
        return a2 != null;
    }

    @DexIgnore
    public synchronized boolean c() {
        yb6 a2;
        a2 = this.c.a(wb6.SKIP_CACHE_LOOKUP);
        a(a2);
        if (a2 == null) {
            c86.g().e("Fabric", "Failed to force reload of settings from Crashlytics.", (Throwable) null);
        }
        return a2 != null;
    }

    @DexIgnore
    public vb6() {
        this.a = new AtomicReference<>();
        this.b = new CountDownLatch(1);
        this.d = false;
    }

    @DexIgnore
    public yb6 a() {
        try {
            this.b.await();
            return this.a.get();
        } catch (InterruptedException unused) {
            c86.g().e("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    @DexIgnore
    public final void a(yb6 yb6) {
        this.a.set(yb6);
        this.b.countDown();
    }
}
