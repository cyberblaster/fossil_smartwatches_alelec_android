package com.fossil;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cb3 extends t63 {
    @DexIgnore
    public Boolean b;
    @DexIgnore
    public eb3 c; // = wz2.a;
    @DexIgnore
    public Boolean d;

    @DexIgnore
    public cb3(x53 x53) {
        super(x53);
        l03.a(x53);
    }

    @DexIgnore
    public static String v() {
        return l03.f.a(null);
    }

    @DexIgnore
    public static long w() {
        return l03.I.a(null).longValue();
    }

    @DexIgnore
    public static long x() {
        return l03.i.a(null).longValue();
    }

    @DexIgnore
    public static boolean y() {
        return l03.e.a(null).booleanValue();
    }

    @DexIgnore
    public final void a(eb3 eb3) {
        this.c = eb3;
    }

    @DexIgnore
    public final int b(String str, m43<Integer> m43) {
        if (str == null) {
            return m43.a(null).intValue();
        }
        String a = this.c.a(str, m43.a());
        if (TextUtils.isEmpty(a)) {
            return m43.a(null).intValue();
        }
        try {
            return m43.a(Integer.valueOf(Integer.parseInt(a))).intValue();
        } catch (NumberFormatException unused) {
            return m43.a(null).intValue();
        }
    }

    @DexIgnore
    public final double c(String str, m43<Double> m43) {
        if (str == null) {
            return m43.a(null).doubleValue();
        }
        String a = this.c.a(str, m43.a());
        if (TextUtils.isEmpty(a)) {
            return m43.a(null).doubleValue();
        }
        try {
            return m43.a(Double.valueOf(Double.parseDouble(a))).doubleValue();
        } catch (NumberFormatException unused) {
            return m43.a(null).doubleValue();
        }
    }

    @DexIgnore
    public final boolean d(String str, m43<Boolean> m43) {
        if (str == null) {
            return m43.a(null).booleanValue();
        }
        String a = this.c.a(str, m43.a());
        if (TextUtils.isEmpty(a)) {
            return m43.a(null).booleanValue();
        }
        return m43.a(Boolean.valueOf(Boolean.parseBoolean(a))).booleanValue();
    }

    @DexIgnore
    public final boolean e(String str, m43<Boolean> m43) {
        return d(str, m43);
    }

    @DexIgnore
    public final boolean f(String str) {
        return d(str, l03.S);
    }

    @DexIgnore
    public final boolean g(String str) {
        return d(str, l03.M);
    }

    @DexIgnore
    public final String h(String str) {
        m43<String> m43 = l03.N;
        if (str == null) {
            return m43.a(null);
        }
        return m43.a(this.c.a(str, m43.a()));
    }

    @DexIgnore
    public final boolean i(String str) {
        return d(str, l03.T);
    }

    @DexIgnore
    public final boolean j(String str) {
        return d(str, l03.V);
    }

    @DexIgnore
    public final boolean k(String str) {
        return d(str, l03.U);
    }

    @DexIgnore
    public final boolean l(String str) {
        return d(str, l03.W);
    }

    @DexIgnore
    public final Bundle m() {
        try {
            if (c().getPackageManager() == null) {
                b().t().a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a = g52.b(c()).a(c().getPackageName(), 128);
            if (a != null) {
                return a.metaData;
            }
            b().t().a("Failed to load metadata: ApplicationInfo is null");
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            b().t().a("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    @DexIgnore
    public final long n() {
        d();
        return 18202;
    }

    @DexIgnore
    public final boolean o() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    ApplicationInfo applicationInfo = c().getApplicationInfo();
                    String a = t42.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.d = Boolean.valueOf(str != null && str.equals(a));
                    }
                    if (this.d == null) {
                        this.d = Boolean.TRUE;
                        b().t().a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.d.booleanValue();
    }

    @DexIgnore
    public final boolean p() {
        d();
        Boolean b2 = b("firebase_analytics_collection_deactivated");
        return b2 != null && b2.booleanValue();
    }

    @DexIgnore
    public final Boolean q() {
        d();
        return b("firebase_analytics_collection_enabled");
    }

    @DexIgnore
    public final Boolean r() {
        e();
        Boolean b2 = b("google_analytics_adid_collection_enabled");
        return Boolean.valueOf(b2 == null || b2.booleanValue());
    }

    @DexIgnore
    public final String s() {
        return a("debug.firebase.analytics.app", "");
    }

    @DexIgnore
    public final String t() {
        return a("debug.deferred.deeplink", "");
    }

    @DexIgnore
    public final boolean u() {
        if (this.b == null) {
            this.b = b("app_measurement_lite");
            if (this.b == null) {
                this.b = false;
            }
        }
        if (this.b.booleanValue() || !this.a.D()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int a(String str) {
        return b(str, l03.t);
    }

    @DexIgnore
    public final boolean e(String str) {
        return "1".equals(this.c.a(str, "measurement.event_sampling_enabled"));
    }

    @DexIgnore
    public final boolean n(String str) {
        return d(str, l03.Y);
    }

    @DexIgnore
    public final long a(String str, m43<Long> m43) {
        if (str == null) {
            return m43.a(null).longValue();
        }
        String a = this.c.a(str, m43.a());
        if (TextUtils.isEmpty(a)) {
            return m43.a(null).longValue();
        }
        try {
            return m43.a(Long.valueOf(Long.parseLong(a))).longValue();
        } catch (NumberFormatException unused) {
            return m43.a(null).longValue();
        }
    }

    @DexIgnore
    public final boolean p(String str) {
        return d(str, l03.g0);
    }

    @DexIgnore
    public final boolean m(String str) {
        return d(str, l03.X);
    }

    @DexIgnore
    public final boolean d(String str) {
        return "1".equals(this.c.a(str, "gaia_collection_enabled"));
    }

    @DexIgnore
    public final Boolean b(String str) {
        w12.b(str);
        Bundle m = m();
        if (m == null) {
            b().t().a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (!m.containsKey(str)) {
            return null;
        } else {
            return Boolean.valueOf(m.getBoolean(str));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b A[SYNTHETIC, Splitter:B:9:0x002b] */
    public final List<String> c(String str) {
        Integer num;
        w12.b(str);
        Bundle m = m();
        if (m == null) {
            b().t().a("Failed to load metadata: Metadata bundle is null");
        } else if (m.containsKey(str)) {
            num = Integer.valueOf(m.getInt(str));
            if (num != null) {
                return null;
            }
            try {
                String[] stringArray = c().getResources().getStringArray(num.intValue());
                if (stringArray == null) {
                    return null;
                }
                return Arrays.asList(stringArray);
            } catch (Resources.NotFoundException e) {
                b().t().a("Failed to load string array from metadata: resource not found", e);
                return null;
            }
        }
        num = null;
        if (num != null) {
        }
    }

    @DexIgnore
    public final boolean a(m43<Boolean> m43) {
        return d((String) null, m43);
    }

    @DexIgnore
    public final boolean o(String str) {
        return d(str, l03.Z);
    }

    @DexIgnore
    public final String a(String str, String str2) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke((Object) null, new Object[]{str, str2});
        } catch (ClassNotFoundException e) {
            b().t().a("Could not find SystemProperties class", e);
            return str2;
        } catch (NoSuchMethodException e2) {
            b().t().a("Could not find SystemProperties.get() method", e2);
            return str2;
        } catch (IllegalAccessException e3) {
            b().t().a("Could not access SystemProperties.get()", e3);
            return str2;
        } catch (InvocationTargetException e4) {
            b().t().a("SystemProperties.get() threw an exception", e4);
            return str2;
        }
    }
}
