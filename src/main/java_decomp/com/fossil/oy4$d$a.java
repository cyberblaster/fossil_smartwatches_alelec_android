package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
public final class oy4$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ oy4$d$a a;

        @DexIgnore
        public a(oy4$d$a oy4_d_a) {
            this.a = oy4_d_a;
        }

        @DexIgnore
        public final void run() {
            this.a.this$0.this$0.C.getNotificationSettingsDao().insertListNotificationSettings(this.a.this$0.$settings);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oy4$d$a(NotificationCallsAndMessagesPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        oy4$d$a oy4_d_a = new oy4$d$a(this.this$0, xe6);
        oy4_d_a.p$ = (il6) obj;
        return oy4_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((oy4$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.C.runInTransaction(new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
