package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wq1 implements Factory<fr1> {
    @DexIgnore
    public /* final */ Provider<zs1> a;

    @DexIgnore
    public wq1(Provider<zs1> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static wq1 a(Provider<zs1> provider) {
        return new wq1(provider);
    }

    @DexIgnore
    public static fr1 a(zs1 zs1) {
        fr1 a2 = vq1.a(zs1);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public fr1 get() {
        return a((zs1) this.a.get());
    }
}
