package com.fossil;

import com.portfolio.platform.uirenew.home.HomeDashboardFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw4 implements MembersInjector<HomeDashboardFragment> {
    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardActivityPresenter dashboardActivityPresenter) {
        homeDashboardFragment.E = dashboardActivityPresenter;
    }

    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardActiveTimePresenter dashboardActiveTimePresenter) {
        homeDashboardFragment.F = dashboardActiveTimePresenter;
    }

    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardCaloriesPresenter dashboardCaloriesPresenter) {
        homeDashboardFragment.G = dashboardCaloriesPresenter;
    }

    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardHeartRatePresenter dashboardHeartRatePresenter) {
        homeDashboardFragment.H = dashboardHeartRatePresenter;
    }

    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardSleepPresenter dashboardSleepPresenter) {
        homeDashboardFragment.I = dashboardSleepPresenter;
    }

    @DexIgnore
    public static void a(HomeDashboardFragment homeDashboardFragment, DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
        homeDashboardFragment.J = dashboardGoalTrackingPresenter;
    }
}
