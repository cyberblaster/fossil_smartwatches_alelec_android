package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f53 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ ea3 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    /*
    static {
        Class<f53> cls = f53.class;
    }
    */

    @DexIgnore
    public f53(ea3 ea3) {
        w12.a(ea3);
        this.a = ea3;
    }

    @DexIgnore
    public final void a() {
        this.a.r();
        this.a.a().g();
        if (!this.b) {
            this.a.c().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.c = this.a.k().u();
            this.a.b().B().a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.c));
            this.b = true;
        }
    }

    @DexIgnore
    public final void b() {
        this.a.r();
        this.a.a().g();
        this.a.a().g();
        if (this.b) {
            this.a.b().B().a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.a.c().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.a.b().t().a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        this.a.r();
        String action = intent.getAction();
        this.a.b().B().a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean u = this.a.k().u();
            if (this.c != u) {
                this.c = u;
                this.a.a().a((Runnable) new e53(this, u));
                return;
            }
            return;
        }
        this.a.b().w().a("NetworkBroadcastReceiver received unknown action", action);
    }
}
