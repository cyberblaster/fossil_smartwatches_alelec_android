package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ry3 extends yy3 {
    @DexIgnore
    public ry3() {
        super(false, 1558, 620, 22, 22, 36, -1, 62);
    }

    @DexIgnore
    public int a(int i) {
        return i <= 8 ? 156 : 155;
    }

    @DexIgnore
    public int d() {
        return 10;
    }
}
