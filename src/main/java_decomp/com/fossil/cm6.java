package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cm6 extends xm6<rm6> {
    @DexIgnore
    public /* final */ am6 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cm6(rm6 rm6, am6 am6) {
        super(rm6);
        wg6.b(rm6, "job");
        wg6.b(am6, "handle");
        this.e = am6;
    }

    @DexIgnore
    public void b(Throwable th) {
        this.e.dispose();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCompletion[" + this.e + ']';
    }
}
