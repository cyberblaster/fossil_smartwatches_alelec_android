package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import java.util.Iterator;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ql0 extends BluetoothGattServerCallback {
    @DexIgnore
    public /* final */ /* synthetic */ fu0 a;

    @DexIgnore
    public ql0(fu0 fu0) {
        this.a = fu0;
    }

    @DexIgnore
    public void onCharacteristicReadRequest(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        BluetoothGattServer bluetoothGattServer;
        T t;
        BluetoothDevice bluetoothDevice2 = bluetoothDevice;
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice2, Integer.valueOf(i), Integer.valueOf(i2), bluetoothGattCharacteristic.getUuid()};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) pj1.CHARACTERISTIC_READ);
        og0 og0 = og0.GATT_SERVER_EVENT;
        String address = bluetoothDevice.getAddress();
        wg6.a(address, "device.address");
        String uuid = UUID.randomUUID().toString();
        wg6.a(uuid, "UUID.randomUUID().toString()");
        qs0.a(new nn0(a2, og0, address, "", uuid, true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.REQUEST_ID, (Object) Integer.valueOf(i)), bm0.OFFSET, (Object) Integer.valueOf(i2)), bm0.CHARACTERISTIC, (Object) bluetoothGattCharacteristic.getUuid().toString()), 448));
        Integer num = null;
        if ((4 | bluetoothGattCharacteristic.getPermissions() | 1 | 2) != 0) {
            Iterator<T> it = this.a.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                h11 h11 = (h11) t;
                wg6.a(h11, "service");
                UUID uuid2 = h11.getUuid();
                BluetoothGattService service = bluetoothGattCharacteristic.getService();
                if (wg6.a(uuid2, service != null ? service.getUuid() : null)) {
                    break;
                }
            }
            h11 h112 = (h11) t;
            if (h112 == null) {
                num = 257;
                int i3 = i;
            } else if (!h112.a((an1) new ve0(bluetoothDevice2, i, i2, bluetoothGattCharacteristic))) {
                num = 6;
            }
        } else {
            int i4 = i;
            num = 2;
        }
        if (num != null && (bluetoothGattServer = this.a.a) != null) {
            bluetoothGattServer.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
        }
    }

    @DexIgnore
    public void onCharacteristicWriteRequest(BluetoothDevice bluetoothDevice, int i, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z, boolean z2, int i2, byte[] bArr) {
        BluetoothGattServer bluetoothGattServer;
        T t;
        byte[] bArr2 = bArr;
        oa1 oa1 = oa1.a;
        Integer num = null;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i), bluetoothGattCharacteristic.getUuid(), Boolean.valueOf(z), Boolean.valueOf(z2), Integer.valueOf(i2), cw0.a(bArr2, (String) null, 1)};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) pj1.CHARACTERISTIC_WRITE);
        og0 og0 = og0.GATT_SERVER_EVENT;
        String address = bluetoothDevice.getAddress();
        wg6.a(address, "device.address");
        String uuid = UUID.randomUUID().toString();
        wg6.a(uuid, "UUID.randomUUID().toString()");
        qs0.a(new nn0(a2, og0, address, "", uuid, true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.REQUEST_ID, (Object) Integer.valueOf(i)), bm0.CHARACTERISTIC, (Object) bluetoothGattCharacteristic.getUuid().toString()), bm0.PREPARED_WRITE, (Object) Boolean.valueOf(z)), bm0.RESPONSE_NEEDED, (Object) Boolean.valueOf(z2)), bm0.OFFSET, (Object) Integer.valueOf(i2)), bm0.VALUE, (Object) cw0.a(bArr2, (String) null, 1)), 448));
        if ((bluetoothGattCharacteristic.getPermissions() | 16 | 32 | 64 | 128 | 256) != 0) {
            Iterator<T> it = this.a.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                h11 h11 = (h11) t;
                wg6.a(h11, "service");
                UUID uuid2 = h11.getUuid();
                BluetoothGattService service = bluetoothGattCharacteristic.getService();
                if (wg6.a(uuid2, service != null ? service.getUuid() : null)) {
                    break;
                }
            }
            h11 h112 = (h11) t;
            if (h112 == null) {
                num = 257;
            } else if (!h112.a((an1) new di0(bluetoothDevice, i, bluetoothGattCharacteristic, z, z2, i2, bArr))) {
                num = 6;
            }
        } else {
            num = 3;
        }
        if (num != null && z2 && (bluetoothGattServer = this.a.a) != null) {
            bluetoothGattServer.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
        }
    }

    @DexIgnore
    public void onConnectionStateChange(BluetoothDevice bluetoothDevice, int i, int i2) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i), Integer.valueOf(i2)};
        for (h11 a2 : this.a.b) {
            a2.a(bluetoothDevice, i2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00d3, code lost:
        r6 = r6.getService();
     */
    @DexIgnore
    public void onDescriptorReadRequest(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattDescriptor bluetoothGattDescriptor) {
        Object obj;
        BluetoothGattServer bluetoothGattServer;
        T t;
        BluetoothGattService service;
        UUID uuid;
        BluetoothDevice bluetoothDevice2 = bluetoothDevice;
        oa1 oa1 = oa1.a;
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        wg6.a(characteristic, "descriptor.characteristic");
        Object[] objArr = {bluetoothDevice2, Integer.valueOf(i), Integer.valueOf(i2), characteristic.getUuid(), bluetoothGattDescriptor.getUuid()};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) pj1.DESCRIPTOR_READ);
        og0 og0 = og0.GATT_SERVER_EVENT;
        String address = bluetoothDevice.getAddress();
        wg6.a(address, "device.address");
        String uuid2 = UUID.randomUUID().toString();
        wg6.a(uuid2, "UUID.randomUUID().toString()");
        JSONObject a3 = cw0.a(cw0.a(new JSONObject(), bm0.REQUEST_ID, (Object) Integer.valueOf(i)), bm0.OFFSET, (Object) Integer.valueOf(i2));
        bm0 bm0 = bm0.CHARACTERISTIC;
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        if (characteristic2 == null || (uuid = characteristic2.getUuid()) == null || (obj = uuid.toString()) == null) {
            obj = JSONObject.NULL;
        }
        qs0.a(new nn0(a2, og0, address, "", uuid2, true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(a3, bm0, obj), bm0.DESCRIPTOR, (Object) bluetoothGattDescriptor.getUuid().toString()), 448));
        Integer num = null;
        if ((bluetoothGattDescriptor.getPermissions() | 1 | 2 | 4) != 0) {
            Iterator<T> it = this.a.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                h11 h11 = (h11) t;
                wg6.a(h11, "service");
                UUID uuid3 = h11.getUuid();
                BluetoothGattCharacteristic characteristic3 = bluetoothGattDescriptor.getCharacteristic();
                UUID uuid4 = (characteristic3 == null || service == null) ? null : service.getUuid();
                if (wg6.a(uuid3, uuid4)) {
                    break;
                }
            }
            h11 h112 = (h11) t;
            if (h112 == null) {
                num = 257;
                int i3 = i;
            } else if (!h112.a((an1) new mg0(bluetoothDevice2, i, i2, bluetoothGattDescriptor))) {
                num = 6;
            }
        } else {
            int i4 = i;
            num = 2;
        }
        if (num != null && (bluetoothGattServer = this.a.a) != null) {
            bluetoothGattServer.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x010b, code lost:
        r4 = r4.getService();
     */
    @DexIgnore
    public void onDescriptorWriteRequest(BluetoothDevice bluetoothDevice, int i, BluetoothGattDescriptor bluetoothGattDescriptor, boolean z, boolean z2, int i2, byte[] bArr) {
        Object obj;
        BluetoothGattServer bluetoothGattServer;
        T t;
        BluetoothGattService service;
        UUID uuid;
        byte[] bArr2 = bArr;
        oa1 oa1 = oa1.a;
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        wg6.a(characteristic, "descriptor.characteristic");
        Integer num = null;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i), characteristic.getUuid(), Boolean.valueOf(z), Boolean.valueOf(z2), Integer.valueOf(i2), cw0.a(bArr2, (String) null, 1)};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) pj1.DESCRIPTOR_WRITE);
        og0 og0 = og0.GATT_SERVER_EVENT;
        String address = bluetoothDevice.getAddress();
        wg6.a(address, "device.address");
        String uuid2 = UUID.randomUUID().toString();
        wg6.a(uuid2, "UUID.randomUUID().toString()");
        JSONObject a3 = cw0.a(new JSONObject(), bm0.REQUEST_ID, (Object) Integer.valueOf(i));
        bm0 bm0 = bm0.CHARACTERISTIC;
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        if (characteristic2 == null || (uuid = characteristic2.getUuid()) == null || (obj = uuid.toString()) == null) {
            obj = JSONObject.NULL;
        }
        qs0.a(new nn0(a2, og0, address, "", uuid2, true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(a3, bm0, obj), bm0.DESCRIPTOR, (Object) bluetoothGattDescriptor.getUuid().toString()), bm0.PREPARED_WRITE, (Object) Boolean.valueOf(z)), bm0.RESPONSE_NEEDED, (Object) Boolean.valueOf(z2)), bm0.OFFSET, (Object) Integer.valueOf(i2)), bm0.VALUE, (Object) cw0.a(bArr2, (String) null, 1)), 448));
        if ((bluetoothGattDescriptor.getPermissions() | 16 | 32 | 64 | 128 | 256) != 0) {
            Iterator<T> it = this.a.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                h11 h11 = (h11) t;
                wg6.a(h11, "service");
                UUID uuid3 = h11.getUuid();
                BluetoothGattCharacteristic characteristic3 = bluetoothGattDescriptor.getCharacteristic();
                UUID uuid4 = (characteristic3 == null || service == null) ? null : service.getUuid();
                if (wg6.a(uuid3, uuid4)) {
                    break;
                }
            }
            h11 h112 = (h11) t;
            if (h112 == null) {
                num = 257;
            } else if (!h112.a((an1) new xj0(bluetoothDevice, i, bluetoothGattDescriptor, z, z2, i2, bArr))) {
                num = 6;
            }
        } else {
            num = 3;
        }
        if (num != null && z2 && (bluetoothGattServer = this.a.a) != null) {
            bluetoothGattServer.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
        }
    }

    @DexIgnore
    public void onExecuteWrite(BluetoothDevice bluetoothDevice, int i, boolean z) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i), Boolean.valueOf(z)};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) pj1.EXECUTE_WRITE);
        og0 og0 = og0.GATT_SERVER_EVENT;
        String address = bluetoothDevice.getAddress();
        wg6.a(address, "device.address");
        String uuid = UUID.randomUUID().toString();
        wg6.a(uuid, "UUID.randomUUID().toString()");
        qs0.a(new nn0(a2, og0, address, "", uuid, true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.REQUEST_ID, (Object) Integer.valueOf(i)), bm0.EXECUTE, (Object) Boolean.valueOf(z)), 448));
        BluetoothGattServer bluetoothGattServer = this.a.a;
        if (bluetoothGattServer != null) {
            bluetoothGattServer.sendResponse(bluetoothDevice, i, 257, 0, (byte[]) null);
        }
    }

    @DexIgnore
    public void onMtuChanged(BluetoothDevice bluetoothDevice, int i) {
        BluetoothDevice bluetoothDevice2 = bluetoothDevice;
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice2, Integer.valueOf(i)};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) pj1.MTU_CHANGED);
        og0 og0 = og0.GATT_SERVER_EVENT;
        String address = bluetoothDevice.getAddress();
        wg6.a(address, "device.address");
        String uuid = UUID.randomUUID().toString();
        wg6.a(uuid, "UUID.randomUUID().toString()");
        qs0.a(new nn0(a2, og0, address, "", uuid, true, (String) null, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.EXCHANGED_MTU, (Object) Integer.valueOf(i)), 448));
        for (h11 h11 : this.a.b) {
            h11.b.put(bluetoothDevice2, Integer.valueOf(i));
        }
    }

    @DexIgnore
    public void onNotificationSent(BluetoothDevice bluetoothDevice, int i) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i)};
        this.a.f.a.c();
        this.a.f.a.a(new jl1(t31.c.a(i), bluetoothDevice));
    }

    @DexIgnore
    public void onPhyRead(BluetoothDevice bluetoothDevice, int i, int i2, int i3) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)};
    }

    @DexIgnore
    public void onPhyUpdate(BluetoothDevice bluetoothDevice, int i, int i2, int i3) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)};
    }

    @DexIgnore
    public void onServiceAdded(int i, BluetoothGattService bluetoothGattService) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {Integer.valueOf(i), bluetoothGattService.getUuid()};
        qs0.h.a(new nn0(cw0.a((Enum<?>) pj1.SERVICE_ADDED), og0.GATT_SERVER_EVENT, "", "", ze0.a("UUID.randomUUID().toString()"), true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.STATUS, (Object) Integer.valueOf(i)), bm0.SERVICE, (Object) bluetoothGattService.getUuid().toString()), 448));
        for (h11 a2 : this.a.b) {
            int i2 = i;
            a2.a(i, bluetoothGattService);
        }
    }
}
