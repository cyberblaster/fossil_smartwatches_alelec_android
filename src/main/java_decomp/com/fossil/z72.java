package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z72 implements Parcelable.Creator<DataType> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        ArrayList<h72> arrayList = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                str = f22.e(parcel, a);
            } else if (a2 == 2) {
                arrayList = f22.c(parcel, a, h72.CREATOR);
            } else if (a2 == 3) {
                str2 = f22.e(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                str3 = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new DataType(str, (List<h72>) arrayList, str2, str3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataType[i];
    }
}
