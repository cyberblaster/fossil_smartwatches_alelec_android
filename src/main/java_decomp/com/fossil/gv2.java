package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv2 extends sh2 implements ev2 {
    @DexIgnore
    public gv2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    public final void d(Bundle bundle) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) bundle);
        b(1, q);
    }
}
