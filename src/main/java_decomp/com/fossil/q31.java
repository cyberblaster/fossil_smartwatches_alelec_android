package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q31 {
    @DexIgnore
    public /* synthetic */ q31(qg6 qg6) {
    }

    @DexIgnore
    public final m51 a(int i) {
        if (i == 0) {
            return m51.DISCONNECTED;
        }
        if (i == 1) {
            return m51.CONNECTING;
        }
        if (i == 2) {
            return m51.CONNECTED;
        }
        if (i != 3) {
            return m51.DISCONNECTED;
        }
        return m51.DISCONNECTING;
    }
}
