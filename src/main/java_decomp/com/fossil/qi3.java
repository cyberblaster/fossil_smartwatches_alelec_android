package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextPaint;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.d7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qi3 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ ColorStateList f;
    @DexIgnore
    public /* final */ float g;
    @DexIgnore
    public /* final */ float h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public Typeface l;

    @DexIgnore
    public qi3(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, xf3.TextAppearance);
        this.a = obtainStyledAttributes.getDimension(xf3.TextAppearance_android_textSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.b = pi3.a(context, obtainStyledAttributes, xf3.TextAppearance_android_textColor);
        pi3.a(context, obtainStyledAttributes, xf3.TextAppearance_android_textColorHint);
        pi3.a(context, obtainStyledAttributes, xf3.TextAppearance_android_textColorLink);
        this.c = obtainStyledAttributes.getInt(xf3.TextAppearance_android_textStyle, 0);
        this.d = obtainStyledAttributes.getInt(xf3.TextAppearance_android_typeface, 1);
        int a2 = pi3.a(obtainStyledAttributes, xf3.TextAppearance_fontFamily, xf3.TextAppearance_android_fontFamily);
        this.j = obtainStyledAttributes.getResourceId(a2, 0);
        this.e = obtainStyledAttributes.getString(a2);
        obtainStyledAttributes.getBoolean(xf3.TextAppearance_textAllCaps, false);
        this.f = pi3.a(context, obtainStyledAttributes, xf3.TextAppearance_android_shadowColor);
        this.g = obtainStyledAttributes.getFloat(xf3.TextAppearance_android_shadowDx, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.h = obtainStyledAttributes.getFloat(xf3.TextAppearance_android_shadowDy, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.i = obtainStyledAttributes.getFloat(xf3.TextAppearance_android_shadowRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public Typeface b() {
        a();
        return this.l;
    }

    @DexIgnore
    public void c(Context context, TextPaint textPaint, si3 si3) {
        if (ri3.a()) {
            a(textPaint, a(context));
        } else {
            a(context, textPaint, si3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends si3 {
        @DexIgnore
        public /* final */ /* synthetic */ TextPaint a;
        @DexIgnore
        public /* final */ /* synthetic */ si3 b;

        @DexIgnore
        public b(TextPaint textPaint, si3 si3) {
            this.a = textPaint;
            this.b = si3;
        }

        @DexIgnore
        public void a(Typeface typeface, boolean z) {
            qi3.this.a(this.a, typeface);
            this.b.a(typeface, z);
        }

        @DexIgnore
        public void a(int i) {
            this.b.a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends d7.a {
        @DexIgnore
        public /* final */ /* synthetic */ si3 a;

        @DexIgnore
        public a(si3 si3) {
            this.a = si3;
        }

        @DexIgnore
        public void a(Typeface typeface) {
            qi3 qi3 = qi3.this;
            Typeface unused = qi3.l = Typeface.create(typeface, qi3.c);
            boolean unused2 = qi3.this.k = true;
            this.a.a(qi3.this.l, false);
        }

        @DexIgnore
        public void a(int i) {
            boolean unused = qi3.this.k = true;
            this.a.a(i);
        }
    }

    @DexIgnore
    public void b(Context context, TextPaint textPaint, si3 si3) {
        c(context, textPaint, si3);
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.i;
        float f3 = this.g;
        float f4 = this.h;
        ColorStateList colorStateList2 = this.f;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    @DexIgnore
    public Typeface a(Context context) {
        if (this.k) {
            return this.l;
        }
        if (!context.isRestricted()) {
            try {
                this.l = d7.a(context, this.j);
                if (this.l != null) {
                    this.l = Typeface.create(this.l, this.c);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
            } catch (Exception e2) {
                Log.d("TextAppearance", "Error loading font " + this.e, e2);
            }
        }
        a();
        this.k = true;
        return this.l;
    }

    @DexIgnore
    public void a(Context context, si3 si3) {
        if (ri3.a()) {
            a(context);
        } else {
            a();
        }
        if (this.j == 0) {
            this.k = true;
        }
        if (this.k) {
            si3.a(this.l, true);
            return;
        }
        try {
            d7.a(context, this.j, new a(si3), (Handler) null);
        } catch (Resources.NotFoundException unused) {
            this.k = true;
            si3.a(1);
        } catch (Exception e2) {
            Log.d("TextAppearance", "Error loading font " + this.e, e2);
            this.k = true;
            si3.a(-3);
        }
    }

    @DexIgnore
    public void a(Context context, TextPaint textPaint, si3 si3) {
        a(textPaint, b());
        a(context, (si3) new b(textPaint, si3));
    }

    @DexIgnore
    public final void a() {
        String str;
        if (this.l == null && (str = this.e) != null) {
            this.l = Typeface.create(str, this.c);
        }
        if (this.l == null) {
            int i2 = this.d;
            if (i2 == 1) {
                this.l = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.l = Typeface.SERIF;
            } else if (i2 != 3) {
                this.l = Typeface.DEFAULT;
            } else {
                this.l = Typeface.MONOSPACE;
            }
            this.l = Typeface.create(this.l, this.c);
        }
    }

    @DexIgnore
    public void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int i2 = (~typeface.getStyle()) & this.c;
        textPaint.setFakeBoldText((i2 & 1) != 0);
        textPaint.setTextSkewX((i2 & 2) != 0 ? -0.25f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        textPaint.setTextSize(this.a);
    }
}
