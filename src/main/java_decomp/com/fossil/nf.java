package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class nf {
    @DexIgnore
    public /* final */ ViewGroup a;

    @DexIgnore
    @Deprecated
    public interface b {
        @DexIgnore
        a a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ViewGroup.MarginLayoutParams {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }
    }

    @DexIgnore
    public nf(ViewGroup viewGroup) {
        if (viewGroup != null) {
            this.a = viewGroup;
            return;
        }
        throw new IllegalArgumentException("host must be non-null");
    }

    @DexIgnore
    public static void a(ViewGroup.LayoutParams layoutParams, TypedArray typedArray, int i, int i2) {
        layoutParams.width = typedArray.getLayoutDimension(i, 0);
        layoutParams.height = typedArray.getLayoutDimension(i2, 0);
    }

    @DexIgnore
    public void b() {
        a a2;
        int childCount = this.a.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewGroup.LayoutParams layoutParams = this.a.getChildAt(i).getLayoutParams();
            if ((layoutParams instanceof b) && (a2 = ((b) layoutParams).a()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    a2.a((ViewGroup.MarginLayoutParams) layoutParams);
                } else {
                    a2.a(layoutParams);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, int i2) {
        a a2;
        int size = (View.MeasureSpec.getSize(i) - this.a.getPaddingLeft()) - this.a.getPaddingRight();
        int size2 = (View.MeasureSpec.getSize(i2) - this.a.getPaddingTop()) - this.a.getPaddingBottom();
        int childCount = this.a.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.a.getChildAt(i3);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof b) && (a2 = ((b) layoutParams).a()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    a2.a(childAt, (ViewGroup.MarginLayoutParams) layoutParams, size, size2);
                } else {
                    a2.a(layoutParams, size, size2);
                }
            }
        }
    }

    @DexIgnore
    public static boolean b(View view, a aVar) {
        return (view.getMeasuredWidthAndState() & -16777216) == 16777216 && aVar.a >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && aVar.j.width == -2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a {
        @DexIgnore
        public float a; // = -1.0f;
        @DexIgnore
        public float b; // = -1.0f;
        @DexIgnore
        public float c; // = -1.0f;
        @DexIgnore
        public float d; // = -1.0f;
        @DexIgnore
        public float e; // = -1.0f;
        @DexIgnore
        public float f; // = -1.0f;
        @DexIgnore
        public float g; // = -1.0f;
        @DexIgnore
        public float h; // = -1.0f;
        @DexIgnore
        public float i;
        @DexIgnore
        public /* final */ c j; // = new c(0, 0);

        @DexIgnore
        public void a(ViewGroup.LayoutParams layoutParams, int i2, int i3) {
            c cVar = this.j;
            cVar.width = layoutParams.width;
            cVar.height = layoutParams.height;
            boolean z = false;
            boolean z2 = (cVar.b || cVar.width == 0) && this.a < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            c cVar2 = this.j;
            if ((cVar2.a || cVar2.height == 0) && this.b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
            }
            float f2 = this.a;
            if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.width = Math.round(((float) i2) * f2);
            }
            float f3 = this.b;
            if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.height = Math.round(((float) i3) * f3);
            }
            float f4 = this.i;
            if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (z2) {
                    layoutParams.width = Math.round(((float) layoutParams.height) * f4);
                    this.j.b = true;
                }
                if (z) {
                    layoutParams.height = Math.round(((float) layoutParams.width) / this.i);
                    this.j.a = true;
                }
            }
        }

        @DexIgnore
        public String toString() {
            return String.format("PercentLayoutInformation width: %f height %f, margins (%f, %f,  %f, %f, %f, %f)", new Object[]{Float.valueOf(this.a), Float.valueOf(this.b), Float.valueOf(this.c), Float.valueOf(this.d), Float.valueOf(this.e), Float.valueOf(this.f), Float.valueOf(this.g), Float.valueOf(this.h)});
        }

        @DexIgnore
        public void a(View view, ViewGroup.MarginLayoutParams marginLayoutParams, int i2, int i3) {
            a(marginLayoutParams, i2, i3);
            c cVar = this.j;
            cVar.leftMargin = marginLayoutParams.leftMargin;
            cVar.topMargin = marginLayoutParams.topMargin;
            cVar.rightMargin = marginLayoutParams.rightMargin;
            cVar.bottomMargin = marginLayoutParams.bottomMargin;
            h9.c(cVar, h9.b(marginLayoutParams));
            h9.b(this.j, h9.a(marginLayoutParams));
            float f2 = this.c;
            if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.leftMargin = Math.round(((float) i2) * f2);
            }
            float f3 = this.d;
            if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.topMargin = Math.round(((float) i3) * f3);
            }
            float f4 = this.e;
            if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.rightMargin = Math.round(((float) i2) * f4);
            }
            float f5 = this.f;
            if (f5 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.bottomMargin = Math.round(((float) i3) * f5);
            }
            boolean z = false;
            float f6 = this.g;
            if (f6 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                h9.c(marginLayoutParams, Math.round(((float) i2) * f6));
                z = true;
            }
            float f7 = this.h;
            if (f7 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                h9.b(marginLayoutParams, Math.round(((float) i2) * f7));
                z = true;
            }
            if (z && view != null) {
                h9.a(marginLayoutParams, x9.o(view));
            }
        }

        @DexIgnore
        public void a(ViewGroup.MarginLayoutParams marginLayoutParams) {
            a((ViewGroup.LayoutParams) marginLayoutParams);
            c cVar = this.j;
            marginLayoutParams.leftMargin = cVar.leftMargin;
            marginLayoutParams.topMargin = cVar.topMargin;
            marginLayoutParams.rightMargin = cVar.rightMargin;
            marginLayoutParams.bottomMargin = cVar.bottomMargin;
            h9.c(marginLayoutParams, h9.b(cVar));
            h9.b(marginLayoutParams, h9.a(this.j));
        }

        @DexIgnore
        public void a(ViewGroup.LayoutParams layoutParams) {
            c cVar = this.j;
            if (!cVar.b) {
                layoutParams.width = cVar.width;
            }
            c cVar2 = this.j;
            if (!cVar2.a) {
                layoutParams.height = cVar2.height;
            }
            c cVar3 = this.j;
            cVar3.b = false;
            cVar3.a = false;
        }
    }

    @DexIgnore
    public static a a(Context context, AttributeSet attributeSet) {
        a aVar;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, mf.PercentLayout_Layout);
        float fraction = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_widthPercent, 1, 1, -1.0f);
        if (fraction != -1.0f) {
            aVar = new a();
            aVar.a = fraction;
        } else {
            aVar = null;
        }
        float fraction2 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_heightPercent, 1, 1, -1.0f);
        if (fraction2 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.b = fraction2;
        }
        float fraction3 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginPercent, 1, 1, -1.0f);
        if (fraction3 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.c = fraction3;
            aVar.d = fraction3;
            aVar.e = fraction3;
            aVar.f = fraction3;
        }
        float fraction4 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginLeftPercent, 1, 1, -1.0f);
        if (fraction4 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.c = fraction4;
        }
        float fraction5 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginTopPercent, 1, 1, -1.0f);
        if (fraction5 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.d = fraction5;
        }
        float fraction6 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginRightPercent, 1, 1, -1.0f);
        if (fraction6 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.e = fraction6;
        }
        float fraction7 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginBottomPercent, 1, 1, -1.0f);
        if (fraction7 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.f = fraction7;
        }
        float fraction8 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginStartPercent, 1, 1, -1.0f);
        if (fraction8 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.g = fraction8;
        }
        float fraction9 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_marginEndPercent, 1, 1, -1.0f);
        if (fraction9 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.h = fraction9;
        }
        float fraction10 = obtainStyledAttributes.getFraction(mf.PercentLayout_Layout_layout_aspectRatio, 1, 1, -1.0f);
        if (fraction10 != -1.0f) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.i = fraction10;
        }
        obtainStyledAttributes.recycle();
        return aVar;
    }

    @DexIgnore
    public boolean a() {
        a a2;
        int childCount = this.a.getChildCount();
        boolean z = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = this.a.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof b) && (a2 = ((b) layoutParams).a()) != null) {
                if (b(childAt, a2)) {
                    layoutParams.width = -2;
                    z = true;
                }
                if (a(childAt, a2)) {
                    layoutParams.height = -2;
                    z = true;
                }
            }
        }
        return z;
    }

    @DexIgnore
    public static boolean a(View view, a aVar) {
        return (view.getMeasuredHeightAndState() & -16777216) == 16777216 && aVar.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && aVar.j.height == -2;
    }
}
