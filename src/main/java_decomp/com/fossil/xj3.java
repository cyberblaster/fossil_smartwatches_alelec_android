package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj3<T> extends hk3<T> {
    @DexIgnore
    public static /* final */ xj3<Object> INSTANCE; // = new xj3<>();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public static <T> hk3<T> withType() {
        return INSTANCE;
    }

    @DexIgnore
    public Set<T> asSet() {
        return Collections.emptySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this;
    }

    @DexIgnore
    public T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    @DexIgnore
    public int hashCode() {
        return 2040732332;
    }

    @DexIgnore
    public boolean isPresent() {
        return false;
    }

    @DexIgnore
    public T or(T t) {
        jk3.a(t, (Object) "use Optional.orNull() instead of Optional.or(null)");
        return t;
    }

    @DexIgnore
    public T orNull() {
        return null;
    }

    @DexIgnore
    public String toString() {
        return "Optional.absent()";
    }

    @DexIgnore
    public <V> hk3<V> transform(ck3<? super T, V> ck3) {
        jk3.a(ck3);
        return hk3.absent();
    }

    @DexIgnore
    public hk3<T> or(hk3<? extends T> hk3) {
        jk3.a(hk3);
        return hk3;
    }

    @DexIgnore
    public T or(nk3<? extends T> nk3) {
        T t = nk3.get();
        jk3.a(t, (Object) "use Optional.orNull() instead of a Supplier that returns null");
        return t;
    }
}
