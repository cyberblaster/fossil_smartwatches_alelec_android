package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class di0 extends an1 {
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ byte[] g;

    @DexIgnore
    public di0(BluetoothDevice bluetoothDevice, int i, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z, boolean z2, int i2, byte[] bArr) {
        super(bluetoothDevice, i);
        this.c = bluetoothGattCharacteristic;
        this.d = z;
        this.e = z2;
        this.f = i2;
        this.g = bArr;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(super.a(), bm0.CHARACTERISTIC, (Object) this.c.getUuid().toString()), bm0.PREPARED_WRITE, (Object) Boolean.valueOf(this.d)), bm0.RESPONSE_NEEDED, (Object) Boolean.valueOf(this.e)), bm0.OFFSET, (Object) Integer.valueOf(this.f)), bm0.VALUE, (Object) cw0.a(this.g, (String) null, 1));
    }
}
