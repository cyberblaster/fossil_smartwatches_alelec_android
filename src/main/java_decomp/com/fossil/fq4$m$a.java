package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MediaAppDetails;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$m$a extends MusicControlComponent.f {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.m j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$m$a(MediaAppDetails mediaAppDetails, Context context, MediaAppDetails mediaAppDetails2, String str, MusicControlComponent.m mVar) {
        super(context, mediaAppDetails2, str);
        this.j = mVar;
    }

    @DexIgnore
    public void a(MusicControlComponent.f fVar) {
        if (fVar != null && this.j.this$0.f().a(fVar)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, ".OldVersionMusicController is added to list controller, packageName=" + fVar.c());
            if (fVar.d() == 3) {
                this.j.this$0.a(fVar.d());
                this.j.this$0.a(fVar.b());
            }
        }
    }

    @DexIgnore
    public void a(int i, int i2, MusicControlComponent.b bVar) {
        wg6.b(bVar, "controller");
        super.a(i, i2, bVar);
        if (i2 == 3) {
            this.j.this$0.a(bVar);
        } else if (this.j.this$0.e() == null) {
            this.j.this$0.a(bVar);
        }
        this.j.this$0.a(i2);
    }

    @DexIgnore
    public void a(MusicControlComponent.c cVar, MusicControlComponent.c cVar2) {
        wg6.b(cVar, "oldMetadata");
        wg6.b(cVar2, "newMetadata");
        super.a(cVar, cVar2);
        this.j.this$0.a(b());
    }
}
