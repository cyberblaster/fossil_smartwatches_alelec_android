package com.fossil;

import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr5 implements Factory<lr5> {
    @DexIgnore
    public static ProfileSetupPresenter a(ir5 ir5, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository, ServerSettingRepository serverSettingRepository) {
        return new ProfileSetupPresenter(ir5, getRecommendedGoalUseCase, userRepository, serverSettingRepository);
    }
}
