package com.fossil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okhttp3.RequestBody;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq6 extends RequestBody {
    @DexIgnore
    public static /* final */ uq6 e; // = uq6.a("multipart/mixed");
    @DexIgnore
    public static /* final */ uq6 f; // = uq6.a("multipart/form-data");
    @DexIgnore
    public static /* final */ byte[] g; // = {58, 32};
    @DexIgnore
    public static /* final */ byte[] h; // = {DateTimeFieldType.HALFDAY_OF_DAY, 10};
    @DexIgnore
    public static /* final */ byte[] i; // = {45, 45};
    @DexIgnore
    public /* final */ mt6 a;
    @DexIgnore
    public /* final */ uq6 b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public long d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ mt6 a;
        @DexIgnore
        public uq6 b;
        @DexIgnore
        public /* final */ List<b> c;

        @DexIgnore
        public a() {
            this(UUID.randomUUID().toString());
        }

        @DexIgnore
        public a a(uq6 uq6) {
            if (uq6 == null) {
                throw new NullPointerException("type == null");
            } else if (uq6.c().equals("multipart")) {
                this.b = uq6;
                return this;
            } else {
                throw new IllegalArgumentException("multipart != " + uq6);
            }
        }

        @DexIgnore
        public a(String str) {
            this.b = vq6.e;
            this.c = new ArrayList();
            this.a = mt6.encodeUtf8(str);
        }

        @DexIgnore
        public a a(sq6 sq6, RequestBody requestBody) {
            a(b.a(sq6, requestBody));
            return this;
        }

        @DexIgnore
        public a a(b bVar) {
            if (bVar != null) {
                this.c.add(bVar);
                return this;
            }
            throw new NullPointerException("part == null");
        }

        @DexIgnore
        public vq6 a() {
            if (!this.c.isEmpty()) {
                return new vq6(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ sq6 a;
        @DexIgnore
        public /* final */ RequestBody b;

        @DexIgnore
        public b(sq6 sq6, RequestBody requestBody) {
            this.a = sq6;
            this.b = requestBody;
        }

        @DexIgnore
        public static b a(sq6 sq6, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (sq6 != null && sq6.a("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (sq6 == null || sq6.a("Content-Length") == null) {
                return new b(sq6, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }
    }

    /*
    static {
        uq6.a("multipart/alternative");
        uq6.a("multipart/digest");
        uq6.a("multipart/parallel");
    }
    */

    @DexIgnore
    public vq6(mt6 mt6, uq6 uq6, List<b> list) {
        this.a = mt6;
        this.b = uq6.a(uq6 + "; boundary=" + mt6.utf8());
        this.c = fr6.a(list);
    }

    @DexIgnore
    public long a() throws IOException {
        long j = this.d;
        if (j != -1) {
            return j;
        }
        long a2 = a((kt6) null, true);
        this.d = a2;
        return a2;
    }

    @DexIgnore
    public uq6 b() {
        return this.b;
    }

    @DexIgnore
    public void a(kt6 kt6) throws IOException {
        a(kt6, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: com.fossil.kt6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.fossil.jt6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.fossil.jt6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: com.fossil.kt6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.fossil.jt6} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final long a(kt6 kt6, boolean z) throws IOException {
        jt6 jt6;
        if (z) {
            kt6 = new jt6();
            jt6 = kt6;
        } else {
            jt6 = 0;
        }
        int size = this.c.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            sq6 sq6 = bVar.a;
            RequestBody requestBody = bVar.b;
            kt6.write(i);
            kt6.a(this.a);
            kt6.write(h);
            if (sq6 != null) {
                int b2 = sq6.b();
                for (int i3 = 0; i3 < b2; i3++) {
                    kt6.a(sq6.a(i3)).write(g).a(sq6.b(i3)).write(h);
                }
            }
            uq6 b3 = requestBody.b();
            if (b3 != null) {
                kt6.a("Content-Type: ").a(b3.toString()).write(h);
            }
            long a2 = requestBody.a();
            if (a2 != -1) {
                kt6.a("Content-Length: ").d(a2).write(h);
            } else if (z) {
                jt6.k();
                return -1;
            }
            kt6.write(h);
            if (z) {
                j += a2;
            } else {
                requestBody.a(kt6);
            }
            kt6.write(h);
        }
        kt6.write(i);
        kt6.a(this.a);
        kt6.write(i);
        kt6.write(h);
        if (!z) {
            return j;
        }
        long p = j + jt6.p();
        jt6.k();
        return p;
    }
}
