package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr0 extends xg6 implements hg6<if1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ qb1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wr0(qb1 qb1) {
        super(1);
        this.a = qb1;
    }

    @DexIgnore
    public final void a(if1 if1) {
        this.a.T = System.currentTimeMillis();
        qb1 qb1 = this.a;
        qb1.W = ((al0) if1).B;
        qb1.V = qb1.W.getFirmwareVersion();
        qb1 qb12 = this.a;
        String str = qb12.Z;
        if (str == null || xj6.b(qb12.V, str, true)) {
            qb1 qb13 = this.a;
            qb13.Y = false;
            qb13.a(km1.a(qb13.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
            return;
        }
        qb1 qb14 = this.a;
        qb14.Y = false;
        qb14.a(km1.a(qb14.v, (eh1) null, sk1.TARGET_FIRMWARE_NOT_MATCHED, (bn0) null, 5));
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((if1) obj);
        return cd6.a;
    }
}
