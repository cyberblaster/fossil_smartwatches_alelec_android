package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ v36 a;
    @DexIgnore
    public /* final */ /* synthetic */ x56 b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ o46 e;

    @DexIgnore
    public s46(o46 o46, v36 v36, x56 x56, boolean z, boolean z2) {
        this.e = o46;
        this.a = v36;
        this.b = x56;
        this.c = z;
        this.d = z2;
    }

    @DexIgnore
    public void run() {
        this.e.b(this.a, this.b, this.c, this.d);
    }
}
