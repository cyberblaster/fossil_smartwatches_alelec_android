package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj2 extends fn2<uj2, a> implements to2 {
    @DexIgnore
    public static /* final */ uj2 zzj;
    @DexIgnore
    public static volatile yo2<uj2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public long zzg;
    @DexIgnore
    public float zzh;
    @DexIgnore
    public double zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<uj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(uj2.zzj);
        }

        @DexIgnore
        public final a a(long j) {
            f();
            ((uj2) this.b).a(j);
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            f();
            ((uj2) this.b).b(str);
            return this;
        }

        @DexIgnore
        public final a j() {
            f();
            ((uj2) this.b).x();
            return this;
        }

        @DexIgnore
        public final a k() {
            f();
            ((uj2) this.b).y();
            return this;
        }

        @DexIgnore
        public final a l() {
            f();
            ((uj2) this.b).z();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((uj2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public final a b(long j) {
            f();
            ((uj2) this.b).b(j);
            return this;
        }

        @DexIgnore
        public final a a(double d) {
            f();
            ((uj2) this.b).a(d);
            return this;
        }
    }

    /*
    static {
        uj2 uj2 = new uj2();
        zzj = uj2;
        fn2.a(uj2.class, uj2);
    }
    */

    @DexIgnore
    public static a A() {
        return (a) zzj.h();
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 1;
        this.zzd = j;
    }

    @DexIgnore
    public final void b(String str) {
        if (str != null) {
            this.zzc |= 4;
            this.zzf = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final long o() {
        return this.zzd;
    }

    @DexIgnore
    public final String p() {
        return this.zze;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String r() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final long t() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean v() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final double w() {
        return this.zzi;
    }

    @DexIgnore
    public final void x() {
        this.zzc &= -5;
        this.zzf = zzj.zzf;
    }

    @DexIgnore
    public final void y() {
        this.zzc &= -9;
        this.zzg = 0;
    }

    @DexIgnore
    public final void z() {
        this.zzc &= -33;
        this.zzi = 0.0d;
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 2;
            this.zze = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(long j) {
        this.zzc |= 8;
        this.zzg = j;
    }

    @DexIgnore
    public final void a(double d) {
        this.zzc |= 32;
        this.zzi = d;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new uj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0002\u0000\u0002\b\u0001\u0003\b\u0002\u0004\u0002\u0003\u0005\u0001\u0004\u0006\u0000\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                yo2<uj2> yo2 = zzk;
                if (yo2 == null) {
                    synchronized (uj2.class) {
                        yo2 = zzk;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzj);
                            zzk = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
