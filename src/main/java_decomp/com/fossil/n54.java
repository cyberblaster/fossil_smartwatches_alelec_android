package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n54 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RelativeLayout r;
    @DexIgnore
    public /* final */ RelativeLayout s;
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public /* final */ RelativeLayout u;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n54(Object obj, View view, int i, RTLImageView rTLImageView, RelativeLayout relativeLayout, RelativeLayout relativeLayout2, RelativeLayout relativeLayout3, ConstraintLayout constraintLayout, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, LinearLayout linearLayout, RelativeLayout relativeLayout4, FlexibleTextView flexibleTextView, View view2, View view3) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = relativeLayout;
        this.s = relativeLayout2;
        this.t = relativeLayout3;
        this.u = relativeLayout4;
    }
}
