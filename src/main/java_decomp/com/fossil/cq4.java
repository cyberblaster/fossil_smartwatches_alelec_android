package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cq4 extends Service {
    @DexIgnore
    public static eq4 c;
    @DexIgnore
    public int a;
    @DexIgnore
    public vh4 b; // = vh4.NOT_START;

    @DexIgnore
    public static void a(eq4 eq4) {
        c = eq4;
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public void d() {
        vh4 vh4;
        if (c != null && this.b != (vh4 = vh4.RUNNING)) {
            this.b = vh4;
            c.a(this.a, vh4);
            c.a(this);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return null;
    }

    @DexIgnore
    public void a() {
        vh4 vh4;
        if (c != null && this.b != (vh4 = vh4.FINISHED)) {
            this.b = vh4;
            c.a(this.a, vh4);
            c.b(this);
        }
    }
}
