package com.fossil;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o53 {
    @DexIgnore
    public /* final */ t53 a;

    @DexIgnore
    public o53(t53 t53) {
        w12.a(t53);
        this.a = t53;
    }

    @DexIgnore
    public static boolean a(Context context) {
        ActivityInfo receiverInfo;
        w12.a(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) == null || !receiverInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @DexIgnore
    public final void a(Context context, Intent intent) {
        x53 a2 = x53.a(context, (mv2) null);
        t43 b = a2.b();
        if (intent == null) {
            b.w().a("Receiver called with null intent");
            return;
        }
        a2.d();
        String action = intent.getAction();
        b.B().a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            b.B().a("Starting wakeful intent.");
            this.a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            try {
                a2.a().a((Runnable) new r53(this, a2, b));
            } catch (Exception e) {
                b.w().a("Install Referrer Reporter encountered a problem", e);
            }
            BroadcastReceiver.PendingResult a3 = this.a.a();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                b.B().a("Install referrer extras are null");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            b.z().a("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            Bundle a4 = a2.w().a(Uri.parse(stringExtra));
            if (a4 == null) {
                b.B().a("No campaign defined in install referrer broadcast");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            long longExtra = intent.getLongExtra("referrer_timestamp_seconds", 0) * 1000;
            if (longExtra == 0) {
                b.w().a("Install referrer is missing timestamp");
            }
            a2.a().a((Runnable) new q53(this, a2, longExtra, a4, context, b, a3));
        }
    }
}
