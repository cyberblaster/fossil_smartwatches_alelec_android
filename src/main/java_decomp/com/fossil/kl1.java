package com.fossil;

import java.io.File;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl1 implements dx6<String> {
    @DexIgnore
    public /* final */ /* synthetic */ File a;
    @DexIgnore
    public /* final */ /* synthetic */ xe0 b;

    @DexIgnore
    public kl1(File file, xe0 xe0, wj1 wj1) {
        this.a = file;
        this.b = xe0;
    }

    @DexIgnore
    public void onFailure(Call<String> call, Throwable th) {
        oa1 oa1 = oa1.a;
        new Object[1][0] = th.getMessage();
        this.b.b();
    }

    @DexIgnore
    public void onResponse(Call<String> call, rx6<String> rx6) {
        oa1 oa1 = oa1.a;
        Object[] objArr = new Object[4];
        objArr[0] = rx6.e();
        objArr[1] = Integer.valueOf(rx6.b());
        objArr[2] = rx6.a();
        zq6 c = rx6.c();
        objArr[3] = c != null ? c.string() : null;
        int b2 = rx6.b();
        if ((200 <= b2 && 299 >= b2) || b2 == 400 || b2 == 413) {
            this.b.c.remove(this.a);
            this.a.delete();
            this.b.c();
            return;
        }
        this.b.b();
    }
}
