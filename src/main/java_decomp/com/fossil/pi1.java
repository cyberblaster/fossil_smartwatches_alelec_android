package com.fossil;

import com.fossil.m60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class pi1 extends tg6 implements hg6<byte[], m60> {
    @DexIgnore
    public pi1(m60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(m60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyDistanceConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((m60.a) this.receiver).a((byte[]) obj);
    }
}
