package com.fossil;

import android.annotation.TargetApi;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pp extends Thread {
    @DexIgnore
    public /* final */ BlockingQueue<up<?>> a;
    @DexIgnore
    public /* final */ op b;
    @DexIgnore
    public /* final */ ip c;
    @DexIgnore
    public /* final */ xp d;
    @DexIgnore
    public volatile boolean e; // = false;

    @DexIgnore
    public pp(BlockingQueue<up<?>> blockingQueue, op opVar, ip ipVar, xp xpVar) {
        this.a = blockingQueue;
        this.b = opVar;
        this.c = ipVar;
        this.d = xpVar;
    }

    @DexIgnore
    @TargetApi(14)
    public final void a(up<?> upVar) {
        if (Build.VERSION.SDK_INT >= 14) {
            TrafficStats.setThreadStatsTag(upVar.getTrafficStatsTag());
        }
    }

    @DexIgnore
    public void b() {
        this.e = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                a();
            } catch (InterruptedException unused) {
                if (this.e) {
                    Thread.currentThread().interrupt();
                    return;
                }
                cq.c("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }

    @DexIgnore
    public final void a() throws InterruptedException {
        b(this.a.take());
    }

    @DexIgnore
    public void b(up<?> upVar) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            upVar.addMarker("network-queue-take");
            if (upVar.isCanceled()) {
                upVar.finish("network-discard-cancelled");
                upVar.notifyListenerResponseNotUsable();
                return;
            }
            a(upVar);
            rp a2 = this.b.a(upVar);
            upVar.addMarker("network-http-complete");
            if (!a2.e || !upVar.hasHadResponseDelivered()) {
                wp<?> parseNetworkResponse = upVar.parseNetworkResponse(a2);
                upVar.addMarker("network-parse-complete");
                if (upVar.shouldCache() && parseNetworkResponse.b != null) {
                    this.c.a(upVar.getCacheKey(), parseNetworkResponse.b);
                    upVar.addMarker("network-cache-written");
                }
                upVar.markDelivered();
                this.d.a(upVar, parseNetworkResponse);
                upVar.notifyListenerResponseReceived(parseNetworkResponse);
                return;
            }
            upVar.finish("not-modified");
            upVar.notifyListenerResponseNotUsable();
        } catch (bq e2) {
            e2.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            a(upVar, e2);
            upVar.notifyListenerResponseNotUsable();
        } catch (Exception e3) {
            cq.a(e3, "Unhandled exception %s", e3.toString());
            bq bqVar = new bq((Throwable) e3);
            bqVar.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.d.a(upVar, bqVar);
            upVar.notifyListenerResponseNotUsable();
        }
    }

    @DexIgnore
    public final void a(up<?> upVar, bq bqVar) {
        this.d.a(upVar, upVar.parseNetworkError(bqVar));
    }
}
