package com.fossil;

import com.facebook.internal.FileLruCache;
import com.fossil.cm3;
import com.fossil.dm3;
import com.fossil.im3;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn3<K, V> extends bm3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] e;
    @DexIgnore
    public /* final */ transient cm3<K, V>[] f;
    @DexIgnore
    public /* final */ transient int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends im3.b<K> {
        @DexIgnore
        public /* final */ qn3<K, V> map;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qn3$a$a")
        /* renamed from: com.fossil.qn3$a$a  reason: collision with other inner class name */
        public static class C0042a<K> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ bm3<K, ?> map;

            @DexIgnore
            public C0042a(bm3<K, ?> bm3) {
                this.map = bm3;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.keySet();
            }
        }

        @DexIgnore
        public a(qn3<K, V> qn3) {
            this.map = qn3;
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return this.map.containsKey(obj);
        }

        @DexIgnore
        public K get(int i) {
            return this.map.e[i].getKey();
        }

        @DexIgnore
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        public Object writeReplace() {
            return new C0042a(this.map);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends zl3<V> {
        @DexIgnore
        public /* final */ qn3<K, V> map;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a<V> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ bm3<?, V> map;

            @DexIgnore
            public a(bm3<?, V> bm3) {
                this.map = bm3;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.values();
            }
        }

        @DexIgnore
        public b(qn3<K, V> qn3) {
            this.map = qn3;
        }

        @DexIgnore
        public V get(int i) {
            return this.map.e[i].getValue();
        }

        @DexIgnore
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        public Object writeReplace() {
            return new a(this.map);
        }
    }

    @DexIgnore
    public qn3(Map.Entry<K, V>[] entryArr, cm3<K, V>[] cm3Arr, int i) {
        this.e = entryArr;
        this.f = cm3Arr;
        this.g = i;
    }

    @DexIgnore
    public static void checkNoConflictInKeyBucket(Object obj, Map.Entry<?, ?> entry, cm3<?, ?> cm3) {
        while (cm3 != null) {
            bm3.checkNoConflict(!obj.equals(cm3.getKey()), FileLruCache.HEADER_CACHEKEY_KEY, entry, cm3);
            cm3 = cm3.getNextInKeyBucket();
        }
    }

    @DexIgnore
    public static <K, V> qn3<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> qn3<K, V> fromEntryArray(int i, Map.Entry<K, V>[] entryArr) {
        Map.Entry<K, V>[] entryArr2;
        cm3 cm3;
        jk3.b(i, entryArr.length);
        if (i == entryArr.length) {
            entryArr2 = entryArr;
        } else {
            entryArr2 = cm3.createEntryArray(i);
        }
        int a2 = sl3.a(i, 1.2d);
        cm3[] createEntryArray = cm3.createEntryArray(a2);
        int i2 = a2 - 1;
        for (int i3 = 0; i3 < i; i3++) {
            cm3 cm32 = entryArr[i3];
            K key = cm32.getKey();
            V value = cm32.getValue();
            bl3.a((Object) key, (Object) value);
            int a3 = sl3.a(key.hashCode()) & i2;
            cm3 cm33 = createEntryArray[a3];
            if (cm33 == null) {
                cm3 = (cm32 instanceof cm3) && cm32.isReusable() ? cm32 : new cm3(key, value);
            } else {
                cm3 = new cm3.b(key, value, cm33);
            }
            createEntryArray[a3] = cm3;
            entryArr2[i3] = cm3;
            checkNoConflictInKeyBucket(key, cm3, cm33);
        }
        return new qn3<>(entryArr2, createEntryArray, i2);
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> createEntrySet() {
        return new dm3.b(this, this.e);
    }

    @DexIgnore
    public im3<K> createKeySet() {
        return new a(this);
    }

    @DexIgnore
    public vl3<V> createValues() {
        return new b(this);
    }

    @DexIgnore
    public V get(Object obj) {
        return get(obj, this.f, this.g);
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.e.length;
    }

    @DexIgnore
    public static <V> V get(Object obj, cm3<?, V>[] cm3Arr, int i) {
        if (obj == null) {
            return null;
        }
        for (cm3<?, V> cm3 = cm3Arr[i & sl3.a(obj.hashCode())]; cm3 != null; cm3 = cm3.getNextInKeyBucket()) {
            if (obj.equals(cm3.getKey())) {
                return cm3.getValue();
            }
        }
        return null;
    }
}
