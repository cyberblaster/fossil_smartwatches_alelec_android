package com.fossil;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g14 implements Factory<ApplicationEventListener> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;
    @DexIgnore
    public /* final */ Provider<an4> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> f;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> g;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> h;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> i;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> j;
    @DexIgnore
    public /* final */ Provider<UserRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<cj4> m;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> o;
    @DexIgnore
    public /* final */ Provider<FileRepository> p;
    @DexIgnore
    public /* final */ Provider<ThemeRepository> q;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> r;

    @DexIgnore
    public g14(b14 b14, Provider<PortfolioApp> provider, Provider<an4> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<cj4> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<FileRepository> provider15, Provider<ThemeRepository> provider16, Provider<RingStyleRepository> provider17) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
        this.p = provider15;
        this.q = provider16;
        this.r = provider17;
    }

    @DexIgnore
    public static g14 a(b14 b14, Provider<PortfolioApp> provider, Provider<an4> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<cj4> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<FileRepository> provider15, Provider<ThemeRepository> provider16, Provider<RingStyleRepository> provider17) {
        return new g14(b14, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17);
    }

    @DexIgnore
    public static ApplicationEventListener b(b14 b14, Provider<PortfolioApp> provider, Provider<an4> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<cj4> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<FileRepository> provider15, Provider<ThemeRepository> provider16, Provider<RingStyleRepository> provider17) {
        return a(b14, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get(), provider10.get(), provider11.get(), provider12.get(), provider13.get(), provider14.get(), provider15.get(), provider16.get(), provider17.get());
    }

    @DexIgnore
    public static ApplicationEventListener a(b14 b14, PortfolioApp portfolioApp, an4 an4, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, cj4 cj4, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, FileRepository fileRepository, ThemeRepository themeRepository, RingStyleRepository ringStyleRepository) {
        ApplicationEventListener a2 = b14.a(portfolioApp, an4, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, deviceRepository, userRepository, alarmsRepository, cj4, watchFaceRepository, watchLocalizationRepository, fileRepository, themeRepository, ringStyleRepository);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public ApplicationEventListener get() {
        b14 b14 = this.a;
        return b(b14, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r);
    }
}
