package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta3 implements Parcelable.Creator<ra3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        Boolean bool = null;
        ArrayList<String> arrayList = null;
        String str8 = null;
        long j6 = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 2:
                    str = f22.e(parcel2, a);
                    break;
                case 3:
                    str2 = f22.e(parcel2, a);
                    break;
                case 4:
                    str3 = f22.e(parcel2, a);
                    break;
                case 5:
                    str4 = f22.e(parcel2, a);
                    break;
                case 6:
                    j = f22.s(parcel2, a);
                    break;
                case 7:
                    j2 = f22.s(parcel2, a);
                    break;
                case 8:
                    str5 = f22.e(parcel2, a);
                    break;
                case 9:
                    z = f22.i(parcel2, a);
                    break;
                case 10:
                    z2 = f22.i(parcel2, a);
                    break;
                case 11:
                    j6 = f22.s(parcel2, a);
                    break;
                case 12:
                    str6 = f22.e(parcel2, a);
                    break;
                case 13:
                    j3 = f22.s(parcel2, a);
                    break;
                case 14:
                    j4 = f22.s(parcel2, a);
                    break;
                case 15:
                    i = f22.q(parcel2, a);
                    break;
                case 16:
                    z3 = f22.i(parcel2, a);
                    break;
                case 17:
                    z4 = f22.i(parcel2, a);
                    break;
                case 18:
                    z5 = f22.i(parcel2, a);
                    break;
                case 19:
                    str7 = f22.e(parcel2, a);
                    break;
                case 21:
                    bool = f22.j(parcel2, a);
                    break;
                case 22:
                    j5 = f22.s(parcel2, a);
                    break;
                case 23:
                    arrayList = f22.g(parcel2, a);
                    break;
                case 24:
                    str8 = f22.e(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new ra3(str, str2, str3, str4, j, j2, str5, z, z2, j6, str6, j3, j4, i, z3, z4, z5, str7, bool, j5, (List<String>) arrayList, str8);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ra3[i];
    }
}
