package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.kr;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ox implements zr<ByteBuffer, qx> {
    @DexIgnore
    public static /* final */ a f; // = new a();
    @DexIgnore
    public static /* final */ b g; // = new b();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ px e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public kr a(kr.a aVar, mr mrVar, ByteBuffer byteBuffer, int i) {
            return new or(aVar, mrVar, byteBuffer, i);
        }
    }

    @DexIgnore
    public ox(Context context, List<ImageHeaderParser> list, au auVar, xt xtVar) {
        this(context, list, auVar, xtVar, g, f);
    }

    @DexIgnore
    public ox(Context context, List<ImageHeaderParser> list, au auVar, xt xtVar, b bVar, a aVar) {
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.e = new px(auVar, xtVar);
        this.c = bVar;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Queue<nr> a; // = r00.a(0);

        @DexIgnore
        public synchronized nr a(ByteBuffer byteBuffer) {
            nr poll;
            poll = this.a.poll();
            if (poll == null) {
                poll = new nr();
            }
            poll.a(byteBuffer);
            return poll;
        }

        @DexIgnore
        public synchronized void a(nr nrVar) {
            nrVar.a();
            this.a.offer(nrVar);
        }
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, xr xrVar) throws IOException {
        return !((Boolean) xrVar.a(wx.b)).booleanValue() && ur.a(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public sx a(ByteBuffer byteBuffer, int i, int i2, xr xrVar) {
        nr a2 = this.c.a(byteBuffer);
        try {
            return a(byteBuffer, i, i2, a2, xrVar);
        } finally {
            this.c.a(a2);
        }
    }

    @DexIgnore
    public final sx a(ByteBuffer byteBuffer, int i, int i2, nr nrVar, xr xrVar) {
        Bitmap.Config config;
        long a2 = m00.a();
        try {
            mr c2 = nrVar.c();
            if (c2.b() > 0) {
                if (c2.c() == 0) {
                    if (xrVar.a(wx.a) == qr.PREFER_RGB_565) {
                        config = Bitmap.Config.RGB_565;
                    } else {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    Bitmap.Config config2 = config;
                    kr a3 = this.d.a(this.e, c2, byteBuffer, a(c2, i, i2));
                    a3.a(config2);
                    a3.b();
                    Bitmap a4 = a3.a();
                    if (a4 == null) {
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + m00.a(a2));
                        }
                        return null;
                    }
                    sx sxVar = new sx(new qx(this.a, a3, cw.a(), i, i2, a4));
                    if (Log.isLoggable("BufferGifDecoder", 2)) {
                        Log.v("BufferGifDecoder", "Decoded GIF from stream in " + m00.a(a2));
                    }
                    return sxVar;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + m00.a(a2));
            }
        }
    }

    @DexIgnore
    public static int a(mr mrVar, int i, int i2) {
        int i3;
        int min = Math.min(mrVar.a() / i2, mrVar.d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i3);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + mrVar.d() + "x" + mrVar.a() + "]");
        }
        return max;
    }
}
