package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gl3<E> extends nm3<E> {
    @DexIgnore
    public /* final */ nm3<E> forward;

    @DexIgnore
    public gl3(nm3<E> nm3) {
        super(jn3.from(nm3.comparator()).reverse());
        this.forward = nm3;
    }

    @DexIgnore
    public E ceiling(E e) {
        return this.forward.floor(e);
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return this.forward.contains(obj);
    }

    @DexIgnore
    public nm3<E> createDescendingSet() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    public E floor(E e) {
        return this.forward.ceiling(e);
    }

    @DexIgnore
    public nm3<E> headSetImpl(E e, boolean z) {
        return this.forward.tailSet(e, z).descendingSet();
    }

    @DexIgnore
    public E higher(E e) {
        return this.forward.lower(e);
    }

    @DexIgnore
    public int indexOf(Object obj) {
        int indexOf = this.forward.indexOf(obj);
        if (indexOf == -1) {
            return indexOf;
        }
        return (size() - 1) - indexOf;
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.forward.isPartialView();
    }

    @DexIgnore
    public E lower(E e) {
        return this.forward.higher(e);
    }

    @DexIgnore
    public int size() {
        return this.forward.size();
    }

    @DexIgnore
    public nm3<E> subSetImpl(E e, boolean z, E e2, boolean z2) {
        return this.forward.subSet(e2, z2, e, z).descendingSet();
    }

    @DexIgnore
    public nm3<E> tailSetImpl(E e, boolean z) {
        return this.forward.headSet(e, z).descendingSet();
    }

    @DexIgnore
    public jo3<E> descendingIterator() {
        return this.forward.iterator();
    }

    @DexIgnore
    public nm3<E> descendingSet() {
        return this.forward;
    }

    @DexIgnore
    public jo3<E> iterator() {
        return this.forward.descendingIterator();
    }
}
