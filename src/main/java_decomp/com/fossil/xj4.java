package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xj4 extends iw {
    @DexIgnore
    public Bitmap a(au auVar, Bitmap bitmap, int i, int i2) {
        return a(auVar, bitmap);
    }

    @DexIgnore
    public static Bitmap a(au auVar, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        int min = Math.min(bitmap.getWidth(), bitmap.getHeight());
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, (bitmap.getWidth() - min) / 2, (bitmap.getHeight() - min) / 2, min, min);
        Bitmap a = auVar.a(min, min, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(a);
        Paint paint = new Paint();
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        paint.setShader(new BitmapShader(createBitmap, tileMode, tileMode));
        paint.setAntiAlias(true);
        float f = ((float) min) / 2.0f;
        canvas.drawCircle(f, f, f, paint);
        return a;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(xj4.class.getName().getBytes(vr.a));
    }
}
