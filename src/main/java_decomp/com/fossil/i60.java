package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ short b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<i60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final i60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new i60(cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).get(0)));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", ", "require: 1"));
        }

        @DexIgnore
        public i60 createFromParcel(Parcel parcel) {
            return new i60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new i60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m24createFromParcel(Parcel parcel) {
            return new i60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public i60(short s) {
        super(s60.CURRENT_HEART_RATE);
        this.b = s;
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) this.b).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(i60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((i60) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CurrentHeartRateConfig");
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.b);
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.CURRENT_HEART_RATE, (Object) Short.valueOf(this.b));
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ i60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = cw0.b(parcel.readByte());
    }
}
