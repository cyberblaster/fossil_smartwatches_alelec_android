package com.fossil;

import com.fossil.sq6;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dr6 {
    @DexIgnore
    public static dr6 a;

    @DexIgnore
    public abstract int a(Response.a aVar);

    @DexIgnore
    public abstract pr6 a(iq6 iq6, aq6 aq6, tr6 tr6, ar6 ar6);

    @DexIgnore
    public abstract qr6 a(iq6 iq6);

    @DexIgnore
    public abstract IOException a(dq6 dq6, IOException iOException);

    @DexIgnore
    public abstract Socket a(iq6 iq6, aq6 aq6, tr6 tr6);

    @DexIgnore
    public abstract void a(jq6 jq6, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract void a(sq6.a aVar, String str);

    @DexIgnore
    public abstract void a(sq6.a aVar, String str, String str2);

    @DexIgnore
    public abstract boolean a(aq6 aq6, aq6 aq62);

    @DexIgnore
    public abstract boolean a(iq6 iq6, pr6 pr6);

    @DexIgnore
    public abstract void b(iq6 iq6, pr6 pr6);
}
