package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lj4<TranscodeType> extends er<TranscodeType> implements Cloneable {
    @DexIgnore
    public lj4(wq wqVar, fr frVar, Class<TranscodeType> cls, Context context) {
        super(wqVar, frVar, cls, context);
    }

    @DexIgnore
    public lj4<TranscodeType> c() {
        return lj4.super.c();
    }

    @DexIgnore
    public lj4<TranscodeType> K() {
        return lj4.super.K();
    }

    @DexIgnore
    public lj4<TranscodeType> L() {
        return lj4.super.L();
    }

    @DexIgnore
    public lj4<TranscodeType> M() {
        return lj4.super.M();
    }

    @DexIgnore
    public lj4<TranscodeType> b(boolean z) {
        return lj4.super.b(z);
    }

    @DexIgnore
    public lj4<TranscodeType> clone() {
        return lj4.super.clone();
    }

    @DexIgnore
    public lj4<TranscodeType> b(int i) {
        return lj4.super.b(i);
    }

    @DexIgnore
    public lj4<TranscodeType> b() {
        return lj4.super.b();
    }

    @DexIgnore
    public lj4<TranscodeType> b(mz<TranscodeType> mzVar) {
        return lj4.super.b(mzVar);
    }

    @DexIgnore
    public lj4<TranscodeType> b(byte[] bArr) {
        return lj4.super.b(bArr);
    }

    @DexIgnore
    public lj4<TranscodeType> a(float f) {
        return lj4.super.a(f);
    }

    @DexIgnore
    public lj4<TranscodeType> a(ft ftVar) {
        return lj4.super.a(ftVar);
    }

    @DexIgnore
    public lj4<TranscodeType> a(br brVar) {
        return lj4.super.a(brVar);
    }

    @DexIgnore
    public lj4<TranscodeType> a(boolean z) {
        return lj4.super.a(z);
    }

    @DexIgnore
    public lj4<TranscodeType> a(int i, int i2) {
        return lj4.super.a(i, i2);
    }

    @DexIgnore
    public lj4<TranscodeType> a(vr vrVar) {
        return lj4.super.a(vrVar);
    }

    @DexIgnore
    public <Y> lj4<TranscodeType> a(wr<Y> wrVar, Y y) {
        return lj4.super.a(wrVar, y);
    }

    @DexIgnore
    public lj4<TranscodeType> a(Class<?> cls) {
        return lj4.super.a(cls);
    }

    @DexIgnore
    public lj4<TranscodeType> a(ow owVar) {
        return lj4.super.a(owVar);
    }

    @DexIgnore
    public lj4<TranscodeType> a(bs<Bitmap> bsVar) {
        return lj4.super.a(bsVar);
    }

    @DexIgnore
    public lj4<TranscodeType> a(gz<?> gzVar) {
        return lj4.super.a(gzVar);
    }

    @DexIgnore
    public lj4<TranscodeType> a(mz<TranscodeType> mzVar) {
        lj4.super.a(mzVar);
        return this;
    }

    @DexIgnore
    public lj4<TranscodeType> a(er<TranscodeType> erVar) {
        lj4.super.a(erVar);
        return this;
    }

    @DexIgnore
    public lj4<TranscodeType> a(Object obj) {
        lj4.super.a(obj);
        return this;
    }

    @DexIgnore
    public lj4<TranscodeType> a(String str) {
        lj4.super.a(str);
        return this;
    }

    @DexIgnore
    public lj4<TranscodeType> a(Uri uri) {
        lj4.super.a(uri);
        return this;
    }

    @DexIgnore
    public lj4<TranscodeType> a(Integer num) {
        return lj4.super.a(num);
    }
}
