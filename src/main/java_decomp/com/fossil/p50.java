package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum p50 {
    CLOCKWISE((byte) 1),
    COUNTER_CLOCKWISE((byte) 2),
    SHORTEST_PATH((byte) 3);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public p50(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
