package com.fossil;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yf3 {
    @DexIgnore
    public static /* final */ TimeInterpolator a; // = new LinearInterpolator();
    @DexIgnore
    public static /* final */ TimeInterpolator b; // = new nc();
    @DexIgnore
    public static /* final */ TimeInterpolator c; // = new mc();
    @DexIgnore
    public static /* final */ TimeInterpolator d; // = new oc();
    @DexIgnore
    public static /* final */ TimeInterpolator e; // = new DecelerateInterpolator();

    @DexIgnore
    public static float a(float f, float f2, float f3) {
        return f + (f3 * (f2 - f));
    }

    @DexIgnore
    public static int a(int i, int i2, float f) {
        return i + Math.round(f * ((float) (i2 - i)));
    }
}
