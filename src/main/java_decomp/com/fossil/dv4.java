package com.fossil;

import com.fossil.zf;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv4 extends zf.d<DailyHeartRateSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        wg6.b(dailyHeartRateSummary, "oldItem");
        wg6.b(dailyHeartRateSummary2, "newItem");
        return wg6.a((Object) dailyHeartRateSummary, (Object) dailyHeartRateSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        wg6.b(dailyHeartRateSummary, "oldItem");
        wg6.b(dailyHeartRateSummary2, "newItem");
        return bk4.d(dailyHeartRateSummary.getDate(), dailyHeartRateSummary2.getDate());
    }
}
