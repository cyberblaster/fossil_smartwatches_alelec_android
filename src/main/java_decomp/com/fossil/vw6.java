package com.fossil;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vw6 extends zw6<Activity> {
    @DexIgnore
    public vw6(Activity activity) {
        super(activity);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        h6.a((Activity) b(), strArr, i);
    }

    @DexIgnore
    public boolean b(String str) {
        return h6.a((Activity) b(), str);
    }

    @DexIgnore
    public Context a() {
        return (Context) b();
    }

    @DexIgnore
    public void b(String str, String str2, String str3, int i, int i2, String... strArr) {
        FragmentManager fragmentManager = ((Activity) b()).getFragmentManager();
        if (fragmentManager.findFragmentByTag("RationaleDialogFragment") instanceof uw6) {
            Log.d("ActPermissionHelper", "Found existing fragment, not showing rationale.");
        } else {
            uw6.a(str2, str3, str, i, i2, strArr).a(fragmentManager, "RationaleDialogFragment");
        }
    }
}
