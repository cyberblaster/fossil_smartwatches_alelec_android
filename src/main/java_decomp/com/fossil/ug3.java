package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ii3;
import java.lang.ref.WeakReference;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ug3 extends dj3 implements p7, Drawable.Callback, ii3.b {
    @DexIgnore
    public static /* final */ int[] J0; // = {16842910};
    @DexIgnore
    public static /* final */ ShapeDrawable K0; // = new ShapeDrawable(new OvalShape());
    @DexIgnore
    public ColorStateList A;
    @DexIgnore
    public PorterDuff.Mode A0; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public ColorStateList B;
    @DexIgnore
    public int[] B0;
    @DexIgnore
    public float C;
    @DexIgnore
    public boolean C0;
    @DexIgnore
    public float D;
    @DexIgnore
    public ColorStateList D0;
    @DexIgnore
    public ColorStateList E;
    @DexIgnore
    public WeakReference<a> E0; // = new WeakReference<>((Object) null);
    @DexIgnore
    public float F;
    @DexIgnore
    public TextUtils.TruncateAt F0;
    @DexIgnore
    public ColorStateList G;
    @DexIgnore
    public boolean G0;
    @DexIgnore
    public CharSequence H;
    @DexIgnore
    public int H0;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean I0;
    @DexIgnore
    public Drawable J;
    @DexIgnore
    public ColorStateList K;
    @DexIgnore
    public float L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public Drawable O;
    @DexIgnore
    public Drawable P;
    @DexIgnore
    public ColorStateList Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public CharSequence S;
    @DexIgnore
    public boolean T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public Drawable V;
    @DexIgnore
    public fg3 W;
    @DexIgnore
    public fg3 X;
    @DexIgnore
    public float Y;
    @DexIgnore
    public float Z;
    @DexIgnore
    public float a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public /* final */ Context g0;
    @DexIgnore
    public /* final */ Paint h0; // = new Paint(1);
    @DexIgnore
    public /* final */ Paint i0;
    @DexIgnore
    public /* final */ Paint.FontMetrics j0; // = new Paint.FontMetrics();
    @DexIgnore
    public /* final */ RectF k0; // = new RectF();
    @DexIgnore
    public /* final */ PointF l0; // = new PointF();
    @DexIgnore
    public /* final */ Path m0; // = new Path();
    @DexIgnore
    public /* final */ ii3 n0;
    @DexIgnore
    public int o0;
    @DexIgnore
    public int p0;
    @DexIgnore
    public int q0;
    @DexIgnore
    public int r0;
    @DexIgnore
    public int s0;
    @DexIgnore
    public int t0;
    @DexIgnore
    public boolean u0;
    @DexIgnore
    public int v0;
    @DexIgnore
    public int w0; // = 255;
    @DexIgnore
    public ColorFilter x0;
    @DexIgnore
    public PorterDuffColorFilter y0;
    @DexIgnore
    public ColorStateList z0;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public ug3(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(context);
        this.g0 = context;
        this.n0 = new ii3(this);
        this.H = "";
        this.n0.b().density = context.getResources().getDisplayMetrics().density;
        this.i0 = null;
        Paint paint = this.i0;
        if (paint != null) {
            paint.setStyle(Paint.Style.STROKE);
        }
        setState(J0);
        b(J0);
        this.G0 = true;
        if (ui3.a) {
            K0.setTint(-1);
        }
    }

    @DexIgnore
    public static ug3 a(Context context, AttributeSet attributeSet, int i, int i2) {
        ug3 ug3 = new ug3(context, attributeSet, i, i2);
        ug3.a(attributeSet, i, i2);
        return ug3;
    }

    @DexIgnore
    public void A(int i) {
        this.H0 = i;
    }

    @DexIgnore
    public void B(int i) {
        h(u0.b(this.g0, i));
    }

    @DexIgnore
    public void C(int i) {
        b(fg3.a(this.g0, i));
    }

    @DexIgnore
    public float D() {
        if (r0() || q0()) {
            return this.Z + this.L + this.a0;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public float E() {
        return s0() ? this.d0 + this.R + this.e0 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float F() {
        this.n0.b().getFontMetrics(this.j0);
        Paint.FontMetrics fontMetrics = this.j0;
        return (fontMetrics.descent + fontMetrics.ascent) / 2.0f;
    }

    @DexIgnore
    public final boolean G() {
        return this.U && this.V != null && this.T;
    }

    @DexIgnore
    public Drawable H() {
        return this.V;
    }

    @DexIgnore
    public ColorStateList I() {
        return this.B;
    }

    @DexIgnore
    public float J() {
        return this.I0 ? q() : this.D;
    }

    @DexIgnore
    public float K() {
        return this.f0;
    }

    @DexIgnore
    public Drawable L() {
        Drawable drawable = this.J;
        if (drawable != null) {
            return o7.h(drawable);
        }
        return null;
    }

    @DexIgnore
    public float M() {
        return this.L;
    }

    @DexIgnore
    public ColorStateList N() {
        return this.K;
    }

    @DexIgnore
    public float O() {
        return this.C;
    }

    @DexIgnore
    public float P() {
        return this.Y;
    }

    @DexIgnore
    public ColorStateList Q() {
        return this.E;
    }

    @DexIgnore
    public float R() {
        return this.F;
    }

    @DexIgnore
    public Drawable S() {
        Drawable drawable = this.O;
        if (drawable != null) {
            return o7.h(drawable);
        }
        return null;
    }

    @DexIgnore
    public CharSequence T() {
        return this.S;
    }

    @DexIgnore
    public float U() {
        return this.e0;
    }

    @DexIgnore
    public float V() {
        return this.R;
    }

    @DexIgnore
    public float W() {
        return this.d0;
    }

    @DexIgnore
    public int[] X() {
        return this.B0;
    }

    @DexIgnore
    public ColorStateList Y() {
        return this.Q;
    }

    @DexIgnore
    public TextUtils.TruncateAt Z() {
        return this.F0;
    }

    @DexIgnore
    public fg3 a0() {
        return this.X;
    }

    @DexIgnore
    public final void b(Canvas canvas, Rect rect) {
        if (!this.I0) {
            this.h0.setColor(this.p0);
            this.h0.setStyle(Paint.Style.FILL);
            this.h0.setColorFilter(j0());
            this.k0.set(rect);
            canvas.drawRoundRect(this.k0, J(), J(), this.h0);
        }
    }

    @DexIgnore
    public float b0() {
        return this.a0;
    }

    @DexIgnore
    public final void c(Canvas canvas, Rect rect) {
        if (r0()) {
            a(rect, this.k0);
            RectF rectF = this.k0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.J.setBounds(0, 0, (int) this.k0.width(), (int) this.k0.height());
            this.J.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public float c0() {
        return this.Z;
    }

    @DexIgnore
    public final void d(Canvas canvas, Rect rect) {
        if (this.F > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && !this.I0) {
            this.h0.setColor(this.r0);
            this.h0.setStyle(Paint.Style.STROKE);
            if (!this.I0) {
                this.h0.setColorFilter(j0());
            }
            RectF rectF = this.k0;
            float f = this.F;
            rectF.set(((float) rect.left) + (f / 2.0f), ((float) rect.top) + (f / 2.0f), ((float) rect.right) - (f / 2.0f), ((float) rect.bottom) - (f / 2.0f));
            float f2 = this.D - (this.F / 2.0f);
            canvas.drawRoundRect(this.k0, f2, f2, this.h0);
        }
    }

    @DexIgnore
    public ColorStateList d0() {
        return this.G;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && getAlpha() != 0) {
            int i = 0;
            int i2 = this.w0;
            if (i2 < 255) {
                i = sg3.a(canvas, (float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom, i2);
            }
            e(canvas, bounds);
            b(canvas, bounds);
            if (this.I0) {
                super.draw(canvas);
            }
            d(canvas, bounds);
            g(canvas, bounds);
            c(canvas, bounds);
            a(canvas, bounds);
            if (this.G0) {
                i(canvas, bounds);
            }
            f(canvas, bounds);
            h(canvas, bounds);
            if (this.w0 < 255) {
                canvas.restoreToCount(i);
            }
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, Rect rect) {
        if (!this.I0) {
            this.h0.setColor(this.o0);
            this.h0.setStyle(Paint.Style.FILL);
            this.k0.set(rect);
            canvas.drawRoundRect(this.k0, J(), J(), this.h0);
        }
    }

    @DexIgnore
    public fg3 e0() {
        return this.W;
    }

    @DexIgnore
    public void f(boolean z) {
        if (this.C0 != z) {
            this.C0 = z;
            t0();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public CharSequence f0() {
        return this.H;
    }

    @DexIgnore
    public final void g(Canvas canvas, Rect rect) {
        this.h0.setColor(this.s0);
        this.h0.setStyle(Paint.Style.FILL);
        this.k0.set(rect);
        if (!this.I0) {
            canvas.drawRoundRect(this.k0, J(), J(), this.h0);
            return;
        }
        b(new RectF(rect), this.m0);
        super.a(canvas, this.h0, this.m0, e());
    }

    @DexIgnore
    public qi3 g0() {
        return this.n0.a();
    }

    @DexIgnore
    public int getAlpha() {
        return this.w0;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.x0;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return (int) this.C;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return Math.min(Math.round(this.Y + D() + this.b0 + this.n0.a(f0().toString()) + this.c0 + E() + this.f0), this.H0);
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.I0) {
            super.getOutline(outline);
            return;
        }
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.D);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), getIntrinsicHeight(), this.D);
        }
        outline.setAlpha(((float) getAlpha()) / 255.0f);
    }

    @DexIgnore
    public final void h(Canvas canvas, Rect rect) {
        Paint paint = this.i0;
        if (paint != null) {
            paint.setColor(f7.c(-16777216, 127));
            canvas.drawRect(rect, this.i0);
            if (r0() || q0()) {
                a(rect, this.k0);
                canvas.drawRect(this.k0, this.i0);
            }
            if (this.H != null) {
                canvas.drawLine((float) rect.left, rect.exactCenterY(), (float) rect.right, rect.exactCenterY(), this.i0);
            }
            if (s0()) {
                c(rect, this.k0);
                canvas.drawRect(this.k0, this.i0);
            }
            this.i0.setColor(f7.c(-65536, 127));
            b(rect, this.k0);
            canvas.drawRect(this.k0, this.i0);
            this.i0.setColor(f7.c(-16711936, 127));
            d(rect, this.k0);
            canvas.drawRect(this.k0, this.i0);
        }
    }

    @DexIgnore
    public float h0() {
        return this.c0;
    }

    @DexIgnore
    public final void i(Canvas canvas, Rect rect) {
        if (this.H != null) {
            Paint.Align a2 = a(rect, this.l0);
            e(rect, this.k0);
            if (this.n0.a() != null) {
                this.n0.b().drawableState = getState();
                this.n0.a(this.g0);
            }
            this.n0.b().setTextAlign(a2);
            int i = 0;
            boolean z = Math.round(this.n0.a(f0().toString())) > Math.round(this.k0.width());
            if (z) {
                i = canvas.save();
                canvas.clipRect(this.k0);
            }
            CharSequence charSequence = this.H;
            if (z && this.F0 != null) {
                charSequence = TextUtils.ellipsize(charSequence, this.n0.b(), this.k0.width(), this.F0);
            }
            CharSequence charSequence2 = charSequence;
            int length = charSequence2.length();
            PointF pointF = this.l0;
            canvas.drawText(charSequence2, 0, length, pointF.x, pointF.y, this.n0.b());
            if (z) {
                canvas.restoreToCount(i);
            }
        }
    }

    @DexIgnore
    public float i0() {
        return this.b0;
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @DexIgnore
    public boolean isStateful() {
        return i(this.A) || i(this.B) || i(this.E) || (this.C0 && i(this.D0)) || b(this.n0.a()) || G() || f(this.J) || f(this.V) || i(this.z0);
    }

    @DexIgnore
    public void j(float f) {
        if (this.Y != f) {
            this.Y = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public final ColorFilter j0() {
        ColorFilter colorFilter = this.x0;
        return colorFilter != null ? colorFilter : this.y0;
    }

    @DexIgnore
    public void k(float f) {
        if (this.F != f) {
            this.F = f;
            this.h0.setStrokeWidth(f);
            if (this.I0) {
                super.e(f);
            }
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean k0() {
        return this.C0;
    }

    @DexIgnore
    public void l(int i) {
        h(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public boolean l0() {
        return this.T;
    }

    @DexIgnore
    public void m(int i) {
        d(u0.b(this.g0, i));
    }

    @DexIgnore
    public boolean m0() {
        return f(this.O);
    }

    @DexIgnore
    public void n(int i) {
        c(this.g0.getResources().getBoolean(i));
    }

    @DexIgnore
    public boolean n0() {
        return this.N;
    }

    @DexIgnore
    public void o(int i) {
        i(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public void o0() {
        a aVar = (a) this.E0.get();
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public boolean onLayoutDirectionChanged(int i) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i);
        if (r0()) {
            onLayoutDirectionChanged |= o7.a(this.J, i);
        }
        if (q0()) {
            onLayoutDirectionChanged |= o7.a(this.V, i);
        }
        if (s0()) {
            onLayoutDirectionChanged |= o7.a(this.O, i);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        boolean onLevelChange = super.onLevelChange(i);
        if (r0()) {
            onLevelChange |= this.J.setLevel(i);
        }
        if (q0()) {
            onLevelChange |= this.V.setLevel(i);
        }
        if (s0()) {
            onLevelChange |= this.O.setLevel(i);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        if (this.I0) {
            super.onStateChange(iArr);
        }
        return a(iArr, X());
    }

    @DexIgnore
    public void p(int i) {
        j(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public boolean p0() {
        return this.G0;
    }

    @DexIgnore
    public void q(int i) {
        e(u0.b(this.g0, i));
    }

    @DexIgnore
    public final boolean q0() {
        return this.U && this.V != null && this.u0;
    }

    @DexIgnore
    public void r(int i) {
        k(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public final boolean r0() {
        return this.I && this.J != null;
    }

    @DexIgnore
    public void s(int i) {
        l(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public final boolean s0() {
        return this.N && this.O != null;
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @DexIgnore
    public void setAlpha(int i) {
        if (this.w0 != i) {
            this.w0 = i;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.x0 != colorFilter) {
            this.x0 = colorFilter;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        if (this.z0 != colorStateList) {
            this.z0 = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        if (this.A0 != mode) {
            this.A0 = mode;
            this.y0 = qh3.a(this, this.z0, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (r0()) {
            visible |= this.J.setVisible(z, z2);
        }
        if (q0()) {
            visible |= this.V.setVisible(z, z2);
        }
        if (s0()) {
            visible |= this.O.setVisible(z, z2);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    @DexIgnore
    public void t(int i) {
        d(u0.c(this.g0, i));
    }

    @DexIgnore
    public final void t0() {
        this.D0 = this.C0 ? ui3.b(this.G) : null;
    }

    @DexIgnore
    public void u(int i) {
        m(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    @TargetApi(21)
    public final void u0() {
        this.P = new RippleDrawable(ui3.b(d0()), this.O, K0);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    @DexIgnore
    public void v(int i) {
        n(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public void w(int i) {
        g(u0.b(this.g0, i));
    }

    @DexIgnore
    public void x(int i) {
        a(fg3.a(this.g0, i));
    }

    @DexIgnore
    public void y(int i) {
        o(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public void z(int i) {
        p(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public void l(float f) {
        if (this.e0 != f) {
            this.e0 = f;
            invalidateSelf();
            if (s0()) {
                o0();
            }
        }
    }

    @DexIgnore
    public void m(float f) {
        if (this.R != f) {
            this.R = f;
            invalidateSelf();
            if (s0()) {
                o0();
            }
        }
    }

    @DexIgnore
    public void n(float f) {
        if (this.d0 != f) {
            this.d0 = f;
            invalidateSelf();
            if (s0()) {
                o0();
            }
        }
    }

    @DexIgnore
    public void o(float f) {
        if (this.a0 != f) {
            float D2 = D();
            this.a0 = f;
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void p(float f) {
        if (this.Z != f) {
            float D2 = D();
            this.Z = f;
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void q(float f) {
        if (this.c0 != f) {
            this.c0 = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void r(float f) {
        if (this.b0 != f) {
            this.b0 = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void D(int i) {
        a(new qi3(this.g0, i));
    }

    @DexIgnore
    public void E(int i) {
        q(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public void F(int i) {
        r(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet, int i, int i2) {
        TypedArray c = ki3.c(this.g0, attributeSet, xf3.Chip, i, i2, new int[0]);
        this.I0 = c.hasValue(xf3.Chip_shapeAppearance);
        f(pi3.a(this.g0, c, xf3.Chip_chipSurfaceColor));
        c(pi3.a(this.g0, c, xf3.Chip_chipBackgroundColor));
        i(c.getDimension(xf3.Chip_chipMinHeight, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        if (c.hasValue(xf3.Chip_chipCornerRadius)) {
            f(c.getDimension(xf3.Chip_chipCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        }
        e(pi3.a(this.g0, c, xf3.Chip_chipStrokeColor));
        k(c.getDimension(xf3.Chip_chipStrokeWidth, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        h(pi3.a(this.g0, c, xf3.Chip_rippleColor));
        b(c.getText(xf3.Chip_android_text));
        a(pi3.c(this.g0, c, xf3.Chip_android_textAppearance));
        int i3 = c.getInt(xf3.Chip_android_ellipsize, 0);
        if (i3 == 1) {
            a(TextUtils.TruncateAt.START);
        } else if (i3 == 2) {
            a(TextUtils.TruncateAt.MIDDLE);
        } else if (i3 == 3) {
            a(TextUtils.TruncateAt.END);
        }
        c(c.getBoolean(xf3.Chip_chipIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconVisible") != null)) {
            c(c.getBoolean(xf3.Chip_chipIconEnabled, false));
        }
        c(pi3.b(this.g0, c, xf3.Chip_chipIcon));
        if (c.hasValue(xf3.Chip_chipIconTint)) {
            d(pi3.a(this.g0, c, xf3.Chip_chipIconTint));
        }
        h(c.getDimension(xf3.Chip_chipIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        d(c.getBoolean(xf3.Chip_closeIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconVisible") != null)) {
            d(c.getBoolean(xf3.Chip_closeIconEnabled, false));
        }
        d(pi3.b(this.g0, c, xf3.Chip_closeIcon));
        g(pi3.a(this.g0, c, xf3.Chip_closeIconTint));
        m(c.getDimension(xf3.Chip_closeIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        a(c.getBoolean(xf3.Chip_android_checkable, false));
        b(c.getBoolean(xf3.Chip_checkedIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconVisible") != null)) {
            b(c.getBoolean(xf3.Chip_checkedIconEnabled, false));
        }
        b(pi3.b(this.g0, c, xf3.Chip_checkedIcon));
        b(fg3.a(this.g0, c, xf3.Chip_showMotionSpec));
        a(fg3.a(this.g0, c, xf3.Chip_hideMotionSpec));
        j(c.getDimension(xf3.Chip_chipStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        p(c.getDimension(xf3.Chip_iconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        o(c.getDimension(xf3.Chip_iconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        r(c.getDimension(xf3.Chip_textStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        q(c.getDimension(xf3.Chip_textEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        n(c.getDimension(xf3.Chip_closeIconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        l(c.getDimension(xf3.Chip_closeIconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        g(c.getDimension(xf3.Chip_chipEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        A(c.getDimensionPixelSize(xf3.Chip_android_maxWidth, Integer.MAX_VALUE));
        c.recycle();
    }

    @DexIgnore
    public final void f(Canvas canvas, Rect rect) {
        if (s0()) {
            c(rect, this.k0);
            RectF rectF = this.k0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.O.setBounds(0, 0, (int) this.k0.width(), (int) this.k0.height());
            if (ui3.a) {
                this.P.setBounds(this.O.getBounds());
                this.P.jumpToCurrentState();
                this.P.draw(canvas);
            } else {
                this.O.draw(canvas);
            }
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public void j(int i) {
        g(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public final void e(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (this.H != null) {
            float D2 = this.Y + D() + this.b0;
            float E2 = this.f0 + E() + this.c0;
            if (o7.e(this) == 0) {
                rectF.left = ((float) rect.left) + D2;
                rectF.right = ((float) rect.right) - E2;
            } else {
                rectF.left = ((float) rect.left) + E2;
                rectF.right = ((float) rect.right) - D2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public final void b(Rect rect, RectF rectF) {
        rectF.set(rect);
        if (s0()) {
            float f = this.f0 + this.e0 + this.R + this.d0 + this.c0;
            if (o7.e(this) == 0) {
                rectF.right = ((float) rect.right) - f;
            } else {
                rectF.left = ((float) rect.left) + f;
            }
        }
    }

    @DexIgnore
    public void k(int i) {
        c(u0.c(this.g0, i));
    }

    @DexIgnore
    public void g(ColorStateList colorStateList) {
        if (this.Q != colorStateList) {
            this.Q = colorStateList;
            if (s0()) {
                o7.a(this.O, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void c(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (s0()) {
            float f = this.f0 + this.e0;
            if (o7.e(this) == 0) {
                rectF.right = ((float) rect.right) - f;
                rectF.left = rectF.right - this.R;
            } else {
                rectF.left = ((float) rect.left) + f;
                rectF.right = rectF.left + this.R;
            }
            float exactCenterY = rect.exactCenterY();
            float f2 = this.R;
            rectF.top = exactCenterY - (f2 / 2.0f);
            rectF.bottom = rectF.top + f2;
        }
    }

    @DexIgnore
    public final void d(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (s0()) {
            float f = this.f0 + this.e0 + this.R + this.d0 + this.c0;
            if (o7.e(this) == 0) {
                rectF.right = (float) rect.right;
                rectF.left = rectF.right - f;
            } else {
                int i = rect.left;
                rectF.left = (float) i;
                rectF.right = ((float) i) + f;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public boolean b(int[] iArr) {
        if (Arrays.equals(this.B0, iArr)) {
            return false;
        }
        this.B0 = iArr;
        if (s0()) {
            return a(getState(), iArr);
        }
        return false;
    }

    @DexIgnore
    public void g(int i) {
        b(this.g0.getResources().getBoolean(i));
    }

    @DexIgnore
    public void g(float f) {
        if (this.f0 != f) {
            this.f0 = f;
            invalidateSelf();
            o0();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r0 = r0.b;
     */
    @DexIgnore
    public static boolean b(qi3 qi3) {
        ColorStateList colorStateList;
        return (qi3 == null || colorStateList == null || !colorStateList.isStateful()) ? false : true;
    }

    @DexIgnore
    public static boolean f(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    @DexIgnore
    public final void e(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback((Drawable.Callback) null);
        }
    }

    @DexIgnore
    public void e(ColorStateList colorStateList) {
        if (this.E != colorStateList) {
            this.E = colorStateList;
            if (this.I0) {
                b(colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void f(ColorStateList colorStateList) {
        if (this.A != colorStateList) {
            this.A = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (!TextUtils.equals(this.H, charSequence)) {
            this.H = charSequence;
            this.n0.a(true);
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.B != colorStateList) {
            this.B = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void d(ColorStateList colorStateList) {
        this.M = true;
        if (this.K != colorStateList) {
            this.K = colorStateList;
            if (r0()) {
                o7.a(this.J, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public static boolean i(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    @DexIgnore
    public void h(int i) {
        c(u0.b(this.g0, i));
    }

    @DexIgnore
    @Deprecated
    public void f(float f) {
        if (this.D != f) {
            this.D = f;
            setShapeAppearanceModel(n().a(f));
        }
    }

    @DexIgnore
    public void h(ColorStateList colorStateList) {
        if (this.G != colorStateList) {
            this.G = colorStateList;
            t0();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void i(float f) {
        if (this.C != f) {
            this.C = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void c(boolean z) {
        if (this.I != z) {
            boolean r02 = r0();
            this.I = z;
            boolean r03 = r0();
            if (r02 != r03) {
                if (r03) {
                    a(this.J);
                } else {
                    e(this.J);
                }
                invalidateSelf();
                o0();
            }
        }
    }

    @DexIgnore
    public void e(int i) {
        a(this.g0.getResources().getBoolean(i));
    }

    @DexIgnore
    public void b(boolean z) {
        if (this.U != z) {
            boolean q02 = q0();
            this.U = z;
            boolean q03 = q0();
            if (q02 != q03) {
                if (q03) {
                    a(this.V);
                } else {
                    e(this.V);
                }
                invalidateSelf();
                o0();
            }
        }
    }

    @DexIgnore
    public void e(boolean z) {
        this.G0 = z;
    }

    @DexIgnore
    public void f(int i) {
        b(u0.c(this.g0, i));
    }

    @DexIgnore
    public void d(boolean z) {
        if (this.N != z) {
            boolean s02 = s0();
            this.N = z;
            boolean s03 = s0();
            if (s02 != s03) {
                if (s03) {
                    a(this.O);
                } else {
                    e(this.O);
                }
                invalidateSelf();
                o0();
            }
        }
    }

    @DexIgnore
    public void h(float f) {
        if (this.L != f) {
            float D2 = D();
            this.L = f;
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    @Deprecated
    public void i(int i) {
        f(this.g0.getResources().getDimension(i));
    }

    @DexIgnore
    public void c(Drawable drawable) {
        Drawable L2 = L();
        if (L2 != drawable) {
            float D2 = D();
            this.J = drawable != null ? o7.i(drawable).mutate() : null;
            float D3 = D();
            e(L2);
            if (r0()) {
                a(this.J);
            }
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void b(Drawable drawable) {
        if (this.V != drawable) {
            float D2 = D();
            this.V = drawable;
            float D3 = D();
            e(this.V);
            a(this.V);
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void d(Drawable drawable) {
        Drawable S2 = S();
        if (S2 != drawable) {
            float E2 = E();
            this.O = drawable != null ? o7.i(drawable).mutate() : null;
            if (ui3.a) {
                u0();
            }
            float E3 = E();
            e(S2);
            if (s0()) {
                a(this.O);
            }
            invalidateSelf();
            if (E2 != E3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void b(fg3 fg3) {
        this.W = fg3;
    }

    @DexIgnore
    public void a(a aVar) {
        this.E0 = new WeakReference<>(aVar);
    }

    @DexIgnore
    public void a(RectF rectF) {
        d(getBounds(), rectF);
    }

    @DexIgnore
    public final void a(Canvas canvas, Rect rect) {
        if (q0()) {
            a(rect, this.k0);
            RectF rectF = this.k0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.V.setBounds(0, 0, (int) this.k0.width(), (int) this.k0.height());
            this.V.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public final void a(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (r0() || q0()) {
            float f = this.Y + this.Z;
            if (o7.e(this) == 0) {
                rectF.left = ((float) rect.left) + f;
                rectF.right = rectF.left + this.L;
            } else {
                rectF.right = ((float) rect.right) - f;
                rectF.left = rectF.right - this.L;
            }
            float exactCenterY = rect.exactCenterY();
            float f2 = this.L;
            rectF.top = exactCenterY - (f2 / 2.0f);
            rectF.bottom = rectF.top + f2;
        }
    }

    @DexIgnore
    public Paint.Align a(Rect rect, PointF pointF) {
        pointF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        Paint.Align align = Paint.Align.LEFT;
        if (this.H != null) {
            float D2 = this.Y + D() + this.b0;
            if (o7.e(this) == 0) {
                pointF.x = ((float) rect.left) + D2;
                align = Paint.Align.LEFT;
            } else {
                pointF.x = ((float) rect.right) - D2;
                align = Paint.Align.RIGHT;
            }
            pointF.y = ((float) rect.centerY()) - F();
        }
        return align;
    }

    @DexIgnore
    public void a() {
        o0();
        invalidateSelf();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x011c  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x014b  */
    public final boolean a(int[] iArr, int[] iArr2) {
        boolean z;
        int colorForState;
        boolean onStateChange = super.onStateChange(iArr);
        ColorStateList colorStateList = this.A;
        int colorForState2 = colorStateList != null ? colorStateList.getColorForState(iArr, this.o0) : 0;
        if (this.o0 != colorForState2) {
            this.o0 = colorForState2;
            onStateChange = true;
        }
        ColorStateList colorStateList2 = this.B;
        int colorForState3 = colorStateList2 != null ? colorStateList2.getColorForState(iArr, this.p0) : 0;
        if (this.p0 != colorForState3) {
            this.p0 = colorForState3;
            onStateChange = true;
        }
        int a2 = yg3.a(colorForState2, colorForState3);
        if ((this.q0 != a2) || (h() == null)) {
            this.q0 = a2;
            a(ColorStateList.valueOf(this.q0));
            onStateChange = true;
        }
        ColorStateList colorStateList3 = this.E;
        int colorForState4 = colorStateList3 != null ? colorStateList3.getColorForState(iArr, this.r0) : 0;
        if (this.r0 != colorForState4) {
            this.r0 = colorForState4;
            onStateChange = true;
        }
        int colorForState5 = (this.D0 == null || !ui3.a(iArr)) ? 0 : this.D0.getColorForState(iArr, this.s0);
        if (this.s0 != colorForState5) {
            this.s0 = colorForState5;
            if (this.C0) {
                onStateChange = true;
            }
        }
        int colorForState6 = (this.n0.a() == null || this.n0.a().b == null) ? 0 : this.n0.a().b.getColorForState(iArr, this.t0);
        if (this.t0 != colorForState6) {
            this.t0 = colorForState6;
            onStateChange = true;
        }
        boolean z2 = a(getState(), 16842912) && this.T;
        if (!(this.u0 == z2 || this.V == null)) {
            float D2 = D();
            this.u0 = z2;
            if (D2 != D()) {
                onStateChange = true;
                z = true;
                ColorStateList colorStateList4 = this.z0;
                colorForState = colorStateList4 == null ? colorStateList4.getColorForState(iArr, this.v0) : 0;
                if (this.v0 != colorForState) {
                    this.v0 = colorForState;
                    this.y0 = qh3.a(this, this.z0, this.A0);
                    onStateChange = true;
                }
                if (f(this.J)) {
                    onStateChange |= this.J.setState(iArr);
                }
                if (f(this.V)) {
                    onStateChange |= this.V.setState(iArr);
                }
                if (f(this.O)) {
                    int[] iArr3 = new int[(iArr.length + iArr2.length)];
                    System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
                    System.arraycopy(iArr2, 0, iArr3, iArr.length, iArr2.length);
                    onStateChange |= this.O.setState(iArr3);
                }
                if (ui3.a && f(this.P)) {
                    onStateChange |= this.P.setState(iArr2);
                }
                if (onStateChange) {
                    invalidateSelf();
                }
                if (z) {
                    o0();
                }
                return onStateChange;
            }
            onStateChange = true;
        }
        z = false;
        ColorStateList colorStateList42 = this.z0;
        if (colorStateList42 == null) {
        }
        if (this.v0 != colorForState) {
        }
        if (f(this.J)) {
        }
        if (f(this.V)) {
        }
        if (f(this.O)) {
        }
        onStateChange |= this.P.setState(iArr2);
        if (onStateChange) {
        }
        if (z) {
        }
        return onStateChange;
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            o7.a(drawable, o7.e(this));
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            if (drawable == this.O) {
                if (drawable.isStateful()) {
                    drawable.setState(X());
                }
                o7.a(drawable, this.Q);
                return;
            }
            if (drawable.isStateful()) {
                drawable.setState(getState());
            }
            Drawable drawable2 = this.J;
            if (drawable == drawable2 && this.M) {
                o7.a(drawable2, this.K);
            }
        }
    }

    @DexIgnore
    public static boolean a(int[] iArr, int i) {
        if (iArr == null) {
            return false;
        }
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(qi3 qi3) {
        this.n0.a(qi3, this.g0);
    }

    @DexIgnore
    public void a(TextUtils.TruncateAt truncateAt) {
        this.F0 = truncateAt;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        if (this.S != charSequence) {
            this.S = l8.b().a(charSequence);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.T != z) {
            this.T = z;
            float D2 = D();
            if (!z && this.u0) {
                this.u0 = false;
            }
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void a(fg3 fg3) {
        this.X = fg3;
    }
}
