package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nz extends gz<nz> {
    @DexIgnore
    public static nz E;
    @DexIgnore
    public static nz F;

    @DexIgnore
    public static nz b(ft ftVar) {
        return (nz) new nz().a(ftVar);
    }

    @DexIgnore
    public static nz c(boolean z) {
        if (z) {
            if (E == null) {
                E = (nz) ((nz) new nz().a(true)).a();
            }
            return E;
        }
        if (F == null) {
            F = (nz) ((nz) new nz().a(false)).a();
        }
        return F;
    }

    @DexIgnore
    public static nz b(vr vrVar) {
        return (nz) new nz().a(vrVar);
    }

    @DexIgnore
    public static nz b(Class<?> cls) {
        return (nz) new nz().a(cls);
    }
}
