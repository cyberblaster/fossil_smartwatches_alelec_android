package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.iw5;
import com.fossil.m24;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn4 extends BroadcastReceiver {
    @DexIgnore
    public an4 a;
    @DexIgnore
    public iw5 b;
    @DexIgnore
    public AlarmHelper c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<iw5.d, iw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ rn4 a;

        @DexIgnore
        public b(rn4 rn4) {
            this.a = rn4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* renamed from: a */
        public void onSuccess(iw5.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "Re-schedule SyncDataReminder");
            AlarmHelper a2 = this.a.a();
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            a2.e(applicationContext);
        }

        @DexIgnore
        public void a(iw5.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", cVar.toString());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public rn4() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.c;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        wg6.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r10v9, types: [com.fossil.iw5, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.fossil.rn4$b, com.portfolio.platform.CoroutineUseCase$e] */
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("TimeTickReceiver", "onReceive() - action = " + action);
        an4 an4 = this.a;
        if (an4 != null) {
            long g = an4.g(PortfolioApp.get.instance().e());
            if (!TextUtils.isEmpty(action) && wg6.a((Object) action, (Object) "android.intent.action.TIME_TICK") && System.currentTimeMillis() - g <= ScanService.BLE_SCAN_TIMEOUT) {
                FossilNotificationBar.a aVar = FossilNotificationBar.c;
                if (context != null) {
                    aVar.a(context);
                } else {
                    wg6.a();
                    throw null;
                }
            }
            if (wg6.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
                TimeZone timeZone = TimeZone.getDefault();
                try {
                    Calendar instance = Calendar.getInstance();
                    wg6.a((Object) instance, "calendar");
                    Calendar instance2 = Calendar.getInstance();
                    wg6.a((Object) instance2, "Calendar.getInstance()");
                    instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                    Calendar instance3 = Calendar.getInstance();
                    wg6.a((Object) instance3, "Calendar.getInstance()");
                    if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                        FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "DST Changed.");
                        Object r10 = this.b;
                        if (r10 != 0) {
                            r10.a(new iw5.b(), new b(this));
                        } else {
                            wg6.d("mDstChangeUseCase");
                            throw null;
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("TimeTickReceiver", ".timeZoneChangeReceiver - ex=" + e);
                }
            }
        } else {
            wg6.d("mSharedPreferencesManager");
            throw null;
        }
    }
}
