package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.rv1;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jy1 implements wy1, c02 {
    @DexIgnore
    public /* final */ Lock a;
    @DexIgnore
    public /* final */ Condition b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ kv1 d;
    @DexIgnore
    public /* final */ ly1 e;
    @DexIgnore
    public /* final */ Map<rv1.c<?>, rv1.f> f;
    @DexIgnore
    public /* final */ Map<rv1.c<?>, gv1> g; // = new HashMap();
    @DexIgnore
    public /* final */ e12 h;
    @DexIgnore
    public /* final */ Map<rv1<?>, Boolean> i;
    @DexIgnore
    public /* final */ rv1.a<? extends ac3, lb3> j;
    @DexIgnore
    public volatile gy1 o;
    @DexIgnore
    public gv1 p; // = null;
    @DexIgnore
    public int q;
    @DexIgnore
    public /* final */ ay1 r;
    @DexIgnore
    public /* final */ xy1 s;

    @DexIgnore
    public jy1(Context context, ay1 ay1, Lock lock, Looper looper, kv1 kv1, Map<rv1.c<?>, rv1.f> map, e12 e12, Map<rv1<?>, Boolean> map2, rv1.a<? extends ac3, lb3> aVar, ArrayList<a02> arrayList, xy1 xy1) {
        this.c = context;
        this.a = lock;
        this.d = kv1;
        this.f = map;
        this.h = e12;
        this.i = map2;
        this.j = aVar;
        this.r = ay1;
        this.s = xy1;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            a02 a02 = arrayList.get(i2);
            i2++;
            a02.a((c02) this);
        }
        this.e = new ly1(this, looper);
        this.b = lock.newCondition();
        this.o = new xx1(this);
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t) {
        t.g();
        return this.o.a(t);
    }

    @DexIgnore
    public final boolean a(yw1 yw1) {
        return false;
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t) {
        t.g();
        return this.o.b(t);
    }

    @DexIgnore
    public final boolean c() {
        return this.o instanceof jx1;
    }

    @DexIgnore
    public final void d() {
    }

    @DexIgnore
    public final void e() {
        if (c()) {
            ((jx1) this.o).d();
        }
    }

    @DexIgnore
    public final gv1 f() {
        b();
        while (g()) {
            try {
                this.b.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new gv1(15, (PendingIntent) null);
            }
        }
        if (c()) {
            return gv1.e;
        }
        gv1 gv1 = this.p;
        if (gv1 != null) {
            return gv1;
        }
        return new gv1(13, (PendingIntent) null);
    }

    @DexIgnore
    public final boolean g() {
        return this.o instanceof ox1;
    }

    @DexIgnore
    public final void h() {
        this.a.lock();
        try {
            this.o = new ox1(this, this.h, this.i, this.d, this.j, this.a, this.c);
            this.o.c();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void i() {
        this.a.lock();
        try {
            this.r.o();
            this.o = new jx1(this);
            this.o.c();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void g(int i2) {
        this.a.lock();
        try {
            this.o.g(i2);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void a() {
        if (this.o.a()) {
            this.g.clear();
        }
    }

    @DexIgnore
    public final void b() {
        this.o.b();
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        this.a.lock();
        try {
            this.p = gv1;
            this.o = new xx1(this);
            this.o.c();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void f(Bundle bundle) {
        this.a.lock();
        try {
            this.o.f(bundle);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void a(gv1 gv1, rv1<?> rv1, boolean z) {
        this.a.lock();
        try {
            this.o.a(gv1, rv1, z);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void a(iy1 iy1) {
        this.e.sendMessage(this.e.obtainMessage(1, iy1));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.e.sendMessage(this.e.obtainMessage(2, runtimeException));
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append(str).append("mState=").println(this.o);
        for (rv1 next : this.i.keySet()) {
            printWriter.append(str).append(next.b()).println(":");
            this.f.get(next.a()).a(concat, fileDescriptor, printWriter, strArr);
        }
    }
}
