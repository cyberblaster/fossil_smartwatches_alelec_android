package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu4 extends RecyclerView.g<b> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<Complication> c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public Complication e;
        @DexIgnore
        public /* final */ /* synthetic */ iu4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.a((Complication) obj);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.iu4$b$b")
        /* renamed from: com.fossil.iu4$b$b  reason: collision with other inner class name */
        public static final class C0015b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0015b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.b((Complication) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public void a(CustomizeWidget customizeWidget) {
                wg6.b(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(iu4 iu4, View view) {
            super(view);
            wg6.b(view, "view");
            this.f = iu4;
            View findViewById = view.findViewById(2131363333);
            wg6.a((Object) findViewById, "view.findViewById(R.id.wc_complication)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363108);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_complication_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362603);
            this.d = view.findViewById(2131362638);
            this.a.setOnClickListener(new a(this));
            this.d.setOnClickListener(new C0015b(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(Complication complication) {
            String str;
            wg6.b(complication, "complication");
            this.e = complication;
            if (this.e != null) {
                this.a.b(complication.getComplicationId());
                this.b.setText(jm4.a(PortfolioApp.get.instance(), complication.getNameKey(), complication.getName()));
            }
            this.a.setSelectedWc(wg6.a((Object) complication.getComplicationId(), (Object) this.f.b));
            if (wg6.a((Object) complication.getComplicationId(), (Object) this.f.b)) {
                View view = this.c;
                wg6.a((Object) view, "ivIndicator");
                view.setBackground(w6.c(PortfolioApp.get.instance(), 2131230988));
            } else {
                View view2 = this.c;
                wg6.a((Object) view2, "ivIndicator");
                view2.setBackground(w6.c(PortfolioApp.get.instance(), 2131230989));
            }
            View view3 = this.d;
            wg6.a((Object) view3, "ivWarning");
            view3.setVisibility(!yj4.b.d(complication.getComplicationId()) ? 0 : 8);
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            Complication complication2 = this.e;
            if (complication2 == null || (str = complication2.getComplicationId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            wg6.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "COMPLICATION", putExtra, (View.OnDragListener) null, new c(this), 4, (Object) null);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Complication complication);

        @DexIgnore
        void b(Complication complication);
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ iu4(ArrayList arrayList, c cVar, int i, qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("ComplicationsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final c d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        T t;
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((Complication) t).getComplicationId(), (Object) "empty")) {
                break;
            }
        }
        Complication complication = (Complication) t;
        if (complication != null) {
            Collections.swap(this.c, this.c.indexOf(complication), 0);
        }
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "complicationId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558647, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026plication, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public iu4(ArrayList<Complication> arrayList, c cVar) {
        wg6.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<Complication> list) {
        wg6.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        e();
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        wg6.b(str, "complicationId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((Complication) t).getComplicationId(), (Object) str)) {
                break;
            }
        }
        Complication complication = (Complication) t;
        if (complication != null) {
            return this.c.indexOf(complication);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            Complication complication = this.c.get(i);
            wg6.a((Object) complication, "mData[position]");
            bVar.a(complication);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wg6.b(cVar, "listener");
        this.d = cVar;
    }
}
