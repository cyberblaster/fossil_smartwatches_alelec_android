package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.j5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h5 {
    @DexIgnore
    public j5 a;
    @DexIgnore
    public j5 b;
    @DexIgnore
    public j5 c;
    @DexIgnore
    public j5 d;
    @DexIgnore
    public j5 e;
    @DexIgnore
    public j5 f;
    @DexIgnore
    public j5 g;
    @DexIgnore
    public ArrayList<j5> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;

    @DexIgnore
    public h5(j5 j5Var, int i2, boolean z) {
        this.a = j5Var;
        this.l = i2;
        this.m = z;
    }

    @DexIgnore
    public static boolean a(j5 j5Var, int i2) {
        if (j5Var.s() != 8 && j5Var.C[i2] == j5.b.MATCH_CONSTRAINT) {
            int[] iArr = j5Var.g;
            if (iArr[i2] == 0 || iArr[i2] == 3) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void b() {
        int i2 = this.l * 2;
        boolean z = false;
        j5 j5Var = this.a;
        j5 j5Var2 = j5Var;
        boolean z2 = false;
        while (!z2) {
            this.i++;
            j5[] j5VarArr = j5Var.i0;
            int i3 = this.l;
            j5 j5Var3 = null;
            j5VarArr[i3] = null;
            j5Var.h0[i3] = null;
            if (j5Var.s() != 8) {
                if (this.b == null) {
                    this.b = j5Var;
                }
                this.d = j5Var;
                j5.b[] bVarArr = j5Var.C;
                int i4 = this.l;
                if (bVarArr[i4] == j5.b.MATCH_CONSTRAINT) {
                    int[] iArr = j5Var.g;
                    if (iArr[i4] == 0 || iArr[i4] == 3 || iArr[i4] == 2) {
                        this.j++;
                        float[] fArr = j5Var.g0;
                        int i5 = this.l;
                        float f2 = fArr[i5];
                        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            this.k += fArr[i5];
                        }
                        if (a(j5Var, this.l)) {
                            if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                this.n = true;
                            } else {
                                this.o = true;
                            }
                            if (this.h == null) {
                                this.h = new ArrayList<>();
                            }
                            this.h.add(j5Var);
                        }
                        if (this.f == null) {
                            this.f = j5Var;
                        }
                        j5 j5Var4 = this.g;
                        if (j5Var4 != null) {
                            j5Var4.h0[this.l] = j5Var;
                        }
                        this.g = j5Var;
                    }
                }
            }
            if (j5Var2 != j5Var) {
                j5Var2.i0[this.l] = j5Var;
            }
            i5 i5Var = j5Var.A[i2 + 1].d;
            if (i5Var != null) {
                j5 j5Var5 = i5Var.b;
                i5[] i5VarArr = j5Var5.A;
                if (i5VarArr[i2].d != null && i5VarArr[i2].d.b == j5Var) {
                    j5Var3 = j5Var5;
                }
            }
            if (j5Var3 == null) {
                j5Var3 = j5Var;
                z2 = true;
            }
            j5Var2 = j5Var;
            j5Var = j5Var3;
        }
        this.c = j5Var;
        if (this.l != 0 || !this.m) {
            this.e = this.a;
        } else {
            this.e = this.c;
        }
        if (this.o && this.n) {
            z = true;
        }
        this.p = z;
    }

    @DexIgnore
    public void a() {
        if (!this.q) {
            b();
        }
        this.q = true;
    }
}
