package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xo6 {
    @DexIgnore
    public static final boolean a(String str, boolean z) {
        wg6.b(str, "propertyName");
        String a = vo6.a(str);
        return a != null ? Boolean.parseBoolean(a) : z;
    }

    @DexIgnore
    public static /* synthetic */ int a(String str, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 4) != 0) {
            i2 = 1;
        }
        if ((i4 & 8) != 0) {
            i3 = Integer.MAX_VALUE;
        }
        return vo6.a(str, i, i2, i3);
    }

    @DexIgnore
    public static final int a(String str, int i, int i2, int i3) {
        wg6.b(str, "propertyName");
        return (int) vo6.a(str, (long) i, (long) i2, (long) i3);
    }

    @DexIgnore
    public static /* synthetic */ long a(String str, long j, long j2, long j3, int i, Object obj) {
        if ((i & 4) != 0) {
            j2 = 1;
        }
        long j4 = j2;
        if ((i & 8) != 0) {
            j3 = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return vo6.a(str, j, j4, j3);
    }

    @DexIgnore
    public static final long a(String str, long j, long j2, long j3) {
        wg6.b(str, "propertyName");
        String a = vo6.a(str);
        if (a == null) {
            return j;
        }
        Long c = wj6.c(a);
        if (c != null) {
            long longValue = c.longValue();
            if (j2 <= longValue && j3 >= longValue) {
                return longValue;
            }
            throw new IllegalStateException(("System property '" + str + "' should be in range " + j2 + ".." + j3 + ", but is '" + longValue + '\'').toString());
        }
        throw new IllegalStateException(("System property '" + str + "' has unrecognized value '" + a + '\'').toString());
    }
}
