package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw implements ImageHeaderParser {
    @DexIgnore
    public static /* final */ byte[] a; // = "Exif\u0000\u0000".getBytes(Charset.forName("UTF-8"));
    @DexIgnore
    public static /* final */ int[] b; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {
        @DexIgnore
        public /* final */ ByteBuffer a;

        @DexIgnore
        public a(ByteBuffer byteBuffer) {
            this.a = byteBuffer;
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        public int a() throws c.a {
            return (b() << 8) | b();
        }

        @DexIgnore
        public short b() throws c.a {
            if (this.a.remaining() >= 1) {
                return (short) (this.a.get() & 255);
            }
            throw new c.a();
        }

        @DexIgnore
        public long skip(long j) {
            int min = (int) Math.min((long) this.a.remaining(), j);
            ByteBuffer byteBuffer = this.a;
            byteBuffer.position(byteBuffer.position() + min);
            return (long) min;
        }

        @DexIgnore
        public int a(byte[] bArr, int i) {
            int min = Math.min(i, this.a.remaining());
            if (min == 0) {
                return -1;
            }
            this.a.get(bArr, 0, min);
            return min;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ ByteBuffer a;

        @DexIgnore
        public b(byte[] bArr, int i) {
            this.a = (ByteBuffer) ByteBuffer.wrap(bArr).order(ByteOrder.BIG_ENDIAN).limit(i);
        }

        @DexIgnore
        public void a(ByteOrder byteOrder) {
            this.a.order(byteOrder);
        }

        @DexIgnore
        public int b(int i) {
            if (a(i, 4)) {
                return this.a.getInt(i);
            }
            return -1;
        }

        @DexIgnore
        public int a() {
            return this.a.remaining();
        }

        @DexIgnore
        public short a(int i) {
            if (a(i, 2)) {
                return this.a.getShort(i);
            }
            return -1;
        }

        @DexIgnore
        public final boolean a(int i, int i2) {
            return this.a.remaining() - i >= i2;
        }
    }

    @DexIgnore
    public interface c {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends IOException {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 1;

            @DexIgnore
            public a() {
                super("Unexpectedly reached end of a file");
            }
        }

        @DexIgnore
        int a() throws IOException;

        @DexIgnore
        int a(byte[] bArr, int i) throws IOException;

        @DexIgnore
        short b() throws IOException;

        @DexIgnore
        long skip(long j) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements c {
        @DexIgnore
        public /* final */ InputStream a;

        @DexIgnore
        public d(InputStream inputStream) {
            this.a = inputStream;
        }

        @DexIgnore
        public int a() throws IOException {
            return (b() << 8) | b();
        }

        @DexIgnore
        public short b() throws IOException {
            int read = this.a.read();
            if (read != -1) {
                return (short) read;
            }
            throw new c.a();
        }

        @DexIgnore
        public long skip(long j) throws IOException {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.a.skip(j2);
                if (skip <= 0) {
                    if (this.a.read() == -1) {
                        break;
                    }
                    skip = 1;
                }
                j2 -= skip;
            }
            return j - j2;
        }

        @DexIgnore
        public int a(byte[] bArr, int i) throws IOException {
            int i2 = 0;
            int i3 = 0;
            while (i2 < i && (i3 = this.a.read(bArr, i2, i - i2)) != -1) {
                i2 += i3;
            }
            if (i2 != 0 || i3 != -1) {
                return i2;
            }
            throw new c.a();
        }
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    @DexIgnore
    public static boolean a(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    @DexIgnore
    public ImageHeaderParser.ImageType a(InputStream inputStream) throws IOException {
        q00.a(inputStream);
        return a((c) new d(inputStream));
    }

    @DexIgnore
    public final int b(c cVar) throws IOException {
        short b2;
        int a2;
        long j;
        long skip;
        do {
            short b3 = cVar.b();
            if (b3 != 255) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Unknown segmentId=" + b3);
                }
                return -1;
            }
            b2 = cVar.b();
            if (b2 == 218) {
                return -1;
            }
            if (b2 == 217) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
                return -1;
            }
            a2 = cVar.a() - 2;
            if (b2 == 225) {
                return a2;
            }
            j = (long) a2;
            skip = cVar.skip(j);
        } while (skip == j);
        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + b2 + ", wanted to skip: " + a2 + ", but actually skipped: " + skip);
        }
        return -1;
    }

    @DexIgnore
    public ImageHeaderParser.ImageType a(ByteBuffer byteBuffer) throws IOException {
        q00.a(byteBuffer);
        return a((c) new a(byteBuffer));
    }

    @DexIgnore
    public int a(InputStream inputStream, xt xtVar) throws IOException {
        q00.a(inputStream);
        d dVar = new d(inputStream);
        q00.a(xtVar);
        return a((c) dVar, xtVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
        return com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0039 */
    public final ImageHeaderParser.ImageType a(c cVar) throws IOException {
        try {
            int a2 = cVar.a();
            if (a2 == 65496) {
                return ImageHeaderParser.ImageType.JPEG;
            }
            short b2 = (a2 << 8) | cVar.b();
            if (b2 == 4671814) {
                return ImageHeaderParser.ImageType.GIF;
            }
            short b3 = (b2 << 8) | cVar.b();
            if (b3 == -1991225785) {
                cVar.skip(21);
                return cVar.b() >= 3 ? ImageHeaderParser.ImageType.PNG_A : ImageHeaderParser.ImageType.PNG;
            } else if (b3 != 1380533830) {
                return ImageHeaderParser.ImageType.UNKNOWN;
            } else {
                cVar.skip(4);
                if (((cVar.a() << 16) | cVar.a()) != 1464156752) {
                    return ImageHeaderParser.ImageType.UNKNOWN;
                }
                int a3 = (cVar.a() << 16) | cVar.a();
                if ((a3 & -256) != 1448097792) {
                    return ImageHeaderParser.ImageType.UNKNOWN;
                }
                int i = a3 & 255;
                if (i == 88) {
                    cVar.skip(4);
                    return (cVar.b() & 16) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
                } else if (i != 76) {
                    return ImageHeaderParser.ImageType.WEBP;
                } else {
                    cVar.skip(4);
                    return (cVar.b() & 8) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
                }
            }
        } catch (c.a unused) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
    }

    @DexIgnore
    public final int a(c cVar, xt xtVar) throws IOException {
        byte[] bArr;
        try {
            int a2 = cVar.a();
            if (!a(a2)) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + a2);
                }
                return -1;
            }
            int b2 = b(cVar);
            if (b2 == -1) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
                }
                return -1;
            }
            bArr = (byte[]) xtVar.b(b2, byte[].class);
            int a3 = a(cVar, bArr, b2);
            xtVar.put(bArr);
            return a3;
        } catch (c.a unused) {
            return -1;
        } catch (Throwable th) {
            xtVar.put(bArr);
            throw th;
        }
    }

    @DexIgnore
    public final int a(c cVar, byte[] bArr, int i) throws IOException {
        int a2 = cVar.a(bArr, i);
        if (a2 != i) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + a2);
            }
            return -1;
        } else if (a(bArr, i)) {
            return a(new b(bArr, i));
        } else {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            }
            return -1;
        }
    }

    @DexIgnore
    public final boolean a(byte[] bArr, int i) {
        boolean z = bArr != null && i > a.length;
        if (!z) {
            return z;
        }
        int i2 = 0;
        while (true) {
            byte[] bArr2 = a;
            if (i2 >= bArr2.length) {
                return z;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public static int a(b bVar) {
        ByteOrder byteOrder;
        short a2 = bVar.a(6);
        if (a2 == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else if (a2 != 19789) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unknown endianness = " + a2);
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else {
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        bVar.a(byteOrder);
        int b2 = bVar.b(10) + 6;
        short a3 = bVar.a(b2);
        for (int i = 0; i < a3; i++) {
            int a4 = a(b2, i);
            short a5 = bVar.a(a4);
            if (a5 == 274) {
                short a6 = bVar.a(a4 + 2);
                if (a6 >= 1 && a6 <= 12) {
                    int b3 = bVar.b(a4 + 4);
                    if (b3 >= 0) {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got tagIndex=" + i + " tagType=" + a5 + " formatCode=" + a6 + " componentCount=" + b3);
                        }
                        int i2 = b3 + b[a6];
                        if (i2 <= 4) {
                            int i3 = a4 + 8;
                            if (i3 < 0 || i3 > bVar.a()) {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + a5);
                                }
                            } else if (i2 >= 0 && i2 + i3 <= bVar.a()) {
                                return bVar.a(i3);
                            } else {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + a5);
                                }
                            }
                        } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + a6);
                        }
                    } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Got invalid format code = " + a6);
                }
            }
        }
        return -1;
    }
}
