package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(t1 t1Var, int i);

        @DexIgnore
        boolean a();

        @DexIgnore
        t1 getItemData();
    }

    @DexIgnore
    void a(q1 q1Var);
}
