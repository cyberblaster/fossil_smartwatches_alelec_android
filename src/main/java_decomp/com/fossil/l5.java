package com.fossil;

import com.fossil.i5;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l5 {
    @DexIgnore
    public List<j5> a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public /* final */ int[] e; // = {this.b, this.c};
    @DexIgnore
    public List<j5> f; // = new ArrayList();
    @DexIgnore
    public List<j5> g; // = new ArrayList();
    @DexIgnore
    public HashSet<j5> h; // = new HashSet<>();
    @DexIgnore
    public HashSet<j5> i; // = new HashSet<>();
    @DexIgnore
    public List<j5> j; // = new ArrayList();
    @DexIgnore
    public List<j5> k; // = new ArrayList();

    @DexIgnore
    public l5(List<j5> list) {
        this.a = list;
    }

    @DexIgnore
    public List<j5> a(int i2) {
        if (i2 == 0) {
            return this.f;
        }
        if (i2 == 1) {
            return this.g;
        }
        return null;
    }

    @DexIgnore
    public Set<j5> b(int i2) {
        if (i2 == 0) {
            return this.h;
        }
        if (i2 == 1) {
            return this.i;
        }
        return null;
    }

    @DexIgnore
    public void a(j5 j5Var, int i2) {
        if (i2 == 0) {
            this.h.add(j5Var);
        } else if (i2 == 1) {
            this.i.add(j5Var);
        }
    }

    @DexIgnore
    public void b() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            a(this.k.get(i2));
        }
    }

    @DexIgnore
    public List<j5> a() {
        if (!this.j.isEmpty()) {
            return this.j;
        }
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            j5 j5Var = this.a.get(i2);
            if (!j5Var.b0) {
                a((ArrayList<j5>) (ArrayList) this.j, j5Var);
            }
        }
        this.k.clear();
        this.k.addAll(this.a);
        this.k.removeAll(this.j);
        return this.j;
    }

    @DexIgnore
    public l5(List<j5> list, boolean z) {
        this.a = list;
        this.d = z;
    }

    @DexIgnore
    public final void a(ArrayList<j5> arrayList, j5 j5Var) {
        if (!j5Var.d0) {
            arrayList.add(j5Var);
            j5Var.d0 = true;
            if (!j5Var.z()) {
                if (j5Var instanceof n5) {
                    n5 n5Var = (n5) j5Var;
                    int i2 = n5Var.l0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        a(arrayList, n5Var.k0[i3]);
                    }
                }
                for (i5 i5Var : j5Var.A) {
                    i5 i5Var2 = i5Var.d;
                    if (i5Var2 != null) {
                        j5 j5Var2 = i5Var2.b;
                        if (!(i5Var2 == null || j5Var2 == j5Var.l())) {
                            a(arrayList, j5Var2);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0087  */
    public final void a(j5 j5Var) {
        i5 i5Var;
        int i2;
        int i3;
        i5 i5Var2;
        i5 i5Var3;
        int i4;
        if (j5Var.b0 && !j5Var.z()) {
            boolean z = false;
            boolean z2 = j5Var.u.d != null;
            if (z2) {
                i5Var = j5Var.u.d;
            } else {
                i5Var = j5Var.s.d;
            }
            if (i5Var != null) {
                j5 j5Var2 = i5Var.b;
                if (!j5Var2.c0) {
                    a(j5Var2);
                }
                i5.d dVar = i5Var.c;
                if (dVar == i5.d.RIGHT) {
                    j5 j5Var3 = i5Var.b;
                    i2 = j5Var3.t() + j5Var3.I;
                } else if (dVar == i5.d.LEFT) {
                    i2 = i5Var.b.I;
                }
                if (!z2) {
                    i3 = i2 - j5Var.u.b();
                } else {
                    i3 = i2 + j5Var.s.b() + j5Var.t();
                }
                j5Var.a(i3 - j5Var.t(), i3);
                i5Var2 = j5Var.w.d;
                if (i5Var2 == null) {
                    j5 j5Var4 = i5Var2.b;
                    if (!j5Var4.c0) {
                        a(j5Var4);
                    }
                    j5 j5Var5 = i5Var2.b;
                    int i5 = (j5Var5.J + j5Var5.Q) - j5Var.Q;
                    j5Var.e(i5, j5Var.F + i5);
                    j5Var.c0 = true;
                    return;
                }
                if (j5Var.v.d != null) {
                    z = true;
                }
                if (z) {
                    i5Var3 = j5Var.v.d;
                } else {
                    i5Var3 = j5Var.t.d;
                }
                if (i5Var3 != null) {
                    j5 j5Var6 = i5Var3.b;
                    if (!j5Var6.c0) {
                        a(j5Var6);
                    }
                    i5.d dVar2 = i5Var3.c;
                    if (dVar2 == i5.d.BOTTOM) {
                        j5 j5Var7 = i5Var3.b;
                        i3 = j5Var7.J + j5Var7.j();
                    } else if (dVar2 == i5.d.TOP) {
                        i3 = i5Var3.b.J;
                    }
                }
                if (z) {
                    i4 = i3 - j5Var.v.b();
                } else {
                    i4 = i3 + j5Var.t.b() + j5Var.j();
                }
                j5Var.e(i4 - j5Var.j(), i4);
                j5Var.c0 = true;
                return;
            }
            i2 = 0;
            if (!z2) {
            }
            j5Var.a(i3 - j5Var.t(), i3);
            i5Var2 = j5Var.w.d;
            if (i5Var2 == null) {
            }
        }
    }
}
