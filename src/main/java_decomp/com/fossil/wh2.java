package com.fossil;

import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wh2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wh2(ov2 ov2, boolean z) {
        super(ov2);
        this.f = ov2;
        this.e = z;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.f.g.setMeasurementEnabled(this.e, this.a);
    }
}
