package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my3 {
    @DexIgnore
    public /* final */ ky3 a;
    @DexIgnore
    public /* final */ List<ly3> b; // = new ArrayList();

    @DexIgnore
    public my3(ky3 ky3) {
        this.a = ky3;
        this.b.add(new ly3(ky3, new int[]{1}));
    }

    @DexIgnore
    public final ly3 a(int i) {
        if (i >= this.b.size()) {
            List<ly3> list = this.b;
            ly3 ly3 = list.get(list.size() - 1);
            for (int size = this.b.size(); size <= i; size++) {
                ky3 ky3 = this.a;
                ly3 = ly3.c(new ly3(ky3, new int[]{1, ky3.a((size - 1) + ky3.a())}));
                this.b.add(ly3);
            }
        }
        return this.b.get(i);
    }

    @DexIgnore
    public void a(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                ly3 a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] a3 = new ly3(this.a, iArr2).a(i, 1).b(a2)[1].a();
                int length2 = i - a3.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(a3, 0, iArr, length + length2, a3.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
