package com.fossil;

import android.view.View;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j45 extends j24 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract CopyOnWriteArrayList<CustomizeRealData> i();
}
