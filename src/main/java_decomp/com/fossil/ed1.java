package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ed1<T> extends ed1<T> {
    @DexIgnore
    public /* final */ w40 a;
    @DexIgnore
    public /* final */ q11 b;
    @DexIgnore
    public /* final */ w31 c;

    @DexIgnore
    public ed1(w31 w31, w40 w40) {
        this.a = w40;
        this.c = w31;
        this.b = q11.CRC32;
    }

    @DexIgnore
    public q11 a() {
        return this.b;
    }

    @DexIgnore
    public abstract T a(byte[] bArr) throws sw0;

    @DexIgnore
    public boolean b(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        if (this.c != w31.x.a(order.getShort(0)) || new w40(bArr[2], bArr[3]).getMajor() != this.a.getMajor() || cw0.b(order.getInt(8)) != cw0.b((bArr.length - 12) - 4)) {
            return false;
        }
        if (cw0.b(order.getInt(bArr.length - 4)) != h51.a.a(md6.a(bArr, 12, bArr.length - 4), a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public ed1(w31 w31) {
        this.a = new w40((byte) 1, (byte) 0);
        this.c = w31;
        this.b = q11.CRC32;
    }
}
