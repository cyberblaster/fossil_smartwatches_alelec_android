package com.fossil;

import android.content.Context;
import android.net.Uri;
import com.fossil.jv;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xv implements jv<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements kv<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public jv<Uri, InputStream> a(nv nvVar) {
            return new xv(this.a);
        }
    }

    @DexIgnore
    public xv(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public jv.a<InputStream> a(Uri uri, int i, int i2, xr xrVar) {
        if (!ss.a(i, i2) || !a(xrVar)) {
            return null;
        }
        return new jv.a<>(new g00(uri), ts.b(this.a, uri));
    }

    @DexIgnore
    public final boolean a(xr xrVar) {
        Long l = (Long) xrVar.a(ex.d);
        return l != null && l.longValue() == -1;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return ss.c(uri);
    }
}
