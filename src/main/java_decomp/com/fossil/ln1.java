package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class ln1 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[sk1.values().length];

    /*
    static {
        a[sk1.INTERRUPTED.ordinal()] = 1;
        a[sk1.CONNECTION_DROPPED.ordinal()] = 2;
        a[sk1.INCOMPATIBLE_FIRMWARE.ordinal()] = 3;
        a[sk1.REQUEST_ERROR.ordinal()] = 4;
        a[sk1.LACK_OF_SERVICE.ordinal()] = 5;
        a[sk1.LACK_OF_CHARACTERISTIC.ordinal()] = 6;
        a[sk1.INVALID_SERIAL_NUMBER.ordinal()] = 7;
        a[sk1.DATA_TRANSFER_RETRY_REACH_THRESHOLD.ordinal()] = 8;
        a[sk1.EXCHANGED_VALUE_NOT_SATISFIED.ordinal()] = 9;
        a[sk1.INVALID_FILE_LENGTH.ordinal()] = 10;
        a[sk1.MISMATCH_VERSION.ordinal()] = 11;
        a[sk1.INVALID_FILE_CRC.ordinal()] = 12;
        a[sk1.INVALID_RESPONSE.ordinal()] = 13;
        a[sk1.INVALID_DATA_LENGTH.ordinal()] = 14;
        a[sk1.NOT_ENOUGH_FILE_TO_PROCESS.ordinal()] = 15;
        a[sk1.WAITING_FOR_EXECUTION_TIMEOUT.ordinal()] = 16;
        a[sk1.REQUEST_UNSUPPORTED.ordinal()] = 17;
        a[sk1.UNSUPPORTED_FILE_HANDLE.ordinal()] = 18;
        a[sk1.BLUETOOTH_OFF.ordinal()] = 19;
        a[sk1.AUTHENTICATION_FAILED.ordinal()] = 20;
        a[sk1.WRONG_RANDOM_NUMBER.ordinal()] = 21;
        a[sk1.SECRET_KEY_IS_REQUIRED.ordinal()] = 22;
        a[sk1.INVALID_PARAMETER.ordinal()] = 23;
        a[sk1.HID_INPUT_DEVICE_DISABLED.ordinal()] = 24;
        a[sk1.NOT_ALLOW_TO_START.ordinal()] = 25;
        a[sk1.UNSUPPORTED_FORMAT.ordinal()] = 26;
        a[sk1.TARGET_FIRMWARE_NOT_MATCHED.ordinal()] = 27;
        a[sk1.DATABASE_ERROR.ordinal()] = 28;
        a[sk1.FLOW_BROKEN.ordinal()] = 29;
        a[sk1.SUCCESS.ordinal()] = 30;
        a[sk1.NOT_START.ordinal()] = 31;
    }
    */
}
