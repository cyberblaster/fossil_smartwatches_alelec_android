package com.fossil;

import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b03 {
    @DexIgnore
    public static volatile Handler d;
    @DexIgnore
    public /* final */ v63 a;
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public volatile long c;

    @DexIgnore
    public b03(v63 v63) {
        w12.a(v63);
        this.a = v63;
        this.b = new e03(this, v63);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void a(long j) {
        c();
        if (j >= 0) {
            this.c = this.a.zzm().b();
            if (!d().postDelayed(this.b, j)) {
                this.a.b().t().a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.c != 0;
    }

    @DexIgnore
    public final void c() {
        this.c = 0;
        d().removeCallbacks(this.b);
    }

    @DexIgnore
    public final Handler d() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (b03.class) {
            if (d == null) {
                d = new br2(this.a.c().getMainLooper());
            }
            handler = d;
        }
        return handler;
    }
}
