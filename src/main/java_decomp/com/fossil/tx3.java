package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tx3 extends wx3 {
    @DexIgnore
    public static /* final */ tx3 a;

    /*
    static {
        tx3 tx3 = new tx3();
        a = tx3;
        tx3.setStackTrace(wx3.NO_TRACE);
    }
    */

    @DexIgnore
    public tx3() {
    }

    @DexIgnore
    public static tx3 getFormatInstance() {
        return wx3.isStackTrace ? new tx3() : a;
    }

    @DexIgnore
    public tx3(Throwable th) {
        super(th);
    }

    @DexIgnore
    public static tx3 getFormatInstance(Throwable th) {
        return wx3.isStackTrace ? new tx3(th) : a;
    }
}
