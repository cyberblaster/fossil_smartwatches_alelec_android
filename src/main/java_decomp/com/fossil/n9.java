package com.fossil;

import android.view.View;
import android.view.ViewParent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n9 {
    @DexIgnore
    public ViewParent a;
    @DexIgnore
    public ViewParent b;
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int[] e;

    @DexIgnore
    public n9(View view) {
        this.c = view;
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.d) {
            x9.K(this.c);
        }
        this.d = z;
    }

    @DexIgnore
    public boolean b() {
        return b(0);
    }

    @DexIgnore
    public boolean c() {
        return this.d;
    }

    @DexIgnore
    public void d() {
        d(0);
    }

    @DexIgnore
    public boolean b(int i) {
        return a(i) != null;
    }

    @DexIgnore
    public boolean c(int i) {
        return a(i, 0);
    }

    @DexIgnore
    public void d(int i) {
        ViewParent a2 = a(i);
        if (a2 != null) {
            aa.a(a2, this.c, i);
            a(i, (ViewParent) null);
        }
    }

    @DexIgnore
    public final boolean b(int i, int i2, int i3, int i4, int[] iArr, int i5, int[] iArr2) {
        ViewParent a2;
        int i6;
        int i7;
        int[] iArr3;
        int[] iArr4 = iArr;
        if (!c() || (a2 = a(i5)) == null) {
            return false;
        }
        if (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) {
            if (iArr4 != null) {
                iArr4[0] = 0;
                iArr4[1] = 0;
            }
            return false;
        }
        if (iArr4 != null) {
            this.c.getLocationInWindow(iArr4);
            i7 = iArr4[0];
            i6 = iArr4[1];
        } else {
            i7 = 0;
            i6 = 0;
        }
        if (iArr2 == null) {
            int[] a3 = a();
            a3[0] = 0;
            a3[1] = 0;
            iArr3 = a3;
        } else {
            iArr3 = iArr2;
        }
        aa.a(a2, this.c, i, i2, i3, i4, i5, iArr3);
        if (iArr4 != null) {
            this.c.getLocationInWindow(iArr4);
            iArr4[0] = iArr4[0] - i7;
            iArr4[1] = iArr4[1] - i6;
        }
        return true;
    }

    @DexIgnore
    public boolean a(int i, int i2) {
        if (b(i2)) {
            return true;
        }
        if (!c()) {
            return false;
        }
        View view = this.c;
        for (ViewParent parent = this.c.getParent(); parent != null; parent = parent.getParent()) {
            if (aa.b(parent, view, this.c, i, i2)) {
                a(i2, parent);
                aa.a(parent, view, this.c, i, i2);
                return true;
            }
            if (parent instanceof View) {
                view = (View) parent;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(int i, int i2, int i3, int i4, int[] iArr) {
        return b(i, i2, i3, i4, iArr, 0, (int[]) null);
    }

    @DexIgnore
    public boolean a(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        return b(i, i2, i3, i4, iArr, i5, (int[]) null);
    }

    @DexIgnore
    public void a(int i, int i2, int i3, int i4, int[] iArr, int i5, int[] iArr2) {
        b(i, i2, i3, i4, iArr, i5, iArr2);
    }

    @DexIgnore
    public boolean a(int i, int i2, int[] iArr, int[] iArr2) {
        return a(i, i2, iArr, iArr2, 0);
    }

    @DexIgnore
    public boolean a(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        ViewParent a2;
        int i4;
        int i5;
        if (!c() || (a2 = a(i3)) == null) {
            return false;
        }
        if (i == 0 && i2 == 0) {
            if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
            return false;
        }
        if (iArr2 != null) {
            this.c.getLocationInWindow(iArr2);
            i5 = iArr2[0];
            i4 = iArr2[1];
        } else {
            i5 = 0;
            i4 = 0;
        }
        if (iArr == null) {
            iArr = a();
        }
        iArr[0] = 0;
        iArr[1] = 0;
        aa.a(a2, this.c, i, i2, iArr, i3);
        if (iArr2 != null) {
            this.c.getLocationInWindow(iArr2);
            iArr2[0] = iArr2[0] - i5;
            iArr2[1] = iArr2[1] - i4;
        }
        if (iArr[0] == 0 && iArr[1] == 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean a(float f, float f2, boolean z) {
        ViewParent a2;
        if (!c() || (a2 = a(0)) == null) {
            return false;
        }
        return aa.a(a2, this.c, f, f2, z);
    }

    @DexIgnore
    public boolean a(float f, float f2) {
        ViewParent a2;
        if (!c() || (a2 = a(0)) == null) {
            return false;
        }
        return aa.a(a2, this.c, f, f2);
    }

    @DexIgnore
    public final ViewParent a(int i) {
        if (i == 0) {
            return this.a;
        }
        if (i != 1) {
            return null;
        }
        return this.b;
    }

    @DexIgnore
    public final void a(int i, ViewParent viewParent) {
        if (i == 0) {
            this.a = viewParent;
        } else if (i == 1) {
            this.b = viewParent;
        }
    }

    @DexIgnore
    public final int[] a() {
        if (this.e == null) {
            this.e = new int[2];
        }
        return this.e;
    }
}
