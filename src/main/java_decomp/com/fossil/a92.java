package com.fossil;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.gcm.PendingCallback;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a92 extends Service {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public int b;
    @DexIgnore
    public ExecutorService c;
    @DexIgnore
    public Messenger d;
    @DexIgnore
    public ComponentName e;
    @DexIgnore
    public y82 f;
    @DexIgnore
    public jf2 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(21)
    public class a extends if2 {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            Messenger messenger;
            if (!v42.a(a92.this, message.sendingUid, "com.google.android.gms")) {
                Log.e("GcmTaskService", "unable to verify presence of Google Play Services");
                return;
            }
            int i = message.what;
            if (i == 1) {
                Bundle data = message.getData();
                if (!data.isEmpty() && (messenger = message.replyTo) != null) {
                    String string = data.getString("tag");
                    ArrayList parcelableArrayList = data.getParcelableArrayList("triggered_uris");
                    long j = data.getLong("max_exec_duration", 180);
                    if (!a92.this.a(string)) {
                        a92.this.a(new b(string, messenger, data.getBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY), j, (List<Uri>) parcelableArrayList));
                    }
                }
            } else if (i != 2) {
                if (i != 4) {
                    String valueOf = String.valueOf(message);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 31);
                    sb.append("Unrecognized message received: ");
                    sb.append(valueOf);
                    Log.e("GcmTaskService", sb.toString());
                    return;
                }
                a92.this.a();
            } else if (Log.isLoggable("GcmTaskService", 3)) {
                String valueOf2 = String.valueOf(message);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("ignoring unimplemented stop message for now: ");
                sb2.append(valueOf2);
                Log.d("GcmTaskService", sb2.toString());
            }
        }
    }

    @DexIgnore
    public abstract int a(c92 c92);

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final boolean a(String str) {
        boolean z;
        synchronized (this.a) {
            z = !this.f.a(str, this.e.getClassName());
            if (z) {
                String packageName = getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 44 + String.valueOf(str).length());
                sb.append(packageName);
                sb.append(" ");
                sb.append(str);
                sb.append(": Task already running, won't start another");
                Log.w("GcmTaskService", sb.toString());
            }
        }
        return z;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (intent == null || !s42.g() || !"com.google.android.gms.gcm.ACTION_TASK_READY".equals(intent.getAction())) {
            return null;
        }
        return this.d.getBinder();
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.f = y82.a((Context) this);
        this.c = ff2.a().a(10, new e92(this), 10);
        this.d = new Messenger(new a(Looper.getMainLooper()));
        this.e = new ComponentName(this, a92.class);
        kf2.a();
        Class<a92> cls = a92.class;
        this.g = kf2.a;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        List<Runnable> shutdownNow = this.c.shutdownNow();
        if (!shutdownNow.isEmpty()) {
            int size = shutdownNow.size();
            StringBuilder sb = new StringBuilder(79);
            sb.append("Shutting down, but not all tasks are finished executing. Remaining: ");
            sb.append(size);
            Log.e("GcmTaskService", sb.toString());
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            a(i2);
            return 2;
        }
        try {
            intent.setExtrasClassLoader(PendingCallback.class.getClassLoader());
            String action = intent.getAction();
            if ("com.google.android.gms.gcm.ACTION_TASK_READY".equals(action)) {
                String stringExtra = intent.getStringExtra("tag");
                Parcelable parcelableExtra = intent.getParcelableExtra("callback");
                Bundle bundleExtra = intent.getBundleExtra(AppLinkData.ARGUMENTS_EXTRAS_KEY);
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("triggered_uris");
                long longExtra = intent.getLongExtra("max_exec_duration", 180);
                if (!(parcelableExtra instanceof PendingCallback)) {
                    String packageName = getPackageName();
                    StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 47 + String.valueOf(stringExtra).length());
                    sb.append(packageName);
                    sb.append(" ");
                    sb.append(stringExtra);
                    sb.append(": Could not process request, invalid callback.");
                    Log.e("GcmTaskService", sb.toString());
                    return 2;
                } else if (a(stringExtra)) {
                    a(i2);
                    return 2;
                } else {
                    a(new b(stringExtra, ((PendingCallback) parcelableExtra).a, bundleExtra, longExtra, (List<Uri>) parcelableArrayListExtra));
                }
            } else if ("com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE".equals(action)) {
                a();
            } else {
                StringBuilder sb2 = new StringBuilder(String.valueOf(action).length() + 37);
                sb2.append("Unknown action received ");
                sb2.append(action);
                sb2.append(", terminating");
                Log.e("GcmTaskService", sb2.toString());
            }
            a(i2);
            return 2;
        } finally {
            a(i2);
        }
    }

    @DexIgnore
    public final void a(int i) {
        synchronized (this.a) {
            this.b = i;
            if (!this.f.a(this.e.getClassName())) {
                stopSelf(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Bundle b;
        @DexIgnore
        public /* final */ List<Uri> c;
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public /* final */ f92 e;
        @DexIgnore
        public /* final */ Messenger f;

        @DexIgnore
        public b(String str, IBinder iBinder, Bundle bundle, long j, List<Uri> list) {
            f92 f92;
            this.a = str;
            if (iBinder == null) {
                f92 = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gcm.INetworkTaskCallback");
                if (queryLocalInterface instanceof f92) {
                    f92 = (f92) queryLocalInterface;
                } else {
                    f92 = new g92(iBinder);
                }
            }
            this.e = f92;
            this.b = bundle;
            this.d = j;
            this.c = list;
            this.f = null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x005c, code lost:
            return;
         */
        @DexIgnore
        public final void a(int i) {
            synchronized (a92.this.a) {
                try {
                    if (a92.this.f.c(this.a, a92.this.e.getClassName())) {
                        a92.this.f.b(this.a, a92.this.e.getClassName());
                        if (!a() && !a92.this.f.a(a92.this.e.getClassName())) {
                            a92.this.stopSelf(a92.this.b);
                        }
                    } else {
                        if (a()) {
                            Messenger messenger = this.f;
                            Message obtain = Message.obtain();
                            obtain.what = 3;
                            obtain.arg1 = i;
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("component", a92.this.e);
                            bundle.putString("tag", this.a);
                            obtain.setData(bundle);
                            messenger.send(obtain);
                        } else {
                            this.e.e(i);
                        }
                        a92.this.f.b(this.a, a92.this.e.getClassName());
                        if (!a() && !a92.this.f.a(a92.this.e.getClassName())) {
                            a92.this.stopSelf(a92.this.b);
                        }
                    }
                } catch (RemoteException unused) {
                    try {
                        String valueOf = String.valueOf(this.a);
                        Log.e("GcmTaskService", valueOf.length() != 0 ? "Error reporting result of operation to scheduler for ".concat(valueOf) : new String("Error reporting result of operation to scheduler for "));
                        a92.this.f.b(this.a, a92.this.e.getClassName());
                        if (!a() && !a92.this.f.a(a92.this.e.getClassName())) {
                            a92.this.stopSelf(a92.this.b);
                        }
                    } catch (Throwable th) {
                        a92.this.f.b(this.a, a92.this.e.getClassName());
                        if (!a() && !a92.this.f.a(a92.this.e.getClassName())) {
                            a92.this.stopSelf(a92.this.b);
                        }
                        throw th;
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
            a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
            throw r2;
         */
        @DexIgnore
        public final void run() {
            String valueOf = String.valueOf(this.a);
            i92 i92 = new i92(valueOf.length() != 0 ? "nts:client:onRunTask:".concat(valueOf) : new String("nts:client:onRunTask:"));
            c92 c92 = new c92(this.a, this.b, this.d, this.c);
            a92.this.g.a("onRunTask", nf2.a);
            a(a92.this.a(c92));
            a((Throwable) null, i92);
        }

        @DexIgnore
        public b(String str, Messenger messenger, Bundle bundle, long j, List<Uri> list) {
            this.a = str;
            this.f = messenger;
            this.b = bundle;
            this.d = j;
            this.c = list;
            this.e = null;
        }

        @DexIgnore
        public final boolean a() {
            return this.f != null;
        }

        @DexIgnore
        public static /* synthetic */ void a(Throwable th, i92 i92) {
            if (th != null) {
                try {
                    i92.close();
                } catch (Throwable th2) {
                    of2.a(th, th2);
                }
            } else {
                i92.close();
            }
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        try {
            this.c.execute(bVar);
        } catch (RejectedExecutionException e2) {
            Log.e("GcmTaskService", "Executor is shutdown. onDestroy was called but main looper had an unprocessed start task message. The task will be retried with backoff delay.", e2);
            bVar.a(1);
        }
    }
}
