package com.fossil;

import com.fossil.m24;
import com.fossil.ur4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yz4$g$b implements m24.e<ur4.d, ur4.c> {
    @DexIgnore
    public /* final */ /* synthetic */ QuickResponseViewModel.g a;

    @DexIgnore
    public yz4$g$b(QuickResponseViewModel.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetReplyMessageMappingUseCase.d dVar) {
        wg6.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d("QuickResponseViewModel", "Set reply message to device success");
        l24.a(this.a.this$0, false, true, 1, (Object) null);
        QuickResponseViewModel.b.a(this.a.this$0.g, (List) null, (Integer) null, (Boolean) null, (Boolean) null, true, false, (Boolean) null, (Boolean) null, 207, (Object) null);
        this.a.this$0.e();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(SetReplyMessageMappingUseCase.c cVar) {
        wg6.b(cVar, "errorValue");
        FLogger.INSTANCE.getLocal().e("QuickResponseViewModel", "Set reply message to device fail");
        l24.a(this.a.this$0, false, true, 1, (Object) null);
        if (!xm4.d.a(PortfolioApp.get.instance().getApplicationContext(), 1)) {
            this.a.this$0.a(uh4.BLUETOOTH_OFF);
            return;
        }
        QuickResponseViewModel.b.a(this.a.this$0.g, (List) null, (Integer) null, (Boolean) null, (Boolean) null, false, true, (Boolean) null, (Boolean) null, 207, (Object) null);
        this.a.this$0.e();
    }
}
