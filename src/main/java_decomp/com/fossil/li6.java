package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface li6<R> extends ei6<R> {

    @DexIgnore
    public interface a<R> {
    }

    @DexIgnore
    public interface b<R> extends a<R>, ii6<R> {
    }

    @DexIgnore
    boolean isConst();

    @DexIgnore
    boolean isLateinit();
}
