package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nt6 implements yt6 {
    @DexIgnore
    public /* final */ yt6 a;

    @DexIgnore
    public nt6(yt6 yt6) {
        if (yt6 != null) {
            this.a = yt6;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public void a(jt6 jt6, long j) throws IOException {
        this.a.a(jt6, j);
    }

    @DexIgnore
    public au6 b() {
        return this.a.b();
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public void flush() throws IOException {
        this.a.flush();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + "(" + this.a.toString() + ")";
    }
}
