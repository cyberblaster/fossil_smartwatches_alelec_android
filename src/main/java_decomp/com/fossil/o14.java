package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o14 implements Factory<GoogleApiService> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<ip4> b;
    @DexIgnore
    public /* final */ Provider<mp4> c;

    @DexIgnore
    public o14(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static o14 a(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return new o14(b14, provider, provider2);
    }

    @DexIgnore
    public static GoogleApiService b(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return a(b14, provider.get(), provider2.get());
    }

    @DexIgnore
    public static GoogleApiService a(b14 b14, ip4 ip4, mp4 mp4) {
        GoogleApiService c2 = b14.c(ip4, mp4);
        z76.a(c2, "Cannot return null from a non-@Nullable @Provides method");
        return c2;
    }

    @DexIgnore
    public GoogleApiService get() {
        return b(this.a, this.b, this.c);
    }
}
