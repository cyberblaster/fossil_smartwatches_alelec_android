package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s86 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public s86(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || s86.class != obj.getClass()) {
            return false;
        }
        s86 s86 = (s86) obj;
        if (this.b != s86.b) {
            return false;
        }
        String str = this.a;
        String str2 = s86.a;
        return str == null ? str2 == null : str.equals(str2);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        return ((str != null ? str.hashCode() : 0) * 31) + (this.b ? 1 : 0);
    }
}
