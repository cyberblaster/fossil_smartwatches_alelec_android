package com.fossil;

import com.fossil.r40;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo0 extends if1 {
    @DexIgnore
    public r40 B;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public oo0(ue1 ue1, q41 q41, String str) {
        super(r1, q41, eh1.READ_DEVICE_INFO_CHARACTERISTICS, str);
        ue1 ue12 = ue1;
        this.B = new r40(ue1.d(), ue12.t, "", "", "", (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262112);
    }

    @DexIgnore
    public Object d() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (qv0) new lh1(this.w), (hg6) new cl0(this), (hg6) new vm0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(super.k(), bm0.DEVICE_INFO, (Object) this.B.a());
    }
}
