package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x92 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ u92 a;
    @DexIgnore
    public /* final */ /* synthetic */ w92 b;

    @DexIgnore
    public x92(w92 w92, u92 u92) {
        this.b = w92;
        this.a = u92;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.b.a.handleIntent(this.a.a);
        this.a.a();
    }
}
