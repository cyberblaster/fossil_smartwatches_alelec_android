package com.fossil;

import com.fossil.im3;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nm3<E> extends om3<E> implements NavigableSet<E>, co3<E> {
    @DexIgnore
    public /* final */ transient Comparator<? super E> comparator;
    @DexIgnore
    public transient nm3<E> descendingSet;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<? super E> comparator;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public b(Comparator<? super E> comparator2, Object[] objArr) {
            this.comparator = comparator2;
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            a aVar = new a(this.comparator);
            aVar.a((E[]) this.elements);
            return aVar.a();
        }
    }

    @DexIgnore
    public nm3(Comparator<? super E> comparator2) {
        this.comparator = comparator2;
    }

    @DexIgnore
    public static <E> nm3<E> construct(Comparator<? super E> comparator2, int i, E... eArr) {
        if (i == 0) {
            return emptySet(comparator2);
        }
        in3.b(eArr, i);
        Arrays.sort(eArr, 0, i, comparator2);
        int i2 = 1;
        for (int i3 = 1; i3 < i; i3++) {
            E e = eArr[i3];
            if (comparator2.compare(e, eArr[i2 - 1]) != 0) {
                eArr[i2] = e;
                i2++;
            }
        }
        Arrays.fill(eArr, i2, i, (Object) null);
        return new tn3(zl3.asImmutableList(eArr, i2), comparator2);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> copyOf(E[] eArr) {
        return construct(jn3.natural(), eArr.length, (Object[]) eArr.clone());
    }

    @DexIgnore
    public static <E> nm3<E> copyOfSorted(SortedSet<E> sortedSet) {
        Comparator<? super E> a2 = do3.a(sortedSet);
        zl3<E> copyOf = zl3.copyOf(sortedSet);
        if (copyOf.isEmpty()) {
            return emptySet(a2);
        }
        return new tn3(copyOf, a2);
    }

    @DexIgnore
    public static <E> tn3<E> emptySet(Comparator<? super E> comparator2) {
        if (jn3.natural().equals(comparator2)) {
            return tn3.NATURAL_EMPTY_SET;
        }
        return new tn3<>(zl3.of(), comparator2);
    }

    @DexIgnore
    public static <E extends Comparable<?>> a<E> naturalOrder() {
        return new a<>(jn3.natural());
    }

    @DexIgnore
    public static <E> nm3<E> of() {
        return tn3.NATURAL_EMPTY_SET;
    }

    @DexIgnore
    public static <E> a<E> orderedBy(Comparator<E> comparator2) {
        return new a<>(comparator2);
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    public static <E extends Comparable<?>> a<E> reverseOrder() {
        return new a<>(jn3.natural().reverse());
    }

    @DexIgnore
    public E ceiling(E e) {
        return pm3.a(tailSet(e, true), null);
    }

    @DexIgnore
    public Comparator<? super E> comparator() {
        return this.comparator;
    }

    @DexIgnore
    public nm3<E> createDescendingSet() {
        return new gl3(this);
    }

    @DexIgnore
    public abstract jo3<E> descendingIterator();

    @DexIgnore
    public E first() {
        return iterator().next();
    }

    @DexIgnore
    public E floor(E e) {
        return qm3.b(headSet(e, true).descendingIterator(), null);
    }

    @DexIgnore
    public abstract nm3<E> headSetImpl(E e, boolean z);

    @DexIgnore
    public E higher(E e) {
        return pm3.a(tailSet(e, false), null);
    }

    @DexIgnore
    public abstract int indexOf(Object obj);

    @DexIgnore
    public abstract jo3<E> iterator();

    @DexIgnore
    public E last() {
        return descendingIterator().next();
    }

    @DexIgnore
    public E lower(E e) {
        return qm3.b(headSet(e, false).descendingIterator(), null);
    }

    @DexIgnore
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract nm3<E> subSetImpl(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    public abstract nm3<E> tailSetImpl(E e, boolean z);

    @DexIgnore
    public int unsafeCompare(Object obj, Object obj2) {
        return unsafeCompare(this.comparator, obj, obj2);
    }

    @DexIgnore
    public Object writeReplace() {
        return new b(this.comparator, toArray());
    }

    @DexIgnore
    public static <E> nm3<E> copyOf(Iterable<? extends E> iterable) {
        return copyOf(jn3.natural(), iterable);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> of(E e) {
        return new tn3(zl3.of(e), jn3.natural());
    }

    @DexIgnore
    public static int unsafeCompare(Comparator<?> comparator2, Object obj, Object obj2) {
        return comparator2.compare(obj, obj2);
    }

    @DexIgnore
    public nm3<E> descendingSet() {
        nm3<E> nm3 = this.descendingSet;
        if (nm3 != null) {
            return nm3;
        }
        nm3<E> createDescendingSet = createDescendingSet();
        this.descendingSet = createDescendingSet;
        createDescendingSet.descendingSet = this;
        return createDescendingSet;
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> of(E e, E e2) {
        return construct(jn3.natural(), 2, e, e2);
    }

    @DexIgnore
    public nm3<E> headSet(E e) {
        return headSet(e, false);
    }

    @DexIgnore
    public nm3<E> subSet(E e, E e2) {
        return subSet(e, true, e2, false);
    }

    @DexIgnore
    public nm3<E> tailSet(E e) {
        return tailSet(e, true);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<E> extends im3.a<E> {
        @DexIgnore
        public /* final */ Comparator<? super E> c;

        @DexIgnore
        public a(Comparator<? super E> comparator) {
            jk3.a(comparator);
            this.c = comparator;
        }

        @DexIgnore
        public a<E> a(E e) {
            super.a(e);
            return this;
        }

        @DexIgnore
        public a<E> a(E... eArr) {
            super.a(eArr);
            return this;
        }

        @DexIgnore
        public a<E> a(Iterator<? extends E> it) {
            super.a(it);
            return this;
        }

        @DexIgnore
        public nm3<E> a() {
            nm3<E> construct = nm3.construct(this.c, this.b, this.a);
            this.b = construct.size();
            return construct;
        }
    }

    @DexIgnore
    public static <E> nm3<E> copyOf(Collection<? extends E> collection) {
        return copyOf(jn3.natural(), collection);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> of(E e, E e2, E e3) {
        return construct(jn3.natural(), 3, e, e2, e3);
    }

    @DexIgnore
    public nm3<E> headSet(E e, boolean z) {
        jk3.a(e);
        return headSetImpl(e, z);
    }

    @DexIgnore
    public nm3<E> subSet(E e, boolean z, E e2, boolean z2) {
        jk3.a(e);
        jk3.a(e2);
        jk3.a(this.comparator.compare(e, e2) <= 0);
        return subSetImpl(e, z, e2, z2);
    }

    @DexIgnore
    public nm3<E> tailSet(E e, boolean z) {
        jk3.a(e);
        return tailSetImpl(e, z);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> of(E e, E e2, E e3, E e4) {
        return construct(jn3.natural(), 4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E> nm3<E> copyOf(Iterator<? extends E> it) {
        return copyOf(jn3.natural(), it);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> of(E e, E e2, E e3, E e4, E e5) {
        return construct(jn3.natural(), 5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> nm3<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        Comparable[] comparableArr = new Comparable[(eArr.length + 6)];
        comparableArr[0] = e;
        comparableArr[1] = e2;
        comparableArr[2] = e3;
        comparableArr[3] = e4;
        comparableArr[4] = e5;
        comparableArr[5] = e6;
        System.arraycopy(eArr, 0, comparableArr, 6, eArr.length);
        return construct(jn3.natural(), comparableArr.length, comparableArr);
    }

    @DexIgnore
    public static <E> nm3<E> copyOf(Comparator<? super E> comparator2, Iterator<? extends E> it) {
        a aVar = new a(comparator2);
        aVar.a((Iterator) it);
        return aVar.a();
    }

    @DexIgnore
    public static <E> nm3<E> copyOf(Comparator<? super E> comparator2, Iterable<? extends E> iterable) {
        jk3.a(comparator2);
        if (do3.a(comparator2, iterable) && (iterable instanceof nm3)) {
            nm3<E> nm3 = (nm3) iterable;
            if (!nm3.isPartialView()) {
                return nm3;
            }
        }
        Object[] c = pm3.c(iterable);
        return construct(comparator2, c.length, c);
    }

    @DexIgnore
    public static <E> nm3<E> copyOf(Comparator<? super E> comparator2, Collection<? extends E> collection) {
        return copyOf(comparator2, collection);
    }
}
