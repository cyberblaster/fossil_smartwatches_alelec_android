package com.fossil;

import com.portfolio.platform.util.NetworkBoundResource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$response$1", f = "NetworkBoundResource.kt", l = {69}, m = "invokeSuspend")
public final class tx5$b$f extends sf6 implements hg6<xe6<? super rx6<RequestType>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkBoundResource.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tx5$b$f(NetworkBoundResource.b bVar, xe6 xe6) {
        super(1, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new tx5$b$f(this.this$0, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((tx5$b$f) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            NetworkBoundResource networkBoundResource = this.this$0.this$0;
            this.label = 1;
            obj = networkBoundResource.createCall(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
