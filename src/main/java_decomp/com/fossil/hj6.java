package com.fossil;

import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj6 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ wh6 b;

    @DexIgnore
    public hj6(String str, wh6 wh6) {
        wg6.b(str, ServerSetting.VALUE);
        wg6.b(wh6, "range");
        this.a = str;
        this.b = wh6;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hj6)) {
            return false;
        }
        hj6 hj6 = (hj6) obj;
        return wg6.a((Object) this.a, (Object) hj6.a) && wg6.a((Object) this.b, (Object) hj6.b);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        wh6 wh6 = this.b;
        if (wh6 != null) {
            i = wh6.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "MatchGroup(value=" + this.a + ", range=" + this.b + ")";
    }
}
