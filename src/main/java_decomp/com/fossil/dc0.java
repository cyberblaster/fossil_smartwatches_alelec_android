package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dc0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<dc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new dc0(parcel.readInt(), parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new dc0[i];
        }
    }

    @DexIgnore
    public dc0(int i, int i2, int i3) throws IllegalArgumentException {
        this.a = i;
        this.b = i2;
        this.c = i3;
        int i4 = this.a;
        boolean z = true;
        if (i4 >= 0 && 359 >= i4) {
            int i5 = this.b;
            if (i5 >= 0 && 120 >= i5) {
                if (!(this.c < 0 ? false : z)) {
                    throw new IllegalArgumentException(ze0.a(ze0.b("z index("), this.c, ") must be larger than ", "[0]."));
                }
                return;
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("distanceFromCenter("), this.b, ") is out of ", "range [0, 120]."));
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("angle("), this.a, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("angle", this.a).put("distance", this.b).put("z_index", this.c);
        wg6.a(put, "JSONObject()\n           \u2026Constant.Z_INDEX, zIndex)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(dc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            dc0 dc0 = (dc0) obj;
            return this.a == dc0.a && this.b == dc0.b && this.c == dc0.c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.a;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.b;
    }

    @DexIgnore
    public final int getZIndex() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a * 31) + this.b) * 31) + this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
