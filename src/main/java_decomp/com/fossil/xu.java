package com.fossil;

import com.fossil.fs;
import com.fossil.jv;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xu<Data> implements jv<byte[], Data> {
    @DexIgnore
    public /* final */ b<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements kv<byte[], ByteBuffer> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xu$a$a")
        /* renamed from: com.fossil.xu$a$a  reason: collision with other inner class name */
        public class C0057a implements b<ByteBuffer> {
            @DexIgnore
            public C0057a(a aVar) {
            }

            @DexIgnore
            public Class<ByteBuffer> getDataClass() {
                return ByteBuffer.class;
            }

            @DexIgnore
            public ByteBuffer a(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }
        }

        @DexIgnore
        public jv<byte[], ByteBuffer> a(nv nvVar) {
            return new xu(new C0057a(this));
        }
    }

    @DexIgnore
    public interface b<Data> {
        @DexIgnore
        Data a(byte[] bArr);

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<Data> implements fs<Data> {
        @DexIgnore
        public /* final */ byte[] a;
        @DexIgnore
        public /* final */ b<Data> b;

        @DexIgnore
        public c(byte[] bArr, b<Data> bVar) {
            this.a = bArr;
            this.b = bVar;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super Data> aVar) {
            aVar.a(this.b.a(this.a));
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.b.getDataClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements kv<byte[], InputStream> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements b<InputStream> {
            @DexIgnore
            public a(d dVar) {
            }

            @DexIgnore
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            public InputStream a(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }
        }

        @DexIgnore
        public jv<byte[], InputStream> a(nv nvVar) {
            return new xu(new a(this));
        }
    }

    @DexIgnore
    public xu(b<Data> bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public boolean a(byte[] bArr) {
        return true;
    }

    @DexIgnore
    public jv.a<Data> a(byte[] bArr, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(bArr), new c(bArr, this.a));
    }
}
