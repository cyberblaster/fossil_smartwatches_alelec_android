package com.fossil;

import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s5 {
    @DexIgnore
    public HashSet<s5> a; // = new HashSet<>(2);
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public void a(s5 s5Var) {
        this.a.add(s5Var);
    }

    @DexIgnore
    public void b() {
        this.b = 0;
        Iterator<s5> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().b();
        }
    }

    @DexIgnore
    public boolean c() {
        return this.b == 1;
    }

    @DexIgnore
    public void d() {
        this.b = 0;
        this.a.clear();
    }

    @DexIgnore
    public void e() {
    }

    @DexIgnore
    public void a() {
        this.b = 1;
        Iterator<s5> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().e();
        }
    }
}
