package com.fossil;

import com.fossil.i60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class cm1 extends tg6 implements hg6<byte[], i60> {
    @DexIgnore
    public cm1(i60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(i60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/CurrentHeartRateConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((i60.a) this.receiver).a((byte[]) obj);
    }
}
