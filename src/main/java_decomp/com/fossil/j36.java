package com.fossil;

import android.app.Activity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j36 extends Activity {
    @DexIgnore
    public void onPause() {
        super.onPause();
        p36.a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        p36.b(this);
    }
}
