package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t31 extends p40 {
    @DexIgnore
    public static /* final */ jy0 c; // = new jy0((qg6) null);
    @DexIgnore
    public /* final */ x11 a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public t31(x11 x11, int i) {
        this.a = x11;
        this.b = i;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.RESULT_CODE, (Object) cw0.a((Enum<?>) this.a));
            if (this.b != 0) {
                cw0.a(jSONObject, bm0.GATT_STATUS, (Object) Integer.valueOf(this.b));
            }
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ t31(x11 x11, int i, int i2) {
        i = (i2 & 2) != 0 ? 0 : i;
        this.a = x11;
        this.b = i;
    }
}
