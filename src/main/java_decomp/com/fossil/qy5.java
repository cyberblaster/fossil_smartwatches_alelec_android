package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qy5 implements Interceptor {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");
    @DexIgnore
    public volatile HttpLoggingInterceptor.a a; // = HttpLoggingInterceptor.a.NONE;

    @DexIgnore
    public qy5 a(HttpLoggingInterceptor.a aVar) {
        if (aVar != null) {
            this.a = aVar;
            return this;
        }
        throw new NullPointerException("level == null. Use Level.EMPTY instead.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d6  */
    public Response intercept(Interceptor.Chain chain) throws IOException {
        long j;
        long j2;
        String str;
        String str2;
        String str3;
        long j3;
        String str4;
        String str5;
        String str6;
        long j4;
        String str7;
        String str8;
        int b2;
        int i;
        Interceptor.Chain chain2 = chain;
        HttpLoggingInterceptor.a aVar = this.a;
        yq6 t = chain.t();
        if (aVar == HttpLoggingInterceptor.a.NONE) {
            return chain2.a(t);
        }
        boolean z = true;
        boolean z2 = aVar == HttpLoggingInterceptor.a.BODY;
        boolean z3 = z2 || aVar == HttpLoggingInterceptor.a.HEADERS;
        RequestBody a2 = t.a();
        if (a2 == null) {
            z = false;
        }
        hq6 c = chain.c();
        wq6 a3 = c != null ? c.a() : wq6.HTTP_1_1;
        long nanoTime = System.nanoTime();
        StringBuilder sb = new StringBuilder();
        sb.append("--> Id: ");
        sb.append(nanoTime);
        sb.append("\n");
        sb.append("--> ");
        sb.append(t.e());
        sb.append(' ');
        sb.append(t.g());
        sb.append(' ');
        sb.append(a3);
        if (z3 || !z) {
            j = nanoTime;
        } else {
            sb.append(" (");
            j = nanoTime;
            sb.append(a2.a());
            sb.append("-byte body)");
        }
        sb.append("\n");
        if (z3) {
            if (z) {
                if (a2.b() != null) {
                    sb.append("--> Content-Type: ");
                    sb.append(a2.b());
                    sb.append("\n");
                }
                if (a2.a() != -1) {
                    sb.append("--> Content-Length: ");
                    str8 = "-byte body)";
                    sb.append(a2.a());
                    sb.append("\n");
                    sq6 c2 = t.c();
                    ku3 ku3 = new ku3();
                    b2 = c2.b();
                    str = "empty";
                    i = 0;
                    while (i < b2) {
                        int i2 = b2;
                        String a4 = c2.a(i);
                        long j5 = j;
                        if (!"Content-Type".equalsIgnoreCase(a4) && !"Content-Length".equalsIgnoreCase(a4)) {
                            ku3.a(a4, c2.b(i));
                        }
                        i++;
                        b2 = i2;
                        j = j5;
                    }
                    j2 = j;
                    sb.append("--> Headers: ");
                    sb.append(ku3);
                    sb.append("\n");
                    if (z2 || !z) {
                        str2 = str8;
                        sb.append("--> END ");
                        sb.append(t.e());
                        sb.append("\n");
                    } else if (a(t.c())) {
                        sb.append("--> END ");
                        sb.append(t.e());
                        sb.append(" (encoded body omitted)");
                        sb.append("\n");
                        str2 = str8;
                    } else {
                        jt6 jt6 = new jt6();
                        a2.a(jt6);
                        Charset charset = b;
                        uq6 b3 = a2.b();
                        if (b3 != null) {
                            charset = b3.a(b);
                        }
                        if (a(jt6)) {
                            sb.append("--> Body: ");
                            sb.append(charset != null ? jt6.a(charset) : str);
                            sb.append("\n");
                            sb.append("--> END ");
                            sb.append(t.e());
                            sb.append(" (");
                            sb.append(a2.a());
                            str2 = str8;
                            sb.append(str2);
                            sb.append("\n");
                        } else {
                            str2 = str8;
                            sb.append("--> END ");
                            sb.append(t.e());
                            sb.append("(binary ");
                            sb.append(a2.a());
                            sb.append("-byte body omitted)");
                            sb.append("\n");
                        }
                    }
                }
            }
            str8 = "-byte body)";
            sq6 c22 = t.c();
            ku3 ku32 = new ku3();
            b2 = c22.b();
            str = "empty";
            i = 0;
            while (i < b2) {
            }
            j2 = j;
            sb.append("--> Headers: ");
            sb.append(ku32);
            sb.append("\n");
            if (z2) {
            }
            str2 = str8;
            sb.append("--> END ");
            sb.append(t.e());
            sb.append("\n");
        } else {
            str = "empty";
            str2 = "-byte body)";
            j2 = j;
        }
        FLogger.INSTANCE.getLocal().d("OkHttp", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<-- Id: ");
        sb2.append(j2);
        sb2.append("\n");
        long nanoTime2 = System.nanoTime();
        try {
            Response a5 = chain2.a(t);
            long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime2);
            zq6 k = a5.k();
            if (k != null) {
                str3 = "OkHttp";
                str4 = "-byte body omitted)";
                j3 = k.contentLength();
            } else {
                str3 = "OkHttp";
                str4 = "-byte body omitted)";
                j3 = -1;
            }
            if (j3 == -1) {
                str6 = "unknown-length";
                str5 = str2;
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(j3);
                str5 = str2;
                sb3.append("-byte");
                str6 = sb3.toString();
            }
            if (z3) {
                str7 = "";
                j4 = j3;
            } else {
                StringBuilder sb4 = new StringBuilder();
                j4 = j3;
                sb4.append(", ");
                sb4.append(str6);
                sb4.append(" body");
                str7 = sb4.toString();
            }
            sb2.append("<-- Code ");
            sb2.append(a5.n());
            sb2.append(' ');
            sb2.append(a5.C());
            sb2.append(' ');
            sb2.append(a5.I().g());
            sb2.append(" (");
            sb2.append(millis);
            sb2.append("ms");
            sb2.append(str7);
            sb2.append(')');
            sb2.append("\n");
            if (z3) {
                sq6 p = a5.p();
                ku3 ku33 = new ku3();
                int b4 = p.b();
                for (int i3 = 0; i3 < b4; i3++) {
                    ku33.a(p.a(i3), p.b(i3));
                }
                sb2.append("<-- Headers: ");
                sb2.append(ku33);
                sb2.append("\n");
                if (!z2 || !yr6.b(a5)) {
                    sb2.append("<-- END HTTP");
                    sb2.append("\n");
                } else if (a(a5.p())) {
                    sb2.append("<-- END HTTP (encoded body omitted)");
                    sb2.append("\n");
                } else if (k != null) {
                    lt6 source = k.source();
                    source.g(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
                    jt6 a6 = source.a();
                    Charset charset2 = b;
                    uq6 contentType = k.contentType();
                    if (contentType != null) {
                        charset2 = contentType.a(b);
                    }
                    if (!a(a6)) {
                        sb2.append("<-- END HTTP (binary ");
                        sb2.append(a6.p());
                        sb2.append(str4);
                        sb2.append("\n");
                        FLogger.INSTANCE.getLocal().d(str3, sb2.toString());
                        return a5;
                    }
                    if (j4 != 0) {
                        sb2.append("<-- Body: ");
                        sb2.append(charset2 != null ? a6.clone().a(charset2) : str);
                        sb2.append("\n");
                    }
                    sb2.append("<-- END HTTP (");
                    sb2.append(a6.p());
                    sb2.append(str5);
                    sb2.append("\n");
                } else {
                    sb2.append("<-- END HTTP (");
                    sb2.append("-1");
                    sb2.append(str5);
                    sb2.append("\n");
                }
            }
            FLogger.INSTANCE.getLocal().d(str3, sb2.toString());
            return a5;
        } catch (Exception e) {
            Exception exc = e;
            sb2.append("<-- HTTP FAILED: ");
            sb2.append(exc);
            sb2.append("\n");
            FLogger.INSTANCE.getLocal().d("OkHttp", sb2.toString());
            throw exc;
        }
    }

    @DexIgnore
    public static boolean a(jt6 jt6) {
        try {
            jt6 jt62 = new jt6();
            jt6.a(jt62, 0, jt6.p() < 64 ? jt6.p() : 64);
            for (int i = 0; i < 16; i++) {
                if (jt62.f()) {
                    return true;
                }
                int o = jt62.o();
                if (Character.isISOControl(o) && !Character.isWhitespace(o)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }

    @DexIgnore
    public final boolean a(sq6 sq6) {
        String a2 = sq6.a("Content-Encoding");
        return a2 != null && !a2.equalsIgnoreCase("identity");
    }
}
