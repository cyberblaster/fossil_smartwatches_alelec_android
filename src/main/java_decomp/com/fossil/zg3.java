package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.material.datepicker.MaterialDatePicker;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg3 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<zg3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ hh3 a;
    @DexIgnore
    public /* final */ hh3 b;
    @DexIgnore
    public /* final */ hh3 c;
    @DexIgnore
    public /* final */ c d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<zg3> {
        @DexIgnore
        public zg3 createFromParcel(Parcel parcel) {
            return new zg3((hh3) parcel.readParcelable(hh3.class.getClassLoader()), (hh3) parcel.readParcelable(hh3.class.getClassLoader()), (hh3) parcel.readParcelable(hh3.class.getClassLoader()), (c) parcel.readParcelable(c.class.getClassLoader()), (a) null);
        }

        @DexIgnore
        public zg3[] newArray(int i) {
            return new zg3[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ long e; // = nh3.a(hh3.a(1900, 0).g);
        @DexIgnore
        public static /* final */ long f; // = nh3.a(hh3.a(2100, 11).g);
        @DexIgnore
        public long a; // = e;
        @DexIgnore
        public long b; // = f;
        @DexIgnore
        public Long c;
        @DexIgnore
        public c d; // = eh3.b(Long.MIN_VALUE);

        @DexIgnore
        public b(zg3 zg3) {
            this.a = zg3.a.g;
            this.b = zg3.b.g;
            this.c = Long.valueOf(zg3.c.g);
            this.d = zg3.d;
        }

        @DexIgnore
        public b a(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public zg3 a() {
            if (this.c == null) {
                long h1 = MaterialDatePicker.h1();
                if (this.a > h1 || h1 > this.b) {
                    h1 = this.a;
                }
                this.c = Long.valueOf(h1);
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("DEEP_COPY_VALIDATOR_KEY", this.d);
            return new zg3(hh3.c(this.a), hh3.c(this.b), hh3.c(this.c.longValue()), (c) bundle.getParcelable("DEEP_COPY_VALIDATOR_KEY"), (a) null);
        }
    }

    @DexIgnore
    public interface c extends Parcelable {
        @DexIgnore
        boolean a(long j);
    }

    @DexIgnore
    public /* synthetic */ zg3(hh3 hh3, hh3 hh32, hh3 hh33, c cVar, a aVar) {
        this(hh3, hh32, hh33, cVar);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public hh3 e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zg3)) {
            return false;
        }
        zg3 zg3 = (zg3) obj;
        if (!this.a.equals(zg3.a) || !this.b.equals(zg3.b) || !this.c.equals(zg3.c) || !this.d.equals(zg3.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int f() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, this.b, this.c, this.d});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, 0);
        parcel.writeParcelable(this.b, 0);
        parcel.writeParcelable(this.c, 0);
        parcel.writeParcelable(this.d, 0);
    }

    @DexIgnore
    public zg3(hh3 hh3, hh3 hh32, hh3 hh33, c cVar) {
        this.a = hh3;
        this.b = hh32;
        this.c = hh33;
        this.d = cVar;
        if (hh3.compareTo(hh33) > 0) {
            throw new IllegalArgumentException("start Month cannot be after current Month");
        } else if (hh33.compareTo(hh32) <= 0) {
            this.f = hh3.b(hh32) + 1;
            this.e = (hh32.d - hh3.d) + 1;
        } else {
            throw new IllegalArgumentException("current Month cannot be after end Month");
        }
    }

    @DexIgnore
    public c a() {
        return this.d;
    }

    @DexIgnore
    public hh3 b() {
        return this.b;
    }

    @DexIgnore
    public int c() {
        return this.f;
    }

    @DexIgnore
    public hh3 d() {
        return this.c;
    }
}
