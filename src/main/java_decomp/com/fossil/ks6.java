package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks6 {
    @DexIgnore
    public static /* final */ mt6 a; // = mt6.encodeUtf8("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
    @DexIgnore
    public static /* final */ String[] b; // = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};
    @DexIgnore
    public static /* final */ String[] c; // = new String[64];
    @DexIgnore
    public static /* final */ String[] d; // = new String[256];

    /*
    static {
        int i = 0;
        int i2 = 0;
        while (true) {
            String[] strArr = d;
            if (i2 >= strArr.length) {
                break;
            }
            strArr[i2] = fr6.a("%8s", Integer.toBinaryString(i2)).replace(' ', '0');
            i2++;
        }
        String[] strArr2 = c;
        strArr2[0] = "";
        strArr2[1] = "END_STREAM";
        int[] iArr = {1};
        strArr2[8] = "PADDED";
        for (int i3 : iArr) {
            c[i3 | 8] = c[i3] + "|PADDED";
        }
        String[] strArr3 = c;
        strArr3[4] = "END_HEADERS";
        strArr3[32] = "PRIORITY";
        strArr3[36] = "END_HEADERS|PRIORITY";
        for (int i4 : new int[]{4, 32, 36}) {
            for (int i5 : iArr) {
                int i6 = i5 | i4;
                c[i6] = c[i5] + '|' + c[i4];
                c[i6 | 8] = c[i5] + '|' + c[i4] + "|PADDED";
            }
        }
        while (true) {
            String[] strArr4 = c;
            if (i < strArr4.length) {
                if (strArr4[i] == null) {
                    strArr4[i] = d[i];
                }
                i++;
            } else {
                return;
            }
        }
    }
    */

    @DexIgnore
    public static IllegalArgumentException a(String str, Object... objArr) {
        throw new IllegalArgumentException(fr6.a(str, objArr));
    }

    @DexIgnore
    public static IOException b(String str, Object... objArr) throws IOException {
        throw new IOException(fr6.a(str, objArr));
    }

    @DexIgnore
    public static String a(boolean z, int i, int i2, byte b2, byte b3) {
        String[] strArr = b;
        String a2 = b2 < strArr.length ? strArr[b2] : fr6.a("0x%02x", Byte.valueOf(b2));
        String a3 = a(b2, b3);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = a2;
        objArr[4] = a3;
        return fr6.a("%s 0x%08x %5d %-13s %s", objArr);
    }

    @DexIgnore
    public static String a(byte b2, byte b3) {
        if (b3 == 0) {
            return "";
        }
        if (!(b2 == 2 || b2 == 3)) {
            if (b2 == 4 || b2 == 6) {
                if (b3 == 1) {
                    return "ACK";
                }
                return d[b3];
            } else if (!(b2 == 7 || b2 == 8)) {
                String[] strArr = c;
                String str = b3 < strArr.length ? strArr[b3] : d[b3];
                if (b2 != 5 || (b3 & 4) == 0) {
                    return (b2 != 0 || (b3 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED");
                }
                return str.replace("HEADERS", "PUSH_PROMISE");
            }
        }
        return d[b3];
    }
}
