package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1", f = "MicroAppPresenter.kt", l = {77}, m = "invokeSuspend")
public final class v95$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppPresenter $this_run;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super List<? extends Category>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v95$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(v95$d$a v95_d_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = v95_d_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return this.this$0.$this_run.t.getAllCategories();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v95$d$a(MicroAppPresenter microAppPresenter, xe6 xe6) {
        super(2, xe6);
        this.$this_run = microAppPresenter;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        v95$d$a v95_d_a = new v95$d$a(this.$this_run, xe6);
        v95_d_a.p$ = (il6) obj;
        return v95_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((v95$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 b = zl6.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(b, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list = (List) obj;
        if (!list.isEmpty()) {
            this.$this_run.h.a(((Category) list.get(0)).getId());
        }
        return cd6.a;
    }
}
