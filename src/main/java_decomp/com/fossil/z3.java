package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.h;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z3 extends Service {
    @DexIgnore
    public /* final */ Map<IBinder, IBinder.DeathRecipient> a; // = new p4();
    @DexIgnore
    public h.a b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends h.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z3$a$a")
        /* renamed from: com.fossil.z3$a$a  reason: collision with other inner class name */
        public class C0058a implements IBinder.DeathRecipient {
            @DexIgnore
            public /* final */ /* synthetic */ b4 a;

            @DexIgnore
            public C0058a(b4 b4Var) {
                this.a = b4Var;
            }

            @DexIgnore
            public void binderDied() {
                z3.this.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean a(long j) {
            return z3.this.a(j);
        }

        @DexIgnore
        public Bundle b(String str, Bundle bundle) {
            return z3.this.a(str, bundle);
        }

        @DexIgnore
        public boolean a(g gVar) {
            b4 b4Var = new b4(gVar);
            try {
                C0058a aVar = new C0058a(b4Var);
                synchronized (z3.this.a) {
                    gVar.asBinder().linkToDeath(aVar, 0);
                    z3.this.a.put(gVar.asBinder(), aVar);
                }
                return z3.this.b(b4Var);
            } catch (RemoteException unused) {
                return false;
            }
        }

        @DexIgnore
        public boolean b(g gVar, Bundle bundle) {
            return z3.this.a(new b4(gVar), bundle);
        }

        @DexIgnore
        public int b(g gVar, String str, Bundle bundle) {
            return z3.this.a(new b4(gVar), str, bundle);
        }

        @DexIgnore
        public boolean a(g gVar, Uri uri, Bundle bundle, List<Bundle> list) {
            return z3.this.a(new b4(gVar), uri, bundle, list);
        }

        @DexIgnore
        public boolean a(g gVar, Uri uri) {
            return z3.this.a(new b4(gVar), uri);
        }

        @DexIgnore
        public boolean a(g gVar, int i, Uri uri, Bundle bundle) {
            return z3.this.a(new b4(gVar), i, uri, bundle);
        }
    }

    @DexIgnore
    public abstract int a(b4 b4Var, String str, Bundle bundle);

    @DexIgnore
    public abstract Bundle a(String str, Bundle bundle);

    @DexIgnore
    public abstract boolean a(long j);

    @DexIgnore
    public boolean a(b4 b4Var) {
        try {
            synchronized (this.a) {
                IBinder a2 = b4Var.a();
                a2.unlinkToDeath(this.a.get(a2), 0);
                this.a.remove(a2);
            }
            return true;
        } catch (NoSuchElementException unused) {
            return false;
        }
    }

    @DexIgnore
    public abstract boolean a(b4 b4Var, int i, Uri uri, Bundle bundle);

    @DexIgnore
    public abstract boolean a(b4 b4Var, Uri uri);

    @DexIgnore
    public abstract boolean a(b4 b4Var, Uri uri, Bundle bundle, List<Bundle> list);

    @DexIgnore
    public abstract boolean a(b4 b4Var, Bundle bundle);

    @DexIgnore
    public abstract boolean b(b4 b4Var);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.b;
    }
}
