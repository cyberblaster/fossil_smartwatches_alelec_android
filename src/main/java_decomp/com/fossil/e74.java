package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e74 extends d74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = new ViewDataBinding.j(22);
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout F;
    @DexIgnore
    public long G;

    /*
    static {
        H.a(1, new String[]{"item_default_place_commute_time", "item_default_place_commute_time"}, new int[]{2, 3}, new int[]{2131558658, 2131558658});
        I.put(2131362483, 4);
        I.put(2131362442, 5);
        I.put(2131362023, 6);
        I.put(2131362632, 7);
        I.put(2131362538, 8);
        I.put(2131363244, 9);
        I.put(2131361902, 10);
        I.put(2131362645, 11);
        I.put(2131362097, 12);
        I.put(2131362656, 13);
        I.put(2131362615, 14);
        I.put(2131362653, 15);
        I.put(2131362291, 16);
        I.put(2131362684, 17);
        I.put(2131362886, 18);
        I.put(2131361916, 19);
        I.put(2131362670, 20);
        I.put(2131362902, 21);
    }
    */

    @DexIgnore
    public e74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 22, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
        ViewDataBinding.d(this.u);
        ViewDataBinding.d(this.v);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r6.v.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.u.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 4;
        }
        this.u.f();
        this.v.f();
        g();
    }

    @DexIgnore
    public e74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 2, objArr[10], objArr[19], objArr[6], objArr[12], objArr[16], objArr[5], objArr[4], objArr[2], objArr[3], objArr[8], objArr[14], objArr[7], objArr[11], objArr[15], objArr[13], objArr[20], objArr[17], objArr[0], objArr[18], objArr[21], objArr[9]);
        this.G = -1;
        this.F = objArr[1];
        this.F.setTag((Object) null);
        this.C.setTag((Object) null);
        a(view);
        f();
    }
}
