package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeEditPresenter extends y35 {
    @DexIgnore
    public DianaCustomizeViewModel e;
    @DexIgnore
    public MutableLiveData<DianaPreset> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<m35> g; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<CustomizeRealData> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson i; // = new Gson();
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public WatchFaceWrapper k;
    @DexIgnore
    public /* final */ LiveData<m35> l;
    @DexIgnore
    public /* final */ z35 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public int o;
    @DexIgnore
    public /* final */ SetDianaPresetToWatchUseCase p;
    @DexIgnore
    public /* final */ om4 q;
    @DexIgnore
    public /* final */ an4 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DianaPreset dianaPreset, xe6 xe6, DianaCustomizeEditPresenter dianaCustomizeEditPresenter, DianaPreset dianaPreset2) {
            super(2, xe6);
            this.$it = dianaPreset;
            this.this$0 = dianaCustomizeEditPresenter;
            this.$currentPreset$inlined = dianaPreset2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$it, xe6, this.this$0, this.$currentPreset$inlined);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v81, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v22, resolved type: java.util.Iterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v86, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v14, resolved type: java.util.Iterator} */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r4v2, types: [com.fossil.d45$b$c, com.portfolio.platform.CoroutineUseCase$e] */
        /* JADX WARNING: type inference failed for: r0v19, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v22, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v48, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v51, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|(1:(1:(4:4|5|6|7)(2:11|12))(13:13|14|15|16|41|42|43|44|45|(7:47|48|75|28|(9:30|31|32|(6:34|35|36|37|(1:39)(7:40|41|42|43|44|45|(1:49)(0))|39)(5:54|55|56|(5:66|(1:68)|75|28|(1:76)(0))(5:58|(2:61|(1:65))(1:60)|75|28|(0)(0))|70)|71|74|75|28|(0)(0))(0)|78|(5:80|(2:82|(3:86|91|92))(2:87|(3:89|91|92))|90|91|92)(5:93|(4:96|(2:98|183)(1:182)|180|94)|181|99|(5:101|102|(10:104|105|106|(6:108|109|110|111|(1:113)(1:114)|113)(8:130|131|132|133|(2:142|(2:144|(1:146)))(3:135|(4:137|138|139|(1:141))|123)|152|102|(1:153)(0))|148|129|151|152|102|(0)(0))(0)|155|(5:157|(2:159|(3:163|168|169))(2:164|(3:166|168|169))|167|168|169)(4:170|(1:172)(1:173)|174|175))(3:154|155|(0)(0))))|49|78|(0)(0)))(5:19|(4:22|(2:24|179)(1:178)|176|20)|177|25|(5:27|28|(0)(0)|78|(0)(0))(3:77|78|(0)(0)))|8|115|116|117|118|119|(8:121|122|123|152|102|(0)(0)|155|(0)(0))|124|155|(0)(0)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(12:13|14|15|16|(2:41|42)|43|44|45|(7:47|48|75|28|(9:30|31|32|(6:34|35|36|37|(1:39)(7:40|41|42|43|44|45|(1:49)(0))|39)(5:54|55|56|(5:66|(1:68)|75|28|(1:76)(0))(5:58|(2:61|(1:65))(1:60)|75|28|(0)(0))|70)|71|74|75|28|(0)(0))(0)|78|(5:80|(2:82|(3:86|91|92))(2:87|(3:89|91|92))|90|91|92)(5:93|(4:96|(2:98|183)(1:182)|180|94)|181|99|(5:101|102|(10:104|105|106|(6:108|109|110|111|(1:113)(1:114)|113)(8:130|131|132|133|(2:142|(2:144|(1:146)))(3:135|(4:137|138|139|(1:141))|123)|152|102|(1:153)(0))|148|129|151|152|102|(0)(0))(0)|155|(5:157|(2:159|(3:163|168|169))(2:164|(3:166|168|169))|167|168|169)(4:170|(1:172)(1:173)|174|175))(3:154|155|(0)(0))))|49|78|(0)(0)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(6:108|109|110|111|(1:113)(1:114)|113) */
        /* JADX WARNING: Can't wrap try/catch for region: R(6:34|35|36|37|(1:39)(7:40|41|42|43|44|45|(1:49)(0))|39) */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x036a, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x036f, code lost:
            r15 = r22;
            r2 = r1;
            r1 = r5;
            r5 = r10;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x0171, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0176, code lost:
            r15 = r22;
            r19 = r12;
            r12 = r9;
            r9 = r19;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x01d6, code lost:
            if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.CommuteTimeSetting) com.fossil.DianaCustomizeEditPresenter.h(r2.this$0).a(r0.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeSetting.class)).getAddress()) != false) goto L_0x01d8;
         */
        @DexIgnore
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:104:0x02dc  */
        /* JADX WARNING: Removed duplicated region for block: B:153:0x03f6  */
        /* JADX WARNING: Removed duplicated region for block: B:157:0x0419  */
        /* JADX WARNING: Removed duplicated region for block: B:170:0x0473  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00e3  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0156 A[Catch:{ Exception -> 0x0171 }] */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0203  */
        /* JADX WARNING: Removed duplicated region for block: B:80:0x022f  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x0289  */
        public final Object invokeSuspend(Object obj) {
            String str;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
            String str2;
            b bVar;
            jl4 jl4;
            String str3;
            String str4;
            List list;
            DianaPresetComplicationSetting dianaPresetComplicationSetting;
            List list2;
            Iterator it;
            il6 il6;
            Object obj2;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
            String str5;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting3;
            Object a;
            Object obj3;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting4;
            DianaPresetComplicationSetting dianaPresetComplicationSetting2;
            List list3;
            Object obj4;
            il6 il62;
            String str6;
            il6 il63;
            DianaPresetComplicationSetting dianaPresetComplicationSetting3;
            Iterator it2;
            Object obj5;
            il6 il64;
            Object obj6;
            il6 il65;
            Parcelable parcelable;
            List list4;
            Parcelable parcelable2;
            ILocalFLogger local;
            StringBuilder sb;
            b bVar2 = this;
            Object a2 = ff6.a();
            int i = bVar2.label;
            String str7 = "weather";
            String str8 = "check setting of ";
            String str9 = "";
            if (i == 0) {
                nc6.a(obj);
                il62 = bVar2.p$;
                ArrayList<DianaPresetComplicationSetting> complications = bVar2.$it.getComplications();
                List arrayList = new ArrayList();
                for (DianaPresetComplicationSetting next : complications) {
                    if (hf6.a(yj4.b.c(next.getId())).booleanValue()) {
                        arrayList.add(next);
                    }
                }
                if (!arrayList.isEmpty()) {
                    it2 = arrayList.iterator();
                    il63 = il62;
                    list3 = arrayList;
                    dianaPresetComplicationSetting3 = null;
                    obj4 = a2;
                    bVar = bVar2;
                    if (!it2.hasNext()) {
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting2);
                    if (dianaPresetComplicationSetting2 != null) {
                    }
                } else {
                    str5 = str7;
                    str = str9;
                    list3 = arrayList;
                    dianaPresetComplicationSetting2 = null;
                    obj4 = a2;
                    bVar = this;
                    ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                    local22.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting2);
                    if (dianaPresetComplicationSetting2 != null) {
                    }
                }
            } else if (i == 1) {
                it2 = bVar2.L$4;
                DianaPresetComplicationSetting dianaPresetComplicationSetting4 = (DianaPresetComplicationSetting) bVar2.L$3;
                DianaPresetComplicationSetting dianaPresetComplicationSetting5 = (DianaPresetComplicationSetting) bVar2.L$2;
                list3 = (List) bVar2.L$1;
                il63 = (il6) bVar2.L$0;
                try {
                    nc6.a(obj);
                    obj5 = a2;
                    str5 = str7;
                    str = str9;
                    il64 = il63;
                    dianaPresetComplicationSetting2 = dianaPresetComplicationSetting4;
                    bVar = bVar2;
                    obj6 = obj;
                } catch (Exception e) {
                    e = e;
                    str5 = str7;
                    str = str9;
                    dianaPresetComplicationSetting3 = dianaPresetComplicationSetting5;
                    obj4 = a2;
                    bVar = bVar2;
                }
                try {
                    parcelable = (Parcelable) obj6;
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                } catch (Exception e2) {
                    e = e2;
                    il65 = il64;
                }
                il65 = il64;
                sb2.append("last setting ");
                sb2.append(parcelable);
                local3.d("DianaCustomizeEditPresenter", sb2.toString());
                if (parcelable == null) {
                    dianaPresetComplicationSetting2.setSettings(bVar.this$0.i.a(parcelable));
                    il63 = il65;
                    DianaPresetComplicationSetting dianaPresetComplicationSetting6 = dianaPresetComplicationSetting5;
                    obj4 = obj5;
                    dianaPresetComplicationSetting3 = dianaPresetComplicationSetting6;
                    str9 = str;
                    str7 = str5;
                    if (!it2.hasNext()) {
                        DianaPresetComplicationSetting dianaPresetComplicationSetting7 = (DianaPresetComplicationSetting) it2.next();
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        str = str9;
                        local4.d("DianaCustomizeEditPresenter", str8 + dianaPresetComplicationSetting7);
                        try {
                        } catch (Exception e3) {
                            e = e3;
                            str5 = str7;
                        }
                        if (vi4.a(dianaPresetComplicationSetting7.getSettings())) {
                            dl6 a3 = bVar.this$0.b();
                            str5 = str7;
                            d45$b$a d45_b_a = new d45$b$a(bVar, dianaPresetComplicationSetting7, (xe6) null);
                            bVar.L$0 = il63;
                            bVar.L$1 = list3;
                            bVar.L$2 = dianaPresetComplicationSetting3;
                            bVar.L$3 = dianaPresetComplicationSetting7;
                            bVar.L$4 = it2;
                            bVar.label = 1;
                            obj6 = gk6.a(a3, d45_b_a, bVar);
                            if (obj6 == obj4) {
                                return obj4;
                            }
                            il64 = il63;
                            dianaPresetComplicationSetting2 = dianaPresetComplicationSetting7;
                            Object obj7 = obj4;
                            dianaPresetComplicationSetting5 = dianaPresetComplicationSetting3;
                            obj5 = obj7;
                            parcelable = (Parcelable) obj6;
                            ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb22 = new StringBuilder();
                            il65 = il64;
                            sb22.append("last setting ");
                            sb22.append(parcelable);
                            local32.d("DianaCustomizeEditPresenter", sb22.toString());
                            if (parcelable == null) {
                                il62 = il65;
                                obj4 = obj5;
                            }
                            return obj4;
                        }
                        str5 = str7;
                        String id = dianaPresetComplicationSetting7.getId();
                        int hashCode = id.hashCode();
                        if (hashCode == -829740640) {
                            if (id.equals("commute-time")) {
                            }
                            str9 = str;
                            str7 = str5;
                            if (!it2.hasNext()) {
                                str5 = str7;
                                str = str9;
                                il62 = il63;
                                dianaPresetComplicationSetting2 = dianaPresetComplicationSetting3;
                            }
                        } else {
                            if (hashCode == 134170930) {
                                if (id.equals("second-timezone") && TextUtils.isEmpty(((SecondTimezoneSetting) bVar.this$0.i.a(dianaPresetComplicationSetting7.getSettings(), SecondTimezoneSetting.class)).getTimeZoneId())) {
                                }
                            }
                            str9 = str;
                            str7 = str5;
                            if (!it2.hasNext()) {
                            }
                        }
                        il6 il66 = il63;
                        dianaPresetComplicationSetting2 = dianaPresetComplicationSetting7;
                        il62 = il66;
                        e = e;
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        local5.e("DianaCustomizeEditPresenter", "exception when parse setting from json " + e);
                        str9 = str;
                        str7 = str5;
                        if (!it2.hasNext()) {
                        }
                    }
                    ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                    local222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting2);
                    if (dianaPresetComplicationSetting2 != null) {
                        String id2 = dianaPresetComplicationSetting2.getId();
                        int hashCode2 = id2.hashCode();
                        if (hashCode2 != -829740640) {
                            if (hashCode2 == 134170930 && id2.equals("second-timezone")) {
                                str6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886361);
                                wg6.a((Object) str6, "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)");
                                bVar.this$0.m.a(str6, dianaPresetComplicationSetting2.getId(), dianaPresetComplicationSetting2.getPosition(), true);
                                return cd6.a;
                            }
                        } else if (id2.equals("commute-time")) {
                            str6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886201);
                            wg6.a((Object) str6, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                            bVar.this$0.m.a(str6, dianaPresetComplicationSetting2.getId(), dianaPresetComplicationSetting2.getPosition(), true);
                            return cd6.a;
                        }
                        str6 = str;
                        bVar.this$0.m.a(str6, dianaPresetComplicationSetting2.getId(), dianaPresetComplicationSetting2.getPosition(), true);
                        return cd6.a;
                    }
                    ArrayList<DianaPresetWatchAppSetting> watchapps = bVar.$it.getWatchapps();
                    List arrayList2 = new ArrayList();
                    for (DianaPresetWatchAppSetting next2 : watchapps) {
                        if (hf6.a(dl4.c.f(next2.getId())).booleanValue()) {
                            arrayList2.add(next2);
                        }
                    }
                    if (!arrayList2.isEmpty()) {
                        il6 = il62;
                        it = arrayList2.iterator();
                        dianaPresetWatchAppSetting2 = null;
                        Object obj8 = obj4;
                        list2 = arrayList2;
                        obj2 = obj8;
                        DianaPresetComplicationSetting dianaPresetComplicationSetting8 = dianaPresetComplicationSetting2;
                        list = list3;
                        dianaPresetComplicationSetting = dianaPresetComplicationSetting8;
                        if (it.hasNext()) {
                            dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) it.next();
                            ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                            local6.d("DianaCustomizeEditPresenter", str8 + dianaPresetWatchAppSetting3);
                            try {
                            } catch (Exception e4) {
                                e = e4;
                                str4 = str8;
                            }
                            if (vi4.a(dianaPresetWatchAppSetting3.getSettings())) {
                                dl6 a4 = bVar.this$0.b();
                                str4 = str8;
                                d45$b$b d45_b_b = new d45$b$b(bVar, dianaPresetWatchAppSetting3, (xe6) null);
                                bVar.L$0 = il6;
                                bVar.L$1 = list;
                                bVar.L$2 = dianaPresetComplicationSetting;
                                bVar.L$3 = list2;
                                bVar.L$4 = dianaPresetWatchAppSetting2;
                                bVar.L$5 = dianaPresetWatchAppSetting3;
                                bVar.L$6 = it;
                                bVar.label = 2;
                                a = gk6.a(a4, d45_b_b, bVar);
                                if (a == obj2) {
                                    return obj2;
                                }
                                obj3 = obj2;
                                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                                bVar2 = bVar;
                                return obj2;
                            }
                            str4 = str8;
                            String id3 = dianaPresetWatchAppSetting3.getId();
                            int hashCode3 = id3.hashCode();
                            if (hashCode3 == -829740640) {
                                str2 = str5;
                                if (id3.equals("commute-time")) {
                                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting5 = dianaPresetWatchAppSetting3;
                                    if (((CommuteTimeWatchAppSetting) bVar.this$0.i.a(dianaPresetWatchAppSetting3.getSettings(), CommuteTimeWatchAppSetting.class)).getAddresses().isEmpty()) {
                                        dianaPresetWatchAppSetting = dianaPresetWatchAppSetting5;
                                    }
                                }
                            } else {
                                if (hashCode3 == 1223440372) {
                                    str2 = str5;
                                    try {
                                    } catch (Exception e5) {
                                        e = e5;
                                    }
                                    if (id3.equals(str2)) {
                                        WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) bVar.this$0.i.a(dianaPresetWatchAppSetting3.getSettings(), WeatherWatchAppSetting.class);
                                    }
                                }
                                str2 = str5;
                            }
                            str5 = str2;
                            str8 = str4;
                            if (it.hasNext()) {
                                str2 = str5;
                                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting2;
                            }
                            e = e;
                            str2 = str5;
                            ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                            local7.e("DianaCustomizeEditPresenter", "exception when parse setting from json " + e);
                            str5 = str2;
                            str8 = str4;
                            if (it.hasNext()) {
                            }
                        }
                        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                        local8.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                        if (dianaPresetWatchAppSetting != null) {
                            String id4 = dianaPresetWatchAppSetting.getId();
                            int hashCode4 = id4.hashCode();
                            if (hashCode4 != -829740640) {
                                if (hashCode4 == 1223440372 && id4.equals(str2)) {
                                    str3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886368);
                                    wg6.a((Object) str3, "LanguageHelper.getString\u2026isplayWeatherOnYourWatch)");
                                    bVar.this$0.m.a(str3, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                                    return cd6.a;
                                }
                            } else if (id4.equals("commute-time")) {
                                str3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886201);
                                wg6.a((Object) str3, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                                bVar.this$0.m.a(str3, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                                return cd6.a;
                            }
                            str3 = str;
                            bVar.this$0.m.a(str3, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                            return cd6.a;
                        }
                        bVar.this$0.m.m();
                        if (bVar.this$0.k() == 1) {
                            jl4 = AnalyticsHelper.f.b("set_complication");
                        } else {
                            jl4 = AnalyticsHelper.f.b("set_watch_apps");
                        }
                        jl4.d();
                        bVar.this$0.p.a(new SetDianaPresetToWatchUseCase.c(bVar.$currentPreset$inlined), new d45$b$c(bVar, jl4));
                        return cd6.a;
                    }
                    str2 = str5;
                    dianaPresetWatchAppSetting = null;
                    ILocalFLogger local82 = FLogger.INSTANCE.getLocal();
                    local82.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                    if (dianaPresetWatchAppSetting != null) {
                    }
                }
                il62 = il65;
                obj4 = obj5;
                ILocalFLogger local2222 = FLogger.INSTANCE.getLocal();
                local2222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting2);
                if (dianaPresetComplicationSetting2 != null) {
                }
            } else if (i == 2) {
                it = bVar2.L$6;
                dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) bVar2.L$5;
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting6 = (DianaPresetWatchAppSetting) bVar2.L$4;
                list2 = (List) bVar2.L$3;
                dianaPresetComplicationSetting = (DianaPresetComplicationSetting) bVar2.L$2;
                list = (List) bVar2.L$1;
                il6 = (il6) bVar2.L$0;
                try {
                    nc6.a(obj);
                    str5 = str7;
                    str4 = str8;
                    dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting6;
                    str = str9;
                    a = obj;
                    obj3 = a2;
                } catch (Exception e6) {
                    e = e6;
                    str4 = str8;
                    str = str9;
                    Object obj9 = a2;
                    bVar = bVar2;
                    dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting6;
                    str2 = str7;
                    obj2 = obj9;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list5 = list;
            dianaPresetWatchAppSetting = dianaPresetWatchAppSetting3;
            try {
                parcelable2 = (Parcelable) a;
                local = FLogger.INSTANCE.getLocal();
                sb = new StringBuilder();
            } catch (Exception e7) {
                e = e7;
                list4 = list5;
            }
            list4 = list5;
            sb.append("last setting ");
            sb.append(parcelable2);
            local.d("DianaCustomizeEditPresenter", sb.toString());
            if (parcelable2 != null) {
                dianaPresetWatchAppSetting.setSettings(bVar2.this$0.i.a(parcelable2));
                list = list4;
                bVar = bVar2;
                dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                obj2 = obj3;
                str2 = str5;
                str5 = str2;
                str8 = str4;
                if (it.hasNext()) {
                }
                ILocalFLogger local822 = FLogger.INSTANCE.getLocal();
                local822.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                if (dianaPresetWatchAppSetting != null) {
                }
            }
            bVar = bVar2;
            str2 = str5;
            ILocalFLogger local8222 = FLogger.INSTANCE.getLocal();
            local8222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
            if (dianaPresetWatchAppSetting != null) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public c(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(DianaPreset dianaPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "start - observe current preset value=" + dianaPreset);
            DianaCustomizeEditPresenter dianaCustomizeEditPresenter = this.a;
            dianaCustomizeEditPresenter.j = dianaCustomizeEditPresenter.n.getCurrentUser();
            MutableLiveData b = this.a.f;
            if (dianaPreset != null) {
                b.a(dianaPreset.clone());
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public d(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "start - observe selected watchApp value=" + str);
            z35 k = this.a.m;
            if (str != null) {
                k.u(str);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public e(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "observe selected complication pos=" + str);
            z35 k = this.a.m;
            if (str != null) {
                k.s(str);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<m35> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public f(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(m35 m35) {
            if (m35 != null) {
                this.a.m.a(m35, DianaCustomizeEditPresenter.g(this.a).c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public g(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                WatchFaceWrapper e = DianaCustomizeEditPresenter.g(this.a).e(str);
                this.a.k = e;
                this.a.m.a(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public h(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "update preset status isChanged=" + bool);
            z35 k = this.a.m;
            if (bool != null) {
                k.e(bool.booleanValue());
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter a;

        @DexIgnore
        public i(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            this.a = dianaCustomizeEditPresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r5v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public final MutableLiveData<m35> apply(DianaPreset dianaPreset) {
            String str;
            String str2;
            DianaPreset dianaPreset2 = dianaPreset;
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(dianaPreset.getComplications());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(dianaPreset.getWatchapps());
            WatchFaceWrapper e = DianaCustomizeEditPresenter.g(this.a).e(dianaPreset.getWatchFaceId());
            ArrayList arrayList3 = new ArrayList();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations presetChanged complications=" + arrayList + " watchapps=" + arrayList2 + " backgroundWrapper=" + e);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) it.next();
                String component1 = dianaPresetComplicationSetting.component1();
                String component2 = dianaPresetComplicationSetting.component2();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations find complicationId=" + component2);
                Complication b = DianaCustomizeEditPresenter.g(this.a).b(component2);
                if (b != null) {
                    String complicationId = b.getComplicationId();
                    String icon = b.getIcon();
                    if (icon != null) {
                        str2 = icon;
                    } else {
                        str2 = "";
                    }
                    String a2 = jm4.a(PortfolioApp.get.instance(), b.getNameKey(), b.getName());
                    om4 f = this.a.q;
                    MFUser c = this.a.j;
                    ArrayList e2 = this.a.h;
                    String complicationId2 = b.getComplicationId();
                    wg6.a((Object) dianaPreset2, "preset");
                    arrayList3.add(new o35(complicationId, str2, a2, component1, f.a(c, e2, complicationId2, dianaPreset2)));
                }
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations presetChanged complicationsDetails=" + arrayList3);
            ArrayList arrayList4 = new ArrayList();
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) it2.next();
                String component12 = dianaPresetWatchAppSetting.component1();
                WatchApp d = DianaCustomizeEditPresenter.g(this.a).d(dianaPresetWatchAppSetting.component2());
                if (d != null) {
                    String watchappId = d.getWatchappId();
                    String icon2 = d.getIcon();
                    if (icon2 != null) {
                        str = icon2;
                    } else {
                        str = "";
                    }
                    arrayList4.add(new o35(watchappId, str, jm4.a(PortfolioApp.get.instance(), d.getNameKey(), d.getName()), component12, (String) null, 16, (qg6) null));
                }
            }
            m35 m35 = (m35) this.a.g.a();
            if (m35 != null) {
                this.a.a((List<o35>) m35.a(), (List<o35>) arrayList3);
                this.a.b((List<o35>) m35.e(), (List<o35>) arrayList4);
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DianaCustomizeEditPresenter", "wrapperPresetTransformations presetChanged watchAppsDetail=" + arrayList4);
            this.a.g.a(new m35(dianaPreset.getId(), dianaPreset.getName(), arrayList3, arrayList4, dianaPreset.isActive(), e));
            return this.a.g;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DianaCustomizeEditPresenter(z35 z35, UserRepository userRepository, int i2, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, om4 om4, an4 an4) {
        wg6.b(z35, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(setDianaPresetToWatchUseCase, "mSetDianaPresetToWatchUseCase");
        wg6.b(om4, "mCustomizeRealDataManager");
        wg6.b(an4, "mSharedPreferencesManager");
        this.m = z35;
        this.n = userRepository;
        this.o = i2;
        this.p = setDianaPresetToWatchUseCase;
        this.q = om4;
        this.r = an4;
        LiveData<m35> b2 = sd.b(this.f, new i(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.l = b2;
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel g(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = dianaCustomizeEditPresenter.e;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void l() {
        DianaPreset dianaPreset = (DianaPreset) this.f.a();
        if (dianaPreset != null) {
            DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
            if (dianaCustomizeViewModel != null) {
                DianaPreset d2 = dianaCustomizeViewModel.d();
                if (d2 != null && (!wg6.a((Object) dianaPreset.getWatchFaceId(), (Object) d2.getWatchFaceId()))) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditPresenter", "Watch face changed from " + d2.getWatchFaceId() + " to " + dianaPreset.getWatchFaceId());
                    AnalyticsHelper.f.c().a("set_background_manually");
                    return;
                }
                return;
            }
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        this.m.a(this);
    }

    @DexIgnore
    public void c(String str, String str2) {
        T t;
        wg6.b(str, "fromPosition");
        wg6.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapComplication - fromPosition=" + str + ", toPosition=" + str2);
        if (!wg6.a((Object) str, (Object) str2)) {
            DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
            T t2 = null;
            if (dianaCustomizeViewModel != null) {
                DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
                if (dianaPreset != null) {
                    DianaPreset clone = dianaPreset.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (wg6.a((Object) ((DianaPresetComplicationSetting) t).getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                    Iterator<T> it2 = clone.getComplications().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (wg6.a((Object) ((DianaPresetComplicationSetting) next).getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting2 = (DianaPresetComplicationSetting) t2;
                    if (!(dianaPresetComplicationSetting == null || dianaPresetComplicationSetting2 == null)) {
                        dianaPresetComplicationSetting.setPosition(str2);
                        dianaPresetComplicationSetting2.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapComplication - update preset " + clone);
                    a(clone);
                    return;
                }
                return;
            }
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void d(String str, String str2) {
        T t;
        wg6.b(str, "fromPosition");
        wg6.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapWatchApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!wg6.a((Object) str, (Object) str2)) {
            DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
            T t2 = null;
            if (dianaCustomizeViewModel != null) {
                DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
                if (dianaPreset != null) {
                    Iterator<T> it = dianaPreset.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (wg6.a((Object) ((DianaPresetWatchAppSetting) t).getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                    Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (wg6.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t2;
                    if (dianaPresetWatchAppSetting != null) {
                        dianaPresetWatchAppSetting.setPosition(str2);
                    }
                    if (dianaPresetWatchAppSetting2 != null) {
                        dianaPresetWatchAppSetting2.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapWatchApp - update preset");
                    wg6.a((Object) dianaPreset, "currentPreset");
                    a(dianaPreset);
                    return;
                }
                return;
            }
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        this.m.g(this.o);
        this.p.f();
        BleCommandResultManager.d.a(CommunicateMode.SET_COMPLICATION_APPS, CommunicateMode.SET_WATCH_APPS, CommunicateMode.SET_PRESET_APPS_DATA);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<DianaPreset> a2 = dianaCustomizeViewModel.a();
            z35 z35 = this.m;
            if (z35 != null) {
                a2.a((DianaCustomizeEditFragment) z35, new c(this));
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.i().a(this.m, new d(this));
                    DianaCustomizeViewModel dianaCustomizeViewModel3 = this.e;
                    if (dianaCustomizeViewModel3 != null) {
                        dianaCustomizeViewModel3.f().a(this.m, new e(this));
                        this.l.a(this.m, new f(this));
                        DianaCustomizeViewModel dianaCustomizeViewModel4 = this.e;
                        if (dianaCustomizeViewModel4 != null) {
                            dianaCustomizeViewModel4.g().a(this.m, new g(this));
                            DianaCustomizeViewModel dianaCustomizeViewModel5 = this.e;
                            if (dianaCustomizeViewModel5 != null) {
                                dianaCustomizeViewModel5.b().a(this.m, new h(this));
                            } else {
                                wg6.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                        } else {
                            wg6.d("mDianaCustomizeViewModel");
                            throw null;
                        }
                    } else {
                        wg6.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
            }
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        this.p.g();
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<DianaPreset> a2 = dianaCustomizeViewModel.a();
            z35 z35 = this.m;
            if (z35 != null) {
                a2.a((DianaCustomizeEditFragment) z35);
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.i().a(this.m);
                    DianaCustomizeViewModel dianaCustomizeViewModel3 = this.e;
                    if (dianaCustomizeViewModel3 != null) {
                        dianaCustomizeViewModel3.f().a(this.m);
                        this.g.a(this.m);
                        return;
                    }
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
                wg6.d("mDianaCustomizeViewModel");
                throw null;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            boolean l2 = dianaCustomizeViewModel.l();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "isPresetChanged " + l2);
            if (l2) {
                this.m.p();
            } else {
                this.m.f(false);
            }
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.m.a(this.k);
    }

    @DexIgnore
    public final ArrayList<o35> j() {
        m35 m35 = (m35) this.l.a();
        if (m35 != null) {
            return m35.a();
        }
        return null;
    }

    @DexIgnore
    public final int k() {
        return this.o;
    }

    @DexIgnore
    public final void b(int i2) {
        this.o = i2;
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "watchAppPos");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.k(str);
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        wg6.b(dianaCustomizeViewModel, "viewModel");
        this.e = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void b(String str, String str2) {
        wg6.b(str, "watchAppId");
        wg6.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "dropWatchApp - watchAppId=" + str + ", toPosition=" + str2);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        T t = null;
        if (dianaCustomizeViewModel != null) {
            if (!dianaCustomizeViewModel.i(str)) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel2.a().a();
                    if (dianaPreset != null) {
                        Iterator<T> it = dianaPreset.getWatchapps().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            T next = it.next();
                            if (wg6.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) str2)) {
                                t = next;
                                break;
                            }
                        }
                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                        if (dianaPresetWatchAppSetting != null) {
                            dianaPresetWatchAppSetting.setId(str);
                        }
                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "dropWatchApp - update preset");
                        wg6.a((Object) dianaPreset, "currentPreset");
                        a(dianaPreset);
                    }
                } else {
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
            if (str.hashCode() == -829740640 && str.equals("commute-time") && !this.r.h()) {
                this.r.o(true);
                this.m.d("commute-time");
                return;
            }
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(int i2) {
        this.o = i2;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "complicationPos");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.j(str);
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<CustomizeRealData> arrayList) {
        wg6.b(arrayList, "customizeRealDataList");
        this.h = arrayList;
    }

    @DexIgnore
    public void a(String str, String str2) {
        wg6.b(str, "complicationId");
        wg6.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "dropComplication - complicationId=" + str + ", toPosition=" + str2);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        T t = null;
        if (dianaCustomizeViewModel == null) {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        } else if (!dianaCustomizeViewModel.h(str)) {
            DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
            if (dianaCustomizeViewModel2 != null) {
                DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel2.a().a();
                if (dianaPreset != null) {
                    DianaPreset clone = dianaPreset.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        if (wg6.a((Object) ((DianaPresetComplicationSetting) next).getPosition(), (Object) str2)) {
                            t = next;
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                    if (dianaPresetComplicationSetting != null) {
                        dianaPresetComplicationSetting.setId(str);
                        dianaPresetComplicationSetting.setSettings("");
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DianaCustomizeEditPresenter", "dropComplication - newPreset=" + clone);
                    a(clone);
                    return;
                }
                return;
            }
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void b(List<o35> list, List<o35> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                z = true;
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!wg6.a((Object) list.get(i2).a(), (Object) list2.get(i2).a())) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process watch app list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                o35 o35 = list.get(i3);
                o35 o352 = list2.get(i3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "Process watch app list, item at " + i3 + " position: " + o35.c() + ", oldId: " + o35.a() + ", newId: " + o352.a());
                String c2 = o352.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && c2.equals("top")) {
                            b("top", o35.a(), o352.a());
                        }
                    } else if (c2.equals("middle")) {
                        b("middle", o35.a(), o352.a());
                    }
                }
                b("bottom", o35.a(), o352.a());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process watch app list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public void a(boolean z) {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "setPresetToWatch currentPreset=" + dianaPreset);
            Set<Integer> a2 = xm4.d.a(dianaPreset);
            if (z) {
                this.r.q(true);
                this.r.r(true);
            }
            if (this.o != 0) {
                xm4 xm4 = xm4.d;
                z35 z35 = this.m;
                if (z35 == null) {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
                } else if (!xm4.a(((DianaCustomizeEditFragment) z35).getContext(), a2)) {
                    return;
                }
            } else {
                ArrayList a3 = qd6.a((T[]) new uh4[]{uh4.BLUETOOTH_OFF});
                xm4 xm42 = xm4.d;
                z35 z352 = this.m;
                if (z352 == null) {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
                } else if (!xm42.a(((DianaCustomizeEditFragment) z352).getContext(), (ArrayList<uh4>) a3)) {
                    return;
                }
            }
            if (dianaPreset != null) {
                rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(dianaPreset, (xe6) null, this, dianaPreset), 3, (Object) null);
                return;
            }
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "updateCurrentPreset=" + dianaPreset);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.a(dianaPreset);
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putInt("KEY_CUSTOMIZE_TAB", this.o);
        }
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
            if (!(dianaPreset == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", new Gson().a(dianaPreset));
            }
            DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
            if (dianaCustomizeViewModel2 != null) {
                DianaPreset d2 = dianaCustomizeViewModel2.d();
                if (!(d2 == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", new Gson().a(d2));
                }
                DianaCustomizeViewModel dianaCustomizeViewModel3 = this.e;
                if (dianaCustomizeViewModel3 != null) {
                    String str = (String) dianaCustomizeViewModel3.f().a();
                    if (!(str == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_COMPLICATION_POS_SELECTED", str);
                    }
                    DianaCustomizeViewModel dianaCustomizeViewModel4 = this.e;
                    if (dianaCustomizeViewModel4 != null) {
                        String str2 = (String) dianaCustomizeViewModel4.i().a();
                        if (!(str2 == null || bundle == null)) {
                            bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
                        }
                        DianaCustomizeViewModel dianaCustomizeViewModel5 = this.e;
                        if (dianaCustomizeViewModel5 != null) {
                            String str3 = (String) dianaCustomizeViewModel5.i().a();
                            if (!(str3 == null || bundle == null)) {
                                bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
                            }
                            return bundle;
                        }
                        wg6.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    wg6.d("mDianaCustomizeViewModel");
                    throw null;
                }
                wg6.d("mDianaCustomizeViewModel");
                throw null;
            }
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void b(String str, String str2, String str3) {
        hl4 a2 = AnalyticsHelper.f.a("set_watch_apps_manually");
        a2.a("button", str);
        a2.a("old_app", str2);
        a2.a("new_app", str3);
        a2.a();
    }

    @DexIgnore
    public final void a(List<o35> list, List<o35> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                z = true;
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!wg6.a((Object) list.get(i2).a(), (Object) list2.get(i2).a())) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process complication list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                o35 o35 = list.get(i3);
                o35 o352 = list2.get(i3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "Process complication list, item at " + i3 + " position: " + o35.c() + ", oldId: " + o35.a() + ", newId: " + o352.a());
                String c2 = o352.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != 115029) {
                            if (hashCode == 3317767 && c2.equals("left")) {
                                a("left", o35.a(), o352.a());
                            }
                        } else if (c2.equals("top")) {
                            a("top", o35.a(), o352.a());
                        }
                    } else if (c2.equals("bottom")) {
                        a("bottom", o35.a(), o352.a());
                    }
                }
                a("right", o35.a(), o352.a());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process complication list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        hl4 a2 = AnalyticsHelper.f.a("set_complication_manually");
        a2.a("view", str);
        a2.a("old_complication", str2);
        a2.a("new_complication", str3);
        a2.a();
    }
}
