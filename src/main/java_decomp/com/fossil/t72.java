package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t72 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<t72> CREATOR; // = new s72();
    @DexIgnore
    public static /* final */ t72 b; // = new t72("com.google.android.gms");
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public t72(String str) {
        w12.a(str);
        this.a = str;
    }

    @DexIgnore
    public static t72 e(String str) {
        if ("com.google.android.gms".equals(str)) {
            return b;
        }
        return new t72(str);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof t72)) {
            return false;
        }
        return this.a.equals(((t72) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String p() {
        return this.a;
    }

    @DexIgnore
    public final String toString() {
        return String.format("Application{%s}", new Object[]{this.a});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a, false);
        g22.a(parcel, a2);
    }
}
