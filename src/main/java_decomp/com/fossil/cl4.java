package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl4 {
    @DexIgnore
    public static final void a(View view, View.OnClickListener onClickListener) {
        wg6.b(view, "$this$setOnSingleClickListener");
        if (onClickListener != null) {
            view.setOnClickListener(new uk4(onClickListener));
            if (onClickListener != null) {
                return;
            }
        }
        view.setOnClickListener((View.OnClickListener) null);
        cd6 cd6 = cd6.a;
    }
}
