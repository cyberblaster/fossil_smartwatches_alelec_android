package com.fossil;

import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class a15$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DNDScheduledTimeModel a;
        @DexIgnore
        public /* final */ /* synthetic */ a15$b$a b;

        @DexIgnore
        public a(DNDScheduledTimeModel dNDScheduledTimeModel, a15$b$a a15_b_a) {
            this.a = dNDScheduledTimeModel;
            this.b = a15_b_a;
        }

        @DexIgnore
        public final void run() {
            this.b.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DNDScheduledTimeModel a;
        @DexIgnore
        public /* final */ /* synthetic */ a15$b$a b;

        @DexIgnore
        public b(DNDScheduledTimeModel dNDScheduledTimeModel, a15$b$a a15_b_a) {
            this.a = dNDScheduledTimeModel;
            this.b = a15_b_a;
        }

        @DexIgnore
        public final void run() {
            this.b.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.a);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a15$b$a(DoNotDisturbScheduledTimePresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        a15$b$a a15_b_a = new a15$b$a(this.this$0, xe6);
        a15_b_a.p$ = (il6) obj;
        return a15_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((a15$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            if (this.this$0.this$0.f == 0) {
                DNDScheduledTimeModel c = this.this$0.this$0.g;
                if (c == null) {
                    return null;
                }
                c.setMinutes(this.this$0.this$0.e);
                this.this$0.this$0.j.runInTransaction(new a(c, this));
                return cd6.a;
            }
            DNDScheduledTimeModel b2 = this.this$0.this$0.h;
            if (b2 == null) {
                return null;
            }
            b2.setMinutes(this.this$0.this$0.e);
            this.this$0.this$0.j.runInTransaction(new b(b2, this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
