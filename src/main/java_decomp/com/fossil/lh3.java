package com.fossil;

import androidx.fragment.app.Fragment;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lh3<S> extends Fragment {
    @DexIgnore
    public /* final */ LinkedHashSet<kh3<S>> a; // = new LinkedHashSet<>();

    @DexIgnore
    public boolean a(kh3<S> kh3) {
        return this.a.add(kh3);
    }

    @DexIgnore
    public void d1() {
        this.a.clear();
    }
}
