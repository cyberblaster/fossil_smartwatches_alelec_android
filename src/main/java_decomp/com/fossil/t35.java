package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t35 implements Factory<m75> {
    @DexIgnore
    public static m75 a(q35 q35) {
        m75 c = q35.c();
        z76.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
