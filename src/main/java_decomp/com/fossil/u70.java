package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u70 extends r70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new u70(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new u70[i];
        }
    }

    @DexIgnore
    public u70() {
        super(t70.DISMISS_NOTIFICATION);
    }

    @DexIgnore
    public /* synthetic */ u70(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
