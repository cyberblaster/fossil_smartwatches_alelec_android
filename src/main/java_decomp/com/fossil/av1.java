package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.wv1;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av1 extends rv1.a<bu1, GoogleSignInOptions> {
    @DexIgnore
    public final /* synthetic */ rv1.f a(Context context, Looper looper, e12 e12, Object obj, wv1.b bVar, wv1.c cVar) {
        return new bu1(context, looper, e12, (GoogleSignInOptions) obj, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ List a(Object obj) {
        GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions) obj;
        if (googleSignInOptions == null) {
            return Collections.emptyList();
        }
        return googleSignInOptions.D();
    }
}
