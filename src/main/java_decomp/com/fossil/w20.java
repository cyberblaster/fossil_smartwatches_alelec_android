package com.fossil;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w20 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ ab6 b;

    @DexIgnore
    public w20(String str, ab6 ab6) {
        this.a = str;
        this.b = ab6;
    }

    @DexIgnore
    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            l86 g = c86.g();
            g.e("CrashlyticsCore", "Error creating marker: " + this.a, e);
            return false;
        }
    }

    @DexIgnore
    public final File b() {
        return new File(this.b.a(), this.a);
    }

    @DexIgnore
    public boolean c() {
        return b().exists();
    }

    @DexIgnore
    public boolean d() {
        return b().delete();
    }
}
