package com.fossil;

import androidx.lifecycle.LiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk5$e$a<T> implements ld<yx5<? extends Integer>> {
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter.e a;
    @DexIgnore
    public /* final */ /* synthetic */ LiveData b;

    @DexIgnore
    public wk5$e$a(ProfileGoalEditPresenter.e eVar, LiveData liveData) {
        this.a = eVar;
        this.b = liveData;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<Integer> yx5) {
        FLogger.INSTANCE.getLocal().d(ProfileGoalEditPresenter.t, "saveSleepGoal observe");
        if ((yx5 != null ? yx5.f() : null) != wh4.SUCCESS || yx5.d() == null) {
            if ((yx5 != null ? yx5.f() : null) == wh4.ERROR) {
                sk5 m = this.a.this$0.m();
                Integer c = yx5.c();
                if (c != null) {
                    int intValue = c.intValue();
                    String e = yx5.e();
                    if (e != null) {
                        m.d(intValue, e);
                        this.b.a(this.a.this$0.m());
                        this.a.this$0.o();
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        this.a.this$0.p();
        this.b.a(this.a.this$0.m());
    }
}
