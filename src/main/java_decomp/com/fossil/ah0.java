package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.TimeZone;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ jf0 CREATOR; // = new jf0((qg6) null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ int c; // = ((TimeZone.getDefault().getOffset(this.d) / 1000) / 60);
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public ah0(long j) {
        this.d = j;
        long j2 = this.d;
        long j3 = (long) 1000;
        this.a = j2 / j3;
        this.b = j2 - (this.a * j3);
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.SECOND, (Object) Long.valueOf(this.a)), bm0.MILLISECOND, (Object) Long.valueOf(this.b)), bm0.TIMEZONE_OFFSET_IN_MINUTE, (Object) Integer.valueOf(this.c));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.d);
    }
}
