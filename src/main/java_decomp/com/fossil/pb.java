package com.fossil;

import android.content.res.AssetManager;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.facebook.share.internal.VideoUploader;
import com.facebook.stetho.dumpapp.Framer;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.CRC32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pb {
    @DexIgnore
    public static /* final */ byte[] A; // = {-119, 80, 78, 71, 13, 10, 26, 10};
    @DexIgnore
    public static /* final */ byte[] B; // = {101, 88, 73, 102};
    @DexIgnore
    public static /* final */ byte[] C; // = {73, 72, 68, 82};
    @DexIgnore
    public static /* final */ byte[] D; // = {73, 69, 78, 68};
    @DexIgnore
    public static /* final */ byte[] E; // = {82, 73, 70, 70};
    @DexIgnore
    public static /* final */ byte[] F; // = {87, 69, 66, 80};
    @DexIgnore
    public static /* final */ byte[] G; // = {69, 88, 73, 70};
    @DexIgnore
    public static SimpleDateFormat H; // = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
    @DexIgnore
    public static /* final */ String[] I; // = {"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD"};
    @DexIgnore
    public static /* final */ int[] J; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
    @DexIgnore
    public static /* final */ byte[] K; // = {65, 83, 67, 73, 73, 0, 0, 0};
    @DexIgnore
    public static /* final */ d[] L; // = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ImageWidth", 256, 3, 4), new d("ImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("Orientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", 301, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", 531, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("SensorTopBorder", 4, 4), new d("SensorLeftBorder", 5, 4), new d("SensorBottomBorder", 6, 4), new d("SensorRightBorder", 7, 4), new d("ISO", 23, 3), new d("JpgFromRaw", 46, 7), new d("Xmp", 700, 1)};
    @DexIgnore
    public static /* final */ d[] M; // = {new d("ExposureTime", 33434, 5), new d("FNumber", 33437, 5), new d("ExposureProgram", 34850, 3), new d("SpectralSensitivity", 34852, 2), new d("PhotographicSensitivity", 34855, 3), new d("OECF", 34856, 7), new d("SensitivityType", 34864, 3), new d("StandardOutputSensitivity", 34865, 4), new d("RecommendedExposureIndex", 34866, 4), new d("ISOSpeed", 34867, 4), new d("ISOSpeedLatitudeyyy", 34868, 4), new d("ISOSpeedLatitudezzz", 34869, 4), new d("ExifVersion", 36864, 2), new d("DateTimeOriginal", 36867, 2), new d("DateTimeDigitized", 36868, 2), new d("OffsetTime", 36880, 2), new d("OffsetTimeOriginal", 36881, 2), new d("OffsetTimeDigitized", 36882, 2), new d("ComponentsConfiguration", 37121, 7), new d("CompressedBitsPerPixel", 37122, 5), new d("ShutterSpeedValue", 37377, 10), new d("ApertureValue", 37378, 5), new d("BrightnessValue", 37379, 10), new d("ExposureBiasValue", 37380, 10), new d("MaxApertureValue", 37381, 5), new d("SubjectDistance", 37382, 5), new d("MeteringMode", 37383, 3), new d("LightSource", 37384, 3), new d("Flash", 37385, 3), new d("FocalLength", 37386, 5), new d("SubjectArea", 37396, 3), new d("MakerNote", 37500, 7), new d("UserComment", 37510, 7), new d("SubSecTime", 37520, 2), new d("SubSecTimeOriginal", 37521, 2), new d("SubSecTimeDigitized", 37522, 2), new d("FlashpixVersion", 40960, 7), new d("ColorSpace", 40961, 3), new d("PixelXDimension", 40962, 3, 4), new d("PixelYDimension", 40963, 3, 4), new d("RelatedSoundFile", 40964, 2), new d("InteroperabilityIFDPointer", 40965, 4), new d("FlashEnergy", 41483, 5), new d("SpatialFrequencyResponse", 41484, 7), new d("FocalPlaneXResolution", 41486, 5), new d("FocalPlaneYResolution", 41487, 5), new d("FocalPlaneResolutionUnit", 41488, 3), new d("SubjectLocation", 41492, 3), new d("ExposureIndex", 41493, 5), new d("SensingMethod", 41495, 3), new d("FileSource", 41728, 7), new d("SceneType", 41729, 7), new d("CFAPattern", 41730, 7), new d("CustomRendered", 41985, 3), new d("ExposureMode", 41986, 3), new d("WhiteBalance", 41987, 3), new d("DigitalZoomRatio", 41988, 5), new d("FocalLengthIn35mmFilm", 41989, 3), new d("SceneCaptureType", 41990, 3), new d("GainControl", 41991, 3), new d("Contrast", 41992, 3), new d("Saturation", 41993, 3), new d("Sharpness", 41994, 3), new d("DeviceSettingDescription", 41995, 7), new d("SubjectDistanceRange", 41996, 3), new d("ImageUniqueID", 42016, 2), new d("CameraOwnerName", 42032, 2), new d("BodySerialNumber", 42033, 2), new d("LensSpecification", 42034, 5), new d("LensMake", 42035, 2), new d("LensModel", 42036, 2), new d("Gamma", 42240, 5), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
    @DexIgnore
    public static /* final */ d[] N; // = {new d("GPSVersionID", 0, 1), new d("GPSLatitudeRef", 1, 2), new d("GPSLatitude", 2, 5), new d("GPSLongitudeRef", 3, 2), new d("GPSLongitude", 4, 5), new d("GPSAltitudeRef", 5, 1), new d("GPSAltitude", 6, 5), new d("GPSTimeStamp", 7, 5), new d("GPSSatellites", 8, 2), new d("GPSStatus", 9, 2), new d("GPSMeasureMode", 10, 2), new d("GPSDOP", 11, 5), new d("GPSSpeedRef", 12, 2), new d("GPSSpeed", 13, 5), new d("GPSTrackRef", 14, 2), new d("GPSTrack", 15, 5), new d("GPSImgDirectionRef", 16, 2), new d("GPSImgDirection", 17, 5), new d("GPSMapDatum", 18, 2), new d("GPSDestLatitudeRef", 19, 2), new d("GPSDestLatitude", 20, 5), new d("GPSDestLongitudeRef", 21, 2), new d("GPSDestLongitude", 22, 5), new d("GPSDestBearingRef", 23, 2), new d("GPSDestBearing", 24, 5), new d("GPSDestDistanceRef", 25, 2), new d("GPSDestDistance", 26, 5), new d("GPSProcessingMethod", 27, 7), new d("GPSAreaInformation", 28, 7), new d("GPSDateStamp", 29, 2), new d("GPSDifferential", 30, 3), new d("GPSHPositioningError", 31, 5)};
    @DexIgnore
    public static /* final */ d[] O; // = {new d("InteroperabilityIndex", 1, 2)};
    @DexIgnore
    public static /* final */ d[] P; // = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ThumbnailImageWidth", 256, 3, 4), new d("ThumbnailImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("ThumbnailOrientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", 301, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", 531, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
    @DexIgnore
    public static /* final */ d Q; // = new d("StripOffsets", 273, 3);
    @DexIgnore
    public static /* final */ d[] R; // = {new d("ThumbnailImage", 256, 7), new d("CameraSettingsIFDPointer", 8224, 4), new d("ImageProcessingIFDPointer", 8256, 4)};
    @DexIgnore
    public static /* final */ d[] S; // = {new d("PreviewImageStart", 257, 4), new d("PreviewImageLength", 258, 4)};
    @DexIgnore
    public static /* final */ d[] T; // = {new d("AspectFrame", 4371, 3)};
    @DexIgnore
    public static /* final */ d[] U; // = {new d("ColorSpace", 55, 3)};
    @DexIgnore
    public static /* final */ d[][] V;
    @DexIgnore
    public static /* final */ d[] W; // = {new d("SubIFDPointer", 330, 4), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("InteroperabilityIFDPointer", 40965, 4), new d("CameraSettingsIFDPointer", 8224, 1), new d("ImageProcessingIFDPointer", 8256, 1)};
    @DexIgnore
    public static /* final */ HashMap<Integer, d>[] X;
    @DexIgnore
    public static /* final */ HashMap<String, d>[] Y;
    @DexIgnore
    public static /* final */ HashSet<String> Z; // = new HashSet<>(Arrays.asList(new String[]{"FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"}));
    @DexIgnore
    public static /* final */ HashMap<Integer, Integer> a0; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Charset b0; // = Charset.forName("US-ASCII");
    @DexIgnore
    public static /* final */ byte[] c0; // = "Exif\u0000\u0000".getBytes(b0);
    @DexIgnore
    public static /* final */ byte[] d0; // = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(b0);
    @DexIgnore
    public static /* final */ boolean r; // = Log.isLoggable("ExifInterface", 3);
    @DexIgnore
    public static /* final */ int[] s; // = {8, 8, 8};
    @DexIgnore
    public static /* final */ int[] t; // = {8};
    @DexIgnore
    public static /* final */ byte[] u; // = {-1, -40, -1};
    @DexIgnore
    public static /* final */ byte[] v; // = {102, 116, 121, 112};
    @DexIgnore
    public static /* final */ byte[] w; // = {109, 105, 102, Framer.STDOUT_FRAME_PREFIX};
    @DexIgnore
    public static /* final */ byte[] x; // = {104, 101, 105, 99};
    @DexIgnore
    public static /* final */ byte[] y; // = {79, 76, 89, 77, 80, 0};
    @DexIgnore
    public static /* final */ byte[] z; // = {79, 76, 89, 77, 80, 85, 83, 0, 73, 73};
    @DexIgnore
    public String a;
    @DexIgnore
    public FileDescriptor b;
    @DexIgnore
    public AssetManager.AssetInputStream c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ HashMap<String, c>[] f;
    @DexIgnore
    public Set<Integer> g;
    @DexIgnore
    public ByteOrder h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends MediaDataSource {
        @DexIgnore
        public long a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;

        @DexIgnore
        public a(pb pbVar, b bVar) {
            this.b = bVar;
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public long getSize() throws IOException {
            return -1;
        }

        @DexIgnore
        public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
            if (i2 == 0) {
                return 0;
            }
            if (j < 0) {
                return -1;
            }
            try {
                if (this.a != j) {
                    if (this.a >= 0 && j >= this.a + ((long) this.b.available())) {
                        return -1;
                    }
                    this.b.a(j);
                    this.a = j;
                }
                if (i2 > this.b.available()) {
                    i2 = this.b.available();
                }
                int read = this.b.read(bArr, i, i2);
                if (read >= 0) {
                    this.a += (long) read;
                    return read;
                }
            } catch (IOException unused) {
            }
            this.a = -1;
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends InputStream implements DataInput {
        @DexIgnore
        public static /* final */ ByteOrder e; // = ByteOrder.LITTLE_ENDIAN;
        @DexIgnore
        public static /* final */ ByteOrder f; // = ByteOrder.BIG_ENDIAN;
        @DexIgnore
        public DataInputStream a;
        @DexIgnore
        public ByteOrder b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public b(InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        public void a(ByteOrder byteOrder) {
            this.b = byteOrder;
        }

        @DexIgnore
        public int available() throws IOException {
            return this.a.available();
        }

        @DexIgnore
        public int k() {
            return this.c;
        }

        @DexIgnore
        public int l() {
            return this.d;
        }

        @DexIgnore
        public long m() throws IOException {
            return ((long) readInt()) & 4294967295L;
        }

        @DexIgnore
        public int read() throws IOException {
            this.d++;
            return this.a.read();
        }

        @DexIgnore
        public boolean readBoolean() throws IOException {
            this.d++;
            return this.a.readBoolean();
        }

        @DexIgnore
        public byte readByte() throws IOException {
            this.d++;
            if (this.d <= this.c) {
                int read = this.a.read();
                if (read >= 0) {
                    return (byte) read;
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        public char readChar() throws IOException {
            this.d += 2;
            return this.a.readChar();
        }

        @DexIgnore
        public double readDouble() throws IOException {
            return Double.longBitsToDouble(readLong());
        }

        @DexIgnore
        public float readFloat() throws IOException {
            return Float.intBitsToFloat(readInt());
        }

        @DexIgnore
        public void readFully(byte[] bArr, int i, int i2) throws IOException {
            this.d += i2;
            if (this.d > this.c) {
                throw new EOFException();
            } else if (this.a.read(bArr, i, i2) != i2) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        @DexIgnore
        public int readInt() throws IOException {
            this.d += 4;
            if (this.d <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                int read3 = this.a.read();
                int read4 = this.a.read();
                if ((read | read2 | read3 | read4) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
                    }
                    if (byteOrder == f) {
                        return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        public String readLine() throws IOException {
            Log.d("ExifInterface", "Currently unsupported");
            return null;
        }

        @DexIgnore
        public long readLong() throws IOException {
            this.d += 8;
            if (this.d <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                int read3 = this.a.read();
                int read4 = this.a.read();
                int read5 = this.a.read();
                int read6 = this.a.read();
                int read7 = this.a.read();
                int read8 = this.a.read();
                if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (((long) read8) << 56) + (((long) read7) << 48) + (((long) read6) << 40) + (((long) read5) << 32) + (((long) read4) << 24) + (((long) read3) << 16) + (((long) read2) << 8) + ((long) read);
                    }
                    int i = read2;
                    if (byteOrder == f) {
                        return (((long) read) << 56) + (((long) i) << 48) + (((long) read3) << 40) + (((long) read4) << 32) + (((long) read5) << 24) + (((long) read6) << 16) + (((long) read7) << 8) + ((long) read8);
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        public short readShort() throws IOException {
            this.d += 2;
            if (this.d <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (short) ((read2 << 8) + read);
                    }
                    if (byteOrder == f) {
                        return (short) ((read << 8) + read2);
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        public String readUTF() throws IOException {
            this.d += 2;
            return this.a.readUTF();
        }

        @DexIgnore
        public int readUnsignedByte() throws IOException {
            this.d++;
            return this.a.readUnsignedByte();
        }

        @DexIgnore
        public int readUnsignedShort() throws IOException {
            this.d += 2;
            if (this.d <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (read2 << 8) + read;
                    }
                    if (byteOrder == f) {
                        return (read << 8) + read2;
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        public int skipBytes(int i) throws IOException {
            int min = Math.min(i, this.c - this.d);
            int i2 = 0;
            while (i2 < min) {
                i2 += this.a.skipBytes(min - i2);
            }
            this.d += i2;
            return i2;
        }

        @DexIgnore
        public b(InputStream inputStream, ByteOrder byteOrder) throws IOException {
            this.b = ByteOrder.BIG_ENDIAN;
            this.a = new DataInputStream(inputStream);
            this.c = this.a.available();
            this.d = 0;
            this.a.mark(this.c);
            this.b = byteOrder;
        }

        @DexIgnore
        public void a(long j) throws IOException {
            int i = this.d;
            if (((long) i) > j) {
                this.d = 0;
                this.a.reset();
                this.a.mark(this.c);
            } else {
                j -= (long) i;
            }
            int i2 = (int) j;
            if (skipBytes(i2) != i2) {
                throw new IOException("Couldn't seek up to the byteCount");
            }
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.a.read(bArr, i, i2);
            this.d += read;
            return read;
        }

        @DexIgnore
        public void readFully(byte[] bArr) throws IOException {
            this.d += bArr.length;
            if (this.d > this.c) {
                throw new EOFException();
            } else if (this.a.read(bArr, 0, bArr.length) != bArr.length) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        @DexIgnore
        public b(byte[] bArr) throws IOException {
            this((InputStream) new ByteArrayInputStream(bArr));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ byte[] c;

        @DexIgnore
        public c(int i, int i2, byte[] bArr) {
            this(i, i2, -1, bArr);
        }

        @DexIgnore
        public static c a(int[] iArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(pb.J[3] * iArr.length)]);
            wrap.order(byteOrder);
            for (int i : iArr) {
                wrap.putShort((short) i);
            }
            return new c(3, iArr.length, wrap.array());
        }

        @DexIgnore
        public int b(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                throw new NumberFormatException("NULL can't be converted to a integer value");
            } else if (d instanceof String) {
                return Integer.parseInt((String) d);
            } else {
                if (d instanceof long[]) {
                    long[] jArr = (long[]) d;
                    if (jArr.length == 1) {
                        return (int) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof int[]) {
                    int[] iArr = (int[]) d;
                    if (iArr.length == 1) {
                        return iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
            }
        }

        @DexIgnore
        public String c(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                return null;
            }
            if (d instanceof String) {
                return (String) d;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (d instanceof long[]) {
                long[] jArr = (long[]) d;
                while (i < jArr.length) {
                    sb.append(jArr[i]);
                    i++;
                    if (i != jArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (d instanceof int[]) {
                int[] iArr = (int[]) d;
                while (i < iArr.length) {
                    sb.append(iArr[i]);
                    i++;
                    if (i != iArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (d instanceof double[]) {
                double[] dArr = (double[]) d;
                while (i < dArr.length) {
                    sb.append(dArr[i]);
                    i++;
                    if (i != dArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (!(d instanceof e[])) {
                return null;
            } else {
                e[] eVarArr = (e[]) d;
                while (i < eVarArr.length) {
                    sb.append(eVarArr[i].a);
                    sb.append('/');
                    sb.append(eVarArr[i].b);
                    i++;
                    if (i != eVarArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:164:0x01ab A[SYNTHETIC, Splitter:B:164:0x01ab] */
        public Object d(ByteOrder byteOrder) {
            b bVar;
            try {
                bVar = new b(this.c);
                try {
                    bVar.a(byteOrder);
                    boolean z = true;
                    int i = 0;
                    switch (this.a) {
                        case 1:
                        case 6:
                            if (this.c.length != 1 || this.c[0] < 0 || this.c[0] > 1) {
                                String str = new String(this.c, pb.b0);
                                try {
                                    bVar.close();
                                } catch (IOException e) {
                                    Log.e("ExifInterface", "IOException occurred while closing InputStream", e);
                                }
                                return str;
                            }
                            String str2 = new String(new char[]{(char) (this.c[0] + 48)});
                            try {
                                bVar.close();
                            } catch (IOException e2) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e2);
                            }
                            return str2;
                        case 2:
                        case 7:
                            if (this.b >= pb.K.length) {
                                int i2 = 0;
                                while (true) {
                                    if (i2 < pb.K.length) {
                                        if (this.c[i2] != pb.K[i2]) {
                                            z = false;
                                        } else {
                                            i2++;
                                        }
                                    }
                                }
                                if (z) {
                                    i = pb.K.length;
                                }
                            }
                            StringBuilder sb = new StringBuilder();
                            while (true) {
                                if (i < this.b) {
                                    byte b2 = this.c[i];
                                    if (b2 != 0) {
                                        if (b2 >= 32) {
                                            sb.append((char) b2);
                                        } else {
                                            sb.append('?');
                                        }
                                        i++;
                                    }
                                }
                            }
                            String sb2 = sb.toString();
                            try {
                                bVar.close();
                            } catch (IOException e3) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e3);
                            }
                            return sb2;
                        case 3:
                            int[] iArr = new int[this.b];
                            while (i < this.b) {
                                iArr[i] = bVar.readUnsignedShort();
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e4) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e4);
                            }
                            return iArr;
                        case 4:
                            long[] jArr = new long[this.b];
                            while (i < this.b) {
                                jArr[i] = bVar.m();
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e5) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e5);
                            }
                            return jArr;
                        case 5:
                            e[] eVarArr = new e[this.b];
                            while (i < this.b) {
                                eVarArr[i] = new e(bVar.m(), bVar.m());
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e6) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e6);
                            }
                            return eVarArr;
                        case 8:
                            int[] iArr2 = new int[this.b];
                            while (i < this.b) {
                                iArr2[i] = bVar.readShort();
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e7) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e7);
                            }
                            return iArr2;
                        case 9:
                            int[] iArr3 = new int[this.b];
                            while (i < this.b) {
                                iArr3[i] = bVar.readInt();
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e8) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e8);
                            }
                            return iArr3;
                        case 10:
                            e[] eVarArr2 = new e[this.b];
                            while (i < this.b) {
                                eVarArr2[i] = new e((long) bVar.readInt(), (long) bVar.readInt());
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e9) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e9);
                            }
                            return eVarArr2;
                        case 11:
                            double[] dArr = new double[this.b];
                            while (i < this.b) {
                                dArr[i] = (double) bVar.readFloat();
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e10) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e10);
                            }
                            return dArr;
                        case 12:
                            double[] dArr2 = new double[this.b];
                            while (i < this.b) {
                                dArr2[i] = bVar.readDouble();
                                i++;
                            }
                            try {
                                bVar.close();
                            } catch (IOException e11) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e11);
                            }
                            return dArr2;
                        default:
                            try {
                                bVar.close();
                            } catch (IOException e12) {
                                Log.e("ExifInterface", "IOException occurred while closing InputStream", e12);
                            }
                            return null;
                    }
                } catch (IOException e13) {
                    e = e13;
                }
                e = e13;
            } catch (IOException e14) {
                e = e14;
                bVar = null;
            } catch (Throwable th) {
                th = th;
                bVar = null;
                if (bVar != null) {
                }
                throw th;
            }
            try {
                Log.w("ExifInterface", "IOException occurred during reading a value", e);
                if (bVar != null) {
                    try {
                        bVar.close();
                    } catch (IOException e15) {
                        Log.e("ExifInterface", "IOException occurred while closing InputStream", e15);
                    }
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (bVar != null) {
                    try {
                        bVar.close();
                    } catch (IOException e16) {
                        Log.e("ExifInterface", "IOException occurred while closing InputStream", e16);
                    }
                }
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "(" + pb.I[this.a] + ", data length:" + this.c.length + ")";
        }

        @DexIgnore
        public c(int i, int i2, long j, byte[] bArr) {
            this.a = i;
            this.b = i2;
            this.c = bArr;
        }

        @DexIgnore
        public static c a(int i, ByteOrder byteOrder) {
            return a(new int[]{i}, byteOrder);
        }

        @DexIgnore
        public static c a(long[] jArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(pb.J[4] * jArr.length)]);
            wrap.order(byteOrder);
            for (long j : jArr) {
                wrap.putInt((int) j);
            }
            return new c(4, jArr.length, wrap.array());
        }

        @DexIgnore
        public static c a(long j, ByteOrder byteOrder) {
            return a(new long[]{j}, byteOrder);
        }

        @DexIgnore
        public static c a(String str) {
            byte[] bytes = (str + 0).getBytes(pb.b0);
            return new c(2, bytes.length, bytes);
        }

        @DexIgnore
        public static c a(e[] eVarArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(pb.J[5] * eVarArr.length)]);
            wrap.order(byteOrder);
            for (e eVar : eVarArr) {
                wrap.putInt((int) eVar.a);
                wrap.putInt((int) eVar.b);
            }
            return new c(5, eVarArr.length, wrap.array());
        }

        @DexIgnore
        public static c a(e eVar, ByteOrder byteOrder) {
            return a(new e[]{eVar}, byteOrder);
        }

        @DexIgnore
        public double a(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                throw new NumberFormatException("NULL can't be converted to a double value");
            } else if (d instanceof String) {
                return Double.parseDouble((String) d);
            } else {
                if (d instanceof long[]) {
                    long[] jArr = (long[]) d;
                    if (jArr.length == 1) {
                        return (double) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof int[]) {
                    int[] iArr = (int[]) d;
                    if (iArr.length == 1) {
                        return (double) iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof double[]) {
                    double[] dArr = (double[]) d;
                    if (dArr.length == 1) {
                        return dArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof e[]) {
                    e[] eVarArr = (e[]) d;
                    if (eVarArr.length == 1) {
                        return eVarArr[0].a();
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a double value");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ long b;

        @DexIgnore
        public e(long j, long j2) {
            if (j2 == 0) {
                this.a = 0;
                this.b = 1;
                return;
            }
            this.a = j;
            this.b = j2;
        }

        @DexIgnore
        public double a() {
            return ((double) this.a) / ((double) this.b);
        }

        @DexIgnore
        public String toString() {
            return this.a + "/" + this.b;
        }
    }

    /*
    static {
        Arrays.asList(new Integer[]{1, 6, 3, 8});
        Arrays.asList(new Integer[]{2, 7, 4, 5});
        new int[1][0] = 4;
        "VP8X".getBytes(Charset.defaultCharset());
        "VP8L".getBytes(Charset.defaultCharset());
        "VP8 ".getBytes(Charset.defaultCharset());
        "ANIM".getBytes(Charset.defaultCharset());
        "ANMF".getBytes(Charset.defaultCharset());
        "XMP ".getBytes(Charset.defaultCharset());
        d[] dVarArr = L;
        V = new d[][]{dVarArr, M, N, O, P, dVarArr, R, S, T, U};
        new d("JPEGInterchangeFormat", 513, 4);
        new d("JPEGInterchangeFormatLength", 514, 4);
        d[][] dVarArr2 = V;
        X = new HashMap[dVarArr2.length];
        Y = new HashMap[dVarArr2.length];
        H.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (int i2 = 0; i2 < V.length; i2++) {
            X[i2] = new HashMap<>();
            Y[i2] = new HashMap<>();
            for (d dVar : V[i2]) {
                X[i2].put(Integer.valueOf(dVar.a), dVar);
                Y[i2].put(dVar.b, dVar);
            }
        }
        a0.put(Integer.valueOf(W[0].a), 5);
        a0.put(Integer.valueOf(W[1].a), 1);
        a0.put(Integer.valueOf(W[2].a), 2);
        a0.put(Integer.valueOf(W[3].a), 3);
        a0.put(Integer.valueOf(W[4].a), 7);
        a0.put(Integer.valueOf(W[5].a), 8);
        Pattern.compile(".*[1-9].*");
        Pattern.compile("^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$");
    }
    */

    @DexIgnore
    public pb(String str) throws IOException {
        d[][] dVarArr = V;
        this.f = new HashMap[dVarArr.length];
        this.g = new HashSet(dVarArr.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (str != null) {
            c(str);
            return;
        }
        throw new NullPointerException("filename cannot be null");
    }

    @DexIgnore
    public static boolean h(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = u;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public String a(String str) {
        if (str != null) {
            c b2 = b(str);
            if (b2 != null) {
                if (!Z.contains(str)) {
                    return b2.c(this.h);
                }
                if (str.equals("GPSTimeStamp")) {
                    int i2 = b2.a;
                    if (i2 == 5 || i2 == 10) {
                        e[] eVarArr = (e[]) b2.d(this.h);
                        if (eVarArr == null || eVarArr.length != 3) {
                            Log.w("ExifInterface", "Invalid GPS Timestamp array. array=" + Arrays.toString(eVarArr));
                            return null;
                        }
                        return String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf((int) (((float) eVarArr[0].a) / ((float) eVarArr[0].b))), Integer.valueOf((int) (((float) eVarArr[1].a) / ((float) eVarArr[1].b))), Integer.valueOf((int) (((float) eVarArr[2].a) / ((float) eVarArr[2].b)))});
                    }
                    Log.w("ExifInterface", "GPS Timestamp format is not rational. format=" + b2.a);
                    return null;
                }
                try {
                    return Double.toString(b2.a(this.h));
                } catch (NumberFormatException unused) {
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    @DexIgnore
    public final c b(String str) {
        if (str != null) {
            if ("ISOSpeedRatings".equals(str)) {
                if (r) {
                    Log.d("ExifInterface", "getExifAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                str = "PhotographicSensitivity";
            }
            for (int i2 = 0; i2 < V.length; i2++) {
                c cVar = this.f[i2].get(str);
                if (cVar != null) {
                    return cVar;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    @DexIgnore
    public final void c() {
        for (int i2 = 0; i2 < this.f.length; i2++) {
            Log.d("ExifInterface", "The size of tag group[" + i2 + "]: " + this.f[i2].size());
            for (Map.Entry next : this.f[i2].entrySet()) {
                c cVar = (c) next.getValue();
                Log.d("ExifInterface", "tagName: " + ((String) next.getKey()) + ", tagType: " + cVar.toString() + ", tagValue: '" + cVar.c(this.h) + "'");
            }
        }
    }

    @DexIgnore
    public final boolean d(byte[] bArr) throws IOException {
        byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i2 = 0; i2 < bytes.length; i2++) {
            if (bArr[i2] != bytes[i2]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002e  */
    public final boolean e(byte[] bArr) throws IOException {
        b bVar;
        boolean z2 = false;
        try {
            bVar = new b(bArr);
            try {
                this.h = i(bVar);
                bVar.a(this.h);
                if (bVar.readShort() == 85) {
                    z2 = true;
                }
                bVar.close();
                return z2;
            } catch (Exception unused) {
                if (bVar != null) {
                }
                return false;
            } catch (Throwable th) {
                th = th;
                if (bVar != null) {
                }
                throw th;
            }
        } catch (Exception unused2) {
            bVar = null;
            if (bVar != null) {
                bVar.close();
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            bVar = null;
            if (bVar != null) {
                bVar.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean f(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = E;
            if (i2 >= bArr2.length) {
                int i3 = 0;
                while (true) {
                    byte[] bArr3 = F;
                    if (i3 >= bArr3.length) {
                        return true;
                    }
                    if (bArr[E.length + i3 + 4] != bArr3[i3]) {
                        return false;
                    }
                    i3++;
                }
            } else if (bArr[i2] != bArr2[i2]) {
                return false;
            } else {
                i2++;
            }
        }
    }

    @DexIgnore
    public final void g(b bVar) throws IOException {
        bVar.skipBytes(c0.length);
        byte[] bArr = new byte[bVar.available()];
        bVar.readFully(bArr);
        this.m = c0.length;
        a(bArr, 0);
    }

    @DexIgnore
    public final ByteOrder i(b bVar) throws IOException {
        short readShort = bVar.readShort();
        if (readShort == 18761) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align II");
            }
            return ByteOrder.LITTLE_ENDIAN;
        } else if (readShort == 19789) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align MM");
            }
            return ByteOrder.BIG_ENDIAN;
        } else {
            throw new IOException("Invalid byte order: " + Integer.toHexString(readShort));
        }
    }

    @DexIgnore
    public final void j(b bVar) throws IOException {
        HashMap<String, c> hashMap = this.f[4];
        c cVar = hashMap.get("Compression");
        if (cVar != null) {
            this.l = cVar.b(this.h);
            int i2 = this.l;
            if (i2 != 1) {
                if (i2 == 6) {
                    a(bVar, (HashMap) hashMap);
                    return;
                } else if (i2 != 7) {
                    return;
                }
            }
            if (a((HashMap) hashMap)) {
                b(bVar, (HashMap) hashMap);
                return;
            }
            return;
        }
        this.l = 6;
        a(bVar, (HashMap) hashMap);
    }

    @DexIgnore
    public final void h(b bVar) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getWebpAttributes starting with: " + bVar);
        }
        bVar.a(ByteOrder.LITTLE_ENDIAN);
        bVar.skipBytes(E.length);
        int readInt = bVar.readInt() + 8;
        int skipBytes = bVar.skipBytes(F.length) + 8;
        while (true) {
            try {
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == bArr.length) {
                    int readInt2 = bVar.readInt();
                    int i2 = skipBytes + 4 + 4;
                    if (Arrays.equals(G, bArr)) {
                        byte[] bArr2 = new byte[readInt2];
                        if (bVar.read(bArr2) == readInt2) {
                            this.m = i2;
                            a(bArr2, 0);
                            this.m = i2;
                            return;
                        }
                        throw new IOException("Failed to read given length for given PNG chunk type: " + g(bArr));
                    }
                    if (readInt2 % 2 == 1) {
                        readInt2++;
                    }
                    int i3 = i2 + readInt2;
                    if (i3 != readInt) {
                        if (i3 <= readInt) {
                            int skipBytes2 = bVar.skipBytes(readInt2);
                            if (skipBytes2 == readInt2) {
                                skipBytes = i2 + skipBytes2;
                            } else {
                                throw new IOException("Encountered WebP file with invalid chunk size");
                            }
                        } else {
                            throw new IOException("Encountered WebP file with invalid chunk size");
                        }
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt WebP file.");
            }
        }
    }

    @DexIgnore
    public final void d(b bVar) throws IOException {
        bVar.skipBytes(84);
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[4];
        bVar.read(bArr);
        bVar.skipBytes(4);
        bVar.read(bArr2);
        int i2 = ByteBuffer.wrap(bArr).getInt();
        int i3 = ByteBuffer.wrap(bArr2).getInt();
        a(bVar, i2, 5);
        bVar.a((long) i3);
        bVar.a(ByteOrder.BIG_ENDIAN);
        int readInt = bVar.readInt();
        if (r) {
            Log.d("ExifInterface", "numberOfDirectoryEntry: " + readInt);
        }
        for (int i4 = 0; i4 < readInt; i4++) {
            int readUnsignedShort = bVar.readUnsignedShort();
            int readUnsignedShort2 = bVar.readUnsignedShort();
            if (readUnsignedShort == Q.a) {
                short readShort = bVar.readShort();
                short readShort2 = bVar.readShort();
                c a2 = c.a((int) readShort, this.h);
                c a3 = c.a((int) readShort2, this.h);
                this.f[0].put("ImageLength", a2);
                this.f[0].put("ImageWidth", a3);
                if (r) {
                    Log.d("ExifInterface", "Updated to length: " + readShort + ", width: " + readShort2);
                    return;
                }
                return;
            }
            bVar.skipBytes(readUnsignedShort2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public d(String str, int i, int i2) {
            this.b = str;
            this.a = i;
            this.c = i2;
            this.d = -1;
        }

        @DexIgnore
        public boolean a(int i) {
            int i2;
            int i3 = this.c;
            if (i3 == 7 || i == 7 || i3 == i || (i2 = this.d) == i) {
                return true;
            }
            if ((i3 == 4 || i2 == 4) && i == 3) {
                return true;
            }
            if ((this.c == 9 || this.d == 9) && i == 8) {
                return true;
            }
            if ((this.c == 12 || this.d == 12) && i == 11) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public d(String str, int i, int i2, int i3) {
            this.b = str;
            this.a = i;
            this.c = i2;
            this.d = i3;
        }
    }

    @DexIgnore
    public final void f(b bVar) throws IOException {
        e(bVar);
        if (this.f[0].get("JpgFromRaw") != null) {
            a(bVar, this.q, 5);
        }
        c cVar = this.f[0].get("ISO");
        c cVar2 = this.f[1].get("PhotographicSensitivity");
        if (cVar != null && cVar2 == null) {
            this.f[1].put("PhotographicSensitivity", cVar);
        }
    }

    @DexIgnore
    public static String g(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            sb.append(String.format("%02x", new Object[]{Byte.valueOf(bArr[i2])}));
        }
        return sb.toString();
    }

    @DexIgnore
    public pb(InputStream inputStream) throws IOException {
        this(inputStream, false);
    }

    @DexIgnore
    public int b() {
        switch (a("Orientation", 1)) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 8:
                return 270;
            case 6:
            case 7:
                return 90;
            default:
                return 0;
        }
    }

    @DexIgnore
    public pb(InputStream inputStream, boolean z2) throws IOException {
        d[][] dVarArr = V;
        this.f = new HashMap[dVarArr.length];
        this.g = new HashSet(dVarArr.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (inputStream != null) {
            this.a = null;
            if (z2) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, VideoUploader.RETRY_DELAY_UNIT_MS);
                if (!b(bufferedInputStream)) {
                    Log.w("ExifInterface", "Given data does not follow the structure of an Exif-only data.");
                    return;
                }
                this.e = true;
                this.c = null;
                this.b = null;
                inputStream = bufferedInputStream;
            } else if (inputStream instanceof AssetManager.AssetInputStream) {
                this.c = (AssetManager.AssetInputStream) inputStream;
                this.b = null;
            } else {
                if (inputStream instanceof FileInputStream) {
                    FileInputStream fileInputStream = (FileInputStream) inputStream;
                    if (a(fileInputStream.getFD())) {
                        this.c = null;
                        this.b = fileInputStream.getFD();
                    }
                }
                this.c = null;
                this.b = null;
            }
            a(inputStream);
            return;
        }
        throw new NullPointerException("inputStream cannot be null");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032  */
    public final boolean b(byte[] bArr) throws IOException {
        b bVar;
        boolean z2 = false;
        try {
            bVar = new b(bArr);
            try {
                this.h = i(bVar);
                bVar.a(this.h);
                short readShort = bVar.readShort();
                if (readShort == 20306 || readShort == 21330) {
                    z2 = true;
                }
                bVar.close();
                return z2;
            } catch (Exception unused) {
                if (bVar != null) {
                }
                return false;
            } catch (Throwable th) {
                th = th;
                if (bVar != null) {
                }
                throw th;
            }
        } catch (Exception unused2) {
            bVar = null;
            if (bVar != null) {
                bVar.close();
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            bVar = null;
            if (bVar != null) {
                bVar.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void c(String str) throws IOException {
        FileInputStream fileInputStream;
        if (str != null) {
            this.c = null;
            this.a = str;
            try {
                fileInputStream = new FileInputStream(str);
                try {
                    if (a(fileInputStream.getFD())) {
                        this.b = fileInputStream.getFD();
                    } else {
                        this.b = null;
                    }
                    a((InputStream) fileInputStream);
                    a((Closeable) fileInputStream);
                } catch (Throwable th) {
                    th = th;
                    a((Closeable) fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = null;
                a((Closeable) fileInputStream);
                throw th;
            }
        } else {
            throw new NullPointerException("filename cannot be null");
        }
    }

    @DexIgnore
    public final void e(b bVar) throws IOException {
        c cVar;
        a(bVar, bVar.available());
        b(bVar, 0);
        d(bVar, 0);
        d(bVar, 5);
        d(bVar, 4);
        d();
        if (this.d == 8 && (cVar = this.f[1].get("MakerNote")) != null) {
            b bVar2 = new b(cVar.c);
            bVar2.a(this.h);
            bVar2.a(6);
            b(bVar2, 9);
            c cVar2 = this.f[9].get("ColorSpace");
            if (cVar2 != null) {
                this.f[1].put("ColorSpace", cVar2);
            }
        }
    }

    @DexIgnore
    public static boolean b(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(c0.length);
        byte[] bArr = new byte[c0.length];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        int i2 = 0;
        while (true) {
            byte[] bArr2 = c0;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public int a(String str, int i2) {
        if (str != null) {
            c b2 = b(str);
            if (b2 == null) {
                return i2;
            }
            try {
                return b2.b(this.h);
            } catch (NumberFormatException unused) {
                return i2;
            }
        } else {
            throw new NullPointerException("tag shouldn't be null");
        }
    }

    @DexIgnore
    public final boolean c(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = A;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public final void a(InputStream inputStream) {
        if (inputStream != null) {
            int i2 = 0;
            while (i2 < V.length) {
                try {
                    this.f[i2] = new HashMap<>();
                    i2++;
                } catch (IOException e2) {
                    if (r) {
                        Log.w("ExifInterface", "Invalid image: ExifInterface got an unsupported image format file(ExifInterface supports JPEG and some RAW image formats only) or a corrupted JPEG file to ExifInterface.", e2);
                    }
                    a();
                    if (!r) {
                        return;
                    }
                } catch (Throwable th) {
                    a();
                    if (r) {
                        c();
                    }
                    throw th;
                }
            }
            if (!this.e) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, VideoUploader.RETRY_DELAY_UNIT_MS);
                this.d = a(bufferedInputStream);
                inputStream = bufferedInputStream;
            }
            b bVar = new b(inputStream);
            if (!this.e) {
                switch (this.d) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                    case 6:
                    case 8:
                    case 11:
                        e(bVar);
                        break;
                    case 4:
                        a(bVar, 0, 0);
                        break;
                    case 7:
                        b(bVar);
                        break;
                    case 9:
                        d(bVar);
                        break;
                    case 10:
                        f(bVar);
                        break;
                    case 12:
                        a(bVar);
                        break;
                    case 13:
                        c(bVar);
                        break;
                    case 14:
                        h(bVar);
                        break;
                }
            } else {
                g(bVar);
            }
            j(bVar);
            a();
            if (!r) {
                return;
            }
            c();
            return;
        }
        throw new NullPointerException("inputstream shouldn't be null");
    }

    @DexIgnore
    public final void c(b bVar) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getPngAttributes starting with: " + bVar);
        }
        bVar.a(ByteOrder.BIG_ENDIAN);
        bVar.skipBytes(A.length);
        int length = A.length + 0;
        while (true) {
            try {
                int readInt = bVar.readInt();
                int i2 = length + 4;
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == bArr.length) {
                    int i3 = i2 + 4;
                    if (i3 == 16) {
                        if (!Arrays.equals(bArr, C)) {
                            throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                        }
                    }
                    if (!Arrays.equals(bArr, D)) {
                        if (Arrays.equals(bArr, B)) {
                            byte[] bArr2 = new byte[readInt];
                            if (bVar.read(bArr2) == readInt) {
                                int readInt2 = bVar.readInt();
                                CRC32 crc32 = new CRC32();
                                crc32.update(bArr);
                                crc32.update(bArr2);
                                if (((int) crc32.getValue()) == readInt2) {
                                    this.m = i3;
                                    a(bArr2, 0);
                                    d();
                                    return;
                                }
                                throw new IOException("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: " + readInt2 + ", calculated CRC value: " + crc32.getValue());
                            }
                            throw new IOException("Failed to read given length for given PNG chunk type: " + g(bArr));
                        }
                        int i4 = readInt + 4;
                        bVar.skipBytes(i4);
                        length = i3 + i4;
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing PNG chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt PNG file.");
            }
        }
    }

    @DexIgnore
    public final void b(b bVar) throws IOException {
        e(bVar);
        c cVar = this.f[1].get("MakerNote");
        if (cVar != null) {
            b bVar2 = new b(cVar.c);
            bVar2.a(this.h);
            byte[] bArr = new byte[y.length];
            bVar2.readFully(bArr);
            bVar2.a(0);
            byte[] bArr2 = new byte[z.length];
            bVar2.readFully(bArr2);
            if (Arrays.equals(bArr, y)) {
                bVar2.a(8);
            } else if (Arrays.equals(bArr2, z)) {
                bVar2.a(12);
            }
            b(bVar2, 6);
            c cVar2 = this.f[7].get("PreviewImageStart");
            c cVar3 = this.f[7].get("PreviewImageLength");
            if (!(cVar2 == null || cVar3 == null)) {
                this.f[5].put("JPEGInterchangeFormat", cVar2);
                this.f[5].put("JPEGInterchangeFormatLength", cVar3);
            }
            c cVar4 = this.f[8].get("AspectFrame");
            if (cVar4 != null) {
                int[] iArr = (int[]) cVar4.d(this.h);
                if (iArr == null || iArr.length != 4) {
                    Log.w("ExifInterface", "Invalid aspect frame values. frame=" + Arrays.toString(iArr));
                } else if (iArr[2] > iArr[0] && iArr[3] > iArr[1]) {
                    int i2 = (iArr[2] - iArr[0]) + 1;
                    int i3 = (iArr[3] - iArr[1]) + 1;
                    if (i2 < i3) {
                        int i4 = i2 + i3;
                        i3 = i4 - i3;
                        i2 = i4 - i3;
                    }
                    c a2 = c.a(i2, this.h);
                    c a3 = c.a(i3, this.h);
                    this.f[0].put("ImageWidth", a2);
                    this.f[0].put("ImageLength", a3);
                }
            }
        }
    }

    @DexIgnore
    public final void d() throws IOException {
        a(0, 5);
        a(0, 4);
        a(5, 4);
        c cVar = this.f[1].get("PixelXDimension");
        c cVar2 = this.f[1].get("PixelYDimension");
        if (!(cVar == null || cVar2 == null)) {
            this.f[0].put("ImageWidth", cVar);
            this.f[0].put("ImageLength", cVar2);
        }
        if (this.f[4].isEmpty() && b((HashMap) this.f[5])) {
            HashMap<String, c>[] hashMapArr = this.f;
            hashMapArr[4] = hashMapArr[5];
            hashMapArr[5] = new HashMap<>();
        }
        if (!b((HashMap) this.f[4])) {
            Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image.");
        }
    }

    @DexIgnore
    public final void d(b bVar, int i2) throws IOException {
        c cVar;
        c cVar2;
        c cVar3 = this.f[i2].get("DefaultCropSize");
        c cVar4 = this.f[i2].get("SensorTopBorder");
        c cVar5 = this.f[i2].get("SensorLeftBorder");
        c cVar6 = this.f[i2].get("SensorBottomBorder");
        c cVar7 = this.f[i2].get("SensorRightBorder");
        if (cVar3 != null) {
            if (cVar3.a == 5) {
                e[] eVarArr = (e[]) cVar3.d(this.h);
                if (eVarArr == null || eVarArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(eVarArr));
                    return;
                }
                cVar2 = c.a(eVarArr[0], this.h);
                cVar = c.a(eVarArr[1], this.h);
            } else {
                int[] iArr = (int[]) cVar3.d(this.h);
                if (iArr == null || iArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(iArr));
                    return;
                }
                cVar2 = c.a(iArr[0], this.h);
                cVar = c.a(iArr[1], this.h);
            }
            this.f[i2].put("ImageWidth", cVar2);
            this.f[i2].put("ImageLength", cVar);
        } else if (cVar4 == null || cVar5 == null || cVar6 == null || cVar7 == null) {
            c(bVar, i2);
        } else {
            int b2 = cVar4.b(this.h);
            int b3 = cVar6.b(this.h);
            int b4 = cVar7.b(this.h);
            int b5 = cVar5.b(this.h);
            if (b3 > b2 && b4 > b5) {
                c a2 = c.a(b3 - b2, this.h);
                c a3 = c.a(b4 - b5, this.h);
                this.f[i2].put("ImageLength", a2);
                this.f[i2].put("ImageWidth", a3);
            }
        }
    }

    @DexIgnore
    public final void c(b bVar, int i2) throws IOException {
        c cVar;
        c cVar2 = this.f[i2].get("ImageLength");
        c cVar3 = this.f[i2].get("ImageWidth");
        if ((cVar2 == null || cVar3 == null) && (cVar = this.f[i2].get("JPEGInterchangeFormat")) != null) {
            a(bVar, cVar.b(this.h), i2);
        }
    }

    @DexIgnore
    public static boolean a(FileDescriptor fileDescriptor) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Os.lseek(fileDescriptor, 0, OsConstants.SEEK_CUR);
                return true;
            } catch (Exception unused) {
                if (r) {
                    Log.d("ExifInterface", "The file descriptor for the given input is not seekable");
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final int a(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(VideoUploader.RETRY_DELAY_UNIT_MS);
        byte[] bArr = new byte[VideoUploader.RETRY_DELAY_UNIT_MS];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        if (h(bArr)) {
            return 4;
        }
        if (d(bArr)) {
            return 9;
        }
        if (a(bArr)) {
            return 12;
        }
        if (b(bArr)) {
            return 7;
        }
        if (e(bArr)) {
            return 10;
        }
        if (c(bArr)) {
            return 13;
        }
        return f(bArr) ? 14 : 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02ba  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02f8  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x015b  */
    public final void b(b bVar, int i2) throws IOException {
        String str;
        long j2;
        long j3;
        boolean z2;
        short s2;
        short s3;
        String str2;
        String str3;
        d dVar;
        int i3;
        int i4;
        int i5;
        b bVar2 = bVar;
        int i6 = i2;
        this.g.add(Integer.valueOf(bVar2.d));
        if (bVar2.d + 2 <= bVar2.c) {
            short readShort = bVar.readShort();
            String str4 = "ExifInterface";
            if (r) {
                Log.d(str4, "numberOfDirectoryEntry: " + readShort);
            }
            if (bVar2.d + (readShort * 12) <= bVar2.c && readShort > 0) {
                char c2 = 0;
                short s4 = 0;
                while (s4 < readShort) {
                    int readUnsignedShort = bVar.readUnsignedShort();
                    int readUnsignedShort2 = bVar.readUnsignedShort();
                    int readInt = bVar.readInt();
                    long l2 = ((long) bVar.l()) + 4;
                    d dVar2 = X[i6].get(Integer.valueOf(readUnsignedShort));
                    if (r) {
                        Object[] objArr = new Object[5];
                        objArr[c2] = Integer.valueOf(i2);
                        objArr[1] = Integer.valueOf(readUnsignedShort);
                        objArr[2] = dVar2 != null ? dVar2.b : null;
                        objArr[3] = Integer.valueOf(readUnsignedShort2);
                        objArr[4] = Integer.valueOf(readInt);
                        Log.d(str4, String.format("ifdType: %d, tagNumber: %d, tagName: %s, dataFormat: %d, numberOfComponents: %d", objArr));
                    }
                    if (dVar2 == null) {
                        if (r) {
                            Log.d(str4, "Skip the tag entry since tag number is not defined: " + readUnsignedShort);
                        }
                    } else if (readUnsignedShort2 <= 0 || readUnsignedShort2 >= J.length) {
                        j2 = l2;
                        if (r) {
                            Log.d(str4, "Skip the tag entry since data format is invalid: " + readUnsignedShort2);
                        }
                        z2 = false;
                        j3 = 0;
                        if (z2) {
                            bVar2.a(j2);
                            s3 = readShort;
                            str2 = str4;
                            s2 = s4;
                        } else {
                            long j4 = j2;
                            if (j3 > 4) {
                                int readInt2 = bVar.readInt();
                                s3 = readShort;
                                if (r) {
                                    StringBuilder sb = new StringBuilder();
                                    s2 = s4;
                                    sb.append("seek to data offset: ");
                                    sb.append(readInt2);
                                    Log.d(str4, sb.toString());
                                } else {
                                    s2 = s4;
                                }
                                int i7 = this.d;
                                if (i7 == 7) {
                                    if ("MakerNote".equals(dVar2.b)) {
                                        this.n = readInt2;
                                    } else if (i6 == 6 && "ThumbnailImage".equals(dVar2.b)) {
                                        this.o = readInt2;
                                        this.p = readInt;
                                        c a2 = c.a(6, this.h);
                                        i3 = readUnsignedShort2;
                                        i4 = readInt;
                                        c a3 = c.a((long) this.o, this.h);
                                        c a4 = c.a((long) this.p, this.h);
                                        this.f[4].put("Compression", a2);
                                        this.f[4].put("JPEGInterchangeFormat", a3);
                                        this.f[4].put("JPEGInterchangeFormatLength", a4);
                                    }
                                    i3 = readUnsignedShort2;
                                    i4 = readInt;
                                } else {
                                    i3 = readUnsignedShort2;
                                    i4 = readInt;
                                    if (i7 == 10 && "JpgFromRaw".equals(dVar2.b)) {
                                        this.q = readInt2;
                                    }
                                }
                                long j5 = (long) readInt2;
                                dVar = dVar2;
                                str3 = "Compression";
                                if (j5 + j3 <= ((long) bVar2.c)) {
                                    bVar2.a(j5);
                                } else {
                                    if (r) {
                                        Log.d(str4, "Skip the tag entry since data offset is invalid: " + readInt2);
                                    }
                                    bVar2.a(j4);
                                    str2 = str4;
                                }
                            } else {
                                s3 = readShort;
                                dVar = dVar2;
                                str3 = "Compression";
                                s2 = s4;
                                i3 = readUnsignedShort2;
                                i4 = readInt;
                            }
                            Integer num = a0.get(Integer.valueOf(readUnsignedShort));
                            if (r) {
                                Log.d(str4, "nextIfdType: " + num + " byteCount: " + j3);
                            }
                            if (num != null) {
                                long j6 = -1;
                                int i8 = i3;
                                if (i8 != 3) {
                                    if (i8 == 4) {
                                        j6 = bVar.m();
                                    } else if (i8 == 8) {
                                        i5 = bVar.readShort();
                                    } else if (i8 == 9 || i8 == 13) {
                                        i5 = bVar.readInt();
                                    }
                                    if (r) {
                                        Log.d(str4, String.format("Offset: %d, tagName: %s", new Object[]{Long.valueOf(j6), dVar.b}));
                                    }
                                    if (j6 <= 0 || j6 >= ((long) bVar2.c)) {
                                        if (r) {
                                            Log.d(str4, "Skip jump into the IFD since its offset is invalid: " + j6);
                                        }
                                    } else if (!this.g.contains(Integer.valueOf((int) j6))) {
                                        bVar2.a(j6);
                                        b(bVar2, num.intValue());
                                    } else if (r) {
                                        Log.d(str4, "Skip jump into the IFD since it has already been read: IfdType " + num + " (at " + j6 + ")");
                                    }
                                    bVar2.a(j4);
                                    str2 = str4;
                                } else {
                                    i5 = bVar.readUnsignedShort();
                                }
                                j6 = (long) i5;
                                if (r) {
                                }
                                if (j6 <= 0 || j6 >= ((long) bVar2.c)) {
                                }
                                bVar2.a(j4);
                                str2 = str4;
                            } else {
                                d dVar3 = dVar;
                                String str5 = str3;
                                int l3 = bVar.l() + this.m;
                                byte[] bArr = new byte[((int) j3)];
                                bVar2.readFully(bArr);
                                str2 = str4;
                                c cVar = new c(i3, i4, (long) l3, bArr);
                                this.f[i2].put(dVar3.b, cVar);
                                if ("DNGVersion".equals(dVar3.b)) {
                                    this.d = 3;
                                }
                                if ((("Make".equals(dVar3.b) || "Model".equals(dVar3.b)) && cVar.c(this.h).contains("PENTAX")) || (str5.equals(dVar3.b) && cVar.b(this.h) == 65535)) {
                                    this.d = 8;
                                }
                                if (((long) bVar.l()) != j4) {
                                    bVar2.a(j4);
                                }
                            }
                            s4 = (short) (s2 + 1);
                            str4 = str2;
                            readShort = s3;
                            c2 = 0;
                            i6 = i2;
                        }
                        s4 = (short) (s2 + 1);
                        str4 = str2;
                        readShort = s3;
                        c2 = 0;
                        i6 = i2;
                    } else if (dVar2.a(readUnsignedShort2)) {
                        if (readUnsignedShort2 == 7) {
                            readUnsignedShort2 = dVar2.c;
                        }
                        j2 = l2;
                        j3 = ((long) J[readUnsignedShort2]) * ((long) readInt);
                        if (j3 < 0 || j3 > 2147483647L) {
                            if (r) {
                                Log.d(str4, "Skip the tag entry since the number of components is invalid: " + readInt);
                            }
                            z2 = false;
                            if (z2) {
                            }
                            s4 = (short) (s2 + 1);
                            str4 = str2;
                            readShort = s3;
                            c2 = 0;
                            i6 = i2;
                        } else {
                            z2 = true;
                            if (z2) {
                            }
                            s4 = (short) (s2 + 1);
                            str4 = str2;
                            readShort = s3;
                            c2 = 0;
                            i6 = i2;
                        }
                    } else if (r) {
                        Log.d(str4, "Skip the tag entry since data format (" + I[readUnsignedShort2] + ") is unexpected for tag: " + dVar2.b);
                    }
                    j2 = l2;
                    z2 = false;
                    j3 = 0;
                    if (z2) {
                    }
                    s4 = (short) (s2 + 1);
                    str4 = str2;
                    readShort = s3;
                    c2 = 0;
                    i6 = i2;
                }
                String str6 = str4;
                if (bVar.l() + 4 <= bVar2.c) {
                    int readInt3 = bVar.readInt();
                    if (r) {
                        str = str6;
                        Log.d(str, String.format("nextIfdOffset: %d", new Object[]{Integer.valueOf(readInt3)}));
                    } else {
                        str = str6;
                    }
                    long j7 = (long) readInt3;
                    if (j7 <= 0 || readInt3 >= bVar2.c) {
                        if (r) {
                            Log.d(str, "Stop reading file since a wrong offset may cause an infinite loop: " + readInt3);
                        }
                    } else if (!this.g.contains(Integer.valueOf(readInt3))) {
                        bVar2.a(j7);
                        if (this.f[4].isEmpty()) {
                            b(bVar2, 4);
                        } else if (this.f[5].isEmpty()) {
                            b(bVar2, 5);
                        }
                    } else if (r) {
                        Log.d(str, "Stop reading file since re-reading an IFD may cause an infinite loop: " + readInt3);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0093 A[Catch:{ all -> 0x008b }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00a2  */
    public final boolean a(byte[] bArr) throws IOException {
        b bVar = null;
        try {
            b bVar2 = new b(bArr);
            try {
                long readInt = (long) bVar2.readInt();
                byte[] bArr2 = new byte[4];
                bVar2.read(bArr2);
                if (!Arrays.equals(bArr2, v)) {
                    bVar2.close();
                    return false;
                }
                long j2 = 16;
                if (readInt == 1) {
                    readInt = bVar2.readLong();
                    if (readInt < 16) {
                        bVar2.close();
                        return false;
                    }
                } else {
                    j2 = 8;
                }
                if (readInt > ((long) bArr.length)) {
                    readInt = (long) bArr.length;
                }
                long j3 = readInt - j2;
                if (j3 < 8) {
                    bVar2.close();
                    return false;
                }
                byte[] bArr3 = new byte[4];
                boolean z2 = false;
                boolean z3 = false;
                for (long j4 = 0; j4 < j3 / 4; j4++) {
                    if (bVar2.read(bArr3) != bArr3.length) {
                        bVar2.close();
                        return false;
                    }
                    if (j4 != 1) {
                        if (Arrays.equals(bArr3, w)) {
                            z2 = true;
                        } else if (Arrays.equals(bArr3, x)) {
                            z3 = true;
                        }
                        if (z2 && z3) {
                            bVar2.close();
                            return true;
                        }
                    }
                }
                bVar2.close();
                return false;
            } catch (Exception e2) {
                e = e2;
                bVar = bVar2;
                try {
                    if (r) {
                    }
                    if (bVar != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    bVar2 = bVar;
                    if (bVar2 != null) {
                        bVar2.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (bVar2 != null) {
                }
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            if (r) {
                Log.d("ExifInterface", "Exception parsing HEIF file type box.", e);
            }
            if (bVar != null) {
                bVar.close();
            }
            return false;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00f4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x018d A[SYNTHETIC] */
    public final void a(b bVar, int i2, int i3) throws IOException {
        b bVar2 = bVar;
        int i4 = i2;
        int i5 = i3;
        if (r) {
            Log.d("ExifInterface", "getJpegAttributes starting with: " + bVar2);
        }
        bVar2.a(ByteOrder.BIG_ENDIAN);
        bVar2.a((long) i4);
        byte readByte = bVar.readByte();
        byte b2 = -1;
        if (readByte == -1) {
            int i6 = 1;
            int i7 = i4 + 1;
            if (bVar.readByte() == -40) {
                int i8 = i7 + 1;
                while (true) {
                    byte readByte2 = bVar.readByte();
                    if (readByte2 == b2) {
                        int i9 = i8 + i6;
                        byte readByte3 = bVar.readByte();
                        if (r) {
                            Log.d("ExifInterface", "Found JPEG segment indicator: " + Integer.toHexString(readByte3 & 255));
                        }
                        int i10 = i9 + i6;
                        if (readByte3 == -39 || readByte3 == -38) {
                            bVar2.a(this.h);
                        } else {
                            int readUnsignedShort = bVar.readUnsignedShort() - 2;
                            int i11 = i10 + 2;
                            if (r) {
                                Log.d("ExifInterface", "JPEG segment: " + Integer.toHexString(readByte3 & 255) + " (length: " + (readUnsignedShort + 2) + ")");
                            }
                            if (readUnsignedShort >= 0) {
                                if (readByte3 == -31) {
                                    byte[] bArr = new byte[readUnsignedShort];
                                    bVar2.readFully(bArr);
                                    int i12 = readUnsignedShort + i11;
                                    if (a(bArr, c0)) {
                                        byte[] bArr2 = c0;
                                        int length = i11 + bArr2.length;
                                        byte[] copyOfRange = Arrays.copyOfRange(bArr, bArr2.length, bArr.length);
                                        this.m = length;
                                        a(copyOfRange, i5);
                                    } else if (a(bArr, d0)) {
                                        byte[] bArr3 = d0;
                                        int length2 = i11 + bArr3.length;
                                        byte[] copyOfRange2 = Arrays.copyOfRange(bArr, bArr3.length, bArr.length);
                                        if (a("Xmp") == null) {
                                            HashMap<String, c> hashMap = this.f[0];
                                            c cVar = r13;
                                            c cVar2 = new c(1, copyOfRange2.length, (long) length2, copyOfRange2);
                                            hashMap.put("Xmp", cVar);
                                        }
                                    }
                                    i11 = i12;
                                } else if (readByte3 != -2) {
                                    switch (readByte3) {
                                        case -64:
                                        case -63:
                                        case -62:
                                        case -61:
                                            if (bVar2.skipBytes(i6) != i6) {
                                            }
                                            break;
                                        default:
                                            switch (readByte3) {
                                                case -59:
                                                case -58:
                                                case -57:
                                                    break;
                                                default:
                                                    switch (readByte3) {
                                                        case -55:
                                                        case -54:
                                                        case -53:
                                                            break;
                                                        default:
                                                            switch (readByte3) {
                                                                case -51:
                                                                case -50:
                                                                case -49:
                                                                    break;
                                                            }
                                                    }
                                            }
                                            if (bVar2.skipBytes(i6) != i6) {
                                                this.f[i5].put("ImageLength", c.a((long) bVar.readUnsignedShort(), this.h));
                                                this.f[i5].put("ImageWidth", c.a((long) bVar.readUnsignedShort(), this.h));
                                                readUnsignedShort -= 5;
                                                break;
                                            } else {
                                                throw new IOException("Invalid SOFx");
                                            }
                                    }
                                    if (readUnsignedShort >= 0) {
                                        throw new IOException("Invalid length");
                                    } else if (bVar2.skipBytes(readUnsignedShort) == readUnsignedShort) {
                                        i8 = i11 + readUnsignedShort;
                                        b2 = -1;
                                        i6 = 1;
                                    } else {
                                        throw new IOException("Invalid JPEG segment");
                                    }
                                } else {
                                    byte[] bArr4 = new byte[readUnsignedShort];
                                    if (bVar2.read(bArr4) != readUnsignedShort) {
                                        throw new IOException("Invalid exif");
                                    } else if (a("UserComment") == null) {
                                        this.f[i6].put("UserComment", c.a(new String(bArr4, b0)));
                                    }
                                }
                                readUnsignedShort = 0;
                                if (readUnsignedShort >= 0) {
                                }
                            } else {
                                throw new IOException("Invalid length");
                            }
                        }
                    } else {
                        throw new IOException("Invalid marker:" + Integer.toHexString(readByte2 & 255));
                    }
                }
                bVar2.a(this.h);
                return;
            }
            throw new IOException("Invalid marker: " + Integer.toHexString(readByte & 255));
        }
        throw new IOException("Invalid marker: " + Integer.toHexString(readByte & 255));
    }

    @DexIgnore
    public final void a(b bVar) throws IOException {
        String str;
        String str2;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mediaMetadataRetriever.setDataSource(new a(this, bVar));
            } else if (this.b != null) {
                mediaMetadataRetriever.setDataSource(this.b);
            } else if (this.a != null) {
                mediaMetadataRetriever.setDataSource(this.a);
            } else {
                mediaMetadataRetriever.release();
                return;
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(33);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(34);
            String extractMetadata3 = mediaMetadataRetriever.extractMetadata(26);
            String extractMetadata4 = mediaMetadataRetriever.extractMetadata(17);
            String str3 = null;
            if ("yes".equals(extractMetadata3)) {
                str3 = mediaMetadataRetriever.extractMetadata(29);
                str2 = mediaMetadataRetriever.extractMetadata(30);
                str = mediaMetadataRetriever.extractMetadata(31);
            } else if ("yes".equals(extractMetadata4)) {
                str3 = mediaMetadataRetriever.extractMetadata(18);
                str2 = mediaMetadataRetriever.extractMetadata(19);
                str = mediaMetadataRetriever.extractMetadata(24);
            } else {
                str2 = null;
                str = null;
            }
            if (str3 != null) {
                this.f[0].put("ImageWidth", c.a(Integer.parseInt(str3), this.h));
            }
            if (str2 != null) {
                this.f[0].put("ImageLength", c.a(Integer.parseInt(str2), this.h));
            }
            if (str != null) {
                int i2 = 1;
                int parseInt = Integer.parseInt(str);
                if (parseInt == 90) {
                    i2 = 6;
                } else if (parseInt == 180) {
                    i2 = 3;
                } else if (parseInt == 270) {
                    i2 = 8;
                }
                this.f[0].put("Orientation", c.a(i2, this.h));
            }
            if (!(extractMetadata == null || extractMetadata2 == null)) {
                int parseInt2 = Integer.parseInt(extractMetadata);
                int parseInt3 = Integer.parseInt(extractMetadata2);
                if (parseInt3 > 6) {
                    bVar.a((long) parseInt2);
                    byte[] bArr = new byte[6];
                    if (bVar.read(bArr) == 6) {
                        int i3 = parseInt2 + 6;
                        int i4 = parseInt3 - 6;
                        if (Arrays.equals(bArr, c0)) {
                            byte[] bArr2 = new byte[i4];
                            if (bVar.read(bArr2) == i4) {
                                this.m = i3;
                                a(bArr2, 0);
                            } else {
                                throw new IOException("Can't read exif");
                            }
                        } else {
                            throw new IOException("Invalid identifier");
                        }
                    } else {
                        throw new IOException("Can't read identifier");
                    }
                } else {
                    throw new IOException("Invalid exif length");
                }
            }
            if (r) {
                Log.d("ExifInterface", "Heif meta: " + str3 + "x" + str2 + ", rotation " + str);
            }
        } finally {
            mediaMetadataRetriever.release();
        }
    }

    @DexIgnore
    public final void b(b bVar, HashMap hashMap) throws IOException {
        b bVar2 = bVar;
        HashMap hashMap2 = hashMap;
        c cVar = (c) hashMap2.get("StripOffsets");
        c cVar2 = (c) hashMap2.get("StripByteCounts");
        if (cVar != null && cVar2 != null) {
            long[] a2 = a(cVar.d(this.h));
            long[] a3 = a(cVar2.d(this.h));
            if (a2 == null || a2.length == 0) {
                Log.w("ExifInterface", "stripOffsets should not be null or have zero length.");
            } else if (a3 == null || a3.length == 0) {
                Log.w("ExifInterface", "stripByteCounts should not be null or have zero length.");
            } else if (a2.length != a3.length) {
                Log.w("ExifInterface", "stripOffsets and stripByteCounts should have same length.");
            } else {
                long j2 = 0;
                for (long j3 : a3) {
                    j2 += j3;
                }
                byte[] bArr = new byte[((int) j2)];
                this.i = true;
                int i2 = 0;
                int i3 = 0;
                for (int i4 = 0; i4 < a2.length; i4++) {
                    int i5 = (int) a2[i4];
                    int i6 = (int) a3[i4];
                    if (i4 < a2.length - 1 && ((long) (i5 + i6)) != a2[i4 + 1]) {
                        this.i = false;
                    }
                    int i7 = i5 - i2;
                    if (i7 < 0) {
                        Log.d("ExifInterface", "Invalid strip offset value");
                    }
                    bVar2.a((long) i7);
                    int i8 = i2 + i7;
                    byte[] bArr2 = new byte[i6];
                    bVar2.read(bArr2);
                    i2 = i8 + i6;
                    System.arraycopy(bArr2, 0, bArr, i3, bArr2.length);
                    i3 += bArr2.length;
                }
                if (this.i) {
                    this.j = ((int) a2[0]) + this.m;
                    this.k = bArr.length;
                }
            }
        }
    }

    @DexIgnore
    public final void a(byte[] bArr, int i2) throws IOException {
        b bVar = new b(bArr);
        a(bVar, bArr.length);
        b(bVar, i2);
    }

    @DexIgnore
    public final void a() {
        String a2 = a("DateTimeOriginal");
        if (a2 != null && a("DateTime") == null) {
            this.f[0].put("DateTime", c.a(a2));
        }
        if (a("ImageWidth") == null) {
            this.f[0].put("ImageWidth", c.a(0, this.h));
        }
        if (a("ImageLength") == null) {
            this.f[0].put("ImageLength", c.a(0, this.h));
        }
        if (a("Orientation") == null) {
            this.f[0].put("Orientation", c.a(0, this.h));
        }
        if (a("LightSource") == null) {
            this.f[1].put("LightSource", c.a(0, this.h));
        }
    }

    @DexIgnore
    public final boolean b(HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("ImageLength");
        c cVar2 = (c) hashMap.get("ImageWidth");
        if (cVar == null || cVar2 == null) {
            return false;
        }
        return cVar.b(this.h) <= 512 && cVar2.b(this.h) <= 512;
    }

    @DexIgnore
    public final void a(b bVar, int i2) throws IOException {
        this.h = i(bVar);
        bVar.a(this.h);
        int readUnsignedShort = bVar.readUnsignedShort();
        int i3 = this.d;
        if (i3 == 7 || i3 == 10 || readUnsignedShort == 42) {
            int readInt = bVar.readInt();
            if (readInt < 8 || readInt >= i2) {
                throw new IOException("Invalid first Ifd offset: " + readInt);
            }
            int i4 = readInt - 8;
            if (i4 > 0 && bVar.skipBytes(i4) != i4) {
                throw new IOException("Couldn't jump to first Ifd: " + i4);
            }
            return;
        }
        throw new IOException("Invalid start code: " + Integer.toHexString(readUnsignedShort));
    }

    @DexIgnore
    public final void a(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("JPEGInterchangeFormat");
        c cVar2 = (c) hashMap.get("JPEGInterchangeFormatLength");
        if (cVar != null && cVar2 != null) {
            int b2 = cVar.b(this.h);
            int b3 = cVar2.b(this.h);
            if (this.d == 7) {
                b2 += this.n;
            }
            int min = Math.min(b3, bVar.k() - b2);
            if (b2 > 0 && min > 0) {
                this.j = this.m + b2;
                this.k = min;
                if (this.a == null && this.c == null && this.b == null) {
                    bVar.a((long) this.j);
                    bVar.readFully(new byte[this.k]);
                }
            }
            if (r) {
                Log.d("ExifInterface", "Setting thumbnail attributes with offset: " + b2 + ", length: " + min);
            }
        }
    }

    @DexIgnore
    public final boolean a(HashMap hashMap) throws IOException {
        c cVar;
        int b2;
        c cVar2 = (c) hashMap.get("BitsPerSample");
        if (cVar2 != null) {
            int[] iArr = (int[]) cVar2.d(this.h);
            if (Arrays.equals(s, iArr)) {
                return true;
            }
            if (this.d == 3 && (cVar = (c) hashMap.get("PhotometricInterpretation")) != null && (((b2 = cVar.b(this.h)) == 1 && Arrays.equals(iArr, t)) || (b2 == 6 && Arrays.equals(iArr, s)))) {
                return true;
            }
        }
        if (!r) {
            return false;
        }
        Log.d("ExifInterface", "Unsupported data type value");
        return false;
    }

    @DexIgnore
    public final void a(int i2, int i3) throws IOException {
        if (!this.f[i2].isEmpty() && !this.f[i3].isEmpty()) {
            c cVar = this.f[i2].get("ImageLength");
            c cVar2 = this.f[i2].get("ImageWidth");
            c cVar3 = this.f[i3].get("ImageLength");
            c cVar4 = this.f[i3].get("ImageWidth");
            if (cVar == null || cVar2 == null) {
                if (r) {
                    Log.d("ExifInterface", "First image does not contain valid size information");
                }
            } else if (cVar3 != null && cVar4 != null) {
                int b2 = cVar.b(this.h);
                int b3 = cVar2.b(this.h);
                int b4 = cVar3.b(this.h);
                int b5 = cVar4.b(this.h);
                if (b2 < b4 && b3 < b5) {
                    HashMap<String, c>[] hashMapArr = this.f;
                    HashMap<String, c> hashMap = hashMapArr[i2];
                    hashMapArr[i2] = hashMapArr[i3];
                    hashMapArr[i3] = hashMap;
                }
            } else if (r) {
                Log.d("ExifInterface", "Second image does not contain valid size information");
            }
        } else if (r) {
            Log.d("ExifInterface", "Cannot perform swap since only one image data exists");
        }
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    public static long[] a(Object obj) {
        if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            long[] jArr = new long[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                jArr[i2] = (long) iArr[i2];
            }
            return jArr;
        } else if (obj instanceof long[]) {
            return (long[]) obj;
        } else {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length < bArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }
}
