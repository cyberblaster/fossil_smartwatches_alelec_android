package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kf1 extends le0 {
    @DexIgnore
    public /* final */ boolean T;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ kf1(ue1 ue1, q41 q41, boolean z, String str, int i) {
        super(ue1, q41, r3, r4, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r7, 48);
        String a = (i & 8) != 0 ? ze0.a("UUID.randomUUID().toString()") : str;
        eh1 eh1 = eh1.SET_FRONT_LIGHT_ENABLE;
        eo0 eo0 = new eo0(z);
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("set", eo0.b());
            jSONObject.put("push", jSONObject2);
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        this.T = z;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.ENABLE, (Object) Boolean.valueOf(this.T));
    }
}
