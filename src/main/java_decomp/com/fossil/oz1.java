package com.fossil;

import android.os.DeadObjectException;
import com.fossil.nw1;
import com.fossil.qw1;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oz1<A extends nw1<? extends ew1, rv1.b>> extends az1 {
    @DexIgnore
    public /* final */ A a;

    @DexIgnore
    public oz1(int i, A a2) {
        super(i);
        this.a = a2;
    }

    @DexIgnore
    public final void a(qw1.a<?> aVar) throws DeadObjectException {
        try {
            this.a.b(aVar.f());
        } catch (RuntimeException e) {
            a(e);
        }
    }

    @DexIgnore
    public final void a(Status status) {
        this.a.c(status);
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        String simpleName = runtimeException.getClass().getSimpleName();
        String localizedMessage = runtimeException.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.a.c(new Status(10, sb.toString()));
    }

    @DexIgnore
    public final void a(k02 k02, boolean z) {
        k02.a((BasePendingResult<? extends ew1>) this.a, z);
    }
}
