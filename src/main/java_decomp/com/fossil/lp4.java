package com.fossil;

import android.text.TextUtils;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.gson.AlarmDeserializer;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.gson.DianaRecommendPresetDeserializer;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.gson.HybridRecommendPresetDeserializer;
import com.portfolio.platform.gson.ThemeDeserializer;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.joda.time.DateTime;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp4 {
    @DexIgnore
    public static /* final */ Retrofit.b a;
    @DexIgnore
    public static Retrofit b;
    @DexIgnore
    public static Cache c;
    @DexIgnore
    public static /* final */ qy5 d;
    @DexIgnore
    public static /* final */ OkHttpClient.b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public static /* final */ lp4 g; // = new lp4();

    /*
    static {
        OkHttpClient.b bVar;
        Retrofit.b bVar2 = new Retrofit.b();
        du3 du3 = new du3();
        du3.b(new qj4());
        du3.a(new qj4());
        du3.a(Date.class, new GsonConvertDate());
        du3.a(DianaPreset.class, new DianaPresetDeserializer());
        du3.a(DianaRecommendPreset.class, new DianaRecommendPresetDeserializer());
        du3.a(HybridPreset.class, new HybridPresetDeserializer());
        du3.a(HybridRecommendPreset.class, new HybridRecommendPresetDeserializer());
        du3.a(DateTime.class, new GsonConvertDateTime());
        du3.a(Alarm.class, new AlarmDeserializer());
        du3.a(Theme.class, new ThemeDeserializer());
        bVar2.a(GsonConverterFactory.a(du3.a()));
        a = bVar2;
        qy5 qy5 = new qy5();
        qy5.a(wg6.a((Object) "release", (Object) "release") ? HttpLoggingInterceptor.a.BASIC : HttpLoggingInterceptor.a.BODY);
        d = qy5;
        if (wg6.a((Object) "release", (Object) "release")) {
            bVar = new OkHttpClient.b();
            bVar.a((Interceptor) d);
        } else {
            bVar = new OkHttpClient.b();
            bVar.a((Interceptor) d);
            bVar.b((Interceptor) new StethoInterceptor());
        }
        e = bVar;
    }
    */

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "baseUrl");
        if (!TextUtils.equals(f, str)) {
            f = str;
            Retrofit.b bVar = a;
            String str2 = f;
            if (str2 != null) {
                bVar.a(str2);
                b = a.a();
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(bq6 bq6) {
        wg6.b(bq6, "authenticator");
        e.a(bq6);
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final void a(File file) {
        wg6.b(file, "cacheDir");
        if (c == null) {
            File file2 = new File(file.getAbsolutePath(), "cacheResponse");
            if (!file2.exists()) {
                file2.mkdir();
            }
            c = new Cache(file2, 10485760);
        }
        e.a(c);
    }

    @DexIgnore
    public final void a() {
        Cache cache = c;
        if (cache != null) {
            cache.k();
        }
    }

    @DexIgnore
    public final void a(Interceptor interceptor) {
        e.b(10, TimeUnit.SECONDS);
        e.a(10, TimeUnit.SECONDS);
        e.c(10, TimeUnit.SECONDS);
        e.b().clear();
        e.a((Interceptor) d);
        if (interceptor != null && !e.b().contains(interceptor)) {
            e.a(interceptor);
        }
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final <S> S a(Class<S> cls) {
        wg6.b(cls, "serviceClass");
        Retrofit retrofit = b;
        if (retrofit != null) {
            return retrofit.a(cls);
        }
        wg6.a();
        throw null;
    }
}
