package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xk1 extends v01 {
    @DexIgnore
    public byte[] D;
    @DexIgnore
    public long E;
    @DexIgnore
    public float F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public int J;
    @DexIgnore
    public rg1 K;
    @DexIgnore
    public long L;
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public /* final */ float Q;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ xk1(ue1 ue1, q41 q41, eh1 eh1, boolean z, short s, float f, String str, int i) {
        this(ue1, q41, eh1, z, s, (i & 32) != 0 ? 0.001f : f, (i & 64) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public void h() {
        if (!(!(this.D.length == 0))) {
            a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else if (this.P) {
            if1.a(this, lx0.PUT_FILE, (lx0) null, 2, (Object) null);
        } else {
            if1.a(this, lx0.GET_FILE_SIZE_WRITTEN, (lx0) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(cw0.a(super.i(), bm0.FILE_SIZE, (Object) Integer.valueOf(this.D.length)), bm0.FILE_CRC, (Object) Long.valueOf(h51.a.a(this.D, q11.CRC32))), bm0.SKIP_RESUME, (Object) Boolean.valueOf(this.P));
    }

    @DexIgnore
    public void j() {
        try {
            this.D = n();
            this.E = 0;
            this.G = 0;
            this.H = 0;
            this.I = true;
            this.J = 0;
            this.K = rg1.UNKNOWN;
        } catch (sw0 e) {
            qs0.h.a(e);
            a(km1.a(this.v, (eh1) null, sk1.UNSUPPORTED_FORMAT, (bn0) null, 5));
        }
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(cw0.a(super.k(), bm0.ERASE_DURATION_IN_MS, (Object) Long.valueOf(Math.max(this.M - this.L, 0))), bm0.TRANSFER_DURATION_IN_MS, (Object) Long.valueOf(Math.max(this.O - this.N, 0)));
    }

    @DexIgnore
    public final vd1 m() {
        if (this.M == 0) {
            this.M = System.currentTimeMillis();
        }
        if (this.N == 0) {
            this.N = System.currentTimeMillis();
        }
        long o = o();
        long j = this.H;
        long j2 = o - j;
        short s = this.C;
        byte[] a = md6.a(this.D, (int) j, (int) o());
        ue1 ue1 = this.w;
        vd1 vd1 = new vd1(s, new ds0(a, Math.min(ue1.j, ue1.o), this.K), this.w);
        ba1 ba1 = new ba1(this, j2);
        if (!vd1.t) {
            vd1.o.add(ba1);
        }
        vd1.b((hg6<? super qv0, cd6>) new wb1(this));
        vd1.a((hg6<? super qv0, cd6>) new rd1(this));
        vd1 vd12 = vd1;
        vd12.s = c();
        return vd12;
    }

    @DexIgnore
    public abstract byte[] n();

    @DexIgnore
    public final long o() {
        return cw0.b(this.D.length);
    }

    @DexIgnore
    public ol0 p() {
        return new ol0(this.C, lx0.VERIFY_FILE, this.w, 0, 8);
    }

    @DexIgnore
    public void q() {
        a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
    }

    @DexIgnore
    public final void r() {
        boolean z = true;
        if (this.I) {
            this.I = false;
        } else if ((((float) (this.H - this.G)) * 1.0f) / ((float) o()) > 0.01f) {
            this.J = 0;
        } else {
            this.J++;
            if (this.J >= 3) {
                z = false;
            }
        }
        if (z) {
            if1.a(this, lx0.PUT_FILE, (lx0) null, 2, (Object) null);
        } else {
            a(km1.a(this.v, (eh1) null, sk1.DATA_TRANSFER_RETRY_REACH_THRESHOLD, (bn0) null, 5));
        }
    }

    @DexIgnore
    public xk1(ue1 ue1, q41 q41, eh1 eh1, boolean z, short s, float f, String str) {
        super(ue1, q41, eh1, s, str);
        this.P = z;
        this.Q = f;
        this.D = new byte[0];
        this.I = true;
        this.K = rg1.UNKNOWN;
    }

    @DexIgnore
    public final void a(t71 t71) {
        a(t71.C);
        this.E = this.H;
        float o = (((float) this.E) * 1.0f) / ((float) o());
        if (Math.abs(o - this.F) > this.Q || o == 1.0f) {
            this.F = o;
            a(o);
        }
        if1.a(this, lx0.PUT_FILE, (lx0) null, 2, (Object) null);
    }

    @DexIgnore
    public static final /* synthetic */ void a(xk1 xk1, long j, long j2) {
        xk1 xk12 = xk1;
        long o = xk1.o();
        if (1 <= j && o >= j) {
            long a = h51.a.a(xk12.D, q11.CRC32);
            xk1.a(j);
            if (j2 != a) {
                ue1 ue1 = xk12.w;
                q41 q41 = xk12.x;
                String str = ue1.t;
                short s = xk12.C;
                byte[] bArr = xk12.D;
                ie1 ie1 = r4;
                ie1 ie12 = new ie1(str, ie1.CREATOR.b(s), ie1.CREATOR.a(s), bArr, cw0.b(bArr.length), h51.a.a(xk12.D, q11.CRC32), 0, false);
                if1.a((if1) xk1, (if1) new t71(ue1, q41, ie1, xk12.z), (hg6) new jh1(xk12), (hg6) new dj1(xk12), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
            } else if (j == xk1.o()) {
                if1.a(xk12, lx0.VERIFY_FILE, (lx0) null, 2, (Object) null);
            } else {
                xk1.r();
            }
        } else {
            xk12.a(0);
            xk1.r();
        }
    }

    @DexIgnore
    public final void a(long j) {
        this.G = this.H;
        this.H = j;
        this.E = this.H;
    }

    @DexIgnore
    public qv0 a(lx0 lx0) {
        int i = l41.a[lx0.ordinal()];
        if (i == 1) {
            xm1 xm1 = new xm1(this.C, this.w, 0, 4);
            xm1.b((hg6<? super qv0, cd6>) new h61(this));
            return xm1;
        } else if (i == 2) {
            if (this.L == 0) {
                this.L = System.currentTimeMillis();
            }
            bi0 bi0 = new bi0(this.H, o() - this.H, o(), this.C, this.w, 0, 32);
            bi0.b((hg6<? super qv0, cd6>) new e81(this));
            return bi0;
        } else if (i == 3) {
            ol0 p = p();
            p.b((hg6<? super qv0, cd6>) new nf1(this));
            return p;
        } else if (i != 4) {
            return null;
        } else {
            return m();
        }
    }
}
