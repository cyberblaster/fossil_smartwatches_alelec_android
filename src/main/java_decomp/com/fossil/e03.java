package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e03 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ v63 a;
    @DexIgnore
    public /* final */ /* synthetic */ b03 b;

    @DexIgnore
    public e03(b03 b03, v63 v63) {
        this.b = b03;
        this.a = v63;
    }

    @DexIgnore
    public final void run() {
        this.a.d();
        if (bb3.a()) {
            this.a.a().a((Runnable) this);
            return;
        }
        boolean b2 = this.b.b();
        long unused = this.b.c = 0;
        if (b2) {
            this.b.a();
        }
    }
}
