package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class jq3 implements Runnable {
    @DexIgnore
    public /* final */ Map.Entry a;
    @DexIgnore
    public /* final */ oq3 b;

    @DexIgnore
    public jq3(Map.Entry entry, oq3 oq3) {
        this.a = entry;
        this.b = oq3;
    }

    @DexIgnore
    public static Runnable a(Map.Entry entry, oq3 oq3) {
        return new jq3(entry, oq3);
    }

    @DexIgnore
    public void run() {
        ((pq3) this.a.getKey()).a(this.b);
    }
}
