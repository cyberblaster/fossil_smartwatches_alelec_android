package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jn3<T> implements Comparator<T> {
    @DexIgnore
    public static /* final */ int LEFT_IS_GREATER; // = 1;
    @DexIgnore
    public static /* final */ int RIGHT_IS_GREATER; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jn3<Object> {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(0);
        @DexIgnore
        public /* final */ ConcurrentMap<Object, Integer> b;

        @DexIgnore
        public a() {
            vm3 vm3 = new vm3();
            ln3.a(vm3);
            this.b = vm3.f();
        }

        @DexIgnore
        public final Integer a(Object obj) {
            Integer num = (Integer) this.b.get(obj);
            if (num != null) {
                return num;
            }
            Integer valueOf = Integer.valueOf(this.a.getAndIncrement());
            Integer putIfAbsent = this.b.putIfAbsent(obj, valueOf);
            return putIfAbsent != null ? putIfAbsent : valueOf;
        }

        @DexIgnore
        public int b(Object obj) {
            return System.identityHashCode(obj);
        }

        @DexIgnore
        public int compare(Object obj, Object obj2) {
            if (obj == obj2) {
                return 0;
            }
            if (obj == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            int b2 = b(obj);
            int b3 = b(obj2);
            if (b2 == b3) {
                int compareTo = a(obj).compareTo(a(obj2));
                if (compareTo != 0) {
                    return compareTo;
                }
                throw new AssertionError();
            } else if (b2 < b3) {
                return -1;
            } else {
                return 1;
            }
        }

        @DexIgnore
        public String toString() {
            return "Ordering.arbitrary()";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ jn3<Object> a; // = new a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ClassCastException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object value;

        @DexIgnore
        public c(Object obj) {
            super("Cannot compare value: " + obj);
            this.value = obj;
        }
    }

    @DexIgnore
    public static jn3<Object> allEqual() {
        return yk3.INSTANCE;
    }

    @DexIgnore
    public static jn3<Object> arbitrary() {
        return b.a;
    }

    @DexIgnore
    public static <T> jn3<T> explicit(List<T> list) {
        return new kl3(list);
    }

    @DexIgnore
    public static <T> jn3<T> from(Comparator<T> comparator) {
        return comparator instanceof jn3 ? (jn3) comparator : new dl3(comparator);
    }

    @DexIgnore
    public static <C extends Comparable> jn3<C> natural() {
        return fn3.INSTANCE;
    }

    @DexIgnore
    public static jn3<Object> usingToString() {
        return lo3.INSTANCE;
    }

    @DexIgnore
    @Deprecated
    public int binarySearch(List<? extends T> list, T t) {
        return Collections.binarySearch(list, t, this);
    }

    @DexIgnore
    public abstract int compare(T t, T t2);

    @DexIgnore
    public <U extends T> jn3<U> compound(Comparator<? super U> comparator) {
        jk3.a(comparator);
        return new el3(this, comparator);
    }

    @DexIgnore
    public <E extends T> List<E> greatestOf(Iterable<E> iterable, int i) {
        return reverse().leastOf(iterable, i);
    }

    @DexIgnore
    public <E extends T> zl3<E> immutableSortedCopy(Iterable<E> iterable) {
        Object[] c2 = pm3.c(iterable);
        for (Object a2 : c2) {
            jk3.a(a2);
        }
        Arrays.sort(c2, this);
        return zl3.asImmutableList(c2);
    }

    @DexIgnore
    public boolean isOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        Object next = it.next();
        while (it.hasNext()) {
            Object next2 = it.next();
            if (compare(next, next2) > 0) {
                return false;
            }
            next = next2;
        }
        return true;
    }

    @DexIgnore
    public boolean isStrictlyOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        Object next = it.next();
        while (it.hasNext()) {
            Object next2 = it.next();
            if (compare(next, next2) >= 0) {
                return false;
            }
            next = next2;
        }
        return true;
    }

    @DexIgnore
    public <E extends T> List<E> leastOf(Iterable<E> iterable, int i) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (((long) collection.size()) <= ((long) i) * 2) {
                Object[] array = collection.toArray();
                Arrays.sort(array, this);
                if (array.length > i) {
                    array = in3.a((T[]) array, i);
                }
                return Collections.unmodifiableList(Arrays.asList(array));
            }
        }
        return leastOf(iterable.iterator(), i);
    }

    @DexIgnore
    public <S extends T> jn3<Iterable<S>> lexicographical() {
        return new rm3(this);
    }

    @DexIgnore
    public <E extends T> E max(Iterator<E> it) {
        E next = it.next();
        while (it.hasNext()) {
            next = max(next, it.next());
        }
        return next;
    }

    @DexIgnore
    public <E extends T> E min(Iterator<E> it) {
        E next = it.next();
        while (it.hasNext()) {
            next = min(next, it.next());
        }
        return next;
    }

    @DexIgnore
    public <S extends T> jn3<S> nullsFirst() {
        return new gn3(this);
    }

    @DexIgnore
    public <S extends T> jn3<S> nullsLast() {
        return new hn3(this);
    }

    @DexIgnore
    public <T2 extends T> jn3<Map.Entry<T2, ?>> onKeys() {
        return onResultOf(ym3.a());
    }

    @DexIgnore
    public <F> jn3<F> onResultOf(ck3<F, ? extends T> ck3) {
        return new al3(ck3, this);
    }

    @DexIgnore
    public <S extends T> jn3<S> reverse() {
        return new vn3(this);
    }

    @DexIgnore
    public <E extends T> List<E> sortedCopy(Iterable<E> iterable) {
        Object[] c2 = pm3.c(iterable);
        Arrays.sort(c2, this);
        return um3.a(Arrays.asList(c2));
    }

    @DexIgnore
    public static <T> jn3<T> compound(Iterable<? extends Comparator<? super T>> iterable) {
        return new el3(iterable);
    }

    @DexIgnore
    public static <T> jn3<T> explicit(T t, T... tArr) {
        return explicit(um3.a(t, (E[]) tArr));
    }

    @DexIgnore
    @Deprecated
    public static <T> jn3<T> from(jn3<T> jn3) {
        jk3.a(jn3);
        return jn3;
    }

    @DexIgnore
    public <E extends T> List<E> greatestOf(Iterator<E> it, int i) {
        return reverse().leastOf(it, i);
    }

    @DexIgnore
    public <E extends T> E max(Iterable<E> iterable) {
        return max(iterable.iterator());
    }

    @DexIgnore
    public <E extends T> E min(Iterable<E> iterable) {
        return min(iterable.iterator());
    }

    @DexIgnore
    public <E extends T> E max(E e, E e2) {
        return compare(e, e2) >= 0 ? e : e2;
    }

    @DexIgnore
    public <E extends T> E min(E e, E e2) {
        return compare(e, e2) <= 0 ? e : e2;
    }

    @DexIgnore
    public <E extends T> E max(E e, E e2, E e3, E... eArr) {
        E max = max(max(e, e2), e3);
        for (E max2 : eArr) {
            max = max(max, max2);
        }
        return max;
    }

    @DexIgnore
    public <E extends T> E min(E e, E e2, E e3, E... eArr) {
        E min = min(min(e, e2), e3);
        for (E min2 : eArr) {
            min = min(min, min2);
        }
        return min;
    }

    @DexIgnore
    public <E extends T> List<E> leastOf(Iterator<E> it, int i) {
        jk3.a(it);
        bl3.a(i, "k");
        if (i == 0 || !it.hasNext()) {
            return zl3.of();
        }
        if (i >= 1073741823) {
            ArrayList<E> a2 = um3.a(it);
            Collections.sort(a2, this);
            if (a2.size() > i) {
                a2.subList(i, a2.size()).clear();
            }
            a2.trimToSize();
            return Collections.unmodifiableList(a2);
        }
        go3 a3 = go3.a(i, this);
        a3.a(it);
        return a3.a();
    }
}
