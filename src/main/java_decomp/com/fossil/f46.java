package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ r36 c;

    @DexIgnore
    public f46(Context context, String str, r36 r36) {
        this.a = context;
        this.b = str;
        this.c = r36;
    }

    @DexIgnore
    public final void run() {
        Long l;
        try {
            q36.f(this.a);
            synchronized (q36.k) {
                l = (Long) q36.k.remove(this.b);
            }
            if (l != null) {
                Long valueOf = Long.valueOf((System.currentTimeMillis() - l.longValue()) / 1000);
                if (valueOf.longValue() <= 0) {
                    valueOf = 1L;
                }
                Long l2 = valueOf;
                String j = q36.j;
                if (j != null && j.equals(this.b)) {
                    j = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                z36 z36 = new z36(this.a, j, this.b, q36.a(this.a, false, this.c), l2, this.c);
                if (!this.b.equals(q36.i)) {
                    q36.m.h("Invalid invocation since previous onResume on diff page.");
                }
                new k46(z36).a();
                String unused = q36.j = this.b;
                return;
            }
            b56 f = q36.m;
            f.c("Starttime for PageID:" + this.b + " not found, lost onResume()?");
        } catch (Throwable th) {
            q36.m.a(th);
            q36.a(this.a, th);
        }
    }
}
