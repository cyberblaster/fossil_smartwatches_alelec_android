package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rl4 implements Cloneable {
    @DexIgnore
    public int a;
    @DexIgnore
    public byte[] b;

    @DexIgnore
    public rl4(int i, byte[] bArr) {
        if (bArr != null) {
            this.a = i;
            this.b = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.b, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        return new java.lang.String(r3.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        return new java.lang.String(r3.b, "iso-8859-1");
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0018 */
    public String a() {
        int i = this.a;
        if (i == 0) {
            return new String(this.b);
        }
        return new String(this.b, pl4.a(i));
    }

    @DexIgnore
    public byte[] b() {
        byte[] bArr = this.b;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        byte[] bArr = this.b;
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        try {
            return new rl4(this.a, bArr2);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }

    @DexIgnore
    public void b(byte[] bArr) {
        if (bArr != null) {
            this.b = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.b, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    @DexIgnore
    public rl4(byte[] bArr) {
        this(106, bArr);
    }

    @DexIgnore
    public rl4(String str) {
        try {
            this.b = str.getBytes("utf-8");
            this.a = 106;
        } catch (UnsupportedEncodingException e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "Default encoding must be supported=" + e);
        }
    }
}
