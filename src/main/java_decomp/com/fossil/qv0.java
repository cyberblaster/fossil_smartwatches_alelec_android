package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qv0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public long e;
    @DexIgnore
    public nn0 f;
    @DexIgnore
    public /* final */ ArrayList<ne0> g;
    @DexIgnore
    public long h;
    @DexIgnore
    public rm1 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public ok0 k;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<qv0, cd6>> l;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<qv0, cd6>> m;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<qv0, cd6>> n;
    @DexIgnore
    public CopyOnWriteArrayList<ig6<qv0, Float, cd6>> o;
    @DexIgnore
    public /* final */ gg6<cd6> p;
    @DexIgnore
    public /* final */ es0 q;
    @DexIgnore
    public /* final */ xt0 r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public bn0 v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public /* final */ lx0 x;
    @DexIgnore
    public /* final */ ue1 y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    public qv0(lx0 lx0, ue1 ue1, int i2) {
        this.x = lx0;
        this.y = ue1;
        this.z = i2;
        this.a = cw0.a((Enum<?>) this.x);
        this.b = ze0.a("UUID.randomUUID().toString()");
        this.c = "";
        this.d = "";
        this.e = System.currentTimeMillis();
        System.currentTimeMillis();
        this.f = new nn0(cw0.a((Enum<?>) this.x), og0.REQUEST, this.y.t, this.c, this.d, false, (String) null, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.REQUEST_UUID, (Object) this.b), 448);
        this.g = new ArrayList<>();
        this.h = 6500;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.j = new Handler(myLooper);
            this.l = new CopyOnWriteArrayList<>();
            this.m = new CopyOnWriteArrayList<>();
            this.n = new CopyOnWriteArrayList<>();
            this.o = new CopyOnWriteArrayList<>();
            this.p = new lq0(this);
            this.q = new es0(this);
            this.r = new xt0(this);
            this.v = new bn0(this.x, this.b, il0.NOT_START, (ch0) null, (sj0) null, 24);
            this.w = true;
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public long a(sg1 sg1) {
        return 0;
    }

    @DexIgnore
    public void a(ni1 ni1) {
        if (ni1.a == h91.CONNECTED) {
            return;
        }
        if (ni1.b == ao0.BLUETOOTH_OFF.a) {
            a(il0.BLUETOOTH_OFF);
        } else {
            a(il0.CONNECTION_DROPPED);
        }
    }

    @DexIgnore
    public void a(ok0 ok0) {
    }

    @DexIgnore
    public final void b() {
        JSONObject jSONObject;
        byte[] bArr;
        if (this.t || this.u) {
            a();
            return;
        }
        this.k = d();
        ok0 ok0 = this.k;
        if (ok0 != null) {
            nn0 nn0 = this.f;
            if (nn0 == null || (jSONObject = nn0.m) == null) {
                jSONObject = new JSONObject();
            }
            boolean z2 = false;
            JSONObject a2 = cw0.a(jSONObject, ok0.a(false));
            JSONObject jSONObject2 = new JSONObject();
            boolean z3 = this instanceof vd1;
            if (z3) {
                bArr = ((vd1) this).G.d;
            } else {
                bArr = ok0 instanceof fb1 ? ((fb1) ok0).l : new byte[0];
            }
            if (bArr.length == 0) {
                z2 = true;
            }
            if (!z2) {
                int length = bArr.length;
                cw0.a(cw0.a(cw0.a(jSONObject2, bm0.RAW_DATA, (Object) (length > 500 || (z3 && wg6.a(this.c, cw0.a((Enum<?>) eh1.SEND_APP_NOTIFICATION)))) ? "" : cw0.a(bArr, (String) null, 1)), bm0.RAW_DATA_LENGTH, (Object) Integer.valueOf(length)), bm0.RAW_DATA_CRC, (Object) Long.valueOf(h51.a.a(bArr, q11.CRC32)));
            }
            JSONObject a3 = cw0.a(a2, jSONObject2);
            nn0 nn02 = this.f;
            if (nn02 != null) {
                nn02.m = a3;
            }
            ue1 ue1 = this.y;
            ok0 ok02 = this.k;
            if (ok02 != null) {
                ue1.a(ok02);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            g();
        }
    }

    @DexIgnore
    public void b(sg1 sg1) {
    }

    @DexIgnore
    public final Handler c() {
        return this.j;
    }

    @DexIgnore
    public abstract ok0 d();

    @DexIgnore
    public long e() {
        return this.h;
    }

    @DexIgnore
    public boolean f() {
        return this.w;
    }

    @DexIgnore
    public abstract void g();

    @DexIgnore
    public JSONObject h() {
        return new JSONObject();
    }

    @DexIgnore
    public JSONObject i() {
        return new JSONObject();
    }

    @DexIgnore
    public final void j() {
        rm1 rm1 = this.i;
        if (rm1 != null) {
            this.j.removeCallbacks(rm1);
        }
        rm1 rm12 = this.i;
        if (rm12 != null) {
            rm12.a = true;
        }
        this.i = null;
    }

    @DexIgnore
    public final void k() {
        nn0 nn0 = this.f;
        if (nn0 != null) {
            cw0.a(nn0.m, bm0.COMPLETED_AT, (Object) Double.valueOf(cw0.a(System.currentTimeMillis())));
            if (this.w) {
                qs0.h.a(nn0);
            }
            this.f = null;
        }
    }

    @DexIgnore
    public void c(ok0 ok0) {
        JSONObject jSONObject;
        this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(ok0.d).c, ok0.d, (sj0) null, 19);
        nn0 nn0 = this.f;
        if (nn0 != null) {
            nn0.i = true;
        }
        nn0 nn02 = this.f;
        if (!(nn02 == null || (jSONObject = nn02.m) == null)) {
            cw0.a(jSONObject, bm0.MESSAGE, (Object) cw0.a((Enum<?>) il0.SUCCESS));
        }
        if (this.v.c == il0.SUCCESS) {
            a(ok0);
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.h = j2;
    }

    @DexIgnore
    public final void a(float f2) {
        Iterator<T> it = this.o.iterator();
        while (it.hasNext()) {
            ((ig6) it.next()).invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public final void a(we1 we1) {
        if (we1 instanceof ni1) {
            a((ni1) we1);
        } else if (we1 instanceof sg1) {
            b((sg1) we1);
        }
    }

    @DexIgnore
    public final void a() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            j();
            this.y.f.remove(this.q);
            this.y.g.remove(this.r);
            k();
            if (f()) {
                qs0 qs0 = qs0.h;
                Object[] array = this.g.toArray(new ne0[0]);
                if (array != null) {
                    ne0[] ne0Arr = (ne0[]) array;
                    JSONObject a2 = cw0.a(cw0.a(new JSONObject(), bm0.MESSAGE, (Object) c(this.v)), bm0.REQUEST_UUID, (Object) this.b);
                    bn0 bn0 = this.v;
                    if (bn0.c == il0.SUCCESS) {
                        cw0.a(a2, i(), false, 2);
                    } else {
                        cw0.a(a2, bm0.ERROR_DETAIL, (Object) bn0.a());
                    }
                    if (!(ne0Arr.length == 0)) {
                        JSONArray jSONArray = new JSONArray();
                        for (ne0 a3 : ne0Arr) {
                            jSONArray.put(a3.a());
                        }
                        cw0.a(a2, bm0.RESPONSES, (Object) jSONArray);
                    }
                    qs0.a(new nn0(cw0.a((Enum<?>) this.x), og0.RESPONSE, this.y.t, this.c, this.d, il0.SUCCESS == this.v.c, (String) null, (r40) null, (bw0) null, a2, 448));
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            il0 il0 = il0.SUCCESS;
            bn0 bn02 = this.v;
            if (il0 == bn02.c) {
                cc0 cc0 = cc0.DEBUG;
                new Object[1][0] = cw0.a(bn02.a(), i()).toString(2);
                Iterator<T> it = this.l.iterator();
                while (it.hasNext()) {
                    ((hg6) it.next()).invoke(this);
                }
            } else {
                cc0 cc02 = cc0.ERROR;
                new Object[1][0] = cw0.a(bn02.a(), i()).toString(2);
                Iterator<T> it2 = this.m.iterator();
                while (it2.hasNext()) {
                    ((hg6) it2.next()).invoke(this);
                }
            }
            Iterator<T> it3 = this.n.iterator();
            while (it3.hasNext()) {
                ((hg6) it3.next()).invoke(this);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001d, code lost:
        r3 = r3.getLogName();
     */
    @DexIgnore
    public String c(bn0 bn0) {
        String logName;
        switch (to0.c[bn0.c.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                sj0 sj0 = bn0.e;
                return (sj0 == null || logName == null) ? cw0.a((Enum<?>) il0.UNKNOWN_ERROR) : logName;
            case 8:
            case 9:
                return cw0.a((Enum<?>) il0.RESPONSE_TIMEOUT);
            default:
                return b(bn0);
        }
    }

    @DexIgnore
    public void b(ok0 ok0) {
        JSONObject jSONObject;
        JSONObject a2;
        this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(ok0.d).c, ok0.d, (sj0) null, 19);
        nn0 nn0 = this.f;
        if (nn0 != null) {
            nn0.i = false;
        }
        nn0 nn02 = this.f;
        if (!(nn02 == null || (jSONObject = nn02.m) == null || (a2 = cw0.a(jSONObject, bm0.MESSAGE, (Object) b(this.v))) == null)) {
            cw0.a(a2, bm0.ERROR_DETAIL, (Object) ok0.d.a());
        }
        k();
        a();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ qv0(lx0 lx0, ue1 ue1, int i2, int i3) {
        this(lx0, ue1, (i3 & 4) != 0 ? 3 : i2);
    }

    @DexIgnore
    public final qv0 b(hg6<? super qv0, cd6> hg6) {
        if (!this.t) {
            this.l.add(hg6);
        } else if (this.v.c == il0.SUCCESS) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public String b(bn0 bn0) {
        il0 il0;
        if (to0.b[bn0.d.b.ordinal()] == 1) {
            return cw0.a((Enum<?>) ao0.m.a(bn0.d.c.b));
        }
        switch (to0.a[bn0.c.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                il0 = bn0.c;
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                il0 = il0.RESPONSE_ERROR;
                break;
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                il0 = il0.UNKNOWN_ERROR;
                break;
            default:
                il0 = il0.UNKNOWN_ERROR;
                break;
        }
        return cw0.a((Enum<?>) il0);
    }

    @DexIgnore
    public final void a(ne0 ne0) {
        this.g.add(ne0);
    }

    @DexIgnore
    public final void a(bn0 bn0) {
        this.v = bn0;
        a();
    }

    @DexIgnore
    public final void a(gg6<cd6> gg6) {
        j();
        if (e() > 0) {
            this.i = new rm1(this, gg6);
            cc0 cc0 = cc0.DEBUG;
            new Object[1][0] = Long.valueOf(e());
            rm1 rm1 = this.i;
            if (rm1 != null) {
                this.j.postDelayed(rm1, e());
            }
        }
    }

    @DexIgnore
    public final void a(il0 il0) {
        lf0 lf0;
        if (!this.t && !this.u) {
            cc0 cc0 = cc0.DEBUG;
            new Object[1][0] = il0;
            this.u = true;
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0, (ch0) null, (sj0) null, 27);
            ok0 ok0 = this.k;
            if (ok0 == null || ok0.c) {
                a();
                return;
            }
            int i2 = to0.d[il0.ordinal()];
            if (i2 == 1) {
                lf0 = lf0.INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT;
            } else if (i2 == 2) {
                lf0 = lf0.CONNECTION_DROPPED;
            } else if (i2 != 3) {
                lf0 = lf0.INTERRUPTED;
            } else {
                lf0 = lf0.BLUETOOTH_OFF;
            }
            this.y.a(this.k, lf0);
        }
    }

    @DexIgnore
    public final qv0 a(hg6<? super qv0, cd6> hg6) {
        if (!this.t) {
            this.m.add(hg6);
        } else if (this.v.c != il0.SUCCESS) {
            hg6.invoke(this);
        }
        return this;
    }
}
