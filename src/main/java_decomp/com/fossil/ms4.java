package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms4 implements Factory<ls4> {
    @DexIgnore
    public /* final */ Provider<LocationSource> a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;

    @DexIgnore
    public ms4(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ms4 a(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        return new ms4(provider, provider2);
    }

    @DexIgnore
    public static ls4 b(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        return new GetUserLocation(provider.get(), provider2.get());
    }

    @DexIgnore
    public GetUserLocation get() {
        return b(this.a, this.b);
    }
}
