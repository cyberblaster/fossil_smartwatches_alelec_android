package com.fossil;

import androidx.work.impl.WorkDatabase;
import com.fossil.am;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class no implements Runnable {
    @DexIgnore
    public static /* final */ String c; // = tl.a("StopWorkRunnable");
    @DexIgnore
    public lm a;
    @DexIgnore
    public String b;

    @DexIgnore
    public no(lm lmVar, String str) {
        this.a = lmVar;
        this.b = str;
    }

    @DexIgnore
    public void run() {
        WorkDatabase g = this.a.g();
        ao d = g.d();
        g.beginTransaction();
        try {
            if (d.d(this.b) == am.a.RUNNING) {
                d.a(am.a.ENQUEUED, this.b);
            }
            boolean e = this.a.e().e(this.b);
            tl.a().a(c, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", new Object[]{this.b, Boolean.valueOf(e)}), new Throwable[0]);
            g.setTransactionSuccessful();
        } finally {
            g.endTransaction();
        }
    }
}
