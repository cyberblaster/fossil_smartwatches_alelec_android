package com.fossil;

import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n41 extends j61 {
    @DexIgnore
    public UUID[] A; // = new UUID[0];
    @DexIgnore
    public rg1[] B; // = new rg1[0];
    @DexIgnore
    public long C; // = 10000;

    @DexIgnore
    public n41(ue1 ue1) {
        super(lx0.DISCOVER_SERVICES, ue1);
    }

    @DexIgnore
    public void a(long j) {
        this.C = j;
    }

    @DexIgnore
    public void c(ok0 ok0) {
        il0 il0;
        JSONObject jSONObject;
        this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(ok0.d).c, ok0.d, (sj0) null, 19);
        nn0 nn0 = this.f;
        if (nn0 != null) {
            nn0.i = true;
        }
        nn0 nn02 = this.f;
        if (!(nn02 == null || (jSONObject = nn02.m) == null)) {
            cw0.a(jSONObject, bm0.MESSAGE, (Object) cw0.a((Enum<?>) il0.SUCCESS));
        }
        if (this.v.c == il0.SUCCESS) {
            a(ok0);
        }
        bn0 bn0 = this.v;
        if (!(this.A.length == 0)) {
            il0 = il0.SUCCESS;
        } else {
            il0 = il0.EMPTY_SERVICES;
        }
        a(bn0.a(bn0, (lx0) null, (String) null, il0, (ch0) null, (sj0) null, 27));
    }

    @DexIgnore
    public long e() {
        return this.C;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) this.y.getBondState())), bm0.BLUETOOTH_DEVICE_TYPE, (Object) Integer.valueOf(this.y.w.getType()));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(cw0.a(cw0.a(super.i(), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) this.y.getBondState())), bm0.BLUETOOTH_DEVICE_TYPE, (Object) Integer.valueOf(this.y.w.getType())), bm0.SERVICES, (Object) cw0.a(this.A)), bm0.CHARACTERISTICS, (Object) cw0.a(this.B));
    }

    @DexIgnore
    public ok0 l() {
        return new c01(this.y.v);
    }

    @DexIgnore
    public void a(ok0 ok0) {
        c01 c01 = (c01) ok0;
        this.A = c01.j;
        this.B = c01.k;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(cw0.a(new JSONObject(), bm0.SERVICES, (Object) cw0.a(this.A)), bm0.CHARACTERISTICS, (Object) cw0.a(this.B)), 7));
    }
}
