package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l00 {
    @DexIgnore
    public static /* final */ Executor a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            runnable.run();
        }
    }

    @DexIgnore
    public static Executor a() {
        return b;
    }

    @DexIgnore
    public static Executor b() {
        return a;
    }
}
