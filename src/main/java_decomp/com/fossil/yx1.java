package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yx1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ox1 a;

    @DexIgnore
    public yx1(ox1 ox1) {
        this.a = ox1;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void run() {
        this.a.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.a.b.unlock();
            }
        } catch (RuntimeException e) {
            this.a.a.a(e);
        } finally {
            this.a.b.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ yx1(ox1 ox1, nx1 nx1) {
        this(ox1);
    }
}
