package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zh3 extends yh3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends dj3 {
        @DexIgnore
        public a(hj3 hj3) {
            super(hj3);
        }

        @DexIgnore
        public boolean isStateful() {
            return true;
        }
    }

    @DexIgnore
    public zh3(FloatingActionButton floatingActionButton, wi3 wi3) {
        super(floatingActionButton, wi3);
    }

    @DexIgnore
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        Drawable drawable;
        this.b = a();
        this.b.setTintList(colorStateList);
        if (mode != null) {
            this.b.setTintMode(mode);
        }
        this.b.a(this.y.getContext());
        if (i > 0) {
            this.d = a(i, colorStateList);
            xh3 xh3 = this.d;
            y8.a(xh3);
            dj3 dj3 = this.b;
            y8.a(dj3);
            drawable = new LayerDrawable(new Drawable[]{xh3, dj3});
        } else {
            this.d = null;
            drawable = this.b;
        }
        this.c = new RippleDrawable(ui3.b(colorStateList2), drawable, (Drawable) null);
        this.e = this.c;
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        Drawable drawable = this.c;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(ui3.b(colorStateList));
        } else {
            super.b(colorStateList);
        }
    }

    @DexIgnore
    public float e() {
        return this.y.getElevation();
    }

    @DexIgnore
    public void o() {
    }

    @DexIgnore
    public void q() {
        B();
    }

    @DexIgnore
    public boolean v() {
        return false;
    }

    @DexIgnore
    public boolean w() {
        return this.z.a() || !y();
    }

    @DexIgnore
    public void z() {
    }

    @DexIgnore
    public void a(float f, float f2, float f3) {
        if (Build.VERSION.SDK_INT == 21) {
            this.y.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(yh3.G, a(f, f3));
            stateListAnimator.addState(yh3.H, a(f, f2));
            stateListAnimator.addState(yh3.I, a(f, f2));
            stateListAnimator.addState(yh3.J, a(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.y, "elevation", new float[]{f}).setDuration(0));
            int i = Build.VERSION.SDK_INT;
            if (i >= 22 && i <= 24) {
                FloatingActionButton floatingActionButton = this.y;
                arrayList.add(ObjectAnimator.ofFloat(floatingActionButton, View.TRANSLATION_Z, new float[]{floatingActionButton.getTranslationZ()}).setDuration(100));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.y, View.TRANSLATION_Z, new float[]{0.0f}).setDuration(100));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(yh3.F);
            stateListAnimator.addState(yh3.K, animatorSet);
            stateListAnimator.addState(yh3.L, a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.y.setStateListAnimator(stateListAnimator);
        }
        if (w()) {
            B();
        }
    }

    @DexIgnore
    public final Animator a(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(this.y, "elevation", new float[]{f}).setDuration(0)).with(ObjectAnimator.ofFloat(this.y, View.TRANSLATION_Z, new float[]{f2}).setDuration(100));
        animatorSet.setInterpolator(yh3.F);
        return animatorSet;
    }

    @DexIgnore
    public void a(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (this.y.isEnabled()) {
            this.y.setElevation(this.h);
            if (this.y.isPressed()) {
                this.y.setTranslationZ(this.j);
            } else if (this.y.isFocused() || this.y.isHovered()) {
                this.y.setTranslationZ(this.i);
            } else {
                this.y.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        } else {
            this.y.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.y.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public xh3 a(int i, ColorStateList colorStateList) {
        Context context = this.y.getContext();
        hj3 hj3 = this.a;
        y8.a(hj3);
        xh3 xh3 = new xh3(hj3);
        xh3.a(w6.a(context, of3.design_fab_stroke_top_outer_color), w6.a(context, of3.design_fab_stroke_top_inner_color), w6.a(context, of3.design_fab_stroke_end_inner_color), w6.a(context, of3.design_fab_stroke_end_outer_color));
        xh3.a((float) i);
        xh3.a(colorStateList);
        return xh3;
    }

    @DexIgnore
    public dj3 a() {
        hj3 hj3 = this.a;
        y8.a(hj3);
        return new a(hj3);
    }

    @DexIgnore
    public void a(Rect rect) {
        if (this.z.a()) {
            super.a(rect);
        } else if (!y()) {
            int sizeDimension = (this.k - this.y.getSizeDimension()) / 2;
            rect.set(sizeDimension, sizeDimension, sizeDimension, sizeDimension);
        } else {
            rect.set(0, 0, 0, 0);
        }
    }
}
