package com.fossil;

import com.fossil.y30;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r20 implements d30 {
    @DexIgnore
    public /* final */ f30 a;
    @DexIgnore
    public /* final */ p30 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[y30.a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[y30.a.JAVA.ordinal()] = 1;
            a[y30.a.NATIVE.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    public r20(f30 f30, p30 p30) {
        this.a = f30;
        this.b = p30;
    }

    @DexIgnore
    public boolean a(c30 c30) {
        int i = a.a[c30.b.getType().ordinal()];
        if (i == 1) {
            this.a.a(c30);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.b.a(c30);
            return true;
        }
    }
}
