package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ze4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ View u;
    @DexIgnore
    public /* final */ RecyclerView v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public ze4(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, RTLImageView rTLImageView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view2, RecyclerView recyclerView, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = rTLImageView;
        this.s = imageView;
        this.t = flexibleTextView;
        this.u = view2;
        this.v = recyclerView;
        this.w = constraintLayout;
        this.x = flexibleTextView3;
    }
}
