package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.NetworkInfo;
import com.fossil.h16;
import com.fossil.n16;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t06 implements Runnable {
    @DexIgnore
    public static /* final */ n16 A; // = new b();
    @DexIgnore
    public static /* final */ Object x; // = new Object();
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> y; // = new a();
    @DexIgnore
    public static /* final */ AtomicInteger z; // = new AtomicInteger();
    @DexIgnore
    public /* final */ int a; // = z.incrementAndGet();
    @DexIgnore
    public /* final */ Picasso b;
    @DexIgnore
    public /* final */ z06 c;
    @DexIgnore
    public /* final */ u06 d;
    @DexIgnore
    public /* final */ p16 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ l16 g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ n16 j;
    @DexIgnore
    public r06 o;
    @DexIgnore
    public List<r06> p;
    @DexIgnore
    public Bitmap q;
    @DexIgnore
    public Future<?> r;
    @DexIgnore
    public Picasso.LoadedFrom s;
    @DexIgnore
    public Exception t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public Picasso.e w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends n16 {
        @DexIgnore
        public n16.a a(l16 l16, int i) throws IOException {
            throw new IllegalStateException("Unrecognized type of request: " + l16);
        }

        @DexIgnore
        public boolean a(l16 l16) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation a;
        @DexIgnore
        public /* final */ /* synthetic */ RuntimeException b;

        @DexIgnore
        public c(Transformation transformation, RuntimeException runtimeException) {
            this.a = transformation;
            this.b = runtimeException;
        }

        @DexIgnore
        public void run() {
            throw new RuntimeException("Transformation " + this.a.key() + " crashed with exception.", this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder a;

        @DexIgnore
        public d(StringBuilder sb) {
            this.a = sb;
        }

        @DexIgnore
        public void run() {
            throw new NullPointerException(this.a.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation a;

        @DexIgnore
        public e(Transformation transformation) {
            this.a = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.a.key() + " returned input Bitmap but recycled it.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation a;

        @DexIgnore
        public f(Transformation transformation) {
            this.a = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.a.key() + " mutated input Bitmap but failed to recycle the original.");
        }
    }

    @DexIgnore
    public t06(Picasso picasso, z06 z06, u06 u06, p16 p16, r06 r06, n16 n16) {
        this.b = picasso;
        this.c = z06;
        this.d = u06;
        this.e = p16;
        this.o = r06;
        this.f = r06.c();
        this.g = r06.h();
        this.w = r06.g();
        this.h = r06.d();
        this.i = r06.e();
        this.j = n16;
        this.v = n16.a();
    }

    @DexIgnore
    public static Bitmap a(InputStream inputStream, l16 l16) throws IOException {
        d16 d16 = new d16(inputStream);
        long b2 = d16.b(65536);
        BitmapFactory.Options b3 = n16.b(l16);
        boolean a2 = n16.a(b3);
        boolean b4 = t16.b((InputStream) d16);
        d16.a(b2);
        if (b4) {
            byte[] c2 = t16.c((InputStream) d16);
            if (a2) {
                BitmapFactory.decodeByteArray(c2, 0, c2.length, b3);
                n16.a(l16.h, l16.i, b3, l16);
            }
            return BitmapFactory.decodeByteArray(c2, 0, c2.length, b3);
        }
        if (a2) {
            BitmapFactory.decodeStream(d16, (Rect) null, b3);
            n16.a(l16.h, l16.i, b3, l16);
            d16.a(b2);
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(d16, (Rect) null, b3);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    @DexIgnore
    public static boolean a(boolean z2, int i2, int i3, int i4, int i5) {
        return !z2 || i2 > i4 || i3 > i5;
    }

    @DexIgnore
    public void b(r06 r06) {
        boolean z2;
        if (this.o == r06) {
            this.o = null;
            z2 = true;
        } else {
            List<r06> list = this.p;
            z2 = list != null ? list.remove(r06) : false;
        }
        if (z2 && r06.g() == this.w) {
            this.w = b();
        }
        if (this.b.n) {
            t16.a("Hunter", "removed", r06.b.d(), t16.a(this, "from "));
        }
    }

    @DexIgnore
    public r06 c() {
        return this.o;
    }

    @DexIgnore
    public List<r06> d() {
        return this.p;
    }

    @DexIgnore
    public l16 e() {
        return this.g;
    }

    @DexIgnore
    public Exception f() {
        return this.t;
    }

    @DexIgnore
    public String g() {
        return this.f;
    }

    @DexIgnore
    public Picasso.LoadedFrom h() {
        return this.s;
    }

    @DexIgnore
    public int i() {
        return this.h;
    }

    @DexIgnore
    public Picasso j() {
        return this.b;
    }

    @DexIgnore
    public Picasso.e k() {
        return this.w;
    }

    @DexIgnore
    public Bitmap l() {
        return this.q;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public Bitmap m() throws IOException {
        Bitmap bitmap;
        if (f16.shouldReadFromMemoryCache(this.h)) {
            bitmap = this.d.a(this.f);
            if (bitmap != null) {
                this.e.b();
                this.s = Picasso.LoadedFrom.MEMORY;
                if (this.b.n) {
                    t16.a("Hunter", "decoded", this.g.d(), "from cache");
                }
                return bitmap;
            }
        } else {
            bitmap = null;
        }
        this.g.c = this.v == 0 ? g16.OFFLINE.index : this.i;
        n16.a a2 = this.j.a(this.g, this.i);
        if (a2 != null) {
            this.s = a2.c();
            this.u = a2.b();
            bitmap = a2.a();
            if (bitmap == null) {
                InputStream d2 = a2.d();
                try {
                    Bitmap a3 = a(d2, this.g);
                    t16.a(d2);
                    bitmap = a3;
                } catch (Throwable th) {
                    t16.a(d2);
                    throw th;
                }
            }
        }
        if (bitmap != null) {
            if (this.b.n) {
                t16.a("Hunter", "decoded", this.g.d());
            }
            this.e.a(bitmap);
            if (this.g.f() || this.u != 0) {
                synchronized (x) {
                    if (this.g.e() || this.u != 0) {
                        bitmap = a(this.g, bitmap, this.u);
                        if (this.b.n) {
                            t16.a("Hunter", "transformed", this.g.d());
                        }
                    }
                    if (this.g.b()) {
                        bitmap = a(this.g.g, bitmap);
                        if (this.b.n) {
                            t16.a("Hunter", "transformed", this.g.d(), "from custom transformations");
                        }
                    }
                }
                if (bitmap != null) {
                    this.e.b(bitmap);
                }
            }
        }
        return bitmap;
    }

    @DexIgnore
    public boolean n() {
        Future<?> future = this.r;
        return future != null && future.isCancelled();
    }

    @DexIgnore
    public boolean o() {
        return this.j.b();
    }

    @DexIgnore
    public void run() {
        try {
            a(this.g);
            if (this.b.n) {
                t16.a("Hunter", "executing", t16.a(this));
            }
            this.q = m();
            if (this.q == null) {
                this.c.c(this);
            } else {
                this.c.b(this);
            }
        } catch (Downloader.a e2) {
            if (!e2.localCacheOnly || e2.responseCode != 504) {
                this.t = e2;
            }
            this.c.c(this);
        } catch (h16.a e3) {
            this.t = e3;
            this.c.d(this);
        } catch (IOException e4) {
            this.t = e4;
            this.c.d(this);
        } catch (OutOfMemoryError e5) {
            StringWriter stringWriter = new StringWriter();
            this.e.a().a(new PrintWriter(stringWriter));
            this.t = new RuntimeException(stringWriter.toString(), e5);
            this.c.c(this);
        } catch (Exception e6) {
            this.t = e6;
            this.c.c(this);
        } catch (Throwable th) {
            Thread.currentThread().setName("Picasso-Idle");
            throw th;
        }
        Thread.currentThread().setName("Picasso-Idle");
    }

    @DexIgnore
    public final Picasso.e b() {
        Picasso.e eVar = Picasso.e.LOW;
        List<r06> list = this.p;
        boolean z2 = true;
        boolean z3 = list != null && !list.isEmpty();
        if (this.o == null && !z3) {
            z2 = false;
        }
        if (!z2) {
            return eVar;
        }
        r06 r06 = this.o;
        if (r06 != null) {
            eVar = r06.g();
        }
        if (z3) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                Picasso.e g2 = this.p.get(i2).g();
                if (g2.ordinal() > eVar.ordinal()) {
                    eVar = g2;
                }
            }
        }
        return eVar;
    }

    @DexIgnore
    public void a(r06 r06) {
        boolean z2 = this.b.n;
        l16 l16 = r06.b;
        if (this.o == null) {
            this.o = r06;
            if (z2) {
                List<r06> list = this.p;
                if (list == null || list.isEmpty()) {
                    t16.a("Hunter", "joined", l16.d(), "to empty hunter");
                } else {
                    t16.a("Hunter", "joined", l16.d(), t16.a(this, "to "));
                }
            }
        } else {
            if (this.p == null) {
                this.p = new ArrayList(3);
            }
            this.p.add(r06);
            if (z2) {
                t16.a("Hunter", "joined", l16.d(), t16.a(this, "to "));
            }
            Picasso.e g2 = r06.g();
            if (g2.ordinal() > this.w.ordinal()) {
                this.w = g2;
            }
        }
    }

    @DexIgnore
    public boolean a() {
        Future<?> future;
        if (this.o != null) {
            return false;
        }
        List<r06> list = this.p;
        if ((list == null || list.isEmpty()) && (future = this.r) != null && future.cancel(false)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean a(boolean z2, NetworkInfo networkInfo) {
        if (!(this.v > 0)) {
            return false;
        }
        this.v--;
        return this.j.a(z2, networkInfo);
    }

    @DexIgnore
    public static void a(l16 l16) {
        String a2 = l16.a();
        StringBuilder sb = y.get();
        sb.ensureCapacity(a2.length() + 8);
        sb.replace(8, sb.length(), a2);
        Thread.currentThread().setName(sb.toString());
    }

    @DexIgnore
    public static t06 a(Picasso picasso, z06 z06, u06 u06, p16 p16, r06 r06) {
        l16 h2 = r06.h();
        List<n16> a2 = picasso.a();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            n16 n16 = a2.get(i2);
            if (n16.a(h2)) {
                return new t06(picasso, z06, u06, p16, r06, n16);
            }
        }
        return new t06(picasso, z06, u06, p16, r06, A);
    }

    @DexIgnore
    public static Bitmap a(List<Transformation> list, Bitmap bitmap) {
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            Transformation transformation = list.get(i2);
            try {
                Bitmap transform = transformation.transform(bitmap);
                if (transform == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Transformation ");
                    sb.append(transformation.key());
                    sb.append(" returned null after ");
                    sb.append(i2);
                    sb.append(" previous transformation(s).\n\nTransformation list:\n");
                    for (Transformation key : list) {
                        sb.append(key.key());
                        sb.append(10);
                    }
                    Picasso.p.post(new d(sb));
                    return null;
                } else if (transform == bitmap && bitmap.isRecycled()) {
                    Picasso.p.post(new e(transformation));
                    return null;
                } else if (transform == bitmap || bitmap.isRecycled()) {
                    i2++;
                    bitmap = transform;
                } else {
                    Picasso.p.post(new f(transformation));
                    return null;
                }
            } catch (RuntimeException e2) {
                Picasso.p.post(new c(transformation, e2));
                return null;
            }
        }
        return bitmap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c4  */
    public static Bitmap a(l16 l16, Bitmap bitmap, int i2) {
        int i3;
        int i4;
        int i5;
        Bitmap createBitmap;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        int i6;
        int i7;
        int i8;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        boolean z2 = l16.l;
        Matrix matrix = new Matrix();
        int i9 = 0;
        if (l16.e()) {
            int i10 = l16.h;
            int i11 = l16.i;
            float f7 = l16.m;
            if (f7 != 0.0f) {
                if (l16.p) {
                    matrix.setRotate(f7, l16.n, l16.o);
                } else {
                    matrix.setRotate(f7);
                }
            }
            if (l16.j) {
                float f8 = (float) i10;
                float f9 = (float) width;
                float f10 = f8 / f9;
                float f11 = (float) i11;
                float f12 = (float) height;
                float f13 = f11 / f12;
                if (f10 > f13) {
                    int ceil = (int) Math.ceil((double) (f12 * (f13 / f10)));
                    i8 = (height - ceil) / 2;
                    f13 = f11 / ((float) ceil);
                    i6 = ceil;
                    f6 = f10;
                    i7 = width;
                } else {
                    int ceil2 = (int) Math.ceil((double) (f9 * (f10 / f13)));
                    f6 = f8 / ((float) ceil2);
                    i6 = height;
                    i9 = (width - ceil2) / 2;
                    i7 = ceil2;
                    i8 = 0;
                }
                if (a(z2, width, height, i10, i11)) {
                    matrix.preScale(f6, f13);
                }
                i5 = i8;
                i4 = i7;
                i3 = i6;
                if (i2 != 0) {
                    matrix.preRotate((float) i2);
                }
                createBitmap = Bitmap.createBitmap(bitmap, i9, i5, i4, i3, matrix, true);
                if (createBitmap != bitmap) {
                    return bitmap;
                }
                bitmap.recycle();
                return createBitmap;
            } else if (l16.k) {
                float f14 = ((float) i10) / ((float) width);
                float f15 = ((float) i11) / ((float) height);
                if (f14 >= f15) {
                    f14 = f15;
                }
                if (a(z2, width, height, i10, i11)) {
                    matrix.preScale(f14, f14);
                }
            } else if (!((i10 == 0 && i11 == 0) || (i10 == width && i11 == height))) {
                if (i10 != 0) {
                    f2 = (float) i10;
                    f3 = (float) width;
                } else {
                    f2 = (float) i11;
                    f3 = (float) height;
                }
                float f16 = f2 / f3;
                if (i11 != 0) {
                    f5 = (float) i11;
                    f4 = (float) height;
                } else {
                    f5 = (float) i10;
                    f4 = (float) width;
                }
                float f17 = f5 / f4;
                if (a(z2, width, height, i10, i11)) {
                    matrix.preScale(f16, f17);
                }
            }
        }
        i4 = width;
        i3 = height;
        i5 = 0;
        if (i2 != 0) {
        }
        createBitmap = Bitmap.createBitmap(bitmap, i9, i5, i4, i3, matrix, true);
        if (createBitmap != bitmap) {
        }
    }
}
