package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class of {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968745;
    @DexIgnore
    public static /* final */ int fastScrollEnabled; // = 2130969306;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalThumbDrawable; // = 2130969307;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalTrackDrawable; // = 2130969308;
    @DexIgnore
    public static /* final */ int fastScrollVerticalThumbDrawable; // = 2130969309;
    @DexIgnore
    public static /* final */ int fastScrollVerticalTrackDrawable; // = 2130969310;
    @DexIgnore
    public static /* final */ int font; // = 2130969326;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969331;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969332;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969333;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969334;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969335;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969336;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969337;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969338;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969339;
    @DexIgnore
    public static /* final */ int layoutManager; // = 2130969478;
    @DexIgnore
    public static /* final */ int recyclerViewStyle; // = 2130969717;
    @DexIgnore
    public static /* final */ int reverseLayout; // = 2130969725;
    @DexIgnore
    public static /* final */ int spanCount; // = 2130969863;
    @DexIgnore
    public static /* final */ int stackFromEnd; // = 2130969870;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130970059;
}
