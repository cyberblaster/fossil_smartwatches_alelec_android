package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum NotificationType {
    UNSUPPORTED((byte) 0, 1),
    INCOMING_CALL((byte) 1, 2),
    TEXT((byte) 2, 4),
    NOTIFICATION((byte) 3, 8),
    EMAIL((byte) 4, 16),
    CALENDAR((byte) 5, 32),
    MISSED_CALL((byte) 6, 64),
    REMOVED((byte) 7, 128);
    
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public NotificationType(byte b2, int i) {
        this.a = b2;
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final byte b() {
        return this.a;
    }
}
