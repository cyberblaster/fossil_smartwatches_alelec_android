package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ln<T> extends mn<T> {
    @DexIgnore
    public static /* final */ String h; // = tl.a("BrdcstRcvrCnstrntTrckr");
    @DexIgnore
    public /* final */ BroadcastReceiver g; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                ln.this.a(context, intent);
            }
        }
    }

    @DexIgnore
    public ln(Context context, to toVar) {
        super(context, toVar);
    }

    @DexIgnore
    public abstract void a(Context context, Intent intent);

    @DexIgnore
    public void b() {
        tl.a().a(h, String.format("%s: registering receiver", new Object[]{getClass().getSimpleName()}), new Throwable[0]);
        this.b.registerReceiver(this.g, d());
    }

    @DexIgnore
    public void c() {
        tl.a().a(h, String.format("%s: unregistering receiver", new Object[]{getClass().getSimpleName()}), new Throwable[0]);
        this.b.unregisterReceiver(this.g);
    }

    @DexIgnore
    public abstract IntentFilter d();
}
