package com.fossil;

import com.fossil.dt;
import com.fossil.lt;
import com.fossil.s00;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ht<R> implements dt.b<R>, s00.f {
    @DexIgnore
    public static /* final */ c C; // = new c();
    @DexIgnore
    public dt<R> A;
    @DexIgnore
    public volatile boolean B;
    @DexIgnore
    public /* final */ e a;
    @DexIgnore
    public /* final */ u00 b;
    @DexIgnore
    public /* final */ lt.a c;
    @DexIgnore
    public /* final */ v8<ht<?>> d;
    @DexIgnore
    public /* final */ c e;
    @DexIgnore
    public /* final */ it f;
    @DexIgnore
    public /* final */ uu g;
    @DexIgnore
    public /* final */ uu h;
    @DexIgnore
    public /* final */ uu i;
    @DexIgnore
    public /* final */ uu j;
    @DexIgnore
    public /* final */ AtomicInteger o;
    @DexIgnore
    public vr p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public rt<?> u;
    @DexIgnore
    public pr v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public mt x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public lt<?> z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ oz a;

        @DexIgnore
        public a(oz ozVar) {
            this.a = ozVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.a.a()) {
                synchronized (ht.this) {
                    if (ht.this.a.a(this.a)) {
                        ht.this.a(this.a);
                    }
                    ht.this.b();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ oz a;

        @DexIgnore
        public b(oz ozVar) {
            this.a = ozVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.a.a()) {
                synchronized (ht.this) {
                    if (ht.this.a.a(this.a)) {
                        ht.this.z.d();
                        ht.this.b(this.a);
                        ht.this.c(this.a);
                    }
                    ht.this.b();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <R> lt<R> a(rt<R> rtVar, boolean z, vr vrVar, lt.a aVar) {
            return new lt(rtVar, z, true, vrVar, aVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ oz a;
        @DexIgnore
        public /* final */ Executor b;

        @DexIgnore
        public d(oz ozVar, Executor executor) {
            this.a = ozVar;
            this.b = executor;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.a.equals(((d) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Iterable<d> {
        @DexIgnore
        public /* final */ List<d> a;

        @DexIgnore
        public e() {
            this(new ArrayList(2));
        }

        @DexIgnore
        public static d c(oz ozVar) {
            return new d(ozVar, l00.a());
        }

        @DexIgnore
        public void a(oz ozVar, Executor executor) {
            this.a.add(new d(ozVar, executor));
        }

        @DexIgnore
        public void b(oz ozVar) {
            this.a.remove(c(ozVar));
        }

        @DexIgnore
        public void clear() {
            this.a.clear();
        }

        @DexIgnore
        public boolean isEmpty() {
            return this.a.isEmpty();
        }

        @DexIgnore
        public Iterator<d> iterator() {
            return this.a.iterator();
        }

        @DexIgnore
        public int size() {
            return this.a.size();
        }

        @DexIgnore
        public e(List<d> list) {
            this.a = list;
        }

        @DexIgnore
        public boolean a(oz ozVar) {
            return this.a.contains(c(ozVar));
        }

        @DexIgnore
        public e a() {
            return new e(new ArrayList(this.a));
        }
    }

    @DexIgnore
    public ht(uu uuVar, uu uuVar2, uu uuVar3, uu uuVar4, it itVar, lt.a aVar, v8<ht<?>> v8Var) {
        this(uuVar, uuVar2, uuVar3, uuVar4, itVar, aVar, v8Var, C);
    }

    @DexIgnore
    public synchronized ht<R> a(vr vrVar, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.p = vrVar;
        this.q = z2;
        this.r = z3;
        this.s = z4;
        this.t = z5;
        return this;
    }

    @DexIgnore
    public synchronized void b(dt<R> dtVar) {
        this.A = dtVar;
        (dtVar.m() ? this.g : c()).execute(dtVar);
    }

    @DexIgnore
    public synchronized void c(oz ozVar) {
        boolean z2;
        this.b.a();
        this.a.b(ozVar);
        if (this.a.isEmpty()) {
            a();
            if (!this.w) {
                if (!this.y) {
                    z2 = false;
                    if (z2 && this.o.get() == 0) {
                        i();
                    }
                }
            }
            z2 = true;
            i();
        }
    }

    @DexIgnore
    public u00 d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e() {
        return this.y || this.w || this.B;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.f.a(r4, r1, (com.fossil.lt<?>) null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.ht.a(r4, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    @DexIgnore
    public void f() {
        synchronized (this) {
            this.b.a();
            if (this.B) {
                i();
            } else if (this.a.isEmpty()) {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            } else if (!this.y) {
                this.y = true;
                vr vrVar = this.p;
                e a2 = this.a.a();
                a(a2.size() + 1);
            } else {
                throw new IllegalStateException("Already failed once");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r5.f.a(r5, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (r0.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.ht.b(r5, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        return;
     */
    @DexIgnore
    public void g() {
        synchronized (this) {
            this.b.a();
            if (this.B) {
                this.u.a();
                i();
            } else if (this.a.isEmpty()) {
                throw new IllegalStateException("Received a resource without any callbacks to notify");
            } else if (!this.w) {
                this.z = this.e.a(this.u, this.q, this.p, this.c);
                this.w = true;
                e a2 = this.a.a();
                a(a2.size() + 1);
                vr vrVar = this.p;
                lt<?> ltVar = this.z;
            } else {
                throw new IllegalStateException("Already have resource");
            }
        }
    }

    @DexIgnore
    public boolean h() {
        return this.t;
    }

    @DexIgnore
    public final synchronized void i() {
        if (this.p != null) {
            this.a.clear();
            this.p = null;
            this.z = null;
            this.u = null;
            this.y = false;
            this.B = false;
            this.w = false;
            this.A.a(false);
            this.A = null;
            this.x = null;
            this.v = null;
            this.d.a(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public ht(uu uuVar, uu uuVar2, uu uuVar3, uu uuVar4, it itVar, lt.a aVar, v8<ht<?>> v8Var, c cVar) {
        this.a = new e();
        this.b = u00.b();
        this.o = new AtomicInteger();
        this.g = uuVar;
        this.h = uuVar2;
        this.i = uuVar3;
        this.j = uuVar4;
        this.f = itVar;
        this.c = aVar;
        this.d = v8Var;
        this.e = cVar;
    }

    @DexIgnore
    public void b(oz ozVar) {
        try {
            ozVar.a(this.z, this.v);
        } catch (Throwable th) {
            throw new xs(th);
        }
    }

    @DexIgnore
    public synchronized void a(oz ozVar, Executor executor) {
        this.b.a();
        this.a.a(ozVar, executor);
        boolean z2 = true;
        if (this.w) {
            a(1);
            executor.execute(new b(ozVar));
        } else if (this.y) {
            a(1);
            executor.execute(new a(ozVar));
        } else {
            if (this.B) {
                z2 = false;
            }
            q00.a(z2, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    @DexIgnore
    public void b() {
        lt<?> ltVar;
        synchronized (this) {
            this.b.a();
            q00.a(e(), "Not yet complete!");
            int decrementAndGet = this.o.decrementAndGet();
            q00.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                ltVar = this.z;
                i();
            } else {
                ltVar = null;
            }
        }
        if (ltVar != null) {
            ltVar.g();
        }
    }

    @DexIgnore
    public final uu c() {
        if (this.r) {
            return this.i;
        }
        return this.s ? this.j : this.h;
    }

    @DexIgnore
    public void a(oz ozVar) {
        try {
            ozVar.a(this.x);
        } catch (Throwable th) {
            throw new xs(th);
        }
    }

    @DexIgnore
    public void a() {
        if (!e()) {
            this.B = true;
            this.A.a();
            this.f.a(this, this.p);
        }
    }

    @DexIgnore
    public synchronized void a(int i2) {
        q00.a(e(), "Not yet complete!");
        if (this.o.getAndAdd(i2) == 0 && this.z != null) {
            this.z.d();
        }
    }

    @DexIgnore
    public void a(rt<R> rtVar, pr prVar) {
        synchronized (this) {
            this.u = rtVar;
            this.v = prVar;
        }
        g();
    }

    @DexIgnore
    public void a(mt mtVar) {
        synchronized (this) {
            this.x = mtVar;
        }
        f();
    }

    @DexIgnore
    public void a(dt<?> dtVar) {
        c().execute(dtVar);
    }
}
