package com.fossil;

import com.fossil.fn2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sj2 extends fn2<sj2, a> implements to2 {
    @DexIgnore
    public static /* final */ sj2 zzg;
    @DexIgnore
    public static volatile yo2<sj2> zzh;
    @DexIgnore
    public on2 zzc; // = fn2.l();
    @DexIgnore
    public on2 zzd; // = fn2.l();
    @DexIgnore
    public nn2<lj2> zze; // = fn2.m();
    @DexIgnore
    public nn2<tj2> zzf; // = fn2.m();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<sj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(sj2.zzg);
        }

        @DexIgnore
        public final a a(Iterable<? extends Long> iterable) {
            f();
            ((sj2) this.b).a(iterable);
            return this;
        }

        @DexIgnore
        public final a b(Iterable<? extends Long> iterable) {
            f();
            ((sj2) this.b).b(iterable);
            return this;
        }

        @DexIgnore
        public final a c(Iterable<? extends lj2> iterable) {
            f();
            ((sj2) this.b).c(iterable);
            return this;
        }

        @DexIgnore
        public final a d(Iterable<? extends tj2> iterable) {
            f();
            ((sj2) this.b).d(iterable);
            return this;
        }

        @DexIgnore
        public final a j() {
            f();
            ((sj2) this.b).w();
            return this;
        }

        @DexIgnore
        public final a k() {
            f();
            ((sj2) this.b).x();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(int i) {
            f();
            ((sj2) this.b).d(i);
            return this;
        }

        @DexIgnore
        public final a b(int i) {
            f();
            ((sj2) this.b).e(i);
            return this;
        }
    }

    /*
    static {
        sj2 sj2 = new sj2();
        zzg = sj2;
        fn2.a(sj2.class, sj2);
    }
    */

    @DexIgnore
    public static a A() {
        return (a) zzg.h();
    }

    @DexIgnore
    public static sj2 B() {
        return zzg;
    }

    @DexIgnore
    public final void a(Iterable<? extends Long> iterable) {
        if (!this.zzc.zza()) {
            this.zzc = fn2.a(this.zzc);
        }
        ql2.a(iterable, this.zzc);
    }

    @DexIgnore
    public final void b(Iterable<? extends Long> iterable) {
        if (!this.zzd.zza()) {
            this.zzd = fn2.a(this.zzd);
        }
        ql2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final void c(Iterable<? extends lj2> iterable) {
        y();
        ql2.a(iterable, this.zze);
    }

    @DexIgnore
    public final void d(int i) {
        y();
        this.zze.remove(i);
    }

    @DexIgnore
    public final void e(int i) {
        z();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final List<Long> n() {
        return this.zzc;
    }

    @DexIgnore
    public final int o() {
        return this.zzc.size();
    }

    @DexIgnore
    public final List<Long> p() {
        return this.zzd;
    }

    @DexIgnore
    public final int q() {
        return this.zzd.size();
    }

    @DexIgnore
    public final List<lj2> r() {
        return this.zze;
    }

    @DexIgnore
    public final int s() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<tj2> t() {
        return this.zzf;
    }

    @DexIgnore
    public final int v() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void w() {
        this.zzc = fn2.l();
    }

    @DexIgnore
    public final void x() {
        this.zzd = fn2.l();
    }

    @DexIgnore
    public final void y() {
        if (!this.zze.zza()) {
            this.zze = fn2.a(this.zze);
        }
    }

    @DexIgnore
    public final void z() {
        if (!this.zzf.zza()) {
            this.zzf = fn2.a(this.zzf);
        }
    }

    @DexIgnore
    public final tj2 c(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final void d(Iterable<? extends tj2> iterable) {
        z();
        ql2.a(iterable, this.zzf);
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new sj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzg, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zzc", "zzd", "zze", lj2.class, "zzf", tj2.class});
            case 4:
                return zzg;
            case 5:
                yo2<sj2> yo2 = zzh;
                if (yo2 == null) {
                    synchronized (sj2.class) {
                        yo2 = zzh;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzg);
                            zzh = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final lj2 b(int i) {
        return this.zze.get(i);
    }
}
