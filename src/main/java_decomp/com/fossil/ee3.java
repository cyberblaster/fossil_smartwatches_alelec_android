package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee3 extends e22 implements rd3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ee3> CREATOR; // = new fe3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ List<te3> b;

    @DexIgnore
    public ee3(String str, List<te3> list) {
        this.a = str;
        this.b = list;
        w12.a(this.a);
        w12.a(this.b);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ee3.class != obj.getClass()) {
            return false;
        }
        ee3 ee3 = (ee3) obj;
        String str = this.a;
        if (str == null ? ee3.a != null : !str.equals(ee3.a)) {
            return false;
        }
        List<te3> list = this.b;
        List<te3> list2 = ee3.b;
        return list == null ? list2 == null : list.equals(list2);
    }

    @DexIgnore
    public final int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = ((str != null ? str.hashCode() : 0) + 31) * 31;
        List<te3> list = this.b;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final String p() {
        return this.a;
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18 + String.valueOf(valueOf).length());
        sb.append("CapabilityInfo{");
        sb.append(str);
        sb.append(", ");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, p(), false);
        g22.c(parcel, 3, this.b, false);
        g22.a(parcel, a2);
    }
}
