package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.uw1;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wv2 extends vv1<rv1.d.C0044d> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends bg2 {
        @DexIgnore
        public /* final */ rc3<Void> a;

        @DexIgnore
        public a(rc3<Void> rc3) {
            this.a = rc3;
        }

        @DexIgnore
        public final void a(xf2 xf2) {
            cx1.a(xf2.o(), this.a);
        }
    }

    @DexIgnore
    public wv2(Context context) {
        super(context, aw2.c, null, (zw1) new kw1());
    }

    @DexIgnore
    public final ag2 a(rc3<Boolean> rc3) {
        return new xw2(this, rc3);
    }

    @DexIgnore
    public qc3<Void> a(yv2 yv2) {
        return cx1.a(a((uw1.a<?>) vw1.a(yv2, yv2.class.getSimpleName())));
    }

    @DexIgnore
    public qc3<Void> a(LocationRequest locationRequest, yv2 yv2, Looper looper) {
        rg2 a2 = rg2.a(locationRequest);
        uw1 a3 = vw1.a(yv2, yg2.a(looper), yv2.class.getSimpleName());
        return a(new vw2(this, a3, a2, a3), new ww2(this, a3.b()));
    }

    @DexIgnore
    public qc3<Location> i() {
        return a(new uw2(this));
    }
}
