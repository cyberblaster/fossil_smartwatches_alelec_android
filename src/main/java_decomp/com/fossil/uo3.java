package com.fossil;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.LinkedList;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo3 {
    @DexIgnore
    public /* final */ Readable a;
    @DexIgnore
    public /* final */ Reader b;
    @DexIgnore
    public /* final */ CharBuffer c; // = oo3.a();
    @DexIgnore
    public /* final */ char[] d; // = this.c.array();
    @DexIgnore
    public /* final */ Queue<String> e; // = new LinkedList();
    @DexIgnore
    public /* final */ so3 f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends so3 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(String str, String str2) {
            uo3.this.e.add(str);
        }
    }

    @DexIgnore
    public uo3(Readable readable) {
        jk3.a(readable);
        this.a = readable;
        this.b = readable instanceof Reader ? (Reader) readable : null;
    }

    @DexIgnore
    public String a() throws IOException {
        int i;
        while (true) {
            if (this.e.peek() != null) {
                break;
            }
            this.c.clear();
            Reader reader = this.b;
            if (reader != null) {
                char[] cArr = this.d;
                i = reader.read(cArr, 0, cArr.length);
            } else {
                i = this.a.read(this.c);
            }
            if (i == -1) {
                this.f.a();
                break;
            }
            this.f.a(this.d, 0, i);
        }
        return this.e.poll();
    }
}
