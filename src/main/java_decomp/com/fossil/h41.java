package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h41 extends xk1 {
    @DexIgnore
    public /* final */ o70 R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ h41(ue1 ue1, q41 q41, o70 o70, short s, String str, int i) {
        super(ue1, q41, eh1.SEND_TRACK_INFO, true, (i & 8) != 0 ? lk1.b.b(ue1.t, w31.MUSIC_CONTROL) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? ze0.a("UUID.randomUUID().toString()") : str, 32);
        this.R = o70;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.TRACK_INFO, (Object) this.R.a());
    }

    @DexIgnore
    public byte[] n() {
        tk0 tk0 = tk0.d;
        short s = this.C;
        w40 w40 = this.x.a().i().get(Short.valueOf(w31.MUSIC_CONTROL.a));
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        return tk0.a(s, w40, this.R);
    }
}
