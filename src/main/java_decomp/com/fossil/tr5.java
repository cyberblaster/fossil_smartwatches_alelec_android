package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr5 extends pr5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ int l; // = (Calendar.getInstance().get(1) - 110);
    @DexIgnore
    public Calendar e;
    @DexIgnore
    public int f; // = this.e.get(5);
    @DexIgnore
    public int g; // = this.e.get(2);
    @DexIgnore
    public int h; // = this.e.get(1);
    @DexIgnore
    public int i; // = this.e.getActualMaximum(5);
    @DexIgnore
    public /* final */ qr5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = tr5.class.getSimpleName();
        wg6.a((Object) simpleName, "BirthdayPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public tr5(qr5 qr5) {
        wg6.b(qr5, "mView");
        this.j = qr5;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        this.e = instance;
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current day: " + this.f + ", new day: " + i2);
        if (i2 >= 1 && i2 <= this.i) {
            this.f = i2;
        }
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current month: " + this.g + ", new month: " + i2);
        if (i2 >= 1 && i2 <= 12) {
            this.g = i2;
            k();
        }
    }

    @DexIgnore
    public void c(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current year: " + this.h + ", new year: " + i2);
        if (i2 >= l) {
            this.h = i2;
        }
    }

    @DexIgnore
    public void f() {
        i();
        this.j.a(new lc6(1, Integer.valueOf(this.i)), new lc6(1, 12), new lc6(Integer.valueOf(l), Integer.valueOf(this.e.get(1))));
    }

    @DexIgnore
    public void g() {
        i();
    }

    @DexIgnore
    public void h() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "confirmBirthday " + this.f + '/' + this.g + '/' + this.h);
        Calendar instance = Calendar.getInstance();
        instance.set(this.h, this.g + -1, this.f);
        qr5 qr5 = this.j;
        wg6.a((Object) instance, "calendar");
        Date time = instance.getTime();
        wg6.a((Object) time, "calendar.time");
        qr5.c(time);
    }

    @DexIgnore
    public final void i() {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        this.e = instance;
        this.f = this.e.get(5);
        this.g = this.e.get(2);
        this.h = this.e.get(1);
        this.i = this.e.getActualMaximum(5);
    }

    @DexIgnore
    public void j() {
        this.j.a(this);
    }

    @DexIgnore
    public final void k() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.h, this.g - 1, 1);
        this.i = instance.getActualMaximum(5);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Day range of " + this.g + '/' + this.h + ": " + this.i);
        this.j.b(1, this.i);
    }
}
