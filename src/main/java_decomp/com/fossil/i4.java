package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.fossil.o4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i4 extends k4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements o4.a {
        @DexIgnore
        public a(i4 i4Var) {
        }

        @DexIgnore
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @DexIgnore
    public void a() {
        o4.r = new a(this);
    }
}
