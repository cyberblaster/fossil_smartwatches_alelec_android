package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg0 extends em0 {
    @DexIgnore
    public static /* final */ if0 CREATOR; // = new if0((qg6) null);
    @DexIgnore
    public /* final */ r70 c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public zg0(byte b, int i, r70 r70) {
        super(xh0.APP_NOTIFICATION_EVENT, b);
        this.c = r70;
        this.d = i;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.t0, (Object) this.c.a()), bm0.NOTIFICATION_UID, (Object) Integer.valueOf(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ zg0(Parcel parcel, qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(r70.class.getClassLoader());
        if (readParcelable != null) {
            this.c = (r70) readParcelable;
            this.d = parcel.readInt();
            return;
        }
        wg6.a();
        throw null;
    }
}
