package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lu0 implements Parcelable.Creator<dw0> {
    @DexIgnore
    public /* synthetic */ lu0(qg6 qg6) {
    }

    @DexIgnore
    public dw0 createFromParcel(Parcel parcel) {
        return new dw0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new dw0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m39createFromParcel(Parcel parcel) {
        return new dw0(parcel, (qg6) null);
    }
}
