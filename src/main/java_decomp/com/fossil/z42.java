package com.fossil;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z42 implements ThreadFactory {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ AtomicInteger b;
    @DexIgnore
    public /* final */ ThreadFactory c;

    @DexIgnore
    public z42(String str) {
        this(str, 0);
    }

    @DexIgnore
    public Thread newThread(Runnable runnable) {
        Thread newThread = this.c.newThread(new a52(runnable, 0));
        String str = this.a;
        int andIncrement = this.b.getAndIncrement();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13);
        sb.append(str);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        newThread.setName(sb.toString());
        return newThread;
    }

    @DexIgnore
    public z42(String str, int i) {
        this.b = new AtomicInteger();
        this.c = Executors.defaultThreadFactory();
        w12.a(str, (Object) "Name must not be null");
        this.a = str;
    }
}
