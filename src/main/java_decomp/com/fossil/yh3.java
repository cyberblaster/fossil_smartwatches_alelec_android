package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yh3 {
    @DexIgnore
    public static /* final */ TimeInterpolator F; // = yf3.c;
    @DexIgnore
    public static /* final */ int[] G; // = {16842919, 16842910};
    @DexIgnore
    public static /* final */ int[] H; // = {16843623, 16842908, 16842910};
    @DexIgnore
    public static /* final */ int[] I; // = {16842908, 16842910};
    @DexIgnore
    public static /* final */ int[] J; // = {16843623, 16842910};
    @DexIgnore
    public static /* final */ int[] K; // = {16842910};
    @DexIgnore
    public static /* final */ int[] L; // = new int[0];
    @DexIgnore
    public /* final */ Rect A; // = new Rect();
    @DexIgnore
    public /* final */ RectF B; // = new RectF();
    @DexIgnore
    public /* final */ RectF C; // = new RectF();
    @DexIgnore
    public /* final */ Matrix D; // = new Matrix();
    @DexIgnore
    public ViewTreeObserver.OnPreDrawListener E;
    @DexIgnore
    public hj3 a;
    @DexIgnore
    public dj3 b;
    @DexIgnore
    public Drawable c;
    @DexIgnore
    public xh3 d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ hi3 l;
    @DexIgnore
    public fg3 m;
    @DexIgnore
    public fg3 n;
    @DexIgnore
    public Animator o;
    @DexIgnore
    public fg3 p;
    @DexIgnore
    public fg3 q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s; // = 1.0f;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u; // = 0;
    @DexIgnore
    public ArrayList<Animator.AnimatorListener> v;
    @DexIgnore
    public ArrayList<Animator.AnimatorListener> w;
    @DexIgnore
    public ArrayList<i> x;
    @DexIgnore
    public /* final */ FloatingActionButton y;
    @DexIgnore
    public /* final */ wi3 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ j c;

        @DexIgnore
        public a(boolean z, j jVar) {
            this.b = z;
            this.c = jVar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            int unused = yh3.this.u = 0;
            Animator unused2 = yh3.this.o = null;
            if (!this.a) {
                yh3.this.y.a(this.b ? 8 : 4, this.b);
                j jVar = this.c;
                if (jVar != null) {
                    jVar.b();
                }
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yh3.this.y.a(0, this.b);
            int unused = yh3.this.u = 1;
            Animator unused2 = yh3.this.o = animator;
            this.a = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ j b;

        @DexIgnore
        public b(boolean z, j jVar) {
            this.a = z;
            this.b = jVar;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            int unused = yh3.this.u = 0;
            Animator unused2 = yh3.this.o = null;
            j jVar = this.b;
            if (jVar != null) {
                jVar.a();
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yh3.this.y.a(0, this.a);
            int unused = yh3.this.u = 2;
            Animator unused2 = yh3.this.o = animator;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends eg3 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        /* renamed from: a */
        public Matrix evaluate(float f, Matrix matrix, Matrix matrix2) {
            float unused = yh3.this.s = f;
            return super.a(f, matrix, matrix2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements TypeEvaluator<Float> {
        @DexIgnore
        public FloatEvaluator a; // = new FloatEvaluator();

        @DexIgnore
        public d(yh3 yh3) {
        }

        @DexIgnore
        /* renamed from: a */
        public Float evaluate(float f, Float f2, Float f3) {
            float floatValue = this.a.evaluate(f, f2, f3).floatValue();
            if (floatValue < 0.1f) {
                floatValue = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            return Float.valueOf(floatValue);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            yh3.this.s();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends l {
        @DexIgnore
        public f(yh3 yh3) {
            super(yh3, (a) null);
        }

        @DexIgnore
        public float a() {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends l {
        @DexIgnore
        public g() {
            super(yh3.this, (a) null);
        }

        @DexIgnore
        public float a() {
            yh3 yh3 = yh3.this;
            return yh3.h + yh3.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends l {
        @DexIgnore
        public h() {
            super(yh3.this, (a) null);
        }

        @DexIgnore
        public float a() {
            yh3 yh3 = yh3.this;
            return yh3.h + yh3.j;
        }
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void a();

        @DexIgnore
        void b();
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        void a();

        @DexIgnore
        void b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends l {
        @DexIgnore
        public k() {
            super(yh3.this, (a) null);
        }

        @DexIgnore
        public float a() {
            return yh3.this.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class l extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public l() {
        }

        @DexIgnore
        public abstract float a();

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            yh3.this.e((float) ((int) this.c));
            this.a = false;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.a) {
                dj3 dj3 = yh3.this.b;
                this.b = dj3 == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : dj3.g();
                this.c = a();
                this.a = true;
            }
            yh3 yh3 = yh3.this;
            float f = this.b;
            yh3.e((float) ((int) (f + ((this.c - f) * valueAnimator.getAnimatedFraction()))));
        }

        @DexIgnore
        public /* synthetic */ l(yh3 yh3, a aVar) {
            this();
        }
    }

    @DexIgnore
    public yh3(FloatingActionButton floatingActionButton, wi3 wi3) {
        this.y = floatingActionButton;
        this.z = wi3;
        this.l = new hi3();
        this.l.a(G, a((l) new h()));
        this.l.a(H, a((l) new g()));
        this.l.a(I, a((l) new g()));
        this.l.a(J, a((l) new g()));
        this.l.a(K, a((l) new k()));
        this.l.a(L, a((l) new f(this)));
        this.r = this.y.getRotation();
    }

    @DexIgnore
    public final void A() {
        c(this.s);
    }

    @DexIgnore
    public final void B() {
        Rect rect = this.A;
        a(rect);
        b(rect);
        this.z.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public void b(int i2) {
        this.k = i2;
    }

    @DexIgnore
    public final void c(float f2) {
        this.s = f2;
        Matrix matrix = this.D;
        a(f2, matrix);
        this.y.setImageMatrix(matrix);
    }

    @DexIgnore
    public final void d(float f2) {
        if (this.j != f2) {
            this.j = f2;
            a(this.h, this.i, this.j);
        }
    }

    @DexIgnore
    public float e() {
        return this.h;
    }

    @DexIgnore
    public boolean f() {
        return this.f;
    }

    @DexIgnore
    public final fg3 g() {
        return this.q;
    }

    @DexIgnore
    public float h() {
        return this.i;
    }

    @DexIgnore
    public final ViewTreeObserver.OnPreDrawListener i() {
        if (this.E == null) {
            this.E = new e();
        }
        return this.E;
    }

    @DexIgnore
    public float j() {
        return this.j;
    }

    @DexIgnore
    public final hj3 k() {
        return this.a;
    }

    @DexIgnore
    public final fg3 l() {
        return this.p;
    }

    @DexIgnore
    public boolean m() {
        if (this.y.getVisibility() == 0) {
            if (this.u == 1) {
                return true;
            }
            return false;
        } else if (this.u != 2) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public boolean n() {
        if (this.y.getVisibility() != 0) {
            if (this.u == 2) {
                return true;
            }
            return false;
        } else if (this.u != 1) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public void o() {
        this.l.b();
    }

    @DexIgnore
    public void p() {
        dj3 dj3 = this.b;
        if (dj3 != null) {
            ej3.a((View) this.y, dj3);
        }
        if (v()) {
            this.y.getViewTreeObserver().addOnPreDrawListener(i());
        }
    }

    @DexIgnore
    public void q() {
    }

    @DexIgnore
    public void r() {
        ViewTreeObserver viewTreeObserver = this.y.getViewTreeObserver();
        ViewTreeObserver.OnPreDrawListener onPreDrawListener = this.E;
        if (onPreDrawListener != null) {
            viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            this.E = null;
        }
    }

    @DexIgnore
    public void s() {
        float rotation = this.y.getRotation();
        if (this.r != rotation) {
            this.r = rotation;
            z();
        }
    }

    @DexIgnore
    public void t() {
        ArrayList<i> arrayList = this.x;
        if (arrayList != null) {
            Iterator<i> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
        }
    }

    @DexIgnore
    public void u() {
        ArrayList<i> arrayList = this.x;
        if (arrayList != null) {
            Iterator<i> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public boolean v() {
        return true;
    }

    @DexIgnore
    public boolean w() {
        return true;
    }

    @DexIgnore
    public final boolean x() {
        return x9.E(this.y) && !this.y.isInEditMode();
    }

    @DexIgnore
    public final boolean y() {
        return !this.f || this.y.getSizeDimension() >= this.k;
    }

    @DexIgnore
    public void z() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.r % 90.0f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (this.y.getLayerType() != 1) {
                    this.y.setLayerType(1, (Paint) null);
                }
            } else if (this.y.getLayerType() != 0) {
                this.y.setLayerType(0, (Paint) null);
            }
        }
        dj3 dj3 = this.b;
        if (dj3 != null) {
            dj3.c((int) this.r);
        }
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        Drawable drawable = this.c;
        if (drawable != null) {
            o7.a(drawable, ui3.b(colorStateList));
        }
    }

    @DexIgnore
    public void e(float f2) {
        dj3 dj3 = this.b;
        if (dj3 != null) {
            dj3.b(f2);
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i2) {
        this.b = a();
        this.b.setTintList(colorStateList);
        if (mode != null) {
            this.b.setTintMode(mode);
        }
        this.b.b(-12303292);
        this.b.a(this.y.getContext());
        ti3 ti3 = new ti3(this.b.n());
        ti3.setTintList(ui3.b(colorStateList2));
        this.c = ti3;
        dj3 dj3 = this.b;
        y8.a(dj3);
        this.e = new LayerDrawable(new Drawable[]{dj3, ti3});
    }

    @DexIgnore
    public final fg3 d() {
        if (this.m == null) {
            this.m = fg3.a(this.y.getContext(), mf3.design_fab_show_motion_spec);
        }
        fg3 fg3 = this.m;
        y8.a(fg3);
        return fg3;
    }

    @DexIgnore
    public final void b(float f2) {
        if (this.i != f2) {
            this.i = f2;
            a(this.h, this.i, this.j);
        }
    }

    @DexIgnore
    public final fg3 c() {
        if (this.n == null) {
            this.n = fg3.a(this.y.getContext(), mf3.design_fab_hide_motion_spec);
        }
        fg3 fg3 = this.n;
        y8.a(fg3);
        return fg3;
    }

    @DexIgnore
    public final void b(fg3 fg3) {
        this.p = fg3;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.g = z2;
        B();
    }

    @DexIgnore
    public void b(Animator.AnimatorListener animatorListener) {
        if (this.v == null) {
            this.v = new ArrayList<>();
        }
        this.v.add(animatorListener);
    }

    @DexIgnore
    public void b(j jVar, boolean z2) {
        if (!n()) {
            Animator animator = this.o;
            if (animator != null) {
                animator.cancel();
            }
            if (x()) {
                if (this.y.getVisibility() != 0) {
                    this.y.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.y.setScaleY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.y.setScaleX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    c(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                fg3 fg3 = this.p;
                if (fg3 == null) {
                    fg3 = d();
                }
                AnimatorSet a2 = a(fg3, 1.0f, 1.0f, 1.0f);
                a2.addListener(new b(z2, jVar));
                ArrayList<Animator.AnimatorListener> arrayList = this.v;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            this.y.a(0, z2);
            this.y.setAlpha(1.0f);
            this.y.setScaleY(1.0f);
            this.y.setScaleX(1.0f);
            c(1.0f);
            if (jVar != null) {
                jVar.a();
            }
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        dj3 dj3 = this.b;
        if (dj3 != null) {
            dj3.setTintList(colorStateList);
        }
        xh3 xh3 = this.d;
        if (xh3 != null) {
            xh3.a(colorStateList);
        }
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        dj3 dj3 = this.b;
        if (dj3 != null) {
            dj3.setTintMode(mode);
        }
    }

    @DexIgnore
    public final void a(float f2) {
        if (this.h != f2) {
            this.h = f2;
            a(this.h, this.i, this.j);
        }
    }

    @DexIgnore
    public final void a(int i2) {
        if (this.t != i2) {
            this.t = i2;
            A();
        }
    }

    @DexIgnore
    public final void a(float f2, Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.y.getDrawable();
        if (drawable != null && this.t != 0) {
            RectF rectF = this.B;
            RectF rectF2 = this.C;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            int i2 = this.t;
            rectF2.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, (float) i2);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            int i3 = this.t;
            matrix.postScale(f2, f2, ((float) i3) / 2.0f, ((float) i3) / 2.0f);
        }
    }

    @DexIgnore
    public final void a(hj3 hj3) {
        this.a = hj3;
        dj3 dj3 = this.b;
        if (dj3 != null) {
            dj3.setShapeAppearanceModel(hj3);
        }
        Drawable drawable = this.c;
        if (drawable instanceof kj3) {
            ((kj3) drawable).setShapeAppearanceModel(hj3);
        }
        xh3 xh3 = this.d;
        if (xh3 != null) {
            xh3.a(hj3);
        }
    }

    @DexIgnore
    public final Drawable b() {
        return this.e;
    }

    @DexIgnore
    public void b(Rect rect) {
        y8.a(this.e, (Object) "Didn't initialize content background");
        if (w()) {
            this.z.a(new InsetDrawable(this.e, rect.left, rect.top, rect.right, rect.bottom));
            return;
        }
        this.z.a(this.e);
    }

    @DexIgnore
    public final void a(fg3 fg3) {
        this.q = fg3;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.f = z2;
    }

    @DexIgnore
    public void a(float f2, float f3, float f4) {
        B();
        e(f2);
    }

    @DexIgnore
    public void a(int[] iArr) {
        this.l.a(iArr);
    }

    @DexIgnore
    public void a(Animator.AnimatorListener animatorListener) {
        if (this.w == null) {
            this.w = new ArrayList<>();
        }
        this.w.add(animatorListener);
    }

    @DexIgnore
    public void a(j jVar, boolean z2) {
        if (!m()) {
            Animator animator = this.o;
            if (animator != null) {
                animator.cancel();
            }
            if (x()) {
                fg3 fg3 = this.q;
                if (fg3 == null) {
                    fg3 = c();
                }
                AnimatorSet a2 = a(fg3, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                a2.addListener(new a(z2, jVar));
                ArrayList<Animator.AnimatorListener> arrayList = this.w;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            this.y.a(z2 ? 8 : 4, z2);
            if (jVar != null) {
                jVar.b();
            }
        }
    }

    @DexIgnore
    public final AnimatorSet a(fg3 fg3, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.y, View.ALPHA, new float[]{f2});
        fg3.b("opacity").a((Animator) ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.y, View.SCALE_X, new float[]{f3});
        fg3.b("scale").a((Animator) ofFloat2);
        a(ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.y, View.SCALE_Y, new float[]{f3});
        fg3.b("scale").a((Animator) ofFloat3);
        a(ofFloat3);
        arrayList.add(ofFloat3);
        a(f4, this.D);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.y, new dg3(), new c(), new Matrix[]{new Matrix(this.D)});
        fg3.b("iconScale").a((Animator) ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        zf3.a(animatorSet, arrayList);
        return animatorSet;
    }

    @DexIgnore
    public final void a(ObjectAnimator objectAnimator) {
        if (Build.VERSION.SDK_INT == 26) {
            objectAnimator.setEvaluator(new d(this));
        }
    }

    @DexIgnore
    public void a(i iVar) {
        if (this.x == null) {
            this.x = new ArrayList<>();
        }
        this.x.add(iVar);
    }

    @DexIgnore
    public void a(Rect rect) {
        int sizeDimension = this.f ? (this.k - this.y.getSizeDimension()) / 2 : 0;
        float e2 = this.g ? e() + this.j : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int max = Math.max(sizeDimension, (int) Math.ceil((double) e2));
        int max2 = Math.max(sizeDimension, (int) Math.ceil((double) (e2 * 1.5f)));
        rect.set(max, max2, max, max2);
    }

    @DexIgnore
    public dj3 a() {
        hj3 hj3 = this.a;
        y8.a(hj3);
        return new dj3(hj3);
    }

    @DexIgnore
    public final ValueAnimator a(l lVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(F);
        valueAnimator.setDuration(100);
        valueAnimator.addListener(lVar);
        valueAnimator.addUpdateListener(lVar);
        valueAnimator.setFloatValues(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
        return valueAnimator;
    }
}
