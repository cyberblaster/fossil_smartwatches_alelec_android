package com.fossil;

import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq5 implements MembersInjector<OnboardingHeightWeightActivity> {
    @DexIgnore
    public static void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity, OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
        onboardingHeightWeightActivity.B = onboardingHeightWeightPresenter;
    }
}
