package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yb6 {
    @DexIgnore
    public /* final */ ib6 a;
    @DexIgnore
    public /* final */ ub6 b;
    @DexIgnore
    public /* final */ tb6 c;
    @DexIgnore
    public /* final */ rb6 d;
    @DexIgnore
    public /* final */ fb6 e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public yb6(long j, ib6 ib6, ub6 ub6, tb6 tb6, rb6 rb6, fb6 fb6, kb6 kb6, int i, int i2) {
        this.f = j;
        this.a = ib6;
        this.b = ub6;
        this.c = tb6;
        this.d = rb6;
        this.e = fb6;
    }

    @DexIgnore
    public boolean a(long j) {
        return this.f < j;
    }
}
