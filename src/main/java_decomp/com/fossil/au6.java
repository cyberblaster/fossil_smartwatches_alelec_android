package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class au6 {
    @DexIgnore
    public static /* final */ au6 d; // = new a();
    @DexIgnore
    public boolean a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends au6 {
        @DexIgnore
        public au6 a(long j) {
            return this;
        }

        @DexIgnore
        public au6 a(long j, TimeUnit timeUnit) {
            return this;
        }

        @DexIgnore
        public void e() throws IOException {
        }
    }

    @DexIgnore
    public au6 a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit != null) {
            this.c = timeUnit.toNanos(j);
            return this;
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public au6 b() {
        this.c = 0;
        return this;
    }

    @DexIgnore
    public long c() {
        if (this.a) {
            return this.b;
        }
        throw new IllegalStateException("No deadline");
    }

    @DexIgnore
    public boolean d() {
        return this.a;
    }

    @DexIgnore
    public void e() throws IOException {
        if (Thread.interrupted()) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException("interrupted");
        } else if (this.a && this.b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    @DexIgnore
    public long f() {
        return this.c;
    }

    @DexIgnore
    public au6 a(long j) {
        this.a = true;
        this.b = j;
        return this;
    }

    @DexIgnore
    public au6 a() {
        this.a = false;
        return this;
    }
}
