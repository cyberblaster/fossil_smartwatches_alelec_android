package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum w36 {
    PAGE_VIEW(1),
    SESSION_ENV(2),
    ERROR(3),
    CUSTOM(1000),
    ADDITION(1001),
    MONITOR_STAT(1002),
    MTA_GAME_USER(1003),
    NETWORK_MONITOR(1004),
    NETWORK_DETECTOR(1005);
    
    @DexIgnore
    public int j;

    /*
    static {
        PAGE_VIEW = new w36("PAGE_VIEW", 0, 1);
        SESSION_ENV = new w36("SESSION_ENV", 1, 2);
        ERROR = new w36("ERROR", 2, 3);
        CUSTOM = new w36("CUSTOM", 3, 1000);
        ADDITION = new w36("ADDITION", 4, 1001);
        MONITOR_STAT = new w36("MONITOR_STAT", 5, 1002);
        MTA_GAME_USER = new w36("MTA_GAME_USER", 6, 1003);
        NETWORK_MONITOR = new w36("NETWORK_MONITOR", 7, 1004);
        NETWORK_DETECTOR = new w36("NETWORK_DETECTOR", 8, 1005);
        w36[] w36Arr = {PAGE_VIEW, SESSION_ENV, ERROR, CUSTOM, ADDITION, MONITOR_STAT, MTA_GAME_USER, NETWORK_MONITOR, NETWORK_DETECTOR};
    }
    */

    @DexIgnore
    public w36(int i) {
        this.j = i;
    }

    @DexIgnore
    public final int a() {
        return this.j;
    }
}
