package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r22 extends sa2 implements s22 {
    @DexIgnore
    public r22() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }

    @DexIgnore
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        f(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
