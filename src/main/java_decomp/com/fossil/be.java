package com.fossil;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class be<Params, Progress, Result> {
    @DexIgnore
    public static /* final */ ThreadFactory f; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> g; // = new LinkedBlockingQueue(10);
    @DexIgnore
    public static /* final */ Executor h; // = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, g, f);
    @DexIgnore
    public static f i;
    @DexIgnore
    public /* final */ h<Params, Result> a; // = new b();
    @DexIgnore
    public /* final */ FutureTask<Result> b; // = new c(this.a);
    @DexIgnore
    public volatile g c; // = g.PENDING;
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "ModernAsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends h<Params, Result> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public Result call() throws Exception {
            be.this.e.set(true);
            Result result = null;
            try {
                Process.setThreadPriority(10);
                result = be.this.a(this.a);
                Binder.flushPendingCommands();
                be.this.d(result);
                return result;
            } catch (Throwable th) {
                be.this.d(result);
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends FutureTask<Result> {
        @DexIgnore
        public c(Callable callable) {
            super(callable);
        }

        @DexIgnore
        public void done() {
            try {
                be.this.e(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occurred while executing doInBackground()", e2.getCause());
            } catch (CancellationException unused) {
                be.this.e(null);
            } catch (Throwable th) {
                throw new RuntimeException("An error occurred while executing doInBackground()", th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class d {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[g.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[g.RUNNING.ordinal()] = 1;
            a[g.FINISHED.ordinal()] = 2;
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Data> {
        @DexIgnore
        public /* final */ be a;
        @DexIgnore
        public /* final */ Data[] b;

        @DexIgnore
        public e(be beVar, Data... dataArr) {
            this.a = beVar;
            this.b = dataArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends Handler {
        @DexIgnore
        public f() {
            super(Looper.getMainLooper());
        }

        @DexIgnore
        public void handleMessage(Message message) {
            e eVar = (e) message.obj;
            int i = message.what;
            if (i == 1) {
                eVar.a.a(eVar.b[0]);
            } else if (i == 2) {
                eVar.a.b((Progress[]) eVar.b);
            }
        }
    }

    @DexIgnore
    public enum g {
        PENDING,
        RUNNING,
        FINISHED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h<Params, Result> implements Callable<Result> {
        @DexIgnore
        public Params[] a;
    }

    @DexIgnore
    public static Handler d() {
        f fVar;
        synchronized (be.class) {
            if (i == null) {
                i = new f();
            }
            fVar = i;
        }
        return fVar;
    }

    @DexIgnore
    public abstract Result a(Params... paramsArr);

    @DexIgnore
    public final boolean a() {
        return this.d.get();
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(Result result) {
        b();
    }

    @DexIgnore
    public void b(Progress... progressArr) {
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public void c(Result result) {
    }

    @DexIgnore
    public void e(Result result) {
        if (!this.e.get()) {
            d(result);
        }
    }

    @DexIgnore
    public final boolean a(boolean z) {
        this.d.set(true);
        return this.b.cancel(z);
    }

    @DexIgnore
    public final be<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.c != g.PENDING) {
            int i2 = d.a[this.c.ordinal()];
            if (i2 == 1) {
                throw new IllegalStateException("Cannot execute task: the task is already running.");
            } else if (i2 != 2) {
                throw new IllegalStateException("We should never reach this state");
            } else {
                throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        } else {
            this.c = g.RUNNING;
            c();
            this.a.a = paramsArr;
            executor.execute(this.b);
            return this;
        }
    }

    @DexIgnore
    public Result d(Result result) {
        d().obtainMessage(1, new e(this, result)).sendToTarget();
        return result;
    }

    @DexIgnore
    public void a(Result result) {
        if (a()) {
            b(result);
        } else {
            c(result);
        }
        this.c = g.FINISHED;
    }
}
