package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr4 extends m24<c, e, d> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((qg6) null);
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ b f; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rr4.g;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v7, types: [com.fossil.rr4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r7v9, types: [com.fossil.rr4, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = rr4.h.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + rr4.this.d() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.FORCE_CONNECT && rr4.this.d()) {
                boolean z = false;
                rr4.this.a(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    rr4.this.a(new e());
                    return;
                }
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                rr4.this.a(new d(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            wg6.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public d(int i, int i2, ArrayList<Integer> arrayList) {
            wg6.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.d {
    }

    /*
    static {
        String simpleName = rr4.class.getSimpleName();
        wg6.a((Object) simpleName, "ReconnectDeviceUseCase::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public Object a(c cVar, xe6<Object> xe6) {
        this.e = true;
        this.d = cVar != null ? cVar.a() : null;
        try {
            PortfolioApp instance = PortfolioApp.get.instance();
            String str = this.d;
            if (str != null) {
                instance.d(str);
                return new Object();
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.e(str2, "Error inside " + g + ".connectDevice - e=" + e2);
        }
    }
}
