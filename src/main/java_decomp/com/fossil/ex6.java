package com.fossil;

import com.fossil.cx6;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex6 extends cx6.a {
    @DexIgnore
    public static /* final */ cx6.a a; // = new ex6();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R> implements cx6<R, CompletableFuture<R>> {
        @DexIgnore
        public /* final */ Type a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ex6$a$a")
        /* renamed from: com.fossil.ex6$a$a  reason: collision with other inner class name */
        public class C0000a extends CompletableFuture<R> {
            @DexIgnore
            public /* final */ /* synthetic */ Call a;

            @DexIgnore
            public C0000a(a aVar, Call call) {
                this.a = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.a.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements dx6<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture a;

            @DexIgnore
            public b(a aVar, CompletableFuture completableFuture) {
                this.a = completableFuture;
            }

            @DexIgnore
            public void onFailure(Call<R> call, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @DexIgnore
            public void onResponse(Call<R> call, rx6<R> rx6) {
                if (rx6.d()) {
                    this.a.complete(rx6.a());
                } else {
                    this.a.completeExceptionally(new hx6(rx6));
                }
            }
        }

        @DexIgnore
        public a(Type type) {
            this.a = type;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public CompletableFuture<R> a(Call<R> call) {
            C0000a aVar = new C0000a(this, call);
            call.a(new b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<R> implements cx6<R, CompletableFuture<rx6<R>>> {
        @DexIgnore
        public /* final */ Type a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends CompletableFuture<rx6<R>> {
            @DexIgnore
            public /* final */ /* synthetic */ Call a;

            @DexIgnore
            public a(b bVar, Call call) {
                this.a = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.a.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ex6$b$b")
        /* renamed from: com.fossil.ex6$b$b  reason: collision with other inner class name */
        public class C0001b implements dx6<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture a;

            @DexIgnore
            public C0001b(b bVar, CompletableFuture completableFuture) {
                this.a = completableFuture;
            }

            @DexIgnore
            public void onFailure(Call<R> call, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @DexIgnore
            public void onResponse(Call<R> call, rx6<R> rx6) {
                this.a.complete(rx6);
            }
        }

        @DexIgnore
        public b(Type type) {
            this.a = type;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public CompletableFuture<rx6<R>> a(Call<R> call) {
            a aVar = new a(this, call);
            call.a(new C0001b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore
    public cx6<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (cx6.a.a(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type a2 = cx6.a.a(0, (ParameterizedType) type);
            if (cx6.a.a(a2) != rx6.class) {
                return new a(a2);
            }
            if (a2 instanceof ParameterizedType) {
                return new b(cx6.a.a(0, (ParameterizedType) a2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
