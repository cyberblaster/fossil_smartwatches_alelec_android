package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
public final class l55$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $searchedRecent;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l55$b$a(CommuteTimeSettingsPresenter.b bVar, jh6 jh6, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$searchedRecent = jh6;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l55$b$a l55_b_a = new l55$b$a(this.this$0, this.$searchedRecent, xe6);
        l55_b_a.p$ = (il6) obj;
        return l55_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l55$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.l.a((List<String>) (List) this.$searchedRecent.element);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
