package com.fossil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st6 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(st6.class.getName());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements yt6 {
        @DexIgnore
        public /* final */ /* synthetic */ au6 a;
        @DexIgnore
        public /* final */ /* synthetic */ OutputStream b;

        @DexIgnore
        public a(au6 au6, OutputStream outputStream) {
            this.a = au6;
            this.b = outputStream;
        }

        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            bu6.a(jt6.b, 0, j);
            while (j > 0) {
                this.a.e();
                vt6 vt6 = jt6.a;
                int min = (int) Math.min(j, (long) (vt6.c - vt6.b));
                this.b.write(vt6.a, vt6.b, min);
                vt6.b += min;
                long j2 = (long) min;
                j -= j2;
                jt6.b -= j2;
                if (vt6.b == vt6.c) {
                    jt6.a = vt6.b();
                    wt6.a(vt6);
                }
            }
        }

        @DexIgnore
        public au6 b() {
            return this.a;
        }

        @DexIgnore
        public void close() throws IOException {
            this.b.close();
        }

        @DexIgnore
        public void flush() throws IOException {
            this.b.flush();
        }

        @DexIgnore
        public String toString() {
            return "sink(" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements yt6 {
        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            jt6.skip(j);
        }

        @DexIgnore
        public au6 b() {
            return au6.d;
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public void flush() throws IOException {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ht6 {
        @DexIgnore
        public /* final */ /* synthetic */ Socket k;

        @DexIgnore
        public d(Socket socket) {
            this.k = socket;
        }

        @DexIgnore
        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        public void i() {
            try {
                this.k.close();
            } catch (Exception e) {
                Logger logger = st6.a;
                Level level = Level.WARNING;
                logger.log(level, "Failed to close timed out socket " + this.k, e);
            } catch (AssertionError e2) {
                if (st6.a(e2)) {
                    Logger logger2 = st6.a;
                    Level level2 = Level.WARNING;
                    logger2.log(level2, "Failed to close timed out socket " + this.k, e2);
                    return;
                }
                throw e2;
            }
        }
    }

    @DexIgnore
    public static lt6 a(zt6 zt6) {
        return new ut6(zt6);
    }

    @DexIgnore
    public static yt6 b(File file) throws FileNotFoundException {
        if (file != null) {
            return a((OutputStream) new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    @DexIgnore
    public static zt6 c(File file) throws FileNotFoundException {
        if (file != null) {
            return a((InputStream) new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    @DexIgnore
    public static kt6 a(yt6 yt6) {
        return new tt6(yt6);
    }

    @DexIgnore
    public static yt6 a(OutputStream outputStream) {
        return a(outputStream, new au6());
    }

    @DexIgnore
    public static zt6 b(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getInputStream() != null) {
            ht6 c2 = c(socket);
            return c2.a(a(socket.getInputStream(), (au6) c2));
        } else {
            throw new IOException("socket's input stream == null");
        }
    }

    @DexIgnore
    public static ht6 c(Socket socket) {
        return new d(socket);
    }

    @DexIgnore
    public static yt6 a(OutputStream outputStream, au6 au6) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (au6 != null) {
            return new a(au6, outputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    @DexIgnore
    public static yt6 a(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getOutputStream() != null) {
            ht6 c2 = c(socket);
            return c2.a(a(socket.getOutputStream(), (au6) c2));
        } else {
            throw new IOException("socket's output stream == null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements zt6 {
        @DexIgnore
        public /* final */ /* synthetic */ au6 a;
        @DexIgnore
        public /* final */ /* synthetic */ InputStream b;

        @DexIgnore
        public b(au6 au6, InputStream inputStream) {
            this.a = au6;
            this.b = inputStream;
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (i == 0) {
                return 0;
            } else {
                try {
                    this.a.e();
                    vt6 b2 = jt6.b(1);
                    int read = this.b.read(b2.a, b2.c, (int) Math.min(j, (long) (8192 - b2.c)));
                    if (read == -1) {
                        return -1;
                    }
                    b2.c += read;
                    long j2 = (long) read;
                    jt6.b += j2;
                    return j2;
                } catch (AssertionError e) {
                    if (st6.a(e)) {
                        throw new IOException(e);
                    }
                    throw e;
                }
            }
        }

        @DexIgnore
        public void close() throws IOException {
            this.b.close();
        }

        @DexIgnore
        public String toString() {
            return "source(" + this.b + ")";
        }

        @DexIgnore
        public au6 b() {
            return this.a;
        }
    }

    @DexIgnore
    public static zt6 a(InputStream inputStream) {
        return a(inputStream, new au6());
    }

    @DexIgnore
    public static zt6 a(InputStream inputStream, au6 au6) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (au6 != null) {
            return new b(au6, inputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    @DexIgnore
    public static yt6 a(File file) throws FileNotFoundException {
        if (file != null) {
            return a((OutputStream) new FileOutputStream(file, true));
        }
        throw new IllegalArgumentException("file == null");
    }

    @DexIgnore
    public static yt6 a() {
        return new c();
    }

    @DexIgnore
    public static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
