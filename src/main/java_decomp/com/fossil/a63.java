package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a63 {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C;
    @DexIgnore
    public String D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public /* final */ x53 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public String j;
    @DexIgnore
    public long k;
    @DexIgnore
    public String l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public long p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public String s;
    @DexIgnore
    public Boolean t;
    @DexIgnore
    public long u;
    @DexIgnore
    public List<String> v;
    @DexIgnore
    public String w;
    @DexIgnore
    public long x;
    @DexIgnore
    public long y;
    @DexIgnore
    public long z;

    @DexIgnore
    public a63(x53 x53, String str) {
        w12.a(x53);
        w12.b(str);
        this.a = x53;
        this.b = str;
        this.a.a().g();
    }

    @DexIgnore
    public final boolean A() {
        this.a.a().g();
        return this.o;
    }

    @DexIgnore
    public final long B() {
        this.a.a().g();
        return this.g;
    }

    @DexIgnore
    public final long C() {
        this.a.a().g();
        return this.F;
    }

    @DexIgnore
    public final long D() {
        this.a.a().g();
        return this.G;
    }

    @DexIgnore
    public final void E() {
        this.a.a().g();
        long j2 = this.g + 1;
        if (j2 > 2147483647L) {
            this.a.b().w().a("Bundle index overflow. appId", t43.a(this.b));
            j2 = 0;
        }
        this.E = true;
        this.g = j2;
    }

    @DexIgnore
    public final long F() {
        this.a.a().g();
        return this.x;
    }

    @DexIgnore
    public final long G() {
        this.a.a().g();
        return this.y;
    }

    @DexIgnore
    public final long H() {
        this.a.a().g();
        return this.z;
    }

    @DexIgnore
    public final long I() {
        this.a.a().g();
        return this.A;
    }

    @DexIgnore
    public final boolean a() {
        this.a.a().g();
        return this.E;
    }

    @DexIgnore
    public final void b(String str) {
        this.a.a().g();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.E |= !ma3.d(this.d, str);
        this.d = str;
    }

    @DexIgnore
    public final void c(String str) {
        this.a.a().g();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.E |= !ma3.d(this.s, str);
        this.s = str;
    }

    @DexIgnore
    public final void d(String str) {
        this.a.a().g();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.E |= !ma3.d(this.w, str);
        this.w = str;
    }

    @DexIgnore
    public final void e(String str) {
        this.a.a().g();
        this.E |= !ma3.d(this.e, str);
        this.e = str;
    }

    @DexIgnore
    public final void f(String str) {
        this.a.a().g();
        this.E |= !ma3.d(this.f, str);
        this.f = str;
    }

    @DexIgnore
    public final void g(String str) {
        this.a.a().g();
        this.E |= !ma3.d(this.j, str);
        this.j = str;
    }

    @DexIgnore
    public final void h(String str) {
        this.a.a().g();
        this.E |= !ma3.d(this.l, str);
        this.l = str;
    }

    @DexIgnore
    public final void i(long j2) {
        this.a.a().g();
        this.E |= this.G != j2;
        this.G = j2;
    }

    @DexIgnore
    public final void j(long j2) {
        this.a.a().g();
        this.E |= this.x != j2;
        this.x = j2;
    }

    @DexIgnore
    public final void k() {
        this.a.a().g();
        this.E = false;
    }

    @DexIgnore
    public final String l() {
        this.a.a().g();
        return this.b;
    }

    @DexIgnore
    public final String m() {
        this.a.a().g();
        return this.c;
    }

    @DexIgnore
    public final String n() {
        this.a.a().g();
        return this.d;
    }

    @DexIgnore
    public final String o() {
        this.a.a().g();
        return this.s;
    }

    @DexIgnore
    public final String p() {
        this.a.a().g();
        return this.w;
    }

    @DexIgnore
    public final String q() {
        this.a.a().g();
        return this.e;
    }

    @DexIgnore
    public final String r() {
        this.a.a().g();
        return this.f;
    }

    @DexIgnore
    public final long s() {
        this.a.a().g();
        return this.h;
    }

    @DexIgnore
    public final long t() {
        this.a.a().g();
        return this.i;
    }

    @DexIgnore
    public final String u() {
        this.a.a().g();
        return this.j;
    }

    @DexIgnore
    public final long v() {
        this.a.a().g();
        return this.k;
    }

    @DexIgnore
    public final String w() {
        this.a.a().g();
        return this.l;
    }

    @DexIgnore
    public final long x() {
        this.a.a().g();
        return this.m;
    }

    @DexIgnore
    public final long y() {
        this.a.a().g();
        return this.n;
    }

    @DexIgnore
    public final long z() {
        this.a.a().g();
        return this.u;
    }

    @DexIgnore
    public final void a(String str) {
        this.a.a().g();
        this.E |= !ma3.d(this.c, str);
        this.c = str;
    }

    @DexIgnore
    public final void k(long j2) {
        this.a.a().g();
        this.E |= this.y != j2;
        this.y = j2;
    }

    @DexIgnore
    public final void l(long j2) {
        this.a.a().g();
        this.E |= this.z != j2;
        this.z = j2;
    }

    @DexIgnore
    public final void m(long j2) {
        this.a.a().g();
        this.E |= this.A != j2;
        this.A = j2;
    }

    @DexIgnore
    public final void n(long j2) {
        this.a.a().g();
        this.E |= this.C != j2;
        this.C = j2;
    }

    @DexIgnore
    public final void o(long j2) {
        this.a.a().g();
        this.E |= this.B != j2;
        this.B = j2;
    }

    @DexIgnore
    public final void p(long j2) {
        this.a.a().g();
        this.E |= this.p != j2;
        this.p = j2;
    }

    @DexIgnore
    public final void e(long j2) {
        this.a.a().g();
        this.E |= this.n != j2;
        this.n = j2;
    }

    @DexIgnore
    public final void f(long j2) {
        this.a.a().g();
        this.E |= this.u != j2;
        this.u = j2;
    }

    @DexIgnore
    public final void g(long j2) {
        boolean z2 = true;
        w12.a(j2 >= 0);
        this.a.a().g();
        boolean z3 = this.E;
        if (this.g == j2) {
            z2 = false;
        }
        this.E = z2 | z3;
        this.g = j2;
    }

    @DexIgnore
    public final void h(long j2) {
        this.a.a().g();
        this.E |= this.F != j2;
        this.F = j2;
    }

    @DexIgnore
    public final void i(String str) {
        this.a.a().g();
        this.E |= !ma3.d(this.D, str);
        this.D = str;
    }

    @DexIgnore
    public final List<String> j() {
        this.a.a().g();
        return this.v;
    }

    @DexIgnore
    public final void b(long j2) {
        this.a.a().g();
        this.E |= this.i != j2;
        this.i = j2;
    }

    @DexIgnore
    public final void c(long j2) {
        this.a.a().g();
        this.E |= this.k != j2;
        this.k = j2;
    }

    @DexIgnore
    public final void d(long j2) {
        this.a.a().g();
        this.E |= this.m != j2;
        this.m = j2;
    }

    @DexIgnore
    public final void a(long j2) {
        this.a.a().g();
        this.E |= this.h != j2;
        this.h = j2;
    }

    @DexIgnore
    public final String e() {
        this.a.a().g();
        String str = this.D;
        i((String) null);
        return str;
    }

    @DexIgnore
    public final long f() {
        this.a.a().g();
        return this.p;
    }

    @DexIgnore
    public final boolean h() {
        this.a.a().g();
        return this.r;
    }

    @DexIgnore
    public final Boolean i() {
        this.a.a().g();
        return this.t;
    }

    @DexIgnore
    public final long b() {
        this.a.a().g();
        return this.C;
    }

    @DexIgnore
    public final long c() {
        this.a.a().g();
        return this.B;
    }

    @DexIgnore
    public final String d() {
        this.a.a().g();
        return this.D;
    }

    @DexIgnore
    public final boolean g() {
        this.a.a().g();
        return this.q;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.a.a().g();
        this.E |= this.o != z2;
        this.o = z2;
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.a.a().g();
        this.E |= this.q != z2;
        this.q = z2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.a.a().g();
        this.E |= this.r != z2;
        this.r = z2;
    }

    @DexIgnore
    public final void a(Boolean bool) {
        this.a.a().g();
        this.E |= !ma3.a(this.t, bool);
        this.t = bool;
    }

    @DexIgnore
    public final void a(List<String> list) {
        this.a.a().g();
        if (!ma3.a(this.v, list)) {
            this.E = true;
            this.v = list != null ? new ArrayList(list) : null;
        }
    }
}
