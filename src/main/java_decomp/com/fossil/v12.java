package com.fossil;

import com.fossil.yv1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v12 {
    @DexIgnore
    public static /* final */ b a; // = new c32();

    @DexIgnore
    public interface a<R extends ew1, T> {
        @DexIgnore
        T a(R r);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        sv1 a(Status status);
    }

    @DexIgnore
    public static <R extends ew1, T> qc3<T> a(yv1<R> yv1, a<R, T> aVar) {
        b bVar = a;
        rc3 rc3 = new rc3();
        yv1.a((yv1.a) new e32(yv1, rc3, aVar, bVar));
        return rc3.a();
    }

    @DexIgnore
    public static <R extends ew1, T extends dw1<R>> qc3<T> a(yv1<R> yv1, T t) {
        return a(yv1, new d32(t));
    }

    @DexIgnore
    public static <R extends ew1> qc3<Void> a(yv1<R> yv1) {
        return a(yv1, new f32());
    }
}
