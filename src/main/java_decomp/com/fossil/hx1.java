package com.fossil;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hx1 extends vz1 {
    @DexIgnore
    public /* final */ q4<lw1<?>> f; // = new q4<>();
    @DexIgnore
    public qw1 g;

    @DexIgnore
    public hx1(tw1 tw1) {
        super(tw1);
        this.a.a("ConnectionlessLifecycleHelper", (LifecycleCallback) this);
    }

    @DexIgnore
    public static void a(Activity activity, qw1 qw1, lw1<?> lw1) {
        tw1 a = LifecycleCallback.a(activity);
        hx1 hx1 = (hx1) a.a("ConnectionlessLifecycleHelper", hx1.class);
        if (hx1 == null) {
            hx1 = new hx1(a);
        }
        hx1.g = qw1;
        w12.a(lw1, (Object) "ApiKey cannot be null");
        hx1.f.add(lw1);
        qw1.a(hx1);
    }

    @DexIgnore
    public void c() {
        super.c();
        i();
    }

    @DexIgnore
    public void d() {
        super.d();
        i();
    }

    @DexIgnore
    public void e() {
        super.e();
        this.g.b(this);
    }

    @DexIgnore
    public final void f() {
        this.g.c();
    }

    @DexIgnore
    public final q4<lw1<?>> h() {
        return this.f;
    }

    @DexIgnore
    public final void i() {
        if (!this.f.isEmpty()) {
            this.g.a(this);
        }
    }

    @DexIgnore
    public final void a(gv1 gv1, int i) {
        this.g.a(gv1, i);
    }
}
