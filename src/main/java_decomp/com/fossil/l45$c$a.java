package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l45$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ l45$c$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(l45$c$a l45_c_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = l45_c_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                DianaPresetRepository k = this.this$0.this$0.b.x;
                DianaPreset dianaPreset = this.this$0.this$0.a;
                if (dianaPreset != null) {
                    String id = dianaPreset.getId();
                    this.L$0 = il6;
                    this.label = 1;
                    if (k.deletePresetById(id, this) == a) {
                        return a;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l45$c$a(HomeDianaCustomizePresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l45$c$a l45_c_a = new l45$c$a(this.this$0, xe6);
        l45_c_a.p$ = (il6) obj;
        return l45_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l45$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("set new preset to watch success, delete current active ");
            DianaPreset dianaPreset = this.this$0.a;
            sb.append(dianaPreset != null ? dianaPreset.getName() : null);
            local.d("HomeDianaCustomizePresenter", sb.toString());
            dl6 d = this.this$0.b.c();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            if (gk6.a(d, aVar, this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.b.t.n();
        this.this$0.b.t.d(this.this$0.b.j());
        return cd6.a;
    }
}
