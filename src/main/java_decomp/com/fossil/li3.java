package com.fossil;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class li3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements s9 {
        @DexIgnore
        public /* final */ /* synthetic */ c a;
        @DexIgnore
        public /* final */ /* synthetic */ d b;

        @DexIgnore
        public a(c cVar, d dVar) {
            this.a = cVar;
            this.b = dVar;
        }

        @DexIgnore
        public fa a(View view, fa faVar) {
            return this.a.a(view, faVar, new d(this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            view.removeOnAttachStateChangeListener(this);
            x9.J(view);
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        fa a(View view, fa faVar, d dVar);
    }

    @DexIgnore
    public static PorterDuff.Mode a(int i, PorterDuff.Mode mode) {
        if (i == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    public static boolean b(View view) {
        return x9.o(view) == 1;
    }

    @DexIgnore
    public static void c(View view) {
        if (x9.D(view)) {
            x9.J(view);
        } else {
            view.addOnAttachStateChangeListener(new b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public d(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }

        @DexIgnore
        public void a(View view) {
            x9.b(view, this.a, this.b, this.c, this.d);
        }

        @DexIgnore
        public d(d dVar) {
            this.a = dVar.a;
            this.b = dVar.b;
            this.c = dVar.c;
            this.d = dVar.d;
        }
    }

    @DexIgnore
    public static float a(Context context, int i) {
        return TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    @DexIgnore
    public static void a(View view, c cVar) {
        x9.a(view, (s9) new a(cVar, new d(x9.t(view), view.getPaddingTop(), x9.s(view), view.getPaddingBottom())));
        c(view);
    }

    @DexIgnore
    public static float a(View view) {
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
            f += x9.k((View) parent);
        }
        return f;
    }
}
