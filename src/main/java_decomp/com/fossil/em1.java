package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em1<T> implements Comparator<if1> {
    @DexIgnore
    public static /* final */ em1 a; // = new em1();

    @DexIgnore
    public int compare(Object obj, Object obj2) {
        return ((if1) obj).e().ordinal() - ((if1) obj2).e().ordinal();
    }
}
