package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r04 {
    @DexIgnore
    public static boolean a(Context context) {
        return w6.a(context, "android.permission.ACCESS_FINE_LOCATION") == 0;
    }
}
