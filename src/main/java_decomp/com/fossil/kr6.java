package com.fossil;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kr6 implements Closeable, Flushable {
    @DexIgnore
    public static /* final */ Pattern y; // = Pattern.compile("[a-z0-9_-]{1,120}");
    @DexIgnore
    public /* final */ us6 a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public long g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public long i; // = 0;
    @DexIgnore
    public kt6 j;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> o; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public long v; // = 0;
    @DexIgnore
    public /* final */ Executor w;
    @DexIgnore
    public /* final */ Runnable x; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r5.a.u = true;
            r5.a.j = com.fossil.st6.a(com.fossil.st6.a());
         */
        @DexIgnore
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0033 */
        public void run() {
            synchronized (kr6.this) {
                if (!(!kr6.this.r) && !kr6.this.s) {
                    kr6.this.E();
                    kr6.this.t = true;
                    if (kr6.this.o()) {
                        kr6.this.D();
                        kr6.this.p = 0;
                    }
                } else {
                    return;
                }
            }
            return;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends lr6 {
        /*
        static {
            Class<kr6> cls = kr6.class;
        }
        */

        @DexIgnore
        public b(yt6 yt6) {
            super(yt6);
        }

        @DexIgnore
        public void a(IOException iOException) {
            kr6.this.q = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Closeable {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ zt6[] c;

        @DexIgnore
        public e(String str, long j, zt6[] zt6Arr, long[] jArr) {
            this.a = str;
            this.b = j;
            this.c = zt6Arr;
        }

        @DexIgnore
        public zt6 b(int i) {
            return this.c[i];
        }

        @DexIgnore
        public void close() {
            for (zt6 a2 : this.c) {
                fr6.a((Closeable) a2);
            }
        }

        @DexIgnore
        public c k() throws IOException {
            return kr6.this.a(this.a, this.b);
        }
    }

    /*
    static {
        Class<kr6> cls = kr6.class;
    }
    */

    @DexIgnore
    public kr6(us6 us6, File file, int i2, int i3, long j2, Executor executor) {
        this.a = us6;
        this.b = file;
        this.f = i2;
        this.c = new File(file, "journal");
        this.d = new File(file, "journal.tmp");
        this.e = new File(file, "journal.bkp");
        this.h = i3;
        this.g = j2;
        this.w = executor;
    }

    @DexIgnore
    public static kr6 a(us6 us6, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            return new kr6(us6, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), fr6.a("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public final void B() throws IOException {
        this.a.e(this.d);
        Iterator<d> it = this.o.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i2 = 0;
            if (next.f == null) {
                while (i2 < this.h) {
                    this.i += next.b[i2];
                    i2++;
                }
            } else {
                next.f = null;
                while (i2 < this.h) {
                    this.a.e(next.c[i2]);
                    this.a.e(next.d[i2]);
                    i2++;
                }
                it.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.p = r0 - r9.o.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        if (r1.f() == false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006c, code lost:
        D();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0070, code lost:
        r9.j = p();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005d */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x007a=Splitter:B:23:0x007a, B:16:0x005d=Splitter:B:16:0x005d} */
    public final void C() throws IOException {
        lt6 a2 = st6.a(this.a.a(this.c));
        try {
            String h2 = a2.h();
            String h3 = a2.h();
            String h4 = a2.h();
            String h5 = a2.h();
            String h6 = a2.h();
            if (!"libcore.io.DiskLruCache".equals(h2) || !"1".equals(h3) || !Integer.toString(this.f).equals(h4) || !Integer.toString(this.h).equals(h5) || !"".equals(h6)) {
                throw new IOException("unexpected journal header: [" + h2 + ", " + h3 + ", " + h5 + ", " + h6 + "]");
            }
            int i2 = 0;
            while (true) {
                g(a2.h());
                i2++;
            }
        } finally {
            fr6.a((Closeable) a2);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public synchronized void D() throws IOException {
        if (this.j != null) {
            this.j.close();
        }
        kt6 a2 = st6.a(this.a.b(this.d));
        try {
            a2.a("libcore.io.DiskLruCache").writeByte(10);
            a2.a("1").writeByte(10);
            a2.d((long) this.f).writeByte(10);
            a2.d((long) this.h).writeByte(10);
            a2.writeByte(10);
            for (d next : this.o.values()) {
                if (next.f != null) {
                    a2.a("DIRTY").writeByte(32);
                    a2.a(next.a);
                    a2.writeByte(10);
                } else {
                    a2.a("CLEAN").writeByte(32);
                    a2.a(next.a);
                    next.a(a2);
                    a2.writeByte(10);
                }
            }
            a2.close();
            if (this.a.d(this.c)) {
                this.a.a(this.c, this.e);
            }
            this.a.a(this.d, this.c);
            this.a.e(this.e);
            this.j = p();
            this.q = false;
            this.u = false;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    @DexIgnore
    public void E() throws IOException {
        while (this.i > this.g) {
            a(this.o.values().iterator().next());
        }
        this.t = false;
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        if (this.r) {
            if (!this.s) {
                for (d dVar : (d[]) this.o.values().toArray(new d[this.o.size()])) {
                    if (dVar.f != null) {
                        dVar.f.a();
                    }
                }
                E();
                this.j.close();
                this.j = null;
                this.s = true;
                return;
            }
        }
        this.s = true;
    }

    @DexIgnore
    public c e(String str) throws IOException {
        return a(str, -1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004d, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        return null;
     */
    @DexIgnore
    public synchronized e f(String str) throws IOException {
        n();
        k();
        i(str);
        d dVar = this.o.get(str);
        if (dVar != null) {
            if (dVar.e) {
                e a2 = dVar.a();
                if (a2 == null) {
                    return null;
                }
                this.p++;
                this.j.a("READ").writeByte(32).a(str).writeByte(10);
                if (o()) {
                    this.w.execute(this.x);
                }
            }
        }
    }

    @DexIgnore
    public synchronized void flush() throws IOException {
        if (this.r) {
            k();
            E();
            this.j.flush();
        }
    }

    @DexIgnore
    public final void g(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                str2 = str.substring(i2);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.o.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.o.get(str2);
            if (dVar == null) {
                dVar = new d(str2);
                this.o.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                dVar.e = true;
                dVar.f = null;
                dVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f = new c(dVar);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        return r7;
     */
    @DexIgnore
    public synchronized boolean h(String str) throws IOException {
        n();
        k();
        i(str);
        d dVar = this.o.get(str);
        if (dVar == null) {
            return false;
        }
        boolean a2 = a(dVar);
        if (a2 && this.i <= this.g) {
            this.t = false;
        }
    }

    @DexIgnore
    public final void i(String str) {
        if (!y.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    @DexIgnore
    public synchronized boolean isClosed() {
        return this.s;
    }

    @DexIgnore
    public final synchronized void k() {
        if (isClosed()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    @DexIgnore
    public void l() throws IOException {
        close();
        this.a.c(this.b);
    }

    @DexIgnore
    public synchronized void m() throws IOException {
        n();
        for (d a2 : (d[]) this.o.values().toArray(new d[this.o.size()])) {
            a(a2);
        }
        this.t = false;
    }

    @DexIgnore
    public synchronized void n() throws IOException {
        if (!this.r) {
            if (this.a.d(this.e)) {
                if (this.a.d(this.c)) {
                    this.a.e(this.e);
                } else {
                    this.a.a(this.e, this.c);
                }
            }
            if (this.a.d(this.c)) {
                try {
                    C();
                    B();
                    this.r = true;
                    return;
                } catch (IOException e2) {
                    at6 d2 = at6.d();
                    d2.a(5, "DiskLruCache " + this.b + " is corrupt: " + e2.getMessage() + ", removing", (Throwable) e2);
                    l();
                    this.s = false;
                } catch (Throwable th) {
                    this.s = false;
                    throw th;
                }
            }
            D();
            this.r = true;
        }
    }

    @DexIgnore
    public boolean o() {
        int i2 = this.p;
        return i2 >= 2000 && i2 >= this.o.size();
    }

    @DexIgnore
    public final kt6 p() throws FileNotFoundException {
        return st6.a((yt6) new b(this.a.f(this.c)));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public /* final */ File[] c;
        @DexIgnore
        public /* final */ File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public d(String str) {
            this.a = str;
            int i = kr6.this.h;
            this.b = new long[i];
            this.c = new File[i];
            this.d = new File[i];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i2 = 0; i2 < kr6.this.h; i2++) {
                sb.append(i2);
                this.c[i2] = new File(kr6.this.b, sb.toString());
                sb.append(".tmp");
                this.d[i2] = new File(kr6.this.b, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public void a(kt6 kt6) throws IOException {
            for (long d2 : this.b) {
                kt6.writeByte(32).d(d2);
            }
        }

        @DexIgnore
        public void b(String[] strArr) throws IOException {
            if (strArr.length == kr6.this.h) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public e a() {
            if (Thread.holdsLock(kr6.this)) {
                zt6[] zt6Arr = new zt6[kr6.this.h];
                long[] jArr = (long[]) this.b.clone();
                int i = 0;
                int i2 = 0;
                while (i2 < kr6.this.h) {
                    try {
                        zt6Arr[i2] = kr6.this.a.a(this.c[i2]);
                        i2++;
                    } catch (FileNotFoundException unused) {
                        while (i < kr6.this.h && zt6Arr[i] != null) {
                            fr6.a((Closeable) zt6Arr[i]);
                            i++;
                        }
                        try {
                            kr6.this.a(this);
                            return null;
                        } catch (IOException unused2) {
                            return null;
                        }
                    }
                }
                return new e(this.a, this.g, zt6Arr, jArr);
            }
            throw new AssertionError();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        return null;
     */
    @DexIgnore
    public synchronized c a(String str, long j2) throws IOException {
        n();
        k();
        i(str);
        d dVar = this.o.get(str);
        if (j2 == -1 || (dVar != null && dVar.g == j2)) {
            if (dVar != null) {
                if (dVar.f != null) {
                    return null;
                }
            }
            if (!this.t) {
                if (!this.u) {
                    this.j.a("DIRTY").writeByte(32).a(str).writeByte(10);
                    this.j.flush();
                    if (this.q) {
                        return null;
                    }
                    if (dVar == null) {
                        dVar = new d(str);
                        this.o.put(str, dVar);
                    }
                    c cVar = new c(dVar);
                    dVar.f = cVar;
                    return cVar;
                }
            }
            this.w.execute(this.x);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {
        @DexIgnore
        public /* final */ d a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends lr6 {
            @DexIgnore
            public a(yt6 yt6) {
                super(yt6);
            }

            @DexIgnore
            public void a(IOException iOException) {
                synchronized (kr6.this) {
                    c.this.c();
                }
            }
        }

        @DexIgnore
        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.e ? null : new boolean[kr6.this.h];
        }

        @DexIgnore
        public yt6 a(int i) {
            synchronized (kr6.this) {
                if (this.c) {
                    throw new IllegalStateException();
                } else if (this.a.f != this) {
                    yt6 a2 = st6.a();
                    return a2;
                } else {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    try {
                        a aVar = new a(kr6.this.a.b(this.a.d[i]));
                        return aVar;
                    } catch (FileNotFoundException unused) {
                        return st6.a();
                    }
                }
            }
        }

        @DexIgnore
        public void b() throws IOException {
            synchronized (kr6.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        kr6.this.a(this, true);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void c() {
            if (this.a.f == this) {
                int i = 0;
                while (true) {
                    kr6 kr6 = kr6.this;
                    if (i < kr6.h) {
                        try {
                            kr6.a.e(this.a.d[i]);
                        } catch (IOException unused) {
                        }
                        i++;
                    } else {
                        this.a.f = null;
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public void a() throws IOException {
            synchronized (kr6.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        kr6.this.a(this, false);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f4, code lost:
        return;
     */
    @DexIgnore
    public synchronized void a(c cVar, boolean z) throws IOException {
        d dVar = cVar.a;
        if (dVar.f == cVar) {
            if (z && !dVar.e) {
                int i2 = 0;
                while (i2 < this.h) {
                    if (!cVar.b[i2]) {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!this.a.d(dVar.d[i2])) {
                        cVar.a();
                        return;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.h; i3++) {
                File file = dVar.d[i3];
                if (!z) {
                    this.a.e(file);
                } else if (this.a.d(file)) {
                    File file2 = dVar.c[i3];
                    this.a.a(file, file2);
                    long j2 = dVar.b[i3];
                    long g2 = this.a.g(file2);
                    dVar.b[i3] = g2;
                    this.i = (this.i - j2) + g2;
                }
            }
            this.p++;
            dVar.f = null;
            if (dVar.e || z) {
                dVar.e = true;
                this.j.a("CLEAN").writeByte(32);
                this.j.a(dVar.a);
                dVar.a(this.j);
                this.j.writeByte(10);
                if (z) {
                    long j3 = this.v;
                    this.v = 1 + j3;
                    dVar.g = j3;
                }
            } else {
                this.o.remove(dVar.a);
                this.j.a("REMOVE").writeByte(32);
                this.j.a(dVar.a);
                this.j.writeByte(10);
            }
            this.j.flush();
            if (this.i > this.g || o()) {
                this.w.execute(this.x);
            }
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public boolean a(d dVar) throws IOException {
        c cVar = dVar.f;
        if (cVar != null) {
            cVar.c();
        }
        for (int i2 = 0; i2 < this.h; i2++) {
            this.a.e(dVar.c[i2]);
            long j2 = this.i;
            long[] jArr = dVar.b;
            this.i = j2 - jArr[i2];
            jArr[i2] = 0;
        }
        this.p++;
        this.j.a("REMOVE").writeByte(32).a(dVar.a).writeByte(10);
        this.o.remove(dVar.a);
        if (o()) {
            this.w.execute(this.x);
        }
        return true;
    }
}
