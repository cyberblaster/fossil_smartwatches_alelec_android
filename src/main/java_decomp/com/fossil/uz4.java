package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseEditDialogViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz4 implements Factory<tz4> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;

    @DexIgnore
    public uz4(Provider<QuickResponseRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static uz4 a(Provider<QuickResponseRepository> provider) {
        return new uz4(provider);
    }

    @DexIgnore
    public static tz4 b(Provider<QuickResponseRepository> provider) {
        return new QuickResponseEditDialogViewModel(provider.get());
    }

    @DexIgnore
    public QuickResponseEditDialogViewModel get() {
        return b(this.a);
    }
}
