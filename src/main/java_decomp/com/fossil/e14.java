package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e14 implements Factory<ApiServiceV2> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<ip4> b;
    @DexIgnore
    public /* final */ Provider<mp4> c;

    @DexIgnore
    public e14(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static e14 a(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return new e14(b14, provider, provider2);
    }

    @DexIgnore
    public static ApiServiceV2 b(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return a(b14, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ApiServiceV2 a(b14 b14, ip4 ip4, mp4 mp4) {
        ApiServiceV2 a2 = b14.a(ip4, mp4);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public ApiServiceV2 get() {
        return b(this.a, this.b, this.c);
    }
}
