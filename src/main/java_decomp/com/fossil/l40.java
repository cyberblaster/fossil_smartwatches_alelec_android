package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l40 extends ac0 {
    @DexIgnore
    public /* final */ m40 b;

    @DexIgnore
    public l40(m40 m40) {
        super(m40);
        this.b = m40;
    }

    @DexIgnore
    public m40 getErrorCode() {
        return this.b;
    }
}
