package com.fossil;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.jv;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sv<Data> implements jv<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"file", "android.resource", "content"})));
    @DexIgnore
    public /* final */ c<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements kv<Uri, AssetFileDescriptor>, c<AssetFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public jv<Uri, AssetFileDescriptor> a(nv nvVar) {
            return new sv(this);
        }

        @DexIgnore
        public fs<AssetFileDescriptor> a(Uri uri) {
            return new cs(this.a, uri);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements kv<Uri, ParcelFileDescriptor>, c<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public fs<ParcelFileDescriptor> a(Uri uri) {
            return new ks(this.a, uri);
        }

        @DexIgnore
        public jv<Uri, ParcelFileDescriptor> a(nv nvVar) {
            return new sv(this);
        }
    }

    @DexIgnore
    public interface c<Data> {
        @DexIgnore
        fs<Data> a(Uri uri);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements kv<Uri, InputStream>, c<InputStream> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public d(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public fs<InputStream> a(Uri uri) {
            return new qs(this.a, uri);
        }

        @DexIgnore
        public jv<Uri, InputStream> a(nv nvVar) {
            return new sv(this);
        }
    }

    @DexIgnore
    public sv(c<Data> cVar) {
        this.a = cVar;
    }

    @DexIgnore
    public jv.a<Data> a(Uri uri, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(uri), this.a.a(uri));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
