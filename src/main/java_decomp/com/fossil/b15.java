package com.fossil;

import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b15 implements Factory<a15> {
    @DexIgnore
    public static DoNotDisturbScheduledTimePresenter a(z05 z05, DNDSettingsDatabase dNDSettingsDatabase) {
        return new DoNotDisturbScheduledTimePresenter(z05, dNDSettingsDatabase);
    }
}
