package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xg6<R> implements sg6<R>, Serializable {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public xg6(int i) {
        this.arity = i;
    }

    @DexIgnore
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public String toString() {
        String a = kh6.a(this);
        wg6.a((Object) a, "Reflection.renderLambdaToString(this)");
        return a;
    }
}
