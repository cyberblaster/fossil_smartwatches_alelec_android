package com.fossil;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.SavedStateHandleController;
import androidx.lifecycle.ViewModelProvider;
import androidx.savedstate.SavedStateRegistry;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qd extends ViewModelProvider.b {
    @DexIgnore
    public static /* final */ Class<?>[] f;
    @DexIgnore
    public static /* final */ Class<?>[] g;
    @DexIgnore
    public /* final */ Application a;
    @DexIgnore
    public /* final */ ViewModelProvider.a b;
    @DexIgnore
    public /* final */ Bundle c;
    @DexIgnore
    public /* final */ Lifecycle d;
    @DexIgnore
    public /* final */ SavedStateRegistry e;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Class<?>[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /*
    static {
        Class<pd> cls = pd.class;
        f = new Class[]{Application.class, cls};
        g = new Class[]{cls};
    }
    */

    @DexIgnore
    @SuppressLint({"LambdaLast"})
    public qd(Application application, gi giVar, Bundle bundle) {
        this.e = giVar.getSavedStateRegistry();
        this.d = giVar.getLifecycle();
        this.c = bundle;
        this.a = application;
        this.b = ViewModelProvider.a.a(application);
    }

    @DexIgnore
    public <T extends td> T a(String str, Class<T> cls) {
        Constructor<T> constructor;
        T t;
        boolean isAssignableFrom = rc.class.isAssignableFrom(cls);
        if (isAssignableFrom) {
            constructor = a(cls, f);
        } else {
            constructor = a(cls, g);
        }
        if (constructor == null) {
            return this.b.create(cls);
        }
        SavedStateHandleController a2 = SavedStateHandleController.a(this.e, this.d, str, this.c);
        if (isAssignableFrom) {
            try {
                t = (td) constructor.newInstance(new Object[]{this.a, a2.a()});
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Failed to access " + cls, e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException("A " + cls + " cannot be instantiated.", e3);
            } catch (InvocationTargetException e4) {
                throw new RuntimeException("An exception happened in constructor of " + cls, e4.getCause());
            }
        } else {
            t = (td) constructor.newInstance(new Object[]{a2.a()});
        }
        t.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", a2);
        return t;
    }

    @DexIgnore
    public <T extends td> T create(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return a(canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @DexIgnore
    public static <T> Constructor<T> a(Class<T> cls, Class<?>[] clsArr) {
        for (Constructor<T> constructor : cls.getConstructors()) {
            if (Arrays.equals(clsArr, constructor.getParameterTypes())) {
                return constructor;
            }
        }
        return null;
    }

    @DexIgnore
    public void a(td tdVar) {
        SavedStateHandleController.a(tdVar, this.e, this.d);
    }
}
