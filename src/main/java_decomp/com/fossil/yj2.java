package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj2 extends ContentObserver {
    @DexIgnore
    public yj2(Handler handler) {
        super((Handler) null);
    }

    @DexIgnore
    public final void onChange(boolean z) {
        zj2.e.set(true);
    }
}
