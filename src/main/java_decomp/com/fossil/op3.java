package com.fossil;

import android.content.Context;
import com.google.firebase.FirebaseApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class op3 implements zp3 {
    @DexIgnore
    public static /* final */ zp3 a; // = new op3();

    @DexIgnore
    public final Object a(xp3 xp3) {
        return np3.a((FirebaseApp) xp3.a(FirebaseApp.class), (Context) xp3.a(Context.class), (rq3) xp3.a(rq3.class));
    }
}
