package com.fossil;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ry5 {
    @DexIgnore
    public static /* final */ float w; // = ((float) (Math.log(0.75d) / Math.log(0.9d)));
    @DexIgnore
    public static /* final */ float[] x; // = new float[101];
    @DexIgnore
    public static /* final */ float y; // = 8.0f;
    @DexIgnore
    public static float z;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public Interpolator r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public /* final */ float v;

    /*
    static {
        float f2;
        float f3;
        float f4 = 0.0f;
        for (int i2 = 0; i2 <= 100; i2++) {
            float f5 = ((float) i2) / 100.0f;
            float f6 = 1.0f;
            while (true) {
                float f7 = ((f6 - f4) / 2.0f) + f4;
                float f8 = 1.0f - f7;
                f2 = 3.0f * f7 * f8;
                f3 = f7 * f7 * f7;
                float f9 = (((f8 * 0.4f) + (0.6f * f7)) * f2) + f3;
                if (((double) Math.abs(f9 - f5)) < 1.0E-5d) {
                    break;
                } else if (f9 > f5) {
                    f6 = f7;
                } else {
                    f4 = f7;
                }
            }
            x[i2] = f2 + f3;
        }
        x[100] = 1.0f;
        z = 1.0f;
        z = 1.0f / b(1.0f);
    }
    */

    @DexIgnore
    public ry5(Context context, Interpolator interpolator) {
        this(context, interpolator, true);
    }

    @DexIgnore
    public final float a(float f2) {
        return this.v * 386.0878f * f2;
    }

    @DexIgnore
    public final float b() {
        return this.t - ((this.u * ((float) g())) / 2000.0f);
    }

    @DexIgnore
    public final int c() {
        return this.k;
    }

    @DexIgnore
    public final int d() {
        return this.e;
    }

    @DexIgnore
    public final int e() {
        return this.c;
    }

    @DexIgnore
    public final boolean f() {
        return this.q;
    }

    @DexIgnore
    public final int g() {
        return (int) (AnimationUtils.currentAnimationTimeMillis() - this.l);
    }

    @DexIgnore
    public ry5(Context context, Interpolator interpolator, boolean z2) {
        this.q = true;
        this.r = interpolator;
        this.v = context.getResources().getDisplayMetrics().density * 160.0f;
        this.u = a(ViewConfiguration.getScrollFriction());
        this.s = z2;
    }

    @DexIgnore
    public static float b(float f2) {
        float f3;
        float f4 = f2 * y;
        if (f4 < 1.0f) {
            f3 = f4 - (1.0f - ((float) Math.exp((double) (-f4))));
        } else {
            f3 = ((1.0f - ((float) Math.exp((double) (1.0f - f4)))) * 0.63212055f) + 0.36787945f;
        }
        return f3 * z;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.q = z2;
    }

    @DexIgnore
    public boolean a() {
        float f2;
        if (this.q) {
            return false;
        }
        int currentAnimationTimeMillis = (int) (AnimationUtils.currentAnimationTimeMillis() - this.l);
        int i2 = this.m;
        if (currentAnimationTimeMillis < i2) {
            int i3 = this.a;
            if (i3 == 0) {
                float f3 = ((float) currentAnimationTimeMillis) * this.n;
                Interpolator interpolator = this.r;
                if (interpolator == null) {
                    f2 = b(f3);
                } else {
                    f2 = interpolator.getInterpolation(f3);
                }
                this.j = this.b + Math.round(this.o * f2);
                this.k = this.c + Math.round(f2 * this.p);
            } else if (i3 == 1) {
                float f4 = ((float) currentAnimationTimeMillis) / ((float) i2);
                int i4 = (int) (f4 * 100.0f);
                float f5 = ((float) i4) / 100.0f;
                int i5 = i4 + 1;
                float[] fArr = x;
                float f6 = fArr[i4];
                float f7 = f6 + (((f4 - f5) / ((((float) i5) / 100.0f) - f5)) * (fArr[i5] - f6));
                int i6 = this.b;
                this.j = i6 + Math.round(((float) (this.d - i6)) * f7);
                this.j = Math.min(this.j, this.g);
                this.j = Math.max(this.j, this.f);
                int i7 = this.c;
                this.k = i7 + Math.round(f7 * ((float) (this.e - i7)));
                this.k = Math.min(this.k, this.i);
                this.k = Math.max(this.k, this.h);
                if (this.j == this.d && this.k == this.e) {
                    this.q = true;
                }
            }
        } else {
            this.j = this.d;
            this.k = this.e;
            this.q = true;
        }
        return true;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5, int i6) {
        this.a = 0;
        this.q = false;
        this.m = i6;
        this.l = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        this.d = i2 + i4;
        this.e = i3 + i5;
        this.o = (float) i4;
        this.p = (float) i5;
        this.n = 1.0f / ((float) this.m);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a6  */
    public void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        int i10;
        int i11;
        int i12;
        int i13 = i2;
        int i14 = i3;
        if (!this.s || this.q) {
            i11 = i4;
        } else {
            float b2 = b();
            float f2 = (float) (this.d - this.b);
            float f3 = (float) (this.e - this.c);
            float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
            float f4 = (f2 / sqrt) * b2;
            float f5 = (f3 / sqrt) * b2;
            i11 = i4;
            float f6 = (float) i11;
            if (Math.signum(f6) == Math.signum(f4)) {
                i10 = i5;
                float f7 = (float) i10;
                if (Math.signum(f7) == Math.signum(f5)) {
                    i11 = (int) (f6 + f4);
                    i10 = (int) (f7 + f5);
                }
                this.a = 1;
                this.q = false;
                float sqrt2 = (float) Math.sqrt((double) ((i11 * i11) + (i10 * i10)));
                this.t = sqrt2;
                double log = Math.log((double) ((0.4f * sqrt2) / 800.0f));
                this.m = (int) (Math.exp(log / (((double) w) - 1.0d)) * 1000.0d);
                this.l = AnimationUtils.currentAnimationTimeMillis();
                this.b = i13;
                this.c = i14;
                float f8 = 1.0f;
                i12 = (sqrt2 > 0.0f ? 1 : (sqrt2 == 0.0f ? 0 : -1));
                float f9 = i12 != 0 ? 1.0f : ((float) i11) / sqrt2;
                if (i12 != 0) {
                    f8 = ((float) i10) / sqrt2;
                }
                float f10 = w;
                this.f = i6;
                this.g = i7;
                this.h = i8;
                this.i = i9;
                float exp = (float) ((int) (Math.exp((((double) f10) / (((double) f10) - 1.0d)) * log) * 800.0d));
                this.d = i13 + Math.round(f9 * exp);
                this.d = Math.min(this.d, this.g);
                this.d = Math.max(this.d, this.f);
                this.e = Math.round(exp * f8) + i14;
                this.e = Math.min(this.e, this.i);
                this.e = Math.max(this.e, this.h);
            }
        }
        i10 = i5;
        this.a = 1;
        this.q = false;
        float sqrt22 = (float) Math.sqrt((double) ((i11 * i11) + (i10 * i10)));
        this.t = sqrt22;
        double log2 = Math.log((double) ((0.4f * sqrt22) / 800.0f));
        this.m = (int) (Math.exp(log2 / (((double) w) - 1.0d)) * 1000.0d);
        this.l = AnimationUtils.currentAnimationTimeMillis();
        this.b = i13;
        this.c = i14;
        float f82 = 1.0f;
        i12 = (sqrt22 > 0.0f ? 1 : (sqrt22 == 0.0f ? 0 : -1));
        if (i12 != 0) {
        }
        if (i12 != 0) {
        }
        float f102 = w;
        this.f = i6;
        this.g = i7;
        this.h = i8;
        this.i = i9;
        float exp2 = (float) ((int) (Math.exp((((double) f102) / (((double) f102) - 1.0d)) * log2) * 800.0d));
        this.d = i13 + Math.round(f9 * exp2);
        this.d = Math.min(this.d, this.g);
        this.d = Math.max(this.d, this.f);
        this.e = Math.round(exp2 * f82) + i14;
        this.e = Math.min(this.e, this.i);
        this.e = Math.max(this.e, this.h);
    }
}
