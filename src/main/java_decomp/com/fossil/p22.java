package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p22 extends i12<u22> {
    @DexIgnore
    public p22(Context context, Looper looper, e12 e12, wv1.b bVar, wv1.c cVar) {
        super(context, looper, 39, e12, bVar, cVar);
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.common.service.START";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.ICommonService");
        if (queryLocalInterface instanceof u22) {
            return (u22) queryLocalInterface;
        }
        return new t22(iBinder);
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }
}
