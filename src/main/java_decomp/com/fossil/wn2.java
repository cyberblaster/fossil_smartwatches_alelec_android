package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn2<K> implements Iterator<Map.Entry<K, Object>> {
    @DexIgnore
    public Iterator<Map.Entry<K, Object>> a;

    @DexIgnore
    public wn2(Iterator<Map.Entry<K, Object>> it) {
        this.a = it;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        Map.Entry next = this.a.next();
        return next.getValue() instanceof rn2 ? new tn2(next) : next;
    }

    @DexIgnore
    public final void remove() {
        this.a.remove();
    }
}
