package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rk6 extends tm6<ym6> implements qk6 {
    @DexIgnore
    public /* final */ sk6 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rk6(ym6 ym6, sk6 sk6) {
        super(ym6);
        wg6.b(ym6, "parent");
        wg6.b(sk6, "childJob");
        this.e = sk6;
    }

    @DexIgnore
    public boolean a(Throwable th) {
        wg6.b(th, "cause");
        return ((ym6) this.d).d(th);
    }

    @DexIgnore
    public void b(Throwable th) {
        this.e.a((gn6) this.d);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "ChildHandle[" + this.e + ']';
    }
}
