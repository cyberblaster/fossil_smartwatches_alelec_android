package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public Calendar a;
    @DexIgnore
    public Calendar b;
    @DexIgnore
    public Calendar c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public /* final */ TreeMap<Long, Integer> f; // = new TreeMap<>();
    @DexIgnore
    public /* final */ int[] g; // = new int[49];
    @DexIgnore
    public int h;
    @DexIgnore
    public RecyclerViewHeartRateCalendar.b i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Calendar o; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ Context p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ /* synthetic */ sz5 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sz5 sz5, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.b = sz5;
            this.a = (FlexibleTextView) view;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public final void a(int i) {
            String str;
            switch ((i / 7) % 7) {
                case 0:
                    str = jm4.a(this.b.p, 2131886538);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                    break;
                case 1:
                    str = jm4.a(this.b.p, 2131886535);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                    break;
                case 2:
                    str = jm4.a(this.b.p, 2131886540);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                    break;
                case 3:
                    str = jm4.a(this.b.p, 2131886541);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                    break;
                case 4:
                    str = jm4.a(this.b.p, 2131886539);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                    break;
                case 5:
                    str = jm4.a(this.b.p, 2131886536);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                    break;
                case 6:
                    str = jm4.a(this.b.p, 2131886537);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                    break;
                default:
                    str = "";
                    break;
            }
            this.a.setText(str);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public sz5(Context context) {
        wg6.b(context, "mContext");
        this.p = context;
        int[][] iArr = new int[7][];
        for (int i2 = 0; i2 < 7; i2++) {
            iArr[i2] = new int[7];
        }
        for (int i3 = 0; i3 <= 6; i3++) {
            for (int i4 = 0; i4 <= 6; i4++) {
                iArr[i3][i4] = (i3 * 7) + i4;
            }
        }
        int[][] iArr2 = new int[7][];
        for (int i5 = 0; i5 < 7; i5++) {
            iArr2[i5] = new int[7];
        }
        for (int i6 = 0; i6 <= 6; i6++) {
            for (int i7 = 0; i7 <= 6; i7++) {
                iArr2[i6][i7] = iArr[i7][6 - i6];
            }
        }
        for (int i8 = 0; i8 <= 6; i8++) {
            System.arraycopy(iArr2[i8], 0, this.g, (i8 * 7) + 1, 6);
        }
        this.o = bk4.a(0, this.o);
    }

    @DexIgnore
    public final void b(Calendar calendar) {
        wg6.b(calendar, "currentDate");
        this.c = calendar;
        if (this.a == null) {
            this.a = this.c;
        }
        if (this.b == null) {
            this.b = this.c;
        }
    }

    @DexIgnore
    public final void c(Calendar calendar) {
        this.a = calendar;
    }

    @DexIgnore
    public final Calendar d() {
        return this.b;
    }

    @DexIgnore
    public final int e() {
        return this.d;
    }

    @DexIgnore
    public final int[] f() {
        return this.g;
    }

    @DexIgnore
    public final int g() {
        return this.m;
    }

    @DexIgnore
    public int getItemCount() {
        Calendar calendar;
        if (this.a == null || (calendar = this.b) == null) {
            return 0;
        }
        if (calendar != null) {
            int i2 = calendar.get(1);
            Calendar calendar2 = this.a;
            if (calendar2 != null) {
                int i3 = (i2 - calendar2.get(1)) * 12;
                Calendar calendar3 = this.b;
                if (calendar3 != null) {
                    int i4 = i3 + calendar3.get(2);
                    Calendar calendar4 = this.a;
                    if (calendar4 != null) {
                        return ((i4 - calendar4.get(2)) + 1) * 49;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public int getItemViewType(int i2) {
        return i2 % 7 == 0 ? 0 : 1;
    }

    @DexIgnore
    public final int h() {
        return this.n;
    }

    @DexIgnore
    public final int i() {
        return this.l;
    }

    @DexIgnore
    public final TreeMap<Long, Integer> j() {
        return this.f;
    }

    @DexIgnore
    public final int k() {
        return this.j;
    }

    @DexIgnore
    public final RecyclerViewHeartRateCalendar.b l() {
        return this.i;
    }

    @DexIgnore
    public final int m() {
        return this.k;
    }

    @DexIgnore
    public final Calendar n() {
        return this.c;
    }

    @DexIgnore
    public final int o() {
        return this.e;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        wg6.b(viewHolder, "holder");
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == 0) {
            ((c) viewHolder).a(i2);
        } else if (itemViewType == 1) {
            ((b) viewHolder).a(i2);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        if (i2 == 0) {
            View inflate = LayoutInflater.from(this.p).inflate(2131558642, viewGroup, false);
            wg6.a((Object) inflate, "titleView");
            inflate.getLayoutParams().width = this.h;
            return new c(this, inflate);
        } else if (i2 != 1) {
            View inflate2 = LayoutInflater.from(this.p).inflate(2131558669, viewGroup, false);
            wg6.a((Object) inflate2, "itemView");
            inflate2.getLayoutParams().width = this.h;
            return new b(this, inflate2);
        } else {
            View inflate3 = LayoutInflater.from(this.p).inflate(2131558669, viewGroup, false);
            wg6.a((Object) inflate3, "itemView");
            inflate3.getLayoutParams().width = this.h;
            return new b(this, inflate3);
        }
    }

    @DexIgnore
    public final Calendar p() {
        return this.a;
    }

    @DexIgnore
    public final void a(Calendar calendar) {
        this.b = calendar;
    }

    @DexIgnore
    public final void c(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public final void a(RecyclerViewHeartRateCalendar.b bVar) {
        wg6.b(bVar, "listener");
        this.i = bVar;
    }

    @DexIgnore
    public final Calendar c() {
        return this.o;
    }

    @DexIgnore
    public final Calendar a(int i2) {
        Object clone = this.o.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            int i3 = this.g[i2 % 49];
            int i4 = calendar.get(7) - 1;
            if (i3 < i4 || i3 >= calendar.getActualMaximum(5) + i4) {
                return null;
            }
            calendar.add(5, i3 - i4);
            FLogger.INSTANCE.getLocal().d("CalendarHeartRateAdapter", "getCalendarItem day=" + calendar.get(5) + " month=" + calendar.get(2));
            return calendar;
        }
        throw new rc6("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final void b(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void a(Map<Long, Integer> map, int i2, int i3, Calendar calendar) {
        int i4;
        wg6.b(map, "data");
        wg6.b(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d("CalendarHeartRateAdapter", "setData");
        this.f.putAll(map);
        TreeMap<Long, Integer> treeMap = this.f;
        int i5 = 0;
        if (treeMap.isEmpty()) {
            i4 = 0;
        } else {
            i4 = 0;
            for (Map.Entry<Long, Integer> value : treeMap.entrySet()) {
                if (((Number) value.getValue()).intValue() > 0) {
                    i4++;
                }
            }
        }
        TreeMap<Long, Integer> treeMap2 = this.f;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry next : treeMap2.entrySet()) {
            if (((Number) next.getValue()).intValue() > 0) {
                linkedHashMap.put(next.getKey(), next.getValue());
            }
        }
        ArrayList arrayList = new ArrayList(linkedHashMap.size());
        for (Map.Entry value2 : linkedHashMap.entrySet()) {
            arrayList.add(Integer.valueOf(((Number) value2.getValue()).intValue()));
        }
        int k2 = yd6.k(arrayList);
        if (i4 > 0) {
            i5 = k2 / i4;
        }
        this.d = i5;
        this.o = calendar;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ /* synthetic */ sz5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sz5 sz5, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.c = sz5;
            View findViewById = view.findViewById(2131362275);
            wg6.a((Object) findViewById, "itemView.findViewById(R.id.ftvDay)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362528);
            wg6.a((Object) findViewById2, "itemView.findViewById(R.id.ivBackground)");
            this.b = (ImageView) findViewById2;
            view.setOnClickListener(this);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r11v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r11v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r11v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r11v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00b4  */
        /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
        public final void a(int i) {
            Integer num;
            if (i != -1) {
                Object clone = this.c.c().clone();
                if (clone != null) {
                    Calendar calendar = (Calendar) clone;
                    int i2 = calendar.get(7) - 1;
                    int i3 = this.c.f()[i % 49];
                    if (i3 < i2 || i3 >= calendar.getActualMaximum(5) + i2) {
                        this.a.setVisibility(4);
                        this.b.setVisibility(4);
                        return;
                    }
                    calendar.add(5, i3 - i2);
                    if (this.c.o() == -1) {
                        Calendar n = this.c.n();
                        if (n == null) {
                            wg6.a();
                            throw null;
                        } else if (bk4.d(n.getTime(), calendar.getTime())) {
                            this.c.c(i);
                            AppCompatTextView appCompatTextView = this.a;
                            appCompatTextView.setTypeface(appCompatTextView.getTypeface(), 1);
                            this.a.setVisibility(0);
                            this.b.setVisibility(4);
                            this.a.setText(String.valueOf(calendar.get(5)));
                            this.a.setTextColor(this.c.m());
                            TreeMap<Long, Integer> j = this.c.j();
                            bk4.d(calendar);
                            wg6.a((Object) calendar, "DateHelper.getStartOfDay(calendar)");
                            num = j.get(Long.valueOf(calendar.getTimeInMillis()));
                            if (num != null && num.intValue() > 0) {
                                this.a.setText(String.valueOf(num.intValue()));
                                this.a.setTextColor(this.c.k());
                                this.b.setVisibility(0);
                                int g = num.intValue() > this.c.e() ? this.c.g() : this.c.i();
                                Boolean t = bk4.t(calendar.getTime());
                                wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
                                if (t.booleanValue()) {
                                    a(this.b, g, true);
                                    return;
                                } else {
                                    a(this, this.b, g, false, 4, (Object) null);
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    }
                    AppCompatTextView appCompatTextView2 = this.a;
                    appCompatTextView2.setTypeface(appCompatTextView2.getTypeface(), 0);
                    this.a.setVisibility(0);
                    this.b.setVisibility(4);
                    this.a.setText(String.valueOf(calendar.get(5)));
                    this.a.setTextColor(this.c.m());
                    TreeMap<Long, Integer> j2 = this.c.j();
                    bk4.d(calendar);
                    wg6.a((Object) calendar, "DateHelper.getStartOfDay(calendar)");
                    num = j2.get(Long.valueOf(calendar.getTimeInMillis()));
                    if (num != null || num.intValue() > 0) {
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type java.util.Calendar");
                }
            }
        }

        @DexIgnore
        public void onClick(View view) {
            Calendar a2;
            wg6.b(view, "v");
            int adapterPosition = getAdapterPosition();
            if (this.c.l() != null && adapterPosition != -1 && (a2 = this.c.a(adapterPosition)) != null && !a2.before(this.c.p()) && !a2.after(this.c.d())) {
                RecyclerViewHeartRateCalendar.b l = this.c.l();
                if (l != null) {
                    l.a(adapterPosition, a2);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, ImageView imageView, int i, boolean z, int i2, Object obj) {
            if ((i2 & 4) != 0) {
                z = false;
            }
            bVar.a(imageView, i, z);
        }

        @DexIgnore
        public final void a(ImageView imageView, int i, boolean z) {
            Drawable background = imageView.getBackground();
            if (background != null) {
                LayerDrawable layerDrawable = (LayerDrawable) background;
                try {
                    Drawable drawable = layerDrawable.getDrawable(0);
                    if (drawable != null) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        if (z) {
                            gradientDrawable.setStroke((int) gy5.a(2.0f), this.c.h());
                        } else {
                            gradientDrawable.setStroke((int) gy5.a(2.0f), 0);
                        }
                        if (layerDrawable.getNumberOfLayers() > 1) {
                            Drawable drawable2 = layerDrawable.getDrawable(1);
                            if (drawable2 != null) {
                                ((GradientDrawable) drawable2).setColor(i);
                                return;
                            }
                            throw new rc6("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                        }
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CalendarHeartRateAdapter", "DayViewHolder - e=" + e);
                }
            } else {
                throw new rc6("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.j = i2;
        this.k = i3;
        this.l = i5;
        this.m = i6;
        this.n = i7;
    }
}
