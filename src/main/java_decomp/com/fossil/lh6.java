package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lh6 {
    @DexIgnore
    public hi6 a(Class cls, String str) {
        return new ah6(cls, str);
    }

    @DexIgnore
    public ii6 a(tg6 tg6) {
        return tg6;
    }

    @DexIgnore
    public ki6 a(yg6 yg6) {
        return yg6;
    }

    @DexIgnore
    public mi6 a(bh6 bh6) {
        return bh6;
    }

    @DexIgnore
    public ni6 a(ch6 ch6) {
        return ch6;
    }

    @DexIgnore
    public fi6 a(Class cls) {
        return new og6(cls);
    }

    @DexIgnore
    public String a(xg6 xg6) {
        return a((sg6) xg6);
    }

    @DexIgnore
    public String a(sg6 sg6) {
        String obj = sg6.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
