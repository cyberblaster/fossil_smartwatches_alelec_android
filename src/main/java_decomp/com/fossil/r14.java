package com.fossil;

import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r14 implements Factory<dp5> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<InAppNotificationRepository> b;

    @DexIgnore
    public r14(b14 b14, Provider<InAppNotificationRepository> provider) {
        this.a = b14;
        this.b = provider;
    }

    @DexIgnore
    public static r14 a(b14 b14, Provider<InAppNotificationRepository> provider) {
        return new r14(b14, provider);
    }

    @DexIgnore
    public static dp5 b(b14 b14, Provider<InAppNotificationRepository> provider) {
        return a(b14, provider.get());
    }

    @DexIgnore
    public static dp5 a(b14 b14, InAppNotificationRepository inAppNotificationRepository) {
        dp5 a2 = b14.a(inAppNotificationRepository);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public dp5 get() {
        return b(this.a, this.b);
    }
}
