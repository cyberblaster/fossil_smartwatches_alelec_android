package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s01 extends xk1 {
    @DexIgnore
    public /* final */ boolean R;
    @DexIgnore
    public s60[] S;
    @DexIgnore
    public /* final */ r60[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ s01(ue1 ue1, q41 q41, r60[] r60Arr, short s, String str, int i) {
        super(ue1, q41, eh1.SET_DEVICE_CONFIGS, true, (i & 8) != 0 ? lk1.b.b(ue1.t, w31.DEVICE_CONFIG) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? ze0.a("UUID.randomUUID().toString()") : str, 32);
        this.T = r60Arr;
        this.R = true;
    }

    @DexIgnore
    public boolean b() {
        return this.R;
    }

    @DexIgnore
    public Object d() {
        s60[] s60Arr = this.S;
        return s60Arr != null ? s60Arr : new s60[0];
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.CONFIGS, (Object) cw0.a((p40[]) this.T));
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.AFFECTED_CONFIGS;
        s60[] s60Arr = this.S;
        return cw0.a(k, bm0, (Object) s60Arr != null ? cw0.a(s60Arr) : null);
    }

    @DexIgnore
    public byte[] n() {
        lm0 lm0 = lm0.f;
        short s = this.C;
        w40 w40 = this.x.a().i().get(Short.valueOf(w31.DEVICE_CONFIG.a));
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        return lm0.a(s, w40, this.T);
    }

    @DexIgnore
    public void q() {
        String str;
        r60[] r60Arr = this.T;
        ArrayList arrayList = new ArrayList(r60Arr.length);
        for (r60 key : r60Arr) {
            arrayList.add(key.getKey());
        }
        Object[] array = arrayList.toArray(new s60[0]);
        if (array != null) {
            this.S = (s60[]) array;
            cc0 cc0 = cc0.DEBUG;
            Object[] objArr = new Object[1];
            s60[] s60Arr = this.S;
            if (s60Arr != null) {
                str = Arrays.toString(s60Arr);
                wg6.a(str, "java.util.Arrays.toString(this)");
            } else {
                str = null;
            }
            objArr[0] = str;
            super.q();
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
