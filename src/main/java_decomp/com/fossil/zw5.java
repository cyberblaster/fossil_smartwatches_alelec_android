package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw5<T> extends LiveData<T> {
    @DexIgnore
    public static /* final */ a k; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T> LiveData<T> a() {
            return new zw5((qg6) null);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public zw5() {
        a((Object) null);
    }

    @DexIgnore
    public /* synthetic */ zw5(qg6 qg6) {
        this();
    }
}
