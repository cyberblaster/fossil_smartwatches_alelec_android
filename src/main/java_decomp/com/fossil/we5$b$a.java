package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we5$b$a<T> implements ld<yx5<? extends List<GoalTrackingSummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingOverviewWeekPresenter.b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1$2$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {62}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ we5$b$a this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.we5$b$a$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1$2$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.we5$b$a$a$a  reason: collision with other inner class name */
        public static final class C0049a extends sf6 implements ig6<il6, xe6<? super BarChart.c>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0049a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0049a aVar = new C0049a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0049a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter = this.this$0.this$0.a.this$0;
                    return goalTrackingOverviewWeekPresenter.a(GoalTrackingOverviewWeekPresenter.d(goalTrackingOverviewWeekPresenter), (List<GoalTrackingSummary>) this.this$0.$data);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(we5$b$a we5_b_a, List list, xe6 xe6) {
            super(2, xe6);
            this.this$0 = we5_b_a;
            this.$data = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$data, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter2 = this.this$0.a.this$0;
                dl6 a2 = goalTrackingOverviewWeekPresenter2.b();
                C0049a aVar = new C0049a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = goalTrackingOverviewWeekPresenter2;
                this.label = 1;
                obj = gk6.a(a2, aVar, this);
                if (obj == a) {
                    return a;
                }
                goalTrackingOverviewWeekPresenter = goalTrackingOverviewWeekPresenter2;
            } else if (i == 1) {
                goalTrackingOverviewWeekPresenter = (GoalTrackingOverviewWeekPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            goalTrackingOverviewWeekPresenter.g = (BarChart.c) obj;
            ve5 h = this.this$0.a.this$0.h;
            BarChart.c b = this.this$0.a.this$0.g;
            if (b == null) {
                b = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
            }
            h.a(b);
            return cd6.a;
        }
    }

    @DexIgnore
    public we5$b$a(GoalTrackingOverviewWeekPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<? extends List<GoalTrackingSummary>> yx5) {
        wh4 a2 = yx5.a();
        List list = (List) yx5.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - mGoalTrackingSummaries -- GoalTrackingSummaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
        if (a2 != wh4.DATABASE_LOADING) {
            rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, list, (xe6) null), 3, (Object) null);
            this.a.this$0.h.c(!this.a.this$0.j.I());
        }
    }
}
