package com.fossil;

import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t40 extends ac0 {
    @DexIgnore
    public static /* final */ a d; // = new a((qg6) null);
    @DexIgnore
    public /* final */ u40 b;
    @DexIgnore
    public /* final */ km1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final t40 a(km1 km1, HashMap<b, Object> hashMap) {
            return new t40(u40.d.a(km1, hashMap), km1);
        }
    }

    @DexIgnore
    public enum b {
        HAS_SERVICE_CHANGED
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ t40(u40 u40, km1 km1, int i) {
        this(u40, (i & 2) != 0 ? new km1((eh1) null, sk1.SUCCESS, (bn0) null, 5) : km1);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = super.a();
        km1 km1 = this.c;
        if (km1.b != sk1.SUCCESS) {
            cw0.a(a2, bm0.PHASE_RESULT, (Object) km1.a());
        }
        return a2;
    }

    @DexIgnore
    public final km1 b() {
        return this.c;
    }

    @DexIgnore
    public t40(u40 u40, km1 km1) {
        super(u40);
        this.b = u40;
        this.c = km1;
    }

    @DexIgnore
    public u40 getErrorCode() {
        return this.b;
    }
}
