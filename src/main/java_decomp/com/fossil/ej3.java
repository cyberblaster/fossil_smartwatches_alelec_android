package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ej3 {
    @DexIgnore
    public static aj3 a(int i) {
        if (i == 0) {
            return new gj3();
        }
        if (i != 1) {
            return a();
        }
        return new bj3();
    }

    @DexIgnore
    public static cj3 b() {
        return new cj3();
    }

    @DexIgnore
    public static aj3 a() {
        return new gj3();
    }

    @DexIgnore
    public static void a(View view, float f) {
        Drawable background = view.getBackground();
        if (background instanceof dj3) {
            ((dj3) background).b(f);
        }
    }

    @DexIgnore
    public static void a(View view) {
        Drawable background = view.getBackground();
        if (background instanceof dj3) {
            a(view, (dj3) background);
        }
    }

    @DexIgnore
    public static void a(View view, dj3 dj3) {
        if (dj3.y()) {
            dj3.d(li3.a(view));
        }
    }
}
