package com.fossil;

import android.os.CountDownTimer;
import com.portfolio.platform.uirenew.pairing.PairingAuthorizeFragment;
import com.sina.weibo.sdk.statistic.StatisticConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us5 {
    @DexIgnore
    public CountDownTimer a; // = new a(this, StatisticConfig.MIN_UPLOAD_INTERVAL, 1000);
    @DexIgnore
    public PairingAuthorizeFragment b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ us5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(us5 us5, long j, long j2) {
            super(j, j2);
            this.a = us5;
        }

        @DexIgnore
        public void onFinish() {
        }

        @DexIgnore
        public void onTick(long j) {
            PairingAuthorizeFragment a2 = this.a.b;
            if (a2 != null) {
                a2.r((int) (j / ((long) 1000)));
            }
        }
    }

    @DexIgnore
    public us5(PairingAuthorizeFragment pairingAuthorizeFragment) {
        this.b = pairingAuthorizeFragment;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.a.cancel();
        if (z) {
            this.a.start();
        }
    }

    @DexIgnore
    public final void a() {
        this.a.cancel();
        this.b = null;
    }
}
