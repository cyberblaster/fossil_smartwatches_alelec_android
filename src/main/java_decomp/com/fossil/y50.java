package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y50 extends be0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ HashSet<x50> a; // = new HashSet<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public y50 createFromParcel(Parcel parcel) {
            return new y50(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new y50[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m71createFromParcel(Parcel parcel) {
            return new y50(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public y50(x50[] x50Arr) {
        vd6.a(this.a, x50Arr);
    }

    @DexIgnore
    public JSONObject a() {
        return b();
    }

    @DexIgnore
    public JSONObject b() {
        JSONArray jSONArray = new JSONArray();
        for (x50 a2 : this.a) {
            jSONArray.put(a2.a());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.comps", jSONArray);
        wg6.a(put, "JSONObject().put(UIScrip\u2026ationAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final HashSet<x50> d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(y50.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.a, ((y50) obj).a) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.ComplicationConfig");
    }

    @DexIgnore
    public final x50 getBottomFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            x50 x50 = (x50) t;
            if (x50.getPositionConfig().getAngle() == 180 && x50.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (x50) t;
    }

    @DexIgnore
    public final x50 getLeftFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            x50 x50 = (x50) t;
            if (x50.getPositionConfig().getAngle() == 270 && x50.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (x50) t;
    }

    @DexIgnore
    public final x50 getRightFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            x50 x50 = (x50) t;
            if (x50.getPositionConfig().getAngle() == 90 && x50.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (x50) t;
    }

    @DexIgnore
    public final x50 getTopFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            x50 x50 = (x50) t;
            if (x50.getPositionConfig().getAngle() == 0 && x50.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (x50) t;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(yd6.m(this.a));
        }
    }

    @DexIgnore
    public y50(x50 x50, x50 x502, x50 x503, x50 x504) {
        x50.a(new lc0(0, 62));
        x502.a(new lc0(90, 62));
        x503.a(new lc0(180, 62));
        x504.a(new lc0(270, 62));
        this.a.add(x50);
        this.a.add(x502);
        this.a.add(x503);
        this.a.add(x504);
    }

    @DexIgnore
    public /* synthetic */ y50(Parcel parcel, qg6 qg6) {
        super(parcel);
        ArrayList createTypedArrayList = parcel.createTypedArrayList(x50.CREATOR);
        if (createTypedArrayList != null) {
            this.a.addAll(createTypedArrayList);
        }
    }
}
