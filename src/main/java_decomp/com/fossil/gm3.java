package com.fossil;

import com.fossil.dn3;
import com.fossil.wn3;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gm3<K, V> extends vk3<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient bm3<K, ? extends vl3<V>> map;
    @DexIgnore
    public /* final */ transient int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gm3<K, V>.f<Map.Entry<K, V>> {
        @DexIgnore
        public a(gm3 gm3) {
            super(gm3, (a) null);
        }

        @DexIgnore
        public Map.Entry<K, V> a(K k, V v) {
            return ym3.a(k, v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gm3<K, V>.f<V> {
        @DexIgnore
        public b(gm3 gm3) {
            super(gm3, (a) null);
        }

        @DexIgnore
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> {
        @DexIgnore
        public zm3<K, V> a;
        @DexIgnore
        public Comparator<? super K> b;
        @DexIgnore
        public Comparator<? super V> c;

        @DexIgnore
        public c() {
            this(bn3.a().a().b());
        }

        @DexIgnore
        public c<K, V> a(K k, V v) {
            bl3.a((Object) k, (Object) v);
            this.a.put(k, v);
            return this;
        }

        @DexIgnore
        public c(zm3<K, V> zm3) {
            this.a = zm3;
        }

        @DexIgnore
        public c<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            return a(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public c<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            for (Map.Entry a2 : iterable) {
                a(a2);
            }
            return this;
        }

        @DexIgnore
        public gm3<K, V> a() {
            if (this.c != null) {
                Iterator<Collection<V>> it = this.a.asMap().values().iterator();
                while (it.hasNext()) {
                    Collections.sort((List) it.next(), this.c);
                }
            }
            if (this.b != null) {
                tm3<K, V> b2 = bn3.a().a().b();
                for (E e : jn3.from(this.b).onKeys().immutableSortedCopy(this.a.asMap().entrySet())) {
                    b2.putAll(e.getKey(), (Iterable) e.getValue());
                }
                this.a = b2;
            }
            return gm3.copyOf(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<K, V> extends vl3<Map.Entry<K, V>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ gm3<K, V> multimap;

        @DexIgnore
        public d(gm3<K, V> gm3) {
            this.multimap = gm3;
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.multimap.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public boolean isPartialView() {
            return this.multimap.isPartialView();
        }

        @DexIgnore
        public int size() {
            return this.multimap.size();
        }

        @DexIgnore
        public jo3<Map.Entry<K, V>> iterator() {
            return this.multimap.entryIterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public static /* final */ wn3.b<gm3> a; // = wn3.a(gm3.class, "map");
        @DexIgnore
        public static /* final */ wn3.b<gm3> b; // = wn3.a(gm3.class, "size");
        @DexIgnore
        public static /* final */ wn3.b<jm3> c; // = wn3.a(jm3.class, "emptySet");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends hm3<K> {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return gm3.this.containsKey(obj);
        }

        @DexIgnore
        public int count(Object obj) {
            Collection collection = (Collection) gm3.this.map.get(obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        public Set<K> elementSet() {
            return gm3.this.keySet();
        }

        @DexIgnore
        public dn3.a<K> getEntry(int i) {
            Map.Entry entry = gm3.this.map.entrySet().asList().get(i);
            return en3.a(entry.getKey(), ((Collection) entry.getValue()).size());
        }

        @DexIgnore
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return gm3.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<K, V> extends vl3<V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ transient gm3<K, V> a;

        @DexIgnore
        public h(gm3<K, V> gm3) {
            this.a = gm3;
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return this.a.containsValue(obj);
        }

        @DexIgnore
        public int copyIntoArray(Object[] objArr, int i) {
            jo3<? extends vl3<V>> it = this.a.map.values().iterator();
            while (it.hasNext()) {
                i = ((vl3) it.next()).copyIntoArray(objArr, i);
            }
            return i;
        }

        @DexIgnore
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.a.size();
        }

        @DexIgnore
        public jo3<V> iterator() {
            return this.a.valueIterator();
        }
    }

    @DexIgnore
    public gm3(bm3<K, ? extends vl3<V>> bm3, int i) {
        this.map = bm3;
        this.size = i;
    }

    @DexIgnore
    public static <K, V> c<K, V> builder() {
        return new c<>();
    }

    @DexIgnore
    public static <K, V> gm3<K, V> copyOf(zm3<? extends K, ? extends V> zm3) {
        if (zm3 instanceof gm3) {
            gm3<K, V> gm3 = (gm3) zm3;
            if (!gm3.isPartialView()) {
                return gm3;
            }
        }
        return am3.copyOf(zm3);
    }

    @DexIgnore
    public static <K, V> gm3<K, V> of() {
        return am3.of();
    }

    @DexIgnore
    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean containsEntry(Object obj, Object obj2) {
        return super.containsEntry(obj, obj2);
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return obj != null && super.containsValue(obj);
    }

    @DexIgnore
    public Map<K, Collection<V>> createAsMap() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    public abstract vl3<V> get(K k);

    @DexIgnore
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    public abstract gm3<V, K> inverse();

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.map.isPartialView();
    }

    @DexIgnore
    @Deprecated
    public boolean put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public boolean remove(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int size() {
        return this.size;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    public static <K, V> gm3<K, V> of(K k, V v) {
        return am3.of(k, v);
    }

    @DexIgnore
    public bm3<K, Collection<V>> asMap() {
        return this.map;
    }

    @DexIgnore
    public vl3<Map.Entry<K, V>> createEntries() {
        return new d(this);
    }

    @DexIgnore
    public hm3<K> createKeys() {
        return new g();
    }

    @DexIgnore
    public vl3<V> createValues() {
        return new h(this);
    }

    @DexIgnore
    public vl3<Map.Entry<K, V>> entries() {
        return (vl3) super.entries();
    }

    @DexIgnore
    public jo3<Map.Entry<K, V>> entryIterator() {
        return new a(this);
    }

    @DexIgnore
    public im3<K> keySet() {
        return this.map.keySet();
    }

    @DexIgnore
    public hm3<K> keys() {
        return (hm3) super.keys();
    }

    @DexIgnore
    @Deprecated
    public boolean putAll(zm3<? extends K, ? extends V> zm3) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public vl3<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public vl3<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public jo3<V> valueIterator() {
        return new b(this);
    }

    @DexIgnore
    public vl3<V> values() {
        return (vl3) super.values();
    }

    @DexIgnore
    public static <K, V> gm3<K, V> of(K k, V v, K k2, V v2) {
        return am3.of(k, v, k2, v2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f<T> extends jo3<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> a;
        @DexIgnore
        public K b;
        @DexIgnore
        public Iterator<V> c;

        @DexIgnore
        public f() {
            this.a = gm3.this.asMap().entrySet().iterator();
            this.b = null;
            this.c = qm3.a();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext() || this.c.hasNext();
        }

        @DexIgnore
        public T next() {
            if (!this.c.hasNext()) {
                Map.Entry next = this.a.next();
                this.b = next.getKey();
                this.c = ((Collection) next.getValue()).iterator();
            }
            return a(this.b, this.c.next());
        }

        @DexIgnore
        public /* synthetic */ f(gm3 gm3, a aVar) {
            this();
        }
    }

    @DexIgnore
    public static <K, V> gm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return am3.of(k, v, k2, v2, k3, v3);
    }

    @DexIgnore
    public static <K, V> gm3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return am3.copyOf(iterable);
    }

    @DexIgnore
    public static <K, V> gm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return am3.of(k, v, k2, v2, k3, v3, k4, v4);
    }

    @DexIgnore
    public static <K, V> gm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return am3.of(k, v, k2, v2, k3, v3, k4, v4, k5, v5);
    }
}
