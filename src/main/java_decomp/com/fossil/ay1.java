package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.facebook.login.LoginStatusClient;
import com.fossil.j12;
import com.fossil.rv1;
import com.fossil.wv1;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay1 extends wv1 implements xy1 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ j12 d;
    @DexIgnore
    public wy1 e; // = null;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ Queue<nw1<?, ?>> i; // = new LinkedList();
    @DexIgnore
    public volatile boolean j;
    @DexIgnore
    public long k;
    @DexIgnore
    public long l;
    @DexIgnore
    public /* final */ dy1 m;
    @DexIgnore
    public /* final */ jv1 n;
    @DexIgnore
    public vy1 o;
    @DexIgnore
    public /* final */ Map<rv1.c<?>, rv1.f> p;
    @DexIgnore
    public Set<Scope> q;
    @DexIgnore
    public /* final */ e12 r;
    @DexIgnore
    public /* final */ Map<rv1<?>, Boolean> s;
    @DexIgnore
    public /* final */ rv1.a<? extends ac3, lb3> t;
    @DexIgnore
    public /* final */ vw1 u;
    @DexIgnore
    public /* final */ ArrayList<a02> v;
    @DexIgnore
    public Integer w;
    @DexIgnore
    public Set<gz1> x;
    @DexIgnore
    public /* final */ lz1 y;
    @DexIgnore
    public /* final */ j12.a z;

    @DexIgnore
    public ay1(Context context, Lock lock, Looper looper, e12 e12, jv1 jv1, rv1.a<? extends ac3, lb3> aVar, Map<rv1<?>, Boolean> map, List<wv1.b> list, List<wv1.c> list2, Map<rv1.c<?>, rv1.f> map2, int i2, int i3, ArrayList<a02> arrayList, boolean z2) {
        Looper looper2 = looper;
        this.k = j42.a() ? 10000 : 120000;
        this.l = LoginStatusClient.DEFAULT_TOAST_DURATION_MS;
        this.q = new HashSet();
        this.u = new vw1();
        this.w = null;
        this.x = null;
        this.z = new zx1(this);
        this.g = context;
        this.b = lock;
        this.c = false;
        this.d = new j12(looper, this.z);
        this.h = looper2;
        this.m = new dy1(this, looper);
        this.n = jv1;
        this.f = i2;
        if (this.f >= 0) {
            this.w = Integer.valueOf(i3);
        }
        this.s = map;
        this.p = map2;
        this.v = arrayList;
        this.y = new lz1(this.p);
        for (wv1.b a : list) {
            this.d.a(a);
        }
        for (wv1.c a2 : list2) {
            this.d.a(a2);
        }
        this.r = e12;
        this.t = aVar;
    }

    @DexIgnore
    public static String c(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T a(T t2) {
        w12.a(t2.i() != null, (Object) "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        w12.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                this.i.add(t2);
                return t2;
            }
            T b3 = this.e.b(t2);
            this.b.unlock();
            return b3;
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T b(T t2) {
        w12.a(t2.i() != null, (Object) "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        w12.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.j) {
                this.i.add(t2);
                while (!this.i.isEmpty()) {
                    nw1 remove = this.i.remove();
                    this.y.a(remove);
                    remove.c(Status.g);
                }
                return t2;
            } else {
                T a = this.e.a(t2);
                this.b.unlock();
                return a;
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void c() {
        this.b.lock();
        try {
            boolean z2 = false;
            if (this.f >= 0) {
                if (this.w != null) {
                    z2 = true;
                }
                w12.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<rv1.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            a(this.w.intValue());
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void d() {
        this.b.lock();
        try {
            this.y.a();
            if (this.e != null) {
                this.e.a();
            }
            this.u.a();
            for (nw1 nw1 : this.i) {
                nw1.a((mz1) null);
                nw1.a();
            }
            this.i.clear();
            if (this.e != null) {
                o();
                this.d.a();
                this.b.unlock();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final Context e() {
        return this.g;
    }

    @DexIgnore
    public final Looper f() {
        return this.h;
    }

    @DexIgnore
    public final boolean g() {
        wy1 wy1 = this.e;
        return wy1 != null && wy1.c();
    }

    @DexIgnore
    public final void h() {
        wy1 wy1 = this.e;
        if (wy1 != null) {
            wy1.d();
        }
    }

    @DexIgnore
    public final void k() {
        d();
        c();
    }

    @DexIgnore
    public final void l() {
        this.b.lock();
        try {
            if (this.j) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void m() {
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public final void n() {
        this.b.lock();
        try {
            if (o()) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final boolean o() {
        if (!this.j) {
            return false;
        }
        this.j = false;
        this.m.removeMessages(2);
        this.m.removeMessages(1);
        vy1 vy1 = this.o;
        if (vy1 != null) {
            vy1.a();
            this.o = null;
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean p() {
        this.b.lock();
        try {
            if (this.x == null) {
                this.b.unlock();
                return false;
            }
            boolean z2 = !this.x.isEmpty();
            this.b.unlock();
            return z2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final String q() {
        StringWriter stringWriter = new StringWriter();
        a("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    @DexIgnore
    public final void a(int i2) {
        this.b.lock();
        boolean z2 = true;
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z2 = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            w12.a(z2, (Object) sb.toString());
            b(i2);
            m();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final gv1 a() {
        boolean z2 = true;
        w12.b(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.b.lock();
        try {
            if (this.f >= 0) {
                if (this.w == null) {
                    z2 = false;
                }
                w12.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<rv1.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            b(this.w.intValue());
            this.d.b();
            return this.e.f();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final yv1<Status> b() {
        w12.b(g(), "GoogleApiClient is not connected yet.");
        w12.b(this.w.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        ax1 ax1 = new ax1(this);
        if (this.p.containsKey(h22.a)) {
            a(this, ax1, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            cy1 cy1 = new cy1(this, atomicReference, ax1);
            by1 by1 = new by1(this, ax1);
            wv1.a aVar = new wv1.a(this.g);
            aVar.a((rv1<? extends rv1.d.e>) h22.c);
            aVar.a((wv1.b) cy1);
            aVar.a((wv1.c) by1);
            aVar.a((Handler) this.m);
            wv1 a = aVar.a();
            atomicReference.set(a);
            a.c();
        }
        return ax1;
    }

    @DexIgnore
    public final void a(wv1 wv1, ax1 ax1, boolean z2) {
        h22.d.a(wv1).a(new fy1(this, ax1, z2, wv1));
    }

    @DexIgnore
    public final void a(wv1.c cVar) {
        this.d.a(cVar);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        while (!this.i.isEmpty()) {
            b(this.i.remove());
        }
        this.d.a(bundle);
    }

    @DexIgnore
    public final void b(int i2) {
        Integer num = this.w;
        if (num == null) {
            this.w = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String c2 = c(i2);
            String c3 = c(this.w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 51 + String.valueOf(c3).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c2);
            sb.append(". Mode was already set to ");
            sb.append(c3);
            throw new IllegalStateException(sb.toString());
        }
        if (this.e == null) {
            boolean z2 = false;
            boolean z3 = false;
            for (rv1.f next : this.p.values()) {
                if (next.m()) {
                    z2 = true;
                }
                if (next.d()) {
                    z3 = true;
                }
            }
            int intValue = this.w.intValue();
            if (intValue != 1) {
                if (intValue == 2) {
                    if (z2) {
                        if (this.c) {
                            this.e = new g02(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, true);
                            return;
                        }
                        this.e = b02.a(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v);
                        return;
                    }
                }
            } else if (!z2) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z3) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.c || z3) {
                this.e = new jy1(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this);
                return;
            }
            this.e = new g02(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, false);
        }
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        if (!this.n.b(this.g, gv1.p())) {
            o();
        }
        if (!this.j) {
            this.d.a(gv1);
            this.d.a();
        }
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        if (i2 == 1 && !z2 && !this.j) {
            this.j = true;
            if (this.o == null && !j42.a()) {
                try {
                    this.o = this.n.a(this.g.getApplicationContext(), (uy1) new hy1(this));
                } catch (SecurityException unused) {
                }
            }
            dy1 dy1 = this.m;
            dy1.sendMessageDelayed(dy1.obtainMessage(1), this.k);
            dy1 dy12 = this.m;
            dy12.sendMessageDelayed(dy12.obtainMessage(2), this.l);
        }
        this.y.b();
        this.d.a(i2);
        this.d.a();
        if (i2 == 2) {
            m();
        }
    }

    @DexIgnore
    public final void b(wv1.c cVar) {
        this.d.b(cVar);
    }

    @DexIgnore
    public final boolean a(yw1 yw1) {
        wy1 wy1 = this.e;
        return wy1 != null && wy1.a(yw1);
    }

    @DexIgnore
    public final void a(gz1 gz1) {
        this.b.lock();
        try {
            if (this.x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.x.remove(gz1)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!p()) {
                this.e.e();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("mContext=").println(this.g);
        printWriter.append(str).append("mResuming=").print(this.j);
        printWriter.append(" mWorkQueue.size()=").print(this.i.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.y.a.size());
        wy1 wy1 = this.e;
        if (wy1 != null) {
            wy1.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public static int a(Iterable<rv1.f> iterable, boolean z2) {
        boolean z3 = false;
        boolean z4 = false;
        for (rv1.f next : iterable) {
            if (next.m()) {
                z3 = true;
            }
            if (next.d()) {
                z4 = true;
            }
        }
        if (!z3) {
            return 3;
        }
        if (!z4 || !z2) {
            return 1;
        }
        return 2;
    }
}
