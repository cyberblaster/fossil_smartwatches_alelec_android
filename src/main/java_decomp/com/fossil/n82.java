package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.u12;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n82 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<n82> CREATOR; // = new m82();
    @DexIgnore
    public /* final */ DataSet a;
    @DexIgnore
    public /* final */ nd2 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public n82(DataSet dataSet, IBinder iBinder, boolean z) {
        this.a = dataSet;
        this.b = pd2.a(iBinder);
        this.c = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof n82) && u12.a(this.a, ((n82) obj).a);
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(this.a);
    }

    @DexIgnore
    public final String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("dataSet", this.a);
        return a2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) this.a, i, false);
        nd2 nd2 = this.b;
        g22.a(parcel, 2, nd2 == null ? null : nd2.asBinder(), false);
        g22.a(parcel, 4, this.c);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public n82(DataSet dataSet, nd2 nd2, boolean z) {
        this.a = dataSet;
        this.b = nd2;
        this.c = z;
    }
}
