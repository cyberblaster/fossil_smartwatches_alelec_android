package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.BinaryFile;
import com.fossil.fitness.FitnessAlgorithm;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.Result;
import com.fossil.fitness.StatusCode;
import com.fossil.fitness.UserProfile;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hx0 extends x51 {
    @DexIgnore
    public /* final */ boolean T; // = true;
    @DexIgnore
    public FitnessData[] U;
    @DexIgnore
    public StatusCode V;
    @DexIgnore
    public /* final */ h60 W;

    @DexIgnore
    public hx0(ue1 ue1, q41 q41, h60 h60, HashMap<io0, Object> hashMap, String str) {
        super(ue1, q41, eh1.SYNC, lk1.b.a(ue1.t, w31.ACTIVITY_FILE), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
        this.W = h60;
    }

    @DexIgnore
    public boolean b() {
        return this.T;
    }

    @DexIgnore
    public Object d() {
        FitnessData[] fitnessDataArr = this.U;
        return fitnessDataArr != null ? fitnessDataArr : new FitnessData[0];
    }

    @DexIgnore
    public void h() {
        if (fm0.f.b(this.x.a())) {
            if1.a((if1) this, (if1) new h21(this.w, this.x, this.W, false, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 32), (hg6) new cs0(this), (hg6) new vt0(this), (ig6) new mv0(this), (hg6) null, (hg6) null, 48, (Object) null);
            return;
        }
        super.h();
    }

    @DexIgnore
    public JSONObject i() {
        JSONObject put = super.i().put(s60.BIOMETRIC_PROFILE.b(), this.W.d());
        wg6.a(put, "super.optionDescription(\u2026ofile.valueDescription())");
        return put;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002f, code lost:
        if (r2 != null) goto L_0x0034;
     */
    @DexIgnore
    public JSONObject k() {
        Object obj;
        String name;
        JSONObject k = super.k();
        bm0 bm0 = bm0.FITNESS_DATA;
        FitnessData[] fitnessDataArr = this.U;
        JSONObject a = cw0.a(k, bm0, (Object) fitnessDataArr != null ? cw0.a(fitnessDataArr) : null);
        bm0 bm02 = bm0.MSL_STATUS_CODE;
        StatusCode statusCode = this.V;
        if (!(statusCode == null || (name = statusCode.name()) == null)) {
            obj = name.toLowerCase(mi0.A.h());
            wg6.a(obj, "(this as java.lang.String).toLowerCase(locale)");
        }
        obj = JSONObject.NULL;
        return cw0.a(a, bm02, obj);
    }

    @DexIgnore
    public void a(ArrayList<ie1> arrayList) {
        b(arrayList);
        a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
    }

    @DexIgnore
    public final void b(ArrayList<ie1> arrayList) {
        ArrayList arrayList2 = new ArrayList(rd6.a(arrayList, 10));
        for (ie1 ie1 : arrayList) {
            arrayList2.add(new BinaryFile(ie1.e, (int) (ie1.h / ((long) 1000))));
        }
        Result parse = FitnessAlgorithm.create().parse(new ArrayList(arrayList2), new UserProfile((short) this.W.getAge(), this.W.getGender().b(), ((float) this.W.getHeightInCentimeter()) / 100.0f, (float) this.W.getWeightInKilogram()));
        wg6.a(parse, "parsedResult");
        this.V = parse.getStatus();
        if (parse.getStatus() == StatusCode.SUCCESS) {
            ArrayList<FitnessData> fitnessData = parse.getFitnessData();
            wg6.a(fitnessData, "parsedResult.fitnessData");
            Object[] array = fitnessData.toArray(new FitnessData[0]);
            if (array != null) {
                this.U = (FitnessData[]) array;
                cc0 cc0 = cc0.DEBUG;
                Object[] objArr = new Object[1];
                FitnessData[] fitnessDataArr = this.U;
                objArr[0] = fitnessDataArr != null ? cw0.a(fitnessDataArr).toString(2) : null;
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        cc0 cc02 = cc0.DEBUG;
        new Object[1][0] = parse.getStatus();
    }
}
