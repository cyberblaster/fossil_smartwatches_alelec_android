package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y80 extends p40 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ j70 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            float readFloat = parcel.readFloat();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                return new y80(readInt, readFloat, j70.valueOf(readString));
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new y80[i];
        }
    }

    @DexIgnore
    public y80(int i, float f, j70 j70) throws IllegalArgumentException {
        this.a = i;
        this.b = cw0.a(f, 2);
        this.c = j70;
        int i2 = this.a;
        if (!(i2 >= 0 && 23 >= i2)) {
            throw new IllegalArgumentException(ze0.a(ze0.b("hourIn24Format("), this.a, ") is out of range ", "[0, 23]."));
        }
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.HOUR, (Object) Integer.valueOf(this.a)), bm0.TEMPERATURE, (Object) Float.valueOf(this.b)), bm0.WEATHER_CONDITION, (Object) cw0.a((Enum<?>) this.c));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject put = new JSONObject().put(AppFilter.COLUMN_HOUR, this.a).put("temp", Float.valueOf(this.b)).put("cond_id", this.c.a());
        wg6.a(put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(y80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            y80 y80 = (y80) obj;
            return this.a == y80.a && this.b == y80.b && this.c == y80.c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherHourForecast");
    }

    @DexIgnore
    public final int getHourIn24Format() {
        return this.a;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.b;
    }

    @DexIgnore
    public final j70 getWeatherCondition() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        return this.c.hashCode() + ((hashCode + (this.a * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
