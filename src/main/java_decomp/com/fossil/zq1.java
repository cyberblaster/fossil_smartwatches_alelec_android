package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.AlarmManagerSchedulerBroadcastReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zq1 implements rr1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ur1 b;
    @DexIgnore
    public AlarmManager c;
    @DexIgnore
    public /* final */ fr1 d;
    @DexIgnore
    public /* final */ zs1 e;

    @DexIgnore
    public zq1(Context context, ur1 ur1, zs1 zs1, fr1 fr1) {
        this(context, ur1, (AlarmManager) context.getSystemService("alarm"), zs1, fr1);
    }

    @DexIgnore
    public boolean a(Intent intent) {
        return PendingIntent.getBroadcast(this.a, 0, intent, 536870912) != null;
    }

    @DexIgnore
    public void a(rp1 rp1, int i) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("backendName", rp1.a());
        builder.appendQueryParameter("priority", String.valueOf(ft1.a(rp1.c())));
        if (rp1.b() != null) {
            builder.appendQueryParameter(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(rp1.b(), 0));
        }
        Intent intent = new Intent(this.a, AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(builder.build());
        intent.putExtra("attemptNumber", i);
        if (a(intent)) {
            mq1.a("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", (Object) rp1);
            return;
        }
        long b2 = this.b.b(rp1);
        long a2 = this.d.a(rp1.c(), b2, i);
        mq1.a("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", rp1, Long.valueOf(a2), Long.valueOf(b2), Integer.valueOf(i));
        this.c.set(3, this.e.a() + a2, PendingIntent.getBroadcast(this.a, 0, intent, 0));
    }

    @DexIgnore
    public zq1(Context context, ur1 ur1, AlarmManager alarmManager, zs1 zs1, fr1 fr1) {
        this.a = context;
        this.b = ur1;
        this.c = alarmManager;
        this.e = zs1;
        this.d = fr1;
    }
}
