package com.fossil;

import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z24 {
    @DexIgnore
    public /* final */ a34 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends y24.c, E extends y24.a> implements y24.d<R, E> {
        @DexIgnore
        public /* final */ y24.d<R, E> a;
        @DexIgnore
        public /* final */ z24 b;

        @DexIgnore
        public a(y24.d<R, E> dVar, z24 z24) {
            this.a = dVar;
            this.b = z24;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(R r) {
            this.b.a(r, this.a);
        }

        @DexIgnore
        public void a(E e) {
            this.b.a(e, this.a);
        }
    }

    @DexIgnore
    public z24(a34 a34) {
        this.a = a34;
    }

    @DexIgnore
    public <Q extends y24.b, R extends y24.c, E extends y24.a> void a(y24<Q, R, E> y24, Q q, y24.d<R, E> dVar) {
        y24.b(q);
        y24.a((y24.d<R, E>) new a(dVar, this));
        mx5.c();
        this.a.execute(new t04(y24));
    }

    @DexIgnore
    public static /* synthetic */ void a(y24 y24) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UseCaseHandler", "execute: getIdlingResource = " + mx5.b());
        y24.b();
        if (!mx5.b().a()) {
            mx5.a();
        }
    }

    @DexIgnore
    public <R extends y24.c, E extends y24.a> void a(R r, y24.d<R, E> dVar) {
        this.a.a(r, dVar);
    }

    @DexIgnore
    public <R extends y24.c, E extends y24.a> void a(E e, y24.d<R, E> dVar) {
        this.a.a(e, dVar);
    }
}
