package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class i93 implements Runnable {
    @DexIgnore
    public /* final */ j93 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ t43 c;
    @DexIgnore
    public /* final */ Intent d;

    @DexIgnore
    public i93(j93 j93, int i, t43 t43, Intent intent) {
        this.a = j93;
        this.b = i;
        this.c = t43;
        this.d = intent;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c, this.d);
    }
}
