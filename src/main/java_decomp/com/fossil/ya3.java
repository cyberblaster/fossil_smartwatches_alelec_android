package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya3 extends za3 {
    @DexIgnore
    public zi2 g;
    @DexIgnore
    public /* final */ /* synthetic */ sa3 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ya3(sa3 sa3, String str, int i, zi2 zi2) {
        super(str, i);
        this.h = sa3;
        this.g = zi2;
    }

    @DexIgnore
    public final int a() {
        return this.g.o();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v15, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final boolean a(Long l, uj2 uj2, boolean z) {
        boolean d = this.h.l().d(this.a, l03.u0);
        boolean d2 = this.h.l().d(this.a, l03.A0);
        boolean r = this.g.r();
        boolean s = this.g.s();
        boolean z2 = d && this.g.v();
        boolean z3 = r || s || z2;
        Boolean bool = null;
        if (!z || z3) {
            xi2 q = this.g.q();
            boolean s2 = q.s();
            if (uj2.s()) {
                if (!q.p()) {
                    this.h.b().w().a("No number filter for long property. property", this.h.i().c(uj2.p()));
                } else {
                    bool = za3.a(za3.a(uj2.t(), q.q()), s2);
                }
            } else if (uj2.v()) {
                if (!q.p()) {
                    this.h.b().w().a("No number filter for double property. property", this.h.i().c(uj2.p()));
                } else {
                    bool = za3.a(za3.a(uj2.w(), q.q()), s2);
                }
            } else if (!uj2.q()) {
                this.h.b().w().a("User property has no value, property", this.h.i().c(uj2.p()));
            } else if (q.n()) {
                bool = za3.a(za3.a(uj2.r(), q.o(), this.h.b()), s2);
            } else if (!q.p()) {
                this.h.b().w().a("No string or number filter defined. property", this.h.i().c(uj2.p()));
            } else if (ia3.a(uj2.r())) {
                bool = za3.a(za3.a(uj2.r(), q.q()), s2);
            } else {
                this.h.b().w().a("Invalid user property value for Numeric number filter. property, value", this.h.i().c(uj2.p()), uj2.r());
            }
            this.h.b().B().a("Property filter result", bool == null ? "null" : bool);
            if (bool == null) {
                return false;
            }
            this.c = true;
            if (d && z2 && !bool.booleanValue()) {
                return true;
            }
            if (!z || this.g.r()) {
                this.d = bool;
            }
            if (bool.booleanValue() && z3 && uj2.n()) {
                long o = uj2.o();
                if (d2 && l != null) {
                    o = l.longValue();
                }
                if (s) {
                    this.f = Long.valueOf(o);
                } else {
                    this.e = Long.valueOf(o);
                }
            }
            return true;
        }
        v43 B = this.h.b().B();
        Integer valueOf = Integer.valueOf(this.b);
        if (this.g.n()) {
            bool = Integer.valueOf(this.g.o());
        }
        B.a("Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID", valueOf, bool);
        return true;
    }
}
