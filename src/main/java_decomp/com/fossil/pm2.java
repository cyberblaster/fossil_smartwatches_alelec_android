package com.fossil;

import com.facebook.internal.FileLruCache;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pm2 extends zl2 {
    @DexIgnore
    public static /* final */ Logger b; // = Logger.getLogger(pm2.class.getName());
    @DexIgnore
    public static /* final */ boolean c; // = cq2.a();
    @DexIgnore
    public rm2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends IOException {
        @DexIgnore
        public a() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        @DexIgnore
        public a(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public a(String str, Throwable th) {
            super(r3.length() != 0 ? "CodedOutputStream was writing to a flat byte array and ran out of space.: ".concat(r3) : new String("CodedOutputStream was writing to a flat byte array and ran out of space.: "), th);
            String valueOf = String.valueOf(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends pm2 {
        @DexIgnore
        public /* final */ byte[] d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public b(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i2 + 0;
                if ((i2 | 0 | (bArr.length - i3)) >= 0) {
                    this.d = bArr;
                    this.f = 0;
                    this.e = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", new Object[]{Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)}));
            }
            throw new NullPointerException(FileLruCache.BufferFile.FILE_NAME_PREFIX);
        }

        @DexIgnore
        public final void a(int i, int i2) throws IOException {
            b((i << 3) | i2);
        }

        @DexIgnore
        public final void b(int i, int i2) throws IOException {
            a(i, 0);
            a(i2);
        }

        @DexIgnore
        public final void c(int i, int i2) throws IOException {
            a(i, 0);
            b(i2);
        }

        @DexIgnore
        public final void d(int i) throws IOException {
            try {
                byte[] bArr = this.d;
                int i2 = this.f;
                this.f = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.d;
                int i5 = this.f;
                this.f = i5 + 1;
                bArr4[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e2) {
                throw new a(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
            }
        }

        @DexIgnore
        public final void e(int i, int i2) throws IOException {
            a(i, 5);
            d(i2);
        }

        @DexIgnore
        public final void a(int i, long j) throws IOException {
            a(i, 0);
            a(j);
        }

        @DexIgnore
        public final void b(yl2 yl2) throws IOException {
            b(yl2.zza());
            yl2.zza((zl2) this);
        }

        @DexIgnore
        public final void c(int i, long j) throws IOException {
            a(i, 1);
            c(j);
        }

        @DexIgnore
        public final void a(int i, boolean z) throws IOException {
            a(i, 0);
            a(z ? (byte) 1 : 0);
        }

        @DexIgnore
        public final void b(int i, yl2 yl2) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, yl2);
            a(1, 4);
        }

        @DexIgnore
        public final void c(long j) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                this.f = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.d;
                int i2 = this.f;
                this.f = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.d;
                int i3 = this.f;
                this.f = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.d;
                int i5 = this.f;
                this.f = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.d;
                int i6 = this.f;
                this.f = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.d;
                int i7 = this.f;
                this.f = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.d;
                int i8 = this.f;
                this.f = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e2) {
                throw new a(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
            }
        }

        @DexIgnore
        public final void a(int i, String str) throws IOException {
            a(i, 2);
            b(str);
        }

        @DexIgnore
        public final void a(int i, yl2 yl2) throws IOException {
            a(i, 2);
            b(yl2);
        }

        @DexIgnore
        public final void b(int i) throws IOException {
            if (!pm2.c || vl2.a() || a() < 5) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.d;
                    int i2 = this.f;
                    this.f = i2 + 1;
                    bArr[i2] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i3 = this.f;
                    this.f = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e2) {
                    throw new a(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
                }
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                cq2.a(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.d;
                int i5 = this.f;
                this.f = i5 + 1;
                cq2.a(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.d;
                    int i7 = this.f;
                    this.f = i7 + 1;
                    cq2.a(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.d;
                int i8 = this.f;
                this.f = i8 + 1;
                cq2.a(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.d;
                    int i10 = this.f;
                    this.f = i10 + 1;
                    cq2.a(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.d;
                int i11 = this.f;
                this.f = i11 + 1;
                cq2.a(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.d;
                    int i13 = this.f;
                    this.f = i13 + 1;
                    cq2.a(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.d;
                int i14 = this.f;
                this.f = i14 + 1;
                cq2.a(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.d;
                int i15 = this.f;
                this.f = i15 + 1;
                cq2.a(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        @DexIgnore
        public final void a(int i, ro2 ro2, fp2 fp2) throws IOException {
            a(i, 2);
            ql2 ql2 = (ql2) ro2;
            int g = ql2.g();
            if (g == -1) {
                g = fp2.zzb(ql2);
                ql2.a(g);
            }
            b(g);
            fp2.a(ro2, this.a);
        }

        @DexIgnore
        public final void a(int i, ro2 ro2) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, 2);
            a(ro2);
            a(1, 4);
        }

        @DexIgnore
        public final void a(ro2 ro2) throws IOException {
            b(ro2.e());
            ro2.a(this);
        }

        @DexIgnore
        public final void a(byte b) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                this.f = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e2) {
                throw new a(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
            }
        }

        @DexIgnore
        public final void b(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.d, this.f, i2);
                this.f += i2;
            } catch (IndexOutOfBoundsException e2) {
                throw new a(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), Integer.valueOf(i2)}), e2);
            }
        }

        @DexIgnore
        public final void a(int i) throws IOException {
            if (i >= 0) {
                b(i);
            } else {
                a((long) i);
            }
        }

        @DexIgnore
        public final void a(long j) throws IOException {
            if (!pm2.c || a() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.d;
                    int i = this.f;
                    this.f = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i2 = this.f;
                    this.f = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e2) {
                    throw new a(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f), Integer.valueOf(this.e), 1}), e2);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.d;
                    int i3 = this.f;
                    this.f = i3 + 1;
                    cq2.a(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                cq2.a(bArr4, (long) i4, (byte) ((int) j));
            }
        }

        @DexIgnore
        public final void b(String str) throws IOException {
            int i = this.f;
            try {
                int g = pm2.g(str.length() * 3);
                int g2 = pm2.g(str.length());
                if (g2 == g) {
                    this.f = i + g2;
                    int a = fq2.a(str, this.d, this.f, a());
                    this.f = i;
                    b((a - i) - g2);
                    this.f = a;
                    return;
                }
                b(fq2.a((CharSequence) str));
                this.f = fq2.a(str, this.d, this.f, a());
            } catch (iq2 e2) {
                this.f = i;
                a(str, e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new a(e3);
            }
        }

        @DexIgnore
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            b(bArr, i, i2);
        }

        @DexIgnore
        public final int a() {
            return this.e - this.f;
        }
    }

    @DexIgnore
    public pm2() {
    }

    @DexIgnore
    public static pm2 a(byte[] bArr) {
        return new b(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static int b(double d) {
        return 8;
    }

    @DexIgnore
    public static int b(float f) {
        return 4;
    }

    @DexIgnore
    public static int b(boolean z) {
        return 1;
    }

    @DexIgnore
    public static int e(int i, long j) {
        return e(i) + e(j);
    }

    @DexIgnore
    public static int e(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    @DexIgnore
    public static int f(int i, int i2) {
        return e(i) + f(i2);
    }

    @DexIgnore
    public static int g(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    @DexIgnore
    public static int g(int i, int i2) {
        return e(i) + g(i2);
    }

    @DexIgnore
    public static int g(long j) {
        return 8;
    }

    @DexIgnore
    public static int h(int i, int i2) {
        return e(i) + g(l(i2));
    }

    @DexIgnore
    public static int h(long j) {
        return 8;
    }

    @DexIgnore
    public static int i(int i) {
        return 4;
    }

    @DexIgnore
    public static int i(int i, int i2) {
        return e(i) + 4;
    }

    @DexIgnore
    public static long i(long j) {
        return (j >> 63) ^ (j << 1);
    }

    @DexIgnore
    public static int j(int i) {
        return 4;
    }

    @DexIgnore
    public static int j(int i, int i2) {
        return e(i) + 4;
    }

    @DexIgnore
    public static int k(int i, int i2) {
        return e(i) + f(i2);
    }

    @DexIgnore
    public static int l(int i) {
        return (i >> 31) ^ (i << 1);
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract void a(byte b2) throws IOException;

    @DexIgnore
    public abstract void a(int i) throws IOException;

    @DexIgnore
    public abstract void a(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void a(int i, long j) throws IOException;

    @DexIgnore
    public abstract void a(int i, ro2 ro2) throws IOException;

    @DexIgnore
    public abstract void a(int i, ro2 ro2, fp2 fp2) throws IOException;

    @DexIgnore
    public abstract void a(int i, yl2 yl2) throws IOException;

    @DexIgnore
    public abstract void a(int i, String str) throws IOException;

    @DexIgnore
    public abstract void a(int i, boolean z) throws IOException;

    @DexIgnore
    public abstract void a(long j) throws IOException;

    @DexIgnore
    public abstract void b(int i) throws IOException;

    @DexIgnore
    public abstract void b(int i, int i2) throws IOException;

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        a(i, i(j));
    }

    @DexIgnore
    public abstract void b(int i, yl2 yl2) throws IOException;

    @DexIgnore
    public final void c(int i) throws IOException {
        b(l(i));
    }

    @DexIgnore
    public abstract void c(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void c(int i, long j) throws IOException;

    @DexIgnore
    public abstract void c(long j) throws IOException;

    @DexIgnore
    public abstract void d(int i) throws IOException;

    @DexIgnore
    public final void d(int i, int i2) throws IOException {
        c(i, l(i2));
    }

    @DexIgnore
    public abstract void e(int i, int i2) throws IOException;

    @DexIgnore
    public static int c(int i, yl2 yl2) {
        int e = e(i);
        int zza = yl2.zza();
        return e + g(zza) + zza;
    }

    @DexIgnore
    public static int d(int i, long j) {
        return e(i) + e(j);
    }

    @DexIgnore
    public static int e(int i) {
        return g(i << 3);
    }

    @DexIgnore
    public static int f(int i, long j) {
        return e(i) + e(i(j));
    }

    @DexIgnore
    public static int g(int i, long j) {
        return e(i) + 8;
    }

    @DexIgnore
    public final void b(long j) throws IOException {
        a(i(j));
    }

    @DexIgnore
    public static int b(int i, float f) {
        return e(i) + 4;
    }

    @DexIgnore
    public static int h(int i, long j) {
        return e(i) + 8;
    }

    @DexIgnore
    public static int k(int i) {
        return f(i);
    }

    @DexIgnore
    public final void a(int i, float f) throws IOException {
        e(i, Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public static int b(int i, double d) {
        return e(i) + 8;
    }

    @DexIgnore
    public static int d(int i, yl2 yl2) {
        return (e(1) << 1) + g(2, i) + c(3, yl2);
    }

    @DexIgnore
    public static int f(int i) {
        if (i >= 0) {
            return g(i);
        }
        return 10;
    }

    @DexIgnore
    public static int h(int i) {
        return g(l(i));
    }

    @DexIgnore
    public final void a(int i, double d) throws IOException {
        c(i, Double.doubleToRawLongBits(d));
    }

    @DexIgnore
    public static int b(int i, boolean z) {
        return e(i) + 1;
    }

    @DexIgnore
    @Deprecated
    public static int c(int i, ro2 ro2, fp2 fp2) {
        int e = e(i) << 1;
        ql2 ql2 = (ql2) ro2;
        int g = ql2.g();
        if (g == -1) {
            g = fp2.zzb(ql2);
            ql2.a(g);
        }
        return e + g;
    }

    @DexIgnore
    public static int f(long j) {
        return e(i(j));
    }

    @DexIgnore
    public final void a(float f) throws IOException {
        d(Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public static int b(int i, String str) {
        return e(i) + a(str);
    }

    @DexIgnore
    public final void a(double d) throws IOException {
        c(Double.doubleToRawLongBits(d));
    }

    @DexIgnore
    public static int b(int i, ro2 ro2, fp2 fp2) {
        return e(i) + a(ro2, fp2);
    }

    @DexIgnore
    public static int d(long j) {
        return e(j);
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        a(z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static int a(int i, vn2 vn2) {
        int e = e(i);
        int a2 = vn2.a();
        return e + g(a2) + a2;
    }

    @DexIgnore
    public final void b() {
        if (a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    @DexIgnore
    public static int a(String str) {
        int i;
        try {
            i = fq2.a((CharSequence) str);
        } catch (iq2 unused) {
            i = str.getBytes(hn2.a).length;
        }
        return g(i) + i;
    }

    @DexIgnore
    public static int a(vn2 vn2) {
        int a2 = vn2.a();
        return g(a2) + a2;
    }

    @DexIgnore
    public static int a(yl2 yl2) {
        int zza = yl2.zza();
        return g(zza) + zza;
    }

    @DexIgnore
    public static int a(ro2 ro2, fp2 fp2) {
        ql2 ql2 = (ql2) ro2;
        int g = ql2.g();
        if (g == -1) {
            g = fp2.zzb(ql2);
            ql2.a(g);
        }
        return g(g) + g;
    }

    @DexIgnore
    public final void a(String str, iq2 iq2) throws IOException {
        b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", iq2);
        byte[] bytes = str.getBytes(hn2.a);
        try {
            b(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new a(e);
        } catch (a e2) {
            throw e2;
        }
    }
}
