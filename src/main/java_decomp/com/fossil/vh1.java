package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattServer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vh1 extends q81 {
    @DexIgnore
    public /* final */ boolean n;
    @DexIgnore
    public /* final */ hn1<p51> o;
    @DexIgnore
    public /* final */ an1 p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ byte[] s;

    @DexIgnore
    public vh1(an1 an1, int i, int i2, byte[] bArr, vj0 vj0) {
        super(ma1.SEND_RESPONSE, an1.a, vj0);
        this.p = an1;
        this.q = i;
        this.r = i2;
        this.s = bArr;
    }

    @DexIgnore
    public void a(fu0 fu0) {
        b31 b31;
        an1 an1 = this.p;
        BluetoothDevice bluetoothDevice = an1.a;
        int i = an1.b;
        int i2 = this.q;
        int i3 = this.r;
        byte[] bArr = this.s;
        BluetoothGattServer bluetoothGattServer = fu0.a;
        if (bluetoothGattServer != null ? bluetoothGattServer.sendResponse(bluetoothDevice, i, i2, i3, bArr) : false) {
            b31 = b31.a(this.f, (ma1) null, g11.SUCCESS, (t31) null, 5);
        } else {
            b31 = b31.a(this.f, (ma1) null, g11.GATT_ERROR, new t31(x11.START_FAIL, 0, 2), 1);
        }
        this.f = b31;
        a();
    }

    @DexIgnore
    public boolean b() {
        return this.n;
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return false;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.o;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        JSONObject a = cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.REQUEST, (Object) this.p.a()), bm0.STATUS, (Object) Integer.valueOf(this.q)), bm0.OFFSET, (Object) Integer.valueOf(this.r));
        bm0 bm0 = bm0.VALUE;
        byte[] bArr = this.s;
        String str = null;
        if (bArr != null) {
            str = cw0.a(bArr, (String) null, 1);
        }
        return cw0.a(a, bm0, (Object) str);
    }
}
