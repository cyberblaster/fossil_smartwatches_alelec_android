package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.kg;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sf implements kg.a {
    @DexIgnore
    public v8<b> a;
    @DexIgnore
    public /* final */ ArrayList<b> b;
    @DexIgnore
    public /* final */ ArrayList<b> c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public Runnable e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ kg g;
    @DexIgnore
    public int h;

    @DexIgnore
    public interface a {
        @DexIgnore
        RecyclerView.ViewHolder a(int i);

        @DexIgnore
        void a(int i, int i2);

        @DexIgnore
        void a(int i, int i2, Object obj);

        @DexIgnore
        void a(b bVar);

        @DexIgnore
        void b(int i, int i2);

        @DexIgnore
        void b(b bVar);

        @DexIgnore
        void c(int i, int i2);

        @DexIgnore
        void d(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Object c;
        @DexIgnore
        public int d;

        @DexIgnore
        public b(int i, int i2, int i3, Object obj) {
            this.a = i;
            this.b = i2;
            this.d = i3;
            this.c = obj;
        }

        @DexIgnore
        public String a() {
            int i = this.a;
            if (i == 1) {
                return "add";
            }
            if (i == 2) {
                return "rm";
            }
            if (i != 4) {
                return i != 8 ? "??" : "mv";
            }
            return "up";
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            int i = this.a;
            if (i != bVar.a) {
                return false;
            }
            if (i == 8 && Math.abs(this.d - this.b) == 1 && this.d == bVar.b && this.b == bVar.d) {
                return true;
            }
            if (this.d != bVar.d || this.b != bVar.b) {
                return false;
            }
            Object obj2 = this.c;
            if (obj2 != null) {
                if (!obj2.equals(bVar.c)) {
                    return false;
                }
            } else if (bVar.c != null) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.a * 31) + this.b) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.b + "c:" + this.d + ",p:" + this.c + "]";
        }
    }

    @DexIgnore
    public sf(a aVar) {
        this(aVar, false);
    }

    @DexIgnore
    public void a() {
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            this.d.b(this.c.get(i));
        }
        a((List<b>) this.c);
        this.h = 0;
    }

    @DexIgnore
    public final boolean b(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            int i3 = bVar.a;
            if (i3 == 8) {
                if (a(bVar.d, i2 + 1) == i) {
                    return true;
                }
            } else if (i3 == 1) {
                int i4 = bVar.b;
                int i5 = bVar.d + i4;
                while (i4 < i5) {
                    if (a(i4, i2 + 1) == i) {
                        return true;
                    }
                    i4++;
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    @DexIgnore
    public final void c(b bVar) {
        g(bVar);
    }

    @DexIgnore
    public final void d(b bVar) {
        char c2;
        boolean z;
        boolean z2;
        int i = bVar.b;
        int i2 = bVar.d + i;
        int i3 = 0;
        char c3 = 65535;
        int i4 = i;
        while (i4 < i2) {
            if (this.d.a(i4) != null || b(i4)) {
                if (c3 == 0) {
                    f(a(2, i, i3, (Object) null));
                    z2 = true;
                } else {
                    z2 = false;
                }
                c2 = 1;
            } else {
                if (c3 == 1) {
                    g(a(2, i, i3, (Object) null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 0;
            }
            if (z) {
                i4 -= i3;
                i2 -= i3;
                i3 = 1;
            } else {
                i3++;
            }
            i4++;
            c3 = c2;
        }
        if (i3 != bVar.d) {
            a(bVar);
            bVar = a(2, i, i3, (Object) null);
        }
        if (c3 == 0) {
            f(bVar);
        } else {
            g(bVar);
        }
    }

    @DexIgnore
    public void e() {
        this.g.b(this.b);
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            b bVar = this.b.get(i);
            int i2 = bVar.a;
            if (i2 == 1) {
                b(bVar);
            } else if (i2 == 2) {
                d(bVar);
            } else if (i2 == 4) {
                e(bVar);
            } else if (i2 == 8) {
                c(bVar);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public void f() {
        a((List<b>) this.b);
        a((List<b>) this.c);
        this.h = 0;
    }

    @DexIgnore
    public final void g(b bVar) {
        this.c.add(bVar);
        int i = bVar.a;
        if (i == 1) {
            this.d.c(bVar.b, bVar.d);
        } else if (i == 2) {
            this.d.b(bVar.b, bVar.d);
        } else if (i == 4) {
            this.d.a(bVar.b, bVar.d, bVar.c);
        } else if (i == 8) {
            this.d.a(bVar.b, bVar.d);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + bVar);
        }
    }

    @DexIgnore
    public sf(a aVar, boolean z) {
        this.a = new w8(30);
        this.b = new ArrayList<>();
        this.c = new ArrayList<>();
        this.h = 0;
        this.d = aVar;
        this.f = z;
        this.g = new kg(this);
    }

    @DexIgnore
    public boolean c() {
        return this.b.size() > 0;
    }

    @DexIgnore
    public int c(int i) {
        return a(i, 0);
    }

    @DexIgnore
    public boolean c(int i, int i2) {
        if (i2 < 1) {
            return false;
        }
        this.b.add(a(2, i, i2, (Object) null));
        this.h |= 2;
        if (this.b.size() == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void f(b bVar) {
        int i;
        int i2 = bVar.a;
        if (i2 == 1 || i2 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int d2 = d(bVar.b, i2);
        int i3 = bVar.b;
        int i4 = bVar.a;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + bVar);
        }
        int i5 = d2;
        int i6 = i3;
        int i7 = 1;
        for (int i8 = 1; i8 < bVar.d; i8++) {
            int d3 = d(bVar.b + (i * i8), bVar.a);
            int i9 = bVar.a;
            if (i9 == 2 ? d3 == i5 : i9 == 4 && d3 == i5 + 1) {
                i7++;
            } else {
                b a2 = a(bVar.a, i5, i7, bVar.c);
                a(a2, i6);
                a(a2);
                if (bVar.a == 4) {
                    i6 += i7;
                }
                i5 = d3;
                i7 = 1;
            }
        }
        Object obj = bVar.c;
        a(bVar);
        if (i7 > 0) {
            b a3 = a(bVar.a, i5, i7, obj);
            a(a3, i6);
            a(a3);
        }
    }

    @DexIgnore
    public void a(b bVar, int i) {
        this.d.a(bVar);
        int i2 = bVar.a;
        if (i2 == 2) {
            this.d.d(i, bVar.d);
        } else if (i2 == 4) {
            this.d.a(i, bVar.d, bVar.c);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    @DexIgnore
    public final void b(b bVar) {
        g(bVar);
    }

    @DexIgnore
    public boolean b(int i, int i2) {
        if (i2 < 1) {
            return false;
        }
        this.b.add(a(1, i, i2, (Object) null));
        this.h |= 1;
        if (this.b.size() == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int a(int i, int i2) {
        int size = this.c.size();
        while (i2 < size) {
            b bVar = this.c.get(i2);
            int i3 = bVar.a;
            if (i3 == 8) {
                int i4 = bVar.b;
                if (i4 == i) {
                    i = bVar.d;
                } else {
                    if (i4 < i) {
                        i--;
                    }
                    if (bVar.d <= i) {
                        i++;
                    }
                }
            } else {
                int i5 = bVar.b;
                if (i5 > i) {
                    continue;
                } else if (i3 == 2) {
                    int i6 = bVar.d;
                    if (i < i5 + i6) {
                        return -1;
                    }
                    i -= i6;
                } else if (i3 == 1) {
                    i += bVar.d;
                }
            }
            i2++;
        }
        return i;
    }

    @DexIgnore
    public void b() {
        a();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            b bVar = this.b.get(i);
            int i2 = bVar.a;
            if (i2 == 1) {
                this.d.b(bVar);
                this.d.c(bVar.b, bVar.d);
            } else if (i2 == 2) {
                this.d.b(bVar);
                this.d.d(bVar.b, bVar.d);
            } else if (i2 == 4) {
                this.d.b(bVar);
                this.d.a(bVar.b, bVar.d, bVar.c);
            } else if (i2 == 8) {
                this.d.b(bVar);
                this.d.a(bVar.b, bVar.d);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        a((List<b>) this.b);
        this.h = 0;
    }

    @DexIgnore
    public final void e(b bVar) {
        int i = bVar.b;
        int i2 = bVar.d + i;
        int i3 = i;
        int i4 = 0;
        char c2 = 65535;
        while (i < i2) {
            if (this.d.a(i) != null || b(i)) {
                if (c2 == 0) {
                    f(a(4, i3, i4, bVar.c));
                    i3 = i;
                    i4 = 0;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    g(a(4, i3, i4, bVar.c));
                    i3 = i;
                    i4 = 0;
                }
                c2 = 0;
            }
            i4++;
            i++;
        }
        if (i4 != bVar.d) {
            Object obj = bVar.c;
            a(bVar);
            bVar = a(4, i3, i4, obj);
        }
        if (c2 == 0) {
            f(bVar);
        } else {
            g(bVar);
        }
    }

    @DexIgnore
    public final int d(int i, int i2) {
        for (int size = this.c.size() - 1; size >= 0; size--) {
            b bVar = this.c.get(size);
            int i3 = bVar.a;
            if (i3 == 8) {
                int i4 = bVar.b;
                int i5 = bVar.d;
                if (i4 >= i5) {
                    int i6 = i5;
                    i5 = i4;
                    i4 = i6;
                }
                if (i < i4 || i > i5) {
                    int i7 = bVar.b;
                    if (i < i7) {
                        if (i2 == 1) {
                            bVar.b = i7 + 1;
                            bVar.d++;
                        } else if (i2 == 2) {
                            bVar.b = i7 - 1;
                            bVar.d--;
                        }
                    }
                } else {
                    int i8 = bVar.b;
                    if (i4 == i8) {
                        if (i2 == 1) {
                            bVar.d++;
                        } else if (i2 == 2) {
                            bVar.d--;
                        }
                        i++;
                    } else {
                        if (i2 == 1) {
                            bVar.b = i8 + 1;
                        } else if (i2 == 2) {
                            bVar.b = i8 - 1;
                        }
                        i--;
                    }
                }
            } else {
                int i9 = bVar.b;
                if (i9 <= i) {
                    if (i3 == 1) {
                        i -= bVar.d;
                    } else if (i3 == 2) {
                        i += bVar.d;
                    }
                } else if (i2 == 1) {
                    bVar.b = i9 + 1;
                } else if (i2 == 2) {
                    bVar.b = i9 - 1;
                }
            }
        }
        for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
            b bVar2 = this.c.get(size2);
            if (bVar2.a == 8) {
                int i10 = bVar2.d;
                if (i10 == bVar2.b || i10 < 0) {
                    this.c.remove(size2);
                    a(bVar2);
                }
            } else if (bVar2.d <= 0) {
                this.c.remove(size2);
                a(bVar2);
            }
        }
        return i;
    }

    @DexIgnore
    public boolean a(int i, int i2, Object obj) {
        if (i2 < 1) {
            return false;
        }
        this.b.add(a(4, i, i2, obj));
        this.h |= 4;
        if (this.b.size() == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean a(int i, int i2, int i3) {
        if (i == i2) {
            return false;
        }
        if (i3 == 1) {
            this.b.add(a(8, i, i2, (Object) null));
            this.h |= 8;
            if (this.b.size() == 1) {
                return true;
            }
            return false;
        }
        throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
    }

    @DexIgnore
    public int a(int i) {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.b.get(i2);
            int i3 = bVar.a;
            if (i3 != 1) {
                if (i3 == 2) {
                    int i4 = bVar.b;
                    if (i4 <= i) {
                        int i5 = bVar.d;
                        if (i4 + i5 > i) {
                            return -1;
                        }
                        i -= i5;
                    } else {
                        continue;
                    }
                } else if (i3 == 8) {
                    int i6 = bVar.b;
                    if (i6 == i) {
                        i = bVar.d;
                    } else {
                        if (i6 < i) {
                            i--;
                        }
                        if (bVar.d <= i) {
                            i++;
                        }
                    }
                }
            } else if (bVar.b <= i) {
                i += bVar.d;
            }
        }
        return i;
    }

    @DexIgnore
    public b a(int i, int i2, int i3, Object obj) {
        b a2 = this.a.a();
        if (a2 == null) {
            return new b(i, i2, i3, obj);
        }
        a2.a = i;
        a2.b = i2;
        a2.d = i3;
        a2.c = obj;
        return a2;
    }

    @DexIgnore
    public void a(b bVar) {
        if (!this.f) {
            bVar.c = null;
            this.a.a(bVar);
        }
    }

    @DexIgnore
    public boolean d(int i) {
        return (i & this.h) != 0;
    }

    @DexIgnore
    public boolean d() {
        return !this.c.isEmpty() && !this.b.isEmpty();
    }

    @DexIgnore
    public void a(List<b> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(list.get(i));
        }
        list.clear();
    }
}
