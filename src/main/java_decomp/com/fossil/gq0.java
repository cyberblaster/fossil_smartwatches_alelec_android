package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.r40;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq0 extends x51 {
    @DexIgnore
    public r40 T;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public gq0(ue1 ue1, q41 q41, short s, String str) {
        super(r1, q41, r3, s, r5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
        eh1 eh1 = eh1.READ_DEVICE_INFO_FILE;
        HashMap a = he6.a(new lc6[]{qc6.a(io0.SKIP_ERASE, true), qc6.a(io0.NUMBER_OF_FILE_REQUIRED, 1), qc6.a(io0.ERASE_CACHE_FILE_BEFORE_GET, true)});
        ue1 ue12 = ue1;
        this.T = new r40(ue1.d(), ue12.t, "", "", "", (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262112);
    }

    @DexIgnore
    public void a(ArrayList<ie1> arrayList) {
        a(this.v);
    }

    @DexIgnore
    public void c(ie1 ie1) {
        sk1 sk1;
        super.c(ie1);
        try {
            this.T = r40.a((r40) nr0.d.a(ie1.e), this.T.getName(), this.T.getMacAddress(), (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262140);
            sk1 = sk1.SUCCESS;
        } catch (sw0 e) {
            qs0.h.a(e);
            sk1 = sk1.UNSUPPORTED_FORMAT;
        }
        this.v = km1.a(this.v, (eh1) null, sk1, (bn0) null, 5);
    }

    @DexIgnore
    public Object d() {
        return this.T;
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(super.k(), bm0.DEVICE_INFO, (Object) this.T.a());
    }
}
