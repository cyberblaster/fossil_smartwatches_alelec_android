package com.fossil;

import com.fossil.uh6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ci6 extends bi6 {
    @DexIgnore
    public static final int a(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    @DexIgnore
    public static final long a(long j, long j2) {
        return j < j2 ? j2 : j;
    }

    @DexIgnore
    public static final uh6 a(uh6 uh6, int i) {
        wg6.b(uh6, "$this$step");
        bi6.a(i > 0, Integer.valueOf(i));
        uh6.a aVar = uh6.d;
        int a = uh6.a();
        int b = uh6.b();
        if (uh6.c() <= 0) {
            i = -i;
        }
        return aVar.a(a, b, i);
    }

    @DexIgnore
    public static final int b(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    @DexIgnore
    public static final long b(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    @DexIgnore
    public static final uh6 c(int i, int i2) {
        return uh6.d.a(i, i2, -1);
    }

    @DexIgnore
    public static final wh6 d(int i, int i2) {
        if (i2 <= Integer.MIN_VALUE) {
            return wh6.f.a();
        }
        return new wh6(i, i2 - 1);
    }

    @DexIgnore
    public static final zh6 a(long j, int i) {
        return new zh6(j, ((long) i) - 1);
    }

    @DexIgnore
    public static final int a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
        } else if (i < i2) {
            return i2;
        } else {
            return i > i3 ? i3 : i;
        }
    }
}
