package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l71 extends ok0 {
    @DexIgnore
    public int j; // = 23;
    @DexIgnore
    public /* final */ int k;

    @DexIgnore
    public l71(int i, at0 at0) {
        super(hm0.REQUEST_MTU, at0);
        this.k = i;
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.a(this.k);
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return p51 instanceof gb1;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.c;
    }

    @DexIgnore
    public void c(p51 p51) {
        this.j = ((gb1) p51).b;
    }
}
