package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt6 implements zt6 {
    @DexIgnore
    public /* final */ lt6 a;
    @DexIgnore
    public /* final */ Inflater b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public rt6(lt6 lt6, Inflater inflater) {
        if (lt6 == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater != null) {
            this.a = lt6;
            this.b = inflater;
        } else {
            throw new IllegalArgumentException("inflater == null");
        }
    }

    @DexIgnore
    public long b(jt6 jt6, long j) throws IOException {
        vt6 b2;
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.d) {
            throw new IllegalStateException("closed");
        } else if (i == 0) {
            return 0;
        } else {
            while (true) {
                boolean c2 = c();
                try {
                    b2 = jt6.b(1);
                    int inflate = this.b.inflate(b2.a, b2.c, (int) Math.min(j, (long) (8192 - b2.c)));
                    if (inflate > 0) {
                        b2.c += inflate;
                        long j2 = (long) inflate;
                        jt6.b += j2;
                        return j2;
                    } else if (this.b.finished()) {
                        break;
                    } else if (this.b.needsDictionary()) {
                        break;
                    } else if (c2) {
                        throw new EOFException("source exhausted prematurely");
                    }
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            }
            d();
            if (b2.b != b2.c) {
                return -1;
            }
            jt6.a = b2.b();
            wt6.a(b2);
            return -1;
        }
    }

    @DexIgnore
    public final boolean c() throws IOException {
        if (!this.b.needsInput()) {
            return false;
        }
        d();
        if (this.b.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.a.f()) {
            return true;
        } else {
            vt6 vt6 = this.a.a().a;
            int i = vt6.c;
            int i2 = vt6.b;
            this.c = i - i2;
            this.b.setInput(vt6.a, i2, this.c);
            return false;
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.d) {
            this.b.end();
            this.d = true;
            this.a.close();
        }
    }

    @DexIgnore
    public final void d() throws IOException {
        int i = this.c;
        if (i != 0) {
            int remaining = i - this.b.getRemaining();
            this.c -= remaining;
            this.a.skip((long) remaining);
        }
    }

    @DexIgnore
    public au6 b() {
        return this.a.b();
    }
}
