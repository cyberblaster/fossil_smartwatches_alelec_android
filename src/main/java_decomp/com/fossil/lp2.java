package com.fossil;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp2 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int a;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> b;
    @DexIgnore
    public /* final */ /* synthetic */ jp2 c;

    @DexIgnore
    public lp2(jp2 jp2) {
        this.c = jp2;
        this.a = this.c.b.size();
    }

    @DexIgnore
    public final boolean hasNext() {
        int i = this.a;
        return (i > 0 && i <= this.c.b.size()) || zza().hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        if (zza().hasNext()) {
            return (Map.Entry) zza().next();
        }
        List b2 = this.c.b;
        int i = this.a - 1;
        this.a = i;
        return (Map.Entry) b2.get(i);
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> zza() {
        if (this.b == null) {
            this.b = this.c.f.entrySet().iterator();
        }
        return this.b;
    }

    @DexIgnore
    public /* synthetic */ lp2(jp2 jp2, ip2 ip2) {
        this(jp2);
    }
}
