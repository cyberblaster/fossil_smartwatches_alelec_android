package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py2 extends jh2 implements oy2 {
    @DexIgnore
    public py2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final tx2 a(x52 x52, GoogleMapOptions googleMapOptions) throws RemoteException {
        tx2 tx2;
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        lh2.a(zza, (Parcelable) googleMapOptions);
        Parcel a = a(3, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            tx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
            if (queryLocalInterface instanceof tx2) {
                tx2 = queryLocalInterface;
            } else {
                tx2 = new sy2(readStrongBinder);
            }
        }
        a.recycle();
        return tx2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final vx2 b(x52 x52) throws RemoteException {
        vx2 vx2;
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        Parcel a = a(8, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            vx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaFragmentDelegate");
            if (queryLocalInterface instanceof vx2) {
                vx2 = queryLocalInterface;
            } else {
                vx2 = new hy2(readStrongBinder);
            }
        }
        a.recycle();
        return vx2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final sx2 d(x52 x52) throws RemoteException {
        sx2 sx2;
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        Parcel a = a(2, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            sx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapFragmentDelegate");
            if (queryLocalInterface instanceof sx2) {
                sx2 = queryLocalInterface;
            } else {
                sx2 = new ry2(readStrongBinder);
            }
        }
        a.recycle();
        return sx2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final qx2 zze() throws RemoteException {
        qx2 qx2;
        Parcel a = a(4, zza());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            qx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
            if (queryLocalInterface instanceof qx2) {
                qx2 = queryLocalInterface;
            } else {
                qx2 = new dy2(readStrongBinder);
            }
        }
        a.recycle();
        return qx2;
    }

    @DexIgnore
    public final mh2 zzf() throws RemoteException {
        Parcel a = a(5, zza());
        mh2 a2 = nh2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void a(x52 x52, int i) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        zza.writeInt(i);
        b(6, zza);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final wx2 a(x52 x52, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException {
        wx2 wx2;
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        lh2.a(zza, (Parcelable) streetViewPanoramaOptions);
        Parcel a = a(7, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            wx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
            if (queryLocalInterface instanceof wx2) {
                wx2 = queryLocalInterface;
            } else {
                wx2 = new iy2(readStrongBinder);
            }
        }
        a.recycle();
        return wx2;
    }
}
