package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l52 extends r32 {
    @DexIgnore
    public int a;

    @DexIgnore
    public l52(byte[] bArr) {
        w12.a(bArr.length == 25);
        this.a = Arrays.hashCode(bArr);
    }

    @DexIgnore
    public static byte[] zza(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        x52 zzb;
        if (obj != null && (obj instanceof q32)) {
            try {
                q32 q32 = (q32) obj;
                if (q32.zzc() != hashCode() || (zzb = q32.zzb()) == null) {
                    return false;
                }
                return Arrays.equals(q(), (byte[]) z52.e(zzb));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a;
    }

    @DexIgnore
    public abstract byte[] q();

    @DexIgnore
    public final x52 zzb() {
        return z52.a(q());
    }

    @DexIgnore
    public final int zzc() {
        return hashCode();
    }
}
