package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t22 extends ta2 implements u22 {
    @DexIgnore
    public t22(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    @DexIgnore
    public final void a(s22 s22) throws RemoteException {
        Parcel q = q();
        ua2.a(q, (IInterface) s22);
        c(1, q);
    }
}
