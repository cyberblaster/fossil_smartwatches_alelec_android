package com.fossil;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mg2 extends fx2 {
    @DexIgnore
    public /* final */ uw1<zv2> a;

    @DexIgnore
    public mg2(uw1<zv2> uw1) {
        this.a = uw1;
    }

    @DexIgnore
    public final synchronized void onLocationChanged(Location location) {
        this.a.a(new ng2(this, location));
    }

    @DexIgnore
    public final synchronized void q() {
        this.a.a();
    }
}
