package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ab extends ya {
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public LayoutInflater o;

    @DexIgnore
    @Deprecated
    public ab(Context context, int i2, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.j = i2;
        this.i = i2;
        this.o = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    @DexIgnore
    public View a(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.o.inflate(this.j, viewGroup, false);
    }

    @DexIgnore
    public View b(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.o.inflate(this.i, viewGroup, false);
    }
}
