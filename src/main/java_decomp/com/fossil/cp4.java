package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp4<T> extends ap4<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public cp4(T t, boolean z) {
        super((qg6) null);
        this.a = t;
        this.b = z;
    }

    @DexIgnore
    public final T a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cp4)) {
            return false;
        }
        cp4 cp4 = (cp4) obj;
        return wg6.a((Object) this.a, (Object) cp4.a) && this.b == cp4.b;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.a;
        int hashCode = (t != null ? t.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + this.a + ", isFromCache=" + this.b + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ cp4(Object obj, boolean z, int i, qg6 qg6) {
        this(obj, (i & 2) != 0 ? false : z);
    }
}
