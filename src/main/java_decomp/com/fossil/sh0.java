package com.fossil;

import com.fossil.fitness.FitnessData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sh0 extends xg6 implements hg6<if1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ym0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sh0(ym0 ym0) {
        super(1);
        this.a = ym0;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        ym0 ym0 = this.a;
        FitnessData[] fitnessDataArr = ((hx0) obj).U;
        if (fitnessDataArr == null) {
            fitnessDataArr = new FitnessData[0];
        }
        ym0.C = fitnessDataArr;
        if (fm0.f.b(this.a.x.a())) {
            ym0 ym02 = this.a;
            ym02.a(km1.a(ym02.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else {
            ym0 ym03 = this.a;
            if (ym03.F > 0) {
                if1.a((if1) ym03, (if1) new r91(ym03.w, ym03.x, (HashMap) null, ym03.z, 4), (hg6) new nm1(ym03), (hg6) new je0(ym03), (ig6) new ag0(ym03), (hg6) null, (hg6) null, 48, (Object) null);
            } else {
                ym03.a(km1.a(ym03.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
            }
        }
        return cd6.a;
    }
}
