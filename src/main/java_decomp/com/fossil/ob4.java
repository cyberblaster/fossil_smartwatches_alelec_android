package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ob4 extends nb4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362026, 1);
        A.put(2131362869, 2);
        A.put(2131362881, 3);
        A.put(2131363206, 4);
        A.put(2131363182, 5);
        A.put(2131363191, 6);
        A.put(2131363181, 7);
        A.put(2131363180, 8);
    }
    */

    @DexIgnore
    public ob4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 9, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ob4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[0], objArr[2], objArr[3], objArr[8], objArr[7], objArr[5], objArr[6], objArr[4]);
        this.y = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
