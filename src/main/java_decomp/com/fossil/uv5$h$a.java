package com.fossil;

import com.fossil.m24;
import com.fossil.os4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv5$h$a implements m24.e<os4.e, os4.b> {
    @DexIgnore
    public /* final */ /* synthetic */ FindDevicePresenter.h a;

    @DexIgnore
    public uv5$h$a(FindDevicePresenter.h hVar) {
        this.a = hVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(os4.e eVar) {
        wg6.b(eVar, "responseValue");
        int a2 = eVar.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "getRssi onSuccess - rssi: " + a2);
        this.a.a.a(a2);
        this.a.a.b(a2);
    }

    @DexIgnore
    public void a(os4.b bVar) {
        wg6.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "getRssi onError");
        this.a.a.a(-9999);
        this.a.a.b(-9999);
    }
}
