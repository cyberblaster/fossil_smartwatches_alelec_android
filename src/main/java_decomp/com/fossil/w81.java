package com.fossil;

import android.database.Cursor;
import android.os.CancellationSignal;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w81 {
    @DexIgnore
    public /* final */ oh a;
    @DexIgnore
    public /* final */ hh<ie1> b;
    @DexIgnore
    public /* final */ vh c;
    @DexIgnore
    public /* final */ vh d;
    @DexIgnore
    public /* final */ vh e;

    @DexIgnore
    public w81(oh ohVar) {
        this.a = ohVar;
        this.b = new l11(this, ohVar);
        this.c = new g31(this, ohVar);
        this.d = new c51(this, ohVar);
        this.e = new z61(this, ohVar);
    }

    @DexIgnore
    public long a(ie1 ie1) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(ie1);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public int a(String str) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.e.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int s = acquire.s();
            this.a.setTransactionSuccessful();
            return s;
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    public List<ie1> a() {
        rh b2 = rh.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false, (CancellationSignal) null);
        try {
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, "deviceMacAddress");
            int b5 = ai.b(a2, "fileType");
            int b6 = ai.b(a2, "fileIndex");
            int b7 = ai.b(a2, "rawData");
            int b8 = ai.b(a2, "fileLength");
            int b9 = ai.b(a2, "fileCrc");
            int b10 = ai.b(a2, "createdTimeStamp");
            int b11 = ai.b(a2, "isCompleted");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                ie1 ie1 = new ie1(a2.getString(b4), (byte) a2.getShort(b5), (byte) a2.getShort(b6), a2.getBlob(b7), a2.getLong(b8), a2.getLong(b9), a2.getLong(b10), a2.getInt(b11) != 0);
                ie1.a = a2.getInt(b3);
                arrayList.add(ie1);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
