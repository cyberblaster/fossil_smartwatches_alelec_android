package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ud4 extends td4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131361905, 1);
        z.put(2131362442, 2);
        z.put(2131362926, 3);
        z.put(2131362093, 4);
        z.put(2131362377, 5);
        z.put(2131362653, 6);
        z.put(2131362314, 7);
        z.put(2131363032, 8);
        z.put(2131362894, 9);
    }
    */

    @DexIgnore
    public ud4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 10, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ud4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[4], objArr[7], objArr[5], objArr[2], objArr[6], objArr[0], objArr[9], objArr[3], objArr[8]);
        this.x = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
