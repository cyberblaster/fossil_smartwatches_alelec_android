package com.fossil;

import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allSearchedWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
public final class r85$b$a extends sf6 implements ig6<il6, xe6<? super List<? extends WatchApp>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppSearchPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r85$b$a(WatchAppSearchPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        r85$b$a r85_b_a = new r85$b$a(this.this$0, xe6);
        r85_b_a.p$ = (il6) obj;
        return r85_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((r85$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            WatchAppRepository e = this.this$0.this$0.l;
            List<String> z = this.this$0.this$0.m.z();
            wg6.a((Object) z, "sharedPreferencesManager.watchAppSearchedIdsRecent");
            return e.getWatchAppByIds(z);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
