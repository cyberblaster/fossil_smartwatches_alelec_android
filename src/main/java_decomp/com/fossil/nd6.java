package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nd6 extends md6 {
    @DexIgnore
    public static final <T> boolean a(T[] tArr, T t) {
        wg6.b(tArr, "$this$contains");
        return b(tArr, t) >= 0;
    }

    @DexIgnore
    public static final boolean b(byte[] bArr, byte b) {
        wg6.b(bArr, "$this$contains");
        return c(bArr, b) >= 0;
    }

    @DexIgnore
    public static final int c(byte[] bArr, byte b) {
        wg6.b(bArr, "$this$indexOf");
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            if (b == bArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> List<T> d(T[] tArr) {
        wg6.b(tArr, "$this$distinct");
        return yd6.m(i(tArr));
    }

    @DexIgnore
    public static final <T> List<T> e(T[] tArr) {
        wg6.b(tArr, "$this$filterNotNull");
        ArrayList arrayList = new ArrayList();
        a(tArr, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T> T f(T[] tArr) {
        wg6.b(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public static final <T> List<T> g(T[] tArr) {
        wg6.b(tArr, "$this$toList");
        int length = tArr.length;
        if (length == 0) {
            return qd6.a();
        }
        if (length != 1) {
            return h(tArr);
        }
        return pd6.a(tArr[0]);
    }

    @DexIgnore
    public static final <T> List<T> h(T[] tArr) {
        wg6.b(tArr, "$this$toMutableList");
        return new ArrayList(qd6.b(tArr));
    }

    @DexIgnore
    public static final <T> Set<T> i(T[] tArr) {
        wg6.b(tArr, "$this$toMutableSet");
        LinkedHashSet linkedHashSet = new LinkedHashSet(he6.a(tArr.length));
        for (T add : tArr) {
            linkedHashSet.add(add);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static final boolean a(short[] sArr, short s) {
        wg6.b(sArr, "$this$contains");
        return b(sArr, s) >= 0;
    }

    @DexIgnore
    public static final <T> int b(T[] tArr, T t) {
        wg6.b(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (wg6.a((Object) t, (Object) tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static final boolean a(int[] iArr, int i) {
        wg6.b(iArr, "$this$contains");
        return b(iArr, i) >= 0;
    }

    @DexIgnore
    public static final byte[] c(byte[] bArr) {
        wg6.b(bArr, "$this$reversedArray");
        int i = 0;
        if (bArr.length == 0) {
            return bArr;
        }
        byte[] bArr2 = new byte[bArr.length];
        int b = b(bArr);
        if (b >= 0) {
            while (true) {
                bArr2[b - i] = bArr[i];
                if (i == b) {
                    break;
                }
                i++;
            }
        }
        return bArr2;
    }

    @DexIgnore
    public static final boolean a(long[] jArr, long j) {
        wg6.b(jArr, "$this$contains");
        return b(jArr, j) >= 0;
    }

    @DexIgnore
    public static final char a(char[] cArr) {
        wg6.b(cArr, "$this$single");
        int length = cArr.length;
        if (length == 0) {
            throw new NoSuchElementException("Array is empty.");
        } else if (length == 1) {
            return cArr[0];
        } else {
            throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    @DexIgnore
    public static final int b(short[] sArr, short s) {
        wg6.b(sArr, "$this$indexOf");
        int length = sArr.length;
        for (int i = 0; i < length; i++) {
            if (s == sArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> List<T> c(T[] tArr, Comparator<? super T> comparator) {
        wg6.b(tArr, "$this$sortedWith");
        wg6.b(comparator, "comparator");
        return md6.b(b(tArr, comparator));
    }

    @DexIgnore
    public static final int b(int[] iArr, int i) {
        wg6.b(iArr, "$this$indexOf");
        int length = iArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (i == iArr[i2]) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final List<Integer> c(int[] iArr) {
        wg6.b(iArr, "$this$toMutableList");
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(Integer.valueOf(valueOf));
        }
        return arrayList;
    }

    @DexIgnore
    public static final <C extends Collection<? super T>, T> C a(T[] tArr, C c) {
        wg6.b(tArr, "$this$filterNotNullTo");
        wg6.b(c, "destination");
        for (T t : tArr) {
            if (t != null) {
                c.add(t);
            }
        }
        return c;
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T[] a(T[] tArr) {
        wg6.b(tArr, "$this$sortedArray");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] copyOf = Arrays.copyOf(tArr, tArr.length);
        wg6.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        T[] tArr2 = (Comparable[]) copyOf;
        if (tArr2 != null) {
            md6.c(tArr2);
            return tArr2;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
    }

    @DexIgnore
    public static final int b(long[] jArr, long j) {
        wg6.b(jArr, "$this$indexOf");
        int length = jArr.length;
        for (int i = 0; i < length; i++) {
            if (j == jArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final wh6 a(byte[] bArr) {
        wg6.b(bArr, "$this$indices");
        return new wh6(0, b(bArr));
    }

    @DexIgnore
    public static final <T> T[] b(T[] tArr, Comparator<? super T> comparator) {
        wg6.b(tArr, "$this$sortedArrayWith");
        wg6.b(comparator, "comparator");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] copyOf = Arrays.copyOf(tArr, tArr.length);
        wg6.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        md6.a(copyOf, comparator);
        return copyOf;
    }

    @DexIgnore
    public static final int b(byte[] bArr) {
        wg6.b(bArr, "$this$lastIndex");
        return bArr.length - 1;
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C b(T[] tArr, C c) {
        wg6.b(tArr, "$this$toCollection");
        wg6.b(c, "destination");
        for (T add : tArr) {
            c.add(add);
        }
        return c;
    }

    @DexIgnore
    public static final List<Integer> b(int[] iArr) {
        wg6.b(iArr, "$this$toList");
        int length = iArr.length;
        if (length == 0) {
            return qd6.a();
        }
        if (length != 1) {
            return c(iArr);
        }
        return pd6.a(Integer.valueOf(iArr[0]));
    }
}
