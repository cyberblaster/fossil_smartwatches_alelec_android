package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vx implements zr<kr, Bitmap> {
    @DexIgnore
    public /* final */ au a;

    @DexIgnore
    public vx(au auVar) {
        this.a = auVar;
    }

    @DexIgnore
    public boolean a(kr krVar, xr xrVar) {
        return true;
    }

    @DexIgnore
    public rt<Bitmap> a(kr krVar, int i, int i2, xr xrVar) {
        return hw.a(krVar.a(), this.a);
    }
}
