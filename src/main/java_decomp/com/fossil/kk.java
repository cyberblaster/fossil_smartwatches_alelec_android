package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kk {
    @DexIgnore
    public static Method b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Field d;
    @DexIgnore
    public static boolean e;
    @DexIgnore
    public float[] a;

    @DexIgnore
    public void a(View view, float f) {
        Float f2 = (Float) view.getTag(kj.save_non_transition_alpha);
        if (f2 != null) {
            view.setAlpha(f2.floatValue() * f);
        } else {
            view.setAlpha(f);
        }
    }

    @DexIgnore
    public float b(View view) {
        Float f = (Float) view.getTag(kj.save_non_transition_alpha);
        if (f != null) {
            return view.getAlpha() / f.floatValue();
        }
        return view.getAlpha();
    }

    @DexIgnore
    public void c(View view) {
        if (view.getTag(kj.save_non_transition_alpha) == null) {
            view.setTag(kj.save_non_transition_alpha, Float.valueOf(view.getAlpha()));
        }
    }

    @DexIgnore
    public void c(View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            c(view2, matrix);
            matrix.postTranslate((float) view2.getScrollX(), (float) view2.getScrollY());
        }
        matrix.postTranslate((float) (-view.getLeft()), (float) (-view.getTop()));
        Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            Matrix matrix3 = new Matrix();
            if (matrix2.invert(matrix3)) {
                matrix.postConcat(matrix3);
            }
        }
    }

    @DexIgnore
    public void a(View view) {
        if (view.getVisibility() == 0) {
            view.setTag(kj.save_non_transition_alpha, (Object) null);
        }
    }

    @DexIgnore
    public void b(View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            b(view2, matrix);
            matrix.preTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        }
        matrix.preTranslate((float) view.getLeft(), (float) view.getTop());
        Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            matrix.preConcat(matrix2);
        }
    }

    @DexIgnore
    public void a(View view, Matrix matrix) {
        if (matrix == null || matrix.isIdentity()) {
            view.setPivotX((float) (view.getWidth() / 2));
            view.setPivotY((float) (view.getHeight() / 2));
            view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setRotation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float[] fArr = this.a;
        if (fArr == null) {
            fArr = new float[9];
            this.a = fArr;
        }
        matrix.getValues(fArr);
        float f = fArr[3];
        float sqrt = ((float) Math.sqrt((double) (1.0f - (f * f)))) * ((float) (fArr[0] < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? -1 : 1));
        float degrees = (float) Math.toDegrees(Math.atan2((double) f, (double) sqrt));
        float f2 = fArr[0] / sqrt;
        float f3 = fArr[4] / sqrt;
        float f4 = fArr[2];
        float f5 = fArr[5];
        view.setPivotX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setPivotY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationX(f4);
        view.setTranslationY(f5);
        view.setRotation(degrees);
        view.setScaleX(f2);
        view.setScaleY(f3);
    }

    @DexIgnore
    public void a(View view, int i, int i2, int i3, int i4) {
        a();
        Method method = b;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    @DexIgnore
    public void a(View view, int i) {
        if (!e) {
            try {
                d = View.class.getDeclaredField("mViewFlags");
                d.setAccessible(true);
            } catch (NoSuchFieldException unused) {
                Log.i("ViewUtilsBase", "fetchViewFlagsField: ");
            }
            e = true;
        }
        Field field = d;
        if (field != null) {
            try {
                d.setInt(view, i | (field.getInt(view) & -13));
            } catch (IllegalAccessException unused2) {
            }
        }
    }

    @DexIgnore
    @SuppressLint({"PrivateApi"})
    public final void a() {
        if (!c) {
            Class<View> cls = View.class;
            try {
                b = cls.getDeclaredMethod("setFrame", new Class[]{Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE});
                b.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewUtilsBase", "Failed to retrieve setFrame method", e2);
            }
            c = true;
        }
    }
}
