package com.fossil;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay0 extends f51 {
    @DexIgnore
    public static /* final */ fw0 CREATOR; // = new fw0((qg6) null);
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;

    @DexIgnore
    public ay0(vd0 vd0, w40 w40, int i, int i2) {
        super(vd0, w40);
        this.c = false;
        this.d = 0;
        this.e = i;
        this.f = i2;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.SHIP_HANDS_TO_TWELVE, (Object) Integer.valueOf(this.c ? 1 : 0)), bm0.TRAVEL_TIME_IN_MINUTE, (Object) Integer.valueOf(this.d));
    }

    @DexIgnore
    public List<uj1> b() {
        short s;
        int i;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new tg0());
        if (this.c) {
            arrayList.add(new dw0(new ge1[]{new ge1(qn0.HOUR, dn1.SHORTEST_PATH, ip0.POSITION, ar0.FULL, 0), new ge1(qn0.MINUTE, dn1.SHORTEST_PATH, ip0.POSITION, ar0.FULL, 0)}));
        }
        arrayList.add(new h31(0.5d));
        arrayList.add(new nu0(ts0.ERROR));
        qn0 qn0 = qn0.HOUR;
        dn1 dn1 = dn1.CLOCK_WISE;
        ip0 c2 = c();
        ar0 ar0 = ar0.FULL;
        if (d()) {
            s = (short) (((this.f * 30) / 60) + (this.e * 30));
        } else {
            int i2 = this.d;
            s = (short) (i2 >= 60 ? (i2 / 60) * 30 : i2 * 6);
        }
        ge1 ge1 = new ge1(qn0, dn1, c2, ar0, s);
        qn0 qn02 = qn0.MINUTE;
        dn1 dn12 = dn1.CLOCK_WISE;
        ip0 c3 = c();
        ar0 ar02 = ar0.FULL;
        if (d()) {
            i = this.f;
        } else {
            i = this.d;
            if (i >= 60) {
                i %= 60;
            }
        }
        arrayList.add(new dw0(new ge1[]{ge1, new ge1(qn02, dn12, c3, ar02, (short) (i * 6))}));
        arrayList.add(new h31(8.0d));
        arrayList.add(new rz0());
        return arrayList;
    }

    @DexIgnore
    public final ip0 c() {
        if (d()) {
            return ip0.POSITION;
        }
        if (this.d >= 60) {
            return ip0.POSITION;
        }
        return ip0.DISTANCE;
    }

    @DexIgnore
    public final boolean d() {
        return this.d == 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(ay0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ay0 ay0 = (ay0) obj;
            return this.c == ay0.c && this.d == ay0.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse");
    }

    @DexIgnore
    public int hashCode() {
        return (Boolean.valueOf(this.c).hashCode() * 31) + this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public ay0(vd0 vd0, w40 w40, boolean z, int i) {
        super(vd0, w40);
        this.c = z;
        this.d = i;
    }

    @DexIgnore
    public /* synthetic */ ay0(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt() != 0;
        this.d = parcel.readInt();
    }
}
