package com.fossil;

import com.fossil.wp;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pq extends qq<JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pq(int i, String str, JSONObject jSONObject, wp.b<JSONObject> bVar, wp.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    @DexIgnore
    public wp<JSONObject> parseNetworkResponse(rp rpVar) {
        try {
            return wp.a(new JSONObject(new String(rpVar.b, jq.a(rpVar.c, qq.PROTOCOL_CHARSET))), jq.a(rpVar));
        } catch (UnsupportedEncodingException e) {
            return wp.a(new tp((Throwable) e));
        } catch (JSONException e2) {
            return wp.a(new tp((Throwable) e2));
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public pq(String str, JSONObject jSONObject, wp.b<JSONObject> bVar, wp.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }
}
