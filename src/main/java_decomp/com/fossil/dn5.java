package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActivityChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn5 implements Factory<cn5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public dn5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static dn5 a(Provider<ThemeRepository> provider) {
        return new dn5(provider);
    }

    @DexIgnore
    public static cn5 b(Provider<ThemeRepository> provider) {
        return new CustomizeActivityChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeActivityChartViewModel get() {
        return b(this.a);
    }
}
