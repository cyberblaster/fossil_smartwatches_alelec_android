package com.fossil;

import com.fossil.vl3;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zl3<E> extends vl3<E> implements List<E>, RandomAccess {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pk3<E> {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public E a(int i) {
            return zl3.this.get(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<E> extends vl3.a<E> {
        @DexIgnore
        public b() {
            this(4);
        }

        @DexIgnore
        public b(int i) {
            super(i);
        }

        @DexIgnore
        public b<E> a(E e) {
            super.a(e);
            return this;
        }

        @DexIgnore
        public b<E> a(Iterator<? extends E> it) {
            super.a(it);
            return this;
        }

        @DexIgnore
        public zl3<E> a() {
            return zl3.asImmutableList(this.a, this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<E> extends zl3<E> {
        @DexIgnore
        public /* final */ transient zl3<E> a;

        @DexIgnore
        public c(zl3<E> zl3) {
            this.a = zl3;
        }

        @DexIgnore
        public final int a(int i) {
            return (size() - 1) - i;
        }

        @DexIgnore
        public final int c(int i) {
            return size() - i;
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return this.a.contains(obj);
        }

        @DexIgnore
        public E get(int i) {
            jk3.a(i, size());
            return this.a.get(a(i));
        }

        @DexIgnore
        public int indexOf(Object obj) {
            int lastIndexOf = this.a.lastIndexOf(obj);
            if (lastIndexOf >= 0) {
                return a(lastIndexOf);
            }
            return -1;
        }

        @DexIgnore
        public boolean isPartialView() {
            return this.a.isPartialView();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Iterator iterator() {
            return zl3.super.iterator();
        }

        @DexIgnore
        public int lastIndexOf(Object obj) {
            int indexOf = this.a.indexOf(obj);
            if (indexOf >= 0) {
                return a(indexOf);
            }
            return -1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return zl3.super.listIterator();
        }

        @DexIgnore
        public zl3<E> reverse() {
            return this.a;
        }

        @DexIgnore
        public int size() {
            return this.a.size();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return zl3.super.listIterator(i);
        }

        @DexIgnore
        public zl3<E> subList(int i, int i2) {
            jk3.b(i, i2, size());
            return this.a.subList(c(i2), c(i)).reverse();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public d(Object[] objArr) {
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            return zl3.copyOf((E[]) this.elements);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends zl3<E> {
        @DexIgnore
        public /* final */ transient int length;
        @DexIgnore
        public /* final */ transient int offset;

        @DexIgnore
        public e(int i, int i2) {
            this.offset = i;
            this.length = i2;
        }

        @DexIgnore
        public E get(int i) {
            jk3.a(i, this.length);
            return zl3.this.get(i + this.offset);
        }

        @DexIgnore
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Iterator iterator() {
            return zl3.super.iterator();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return zl3.super.listIterator();
        }

        @DexIgnore
        public int size() {
            return this.length;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return zl3.super.listIterator(i);
        }

        @DexIgnore
        public zl3<E> subList(int i, int i2) {
            jk3.b(i, i2, this.length);
            zl3 zl3 = zl3.this;
            int i3 = this.offset;
            return zl3.subList(i + i3, i2 + i3);
        }
    }

    @DexIgnore
    public static <E> zl3<E> a(Object... objArr) {
        in3.a(objArr);
        return asImmutableList(objArr);
    }

    @DexIgnore
    public static <E> zl3<E> asImmutableList(Object[] objArr) {
        return asImmutableList(objArr, objArr.length);
    }

    @DexIgnore
    public static <E> b<E> builder() {
        return new b<>();
    }

    @DexIgnore
    public static <E> zl3<E> copyOf(Iterable<? extends E> iterable) {
        jk3.a(iterable);
        return iterable instanceof Collection ? copyOf((Collection) iterable) : copyOf(iterable.iterator());
    }

    @DexIgnore
    public static <E> zl3<E> of() {
        return pn3.EMPTY;
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    @Deprecated
    public final void add(int i, E e2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final zl3<E> asList() {
        return this;
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return um3.a((List<?>) this, obj);
    }

    @DexIgnore
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = ~(~((i * 31) + get(i2).hashCode()));
        }
        return i;
    }

    @DexIgnore
    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return um3.b(this, obj);
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return um3.d(this, obj);
    }

    @DexIgnore
    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public zl3<E> reverse() {
        return size() <= 1 ? this : new c(this);
    }

    @DexIgnore
    @Deprecated
    public final E set(int i, E e2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public zl3<E> subListUnchecked(int i, int i2) {
        return new e(i, i2 - i);
    }

    @DexIgnore
    public Object writeReplace() {
        return new d(toArray());
    }

    @DexIgnore
    public static <E> zl3<E> asImmutableList(Object[] objArr, int i) {
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return new ao3(objArr[0]);
        }
        if (i < objArr.length) {
            objArr = in3.a((T[]) objArr, i);
        }
        return new pn3(objArr);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2) {
        return new ao3(e2);
    }

    @DexIgnore
    public jo3<E> iterator() {
        return listIterator();
    }

    @DexIgnore
    public zl3<E> subList(int i, int i2) {
        jk3.b(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return of();
        }
        if (i3 != 1) {
            return subListUnchecked(i, i2);
        }
        return of(get(i));
    }

    @DexIgnore
    public static <E> zl3<E> copyOf(Collection<? extends E> collection) {
        if (!(collection instanceof vl3)) {
            return a(collection.toArray());
        }
        zl3<E> asList = ((vl3) collection).asList();
        return asList.isPartialView() ? asImmutableList(asList.toArray()) : asList;
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3) {
        return a(e2, e3);
    }

    @DexIgnore
    public ko3<E> listIterator() {
        return listIterator(0);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4) {
        return a(e2, e3, e4);
    }

    @DexIgnore
    public ko3<E> listIterator(int i) {
        return new a(size(), i);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5) {
        return a(e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6) {
        return a(e2, e3, e4, e5, e6);
    }

    @DexIgnore
    public static <E> zl3<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        b bVar = new b();
        bVar.a((Object) next);
        bVar.a(it);
        return bVar.a();
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7) {
        return a(e2, e3, e4, e5, e6, e7);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        return a(e2, e3, e4, e5, e6, e7, e8);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9, e10);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9, e10, e11);
    }

    @DexIgnore
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11, E e12) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12);
    }

    @DexIgnore
    public static <E> zl3<E> copyOf(E[] eArr) {
        int length = eArr.length;
        if (length == 0) {
            return of();
        }
        if (length == 1) {
            return new ao3(eArr[0]);
        }
        Object[] objArr = (Object[]) eArr.clone();
        in3.a(objArr);
        return new pn3(objArr);
    }

    @DexIgnore
    @SafeVarargs
    public static <E> zl3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11, E e12, E e13, E... eArr) {
        Object[] objArr = new Object[(eArr.length + 12)];
        objArr[0] = e2;
        objArr[1] = e3;
        objArr[2] = e4;
        objArr[3] = e5;
        objArr[4] = e6;
        objArr[5] = e7;
        objArr[6] = e8;
        objArr[7] = e9;
        objArr[8] = e10;
        objArr[9] = e11;
        objArr[10] = e12;
        objArr[11] = e13;
        System.arraycopy(eArr, 0, objArr, 12, eArr.length);
        return a(objArr);
    }
}
