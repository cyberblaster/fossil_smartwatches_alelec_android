package com.fossil;

import android.app.Activity;
import android.widget.Toast;
import com.portfolio.platform.data.InAppNotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp5 {
    @DexIgnore
    public static /* final */ ic6 a; // = jc6.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ b b; // = new b((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements gg6<fp5> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        public final fp5 invoke() {
            return c.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ /* synthetic */ li6[] a;

        /*
        static {
            dh6 dh6 = new dh6(kh6.a(b.class), "instance", "getInstance()Lcom/portfolio/platform/uirenew/inappnotification/InAppNotificationUtils;");
            kh6.a((ch6) dh6);
            a = new li6[]{dh6};
        }
        */

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final fp5 a() {
            ic6 a2 = fp5.a;
            b bVar = fp5.b;
            li6 li6 = a[0];
            return (fp5) a2.getValue();
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ fp5 a; // = new fp5();
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final fp5 a() {
            return a;
        }
    }

    /*
    static {
        wg6.a((Object) ep5.INSTANCE.getClass().getSimpleName(), "InAppNotificationUtils::\u2026lass.javaClass.simpleName");
    }
    */

    @DexIgnore
    public final void a(Activity activity, InAppNotification inAppNotification) {
        wg6.b(inAppNotification, "inAppNotification");
        if (activity != null) {
            Toast.makeText(activity, "Title: " + inAppNotification.getTitle() + " - Message Body: " + inAppNotification.getContent(), 1).show();
        }
    }
}
