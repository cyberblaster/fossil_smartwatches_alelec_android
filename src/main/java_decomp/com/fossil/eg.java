package com.fossil;

import android.graphics.Canvas;
import android.os.Build;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eg implements dg {
    @DexIgnore
    public static /* final */ dg a; // = new eg();

    @DexIgnore
    public static float a(RecyclerView recyclerView, View view) {
        int childCount = recyclerView.getChildCount();
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (childAt != view) {
                float k = x9.k(childAt);
                if (k > f) {
                    f = k;
                }
            }
        }
        return f;
    }

    @DexIgnore
    public void a(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
    }

    @DexIgnore
    public void b(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        if (Build.VERSION.SDK_INT >= 21 && z && view.getTag(qf.item_touch_helper_previous_elevation) == null) {
            Float valueOf = Float.valueOf(x9.k(view));
            x9.a(view, a(recyclerView, view) + 1.0f);
            view.setTag(qf.item_touch_helper_previous_elevation, valueOf);
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }

    @DexIgnore
    public void b(View view) {
    }

    @DexIgnore
    public void a(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            Object tag = view.getTag(qf.item_touch_helper_previous_elevation);
            if (tag instanceof Float) {
                x9.a(view, ((Float) tag).floatValue());
            }
            view.setTag(qf.item_touch_helper_previous_elevation, (Object) null);
        }
        view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }
}
