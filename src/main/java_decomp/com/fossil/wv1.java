package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.fossil.e12;
import com.fossil.rv1;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class wv1 {
    @DexIgnore
    public static /* final */ Set<wv1> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    @Deprecated
    public interface b extends pw1 {
    }

    @DexIgnore
    @Deprecated
    public interface c extends ww1 {
    }

    @DexIgnore
    public static Set<wv1> i() {
        Set<wv1> set;
        synchronized (a) {
            set = a;
        }
        return set;
    }

    @DexIgnore
    public abstract gv1 a();

    @DexIgnore
    public <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T a(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void a(c cVar);

    @DexIgnore
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    public <A extends rv1.b, T extends nw1<? extends ew1, A>> T b(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract yv1<Status> b();

    @DexIgnore
    public abstract void b(c cVar);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public Context e() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Looper f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public void h() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static final class a {
        @DexIgnore
        public Account a;
        @DexIgnore
        public /* final */ Set<Scope> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Scope> c; // = new HashSet();
        @DexIgnore
        public int d;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public /* final */ Map<rv1<?>, e12.b> h; // = new p4();
        @DexIgnore
        public /* final */ Context i;
        @DexIgnore
        public /* final */ Map<rv1<?>, rv1.d> j; // = new p4();
        @DexIgnore
        public sw1 k;
        @DexIgnore
        public int l; // = -1;
        @DexIgnore
        public c m;
        @DexIgnore
        public Looper n;
        @DexIgnore
        public jv1 o; // = jv1.a();
        @DexIgnore
        public rv1.a<? extends ac3, lb3> p; // = zb3.c;
        @DexIgnore
        public /* final */ ArrayList<b> q; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<c> r; // = new ArrayList<>();

        @DexIgnore
        public a(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @DexIgnore
        public final a a(Handler handler) {
            w12.a(handler, (Object) "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }

        @DexIgnore
        public final e12 b() {
            lb3 lb3 = lb3.j;
            if (this.j.containsKey(zb3.e)) {
                lb3 = (lb3) this.j.get(zb3.e);
            }
            return new e12(this.a, this.b, this.h, this.d, this.e, this.f, this.g, lb3, false);
        }

        @DexIgnore
        public final a a(b bVar) {
            w12.a(bVar, (Object) "Listener must not be null");
            this.q.add(bVar);
            return this;
        }

        @DexIgnore
        public final a a(c cVar) {
            w12.a(cVar, (Object) "Listener must not be null");
            this.r.add(cVar);
            return this;
        }

        @DexIgnore
        public final a a(Scope scope) {
            w12.a(scope, (Object) "Scope must not be null");
            this.b.add(scope);
            return this;
        }

        @DexIgnore
        public final a a(String[] strArr) {
            for (String scope : strArr) {
                this.b.add(new Scope(scope));
            }
            return this;
        }

        @DexIgnore
        public final a a(rv1<? extends rv1.d.e> rv1) {
            w12.a(rv1, (Object) "Api must not be null");
            this.j.put(rv1, (Object) null);
            List<Scope> a2 = rv1.c().a(null);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final a a(rv1<? extends rv1.d.e> rv1, Scope... scopeArr) {
            w12.a(rv1, (Object) "Api must not be null");
            this.j.put(rv1, (Object) null);
            a(rv1, (rv1.d) null, scopeArr);
            return this;
        }

        @DexIgnore
        public final <O extends rv1.d.c> a a(rv1<O> rv1, O o2) {
            w12.a(rv1, (Object) "Api must not be null");
            w12.a(o2, (Object) "Null options are not permitted for this Api");
            this.j.put(rv1, o2);
            List<Scope> a2 = rv1.c().a(o2);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final wv1 a() {
            w12.a(!this.j.isEmpty(), (Object) "must call addApi() to add at least one API");
            e12 b2 = b();
            rv1 rv1 = null;
            Map<rv1<?>, e12.b> f2 = b2.f();
            p4 p4Var = new p4();
            p4 p4Var2 = new p4();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (rv1 next : this.j.keySet()) {
                rv1.d dVar = this.j.get(next);
                boolean z2 = f2.get(next) != null;
                p4Var.put(next, Boolean.valueOf(z2));
                a02 a02 = new a02(next, z2);
                arrayList.add(a02);
                rv1.a d2 = next.d();
                rv1 rv12 = next;
                rv1.f a2 = d2.a(this.i, this.n, b2, dVar, a02, a02);
                p4Var2.put(rv12.a(), a2);
                if (d2.a() == 1) {
                    z = dVar != null;
                }
                if (a2.d()) {
                    if (rv1 == null) {
                        rv1 = rv12;
                    } else {
                        String b3 = rv12.b();
                        String b4 = rv1.b();
                        StringBuilder sb = new StringBuilder(String.valueOf(b3).length() + 21 + String.valueOf(b4).length());
                        sb.append(b3);
                        sb.append(" cannot be used with ");
                        sb.append(b4);
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
            if (rv1 != null) {
                if (!z) {
                    w12.b(this.a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", rv1.b());
                    w12.b(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", rv1.b());
                } else {
                    String b5 = rv1.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b5).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b5);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            ay1 ay1 = new ay1(this.i, new ReentrantLock(), this.n, b2, this.o, this.p, p4Var, this.q, this.r, p4Var2, this.l, ay1.a((Iterable<rv1.f>) p4Var2.values(), true), arrayList, false);
            synchronized (wv1.a) {
                wv1.a.add(ay1);
            }
            if (this.l >= 0) {
                tz1.b(this.k).a(this.l, ay1, this.m);
            }
            return ay1;
        }

        @DexIgnore
        public final <O extends rv1.d> void a(rv1<O> rv1, O o2, Scope... scopeArr) {
            HashSet hashSet = new HashSet(rv1.c().a(o2));
            for (Scope add : scopeArr) {
                hashSet.add(add);
            }
            this.h.put(rv1, new e12.b(hashSet));
        }
    }

    @DexIgnore
    public boolean a(yw1 yw1) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public void a(gz1 gz1) {
        throw new UnsupportedOperationException();
    }
}
