package com.fossil;

import android.text.TextUtils;
import com.fossil.m24;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs4 extends m24<b, m24.d, m24.a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ GoalTrackingRepository d;
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = qs4.class.getSimpleName();
        wg6.a((Object) simpleName, "FetchGoalTrackingData::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public qs4(GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(userRepository, "mUserRepository");
        this.d = goalTrackingRepository;
        this.e = userRepository;
    }

    @DexIgnore
    public String c() {
        return f;
    }

    @DexIgnore
    public Object a(b bVar, xe6<? super cd6> xe6) {
        if (bVar == null) {
            return cd6.a;
        }
        Date a2 = bVar.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        local.d(str, "executeUseCase - date=" + uy5.a(a2));
        MFUser currentUser = this.e.getCurrentUser();
        if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
            return cd6.a;
        }
        Date d2 = bk4.d(currentUser.getCreatedAt());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = f;
        StringBuilder sb = new StringBuilder();
        sb.append("executeUseCase - createdDate=");
        wg6.a((Object) d2, "createdDate");
        sb.append(uy5.a(d2));
        local3.d(str3, sb.toString());
        if (bk4.b(d2, a2) || bk4.b(a2, new Date())) {
            return cd6.a;
        }
        Calendar q = bk4.q(a2);
        wg6.a((Object) q, "DateHelper.getStartOfWeek(date)");
        Date time = q.getTime();
        Date date = bk4.c(d2, time) ? d2 : time;
        GoalTrackingRepository goalTrackingRepository = this.d;
        wg6.a((Object) date, "startDate");
        Object loadGoalTrackingDataList$default = GoalTrackingRepository.loadGoalTrackingDataList$default(goalTrackingRepository, date, a2, 0, 0, xe6, 12, (Object) null);
        if (loadGoalTrackingDataList$default == ff6.a()) {
            return loadGoalTrackingDataList$default;
        }
        return cd6.a;
    }
}
