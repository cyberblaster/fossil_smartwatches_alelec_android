package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu4 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ Category a;
    @DexIgnore
    public int b;
    @DexIgnore
    public ArrayList<Category> c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public /* final */ /* synthetic */ gu4 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gu4$a$a")
        /* renamed from: com.fossil.gu4$a$a  reason: collision with other inner class name */
        public static final class C0012a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0012a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.b.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CategoriesAdapter", "onItemClick pos=" + this.a.getAdapterPosition() + " currentPos=" + this.a.b.b);
                    a aVar = this.a;
                    aVar.b.b = aVar.getAdapterPosition();
                    if (this.a.b.b != 0) {
                        c c = this.a.b.c();
                        if (c != null) {
                            Object obj = this.a.b.c.get(this.a.b.b);
                            wg6.a(obj, "mData[mSelectedIndex]");
                            c.a((Category) obj);
                            return;
                        }
                        return;
                    }
                    c c2 = this.a.b.c();
                    if (c2 != null) {
                        c2.a();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gu4 gu4, View view) {
            super(view);
            wg6.b(view, "view");
            this.b = gu4;
            View findViewById = view.findViewById(2131362308);
            wg6.a((Object) findViewById, "view.findViewById(R.id.ftv_category)");
            this.a = (FlexibleButton) findViewById;
            this.a.setOnClickListener(new C0012a(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r6v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r6v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r6v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r6v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v9, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r1v10, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r5v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        public final void a(Category category, int i) {
            wg6.b(category, "category");
            Context context = this.a.getContext();
            Object r1 = this.a;
            r1.setCompoundDrawablePadding((int) gy5.a(6, r1.getContext()));
            if (TextUtils.isEmpty(category.getId())) {
                Object r5 = this.a;
                wg6.a((Object) context, "context");
                r5.setText(context.getResources().getString(2131886753));
            } else {
                String a2 = jm4.a(PortfolioApp.get.instance(), category.getName(), category.getEnglishName());
                if (i == 0) {
                    this.a.setText(a2);
                } else {
                    wg6.a((Object) a2, "name");
                    if (a2 != null) {
                        String upperCase = a2.toUpperCase();
                        wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        this.a.setText(upperCase);
                    } else {
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
            if (i == 0) {
                this.a.a("flexible_button_search");
                Drawable c = w6.c(context, 2131231061);
                if (c != null) {
                    c.setTintList(ColorStateList.valueOf(w6.a(context, 2131100008)));
                }
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds(c, (Drawable) null, (Drawable) null, (Drawable) null);
            } else if (i == this.b.b) {
                this.a.setSelected(true);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.a("flexible_button_nonBrandSurface");
            } else {
                this.a.setSelected(false);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.a("flexible_button_checkbox_typeface");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(Category category);
    }

    /*
    static {
        new b((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ gu4(ArrayList arrayList, c cVar, int i, qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final c c() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public gu4(ArrayList<Category> arrayList, c cVar) {
        wg6.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.a = new Category("Search", "Search", "Customization_Complications_Elements_Input__Search", "", "", -1);
        this.b = -1;
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558646, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026_category, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<Category> list) {
        wg6.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        this.c.add(0, this.a);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        wg6.b(str, "category");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((Category) t).getId(), (Object) str)) {
                break;
            }
        }
        Category category = (Category) t;
        if (category != null) {
            return this.c.indexOf(category);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            Category category = this.c.get(i);
            wg6.a((Object) category, "mData[position]");
            aVar.a(category, i);
        }
    }

    @DexIgnore
    public final void a(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CategoriesAdapter", "setSelectedCategory pos=" + i);
        if (i < getItemCount()) {
            this.b = i;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wg6.b(cVar, "listener");
        this.d = cVar;
    }
}
