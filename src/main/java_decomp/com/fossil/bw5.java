package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw5 extends zv5 {
    @DexIgnore
    public /* final */ aw5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        wg6.a((Object) bw5.class.getSimpleName(), "WelcomePresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public bw5(aw5 aw5) {
        wg6.b(aw5, "mView");
        this.e = aw5;
    }

    @DexIgnore
    public void f() {
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.e.S0();
    }

    @DexIgnore
    public void i() {
        this.e.N0();
    }

    @DexIgnore
    public void j() {
        this.e.a(this);
    }
}
