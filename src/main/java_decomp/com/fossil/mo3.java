package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mo3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends no3 {
        @DexIgnore
        public /* final */ Charset a;

        @DexIgnore
        public a(Charset charset) {
            jk3.a(charset);
            this.a = charset;
        }

        @DexIgnore
        public Reader a() throws IOException {
            return new InputStreamReader(mo3.this.a(), this.a);
        }

        @DexIgnore
        public String toString() {
            return mo3.this.toString() + ".asCharSource(" + this.a + ")";
        }
    }

    @DexIgnore
    public no3 a(Charset charset) {
        return new a(charset);
    }

    @DexIgnore
    public abstract InputStream a() throws IOException;
}
