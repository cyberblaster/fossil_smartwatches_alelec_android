package com.fossil;

import com.fossil.g60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rw0 extends tg6 implements hg6<byte[], g60> {
    @DexIgnore
    public rw0(g60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(g60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/BatteryConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((g60.a) this.receiver).a((byte[]) obj);
    }
}
