package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hj3;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sj3 extends tj3 {
    @DexIgnore
    public static /* final */ boolean o; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.e e; // = new b(this.a);
    @DexIgnore
    public /* final */ TextInputLayout.f f; // = new c();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public long i; // = RecyclerView.FOREVER_NS;
    @DexIgnore
    public StateListDrawable j;
    @DexIgnore
    public dj3 k;
    @DexIgnore
    public AccessibilityManager l;
    @DexIgnore
    public ValueAnimator m;
    @DexIgnore
    public ValueAnimator n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj3$a$a")
        /* renamed from: com.fossil.sj3$a$a  reason: collision with other inner class name */
        public class C0045a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ AutoCompleteTextView a;

            @DexIgnore
            public C0045a(AutoCompleteTextView autoCompleteTextView) {
                this.a = autoCompleteTextView;
            }

            @DexIgnore
            public void run() {
                boolean isPopupShowing = this.a.isPopupShowing();
                sj3.this.a(isPopupShowing);
                boolean unused = sj3.this.g = isPopupShowing;
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            sj3 sj3 = sj3.this;
            AutoCompleteTextView a2 = sj3.a(sj3.a.getEditText());
            a2.post(new C0045a(a2));
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TextInputLayout.e {
        @DexIgnore
        public b(TextInputLayout textInputLayout) {
            super(textInputLayout);
        }

        @DexIgnore
        public void a(View view, ia iaVar) {
            super.a(view, iaVar);
            iaVar.a((CharSequence) Spinner.class.getName());
            if (iaVar.x()) {
                iaVar.d((CharSequence) null);
            }
        }

        @DexIgnore
        public void c(View view, AccessibilityEvent accessibilityEvent) {
            super.c(view, accessibilityEvent);
            sj3 sj3 = sj3.this;
            AutoCompleteTextView a = sj3.a(sj3.a.getEditText());
            if (accessibilityEvent.getEventType() == 1 && sj3.this.l.isTouchExplorationEnabled()) {
                sj3.this.d(a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements TextInputLayout.f {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(TextInputLayout textInputLayout) {
            AutoCompleteTextView a2 = sj3.this.a(textInputLayout.getEditText());
            sj3.this.b(a2);
            sj3.this.a(a2);
            sj3.this.c(a2);
            a2.setThreshold(0);
            a2.removeTextChangedListener(sj3.this.d);
            a2.addTextChangedListener(sj3.this.d);
            textInputLayout.setErrorIconDrawable((Drawable) null);
            textInputLayout.setTextInputAccessibilityDelegate(sj3.this.e);
            textInputLayout.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            sj3.this.d((AutoCompleteTextView) sj3.this.a.getEditText());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ AutoCompleteTextView a;

        @DexIgnore
        public e(AutoCompleteTextView autoCompleteTextView) {
            this.a = autoCompleteTextView;
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                if (sj3.this.d()) {
                    boolean unused = sj3.this.g = false;
                }
                sj3.this.d(this.a);
                view.performClick();
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements View.OnFocusChangeListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            sj3.this.a.setEndIconActivated(z);
            if (!z) {
                sj3.this.a(false);
                boolean unused = sj3.this.g = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements AutoCompleteTextView.OnDismissListener {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void onDismiss() {
            boolean unused = sj3.this.g = true;
            long unused2 = sj3.this.i = System.currentTimeMillis();
            sj3.this.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends AnimatorListenerAdapter {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            sj3 sj3 = sj3.this;
            sj3.c.setChecked(sj3.h);
            sj3.this.n.start();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            sj3.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore
    public sj3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public boolean a(int i2) {
        return i2 != 0;
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public final void c(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setOnTouchListener(new e(autoCompleteTextView));
        autoCompleteTextView.setOnFocusChangeListener(new f());
        if (o) {
            autoCompleteTextView.setOnDismissListener(new g());
        }
    }

    @DexIgnore
    public final void d(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView != null) {
            if (d()) {
                this.g = false;
            }
            if (!this.g) {
                if (o) {
                    a(!this.h);
                } else {
                    this.h = !this.h;
                    this.c.toggle();
                }
                if (this.h) {
                    autoCompleteTextView.requestFocus();
                    autoCompleteTextView.showDropDown();
                    return;
                }
                autoCompleteTextView.dismissDropDown();
                return;
            }
            this.g = false;
        }
    }

    @DexIgnore
    public final void b(AutoCompleteTextView autoCompleteTextView) {
        if (o) {
            int boxBackgroundMode = this.a.getBoxBackgroundMode();
            if (boxBackgroundMode == 2) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.k);
            } else if (boxBackgroundMode == 1) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.j);
            }
        }
    }

    @DexIgnore
    public void a() {
        float dimensionPixelOffset = (float) this.b.getResources().getDimensionPixelOffset(pf3.mtrl_shape_corner_size_small_component);
        float dimensionPixelOffset2 = (float) this.b.getResources().getDimensionPixelOffset(pf3.mtrl_exposed_dropdown_menu_popup_elevation);
        int dimensionPixelOffset3 = this.b.getResources().getDimensionPixelOffset(pf3.mtrl_exposed_dropdown_menu_popup_vertical_padding);
        dj3 a2 = a(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        dj3 a3 = a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        this.k = a2;
        this.j = new StateListDrawable();
        this.j.addState(new int[]{16842922}, a2);
        this.j.addState(new int[0], a3);
        this.a.setEndIconDrawable(u0.c(this.b, o ? qf3.mtrl_dropdown_arrow : qf3.mtrl_ic_arrow_drop_down));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(vf3.exposed_dropdown_menu_content_description));
        this.a.setEndIconOnClickListener(new d());
        this.a.a(this.f);
        c();
        x9.h(this.c, 2);
        this.l = (AccessibilityManager) this.b.getSystemService("accessibility");
    }

    @DexIgnore
    public final void c() {
        this.n = a(67, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        this.m = a(50, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.m.addListener(new h());
    }

    @DexIgnore
    public final void b(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, dj3 dj3) {
        LayerDrawable layerDrawable;
        int a2 = yg3.a((View) autoCompleteTextView, nf3.colorSurface);
        dj3 dj32 = new dj3(dj3.n());
        int a3 = yg3.a(i2, a2, 0.1f);
        dj32.a(new ColorStateList(iArr, new int[]{a3, 0}));
        if (o) {
            dj32.setTint(a2);
            ColorStateList colorStateList = new ColorStateList(iArr, new int[]{a3, a2});
            dj3 dj33 = new dj3(dj3.n());
            dj33.setTint(-1);
            layerDrawable = new LayerDrawable(new Drawable[]{new RippleDrawable(colorStateList, dj32, dj33), dj3});
        } else {
            layerDrawable = new LayerDrawable(new Drawable[]{dj32, dj3});
        }
        x9.a((View) autoCompleteTextView, (Drawable) layerDrawable);
    }

    @DexIgnore
    public final boolean d() {
        long currentTimeMillis = System.currentTimeMillis() - this.i;
        return currentTimeMillis < 0 || currentTimeMillis > 300;
    }

    @DexIgnore
    public final void a(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView.getKeyListener() == null) {
            int boxBackgroundMode = this.a.getBoxBackgroundMode();
            dj3 boxBackground = this.a.getBoxBackground();
            int a2 = yg3.a((View) autoCompleteTextView, nf3.colorControlHighlight);
            int[][] iArr = {new int[]{16842919}, new int[0]};
            if (boxBackgroundMode == 2) {
                b(autoCompleteTextView, a2, iArr, boxBackground);
            } else if (boxBackgroundMode == 1) {
                a(autoCompleteTextView, a2, iArr, boxBackground);
            }
        }
    }

    @DexIgnore
    public final void a(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, dj3 dj3) {
        int boxBackgroundColor = this.a.getBoxBackgroundColor();
        int[] iArr2 = {yg3.a(i2, boxBackgroundColor, 0.1f), boxBackgroundColor};
        if (o) {
            x9.a((View) autoCompleteTextView, (Drawable) new RippleDrawable(new ColorStateList(iArr, iArr2), dj3, dj3));
            return;
        }
        dj3 dj32 = new dj3(dj3.n());
        dj32.a(new ColorStateList(iArr, iArr2));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{dj3, dj32});
        int t = x9.t(autoCompleteTextView);
        int paddingTop = autoCompleteTextView.getPaddingTop();
        int s = x9.s(autoCompleteTextView);
        int paddingBottom = autoCompleteTextView.getPaddingBottom();
        x9.a((View) autoCompleteTextView, (Drawable) layerDrawable);
        x9.b(autoCompleteTextView, t, paddingTop, s, paddingBottom);
    }

    @DexIgnore
    public final dj3 a(float f2, float f3, float f4, int i2) {
        hj3.b n2 = hj3.n();
        n2.d(f2);
        n2.e(f2);
        n2.b(f3);
        n2.c(f3);
        hj3 a2 = n2.a();
        dj3 a3 = dj3.a(this.b, f4);
        a3.setShapeAppearanceModel(a2);
        a3.a(0, i2, 0, i2);
        return a3;
    }

    @DexIgnore
    public final AutoCompleteTextView a(EditText editText) {
        if (editText instanceof AutoCompleteTextView) {
            return (AutoCompleteTextView) editText;
        }
        throw new RuntimeException("EditText needs to be an AutoCompleteTextView if an Exposed Dropdown Menu is being used.");
    }

    @DexIgnore
    public final void a(boolean z) {
        if (this.h != z) {
            this.h = z;
            this.n.cancel();
            this.m.start();
        }
    }

    @DexIgnore
    public final ValueAnimator a(int i2, float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(yf3.a);
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new i());
        return ofFloat;
    }
}
