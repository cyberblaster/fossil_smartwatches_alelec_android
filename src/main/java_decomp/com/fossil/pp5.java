package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp5 implements MembersInjector<np5> {
    @DexIgnore
    public static void a(LoginPresenter loginPresenter, LoginEmailUseCase loginEmailUseCase) {
        loginPresenter.g = loginEmailUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, LoginSocialUseCase loginSocialUseCase) {
        loginPresenter.h = loginSocialUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, DownloadUserInfoUseCase downloadUserInfoUseCase) {
        loginPresenter.i = downloadUserInfoUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, rr4 rr4) {
        loginPresenter.j = rr4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, cj4 cj4) {
        loginPresenter.k = cj4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, UserRepository userRepository) {
        loginPresenter.l = userRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, DeviceRepository deviceRepository) {
        loginPresenter.m = deviceRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, an4 an4) {
        loginPresenter.n = an4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchActivities fetchActivities) {
        loginPresenter.o = fetchActivities;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchSummaries fetchSummaries) {
        loginPresenter.p = fetchSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, z24 z24) {
        loginPresenter.q = z24;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchSleepSessions fetchSleepSessions) {
        loginPresenter.r = fetchSleepSessions;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchSleepSummaries fetchSleepSummaries) {
        loginPresenter.s = fetchSleepSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchHeartRateSamples fetchHeartRateSamples) {
        loginPresenter.t = fetchHeartRateSamples;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries) {
        loginPresenter.u = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, lt4 lt4) {
        loginPresenter.v = lt4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, kn4 kn4) {
        loginPresenter.w = kn4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, mt4 mt4) {
        loginPresenter.x = mt4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, pt4 pt4) {
        loginPresenter.y = pt4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, ot4 ot4) {
        loginPresenter.z = ot4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, CheckAuthenticationSocialExisting checkAuthenticationSocialExisting) {
        loginPresenter.A = checkAuthenticationSocialExisting;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, AnalyticsHelper analyticsHelper) {
        loginPresenter.B = analyticsHelper;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, SummariesRepository summariesRepository) {
        loginPresenter.C = summariesRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, SleepSummariesRepository sleepSummariesRepository) {
        loginPresenter.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, GoalTrackingRepository goalTrackingRepository) {
        loginPresenter.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries) {
        loginPresenter.F = fetchDailyGoalTrackingSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, qs4 qs4) {
        loginPresenter.G = qs4;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, GetSecretKeyUseCase getSecretKeyUseCase) {
        loginPresenter.H = getSecretKeyUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, WatchLocalizationRepository watchLocalizationRepository) {
        loginPresenter.I = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, AlarmsRepository alarmsRepository) {
        loginPresenter.J = alarmsRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter) {
        loginPresenter.C();
    }
}
