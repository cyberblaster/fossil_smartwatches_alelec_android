package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dr4;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr4 extends RecyclerView.g<a> {
    @DexIgnore
    public ArrayList<dr4> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ir4> b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public ImageView b;
        @DexIgnore
        public RecyclerView c;
        @DexIgnore
        public /* final */ /* synthetic */ cr4 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cr4$a$a")
        /* renamed from: com.fossil.cr4$a$a  reason: collision with other inner class name */
        public static final class C0007a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0007a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.d();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.d();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(cr4 cr4, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.d = cr4;
            View findViewById = view.findViewById(2131363119);
            if (findViewById != null) {
                TextView textView = (TextView) findViewById;
                textView.setOnClickListener(new C0007a(this));
                this.a = textView;
                View findViewById2 = view.findViewById(2131362539);
                if (findViewById2 != null) {
                    ImageView imageView = (ImageView) findViewById2;
                    imageView.setOnClickListener(new b(this));
                    this.b = imageView;
                    RecyclerView findViewById3 = view.findViewById(2131362872);
                    if (findViewById3 != null) {
                        this.c = findViewById3;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final RecyclerView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }

        @DexIgnore
        public final void d() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                ArrayList<ir4> d2 = this.d.d();
                if (d2 != null) {
                    boolean c2 = d2.get(adapterPosition).c();
                    ArrayList<ir4> d3 = this.d.d();
                    if (d3 != null) {
                        d3.get(adapterPosition).a(!c2);
                        this.d.notifyItemChanged(adapterPosition);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final ImageView a() {
            return this.b;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements dr4.e {
        @DexIgnore
        public /* final */ /* synthetic */ cr4 a;

        @DexIgnore
        public c(ir4 ir4, cr4 cr4) {
            this.a = cr4;
        }

        @DexIgnore
        public void a(String str, int i, int i2, Object obj, Bundle bundle) {
            wg6.b(str, "tagName");
            b c = this.a.c();
            if (c != null) {
                c.a(str, i, i2, obj, bundle);
            }
        }

        @DexIgnore
        public void b(String str, int i, int i2, Object obj, Bundle bundle) {
            wg6.b(str, "tagName");
            b c = this.a.c();
            if (c != null) {
                c.b(str, i, i2, obj, bundle);
            }
        }
    }

    @DexIgnore
    public final void a(ArrayList<ir4> arrayList) {
        this.a.clear();
        if (arrayList != null) {
            for (ir4 ir4 : arrayList) {
                ArrayList<dr4> arrayList2 = this.a;
                dr4 dr4 = new dr4();
                dr4.a((dr4.e) new c(ir4, this));
                dr4.a((List<? extends er4>) ir4.a());
                arrayList2.add(dr4);
            }
        }
        this.b = arrayList;
    }

    @DexIgnore
    public final b c() {
        return this.c;
    }

    @DexIgnore
    public final ArrayList<ir4> d() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        ArrayList<ir4> arrayList = this.b;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558657, viewGroup, false);
        wg6.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        TextView c2 = aVar.c();
        ArrayList<ir4> arrayList = this.b;
        if (arrayList != null) {
            c2.setText(arrayList.get(i).b());
            RecyclerView b2 = aVar.b();
            b2.setLayoutManager(new LinearLayoutManager(b2.getContext()));
            dr4 dr4 = this.a.get(i);
            wg6.a((Object) dr4, "listDebugChildAdapter[position]");
            dr4 dr42 = dr4;
            dr42.a(i);
            b2.setAdapter(dr42);
            ArrayList<ir4> arrayList2 = this.b;
            if (arrayList2 == null) {
                wg6.a();
                throw null;
            } else if (arrayList2.get(i).c()) {
                aVar.a().setImageResource(2131231038);
                aVar.b().setVisibility(0);
            } else {
                aVar.a().setImageResource(2131231037);
                aVar.b().setVisibility(8);
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "itemClickListener");
        this.c = bVar;
    }
}
