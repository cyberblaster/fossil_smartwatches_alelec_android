package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eg4 extends dg4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362297, 1);
        A.put(2131362048, 2);
        A.put(2131361814, 3);
        A.put(2131362690, 4);
        A.put(2131362786, 5);
        A.put(2131362785, 6);
        A.put(2131363274, 7);
    }
    */

    @DexIgnore
    public eg4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 8, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eg4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[3], objArr[2], objArr[0], objArr[1], objArr[4], objArr[6], objArr[5], objArr[7]);
        this.y = -1;
        this.s.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
