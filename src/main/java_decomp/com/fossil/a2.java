package com.fossil;

import android.widget.ListView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a2 {
    @DexIgnore
    boolean c();

    @DexIgnore
    void d();

    @DexIgnore
    void dismiss();

    @DexIgnore
    ListView f();
}
