package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iw implements bs<Bitmap> {
    @DexIgnore
    public abstract Bitmap a(au auVar, Bitmap bitmap, int i, int i2);

    @DexIgnore
    public final rt<Bitmap> a(Context context, rt<Bitmap> rtVar, int i, int i2) {
        if (r00.b(i, i2)) {
            au c = wq.a(context).c();
            Bitmap bitmap = rtVar.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap a = a(c, bitmap, i, i2);
            return bitmap.equals(a) ? rtVar : hw.a(a, c);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
