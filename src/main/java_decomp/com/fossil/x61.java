package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum x61 {
    UPDATE_CHALLENGE_INFO("update_info"),
    GET_CHALLENGE_INFO("req_chl_info"),
    LIST_CHALLENGES("req_chl_list");
    
    @DexIgnore
    public static /* final */ a51 f; // = null;
    @DexIgnore
    public /* final */ String a;

    /*
    static {
        f = new a51((qg6) null);
    }
    */

    @DexIgnore
    public x61(String str) {
        this.a = str;
    }
}
