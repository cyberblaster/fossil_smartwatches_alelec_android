package com.fossil;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fy implements my {
    @DexIgnore
    public /* final */ Set<ny> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public void a(ny nyVar) {
        this.a.add(nyVar);
        if (this.c) {
            nyVar.b();
        } else if (this.b) {
            nyVar.a();
        } else {
            nyVar.c();
        }
    }

    @DexIgnore
    public void b(ny nyVar) {
        this.a.remove(nyVar);
    }

    @DexIgnore
    public void c() {
        this.b = false;
        for (T c2 : r00.a(this.a)) {
            c2.c();
        }
    }

    @DexIgnore
    public void b() {
        this.b = true;
        for (T a2 : r00.a(this.a)) {
            a2.a();
        }
    }

    @DexIgnore
    public void a() {
        this.c = true;
        for (T b2 : r00.a(this.a)) {
            b2.b();
        }
    }
}
