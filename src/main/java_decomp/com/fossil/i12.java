package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.c12;
import com.fossil.j12;
import com.fossil.rv1;
import com.fossil.wv1;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i12<T extends IInterface> extends c12<T> implements rv1.f, j12.a {
    @DexIgnore
    public /* final */ e12 B;
    @DexIgnore
    public /* final */ Set<Scope> C;
    @DexIgnore
    public /* final */ Account D;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public i12(Context context, Looper looper, int i, e12 e12, pw1 pw1, ww1 ww1) {
        this(context, looper, r3, r4, i, e12, pw1, ww1);
        k12 a = k12.a(context);
        jv1 a2 = jv1.a();
        w12.a(pw1);
        w12.a(ww1);
    }

    @DexIgnore
    public static c12.a a(pw1 pw1) {
        if (pw1 == null) {
            return null;
        }
        return new z22(pw1);
    }

    @DexIgnore
    public final e12 G() {
        return this.B;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return set;
    }

    @DexIgnore
    public final Set<Scope> b(Set<Scope> set) {
        Set<Scope> a = a(set);
        for (Scope contains : a) {
            if (!set.contains(contains)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return a;
    }

    @DexIgnore
    public Set<Scope> e() {
        return m() ? this.C : Collections.emptySet();
    }

    @DexIgnore
    public int j() {
        return super.j();
    }

    @DexIgnore
    public final Account s() {
        return this.D;
    }

    @DexIgnore
    public final Set<Scope> x() {
        return this.C;
    }

    @DexIgnore
    public static c12.b a(ww1 ww1) {
        if (ww1 == null) {
            return null;
        }
        return new a32(ww1);
    }

    @DexIgnore
    @Deprecated
    public i12(Context context, Looper looper, int i, e12 e12, wv1.b bVar, wv1.c cVar) {
        this(context, looper, i, e12, (pw1) bVar, (ww1) cVar);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i12(Context context, Looper looper, k12 k12, jv1 jv1, int i, e12 e12, pw1 pw1, ww1 ww1) {
        super(context, looper, k12, jv1, i, a(pw1), a(ww1), e12.g());
        this.B = e12;
        this.D = e12.a();
        this.C = b(e12.d());
    }
}
