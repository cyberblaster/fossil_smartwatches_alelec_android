package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fg6 extends Error {
    @DexIgnore
    public fg6() {
        super("Kotlin reflection implementation is not found at runtime. Make sure you have kotlin-reflect.jar in the classpath");
    }

    @DexIgnore
    public fg6(String str) {
        super(str);
    }

    @DexIgnore
    public fg6(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public fg6(Throwable th) {
        super(th);
    }
}
