package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jb1<K, V> {
    @DexIgnore
    public static /* final */ n91 a; // = new n91((qg6) null);

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        r8 = r5.a;
     */
    @DexIgnore
    public final byte[] a(short s, w40 w40, K k) throws sw0 {
        w40 w402;
        u31[] a2 = a();
        int length = a2.length;
        u31 u31 = null;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            u31 u312 = a2[i];
            if (wg6.a(u312.a, w40)) {
                u31 = u312;
                break;
            }
            if (u312.a.getMajor() == w40.getMajor()) {
                byte minor = u312.a.getMinor();
                byte minor2 = (u31 == null || w402 == null) ? 0 : w402.getMinor();
                if (minor >= minor2) {
                    u31 = u312;
                }
            }
            i++;
        }
        if (u31 != null) {
            return u31.a(s, k);
        }
        throw new sw0(yu0.UNSUPPORTED_VERSION, "Not support version " + w40 + '.', (Throwable) null, 4);
    }

    @DexIgnore
    public abstract u31<K>[] a();

    @DexIgnore
    public abstract ed1<V>[] b();

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002a, code lost:
        r7 = r4.a;
     */
    @DexIgnore
    public final ed1<V> a(w40 w40) {
        w40 w402;
        ed1<V> ed1 = null;
        for (ed1<V> ed12 : b()) {
            if (wg6.a(ed12.a, w40)) {
                return ed12;
            }
            if (ed12.a.getMajor() == w40.getMajor()) {
                byte minor = ed12.a.getMinor();
                byte minor2 = (ed1 == null || w402 == null) ? 0 : w402.getMinor();
                if (minor >= minor2) {
                    ed1 = ed12;
                }
            }
        }
        return ed1;
    }

    @DexIgnore
    public final V a(byte[] bArr) throws sw0 {
        try {
            w40 w40 = new w40(bArr[2], bArr[3]);
            ed1 a2 = a(w40);
            if (a2 == null) {
                yu0 yu0 = yu0.UNSUPPORTED_VERSION;
                throw new sw0(yu0, "Not support version " + w40 + '.', (Throwable) null, 4);
            } else if (a2.b(bArr)) {
                return a2.a(bArr);
            } else {
                throw new sw0(yu0.INVALID_FILE_DATA, "Invalid file.", (Throwable) null, 4);
            }
        } catch (Exception e) {
            throw new sw0(yu0.INVALID_FILE_DATA, "Invalid file data.", e);
        }
    }
}
