package com.fossil;

import android.util.Log;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lb extends hb {
    @DexIgnore
    public Set<Class<? extends hb>> a; // = new HashSet();
    @DexIgnore
    public List<hb> b; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<String> c; // = new CopyOnWriteArrayList();

    @DexIgnore
    public void a(hb hbVar) {
        if (this.a.add(hbVar.getClass())) {
            this.b.add(hbVar);
            for (hb a2 : hbVar.a()) {
                a(a2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        boolean z = false;
        for (String next : this.c) {
            try {
                Class<?> cls = Class.forName(next);
                if (hb.class.isAssignableFrom(cls)) {
                    a((hb) cls.newInstance());
                    this.c.remove(next);
                    z = true;
                }
            } catch (ClassNotFoundException unused) {
            } catch (IllegalAccessException e) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e);
            } catch (InstantiationException e2) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e2);
            }
        }
        return z;
    }

    @DexIgnore
    public ViewDataBinding a(jb jbVar, View view, int i) {
        for (hb a2 : this.b) {
            ViewDataBinding a3 = a2.a(jbVar, view, i);
            if (a3 != null) {
                return a3;
            }
        }
        if (b()) {
            return a(jbVar, view, i);
        }
        return null;
    }

    @DexIgnore
    public ViewDataBinding a(jb jbVar, View[] viewArr, int i) {
        for (hb a2 : this.b) {
            ViewDataBinding a3 = a2.a(jbVar, viewArr, i);
            if (a3 != null) {
                return a3;
            }
        }
        if (b()) {
            return a(jbVar, viewArr, i);
        }
        return null;
    }
}
