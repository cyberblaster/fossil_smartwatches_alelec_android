package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nf3 {
    @DexIgnore
    public static /* final */ int actionBarDivider; // = 2130968646;
    @DexIgnore
    public static /* final */ int actionBarItemBackground; // = 2130968647;
    @DexIgnore
    public static /* final */ int actionBarPopupTheme; // = 2130968648;
    @DexIgnore
    public static /* final */ int actionBarSize; // = 2130968649;
    @DexIgnore
    public static /* final */ int actionBarSplitStyle; // = 2130968650;
    @DexIgnore
    public static /* final */ int actionBarStyle; // = 2130968651;
    @DexIgnore
    public static /* final */ int actionBarTabBarStyle; // = 2130968652;
    @DexIgnore
    public static /* final */ int actionBarTabStyle; // = 2130968653;
    @DexIgnore
    public static /* final */ int actionBarTabTextStyle; // = 2130968654;
    @DexIgnore
    public static /* final */ int actionBarTheme; // = 2130968655;
    @DexIgnore
    public static /* final */ int actionBarWidgetTheme; // = 2130968656;
    @DexIgnore
    public static /* final */ int actionButtonStyle; // = 2130968657;
    @DexIgnore
    public static /* final */ int actionDropDownStyle; // = 2130968658;
    @DexIgnore
    public static /* final */ int actionLayout; // = 2130968659;
    @DexIgnore
    public static /* final */ int actionMenuTextAppearance; // = 2130968660;
    @DexIgnore
    public static /* final */ int actionMenuTextColor; // = 2130968661;
    @DexIgnore
    public static /* final */ int actionModeBackground; // = 2130968662;
    @DexIgnore
    public static /* final */ int actionModeCloseButtonStyle; // = 2130968663;
    @DexIgnore
    public static /* final */ int actionModeCloseDrawable; // = 2130968664;
    @DexIgnore
    public static /* final */ int actionModeCopyDrawable; // = 2130968665;
    @DexIgnore
    public static /* final */ int actionModeCutDrawable; // = 2130968666;
    @DexIgnore
    public static /* final */ int actionModeFindDrawable; // = 2130968667;
    @DexIgnore
    public static /* final */ int actionModePasteDrawable; // = 2130968668;
    @DexIgnore
    public static /* final */ int actionModePopupWindowStyle; // = 2130968669;
    @DexIgnore
    public static /* final */ int actionModeSelectAllDrawable; // = 2130968670;
    @DexIgnore
    public static /* final */ int actionModeShareDrawable; // = 2130968671;
    @DexIgnore
    public static /* final */ int actionModeSplitBackground; // = 2130968672;
    @DexIgnore
    public static /* final */ int actionModeStyle; // = 2130968673;
    @DexIgnore
    public static /* final */ int actionModeWebSearchDrawable; // = 2130968674;
    @DexIgnore
    public static /* final */ int actionOverflowButtonStyle; // = 2130968675;
    @DexIgnore
    public static /* final */ int actionOverflowMenuStyle; // = 2130968676;
    @DexIgnore
    public static /* final */ int actionProviderClass; // = 2130968677;
    @DexIgnore
    public static /* final */ int actionTextColorAlpha; // = 2130968678;
    @DexIgnore
    public static /* final */ int actionViewClass; // = 2130968679;
    @DexIgnore
    public static /* final */ int activityChooserViewStyle; // = 2130968688;
    @DexIgnore
    public static /* final */ int alertDialogButtonGroupStyle; // = 2130968738;
    @DexIgnore
    public static /* final */ int alertDialogCenterButtons; // = 2130968739;
    @DexIgnore
    public static /* final */ int alertDialogStyle; // = 2130968740;
    @DexIgnore
    public static /* final */ int alertDialogTheme; // = 2130968741;
    @DexIgnore
    public static /* final */ int allowStacking; // = 2130968744;
    @DexIgnore
    public static /* final */ int alpha; // = 2130968745;
    @DexIgnore
    public static /* final */ int alphabeticModifiers; // = 2130968749;
    @DexIgnore
    public static /* final */ int animationMode; // = 2130968752;
    @DexIgnore
    public static /* final */ int appBarLayoutStyle; // = 2130968753;
    @DexIgnore
    public static /* final */ int arrowHeadLength; // = 2130968754;
    @DexIgnore
    public static /* final */ int arrowShaftLength; // = 2130968755;
    @DexIgnore
    public static /* final */ int autoCompleteTextViewStyle; // = 2130968757;
    @DexIgnore
    public static /* final */ int autoSizeMaxTextSize; // = 2130968759;
    @DexIgnore
    public static /* final */ int autoSizeMinTextSize; // = 2130968760;
    @DexIgnore
    public static /* final */ int autoSizePresetSizes; // = 2130968761;
    @DexIgnore
    public static /* final */ int autoSizeStepGranularity; // = 2130968762;
    @DexIgnore
    public static /* final */ int autoSizeTextType; // = 2130968763;
    @DexIgnore
    public static /* final */ int background; // = 2130968768;
    @DexIgnore
    public static /* final */ int backgroundColor; // = 2130968769;
    @DexIgnore
    public static /* final */ int backgroundInsetBottom; // = 2130968771;
    @DexIgnore
    public static /* final */ int backgroundInsetEnd; // = 2130968772;
    @DexIgnore
    public static /* final */ int backgroundInsetStart; // = 2130968773;
    @DexIgnore
    public static /* final */ int backgroundInsetTop; // = 2130968774;
    @DexIgnore
    public static /* final */ int backgroundOverlayColorAlpha; // = 2130968775;
    @DexIgnore
    public static /* final */ int backgroundSplit; // = 2130968776;
    @DexIgnore
    public static /* final */ int backgroundStacked; // = 2130968777;
    @DexIgnore
    public static /* final */ int backgroundTint; // = 2130968779;
    @DexIgnore
    public static /* final */ int backgroundTintMode; // = 2130968780;
    @DexIgnore
    public static /* final */ int badgeGravity; // = 2130968782;
    @DexIgnore
    public static /* final */ int badgeStyle; // = 2130968783;
    @DexIgnore
    public static /* final */ int badgeTextColor; // = 2130968784;
    @DexIgnore
    public static /* final */ int barLength; // = 2130968785;
    @DexIgnore
    public static /* final */ int behavior_autoHide; // = 2130968848;
    @DexIgnore
    public static /* final */ int behavior_autoShrink; // = 2130968849;
    @DexIgnore
    public static /* final */ int behavior_expandedOffset; // = 2130968850;
    @DexIgnore
    public static /* final */ int behavior_fitToContents; // = 2130968851;
    @DexIgnore
    public static /* final */ int behavior_halfExpandedRatio; // = 2130968852;
    @DexIgnore
    public static /* final */ int behavior_hideable; // = 2130968853;
    @DexIgnore
    public static /* final */ int behavior_overlapTop; // = 2130968854;
    @DexIgnore
    public static /* final */ int behavior_peekHeight; // = 2130968855;
    @DexIgnore
    public static /* final */ int behavior_saveFlags; // = 2130968856;
    @DexIgnore
    public static /* final */ int behavior_skipCollapsed; // = 2130968857;
    @DexIgnore
    public static /* final */ int borderWidth; // = 2130968863;
    @DexIgnore
    public static /* final */ int borderlessButtonStyle; // = 2130968867;
    @DexIgnore
    public static /* final */ int bottomAppBarStyle; // = 2130968868;
    @DexIgnore
    public static /* final */ int bottomNavigationStyle; // = 2130968870;
    @DexIgnore
    public static /* final */ int bottomSheetDialogTheme; // = 2130968872;
    @DexIgnore
    public static /* final */ int bottomSheetStyle; // = 2130968873;
    @DexIgnore
    public static /* final */ int boxBackgroundColor; // = 2130968874;
    @DexIgnore
    public static /* final */ int boxBackgroundMode; // = 2130968875;
    @DexIgnore
    public static /* final */ int boxCollapsedPaddingTop; // = 2130968876;
    @DexIgnore
    public static /* final */ int boxCornerRadiusBottomEnd; // = 2130968877;
    @DexIgnore
    public static /* final */ int boxCornerRadiusBottomStart; // = 2130968878;
    @DexIgnore
    public static /* final */ int boxCornerRadiusTopEnd; // = 2130968879;
    @DexIgnore
    public static /* final */ int boxCornerRadiusTopStart; // = 2130968880;
    @DexIgnore
    public static /* final */ int boxStrokeColor; // = 2130968883;
    @DexIgnore
    public static /* final */ int boxStrokeWidth; // = 2130968885;
    @DexIgnore
    public static /* final */ int boxStrokeWidthFocused; // = 2130968886;
    @DexIgnore
    public static /* final */ int buttonBarButtonStyle; // = 2130968887;
    @DexIgnore
    public static /* final */ int buttonBarNegativeButtonStyle; // = 2130968888;
    @DexIgnore
    public static /* final */ int buttonBarNeutralButtonStyle; // = 2130968889;
    @DexIgnore
    public static /* final */ int buttonBarPositiveButtonStyle; // = 2130968890;
    @DexIgnore
    public static /* final */ int buttonBarStyle; // = 2130968891;
    @DexIgnore
    public static /* final */ int buttonCompat; // = 2130968892;
    @DexIgnore
    public static /* final */ int buttonGravity; // = 2130968893;
    @DexIgnore
    public static /* final */ int buttonIconDimen; // = 2130968894;
    @DexIgnore
    public static /* final */ int buttonPanelSideLayout; // = 2130968895;
    @DexIgnore
    public static /* final */ int buttonStyle; // = 2130968903;
    @DexIgnore
    public static /* final */ int buttonStyleSmall; // = 2130968904;
    @DexIgnore
    public static /* final */ int buttonTint; // = 2130968905;
    @DexIgnore
    public static /* final */ int buttonTintMode; // = 2130968906;
    @DexIgnore
    public static /* final */ int cardBackgroundColor; // = 2130968927;
    @DexIgnore
    public static /* final */ int cardCornerRadius; // = 2130968928;
    @DexIgnore
    public static /* final */ int cardElevation; // = 2130968929;
    @DexIgnore
    public static /* final */ int cardForegroundColor; // = 2130968930;
    @DexIgnore
    public static /* final */ int cardMaxElevation; // = 2130968931;
    @DexIgnore
    public static /* final */ int cardPreventCornerOverlap; // = 2130968932;
    @DexIgnore
    public static /* final */ int cardUseCompatPadding; // = 2130968933;
    @DexIgnore
    public static /* final */ int cardViewStyle; // = 2130968934;
    @DexIgnore
    public static /* final */ int checkboxStyle; // = 2130968943;
    @DexIgnore
    public static /* final */ int checkedButton; // = 2130968944;
    @DexIgnore
    public static /* final */ int checkedChip; // = 2130968945;
    @DexIgnore
    public static /* final */ int checkedIcon; // = 2130968946;
    @DexIgnore
    public static /* final */ int checkedIconEnabled; // = 2130968947;
    @DexIgnore
    public static /* final */ int checkedIconTint; // = 2130968948;
    @DexIgnore
    public static /* final */ int checkedIconVisible; // = 2130968949;
    @DexIgnore
    public static /* final */ int checkedTextViewStyle; // = 2130968950;
    @DexIgnore
    public static /* final */ int chipBackgroundColor; // = 2130968951;
    @DexIgnore
    public static /* final */ int chipCornerRadius; // = 2130968952;
    @DexIgnore
    public static /* final */ int chipEndPadding; // = 2130968953;
    @DexIgnore
    public static /* final */ int chipGroupStyle; // = 2130968954;
    @DexIgnore
    public static /* final */ int chipIcon; // = 2130968955;
    @DexIgnore
    public static /* final */ int chipIconEnabled; // = 2130968956;
    @DexIgnore
    public static /* final */ int chipIconSize; // = 2130968957;
    @DexIgnore
    public static /* final */ int chipIconTint; // = 2130968958;
    @DexIgnore
    public static /* final */ int chipIconVisible; // = 2130968959;
    @DexIgnore
    public static /* final */ int chipMinHeight; // = 2130968960;
    @DexIgnore
    public static /* final */ int chipMinTouchTargetSize; // = 2130968961;
    @DexIgnore
    public static /* final */ int chipSpacing; // = 2130968962;
    @DexIgnore
    public static /* final */ int chipSpacingHorizontal; // = 2130968963;
    @DexIgnore
    public static /* final */ int chipSpacingVertical; // = 2130968964;
    @DexIgnore
    public static /* final */ int chipStandaloneStyle; // = 2130968965;
    @DexIgnore
    public static /* final */ int chipStartPadding; // = 2130968966;
    @DexIgnore
    public static /* final */ int chipStrokeColor; // = 2130968967;
    @DexIgnore
    public static /* final */ int chipStrokeWidth; // = 2130968968;
    @DexIgnore
    public static /* final */ int chipStyle; // = 2130968969;
    @DexIgnore
    public static /* final */ int chipSurfaceColor; // = 2130968970;
    @DexIgnore
    public static /* final */ int closeIcon; // = 2130968987;
    @DexIgnore
    public static /* final */ int closeIconEnabled; // = 2130968988;
    @DexIgnore
    public static /* final */ int closeIconEndPadding; // = 2130968989;
    @DexIgnore
    public static /* final */ int closeIconSize; // = 2130968990;
    @DexIgnore
    public static /* final */ int closeIconStartPadding; // = 2130968991;
    @DexIgnore
    public static /* final */ int closeIconTint; // = 2130968992;
    @DexIgnore
    public static /* final */ int closeIconVisible; // = 2130968993;
    @DexIgnore
    public static /* final */ int closeItemLayout; // = 2130968994;
    @DexIgnore
    public static /* final */ int collapseContentDescription; // = 2130968996;
    @DexIgnore
    public static /* final */ int collapseIcon; // = 2130968997;
    @DexIgnore
    public static /* final */ int collapsedTitleGravity; // = 2130969000;
    @DexIgnore
    public static /* final */ int collapsedTitleTextAppearance; // = 2130969001;
    @DexIgnore
    public static /* final */ int color; // = 2130969002;
    @DexIgnore
    public static /* final */ int colorAccent; // = 2130969003;
    @DexIgnore
    public static /* final */ int colorBackgroundFloating; // = 2130969005;
    @DexIgnore
    public static /* final */ int colorButtonNormal; // = 2130969006;
    @DexIgnore
    public static /* final */ int colorControlActivated; // = 2130969007;
    @DexIgnore
    public static /* final */ int colorControlHighlight; // = 2130969008;
    @DexIgnore
    public static /* final */ int colorControlNormal; // = 2130969009;
    @DexIgnore
    public static /* final */ int colorError; // = 2130969010;
    @DexIgnore
    public static /* final */ int colorOnBackground; // = 2130969011;
    @DexIgnore
    public static /* final */ int colorOnError; // = 2130969012;
    @DexIgnore
    public static /* final */ int colorOnPrimary; // = 2130969013;
    @DexIgnore
    public static /* final */ int colorOnPrimarySurface; // = 2130969014;
    @DexIgnore
    public static /* final */ int colorOnSecondary; // = 2130969015;
    @DexIgnore
    public static /* final */ int colorOnSurface; // = 2130969016;
    @DexIgnore
    public static /* final */ int colorPrimary; // = 2130969017;
    @DexIgnore
    public static /* final */ int colorPrimaryDark; // = 2130969018;
    @DexIgnore
    public static /* final */ int colorPrimarySurface; // = 2130969019;
    @DexIgnore
    public static /* final */ int colorPrimaryVariant; // = 2130969020;
    @DexIgnore
    public static /* final */ int colorSecondary; // = 2130969022;
    @DexIgnore
    public static /* final */ int colorSecondaryVariant; // = 2130969023;
    @DexIgnore
    public static /* final */ int colorSurface; // = 2130969024;
    @DexIgnore
    public static /* final */ int colorSwitchThumbNormal; // = 2130969025;
    @DexIgnore
    public static /* final */ int commitIcon; // = 2130969038;
    @DexIgnore
    public static /* final */ int contentDescription; // = 2130969053;
    @DexIgnore
    public static /* final */ int contentInsetEnd; // = 2130969054;
    @DexIgnore
    public static /* final */ int contentInsetEndWithActions; // = 2130969055;
    @DexIgnore
    public static /* final */ int contentInsetLeft; // = 2130969056;
    @DexIgnore
    public static /* final */ int contentInsetRight; // = 2130969057;
    @DexIgnore
    public static /* final */ int contentInsetStart; // = 2130969058;
    @DexIgnore
    public static /* final */ int contentInsetStartWithNavigation; // = 2130969059;
    @DexIgnore
    public static /* final */ int contentPadding; // = 2130969060;
    @DexIgnore
    public static /* final */ int contentPaddingBottom; // = 2130969061;
    @DexIgnore
    public static /* final */ int contentPaddingLeft; // = 2130969062;
    @DexIgnore
    public static /* final */ int contentPaddingRight; // = 2130969063;
    @DexIgnore
    public static /* final */ int contentPaddingTop; // = 2130969064;
    @DexIgnore
    public static /* final */ int contentScrim; // = 2130969065;
    @DexIgnore
    public static /* final */ int controlBackground; // = 2130969066;
    @DexIgnore
    public static /* final */ int coordinatorLayoutStyle; // = 2130969083;
    @DexIgnore
    public static /* final */ int cornerFamily; // = 2130969084;
    @DexIgnore
    public static /* final */ int cornerFamilyBottomLeft; // = 2130969085;
    @DexIgnore
    public static /* final */ int cornerFamilyBottomRight; // = 2130969086;
    @DexIgnore
    public static /* final */ int cornerFamilyTopLeft; // = 2130969087;
    @DexIgnore
    public static /* final */ int cornerFamilyTopRight; // = 2130969088;
    @DexIgnore
    public static /* final */ int cornerRadius; // = 2130969089;
    @DexIgnore
    public static /* final */ int cornerSize; // = 2130969090;
    @DexIgnore
    public static /* final */ int cornerSizeBottomLeft; // = 2130969091;
    @DexIgnore
    public static /* final */ int cornerSizeBottomRight; // = 2130969092;
    @DexIgnore
    public static /* final */ int cornerSizeTopLeft; // = 2130969093;
    @DexIgnore
    public static /* final */ int cornerSizeTopRight; // = 2130969094;
    @DexIgnore
    public static /* final */ int counterEnabled; // = 2130969095;
    @DexIgnore
    public static /* final */ int counterMaxLength; // = 2130969096;
    @DexIgnore
    public static /* final */ int counterOverflowTextAppearance; // = 2130969097;
    @DexIgnore
    public static /* final */ int counterOverflowTextColor; // = 2130969098;
    @DexIgnore
    public static /* final */ int counterTextAppearance; // = 2130969099;
    @DexIgnore
    public static /* final */ int counterTextColor; // = 2130969100;
    @DexIgnore
    public static /* final */ int customNavigationLayout; // = 2130969151;
    @DexIgnore
    public static /* final */ int dayInvalidStyle; // = 2130969190;
    @DexIgnore
    public static /* final */ int daySelectedStyle; // = 2130969191;
    @DexIgnore
    public static /* final */ int dayStyle; // = 2130969192;
    @DexIgnore
    public static /* final */ int dayTodayStyle; // = 2130969193;
    @DexIgnore
    public static /* final */ int defaultQueryHint; // = 2130969198;
    @DexIgnore
    public static /* final */ int dialogCornerRadius; // = 2130969222;
    @DexIgnore
    public static /* final */ int dialogPreferredPadding; // = 2130969223;
    @DexIgnore
    public static /* final */ int dialogTheme; // = 2130969224;
    @DexIgnore
    public static /* final */ int displayOptions; // = 2130969225;
    @DexIgnore
    public static /* final */ int divider; // = 2130969227;
    @DexIgnore
    public static /* final */ int dividerHorizontal; // = 2130969228;
    @DexIgnore
    public static /* final */ int dividerPadding; // = 2130969229;
    @DexIgnore
    public static /* final */ int dividerVertical; // = 2130969230;
    @DexIgnore
    public static /* final */ int drawableBottomCompat; // = 2130969233;
    @DexIgnore
    public static /* final */ int drawableEndCompat; // = 2130969234;
    @DexIgnore
    public static /* final */ int drawableLeftCompat; // = 2130969235;
    @DexIgnore
    public static /* final */ int drawableRightCompat; // = 2130969236;
    @DexIgnore
    public static /* final */ int drawableSize; // = 2130969237;
    @DexIgnore
    public static /* final */ int drawableStartCompat; // = 2130969238;
    @DexIgnore
    public static /* final */ int drawableTint; // = 2130969239;
    @DexIgnore
    public static /* final */ int drawableTintMode; // = 2130969240;
    @DexIgnore
    public static /* final */ int drawableTopCompat; // = 2130969241;
    @DexIgnore
    public static /* final */ int drawerArrowStyle; // = 2130969242;
    @DexIgnore
    public static /* final */ int dropDownListViewStyle; // = 2130969243;
    @DexIgnore
    public static /* final */ int dropdownListPreferredItemHeight; // = 2130969244;
    @DexIgnore
    public static /* final */ int editTextBackground; // = 2130969247;
    @DexIgnore
    public static /* final */ int editTextColor; // = 2130969248;
    @DexIgnore
    public static /* final */ int editTextStyle; // = 2130969250;
    @DexIgnore
    public static /* final */ int elevation; // = 2130969258;
    @DexIgnore
    public static /* final */ int elevationOverlayColor; // = 2130969260;
    @DexIgnore
    public static /* final */ int elevationOverlayEnabled; // = 2130969261;
    @DexIgnore
    public static /* final */ int endIconCheckable; // = 2130969268;
    @DexIgnore
    public static /* final */ int endIconContentDescription; // = 2130969269;
    @DexIgnore
    public static /* final */ int endIconDrawable; // = 2130969270;
    @DexIgnore
    public static /* final */ int endIconMode; // = 2130969271;
    @DexIgnore
    public static /* final */ int endIconTint; // = 2130969272;
    @DexIgnore
    public static /* final */ int endIconTintMode; // = 2130969273;
    @DexIgnore
    public static /* final */ int enforceMaterialTheme; // = 2130969274;
    @DexIgnore
    public static /* final */ int enforceTextAppearance; // = 2130969275;
    @DexIgnore
    public static /* final */ int ensureMinTouchTargetSize; // = 2130969276;
    @DexIgnore
    public static /* final */ int errorEnabled; // = 2130969277;
    @DexIgnore
    public static /* final */ int errorIconDrawable; // = 2130969278;
    @DexIgnore
    public static /* final */ int errorIconTint; // = 2130969279;
    @DexIgnore
    public static /* final */ int errorIconTintMode; // = 2130969280;
    @DexIgnore
    public static /* final */ int errorTextAppearance; // = 2130969282;
    @DexIgnore
    public static /* final */ int errorTextColor; // = 2130969283;
    @DexIgnore
    public static /* final */ int expandActivityOverflowButtonDrawable; // = 2130969288;
    @DexIgnore
    public static /* final */ int expanded; // = 2130969289;
    @DexIgnore
    public static /* final */ int expandedTitleGravity; // = 2130969290;
    @DexIgnore
    public static /* final */ int expandedTitleMargin; // = 2130969291;
    @DexIgnore
    public static /* final */ int expandedTitleMarginBottom; // = 2130969292;
    @DexIgnore
    public static /* final */ int expandedTitleMarginEnd; // = 2130969293;
    @DexIgnore
    public static /* final */ int expandedTitleMarginStart; // = 2130969294;
    @DexIgnore
    public static /* final */ int expandedTitleMarginTop; // = 2130969295;
    @DexIgnore
    public static /* final */ int expandedTitleTextAppearance; // = 2130969296;
    @DexIgnore
    public static /* final */ int extendMotionSpec; // = 2130969297;
    @DexIgnore
    public static /* final */ int extendedFloatingActionButtonStyle; // = 2130969298;
    @DexIgnore
    public static /* final */ int fabAlignmentMode; // = 2130969299;
    @DexIgnore
    public static /* final */ int fabAnimationMode; // = 2130969300;
    @DexIgnore
    public static /* final */ int fabCradleMargin; // = 2130969301;
    @DexIgnore
    public static /* final */ int fabCradleRoundedCornerRadius; // = 2130969302;
    @DexIgnore
    public static /* final */ int fabCradleVerticalOffset; // = 2130969303;
    @DexIgnore
    public static /* final */ int fabCustomSize; // = 2130969304;
    @DexIgnore
    public static /* final */ int fabSize; // = 2130969305;
    @DexIgnore
    public static /* final */ int fastScrollEnabled; // = 2130969306;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalThumbDrawable; // = 2130969307;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalTrackDrawable; // = 2130969308;
    @DexIgnore
    public static /* final */ int fastScrollVerticalThumbDrawable; // = 2130969309;
    @DexIgnore
    public static /* final */ int fastScrollVerticalTrackDrawable; // = 2130969310;
    @DexIgnore
    public static /* final */ int firstBaselineToTopHeight; // = 2130969315;
    @DexIgnore
    public static /* final */ int floatingActionButtonStyle; // = 2130969325;
    @DexIgnore
    public static /* final */ int font; // = 2130969326;
    @DexIgnore
    public static /* final */ int fontFamily; // = 2130969327;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969331;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969332;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969333;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969334;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969335;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969336;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969337;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969338;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969339;
    @DexIgnore
    public static /* final */ int foregroundInsidePadding; // = 2130969340;
    @DexIgnore
    public static /* final */ int gapBetweenBars; // = 2130969346;
    @DexIgnore
    public static /* final */ int goIcon; // = 2130969348;
    @DexIgnore
    public static /* final */ int headerLayout; // = 2130969364;
    @DexIgnore
    public static /* final */ int height; // = 2130969379;
    @DexIgnore
    public static /* final */ int helperText; // = 2130969382;
    @DexIgnore
    public static /* final */ int helperTextEnabled; // = 2130969383;
    @DexIgnore
    public static /* final */ int helperTextTextAppearance; // = 2130969384;
    @DexIgnore
    public static /* final */ int helperTextTextColor; // = 2130969385;
    @DexIgnore
    public static /* final */ int hideMotionSpec; // = 2130969386;
    @DexIgnore
    public static /* final */ int hideOnContentScroll; // = 2130969387;
    @DexIgnore
    public static /* final */ int hideOnScroll; // = 2130969388;
    @DexIgnore
    public static /* final */ int hintAnimationEnabled; // = 2130969389;
    @DexIgnore
    public static /* final */ int hintEnabled; // = 2130969391;
    @DexIgnore
    public static /* final */ int hintTextAppearance; // = 2130969392;
    @DexIgnore
    public static /* final */ int hintTextColor; // = 2130969393;
    @DexIgnore
    public static /* final */ int homeAsUpIndicator; // = 2130969395;
    @DexIgnore
    public static /* final */ int homeLayout; // = 2130969396;
    @DexIgnore
    public static /* final */ int hoveredFocusedTranslationZ; // = 2130969397;
    @DexIgnore
    public static /* final */ int icon; // = 2130969398;
    @DexIgnore
    public static /* final */ int iconEndPadding; // = 2130969399;
    @DexIgnore
    public static /* final */ int iconGravity; // = 2130969400;
    @DexIgnore
    public static /* final */ int iconPadding; // = 2130969401;
    @DexIgnore
    public static /* final */ int iconSize; // = 2130969402;
    @DexIgnore
    public static /* final */ int iconStartPadding; // = 2130969403;
    @DexIgnore
    public static /* final */ int iconTint; // = 2130969404;
    @DexIgnore
    public static /* final */ int iconTintMode; // = 2130969405;
    @DexIgnore
    public static /* final */ int iconifiedByDefault; // = 2130969411;
    @DexIgnore
    public static /* final */ int imageButtonStyle; // = 2130969415;
    @DexIgnore
    public static /* final */ int indeterminateProgressStyle; // = 2130969420;
    @DexIgnore
    public static /* final */ int initialActivityCount; // = 2130969431;
    @DexIgnore
    public static /* final */ int insetForeground; // = 2130969432;
    @DexIgnore
    public static /* final */ int isLightTheme; // = 2130969440;
    @DexIgnore
    public static /* final */ int isMaterialTheme; // = 2130969441;
    @DexIgnore
    public static /* final */ int itemBackground; // = 2130969446;
    @DexIgnore
    public static /* final */ int itemFillColor; // = 2130969447;
    @DexIgnore
    public static /* final */ int itemHorizontalPadding; // = 2130969448;
    @DexIgnore
    public static /* final */ int itemHorizontalTranslationEnabled; // = 2130969449;
    @DexIgnore
    public static /* final */ int itemIconPadding; // = 2130969450;
    @DexIgnore
    public static /* final */ int itemIconSize; // = 2130969451;
    @DexIgnore
    public static /* final */ int itemIconTint; // = 2130969452;
    @DexIgnore
    public static /* final */ int itemMaxLines; // = 2130969453;
    @DexIgnore
    public static /* final */ int itemPadding; // = 2130969454;
    @DexIgnore
    public static /* final */ int itemRippleColor; // = 2130969455;
    @DexIgnore
    public static /* final */ int itemShapeAppearance; // = 2130969456;
    @DexIgnore
    public static /* final */ int itemShapeAppearanceOverlay; // = 2130969457;
    @DexIgnore
    public static /* final */ int itemShapeFillColor; // = 2130969458;
    @DexIgnore
    public static /* final */ int itemShapeInsetBottom; // = 2130969459;
    @DexIgnore
    public static /* final */ int itemShapeInsetEnd; // = 2130969460;
    @DexIgnore
    public static /* final */ int itemShapeInsetStart; // = 2130969461;
    @DexIgnore
    public static /* final */ int itemShapeInsetTop; // = 2130969462;
    @DexIgnore
    public static /* final */ int itemSpacing; // = 2130969463;
    @DexIgnore
    public static /* final */ int itemStrokeColor; // = 2130969464;
    @DexIgnore
    public static /* final */ int itemStrokeWidth; // = 2130969465;
    @DexIgnore
    public static /* final */ int itemTextAppearance; // = 2130969466;
    @DexIgnore
    public static /* final */ int itemTextAppearanceActive; // = 2130969467;
    @DexIgnore
    public static /* final */ int itemTextAppearanceInactive; // = 2130969468;
    @DexIgnore
    public static /* final */ int itemTextColor; // = 2130969469;
    @DexIgnore
    public static /* final */ int keylines; // = 2130969470;
    @DexIgnore
    public static /* final */ int labelVisibilityMode; // = 2130969471;
    @DexIgnore
    public static /* final */ int lastBaselineToBottomHeight; // = 2130969472;
    @DexIgnore
    public static /* final */ int layout; // = 2130969477;
    @DexIgnore
    public static /* final */ int layoutManager; // = 2130969478;
    @DexIgnore
    public static /* final */ int layout_anchor; // = 2130969479;
    @DexIgnore
    public static /* final */ int layout_anchorGravity; // = 2130969480;
    @DexIgnore
    public static /* final */ int layout_behavior; // = 2130969482;
    @DexIgnore
    public static /* final */ int layout_collapseMode; // = 2130969483;
    @DexIgnore
    public static /* final */ int layout_collapseParallaxMultiplier; // = 2130969484;
    @DexIgnore
    public static /* final */ int layout_dodgeInsetEdges; // = 2130969526;
    @DexIgnore
    public static /* final */ int layout_insetEdge; // = 2130969536;
    @DexIgnore
    public static /* final */ int layout_keyline; // = 2130969537;
    @DexIgnore
    public static /* final */ int layout_scrollFlags; // = 2130969546;
    @DexIgnore
    public static /* final */ int layout_scrollInterpolator; // = 2130969547;
    @DexIgnore
    public static /* final */ int liftOnScroll; // = 2130969550;
    @DexIgnore
    public static /* final */ int liftOnScrollTargetViewId; // = 2130969551;
    @DexIgnore
    public static /* final */ int lineHeight; // = 2130969554;
    @DexIgnore
    public static /* final */ int lineSpacing; // = 2130969555;
    @DexIgnore
    public static /* final */ int listChoiceBackgroundIndicator; // = 2130969557;
    @DexIgnore
    public static /* final */ int listChoiceIndicatorMultipleAnimated; // = 2130969558;
    @DexIgnore
    public static /* final */ int listChoiceIndicatorSingleAnimated; // = 2130969559;
    @DexIgnore
    public static /* final */ int listDividerAlertDialog; // = 2130969560;
    @DexIgnore
    public static /* final */ int listItemLayout; // = 2130969561;
    @DexIgnore
    public static /* final */ int listLayout; // = 2130969562;
    @DexIgnore
    public static /* final */ int listMenuViewStyle; // = 2130969563;
    @DexIgnore
    public static /* final */ int listPopupWindowStyle; // = 2130969564;
    @DexIgnore
    public static /* final */ int listPreferredItemHeight; // = 2130969565;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightLarge; // = 2130969566;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightSmall; // = 2130969567;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingEnd; // = 2130969568;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingLeft; // = 2130969569;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingRight; // = 2130969570;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingStart; // = 2130969571;
    @DexIgnore
    public static /* final */ int logo; // = 2130969576;
    @DexIgnore
    public static /* final */ int logoDescription; // = 2130969577;
    @DexIgnore
    public static /* final */ int materialAlertDialogBodyTextStyle; // = 2130969590;
    @DexIgnore
    public static /* final */ int materialAlertDialogTheme; // = 2130969591;
    @DexIgnore
    public static /* final */ int materialAlertDialogTitleIconStyle; // = 2130969592;
    @DexIgnore
    public static /* final */ int materialAlertDialogTitlePanelStyle; // = 2130969593;
    @DexIgnore
    public static /* final */ int materialAlertDialogTitleTextStyle; // = 2130969594;
    @DexIgnore
    public static /* final */ int materialButtonOutlinedStyle; // = 2130969595;
    @DexIgnore
    public static /* final */ int materialButtonStyle; // = 2130969596;
    @DexIgnore
    public static /* final */ int materialButtonToggleGroupStyle; // = 2130969597;
    @DexIgnore
    public static /* final */ int materialCalendarDay; // = 2130969598;
    @DexIgnore
    public static /* final */ int materialCalendarFullscreenTheme; // = 2130969599;
    @DexIgnore
    public static /* final */ int materialCalendarHeaderConfirmButton; // = 2130969600;
    @DexIgnore
    public static /* final */ int materialCalendarHeaderDivider; // = 2130969601;
    @DexIgnore
    public static /* final */ int materialCalendarHeaderLayout; // = 2130969602;
    @DexIgnore
    public static /* final */ int materialCalendarHeaderSelection; // = 2130969603;
    @DexIgnore
    public static /* final */ int materialCalendarHeaderTitle; // = 2130969604;
    @DexIgnore
    public static /* final */ int materialCalendarHeaderToggleButton; // = 2130969605;
    @DexIgnore
    public static /* final */ int materialCalendarStyle; // = 2130969606;
    @DexIgnore
    public static /* final */ int materialCalendarTheme; // = 2130969607;
    @DexIgnore
    public static /* final */ int materialCardViewStyle; // = 2130969608;
    @DexIgnore
    public static /* final */ int materialThemeOverlay; // = 2130969609;
    @DexIgnore
    public static /* final */ int maxActionInlineWidth; // = 2130969610;
    @DexIgnore
    public static /* final */ int maxButtonHeight; // = 2130969611;
    @DexIgnore
    public static /* final */ int maxCharacterCount; // = 2130969612;
    @DexIgnore
    public static /* final */ int maxImageSize; // = 2130969613;
    @DexIgnore
    public static /* final */ int measureWithLargestChild; // = 2130969616;
    @DexIgnore
    public static /* final */ int menu; // = 2130969617;
    @DexIgnore
    public static /* final */ int minTouchTargetSize; // = 2130969627;
    @DexIgnore
    public static /* final */ int multiChoiceItemLayout; // = 2130969630;
    @DexIgnore
    public static /* final */ int navigationContentDescription; // = 2130969635;
    @DexIgnore
    public static /* final */ int navigationIcon; // = 2130969636;
    @DexIgnore
    public static /* final */ int navigationMode; // = 2130969637;
    @DexIgnore
    public static /* final */ int navigationViewStyle; // = 2130969639;
    @DexIgnore
    public static /* final */ int number; // = 2130969647;
    @DexIgnore
    public static /* final */ int numericModifiers; // = 2130969657;
    @DexIgnore
    public static /* final */ int overlapAnchor; // = 2130969658;
    @DexIgnore
    public static /* final */ int paddingBottomNoButtons; // = 2130969659;
    @DexIgnore
    public static /* final */ int paddingEnd; // = 2130969660;
    @DexIgnore
    public static /* final */ int paddingStart; // = 2130969662;
    @DexIgnore
    public static /* final */ int paddingTopNoTitle; // = 2130969663;
    @DexIgnore
    public static /* final */ int panelBackground; // = 2130969665;
    @DexIgnore
    public static /* final */ int panelMenuListTheme; // = 2130969666;
    @DexIgnore
    public static /* final */ int panelMenuListWidth; // = 2130969667;
    @DexIgnore
    public static /* final */ int passwordToggleContentDescription; // = 2130969669;
    @DexIgnore
    public static /* final */ int passwordToggleDrawable; // = 2130969670;
    @DexIgnore
    public static /* final */ int passwordToggleEnabled; // = 2130969671;
    @DexIgnore
    public static /* final */ int passwordToggleTint; // = 2130969672;
    @DexIgnore
    public static /* final */ int passwordToggleTintMode; // = 2130969673;
    @DexIgnore
    public static /* final */ int popupMenuBackground; // = 2130969675;
    @DexIgnore
    public static /* final */ int popupMenuStyle; // = 2130969676;
    @DexIgnore
    public static /* final */ int popupTheme; // = 2130969677;
    @DexIgnore
    public static /* final */ int popupWindowStyle; // = 2130969678;
    @DexIgnore
    public static /* final */ int preserveIconSpacing; // = 2130969679;
    @DexIgnore
    public static /* final */ int pressedTranslationZ; // = 2130969680;
    @DexIgnore
    public static /* final */ int progressBarPadding; // = 2130969689;
    @DexIgnore
    public static /* final */ int progressBarStyle; // = 2130969690;
    @DexIgnore
    public static /* final */ int queryBackground; // = 2130969703;
    @DexIgnore
    public static /* final */ int queryHint; // = 2130969704;
    @DexIgnore
    public static /* final */ int radioButtonStyle; // = 2130969705;
    @DexIgnore
    public static /* final */ int rangeFillColor; // = 2130969707;
    @DexIgnore
    public static /* final */ int ratingBarStyle; // = 2130969708;
    @DexIgnore
    public static /* final */ int ratingBarStyleIndicator; // = 2130969709;
    @DexIgnore
    public static /* final */ int ratingBarStyleSmall; // = 2130969710;
    @DexIgnore
    public static /* final */ int recyclerViewStyle; // = 2130969717;
    @DexIgnore
    public static /* final */ int reverseLayout; // = 2130969725;
    @DexIgnore
    public static /* final */ int rippleColor; // = 2130969732;
    @DexIgnore
    public static /* final */ int scrimAnimationDuration; // = 2130969753;
    @DexIgnore
    public static /* final */ int scrimBackground; // = 2130969754;
    @DexIgnore
    public static /* final */ int scrimVisibleHeightTrigger; // = 2130969755;
    @DexIgnore
    public static /* final */ int searchHintIcon; // = 2130969758;
    @DexIgnore
    public static /* final */ int searchIcon; // = 2130969759;
    @DexIgnore
    public static /* final */ int searchViewStyle; // = 2130969761;
    @DexIgnore
    public static /* final */ int seekBarStyle; // = 2130969762;
    @DexIgnore
    public static /* final */ int selectableItemBackground; // = 2130969763;
    @DexIgnore
    public static /* final */ int selectableItemBackgroundBorderless; // = 2130969764;
    @DexIgnore
    public static /* final */ int shapeAppearance; // = 2130969786;
    @DexIgnore
    public static /* final */ int shapeAppearanceLargeComponent; // = 2130969787;
    @DexIgnore
    public static /* final */ int shapeAppearanceMediumComponent; // = 2130969788;
    @DexIgnore
    public static /* final */ int shapeAppearanceOverlay; // = 2130969789;
    @DexIgnore
    public static /* final */ int shapeAppearanceSmallComponent; // = 2130969790;
    @DexIgnore
    public static /* final */ int showAsAction; // = 2130969792;
    @DexIgnore
    public static /* final */ int showDividers; // = 2130969793;
    @DexIgnore
    public static /* final */ int showMotionSpec; // = 2130969794;
    @DexIgnore
    public static /* final */ int showText; // = 2130969795;
    @DexIgnore
    public static /* final */ int showTitle; // = 2130969796;
    @DexIgnore
    public static /* final */ int shrinkMotionSpec; // = 2130969797;
    @DexIgnore
    public static /* final */ int singleChoiceItemLayout; // = 2130969798;
    @DexIgnore
    public static /* final */ int singleLine; // = 2130969799;
    @DexIgnore
    public static /* final */ int singleSelection; // = 2130969800;
    @DexIgnore
    public static /* final */ int snackbarButtonStyle; // = 2130969858;
    @DexIgnore
    public static /* final */ int snackbarStyle; // = 2130969859;
    @DexIgnore
    public static /* final */ int spanCount; // = 2130969863;
    @DexIgnore
    public static /* final */ int spinBars; // = 2130969864;
    @DexIgnore
    public static /* final */ int spinnerDropDownItemStyle; // = 2130969865;
    @DexIgnore
    public static /* final */ int spinnerStyle; // = 2130969866;
    @DexIgnore
    public static /* final */ int splitTrack; // = 2130969867;
    @DexIgnore
    public static /* final */ int srcCompat; // = 2130969868;
    @DexIgnore
    public static /* final */ int stackFromEnd; // = 2130969870;
    @DexIgnore
    public static /* final */ int startIconCheckable; // = 2130969871;
    @DexIgnore
    public static /* final */ int startIconContentDescription; // = 2130969872;
    @DexIgnore
    public static /* final */ int startIconDrawable; // = 2130969873;
    @DexIgnore
    public static /* final */ int startIconTint; // = 2130969874;
    @DexIgnore
    public static /* final */ int startIconTintMode; // = 2130969875;
    @DexIgnore
    public static /* final */ int state_above_anchor; // = 2130969877;
    @DexIgnore
    public static /* final */ int state_collapsed; // = 2130969878;
    @DexIgnore
    public static /* final */ int state_collapsible; // = 2130969879;
    @DexIgnore
    public static /* final */ int state_dragged; // = 2130969880;
    @DexIgnore
    public static /* final */ int state_liftable; // = 2130969881;
    @DexIgnore
    public static /* final */ int state_lifted; // = 2130969882;
    @DexIgnore
    public static /* final */ int statusBarBackground; // = 2130969888;
    @DexIgnore
    public static /* final */ int statusBarForeground; // = 2130969889;
    @DexIgnore
    public static /* final */ int statusBarScrim; // = 2130969890;
    @DexIgnore
    public static /* final */ int strokeColor; // = 2130969892;
    @DexIgnore
    public static /* final */ int strokeWidth; // = 2130969896;
    @DexIgnore
    public static /* final */ int subMenuArrow; // = 2130969899;
    @DexIgnore
    public static /* final */ int submitBackground; // = 2130969901;
    @DexIgnore
    public static /* final */ int subtitle; // = 2130969902;
    @DexIgnore
    public static /* final */ int subtitleTextAppearance; // = 2130969903;
    @DexIgnore
    public static /* final */ int subtitleTextColor; // = 2130969904;
    @DexIgnore
    public static /* final */ int subtitleTextStyle; // = 2130969905;
    @DexIgnore
    public static /* final */ int suggestionRowLayout; // = 2130969906;
    @DexIgnore
    public static /* final */ int switchMinWidth; // = 2130969910;
    @DexIgnore
    public static /* final */ int switchPadding; // = 2130969911;
    @DexIgnore
    public static /* final */ int switchStyle; // = 2130969912;
    @DexIgnore
    public static /* final */ int switchTextAppearance; // = 2130969913;
    @DexIgnore
    public static /* final */ int tabBackground; // = 2130969917;
    @DexIgnore
    public static /* final */ int tabContentStart; // = 2130969918;
    @DexIgnore
    public static /* final */ int tabGravity; // = 2130969919;
    @DexIgnore
    public static /* final */ int tabIconTint; // = 2130969920;
    @DexIgnore
    public static /* final */ int tabIconTintMode; // = 2130969921;
    @DexIgnore
    public static /* final */ int tabIndicator; // = 2130969922;
    @DexIgnore
    public static /* final */ int tabIndicatorAnimationDuration; // = 2130969923;
    @DexIgnore
    public static /* final */ int tabIndicatorColor; // = 2130969924;
    @DexIgnore
    public static /* final */ int tabIndicatorFullWidth; // = 2130969925;
    @DexIgnore
    public static /* final */ int tabIndicatorGravity; // = 2130969926;
    @DexIgnore
    public static /* final */ int tabIndicatorHeight; // = 2130969927;
    @DexIgnore
    public static /* final */ int tabInlineLabel; // = 2130969928;
    @DexIgnore
    public static /* final */ int tabMaxWidth; // = 2130969929;
    @DexIgnore
    public static /* final */ int tabMinWidth; // = 2130969930;
    @DexIgnore
    public static /* final */ int tabMode; // = 2130969931;
    @DexIgnore
    public static /* final */ int tabPadding; // = 2130969932;
    @DexIgnore
    public static /* final */ int tabPaddingBottom; // = 2130969933;
    @DexIgnore
    public static /* final */ int tabPaddingEnd; // = 2130969934;
    @DexIgnore
    public static /* final */ int tabPaddingStart; // = 2130969935;
    @DexIgnore
    public static /* final */ int tabPaddingTop; // = 2130969936;
    @DexIgnore
    public static /* final */ int tabRippleColor; // = 2130969937;
    @DexIgnore
    public static /* final */ int tabSelectedTextColor; // = 2130969938;
    @DexIgnore
    public static /* final */ int tabStyle; // = 2130969939;
    @DexIgnore
    public static /* final */ int tabTextAppearance; // = 2130969940;
    @DexIgnore
    public static /* final */ int tabTextColor; // = 2130969941;
    @DexIgnore
    public static /* final */ int tabUnboundedRipple; // = 2130969942;
    @DexIgnore
    public static /* final */ int textAllCaps; // = 2130969952;
    @DexIgnore
    public static /* final */ int textAppearanceBody1; // = 2130969953;
    @DexIgnore
    public static /* final */ int textAppearanceBody2; // = 2130969954;
    @DexIgnore
    public static /* final */ int textAppearanceButton; // = 2130969955;
    @DexIgnore
    public static /* final */ int textAppearanceCaption; // = 2130969956;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline1; // = 2130969957;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline2; // = 2130969958;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline3; // = 2130969959;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline4; // = 2130969960;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline5; // = 2130969961;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline6; // = 2130969962;
    @DexIgnore
    public static /* final */ int textAppearanceLargePopupMenu; // = 2130969963;
    @DexIgnore
    public static /* final */ int textAppearanceLineHeightEnabled; // = 2130969964;
    @DexIgnore
    public static /* final */ int textAppearanceListItem; // = 2130969965;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSecondary; // = 2130969966;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSmall; // = 2130969967;
    @DexIgnore
    public static /* final */ int textAppearanceOverline; // = 2130969968;
    @DexIgnore
    public static /* final */ int textAppearancePopupMenuHeader; // = 2130969969;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultSubtitle; // = 2130969970;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultTitle; // = 2130969971;
    @DexIgnore
    public static /* final */ int textAppearanceSmallPopupMenu; // = 2130969972;
    @DexIgnore
    public static /* final */ int textAppearanceSubtitle1; // = 2130969973;
    @DexIgnore
    public static /* final */ int textAppearanceSubtitle2; // = 2130969974;
    @DexIgnore
    public static /* final */ int textColorAlertDialogListItem; // = 2130969984;
    @DexIgnore
    public static /* final */ int textColorSearchUrl; // = 2130969985;
    @DexIgnore
    public static /* final */ int textEndPadding; // = 2130969988;
    @DexIgnore
    public static /* final */ int textInputStyle; // = 2130969996;
    @DexIgnore
    public static /* final */ int textLocale; // = 2130969997;
    @DexIgnore
    public static /* final */ int textStartPadding; // = 2130970000;
    @DexIgnore
    public static /* final */ int theme; // = 2130970013;
    @DexIgnore
    public static /* final */ int themeLineHeight; // = 2130970014;
    @DexIgnore
    public static /* final */ int thickness; // = 2130970016;
    @DexIgnore
    public static /* final */ int thumbTextPadding; // = 2130970017;
    @DexIgnore
    public static /* final */ int thumbTint; // = 2130970018;
    @DexIgnore
    public static /* final */ int thumbTintMode; // = 2130970019;
    @DexIgnore
    public static /* final */ int tickMark; // = 2130970024;
    @DexIgnore
    public static /* final */ int tickMarkTint; // = 2130970025;
    @DexIgnore
    public static /* final */ int tickMarkTintMode; // = 2130970026;
    @DexIgnore
    public static /* final */ int tint; // = 2130970028;
    @DexIgnore
    public static /* final */ int tintMode; // = 2130970029;
    @DexIgnore
    public static /* final */ int title; // = 2130970031;
    @DexIgnore
    public static /* final */ int titleEnabled; // = 2130970032;
    @DexIgnore
    public static /* final */ int titleMargin; // = 2130970033;
    @DexIgnore
    public static /* final */ int titleMarginBottom; // = 2130970034;
    @DexIgnore
    public static /* final */ int titleMarginEnd; // = 2130970035;
    @DexIgnore
    public static /* final */ int titleMarginStart; // = 2130970036;
    @DexIgnore
    public static /* final */ int titleMarginTop; // = 2130970037;
    @DexIgnore
    public static /* final */ int titleMargins; // = 2130970038;
    @DexIgnore
    public static /* final */ int titleTextAppearance; // = 2130970039;
    @DexIgnore
    public static /* final */ int titleTextColor; // = 2130970040;
    @DexIgnore
    public static /* final */ int titleTextStyle; // = 2130970041;
    @DexIgnore
    public static /* final */ int toolbarId; // = 2130970048;
    @DexIgnore
    public static /* final */ int toolbarNavigationButtonStyle; // = 2130970049;
    @DexIgnore
    public static /* final */ int toolbarStyle; // = 2130970050;
    @DexIgnore
    public static /* final */ int tooltipForegroundColor; // = 2130970051;
    @DexIgnore
    public static /* final */ int tooltipFrameBackground; // = 2130970052;
    @DexIgnore
    public static /* final */ int tooltipText; // = 2130970053;
    @DexIgnore
    public static /* final */ int track; // = 2130970055;
    @DexIgnore
    public static /* final */ int trackTint; // = 2130970056;
    @DexIgnore
    public static /* final */ int trackTintMode; // = 2130970057;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130970059;
    @DexIgnore
    public static /* final */ int useCompatPadding; // = 2130970076;
    @DexIgnore
    public static /* final */ int useMaterialThemeColors; // = 2130970077;
    @DexIgnore
    public static /* final */ int viewInflaterClass; // = 2130970079;
    @DexIgnore
    public static /* final */ int voiceIcon; // = 2130970095;
    @DexIgnore
    public static /* final */ int windowActionBar; // = 2130970162;
    @DexIgnore
    public static /* final */ int windowActionBarOverlay; // = 2130970163;
    @DexIgnore
    public static /* final */ int windowActionModeOverlay; // = 2130970164;
    @DexIgnore
    public static /* final */ int windowFixedHeightMajor; // = 2130970165;
    @DexIgnore
    public static /* final */ int windowFixedHeightMinor; // = 2130970166;
    @DexIgnore
    public static /* final */ int windowFixedWidthMajor; // = 2130970167;
    @DexIgnore
    public static /* final */ int windowFixedWidthMinor; // = 2130970168;
    @DexIgnore
    public static /* final */ int windowMinWidthMajor; // = 2130970169;
    @DexIgnore
    public static /* final */ int windowMinWidthMinor; // = 2130970170;
    @DexIgnore
    public static /* final */ int windowNoTitle; // = 2130970171;
    @DexIgnore
    public static /* final */ int yearSelectedStyle; // = 2130970172;
    @DexIgnore
    public static /* final */ int yearStyle; // = 2130970173;
    @DexIgnore
    public static /* final */ int yearTodayStyle; // = 2130970174;
}
