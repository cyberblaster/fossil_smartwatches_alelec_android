package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j53 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ h53 e;

    @DexIgnore
    public j53(h53 h53, String str, boolean z) {
        this.e = h53;
        w12.b(str);
        this.a = str;
        this.b = z;
    }

    @DexIgnore
    public final boolean a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.A().getBoolean(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void a(boolean z) {
        SharedPreferences.Editor edit = this.e.A().edit();
        edit.putBoolean(this.a, z);
        edit.apply();
        this.d = z;
    }
}
