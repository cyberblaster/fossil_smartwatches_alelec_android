package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ ue1 a;

    @DexIgnore
    public ek1(ue1 ue1) {
        this.a = ue1;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == 2084501365 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED")) {
            mw0 a2 = mw0.e.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", 10));
            mw0 a3 = mw0.e.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", 10));
            if (wg6.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE"), this.a.w)) {
                ue1 ue1 = this.a;
                ue1.a.post(new bt0(ue1, new t31(x11.SUCCESS, 0, 2), a2, a3));
                ue1 ue12 = this.a;
                ue12.a.post(new k71(ue12, new t31(x11.SUCCESS, 0, 2), a2, a3));
            }
        }
    }
}
