package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.dumpapp.DumpappHttpSocketLikeHandler;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p30 extends r86 implements d30 {
    @DexIgnore
    public p30(i86 i86, String str, String str2, va6 va6) {
        super(i86, str, str2, va6, ta6.POST);
    }

    @DexIgnore
    public boolean a(c30 c30) {
        ua6 a = a();
        a(a, c30.a);
        a(a, c30.b);
        l86 g = c86.g();
        g.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a.g();
        l86 g3 = c86.g();
        g3.d("CrashlyticsCore", "Result was: " + g2);
        return m96.a(g2) == 0;
    }

    @DexIgnore
    public final ua6 a(ua6 ua6, String str) {
        ua6.c(GraphRequest.USER_AGENT_HEADER, "Crashlytics Android SDK/" + this.e.j());
        ua6.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        ua6.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.j());
        ua6.c("X-CRASHLYTICS-API-KEY", str);
        return ua6;
    }

    @DexIgnore
    public final ua6 a(ua6 ua6, y30 y30) {
        ua6.e("report_id", y30.b());
        for (File file : y30.d()) {
            if (file.getName().equals("minidump")) {
                ua6.a("minidump_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("metadata")) {
                ua6.a("crash_meta_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("binaryImages")) {
                ua6.a("binary_images_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("session")) {
                ua6.a("session_meta_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("app")) {
                ua6.a("app_meta_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals(DeviceRequestsHelper.DEVICE_INFO_DEVICE)) {
                ua6.a("device_meta_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("os")) {
                ua6.a("os_meta_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("user")) {
                ua6.a("user_meta_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("logs")) {
                ua6.a("logs_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            } else if (file.getName().equals("keys")) {
                ua6.a("keys_file", file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            }
        }
        return ua6;
    }
}
