package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dk1 implements Parcelable.Creator<wl1> {
    @DexIgnore
    public /* synthetic */ dk1(qg6 qg6) {
    }

    @DexIgnore
    public wl1 createFromParcel(Parcel parcel) {
        return new wl1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new wl1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m11createFromParcel(Parcel parcel) {
        return new wl1(parcel, (qg6) null);
    }
}
