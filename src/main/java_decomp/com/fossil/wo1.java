package com.fossil;

import com.fossil.cp1;
import com.fossil.yw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo1 extends yw3<wo1, b> implements xo1 {
    @DexIgnore
    public static /* final */ wo1 p; // = new wo1();
    @DexIgnore
    public static volatile gx3<wo1> q;
    @DexIgnore
    public long d;
    @DexIgnore
    public int e;
    @DexIgnore
    public long f;
    @DexIgnore
    public sw3 g; // = sw3.EMPTY;
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public String i; // = "";
    @DexIgnore
    public long j;
    @DexIgnore
    public cp1 o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<wo1, b> implements xo1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b a(long j) {
            d();
            ((wo1) this.b).d = j;
            return this;
        }

        @DexIgnore
        public b b(long j) {
            d();
            ((wo1) this.b).f = j;
            return this;
        }

        @DexIgnore
        public b c(long j) {
            d();
            ((wo1) this.b).j = j;
            return this;
        }

        @DexIgnore
        public b() {
            super(wo1.p);
        }

        @DexIgnore
        public b a(int i) {
            d();
            ((wo1) this.b).e = i;
            return this;
        }

        @DexIgnore
        public b a(sw3 sw3) {
            d();
            wo1.a((wo1) this.b, sw3);
            return this;
        }

        @DexIgnore
        public b a(cp1.b bVar) {
            d();
            ((wo1) this.b).a(bVar);
            return this;
        }
    }

    /*
    static {
        p.g();
    }
    */

    @DexIgnore
    public static b k() {
        return (b) p.c();
    }

    @DexIgnore
    public static gx3<wo1> l() {
        return p.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new wo1();
            case 2:
                return p;
            case 3:
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                wo1 wo1 = (wo1) obj2;
                this.d = kVar.a(this.d != 0, this.d, wo1.d != 0, wo1.d);
                this.e = kVar.a(this.e != 0, this.e, wo1.e != 0, wo1.e);
                this.f = kVar.a(this.f != 0, this.f, wo1.f != 0, wo1.f);
                this.g = kVar.a(this.g != sw3.EMPTY, this.g, wo1.g != sw3.EMPTY, wo1.g);
                this.h = kVar.a(!this.h.isEmpty(), this.h, !wo1.h.isEmpty(), wo1.h);
                this.i = kVar.a(!this.i.isEmpty(), this.i, !wo1.i.isEmpty(), wo1.i);
                this.j = kVar.a(this.j != 0, this.j, wo1.j != 0, wo1.j);
                this.o = (cp1) kVar.a(this.o, wo1.o);
                yw3.i iVar = yw3.i.a;
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 8) {
                                this.d = tw3.e();
                            } else if (l == 50) {
                                this.g = tw3.b();
                            } else if (l == 66) {
                                this.h = tw3.k();
                            } else if (l == 88) {
                                this.e = tw3.d();
                            } else if (l == 106) {
                                this.i = tw3.k();
                            } else if (l == 120) {
                                this.j = tw3.j();
                            } else if (l == 136) {
                                this.f = tw3.e();
                            } else if (l == 186) {
                                cp1.b bVar = this.o != null ? (cp1.b) this.o.c() : null;
                                this.o = (cp1) tw3.a(cp1.m(), ww3);
                                if (bVar != null) {
                                    bVar.b(this.o);
                                    this.o = (cp1) bVar.c();
                                }
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (q == null) {
                    synchronized (wo1.class) {
                        if (q == null) {
                            q = new yw3.c(p);
                        }
                    }
                }
                return q;
            default:
                throw new UnsupportedOperationException();
        }
        return p;
    }

    @DexIgnore
    public int d() {
        int i2 = this.c;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        long j2 = this.d;
        if (j2 != 0) {
            i3 = 0 + uw3.d(1, j2);
        }
        if (!this.g.isEmpty()) {
            i3 += uw3.b(6, this.g);
        }
        if (!this.h.isEmpty()) {
            i3 += uw3.b(8, this.h);
        }
        int i4 = this.e;
        if (i4 != 0) {
            i3 += uw3.d(11, i4);
        }
        if (!this.i.isEmpty()) {
            i3 += uw3.b(13, this.i);
        }
        long j3 = this.j;
        if (j3 != 0) {
            i3 += uw3.e(15, j3);
        }
        long j4 = this.f;
        if (j4 != 0) {
            i3 += uw3.d(17, j4);
        }
        cp1 cp1 = this.o;
        if (cp1 != null) {
            if (cp1 == null) {
                cp1 = cp1.k();
            }
            i3 += uw3.b(23, (dx3) cp1);
        }
        this.c = i3;
        return i3;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        long j2 = this.d;
        if (j2 != 0) {
            uw3.a(1, j2);
        }
        if (!this.g.isEmpty()) {
            uw3.a(6, this.g);
        }
        if (!this.h.isEmpty()) {
            uw3.a(8, this.h);
        }
        int i2 = this.e;
        if (i2 != 0) {
            uw3.b(11, i2);
        }
        if (!this.i.isEmpty()) {
            uw3.a(13, this.i);
        }
        long j3 = this.j;
        if (j3 != 0) {
            uw3.b(15, j3);
        }
        long j4 = this.f;
        if (j4 != 0) {
            uw3.a(17, j4);
        }
        cp1 cp1 = this.o;
        if (cp1 != null) {
            if (cp1 == null) {
                cp1 = cp1.k();
            }
            uw3.a(23, (dx3) cp1);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(wo1 wo1, sw3 sw3) {
        if (sw3 != null) {
            wo1.g = sw3;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(cp1.b bVar) {
        this.o = (cp1) bVar.build();
    }
}
