package com.fossil;

import com.fossil.d5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i5 {
    @DexIgnore
    public q5 a; // = new q5(this);
    @DexIgnore
    public /* final */ j5 b;
    @DexIgnore
    public /* final */ d c;
    @DexIgnore
    public i5 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public c g; // = c.NONE;
    @DexIgnore
    public int h;
    @DexIgnore
    public d5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[d.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[d.CENTER.ordinal()] = 1;
            a[d.LEFT.ordinal()] = 2;
            a[d.RIGHT.ordinal()] = 3;
            a[d.TOP.ordinal()] = 4;
            a[d.BOTTOM.ordinal()] = 5;
            a[d.BASELINE.ordinal()] = 6;
            a[d.CENTER_X.ordinal()] = 7;
            a[d.CENTER_Y.ordinal()] = 8;
            a[d.NONE.ordinal()] = 9;
        }
        */
    }

    @DexIgnore
    public enum b {
        RELAXED,
        STRICT
    }

    @DexIgnore
    public enum c {
        NONE,
        STRONG,
        WEAK
    }

    @DexIgnore
    public enum d {
        NONE,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        BASELINE,
        CENTER,
        CENTER_X,
        CENTER_Y
    }

    @DexIgnore
    public i5(j5 j5Var, d dVar) {
        b bVar = b.RELAXED;
        this.h = 0;
        this.b = j5Var;
        this.c = dVar;
    }

    @DexIgnore
    public void a(x4 x4Var) {
        d5 d5Var = this.i;
        if (d5Var == null) {
            this.i = new d5(d5.a.UNRESTRICTED, (String) null);
        } else {
            d5Var.a();
        }
    }

    @DexIgnore
    public int b() {
        i5 i5Var;
        if (this.b.s() == 8) {
            return 0;
        }
        if (this.f <= -1 || (i5Var = this.d) == null || i5Var.b.s() != 8) {
            return this.e;
        }
        return this.f;
    }

    @DexIgnore
    public j5 c() {
        return this.b;
    }

    @DexIgnore
    public q5 d() {
        return this.a;
    }

    @DexIgnore
    public d5 e() {
        return this.i;
    }

    @DexIgnore
    public c f() {
        return this.g;
    }

    @DexIgnore
    public i5 g() {
        return this.d;
    }

    @DexIgnore
    public d h() {
        return this.c;
    }

    @DexIgnore
    public boolean i() {
        return this.d != null;
    }

    @DexIgnore
    public void j() {
        this.d = null;
        this.e = 0;
        this.f = -1;
        this.g = c.STRONG;
        this.h = 0;
        b bVar = b.RELAXED;
        this.a.d();
    }

    @DexIgnore
    public String toString() {
        return this.b.g() + ":" + this.c.toString();
    }

    @DexIgnore
    public int a() {
        return this.h;
    }

    @DexIgnore
    public boolean a(i5 i5Var, int i2, c cVar, int i3) {
        return a(i5Var, i2, -1, cVar, i3, false);
    }

    @DexIgnore
    public boolean a(i5 i5Var, int i2, int i3, c cVar, int i4, boolean z) {
        if (i5Var == null) {
            this.d = null;
            this.e = 0;
            this.f = -1;
            this.g = c.NONE;
            this.h = 2;
            return true;
        } else if (!z && !a(i5Var)) {
            return false;
        } else {
            this.d = i5Var;
            if (i2 > 0) {
                this.e = i2;
            } else {
                this.e = 0;
            }
            this.f = i3;
            this.g = cVar;
            this.h = i4;
            return true;
        }
    }

    @DexIgnore
    public boolean a(i5 i5Var) {
        if (i5Var == null) {
            return false;
        }
        d h2 = i5Var.h();
        d dVar = this.c;
        if (h2 != dVar) {
            switch (a.a[dVar.ordinal()]) {
                case 1:
                    if (h2 == d.BASELINE || h2 == d.CENTER_X || h2 == d.CENTER_Y) {
                        return false;
                    }
                    return true;
                case 2:
                case 3:
                    boolean z = h2 == d.LEFT || h2 == d.RIGHT;
                    if (i5Var.c() instanceof m5) {
                        return z || h2 == d.CENTER_X;
                    }
                    return z;
                case 4:
                case 5:
                    boolean z2 = h2 == d.TOP || h2 == d.BOTTOM;
                    if (i5Var.c() instanceof m5) {
                        return z2 || h2 == d.CENTER_Y;
                    }
                    return z2;
                case 6:
                case 7:
                case 8:
                case 9:
                    return false;
                default:
                    throw new AssertionError(this.c.name());
            }
        } else if (dVar != d.BASELINE || (i5Var.c().y() && c().y())) {
            return true;
        } else {
            return false;
        }
    }
}
