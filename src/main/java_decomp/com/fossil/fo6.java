package com.fossil;

import com.fossil.mc6;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fo6 {
    @DexIgnore
    public static /* final */ int a; // = b(Throwable.class, -1);
    @DexIgnore
    public static /* final */ ReentrantReadWriteLock b; // = new ReentrantReadWriteLock();
    @DexIgnore
    public static /* final */ WeakHashMap<Class<? extends Throwable>, hg6<Throwable, Throwable>> c; // = new WeakHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            wg6.b(th, "e");
            try {
                mc6.a aVar = mc6.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[]{th.getMessage(), th});
                if (newInstance != null) {
                    obj = mc6.m1constructorimpl((Throwable) newInstance);
                    if (mc6.m6isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                mc6.a aVar2 = mc6.Companion;
                obj = mc6.m1constructorimpl(nc6.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            wg6.b(th, "e");
            try {
                mc6.a aVar = mc6.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[]{th});
                if (newInstance != null) {
                    obj = mc6.m1constructorimpl((Throwable) newInstance);
                    if (mc6.m6isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                mc6.a aVar2 = mc6.Companion;
                obj = mc6.m1constructorimpl(nc6.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            wg6.b(th, "e");
            try {
                mc6.a aVar = mc6.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[]{th.getMessage()});
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    obj = mc6.m1constructorimpl(th2);
                    if (mc6.m6isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                mc6.a aVar2 = mc6.Companion;
                obj = mc6.m1constructorimpl(nc6.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends xg6 implements hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            wg6.b(th, "e");
            try {
                mc6.a aVar = mc6.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[0]);
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    obj = mc6.m1constructorimpl(th2);
                    if (mc6.m6isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                mc6.a aVar2 = mc6.Companion;
                obj = mc6.m1constructorimpl(nc6.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            Constructor constructor = (Constructor) t2;
            wg6.a((Object) constructor, "it");
            Integer valueOf = Integer.valueOf(constructor.getParameterTypes().length);
            Constructor constructor2 = (Constructor) t;
            wg6.a((Object) constructor2, "it");
            return ue6.a(valueOf, Integer.valueOf(constructor2.getParameterTypes().length));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends xg6 implements hg6 {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(1);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            wg6.b(th, "it");
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends xg6 implements hg6 {
        @DexIgnore
        public static /* final */ g INSTANCE; // = new g();

        @DexIgnore
        public g() {
            super(1);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            wg6.b(th, "it");
            return null;
        }
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    public static final <E extends java.lang.Throwable> E a(E r9) {
        /*
            java.lang.String r0 = "exception"
            com.fossil.wg6.b(r9, r0)
            boolean r0 = r9 instanceof com.fossil.bl6
            r1 = 0
            if (r0 == 0) goto L_0x002c
            com.fossil.mc6$a r0 = com.fossil.mc6.Companion     // Catch:{ all -> 0x0017 }
            com.fossil.bl6 r9 = (com.fossil.bl6) r9     // Catch:{ all -> 0x0017 }
            java.lang.Throwable r9 = r9.createCopy()     // Catch:{ all -> 0x0017 }
            java.lang.Object r9 = com.fossil.mc6.m1constructorimpl(r9)     // Catch:{ all -> 0x0017 }
            goto L_0x0022
        L_0x0017:
            r9 = move-exception
            com.fossil.mc6$a r0 = com.fossil.mc6.Companion
            java.lang.Object r9 = com.fossil.nc6.a((java.lang.Throwable) r9)
            java.lang.Object r9 = com.fossil.mc6.m1constructorimpl(r9)
        L_0x0022:
            boolean r0 = com.fossil.mc6.m6isFailureimpl(r9)
            if (r0 == 0) goto L_0x0029
            r9 = r1
        L_0x0029:
            java.lang.Throwable r9 = (java.lang.Throwable) r9
            return r9
        L_0x002c:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = b
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.lock()
            java.util.WeakHashMap<java.lang.Class<? extends java.lang.Throwable>, com.fossil.hg6<java.lang.Throwable, java.lang.Throwable>> r2 = c     // Catch:{ all -> 0x0129 }
            java.lang.Class r3 = r9.getClass()     // Catch:{ all -> 0x0129 }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x0129 }
            com.fossil.hg6 r2 = (com.fossil.hg6) r2     // Catch:{ all -> 0x0129 }
            r0.unlock()
            if (r2 == 0) goto L_0x004d
            java.lang.Object r9 = r2.invoke(r9)
            java.lang.Throwable r9 = (java.lang.Throwable) r9
            return r9
        L_0x004d:
            int r0 = a
            java.lang.Class r2 = r9.getClass()
            r3 = 0
            int r2 = b(r2, r3)
            if (r0 == r2) goto L_0x00a2
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = b
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r2 = r0.readLock()
            int r4 = r0.getWriteHoldCount()
            if (r4 != 0) goto L_0x006b
            int r4 = r0.getReadHoldCount()
            goto L_0x006c
        L_0x006b:
            r4 = 0
        L_0x006c:
            r5 = 0
        L_0x006d:
            if (r5 >= r4) goto L_0x0075
            r2.unlock()
            int r5 = r5 + 1
            goto L_0x006d
        L_0x0075:
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()
            r0.lock()
            java.util.WeakHashMap<java.lang.Class<? extends java.lang.Throwable>, com.fossil.hg6<java.lang.Throwable, java.lang.Throwable>> r5 = c     // Catch:{ all -> 0x0095 }
            java.lang.Class r9 = r9.getClass()     // Catch:{ all -> 0x0095 }
            com.fossil.fo6$f r6 = com.fossil.fo6.f.INSTANCE     // Catch:{ all -> 0x0095 }
            r5.put(r9, r6)     // Catch:{ all -> 0x0095 }
            com.fossil.cd6 r9 = com.fossil.cd6.a     // Catch:{ all -> 0x0095 }
        L_0x0089:
            if (r3 >= r4) goto L_0x0091
            r2.lock()
            int r3 = r3 + 1
            goto L_0x0089
        L_0x0091:
            r0.unlock()
            return r1
        L_0x0095:
            r9 = move-exception
        L_0x0096:
            if (r3 >= r4) goto L_0x009e
            r2.lock()
            int r3 = r3 + 1
            goto L_0x0096
        L_0x009e:
            r0.unlock()
            throw r9
        L_0x00a2:
            java.lang.Class r0 = r9.getClass()
            java.lang.reflect.Constructor[] r0 = r0.getConstructors()
            java.lang.String r2 = "exception.javaClass.constructors"
            com.fossil.wg6.a((java.lang.Object) r0, (java.lang.String) r2)
            com.fossil.fo6$e r2 = new com.fossil.fo6$e
            r2.<init>()
            java.util.List r0 = com.fossil.nd6.c((T[]) r0, r2)
            java.util.Iterator r0 = r0.iterator()
            r2 = r1
        L_0x00bd:
            boolean r4 = r0.hasNext()
            if (r4 == 0) goto L_0x00d4
            java.lang.Object r2 = r0.next()
            java.lang.reflect.Constructor r2 = (java.lang.reflect.Constructor) r2
            java.lang.String r4 = "constructor"
            com.fossil.wg6.a((java.lang.Object) r2, (java.lang.String) r4)
            com.fossil.hg6 r2 = a((java.lang.reflect.Constructor<?>) r2)
            if (r2 == 0) goto L_0x00bd
        L_0x00d4:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = b
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r0.readLock()
            int r5 = r0.getWriteHoldCount()
            if (r5 != 0) goto L_0x00e5
            int r5 = r0.getReadHoldCount()
            goto L_0x00e6
        L_0x00e5:
            r5 = 0
        L_0x00e6:
            r6 = 0
        L_0x00e7:
            if (r6 >= r5) goto L_0x00ef
            r4.unlock()
            int r6 = r6 + 1
            goto L_0x00e7
        L_0x00ef:
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()
            r0.lock()
            java.util.WeakHashMap<java.lang.Class<? extends java.lang.Throwable>, com.fossil.hg6<java.lang.Throwable, java.lang.Throwable>> r6 = c     // Catch:{ all -> 0x011c }
            java.lang.Class r7 = r9.getClass()     // Catch:{ all -> 0x011c }
            if (r2 == 0) goto L_0x0100
            r8 = r2
            goto L_0x0102
        L_0x0100:
            com.fossil.fo6$g r8 = com.fossil.fo6.g.INSTANCE     // Catch:{ all -> 0x011c }
        L_0x0102:
            r6.put(r7, r8)     // Catch:{ all -> 0x011c }
            com.fossil.cd6 r6 = com.fossil.cd6.a     // Catch:{ all -> 0x011c }
        L_0x0107:
            if (r3 >= r5) goto L_0x010f
            r4.lock()
            int r3 = r3 + 1
            goto L_0x0107
        L_0x010f:
            r0.unlock()
            if (r2 == 0) goto L_0x011b
            java.lang.Object r9 = r2.invoke(r9)
            r1 = r9
            java.lang.Throwable r1 = (java.lang.Throwable) r1
        L_0x011b:
            return r1
        L_0x011c:
            r9 = move-exception
        L_0x011d:
            if (r3 >= r5) goto L_0x0125
            r4.lock()
            int r3 = r3 + 1
            goto L_0x011d
        L_0x0125:
            r0.unlock()
            throw r9
        L_0x0129:
            r9 = move-exception
            r0.unlock()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fo6.a(java.lang.Throwable):java.lang.Throwable");
    }

    @DexIgnore
    public static final int b(Class<?> cls, int i) {
        Integer num;
        eg6.a(cls);
        try {
            mc6.a aVar = mc6.Companion;
            num = mc6.m1constructorimpl(Integer.valueOf(a(cls, 0, 1, (Object) null)));
        } catch (Throwable th) {
            mc6.a aVar2 = mc6.Companion;
            num = mc6.m1constructorimpl(nc6.a(th));
        }
        Integer valueOf = Integer.valueOf(i);
        if (mc6.m6isFailureimpl(num)) {
            num = valueOf;
        }
        return ((Number) num).intValue();
    }

    @DexIgnore
    public static final hg6<Throwable, Throwable> a(Constructor<?> constructor) {
        Class[] parameterTypes = constructor.getParameterTypes();
        int length = parameterTypes.length;
        if (length == 0) {
            return new d(constructor);
        }
        if (length == 1) {
            Class cls = parameterTypes[0];
            if (wg6.a((Object) cls, (Object) Throwable.class)) {
                return new b(constructor);
            }
            if (wg6.a((Object) cls, (Object) String.class)) {
                return new c(constructor);
            }
            return null;
        } else if (length == 2 && wg6.a((Object) parameterTypes[0], (Object) String.class) && wg6.a((Object) parameterTypes[1], (Object) Throwable.class)) {
            return new a(constructor);
        } else {
            return null;
        }
    }

    @DexIgnore
    public static /* synthetic */ int a(Class cls, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return a(cls, i);
    }

    @DexIgnore
    public static final int a(Class<?> cls, int i) {
        Class<? super Object> superclass;
        do {
            Field[] declaredFields = r6.getDeclaredFields();
            wg6.a((Object) declaredFields, "declaredFields");
            int i2 = 0;
            Class<? super Object> cls2 = cls;
            for (Field field : declaredFields) {
                wg6.a((Object) field, "it");
                if (!Modifier.isStatic(field.getModifiers())) {
                    i2++;
                }
            }
            i += i2;
            superclass = cls2.getSuperclass();
            cls2 = superclass;
        } while (superclass != null);
        return i;
    }
}
