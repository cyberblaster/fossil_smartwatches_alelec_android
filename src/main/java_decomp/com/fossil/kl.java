package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl extends RecyclerView.q {
    @DexIgnore
    public ViewPager2.i a;
    @DexIgnore
    public /* final */ ViewPager2 b;
    @DexIgnore
    public /* final */ RecyclerView c; // = this.b.j;
    @DexIgnore
    public /* final */ LinearLayoutManager d; // = ((LinearLayoutManager) this.c.getLayoutManager());
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public a g; // = new a();
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public float b;
        @DexIgnore
        public int c;

        @DexIgnore
        public void a() {
            this.a = -1;
            this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.c = 0;
        }
    }

    @DexIgnore
    public kl(ViewPager2 viewPager2) {
        this.b = viewPager2;
        h();
    }

    @DexIgnore
    public final void a(boolean z) {
        this.m = z;
        this.e = z ? 4 : 1;
        int i2 = this.i;
        if (i2 != -1) {
            this.h = i2;
            this.i = -1;
        } else if (this.h == -1) {
            this.h = a();
        }
        b(1);
    }

    @DexIgnore
    public double b() {
        i();
        a aVar = this.g;
        return ((double) aVar.a) + ((double) aVar.b);
    }

    @DexIgnore
    public int c() {
        return this.f;
    }

    @DexIgnore
    public boolean d() {
        return this.m;
    }

    @DexIgnore
    public boolean e() {
        return this.f == 0;
    }

    @DexIgnore
    public final boolean f() {
        int i2 = this.e;
        return i2 == 1 || i2 == 4;
    }

    @DexIgnore
    public void g() {
        this.l = true;
    }

    @DexIgnore
    public final void h() {
        this.e = 0;
        this.f = 0;
        this.g.a();
        this.h = -1;
        this.i = -1;
        this.j = false;
        this.k = false;
        this.m = false;
        this.l = false;
    }

    @DexIgnore
    public final void i() {
        int i2;
        a aVar = this.g;
        aVar.a = this.d.I();
        int i3 = aVar.a;
        if (i3 == -1) {
            aVar.a();
            return;
        }
        View c2 = this.d.c(i3);
        if (c2 == null) {
            aVar.a();
            return;
        }
        int k2 = this.d.k(c2);
        int m2 = this.d.m(c2);
        int n = this.d.n(c2);
        int d2 = this.d.d(c2);
        ViewGroup.LayoutParams layoutParams = c2.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            k2 += marginLayoutParams.leftMargin;
            m2 += marginLayoutParams.rightMargin;
            n += marginLayoutParams.topMargin;
            d2 += marginLayoutParams.bottomMargin;
        }
        int height = c2.getHeight() + n + d2;
        int width = m2 + c2.getWidth() + k2;
        if (this.d.Q() == 0) {
            i2 = (c2.getLeft() - k2) - this.c.getPaddingLeft();
            if (this.b.c()) {
                i2 = -i2;
            }
        } else {
            i2 = (c2.getTop() - n) - this.c.getPaddingTop();
            width = height;
        }
        aVar.c = -i2;
        int i4 = aVar.c;
        if (i4 >= 0) {
            aVar.b = width == 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : ((float) i4) / ((float) width);
        } else if (new gl(this.d).c()) {
            throw new IllegalStateException("Page(s) contain a ViewGroup with a LayoutTransition (or animateLayoutChanges=\"true\"), which interferes with the scrolling animation. Make sure to call getLayoutTransition().setAnimateParentHierarchy(false) on all ViewGroups with a LayoutTransition before an animation is started.");
        } else {
            throw new IllegalStateException(String.format(Locale.US, "Page can only be offset by a positive amount, not by %d", new Object[]{Integer.valueOf(aVar.c)}));
        }
    }

    @DexIgnore
    public void onScrollStateChanged(RecyclerView recyclerView, int i2) {
        boolean z = true;
        if (!(this.e == 1 && this.f == 1) && i2 == 1) {
            a(false);
        } else if (!f() || i2 != 2) {
            if (f() && i2 == 0) {
                i();
                if (!this.k) {
                    int i3 = this.g.a;
                    if (i3 != -1) {
                        a(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
                    }
                } else {
                    a aVar = this.g;
                    if (aVar.c == 0) {
                        int i4 = this.h;
                        int i5 = aVar.a;
                        if (i4 != i5) {
                            a(i5);
                        }
                    } else {
                        z = false;
                    }
                }
                if (z) {
                    b(0);
                    h();
                }
            }
            if (this.e == 2 && i2 == 0 && this.l) {
                i();
                a aVar2 = this.g;
                if (aVar2.c == 0) {
                    int i6 = this.i;
                    int i7 = aVar2.a;
                    if (i6 != i7) {
                        if (i7 == -1) {
                            i7 = 0;
                        }
                        a(i7);
                    }
                    b(0);
                    h();
                }
            }
        } else if (this.k) {
            b(2);
            this.j = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        if ((r5 < 0) == r3.b.c()) goto L_0x0022;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
    public void onScrolled(RecyclerView recyclerView, int i2, int i3) {
        boolean z;
        int i4;
        int i5;
        int i6;
        this.k = true;
        i();
        if (this.j) {
            this.j = false;
            if (i3 <= 0) {
                if (i3 == 0) {
                }
                z = false;
                if (z) {
                    a aVar = this.g;
                    if (aVar.c != 0) {
                        i4 = aVar.a + 1;
                        this.i = i4;
                        i5 = this.h;
                        i6 = this.i;
                        if (i5 != i6) {
                            a(i6);
                        }
                    }
                }
                i4 = this.g.a;
                this.i = i4;
                i5 = this.h;
                i6 = this.i;
                if (i5 != i6) {
                }
            }
            z = true;
            if (z) {
            }
            i4 = this.g.a;
            this.i = i4;
            i5 = this.h;
            i6 = this.i;
            if (i5 != i6) {
            }
        } else if (this.e == 0) {
            int i7 = this.g.a;
            if (i7 == -1) {
                i7 = 0;
            }
            a(i7);
        }
        int i8 = this.g.a;
        if (i8 == -1) {
            i8 = 0;
        }
        a aVar2 = this.g;
        a(i8, aVar2.b, aVar2.c);
        int i9 = this.g.a;
        int i10 = this.i;
        if ((i9 == i10 || i10 == -1) && this.g.c == 0 && this.f != 1) {
            b(0);
            h();
        }
    }

    @DexIgnore
    public final void b(int i2) {
        if ((this.e != 3 || this.f != 0) && this.f != i2) {
            this.f = i2;
            ViewPager2.i iVar = this.a;
            if (iVar != null) {
                iVar.a(i2);
            }
        }
    }

    @DexIgnore
    public void a(int i2, boolean z) {
        this.e = z ? 2 : 3;
        boolean z2 = false;
        this.m = false;
        if (this.i != i2) {
            z2 = true;
        }
        this.i = i2;
        b(2);
        if (z2) {
            a(i2);
        }
    }

    @DexIgnore
    public void a(ViewPager2.i iVar) {
        this.a = iVar;
    }

    @DexIgnore
    public final void a(int i2) {
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final void a(int i2, float f2, int i3) {
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public final int a() {
        return this.d.I();
    }
}
