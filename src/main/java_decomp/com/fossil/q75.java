package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q75 implements Factory<p75> {
    @DexIgnore
    public static WatchAppsPresenter a(m75 m75, CategoryRepository categoryRepository, an4 an4) {
        return new WatchAppsPresenter(m75, categoryRepository, an4);
    }
}
