package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x90 extends d90 {
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public x90(e90 e90, byte b, int i) {
        super(e90, b);
        this.c = i;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.REQUEST_ID, (Object) Integer.valueOf(this.c));
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(getClass(), obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((x90) obj).c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.DeviceRequest");
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }

    @DexIgnore
    public x90(Parcel parcel) {
        super(parcel);
        this.c = parcel.readInt();
    }
}
