package com.fossil;

import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1$1", f = "ComplicationSearchPresenter.kt", l = {}, m = "invokeSuspend")
public final class m65$b$a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationSearchPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m65$b$a(ComplicationSearchPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        m65$b$a m65_b_a = new m65$b$a(this.this$0, xe6);
        m65_b_a.p$ = (il6) obj;
        return m65_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((m65$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.j.addAll(this.this$0.this$0.m.getAllComplicationRaw());
            ArrayList d = this.this$0.this$0.k;
            ComplicationRepository c = this.this$0.this$0.m;
            List<String> d2 = this.this$0.this$0.n.d();
            wg6.a((Object) d2, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
            return hf6.a(d.addAll(c.getComplicationByIds(d2)));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
