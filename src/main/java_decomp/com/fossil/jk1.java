package com.fossil;

import com.fossil.x60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class jk1 extends tg6 implements hg6<byte[], x60> {
    @DexIgnore
    public jk1(x60.b bVar) {
        super(1, bVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(x60.b.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/InactiveNudgeConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((x60.b) this.receiver).a((byte[]) obj);
    }
}
