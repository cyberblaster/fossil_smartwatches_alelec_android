package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy0 extends ok0 {
    @DexIgnore
    public /* final */ li1 j; // = li1.HIGH;
    @DexIgnore
    public m51 k; // = m51.DISCONNECTED;

    @DexIgnore
    public iy0(at0 at0) {
        super(hm0.DISCONNECT_HID, at0);
    }

    @DexIgnore
    public void a(p51 p51) {
        ch0 ch0;
        c(p51);
        t31 t31 = p51.a;
        if (t31.a != x11.SUCCESS) {
            ch0 a = ch0.d.a(t31);
            ch0 = ch0.a(this.d, (hm0) null, a.b, a.c, 1);
        } else if (this.k == m51.DISCONNECTED) {
            ch0 = ch0.a(this.d, (hm0) null, lf0.SUCCESS, (t31) null, 5);
        } else {
            ch0 = ch0.a(this.d, (hm0) null, lf0.UNEXPECTED_RESULT, (t31) null, 5);
        }
        this.d = ch0;
    }

    @DexIgnore
    public li1 b() {
        return this.j;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.l;
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return (p51 instanceof sp0) && ((sp0) p51).b == m51.DISCONNECTED;
    }

    @DexIgnore
    public void c(p51 p51) {
        this.k = ((sp0) p51).b;
    }

    @DexIgnore
    public void a(ue1 ue1) {
        int i = db1.d[ue1.e().ordinal()];
        if (i == 1) {
            t31 t31 = new t31(x11.SUCCESS, 0, 2);
            m51 m51 = m51.DISCONNECTED;
            ue1.a.post(new nw0(ue1, t31, m51, m51));
        } else if (i == 2 || i == 3) {
            t31 a = t31.c.a(s81.d.b(ue1.w));
            if (a.a != x11.SUCCESS) {
                ue1.a.post(new nw0(ue1, a, m51.CONNECTED, m51.DISCONNECTED));
            }
        }
    }
}
