package com.fossil;

import android.annotation.TargetApi;
import android.icu.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nh3 {
    @DexIgnore
    public static TimeZone a() {
        return TimeZone.getTimeZone("UTC");
    }

    @DexIgnore
    public static Calendar b() {
        return a(Calendar.getInstance());
    }

    @DexIgnore
    @TargetApi(24)
    public static android.icu.util.TimeZone c() {
        return android.icu.util.TimeZone.getTimeZone("UTC");
    }

    @DexIgnore
    public static Calendar d() {
        return b((Calendar) null);
    }

    @DexIgnore
    public static SimpleDateFormat e() {
        return d(Locale.getDefault());
    }

    @DexIgnore
    public static Calendar a(Calendar calendar) {
        Calendar b = b(calendar);
        Calendar d = d();
        d.set(b.get(1), b.get(2), b.get(5));
        return d;
    }

    @DexIgnore
    public static Calendar b(Calendar calendar) {
        Calendar instance = Calendar.getInstance(a());
        if (calendar == null) {
            instance.clear();
        } else {
            instance.setTimeInMillis(calendar.getTimeInMillis());
        }
        return instance;
    }

    @DexIgnore
    @TargetApi(24)
    public static DateFormat c(Locale locale) {
        return a("yMMMEd", locale);
    }

    @DexIgnore
    public static SimpleDateFormat d(Locale locale) {
        return b("MMMM, yyyy", locale);
    }

    @DexIgnore
    public static SimpleDateFormat b(String str, Locale locale) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str, locale);
        simpleDateFormat.setTimeZone(a());
        return simpleDateFormat;
    }

    @DexIgnore
    public static java.text.DateFormat b(Locale locale) {
        return a(0, locale);
    }

    @DexIgnore
    public static long a(long j) {
        Calendar d = d();
        d.setTimeInMillis(j);
        return a(d).getTimeInMillis();
    }

    @DexIgnore
    @TargetApi(24)
    public static DateFormat a(String str, Locale locale) {
        DateFormat instanceForSkeleton = DateFormat.getInstanceForSkeleton(str, locale);
        instanceForSkeleton.setTimeZone(c());
        return instanceForSkeleton;
    }

    @DexIgnore
    public static java.text.DateFormat a(int i, Locale locale) {
        java.text.DateFormat dateInstance = java.text.DateFormat.getDateInstance(i, locale);
        dateInstance.setTimeZone(a());
        return dateInstance;
    }

    @DexIgnore
    @TargetApi(24)
    public static DateFormat a(Locale locale) {
        return a("MMMEd", locale);
    }
}
