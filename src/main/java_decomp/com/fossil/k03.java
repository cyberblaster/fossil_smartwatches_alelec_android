package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k03 implements Parcelable.Creator<i03> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 2) {
                f22.v(parcel, a);
            } else {
                bundle = f22.a(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new i03(bundle);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new i03[i];
    }
}
