package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<we3> CREATOR; // = new xe3();
    @DexIgnore
    public byte a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public we3(byte b2, byte b3, String str) {
        this.a = b2;
        this.b = b3;
        this.c = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || we3.class != obj.getClass()) {
            return false;
        }
        we3 we3 = (we3) obj;
        return this.a == we3.a && this.b == we3.b && this.c.equals(we3.c);
    }

    @DexIgnore
    public final int hashCode() {
        return ((((this.a + 31) * 31) + this.b) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public final String toString() {
        byte b2 = this.a;
        byte b3 = this.b;
        String str = this.c;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 73);
        sb.append("AmsEntityUpdateParcelable{, mEntityId=");
        sb.append(b2);
        sb.append(", mAttributeId=");
        sb.append(b3);
        sb.append(", mValue='");
        sb.append(str);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a);
        g22.a(parcel, 3, this.b);
        g22.a(parcel, 4, this.c, false);
        g22.a(parcel, a2);
    }
}
