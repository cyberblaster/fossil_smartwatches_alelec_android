package com.fossil;

import android.os.RemoteException;
import com.fossil.qw1;
import com.fossil.rv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rz1 extends pz1<Void> {
    @DexIgnore
    public /* final */ xw1<rv1.b, ?> b;
    @DexIgnore
    public /* final */ dx1<rv1.b, ?> c;

    @DexIgnore
    public rz1(zy1 zy1, rc3<Void> rc3) {
        super(3, rc3);
        this.b = zy1.a;
        this.c = zy1.b;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(k02 k02, boolean z) {
    }

    @DexIgnore
    public final iv1[] b(qw1.a<?> aVar) {
        return this.b.c();
    }

    @DexIgnore
    public final boolean c(qw1.a<?> aVar) {
        return this.b.d();
    }

    @DexIgnore
    public final void d(qw1.a<?> aVar) throws RemoteException {
        this.b.a(aVar.f(), this.a);
        if (this.b.b() != null) {
            aVar.l().put(this.b.b(), new zy1(this.b, this.c));
        }
    }
}
