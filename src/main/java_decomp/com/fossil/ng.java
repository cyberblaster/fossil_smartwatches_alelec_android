package com.fossil;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ng extends a9 {
    @DexIgnore
    public /* final */ RecyclerView d;
    @DexIgnore
    public /* final */ a e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends a9 {
        @DexIgnore
        public /* final */ ng d;
        @DexIgnore
        public Map<View, a9> e; // = new WeakHashMap();

        @DexIgnore
        public a(ng ngVar) {
            this.d = ngVar;
        }

        @DexIgnore
        public void a(View view, ia iaVar) {
            if (this.d.c() || this.d.d.getLayoutManager() == null) {
                super.a(view, iaVar);
                return;
            }
            this.d.d.getLayoutManager().a(view, iaVar);
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                a9Var.a(view, iaVar);
            } else {
                super.a(view, iaVar);
            }
        }

        @DexIgnore
        public void b(View view, AccessibilityEvent accessibilityEvent) {
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                a9Var.b(view, accessibilityEvent);
            } else {
                super.b(view, accessibilityEvent);
            }
        }

        @DexIgnore
        public a9 c(View view) {
            return this.e.remove(view);
        }

        @DexIgnore
        public void d(View view) {
            a9 b = x9.b(view);
            if (b != null && b != this) {
                this.e.put(view, b);
            }
        }

        @DexIgnore
        public void c(View view, AccessibilityEvent accessibilityEvent) {
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                a9Var.c(view, accessibilityEvent);
            } else {
                super.c(view, accessibilityEvent);
            }
        }

        @DexIgnore
        public void d(View view, AccessibilityEvent accessibilityEvent) {
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                a9Var.d(view, accessibilityEvent);
            } else {
                super.d(view, accessibilityEvent);
            }
        }

        @DexIgnore
        public boolean a(View view, int i, Bundle bundle) {
            if (this.d.c() || this.d.d.getLayoutManager() == null) {
                return super.a(view, i, bundle);
            }
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                if (a9Var.a(view, i, bundle)) {
                    return true;
                }
            } else if (super.a(view, i, bundle)) {
                return true;
            }
            return this.d.d.getLayoutManager().a(view, i, bundle);
        }

        @DexIgnore
        public void a(View view, int i) {
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                a9Var.a(view, i);
            } else {
                super.a(view, i);
            }
        }

        @DexIgnore
        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                return a9Var.a(view, accessibilityEvent);
            }
            return super.a(view, accessibilityEvent);
        }

        @DexIgnore
        public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            a9 a9Var = this.e.get(viewGroup);
            if (a9Var != null) {
                return a9Var.a(viewGroup, view, accessibilityEvent);
            }
            return super.a(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        public ja a(View view) {
            a9 a9Var = this.e.get(view);
            if (a9Var != null) {
                return a9Var.a(view);
            }
            return super.a(view);
        }
    }

    @DexIgnore
    public ng(RecyclerView recyclerView) {
        this.d = recyclerView;
        a9 b = b();
        if (b == null || !(b instanceof a)) {
            this.e = new a(this);
        } else {
            this.e = (a) b;
        }
    }

    @DexIgnore
    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        if (c() || this.d.getLayoutManager() == null) {
            return false;
        }
        return this.d.getLayoutManager().a(i, bundle);
    }

    @DexIgnore
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        if ((view instanceof RecyclerView) && !c()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    @DexIgnore
    public boolean c() {
        return this.d.hasPendingAdapterUpdates();
    }

    @DexIgnore
    public void a(View view, ia iaVar) {
        super.a(view, iaVar);
        if (!c() && this.d.getLayoutManager() != null) {
            this.d.getLayoutManager().a(iaVar);
        }
    }

    @DexIgnore
    public a9 b() {
        return this.e;
    }
}
