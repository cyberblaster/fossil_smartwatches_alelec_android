package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m86 extends i86<Boolean> {
    @DexIgnore
    public /* final */ va6 g; // = new sa6();
    @DexIgnore
    public PackageManager h;
    @DexIgnore
    public String i;
    @DexIgnore
    public PackageInfo j;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ Future<Map<String, k86>> t;
    @DexIgnore
    public /* final */ Collection<i86> u;

    @DexIgnore
    public m86(Future<Map<String, k86>> future, Collection<i86> collection) {
        this.t = future;
        this.u = collection;
    }

    @DexIgnore
    public Map<String, k86> a(Map<String, k86> map, Collection<i86> collection) {
        for (i86 next : collection) {
            if (!map.containsKey(next.h())) {
                map.put(next.h(), new k86(next.h(), next.j(), "binary"));
            }
        }
        return map;
    }

    @DexIgnore
    public final boolean b(String str, ib6 ib6, Collection<k86> collection) {
        return new mb6(this, n(), ib6.b, this.g).a(a(sb6.a(d(), str), collection));
    }

    @DexIgnore
    public String h() {
        return "io.fabric.sdk.android:fabric";
    }

    @DexIgnore
    public String j() {
        return "1.4.8.32";
    }

    @DexIgnore
    public boolean m() {
        try {
            this.q = g().g();
            this.h = d().getPackageManager();
            this.i = d().getPackageName();
            this.j = this.h.getPackageInfo(this.i, 0);
            this.o = Integer.toString(this.j.versionCode);
            this.p = this.j.versionName == null ? "0.0" : this.j.versionName;
            this.r = this.h.getApplicationLabel(d().getApplicationInfo()).toString();
            this.s = Integer.toString(d().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            c86.g().e("Fabric", "Failed init", e);
            return false;
        }
    }

    @DexIgnore
    public String n() {
        return z86.b(d(), "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public final yb6 o() {
        try {
            vb6 d = vb6.d();
            d.a(this, this.e, this.g, this.o, this.p, n(), c96.a(d()));
            d.b();
            return vb6.d().a();
        } catch (Exception e) {
            c86.g().e("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    @DexIgnore
    public Boolean c() {
        boolean z;
        Map map;
        String c = z86.c(d());
        yb6 o2 = o();
        if (o2 != null) {
            try {
                if (this.t != null) {
                    map = this.t.get();
                } else {
                    map = new HashMap();
                }
                a((Map<String, k86>) map, this.u);
                z = a(c, o2.a, (Collection<k86>) map.values());
            } catch (Exception e) {
                c86.g().e("Fabric", "Error performing auto configuration.", e);
            }
            return Boolean.valueOf(z);
        }
        z = false;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final boolean a(String str, ib6 ib6, Collection<k86> collection) {
        if ("new".equals(ib6.a)) {
            if (b(str, ib6, collection)) {
                return vb6.d().c();
            }
            c86.g().e("Fabric", "Failed to create app with Crashlytics service.", (Throwable) null);
            return false;
        } else if ("configured".equals(ib6.a)) {
            return vb6.d().c();
        } else {
            if (ib6.e) {
                c86.g().d("Fabric", "Server says an update is required - forcing a full App update.");
                c(str, ib6, collection);
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean c(String str, ib6 ib6, Collection<k86> collection) {
        return a(ib6, sb6.a(d(), str), collection);
    }

    @DexIgnore
    public final boolean a(ib6 ib6, sb6 sb6, Collection<k86> collection) {
        return new dc6(this, n(), ib6.b, this.g).a(a(sb6, collection));
    }

    @DexIgnore
    public final hb6 a(sb6 sb6, Collection<k86> collection) {
        Context d = d();
        return new hb6(new x86().d(d), g().d(), this.p, this.o, z86.a(z86.n(d)), this.r, d96.determineFrom(this.q).getId(), this.s, ShareWebViewClient.RESP_SUCC_CODE, sb6, collection);
    }
}
