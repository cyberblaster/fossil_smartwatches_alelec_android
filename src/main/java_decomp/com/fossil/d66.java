package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d66 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ Throwable b;

    @DexIgnore
    public d66(Context context, Throwable th) {
        this.a = context;
        this.b = th;
    }

    @DexIgnore
    public final void run() {
        try {
            if (n36.s()) {
                new k46(new u36(this.a, q36.a(this.a, false, (r36) null), 99, this.b, x36.m)).a();
            }
        } catch (Throwable th) {
            b56 f = q36.m;
            f.c("reportSdkSelfException error: " + th);
        }
    }
}
