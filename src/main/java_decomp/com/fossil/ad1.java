package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad1 {
    @DexIgnore
    public /* synthetic */ ad1(qg6 qg6) {
    }

    @DexIgnore
    public final rg1 a(byte b) {
        rg1 rg1;
        rg1[] values = rg1.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                rg1 = null;
                break;
            }
            rg1 = values[i];
            if (rg1.b == b) {
                break;
            }
            i++;
        }
        return rg1 != null ? rg1 : rg1.UNKNOWN;
    }
}
