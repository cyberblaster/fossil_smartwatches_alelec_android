package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm2 extends sl2<Double> implements nn2<Double>, cp2, RandomAccess {
    @DexIgnore
    public double[] b;
    @DexIgnore
    public int c;

    /*
    static {
        new qm2(new double[0], 0).k();
    }
    */

    @DexIgnore
    public qm2() {
        this(new double[10], 0);
    }

    @DexIgnore
    public final void a(double d) {
        a();
        int i = this.c;
        double[] dArr = this.b;
        if (i == dArr.length) {
            double[] dArr2 = new double[(((i * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.b = dArr2;
        }
        double[] dArr3 = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        dArr3[i2] = d;
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        double doubleValue = ((Double) obj).doubleValue();
        a();
        if (i < 0 || i > (i2 = this.c)) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
        double[] dArr = this.b;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.b, i, dArr2, i + 1, this.c - i);
            this.b = dArr2;
        }
        this.b[i] = doubleValue;
        this.c++;
        this.modCount++;
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Double> collection) {
        a();
        hn2.a(collection);
        if (!(collection instanceof qm2)) {
            return super.addAll(collection);
        }
        qm2 qm2 = (qm2) collection;
        int i = qm2.c;
        if (i == 0) {
            return false;
        }
        int i2 = this.c;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.b;
            if (i3 > dArr.length) {
                this.b = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(qm2.b, 0, this.b, this.c, qm2.c);
            this.c = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qm2)) {
            return super.equals(obj);
        }
        qm2 qm2 = (qm2) obj;
        if (this.c != qm2.c) {
            return false;
        }
        double[] dArr = qm2.b;
        for (int i = 0; i < this.c; i++) {
            if (Double.doubleToLongBits(this.b[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        zzb(i);
        return Double.valueOf(this.b[i]);
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + hn2.a(Double.doubleToLongBits(this.b[i2]));
        }
        return i;
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Double.valueOf(this.b[i]))) {
                double[] dArr = this.b;
                System.arraycopy(dArr, i + 1, dArr, i, (this.c - i) - 1);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            double[] dArr = this.b;
            System.arraycopy(dArr, i2, dArr, i, this.c - i2);
            this.c -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        a();
        zzb(i);
        double[] dArr = this.b;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    @DexIgnore
    public final int size() {
        return this.c;
    }

    @DexIgnore
    public final /* synthetic */ nn2 zza(int i) {
        if (i >= this.c) {
            return new qm2(Arrays.copyOf(this.b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final void zzb(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
    }

    @DexIgnore
    public final String zzc(int i) {
        int i2 = this.c;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public qm2(double[] dArr, int i) {
        this.b = dArr;
        this.c = i;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        zzb(i);
        double[] dArr = this.b;
        double d = dArr[i];
        int i2 = this.c;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.c--;
        this.modCount++;
        return Double.valueOf(d);
    }

    @DexIgnore
    public final /* synthetic */ boolean add(Object obj) {
        a(((Double) obj).doubleValue());
        return true;
    }
}
