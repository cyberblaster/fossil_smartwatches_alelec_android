package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jz {
    @DexIgnore
    boolean a(jz jzVar);

    @DexIgnore
    void c();

    @DexIgnore
    void clear();

    @DexIgnore
    boolean d();

    @DexIgnore
    void e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean g();

    @DexIgnore
    boolean isRunning();
}
