package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pk2<T> {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context g;
    @DexIgnore
    public static dl2<lk2> h;
    @DexIgnore
    public static /* final */ AtomicInteger i; // = new AtomicInteger();
    @DexIgnore
    public /* final */ vk2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ T c;
    @DexIgnore
    public volatile int d;
    @DexIgnore
    public volatile T e;

    @DexIgnore
    public pk2(vk2 vk2, String str, T t) {
        this.d = -1;
        if (vk2.a != null) {
            this.a = vk2;
            this.b = str;
            this.c = t;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @DexIgnore
    public static void a(Context context) {
        synchronized (f) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (g != context) {
                ak2.e();
                yk2.a();
                kk2.a();
                i.incrementAndGet();
                g = context;
                h = hl2.a(sk2.a);
            }
        }
    }

    @DexIgnore
    public static void c() {
        i.incrementAndGet();
    }

    @DexIgnore
    public static final /* synthetic */ lk2 d() {
        new ok2();
        return ok2.a(g);
    }

    @DexIgnore
    public abstract T a(Object obj);

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ba  */
    public final T b() {
        T t;
        fk2 fk2;
        Object zza;
        int i2 = i.get();
        if (this.d < i2) {
            synchronized (this) {
                if (this.d < i2) {
                    if (g != null) {
                        T t2 = null;
                        String a2 = h.zza().a(this.a.a, (String) null, this.a.c, this.b);
                        if (a2 != null) {
                            t = a((Object) a2);
                        } else {
                            String str = (String) kk2.a(g).zza("gms:phenotype:phenotype_flag:debug_bypass_phenotype");
                            if (!(str != null && zj2.c.matcher(str).matches())) {
                                if (this.a.a != null) {
                                    fk2 = nk2.a(g, this.a.a) ? ak2.a(g.getContentResolver(), this.a.a) : null;
                                } else {
                                    fk2 = yk2.a(g, (String) null);
                                }
                                if (!(fk2 == null || (zza = fk2.zza(a())) == null)) {
                                    t = a(zza);
                                    if (t != null) {
                                        Object zza2 = kk2.a(g).zza(a(this.a.b));
                                        if (zza2 != null) {
                                            t2 = a(zza2);
                                        }
                                        t = t2;
                                        if (t == null) {
                                            t = this.c;
                                        }
                                    }
                                }
                            } else if (Log.isLoggable("PhenotypeFlag", 3)) {
                                String valueOf = String.valueOf(a());
                                Log.d("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
                            }
                            t = null;
                            if (t != null) {
                            }
                        }
                        this.e = t;
                        this.d = i2;
                    } else {
                        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
                    }
                }
            }
        }
        return this.e;
    }

    @DexIgnore
    public /* synthetic */ pk2(vk2 vk2, String str, Object obj, rk2 rk2) {
        this(vk2, str, obj);
    }

    @DexIgnore
    public final String a(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @DexIgnore
    public final String a() {
        return a(this.a.c);
    }

    @DexIgnore
    public static pk2<Long> b(vk2 vk2, String str, long j) {
        return new rk2(vk2, str, Long.valueOf(j));
    }

    @DexIgnore
    public static pk2<Boolean> b(vk2 vk2, String str, boolean z) {
        return new uk2(vk2, str, Boolean.valueOf(z));
    }

    @DexIgnore
    public static pk2<Double> b(vk2 vk2, String str, double d2) {
        return new tk2(vk2, str, Double.valueOf(d2));
    }

    @DexIgnore
    public static pk2<String> b(vk2 vk2, String str, String str2) {
        return new wk2(vk2, str, str2);
    }
}
