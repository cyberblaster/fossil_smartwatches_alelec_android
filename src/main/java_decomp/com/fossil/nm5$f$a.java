package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1$1", f = "ThemesViewModel.kt", l = {28, 29, 31}, m = "invokeSuspend")
public final class nm5$f$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemesViewModel.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nm5$f$a(ThemesViewModel.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        nm5$f$a nm5_f_a = new nm5$f$a(this.this$0, xe6);
        nm5_f_a.p$ = (il6) obj;
        return nm5_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((nm5$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a9 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b1  */
    public final Object invokeSuspend(Object obj) {
        ThemesViewModel themesViewModel;
        ArrayList arrayList;
        il6 il6;
        ThemesViewModel themesViewModel2;
        String str;
        Object listStyleById;
        il6 il62;
        ThemesViewModel themesViewModel3;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il63 = this.p$;
            themesViewModel3 = this.this$0.this$0;
            ThemeRepository e = themesViewModel3.g;
            this.L$0 = il63;
            this.L$1 = themesViewModel3;
            this.label = 1;
            Object listTheme = e.getListTheme(this);
            if (listTheme == a) {
                return a;
            }
            Object obj2 = listTheme;
            il62 = il63;
            obj = obj2;
        } else if (i == 1) {
            themesViewModel3 = (ThemesViewModel) this.L$1;
            il62 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            themesViewModel2 = (ThemesViewModel) this.L$1;
            il6 = (il6) this.L$0;
            nc6.a(obj);
            str = (String) obj;
            if (str == null) {
                str = "default";
            }
            themesViewModel2.c = str;
            ThemesViewModel themesViewModel4 = this.this$0.this$0;
            themesViewModel4.d = themesViewModel4.c;
            ThemesViewModel themesViewModel5 = this.this$0.this$0;
            ThemeRepository e2 = themesViewModel5.g;
            String b = this.this$0.this$0.c;
            this.L$0 = il6;
            this.L$1 = themesViewModel5;
            this.label = 3;
            listStyleById = e2.getListStyleById(b, this);
            if (listStyleById != a) {
                return a;
            }
            themesViewModel = themesViewModel5;
            obj = listStyleById;
            arrayList = (ArrayList) obj;
            if (arrayList == null) {
            }
            themesViewModel.b = arrayList;
            return cd6.a;
        } else if (i == 3) {
            themesViewModel = (ThemesViewModel) this.L$1;
            il6 il64 = (il6) this.L$0;
            nc6.a(obj);
            arrayList = (ArrayList) obj;
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            themesViewModel.b = arrayList;
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj != null) {
            themesViewModel3.a = (ArrayList) obj;
            themesViewModel2 = this.this$0.this$0;
            ThemeRepository e3 = themesViewModel2.g;
            this.L$0 = il62;
            this.L$1 = themesViewModel2;
            this.label = 2;
            obj = e3.getCurrentThemeId(this);
            if (obj == a) {
                return a;
            }
            il6 = il62;
            str = (String) obj;
            if (str == null) {
            }
            themesViewModel2.c = str;
            ThemesViewModel themesViewModel42 = this.this$0.this$0;
            themesViewModel42.d = themesViewModel42.c;
            ThemesViewModel themesViewModel52 = this.this$0.this$0;
            ThemeRepository e22 = themesViewModel52.g;
            String b2 = this.this$0.this$0.c;
            this.L$0 = il6;
            this.L$1 = themesViewModel52;
            this.label = 3;
            listStyleById = e22.getListStyleById(b2, this);
            if (listStyleById != a) {
            }
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.data.model.Theme> /* = java.util.ArrayList<com.portfolio.platform.data.model.Theme> */");
        }
    }
}
