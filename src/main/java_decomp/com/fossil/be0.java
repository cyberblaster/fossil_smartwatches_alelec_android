package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class be0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<be0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            parcel.setDataPosition(0);
            if (wg6.a(readString, y50.class.getCanonicalName())) {
                return y50.CREATOR.createFromParcel(parcel);
            }
            if (wg6.a(readString, p80.class.getCanonicalName())) {
                return p80.CREATOR.createFromParcel(parcel);
            }
            if (wg6.a(readString, m50.class.getCanonicalName())) {
                return m50.CREATOR.createFromParcel(parcel);
            }
            throw new IllegalArgumentException("Invalid parcel!");
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new be0[i];
        }
    }

    @DexIgnore
    public be0() {
        mi0.A.g();
    }

    @DexIgnore
    public final void a(w40 w40) {
    }

    @DexIgnore
    public abstract JSONObject b();

    @DexIgnore
    public final JSONObject c() {
        try {
            JSONObject put = new JSONObject().put("push", new JSONObject().put("set", b()));
            wg6.a(put, "JSONObject().put(UIScrip\u2026ET, getAssignmentJSON()))");
            return put;
        } catch (JSONException e) {
            qs0.h.a(e);
            return new JSONObject();
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getCanonicalName());
        }
    }

    @DexIgnore
    public be0(Parcel parcel) {
        mi0.A.g();
        parcel.readString();
    }
}
