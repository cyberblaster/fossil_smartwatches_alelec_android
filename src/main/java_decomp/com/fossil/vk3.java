package com.fossil;

import com.fossil.cn3;
import com.fossil.ym3;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vk3<K, V> implements zm3<K, V> {
    @DexIgnore
    public transient Collection<Map.Entry<K, V>> a;
    @DexIgnore
    public transient Set<K> b;
    @DexIgnore
    public transient dn3<K> c;
    @DexIgnore
    public transient Collection<V> d;
    @DexIgnore
    public transient Map<K, Collection<V>> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends cn3.c<K, V> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public zm3<K, V> a() {
            return vk3.this;
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return vk3.this.entryIterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends vk3<K, V>.b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public c(vk3 vk3) {
            super();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return yn3.a((Set<?>) this, obj);
        }

        @DexIgnore
        public int hashCode() {
            return yn3.a((Set<?>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AbstractCollection<V> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void clear() {
            vk3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return vk3.this.containsValue(obj);
        }

        @DexIgnore
        public Iterator<V> iterator() {
            return vk3.this.valueIterator();
        }

        @DexIgnore
        public int size() {
            return vk3.this.size();
        }
    }

    @DexIgnore
    public Map<K, Collection<V>> asMap() {
        Map<K, Collection<V>> map = this.e;
        if (map != null) {
            return map;
        }
        Map<K, Collection<V>> createAsMap = createAsMap();
        this.e = createAsMap;
        return createAsMap;
    }

    @DexIgnore
    public boolean containsEntry(Object obj, Object obj2) {
        Collection collection = (Collection) asMap().get(obj);
        return collection != null && collection.contains(obj2);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        for (Collection contains : asMap().values()) {
            if (contains.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Map<K, Collection<V>> createAsMap();

    @DexIgnore
    public Collection<Map.Entry<K, V>> createEntries() {
        if (this instanceof xn3) {
            return new c();
        }
        return new b();
    }

    @DexIgnore
    public Set<K> createKeySet() {
        return new ym3.e(asMap());
    }

    @DexIgnore
    public dn3<K> createKeys() {
        return new cn3.d(this);
    }

    @DexIgnore
    public Collection<V> createValues() {
        return new d();
    }

    @DexIgnore
    public Collection<Map.Entry<K, V>> entries() {
        Collection<Map.Entry<K, V>> collection = this.a;
        if (collection != null) {
            return collection;
        }
        Collection<Map.Entry<K, V>> createEntries = createEntries();
        this.a = createEntries;
        return createEntries;
    }

    @DexIgnore
    public abstract Iterator<Map.Entry<K, V>> entryIterator();

    @DexIgnore
    public boolean equals(Object obj) {
        return cn3.a((zm3<?, ?>) this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return asMap().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public Set<K> keySet() {
        Set<K> set = this.b;
        if (set != null) {
            return set;
        }
        Set<K> createKeySet = createKeySet();
        this.b = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    public dn3<K> keys() {
        dn3<K> dn3 = this.c;
        if (dn3 != null) {
            return dn3;
        }
        dn3<K> createKeys = createKeys();
        this.c = createKeys;
        return createKeys;
    }

    @DexIgnore
    public abstract boolean put(K k, V v);

    @DexIgnore
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        jk3.a(iterable);
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.isEmpty() || !get(k).addAll(collection)) {
                return false;
            }
            return true;
        }
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext() || !qm3.a(get(k), it)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean remove(Object obj, Object obj2) {
        Collection collection = (Collection) asMap().get(obj);
        return collection != null && collection.remove(obj2);
    }

    @DexIgnore
    public abstract Collection<V> replaceValues(K k, Iterable<? extends V> iterable);

    @DexIgnore
    public String toString() {
        return asMap().toString();
    }

    @DexIgnore
    public abstract Iterator<V> valueIterator();

    @DexIgnore
    public Collection<V> values() {
        Collection<V> collection = this.d;
        if (collection != null) {
            return collection;
        }
        Collection<V> createValues = createValues();
        this.d = createValues;
        return createValues;
    }

    @DexIgnore
    public boolean putAll(zm3<? extends K, ? extends V> zm3) {
        boolean z = false;
        for (Map.Entry next : zm3.entries()) {
            z |= put(next.getKey(), next.getValue());
        }
        return z;
    }
}
