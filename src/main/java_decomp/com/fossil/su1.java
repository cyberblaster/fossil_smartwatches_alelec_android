package com.fossil;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class su1 implements Comparator {
    @DexIgnore
    public static /* final */ Comparator a; // = new su1();

    @DexIgnore
    public final int compare(Object obj, Object obj2) {
        return ((Scope) obj).p().compareTo(((Scope) obj2).p());
    }
}
