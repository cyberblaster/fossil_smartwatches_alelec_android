package com.fossil;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi0 {
    @DexIgnore
    public static /* final */ mi0 A; // = new mi0();
    @DexIgnore
    public static /* final */ Charset a; // = ej6.a;
    @DexIgnore
    public static /* final */ Locale b;
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ UUID d; // = UUID.fromString("00001805-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID e; // = UUID.fromString("00002a2b-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID f; // = UUID.fromString("00002a0f-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID g; // = UUID.fromString("3dda0001-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ UUID i; // = UUID.fromString("3dda0002-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID j; // = UUID.fromString("3dda0003-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID k; // = UUID.fromString("3dda0004-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID l; // = UUID.fromString("3dda0005-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID m; // = UUID.fromString("3dda0006-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID n; // = UUID.fromString("3dda0007-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID o; // = UUID.fromString("00002a24-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID p; // = UUID.fromString("00002a25-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID q; // = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID r; // = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID s; // = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID t; // = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ nq0 u; // = nq0.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY;
    @DexIgnore
    public static /* final */ w40 v; // = new w40((byte) 0, (byte) 0);
    @DexIgnore
    public static /* final */ w40 w; // = new w40((byte) 1, (byte) 0);
    @DexIgnore
    public static /* final */ w40 x; // = new w40((byte) 1, (byte) 0);
    @DexIgnore
    public static /* final */ w40 y; // = new w40((byte) 0, (byte) 0);
    @DexIgnore
    public static /* final */ it0 z; // = new it0(12, 12, 0, 600);

    /*
    static {
        Locale locale = Locale.ROOT;
        wg6.a(locale, "Locale.ROOT");
        b = locale;
        String property = System.getProperty("line.separator");
        if (property == null) {
            property = "\n";
        }
        c = property;
        UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
        String a2 = xj6.a("3dda0001-957f-7d4a-34a6-74696673696d", "-", "", false, 4, (Object) null);
        Locale locale2 = b;
        if (a2 != null) {
            String upperCase = a2.toUpperCase(locale2);
            wg6.a(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            h = upperCase;
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }
    */

    @DexIgnore
    public final UUID a() {
        return s;
    }

    @DexIgnore
    public final UUID b() {
        return e;
    }

    @DexIgnore
    public final UUID c() {
        return f;
    }

    @DexIgnore
    public final UUID d() {
        return d;
    }

    @DexIgnore
    public final nq0 e() {
        return u;
    }

    @DexIgnore
    public final Charset f() {
        return a;
    }

    @DexIgnore
    public final w40 g() {
        return y;
    }

    @DexIgnore
    public final Locale h() {
        return b;
    }

    @DexIgnore
    public final w40 i() {
        return v;
    }

    @DexIgnore
    public final UUID j() {
        return q;
    }

    @DexIgnore
    public final UUID k() {
        return o;
    }

    @DexIgnore
    public final UUID l() {
        return p;
    }

    @DexIgnore
    public final UUID m() {
        return r;
    }

    @DexIgnore
    public final w40 n() {
        return w;
    }

    @DexIgnore
    public final UUID o() {
        return t;
    }

    @DexIgnore
    public final it0 p() {
        return z;
    }

    @DexIgnore
    public final UUID q() {
        return m;
    }

    @DexIgnore
    public final UUID r() {
        return l;
    }

    @DexIgnore
    public final UUID s() {
        return i;
    }

    @DexIgnore
    public final UUID t() {
        return j;
    }

    @DexIgnore
    public final UUID u() {
        return k;
    }

    @DexIgnore
    public final UUID v() {
        return n;
    }

    @DexIgnore
    public final UUID w() {
        return g;
    }

    @DexIgnore
    public final String x() {
        return h;
    }

    @DexIgnore
    public final String y() {
        return c;
    }

    @DexIgnore
    public final w40 z() {
        return x;
    }
}
