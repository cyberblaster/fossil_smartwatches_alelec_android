package com.fossil;

import android.os.RemoteException;
import com.fossil.qw1;
import com.fossil.uw1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz1 extends pz1<Boolean> {
    @DexIgnore
    public /* final */ uw1.a<?> b;

    @DexIgnore
    public sz1(uw1.a<?> aVar, rc3<Boolean> rc3) {
        super(4, rc3);
        this.b = aVar;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(k02 k02, boolean z) {
    }

    @DexIgnore
    public final iv1[] b(qw1.a<?> aVar) {
        zy1 zy1 = aVar.l().get(this.b);
        if (zy1 == null) {
            return null;
        }
        return zy1.a.c();
    }

    @DexIgnore
    public final boolean c(qw1.a<?> aVar) {
        zy1 zy1 = aVar.l().get(this.b);
        return zy1 != null && zy1.a.d();
    }

    @DexIgnore
    public final void d(qw1.a<?> aVar) throws RemoteException {
        zy1 remove = aVar.l().remove(this.b);
        if (remove != null) {
            remove.b.a(aVar.f(), this.a);
            remove.a.a();
            return;
        }
        this.a.b(false);
    }
}
