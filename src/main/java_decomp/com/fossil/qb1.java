package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.r40;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb1 extends p21 {
    @DexIgnore
    public long S;
    @DexIgnore
    public long T;
    @DexIgnore
    public String U;
    @DexIgnore
    public String V;
    @DexIgnore
    public r40 W;
    @DexIgnore
    public int X;
    @DexIgnore
    public boolean Y;
    @DexIgnore
    public /* final */ String Z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public qb1(ue1 ue1, q41 q41, byte[] bArr, String str) {
        super(ue1, q41, eh1.OTA, false, lk1.b.b(r11.t, w31.OTA), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192);
        ue1 ue12 = ue1;
        this.Z = str;
        String firmwareVersion = q41.a().getFirmwareVersion();
        this.U = firmwareVersion.length() == 0 ? "unknown" : firmwareVersion;
        this.V = "unknown";
        this.W = new r40(ue1.d(), ue12.t, "", "", "", (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262112);
        this.Y = true;
    }

    @DexIgnore
    public Object d() {
        return this.W.getFirmwareVersion();
    }

    @DexIgnore
    public void h() {
        this.U = this.x.a().getFirmwareVersion();
        String str = this.Z;
        if (str == null || !xj6.b(this.U, str, true)) {
            if (fm0.f.a()) {
                this.w.u = true;
                a((hg6<? super if1, cd6>) new bx0(this));
            }
            if (fm0.f.b(this.x.a())) {
                if1.a((if1) this, (if1) new hm1(this.w, this.x, this.R, false, 23131, 0.001f, this.z), (hg6) new b61(this), (hg6) new y71(this), (ig6) new v91(this), (hg6) null, (hg6) null, 48, (Object) null);
                return;
            }
            super.h();
            return;
        }
        this.V = this.U;
        this.W = this.x.a();
        this.Y = false;
        a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
    }

    @DexIgnore
    public JSONObject i() {
        JSONObject i = super.i();
        bm0 bm0 = bm0.TARGET_FIRMWARE;
        Object obj = this.Z;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        return cw0.a(i, bm0, obj);
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(cw0.a(cw0.a(super.k(), bm0.RECONNECT_DURATION_IN_MS, (Object) Long.valueOf(Math.max(this.T - this.S, 0))), bm0.OLD_FIRMWARE, (Object) this.U), bm0.NEW_FIRMWARE, (Object) this.V);
    }

    @DexIgnore
    public ol0 p() {
        return new kg0(this.C, this.w);
    }

    @DexIgnore
    public void q() {
        if (fm0.f.a()) {
            if1.a((if1) this, (qv0) new zk1(this.w), (hg6) vy0.a, (hg6) p01.a, (ig6) null, (hg6) new j21(this), (hg6) f41.a, 8, (Object) null);
            return;
        }
        t();
    }

    @DexIgnore
    public final r40 s() {
        return this.W;
    }

    @DexIgnore
    public final void t() {
        int i = this.X;
        if (i < 2) {
            this.X = i + 1;
            d41 d41 = d41.AUTO_CONNECT;
            fm0.f.f();
            HashMap a = he6.a(new lc6[]{qc6.a(d41, false), qc6.a(d41.CONNECTION_TIME_OUT, 55000L)});
            if (this.S == 0) {
                this.S = System.currentTimeMillis();
            }
            if1.a((if1) this, (if1) new al0(this.w, this.x, a, this.z), (hg6) new wr0(this), (hg6) new pt0(this), (ig6) null, (hg6) null, (hg6) gv0.a, 24, (Object) null);
            return;
        }
        this.Y = true;
        a(this.v);
    }

    @DexIgnore
    public void a(h91 h91) {
        if (eq0.a[h91.ordinal()] == 1) {
            qv0 qv0 = this.b;
            if (qv0 == null || qv0.t) {
                if1 if1 = this.n;
                if (if1 == null || if1.t) {
                    a(sk1.CONNECTION_DROPPED);
                }
            }
        }
    }
}
