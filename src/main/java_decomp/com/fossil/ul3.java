package com.fossil;

import com.fossil.bm3;
import java.util.Arrays;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ul3<K, V> extends bm3<K, V> implements zk3<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends bm3.e {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public b(ul3<?, ?> ul3) {
            super(ul3);
        }

        @DexIgnore
        public Object readResolve() {
            return createMap(new a());
        }
    }

    @DexIgnore
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    public static <K, V> ul3<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if (map instanceof ul3) {
            ul3<K, V> ul3 = (ul3) map;
            if (!ul3.isPartialView()) {
                return ul3;
            }
        }
        return copyOf(map.entrySet());
    }

    @DexIgnore
    public static <K, V> ul3<K, V> of() {
        return on3.EMPTY;
    }

    @DexIgnore
    @Deprecated
    public V forcePut(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract ul3<V, K> inverse();

    @DexIgnore
    public Object writeReplace() {
        return new b(this);
    }

    @DexIgnore
    public static <K, V> ul3<K, V> of(K k, V v) {
        return new zn3(k, v);
    }

    @DexIgnore
    public static <K, V> ul3<K, V> of(K k, V v, K k2, V v2) {
        return on3.fromEntries(bm3.entryOf(k, v), bm3.entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> ul3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return on3.fromEntries(bm3.entryOf(k, v), bm3.entryOf(k2, v2), bm3.entryOf(k3, v3));
    }

    @DexIgnore
    public im3<V> values() {
        return inverse().keySet();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends bm3.b<K, V> {
        @DexIgnore
        public a<K, V> a(K k, V v) {
            super.a(k, v);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            super.a(entry);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Map<? extends K, ? extends V> map) {
            super.a(map);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a(iterable);
            return this;
        }

        @DexIgnore
        public ul3<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return ul3.of();
            }
            boolean z = true;
            if (i == 1) {
                return ul3.of(this.b[0].getKey(), this.b[0].getValue());
            }
            if (this.a != null) {
                if (this.d) {
                    this.b = (cm3[]) in3.a((T[]) this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, jn3.from(this.a).onResultOf(ym3.c()));
            }
            if (this.c != this.b.length) {
                z = false;
            }
            this.d = z;
            return on3.fromEntryArray(this.c, this.b);
        }
    }

    @DexIgnore
    public static <K, V> ul3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) pm3.a(iterable, (T[]) bm3.EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return on3.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static <K, V> ul3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return on3.fromEntries(bm3.entryOf(k, v), bm3.entryOf(k2, v2), bm3.entryOf(k3, v3), bm3.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> ul3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return on3.fromEntries(bm3.entryOf(k, v), bm3.entryOf(k2, v2), bm3.entryOf(k3, v3), bm3.entryOf(k4, v4), bm3.entryOf(k5, v5));
    }
}
