package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq6 implements dq6 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ ds6 b;
    @DexIgnore
    public /* final */ ht6 c; // = new a();
    @DexIgnore
    public pq6 d;
    @DexIgnore
    public /* final */ yq6 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ht6 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void i() {
            xq6.this.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends er6 {
        @DexIgnore
        public /* final */ eq6 b;

        /*
        static {
            Class<xq6> cls = xq6.class;
        }
        */

        @DexIgnore
        public b(eq6 eq6) {
            super("OkHttp %s", xq6.this.c());
            this.b = eq6;
        }

        @DexIgnore
        public void a(ExecutorService executorService) {
            try {
                executorService.execute(this);
            } catch (RejectedExecutionException e) {
                InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                interruptedIOException.initCause(e);
                xq6.this.d.a((dq6) xq6.this, (IOException) interruptedIOException);
                this.b.onFailure(xq6.this, interruptedIOException);
                xq6.this.a.h().b(this);
            } catch (Throwable th) {
                xq6.this.a.h().b(this);
                throw th;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004a A[Catch:{ all -> 0x003d }] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006a A[Catch:{ all -> 0x003d }] */
        public void b() {
            IOException e;
            xq6.this.c.g();
            boolean z = true;
            try {
                Response b2 = xq6.this.b();
                if (xq6.this.b.b()) {
                    try {
                        this.b.onFailure(xq6.this, new IOException("Canceled"));
                    } catch (IOException e2) {
                        e = e2;
                        try {
                            IOException a = xq6.this.a(e);
                            if (!z) {
                                at6 d = at6.d();
                                d.a(4, "Callback failure for " + xq6.this.d(), (Throwable) a);
                            } else {
                                xq6.this.d.a((dq6) xq6.this, a);
                                this.b.onFailure(xq6.this, a);
                            }
                            xq6.this.a.h().b(this);
                        } catch (Throwable th) {
                            xq6.this.a.h().b(this);
                            throw th;
                        }
                    }
                } else {
                    this.b.onResponse(xq6.this, b2);
                }
            } catch (IOException e3) {
                e = e3;
                z = false;
                IOException a2 = xq6.this.a(e);
                if (!z) {
                }
                xq6.this.a.h().b(this);
            }
            xq6.this.a.h().b(this);
        }

        @DexIgnore
        public xq6 c() {
            return xq6.this;
        }

        @DexIgnore
        public String d() {
            return xq6.this.e.g().g();
        }
    }

    @DexIgnore
    public xq6(OkHttpClient okHttpClient, yq6 yq6, boolean z) {
        this.a = okHttpClient;
        this.e = yq6;
        this.f = z;
        this.b = new ds6(okHttpClient, z);
        this.c.a((long) okHttpClient.b(), TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public Response b() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.a.n());
        arrayList.add(this.b);
        arrayList.add(new ur6(this.a.g()));
        arrayList.add(new hr6(this.a.o()));
        arrayList.add(new nr6(this.a));
        if (!this.f) {
            arrayList.addAll(this.a.p());
        }
        arrayList.add(new vr6(this.f));
        return new as6(arrayList, (tr6) null, (wr6) null, (pr6) null, 0, this.e, this, this.d, this.a.d(), this.a.z(), this.a.D()).a(this.e);
    }

    @DexIgnore
    public String c() {
        return this.e.g().m();
    }

    @DexIgnore
    public void cancel() {
        this.b.a();
    }

    @DexIgnore
    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append(v() ? "canceled " : "");
        sb.append(this.f ? "web socket" : "call");
        sb.append(" to ");
        sb.append(c());
        return sb.toString();
    }

    @DexIgnore
    public Response s() throws IOException {
        synchronized (this) {
            if (!this.g) {
                this.g = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        a();
        this.c.g();
        this.d.b(this);
        try {
            this.a.h().a(this);
            Response b2 = b();
            if (b2 != null) {
                this.a.h().b(this);
                return b2;
            }
            throw new IOException("Canceled");
        } catch (IOException e2) {
            IOException a2 = a(e2);
            this.d.a((dq6) this, a2);
            throw a2;
        } catch (Throwable th) {
            this.a.h().b(this);
            throw th;
        }
    }

    @DexIgnore
    public yq6 t() {
        return this.e;
    }

    @DexIgnore
    public boolean v() {
        return this.b.b();
    }

    @DexIgnore
    public static xq6 a(OkHttpClient okHttpClient, yq6 yq6, boolean z) {
        xq6 xq6 = new xq6(okHttpClient, yq6, z);
        xq6.d = okHttpClient.j().a(xq6);
        return xq6;
    }

    @DexIgnore
    public xq6 clone() {
        return a(this.a, this.e, this.f);
    }

    @DexIgnore
    public IOException a(IOException iOException) {
        if (!this.c.h()) {
            return iOException;
        }
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public final void a() {
        this.b.a(at6.d().a("response.body().close()"));
    }

    @DexIgnore
    public void a(eq6 eq6) {
        synchronized (this) {
            if (!this.g) {
                this.g = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        a();
        this.d.b(this);
        this.a.h().a(new b(eq6));
    }
}
