package com.fossil;

import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import com.facebook.places.PlaceManager;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms0 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ fu0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ms0(fu0 fu0) {
        super(0);
        this.a = fu0;
    }

    @DexIgnore
    public final void invoke() {
        Object obj;
        List<BluetoothGattService> services;
        List<BluetoothGattService> services2;
        Object systemService = this.a.h.getSystemService(PlaceManager.PARAM_BLUETOOTH);
        if (systemService != null) {
            fu0 fu0 = this.a;
            fu0.a = ((BluetoothManager) systemService).openGattServer(fu0.h, fu0.e);
            for (h11 h11 : this.a.b) {
                BluetoothGattServer bluetoothGattServer = this.a.a;
                if (bluetoothGattServer != null) {
                    bluetoothGattServer.addService(h11);
                }
            }
            fu0 fu02 = this.a;
            fu02.c = fu02.a != null;
            oa1 oa1 = oa1.a;
            Object[] objArr = new Object[2];
            objArr[0] = Boolean.valueOf(this.a.c);
            BluetoothGattServer bluetoothGattServer2 = this.a.a;
            objArr[1] = (bluetoothGattServer2 == null || (services2 = bluetoothGattServer2.getServices()) == null) ? null : yd6.a(services2, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, bp0.a, 31, (Object) null);
            qs0 qs0 = qs0.h;
            String a2 = cw0.a((Enum<?>) pj1.GATT_SERVER_STARTED);
            og0 og0 = og0.GATT_SERVER_EVENT;
            String a3 = ze0.a("UUID.randomUUID().toString()");
            boolean z = this.a.c;
            JSONObject jSONObject = new JSONObject();
            bm0 bm0 = bm0.SERVICES;
            BluetoothGattServer bluetoothGattServer3 = this.a.a;
            if (bluetoothGattServer3 == null || (services = bluetoothGattServer3.getServices()) == null || (obj = yd6.a(services, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, tq0.a, 31, (Object) null)) == null) {
                obj = JSONObject.NULL;
            }
            qs0.a(new nn0(a2, og0, "", "", a3, z, (String) null, (r40) null, (bw0) null, cw0.a(jSONObject, bm0, obj), 448));
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.bluetooth.BluetoothManager");
    }
}
