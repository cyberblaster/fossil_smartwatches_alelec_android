package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f3 extends ContextWrapper {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static ArrayList<WeakReference<f3>> d;
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ Resources.Theme b;

    @DexIgnore
    public f3(Context context) {
        super(context);
        if (n3.b()) {
            this.a = new n3(this, context.getResources());
            this.b = this.a.newTheme();
            this.b.setTo(context.getTheme());
            return;
        }
        this.a = new h3(this, context.getResources());
        this.b = null;
    }

    @DexIgnore
    public static boolean a(Context context) {
        if ((context instanceof f3) || (context.getResources() instanceof h3) || (context.getResources() instanceof n3)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || n3.b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static Context b(Context context) {
        if (!a(context)) {
            return context;
        }
        synchronized (c) {
            if (d == null) {
                d = new ArrayList<>();
            } else {
                for (int size = d.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        d.remove(size);
                    }
                }
                for (int size2 = d.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = d.get(size2);
                    f3 f3Var = weakReference2 != null ? (f3) weakReference2.get() : null;
                    if (f3Var != null && f3Var.getBaseContext() == context) {
                        return f3Var;
                    }
                }
            }
            f3 f3Var2 = new f3(context);
            d.add(new WeakReference(f3Var2));
            return f3Var2;
        }
    }

    @DexIgnore
    public AssetManager getAssets() {
        return this.a.getAssets();
    }

    @DexIgnore
    public Resources getResources() {
        return this.a;
    }

    @DexIgnore
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        return theme == null ? super.getTheme() : theme;
    }

    @DexIgnore
    public void setTheme(int i) {
        Resources.Theme theme = this.b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
