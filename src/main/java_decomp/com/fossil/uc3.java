package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uc3 extends gc3 {
    @DexIgnore
    public /* final */ od3<Void> a; // = new od3<>();

    @DexIgnore
    public final boolean a() {
        return this.a.d();
    }

    @DexIgnore
    public final void b() {
        this.a.b(null);
    }

    @DexIgnore
    public final gc3 a(nc3 nc3) {
        this.a.a(new vc3(this, nc3));
        return this;
    }
}
