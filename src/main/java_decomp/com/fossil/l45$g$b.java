package com.fossil;

import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1", f = "HomeDianaCustomizePresenter.kt", l = {419}, m = "invokeSuspend")
public final class l45$g$b extends sf6 implements ig6<il6, xe6<? super List<? extends m35>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l45$g$b(HomeDianaCustomizePresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l45$g$b l45_g_b = new l45$g$b(this.this$0, xe6);
        l45_g_b.p$ = (il6) obj;
        return l45_g_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l45$g$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        xp6 xp6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            xp6 m = this.this$0.this$0.p;
            this.L$0 = il6;
            this.L$1 = m;
            this.label = 1;
            if (m.a((Object) null, this) == a) {
                return a;
            }
            xp6 = m;
        } else if (i == 1) {
            xp6 = (xp6) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            CopyOnWriteArrayList<DianaPreset> n = this.this$0.this$0.i;
            ArrayList arrayList = new ArrayList(rd6.a(n, 10));
            for (DianaPreset dianaPreset : n) {
                HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0.this$0;
                wg6.a((Object) dianaPreset, "it");
                arrayList.add(homeDianaCustomizePresenter.a(dianaPreset));
            }
            return arrayList;
        } finally {
            xp6.a((Object) null);
        }
    }
}
