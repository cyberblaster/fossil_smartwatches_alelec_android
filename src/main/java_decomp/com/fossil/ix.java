package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ix<T extends Drawable> implements rt<T>, nt {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public ix(T t) {
        q00.a(t);
        this.a = (Drawable) t;
    }

    @DexIgnore
    public void d() {
        T t = this.a;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof qx) {
            ((qx) t).e().prepareToDraw();
        }
    }

    @DexIgnore
    public final T get() {
        Drawable.ConstantState constantState = this.a.getConstantState();
        if (constantState == null) {
            return this.a;
        }
        return constantState.newDrawable();
    }
}
