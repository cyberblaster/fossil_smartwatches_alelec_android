package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b16 extends r06<ImageView> {
    @DexIgnore
    public v06 m;

    @DexIgnore
    public b16(Picasso picasso, ImageView imageView, l16 l16, int i, int i2, int i3, Drawable drawable, String str, Object obj, v06 v06, boolean z) {
        super(picasso, imageView, l16, i, i2, i3, drawable, str, obj, z);
        this.m = v06;
    }

    @DexIgnore
    public void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            ImageView imageView = (ImageView) this.c.get();
            if (imageView != null) {
                Picasso picasso = this.a;
                Bitmap bitmap2 = bitmap;
                Picasso.LoadedFrom loadedFrom2 = loadedFrom;
                j16.a(imageView, picasso.e, bitmap2, loadedFrom2, this.d, picasso.m);
                v06 v06 = this.m;
                if (v06 != null) {
                    v06.onSuccess();
                    return;
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[]{this}));
    }

    @DexIgnore
    public void b() {
        ImageView imageView = (ImageView) this.c.get();
        if (imageView != null) {
            int i = this.g;
            if (i != 0) {
                imageView.setImageResource(i);
            } else {
                Drawable drawable = this.h;
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                }
            }
            v06 v06 = this.m;
            if (v06 != null) {
                v06.onError();
            }
        }
    }

    @DexIgnore
    public void a() {
        super.a();
        if (this.m != null) {
            this.m = null;
        }
    }
}
