package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import com.fossil.q1;
import com.fossil.x1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q0 extends ActionBar {
    @DexIgnore
    public r2 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Window.Callback c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ArrayList<ActionBar.a> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Runnable g; // = new a();
    @DexIgnore
    public /* final */ Toolbar.e h; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            q0.this.n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Toolbar.e {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            return q0.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements q1.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(q1 q1Var) {
            q0 q0Var = q0.this;
            if (q0Var.c == null) {
                return;
            }
            if (q0Var.a.a()) {
                q0.this.c.onPanelClosed(108, q1Var);
            } else if (q0.this.c.onPreparePanel(0, (View) null, q1Var)) {
                q0.this.c.onMenuOpened(108, q1Var);
            }
        }

        @DexIgnore
        public boolean a(q1 q1Var, MenuItem menuItem) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends j1 {
        @DexIgnore
        public e(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public View onCreatePanelView(int i) {
            if (i == 0) {
                return new View(q0.this.a.getContext());
            }
            return super.onCreatePanelView(i);
        }

        @DexIgnore
        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                q0 q0Var = q0.this;
                if (!q0Var.b) {
                    q0Var.a.b();
                    q0.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    @DexIgnore
    public q0(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.a = new j3(toolbar, false);
        this.c = new e(callback);
        this.a.setWindowCallback(this.c);
        toolbar.setOnMenuItemClickListener(this.h);
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void a(float f2) {
        x9.a((View) this.a.k(), f2);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void c(boolean z) {
    }

    @DexIgnore
    public void d(boolean z) {
        a(z ? 4 : 0, 4);
    }

    @DexIgnore
    public void e(boolean z) {
    }

    @DexIgnore
    public boolean e() {
        return this.a.e();
    }

    @DexIgnore
    public boolean f() {
        if (!this.a.h()) {
            return false;
        }
        this.a.collapseActionView();
        return true;
    }

    @DexIgnore
    public int g() {
        return this.a.l();
    }

    @DexIgnore
    public Context h() {
        return this.a.getContext();
    }

    @DexIgnore
    public boolean i() {
        this.a.k().removeCallbacks(this.g);
        x9.a((View) this.a.k(), this.g);
        return true;
    }

    @DexIgnore
    public void j() {
        this.a.k().removeCallbacks(this.g);
    }

    @DexIgnore
    public boolean k() {
        return this.a.f();
    }

    @DexIgnore
    public final Menu l() {
        if (!this.d) {
            this.a.a((x1.a) new c(), (q1.a) new d());
            this.d = true;
        }
        return this.a.i();
    }

    @DexIgnore
    public Window.Callback m() {
        return this.c;
    }

    @DexIgnore
    public void n() {
        Menu l = l();
        q1 q1Var = l instanceof q1 ? (q1) l : null;
        if (q1Var != null) {
            q1Var.s();
        }
        try {
            l.clear();
            if (!this.c.onCreatePanelMenu(0, l) || !this.c.onPreparePanel(0, (View) null, l)) {
                l.clear();
            }
        } finally {
            if (q1Var != null) {
                q1Var.r();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements x1.a {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean a(q1 q1Var) {
            Window.Callback callback = q0.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, q1Var);
            return true;
        }

        @DexIgnore
        public void a(q1 q1Var, boolean z) {
            if (!this.a) {
                this.a = true;
                q0.this.a.g();
                Window.Callback callback = q0.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, q1Var);
                }
                this.a = false;
            }
        }
    }

    @DexIgnore
    public void a(Configuration configuration) {
        super.a(configuration);
    }

    @DexIgnore
    public void b(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.get(i).a(z);
            }
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.a.setTitle(charSequence);
    }

    @DexIgnore
    public void a(int i, int i2) {
        this.a.a((i & i2) | ((~i2) & this.a.l()));
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            k();
        }
        return true;
    }

    @DexIgnore
    public boolean a(int i, KeyEvent keyEvent) {
        Menu l = l();
        if (l == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        l.setQwertyMode(z);
        return l.performShortcut(i, keyEvent, 0);
    }
}
