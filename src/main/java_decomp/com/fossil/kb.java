package com.fossil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kb {
    @DexIgnore
    public static hb a; // = new ib();
    @DexIgnore
    public static jb b; // = null;

    @DexIgnore
    public static jb a() {
        return b;
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, i, viewGroup, z, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z, jb jbVar) {
        int i2 = 0;
        boolean z2 = viewGroup != null && z;
        if (z2) {
            i2 = viewGroup.getChildCount();
        }
        View inflate = layoutInflater.inflate(i, viewGroup, z);
        if (z2) {
            return a(jbVar, viewGroup, i2, i);
        }
        return a(jbVar, inflate, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(jb jbVar, View[] viewArr, int i) {
        return a.a(jbVar, viewArr, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(jb jbVar, View view, int i) {
        return a.a(jbVar, view, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Activity activity, int i) {
        return a(activity, i, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Activity activity, int i, jb jbVar) {
        activity.setContentView(i);
        return a(jbVar, (ViewGroup) activity.getWindow().getDecorView().findViewById(16908290), 0, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(jb jbVar, ViewGroup viewGroup, int i, int i2) {
        int childCount = viewGroup.getChildCount();
        int i3 = childCount - i;
        if (i3 == 1) {
            return a(jbVar, viewGroup.getChildAt(childCount - 1), i2);
        }
        View[] viewArr = new View[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            viewArr[i4] = viewGroup.getChildAt(i4 + i);
        }
        return a(jbVar, viewArr, i2);
    }
}
