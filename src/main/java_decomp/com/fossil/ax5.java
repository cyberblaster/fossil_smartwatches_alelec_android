package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ax5<T> {
    @DexIgnore
    public T a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends FragmentManager.g {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;
        @DexIgnore
        public /* final */ /* synthetic */ FragmentManager b;

        @DexIgnore
        public a(Fragment fragment, FragmentManager fragmentManager) {
            this.a = fragment;
            this.b = fragmentManager;
        }

        @DexIgnore
        public void g(FragmentManager fragmentManager, Fragment fragment) {
            if (Objects.equals(fragment, this.a)) {
                ax5.this.a = null;
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    public ax5(Fragment fragment, T t) {
        FragmentManager fragmentManager = fragment.getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.a(new a(fragment, fragmentManager), false);
        }
        this.a = t;
    }

    @DexIgnore
    public T a() {
        return this.a;
    }
}
