package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g55$b$b extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g55$b$b(CommuteTimeSettingsDefaultAddressPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        g55$b$b g55_b_b = new g55$b$b(this.this$0, xe6);
        g55_b_b.p$ = (il6) obj;
        return g55_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((g55$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.i().getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
