package com.fossil;

import android.content.Context;
import android.os.Bundle;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h10 implements n10 {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public h10(Object obj, Method method) {
        this.b = obj;
        this.a = method;
    }

    @DexIgnore
    public static Class a(Context context) {
        try {
            return context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement");
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static n10 b(Context context) {
        Object a2;
        Method b2;
        Class a3 = a(context);
        if (a3 == null || (a2 = a(context, a3)) == null || (b2 = b(context, a3)) == null) {
            return null;
        }
        return new h10(a2, b2);
    }

    @DexIgnore
    public static Object a(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke(cls, new Object[]{context});
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public void a(String str, Bundle bundle) {
        a("fab", str, bundle);
    }

    @DexIgnore
    public static Method b(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("logEventInternal", new Class[]{String.class, String.class, Bundle.class});
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        try {
            this.a.invoke(this.b, new Object[]{str, str2, bundle});
        } catch (Exception unused) {
        }
    }
}
