package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cd4 extends bd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131362081, 1);
        y.put(2131361851, 2);
        y.put(2131363218, 3);
        y.put(2131362470, 4);
        y.put(2131362278, 5);
        y.put(2131363038, 6);
        y.put(2131363024, 7);
        y.put(2131363028, 8);
        y.put(2131363343, 9);
        y.put(2131362289, 10);
        y.put(2131363040, 11);
        y.put(2131363026, 12);
        y.put(2131363030, 13);
        y.put(2131362171, 14);
        y.put(2131362277, 15);
        y.put(2131363037, 16);
        y.put(2131363023, 17);
        y.put(2131363027, 18);
        y.put(2131363005, 19);
        y.put(2131362285, 20);
        y.put(2131363039, 21);
        y.put(2131363025, 22);
        y.put(2131363029, 23);
    }
    */

    @DexIgnore
    public cd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 24, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public cd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[1], objArr[14], objArr[15], objArr[5], objArr[20], objArr[10], objArr[4], objArr[0], objArr[19], objArr[17], objArr[7], objArr[22], objArr[12], objArr[18], objArr[8], objArr[23], objArr[13], objArr[16], objArr[6], objArr[21], objArr[11], objArr[3], objArr[9]);
        this.w = -1;
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
