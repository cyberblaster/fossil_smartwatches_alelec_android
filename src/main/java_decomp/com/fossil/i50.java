package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i50 extends b50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<i50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return (i50) b50.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new i50[i];
        }
    }

    @DexIgnore
    public i50(byte b, byte b2, Set<? extends k70> set, boolean z, boolean z2) {
        super(b, b2, set, true, z, z2);
        if (!(!d().isEmpty())) {
            throw new IllegalArgumentException("Day Of Week must not be empty.");
        }
    }

    @DexIgnore
    public final Set<k70> getDaysOfWeek() {
        return d();
    }

    @DexIgnore
    public i50(byte b, byte b2, Set<? extends k70> set) {
        this(b, b2, set, false, true);
    }
}
