package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum NotificationFlag {
    SILENT((byte) 1),
    IMPORTANT((byte) 2),
    PRE_EXISTING((byte) 4),
    ALLOW_USER_POSITIVE_ACTION((byte) 8),
    ALLOW_USER_NEGATIVE_ACTION((byte) 16),
    ALLOW_USER_REPLY_ACTION((byte) 32);
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final NotificationFlag[] a(String[] strArr) {
            NotificationFlag notificationFlag;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                NotificationFlag[] values = NotificationFlag.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        notificationFlag = null;
                        break;
                    }
                    notificationFlag = values[i];
                    if (wg6.a(cw0.a((Enum<?>) notificationFlag), str) || wg6.a(notificationFlag.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (notificationFlag != null) {
                    arrayList.add(notificationFlag);
                }
            }
            Object[] array = arrayList.toArray(new NotificationFlag[0]);
            if (array != null) {
                return (NotificationFlag[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public NotificationFlag(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
