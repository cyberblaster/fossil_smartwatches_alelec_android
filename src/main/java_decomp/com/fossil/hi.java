package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hi implements li {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Object[] b;

    @DexIgnore
    public hi(String str, Object[] objArr) {
        this.a = str;
        this.b = objArr;
    }

    @DexIgnore
    public void a(ki kiVar) {
        a(kiVar, this.b);
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public int a() {
        Object[] objArr = this.b;
        if (objArr == null) {
            return 0;
        }
        return objArr.length;
    }

    @DexIgnore
    public static void a(ki kiVar, Object[] objArr) {
        if (objArr != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                Object obj = objArr[i];
                i++;
                a(kiVar, i, obj);
            }
        }
    }

    @DexIgnore
    public hi(String str) {
        this(str, (Object[]) null);
    }

    @DexIgnore
    public static void a(ki kiVar, int i, Object obj) {
        if (obj == null) {
            kiVar.a(i);
        } else if (obj instanceof byte[]) {
            kiVar.a(i, (byte[]) obj);
        } else if (obj instanceof Float) {
            kiVar.a(i, (double) ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            kiVar.a(i, ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            kiVar.a(i, ((Long) obj).longValue());
        } else if (obj instanceof Integer) {
            kiVar.a(i, (long) ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            kiVar.a(i, (long) ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            kiVar.a(i, (long) ((Byte) obj).byteValue());
        } else if (obj instanceof String) {
            kiVar.a(i, (String) obj);
        } else if (obj instanceof Boolean) {
            kiVar.a(i, ((Boolean) obj).booleanValue() ? 1 : 0);
        } else {
            throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte," + " string");
        }
    }
}
