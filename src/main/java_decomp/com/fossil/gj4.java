package com.fossil;

import com.portfolio.platform.data.model.InstalledApp;
import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj4 implements vr {
    @DexIgnore
    public /* final */ InstalledApp b;

    @DexIgnore
    public gj4(InstalledApp installedApp) {
        wg6.b(installedApp, "installedApp");
        this.b = installedApp;
    }

    @DexIgnore
    public final InstalledApp a() {
        return this.b;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        wg6.b(messageDigest, "messageDigest");
        String identifier = this.b.getIdentifier();
        wg6.a((Object) identifier, "installedApp.identifier");
        Charset charset = vr.a;
        wg6.a((Object) charset, "Key.CHARSET");
        if (identifier != null) {
            byte[] bytes = identifier.getBytes(charset);
            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }
}
