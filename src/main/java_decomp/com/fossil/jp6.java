package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp6 extends jm6 implements np6, Executor {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater e; // = AtomicIntegerFieldUpdater.newUpdater(jp6.class, "inFlightTasks");
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<Runnable> a; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ hp6 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ pp6 d;
    @DexIgnore
    public volatile int inFlightTasks; // = 0;

    @DexIgnore
    public jp6(hp6 hp6, int i, pp6 pp6) {
        wg6.b(hp6, "dispatcher");
        wg6.b(pp6, "taskMode");
        this.b = hp6;
        this.c = i;
        this.d = pp6;
    }

    @DexIgnore
    public void a(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        a(runnable, false);
    }

    @DexIgnore
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        wg6.b(runnable, Constants.COMMAND);
        a(runnable, false);
    }

    @DexIgnore
    public void m() {
        Runnable poll = this.a.poll();
        if (poll != null) {
            this.b.a(poll, this, true);
            return;
        }
        e.decrementAndGet(this);
        Runnable poll2 = this.a.poll();
        if (poll2 != null) {
            a(poll2, true);
        }
    }

    @DexIgnore
    public pp6 n() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "[dispatcher = " + this.b + ']';
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0010  */
    public final void a(Runnable runnable, boolean z) {
        while (e.incrementAndGet(this) > this.c) {
            this.a.add(runnable);
            if (e.decrementAndGet(this) >= this.c || (runnable = this.a.poll()) == null) {
                return;
            }
            while (e.incrementAndGet(this) > this.c) {
            }
        }
        this.b.a(runnable, this, z);
    }
}
