package com.fossil;

import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class hb5$b$b extends sf6 implements ig6<il6, xe6<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $data;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hb5$b$b(ArrayList arrayList, xe6 xe6) {
        super(2, xe6);
        this.$data = arrayList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        hb5$b$b hb5_b_b = new hb5$b$b(this.$data, xe6);
        hb5_b_b.p$ = (il6) obj;
        return hb5_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((hb5$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        ArrayList<ArrayList<BarChart.b>> c;
        ArrayList<BarChart.b> arrayList;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            Iterator it = this.$data.iterator();
            int i = 0;
            if (!it.hasNext()) {
                obj2 = null;
            } else {
                obj2 = it.next();
                if (it.hasNext()) {
                    ArrayList<BarChart.b> arrayList2 = ((BarChart.a) obj2).c().get(0);
                    wg6.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                    int i2 = 0;
                    for (BarChart.b e : arrayList2) {
                        i2 += hf6.a(e.e()).intValue();
                    }
                    Integer a = hf6.a(i2);
                    do {
                        Object next = it.next();
                        ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).c().get(0);
                        wg6.a((Object) arrayList3, "it.mListOfBarPoints[0]");
                        int i3 = 0;
                        for (BarChart.b e2 : arrayList3) {
                            i3 += hf6.a(e2.e()).intValue();
                        }
                        Integer a2 = hf6.a(i3);
                        if (a.compareTo(a2) < 0) {
                            obj2 = next;
                            a = a2;
                        }
                    } while (it.hasNext());
                }
            }
            BarChart.a aVar = (BarChart.a) obj2;
            if (aVar == null || (c = aVar.c()) == null || (arrayList = c.get(0)) == null) {
                return null;
            }
            for (BarChart.b e3 : arrayList) {
                i += hf6.a(e3.e()).intValue();
            }
            return hf6.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
