package com.fossil;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface uy4 extends k24<ty4> {
    @DexIgnore
    void J();

    @DexIgnore
    void N(boolean z);

    @DexIgnore
    void a(Cursor cursor);

    @DexIgnore
    void a(List<wx4> list, FilterQueryProvider filterQueryProvider);

    @DexIgnore
    void close();
}
