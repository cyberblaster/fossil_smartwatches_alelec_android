package com.fossil;

import com.fossil.ym3;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sk3<K, V> extends vk3<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 2447537837011683357L;
    @DexIgnore
    public transient Map<K, Collection<V>> f;
    @DexIgnore
    public transient int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sk3<K, V>.d<V> {
        @DexIgnore
        public a(sk3 sk3) {
            super();
        }

        @DexIgnore
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends sk3<K, V>.d<Map.Entry<K, V>> {
        @DexIgnore
        public b(sk3 sk3) {
            super();
        }

        @DexIgnore
        public Map.Entry<K, V> a(K k, V v) {
            return ym3.a(k, v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ym3.g<K, Collection<V>> {
        @DexIgnore
        public /* final */ transient Map<K, Collection<V>> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ym3.d<K, Collection<V>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public Map<K, Collection<V>> a() {
                return c.this;
            }

            @DexIgnore
            public boolean contains(Object obj) {
                return cl3.a((Collection<?>) c.this.c.entrySet(), obj);
            }

            @DexIgnore
            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new b();
            }

            @DexIgnore
            public boolean remove(Object obj) {
                if (!contains(obj)) {
                    return false;
                }
                sk3.this.b(((Map.Entry) obj).getKey());
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Iterator<Map.Entry<K, Collection<V>>> {
            @DexIgnore
            public /* final */ Iterator<Map.Entry<K, Collection<V>>> a; // = c.this.c.entrySet().iterator();
            @DexIgnore
            public Collection<V> b;

            @DexIgnore
            public b() {
            }

            @DexIgnore
            public boolean hasNext() {
                return this.a.hasNext();
            }

            @DexIgnore
            public void remove() {
                this.a.remove();
                sk3.access$220(sk3.this, this.b.size());
                this.b.clear();
            }

            @DexIgnore
            public Map.Entry<K, Collection<V>> next() {
                Map.Entry next = this.a.next();
                this.b = (Collection) next.getValue();
                return c.this.a(next);
            }
        }

        @DexIgnore
        public c(Map<K, Collection<V>> map) {
            this.c = map;
        }

        @DexIgnore
        public Set<Map.Entry<K, Collection<V>>> a() {
            return new a();
        }

        @DexIgnore
        public void clear() {
            if (this.c == sk3.this.f) {
                sk3.this.clear();
            } else {
                qm3.a((Iterator<?>) new b());
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj) {
            return ym3.d(this.c, obj);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || this.c.equals(obj);
        }

        @DexIgnore
        public int hashCode() {
            return this.c.hashCode();
        }

        @DexIgnore
        public Set<K> keySet() {
            return sk3.this.keySet();
        }

        @DexIgnore
        public int size() {
            return this.c.size();
        }

        @DexIgnore
        public String toString() {
            return this.c.toString();
        }

        @DexIgnore
        public Map.Entry<K, Collection<V>> a(Map.Entry<K, Collection<V>> entry) {
            K key = entry.getKey();
            return ym3.a(key, sk3.this.wrapCollection(key, entry.getValue()));
        }

        @DexIgnore
        public Collection<V> get(Object obj) {
            Collection collection = (Collection) ym3.e(this.c, obj);
            if (collection == null) {
                return null;
            }
            return sk3.this.wrapCollection(obj, collection);
        }

        @DexIgnore
        public Collection<V> remove(Object obj) {
            Collection remove = this.c.remove(obj);
            if (remove == null) {
                return null;
            }
            Collection<V> createCollection = sk3.this.createCollection();
            createCollection.addAll(remove);
            sk3.access$220(sk3.this, remove.size());
            remove.clear();
            return createCollection;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class d<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> a;
        @DexIgnore
        public K b; // = null;
        @DexIgnore
        public Collection<V> c; // = null;
        @DexIgnore
        public Iterator<V> d; // = qm3.c();

        @DexIgnore
        public d() {
            this.a = sk3.this.f.entrySet().iterator();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext() || this.d.hasNext();
        }

        @DexIgnore
        public T next() {
            if (!this.d.hasNext()) {
                Map.Entry next = this.a.next();
                this.b = next.getKey();
                this.c = (Collection) next.getValue();
                this.d = this.c.iterator();
            }
            return a(this.b, this.d.next());
        }

        @DexIgnore
        public void remove() {
            this.d.remove();
            if (this.c.isEmpty()) {
                this.a.remove();
            }
            sk3.access$210(sk3.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ym3.e<K, Collection<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<K> {
            @DexIgnore
            public Map.Entry<K, Collection<V>> a;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator b;

            @DexIgnore
            public a(Iterator it) {
                this.b = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            public K next() {
                this.a = (Map.Entry) this.b.next();
                return this.a.getKey();
            }

            @DexIgnore
            public void remove() {
                bl3.a(this.a != null);
                Collection value = this.a.getValue();
                this.b.remove();
                sk3.access$220(sk3.this, value.size());
                value.clear();
            }
        }

        @DexIgnore
        public e(Map<K, Collection<V>> map) {
            super(map);
        }

        @DexIgnore
        public void clear() {
            qm3.a((Iterator<?>) iterator());
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            return a().keySet().containsAll(collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || a().keySet().equals(obj);
        }

        @DexIgnore
        public int hashCode() {
            return a().keySet().hashCode();
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return new a(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int i;
            Collection collection = (Collection) a().remove(obj);
            if (collection != null) {
                i = collection.size();
                collection.clear();
                sk3.access$220(sk3.this, i);
            } else {
                i = 0;
            }
            if (i > 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends sk3<K, V>.j implements RandomAccess {
        @DexIgnore
        public f(sk3 sk3, K k, List<V> list, sk3<K, V>.i iVar) {
            super(k, list, iVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends sk3<K, V>.c implements SortedMap<K, Collection<V>> {
        @DexIgnore
        public SortedSet<K> e;

        @DexIgnore
        public g(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        public SortedSet<K> c() {
            return new h(d());
        }

        @DexIgnore
        public Comparator<? super K> comparator() {
            return d().comparator();
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> d() {
            return (SortedMap) this.c;
        }

        @DexIgnore
        public K firstKey() {
            return d().firstKey();
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> headMap(K k) {
            return new g(d().headMap(k));
        }

        @DexIgnore
        public K lastKey() {
            return d().lastKey();
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> subMap(K k, K k2) {
            return new g(d().subMap(k, k2));
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> tailMap(K k) {
            return new g(d().tailMap(k));
        }

        @DexIgnore
        public SortedSet<K> keySet() {
            SortedSet<K> sortedSet = this.e;
            if (sortedSet != null) {
                return sortedSet;
            }
            SortedSet<K> c = c();
            this.e = c;
            return c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends sk3<K, V>.e implements SortedSet<K> {
        @DexIgnore
        public h(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> b() {
            return (SortedMap) super.a();
        }

        @DexIgnore
        public Comparator<? super K> comparator() {
            return b().comparator();
        }

        @DexIgnore
        public K first() {
            return b().firstKey();
        }

        @DexIgnore
        public SortedSet<K> headSet(K k) {
            return new h(b().headMap(k));
        }

        @DexIgnore
        public K last() {
            return b().lastKey();
        }

        @DexIgnore
        public SortedSet<K> subSet(K k, K k2) {
            return new h(b().subMap(k, k2));
        }

        @DexIgnore
        public SortedSet<K> tailSet(K k) {
            return new h(b().tailMap(k));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends sk3<K, V>.i implements Set<V> {
        @DexIgnore
        public k(K k, Set<V> set) {
            super(k, set, (sk3<K, V>.i) null);
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean a = yn3.a((Set<?>) (Set) this.b, collection);
            if (a) {
                sk3.access$212(sk3.this, this.b.size() - size);
                f();
            }
            return a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends sk3<K, V>.i implements SortedSet<V> {
        @DexIgnore
        public l(K k, SortedSet<V> sortedSet, sk3<K, V>.i iVar) {
            super(k, sortedSet, iVar);
        }

        @DexIgnore
        public Comparator<? super V> comparator() {
            return g().comparator();
        }

        @DexIgnore
        public V first() {
            e();
            return g().first();
        }

        @DexIgnore
        public SortedSet<V> g() {
            return (SortedSet) c();
        }

        @DexIgnore
        public SortedSet<V> headSet(V v) {
            e();
            return new l(d(), g().headSet(v), b() == null ? this : b());
        }

        @DexIgnore
        public V last() {
            e();
            return g().last();
        }

        @DexIgnore
        public SortedSet<V> subSet(V v, V v2) {
            e();
            return new l(d(), g().subSet(v, v2), b() == null ? this : b());
        }

        @DexIgnore
        public SortedSet<V> tailSet(V v) {
            e();
            return new l(d(), g().tailSet(v), b() == null ? this : b());
        }
    }

    @DexIgnore
    public sk3(Map<K, Collection<V>> map) {
        jk3.a(map.isEmpty());
        this.f = map;
    }

    @DexIgnore
    public static /* synthetic */ int access$208(sk3 sk3) {
        int i2 = sk3.g;
        sk3.g = i2 + 1;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$210(sk3 sk3) {
        int i2 = sk3.g;
        sk3.g = i2 - 1;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$212(sk3 sk3, int i2) {
        int i3 = sk3.g + i2;
        sk3.g = i3;
        return i3;
    }

    @DexIgnore
    public static /* synthetic */ int access$220(sk3 sk3, int i2) {
        int i3 = sk3.g - i2;
        sk3.g = i3;
        return i3;
    }

    @DexIgnore
    public final Collection<V> a(K k2) {
        Collection<V> collection = this.f.get(k2);
        if (collection != null) {
            return collection;
        }
        Collection<V> createCollection = createCollection(k2);
        this.f.put(k2, createCollection);
        return createCollection;
    }

    @DexIgnore
    public final void b(Object obj) {
        Collection collection = (Collection) ym3.f(this.f, obj);
        if (collection != null) {
            int size = collection.size();
            collection.clear();
            this.g -= size;
        }
    }

    @DexIgnore
    public Map<K, Collection<V>> backingMap() {
        return this.f;
    }

    @DexIgnore
    public void clear() {
        for (Collection<V> clear : this.f.values()) {
            clear.clear();
        }
        this.f.clear();
        this.g = 0;
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return this.f.containsKey(obj);
    }

    @DexIgnore
    public Map<K, Collection<V>> createAsMap() {
        Map<K, Collection<V>> map = this.f;
        return map instanceof SortedMap ? new g((SortedMap) map) : new c(map);
    }

    @DexIgnore
    public abstract Collection<V> createCollection();

    @DexIgnore
    public Collection<V> createCollection(K k2) {
        return createCollection();
    }

    @DexIgnore
    public Set<K> createKeySet() {
        Map<K, Collection<V>> map = this.f;
        return map instanceof SortedMap ? new h((SortedMap) map) : new e(map);
    }

    @DexIgnore
    public Collection<V> createUnmodifiableEmptyCollection() {
        return unmodifiableCollectionSubclass(createCollection());
    }

    @DexIgnore
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> entryIterator() {
        return new b(this);
    }

    @DexIgnore
    public Collection<V> get(K k2) {
        Collection collection = this.f.get(k2);
        if (collection == null) {
            collection = createCollection(k2);
        }
        return wrapCollection(k2, collection);
    }

    @DexIgnore
    public boolean put(K k2, V v) {
        Collection collection = this.f.get(k2);
        if (collection == null) {
            Collection createCollection = createCollection(k2);
            if (createCollection.add(v)) {
                this.g++;
                this.f.put(k2, createCollection);
                return true;
            }
            throw new AssertionError("New Collection violated the Collection spec");
        } else if (!collection.add(v)) {
            return false;
        } else {
            this.g++;
            return true;
        }
    }

    @DexIgnore
    public Collection<V> removeAll(Object obj) {
        Collection remove = this.f.remove(obj);
        if (remove == null) {
            return createUnmodifiableEmptyCollection();
        }
        Collection createCollection = createCollection();
        createCollection.addAll(remove);
        this.g -= remove.size();
        remove.clear();
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    public Collection<V> replaceValues(K k2, Iterable<? extends V> iterable) {
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext()) {
            return removeAll(k2);
        }
        Collection a2 = a(k2);
        Collection createCollection = createCollection();
        createCollection.addAll(a2);
        this.g -= a2.size();
        a2.clear();
        while (it.hasNext()) {
            if (a2.add(it.next())) {
                this.g++;
            }
        }
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    public final void setMap(Map<K, Collection<V>> map) {
        this.f = map;
        this.g = 0;
        for (Collection next : map.values()) {
            jk3.a(!next.isEmpty());
            this.g += next.size();
        }
    }

    @DexIgnore
    public int size() {
        return this.g;
    }

    @DexIgnore
    public Collection<V> unmodifiableCollectionSubclass(Collection<V> collection) {
        if (collection instanceof SortedSet) {
            return Collections.unmodifiableSortedSet((SortedSet) collection);
        }
        if (collection instanceof Set) {
            return Collections.unmodifiableSet((Set) collection);
        }
        if (collection instanceof List) {
            return Collections.unmodifiableList((List) collection);
        }
        return Collections.unmodifiableCollection(collection);
    }

    @DexIgnore
    public Iterator<V> valueIterator() {
        return new a(this);
    }

    @DexIgnore
    public Collection<V> values() {
        return super.values();
    }

    @DexIgnore
    public Collection<V> wrapCollection(K k2, Collection<V> collection) {
        if (collection instanceof SortedSet) {
            return new l(k2, (SortedSet) collection, (sk3<K, V>.i) null);
        }
        if (collection instanceof Set) {
            return new k(k2, (Set) collection);
        }
        if (collection instanceof List) {
            return a(k2, (List) collection, (sk3<K, V>.i) null);
        }
        return new i(k2, collection, (sk3<K, V>.i) null);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends AbstractCollection<V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public Collection<V> b;
        @DexIgnore
        public /* final */ sk3<K, V>.i c;
        @DexIgnore
        public /* final */ Collection<V> d;

        @DexIgnore
        public i(K k, Collection<V> collection, sk3<K, V>.i iVar) {
            Collection<V> collection2;
            this.a = k;
            this.b = collection;
            this.c = iVar;
            if (iVar == null) {
                collection2 = null;
            } else {
                collection2 = iVar.c();
            }
            this.d = collection2;
        }

        @DexIgnore
        public void a() {
            sk3<K, V>.i iVar = this.c;
            if (iVar != null) {
                iVar.a();
            } else {
                sk3.this.f.put(this.a, this.b);
            }
        }

        @DexIgnore
        public boolean add(V v) {
            e();
            boolean isEmpty = this.b.isEmpty();
            boolean add = this.b.add(v);
            if (add) {
                sk3.access$208(sk3.this);
                if (isEmpty) {
                    a();
                }
            }
            return add;
        }

        @DexIgnore
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.b.addAll(collection);
            if (addAll) {
                sk3.access$212(sk3.this, this.b.size() - size);
                if (size == 0) {
                    a();
                }
            }
            return addAll;
        }

        @DexIgnore
        public sk3<K, V>.i b() {
            return this.c;
        }

        @DexIgnore
        public Collection<V> c() {
            return this.b;
        }

        @DexIgnore
        public void clear() {
            int size = size();
            if (size != 0) {
                this.b.clear();
                sk3.access$220(sk3.this, size);
                f();
            }
        }

        @DexIgnore
        public boolean contains(Object obj) {
            e();
            return this.b.contains(obj);
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            e();
            return this.b.containsAll(collection);
        }

        @DexIgnore
        public K d() {
            return this.a;
        }

        @DexIgnore
        public void e() {
            Collection<V> collection;
            sk3<K, V>.i iVar = this.c;
            if (iVar != null) {
                iVar.e();
                if (this.c.c() != this.d) {
                    throw new ConcurrentModificationException();
                }
            } else if (this.b.isEmpty() && (collection = (Collection) sk3.this.f.get(this.a)) != null) {
                this.b = collection;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            e();
            return this.b.equals(obj);
        }

        @DexIgnore
        public void f() {
            sk3<K, V>.i iVar = this.c;
            if (iVar != null) {
                iVar.f();
            } else if (this.b.isEmpty()) {
                sk3.this.f.remove(this.a);
            }
        }

        @DexIgnore
        public int hashCode() {
            e();
            return this.b.hashCode();
        }

        @DexIgnore
        public Iterator<V> iterator() {
            e();
            return new a();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            e();
            boolean remove = this.b.remove(obj);
            if (remove) {
                sk3.access$210(sk3.this);
                f();
            }
            return remove;
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean removeAll = this.b.removeAll(collection);
            if (removeAll) {
                sk3.access$212(sk3.this, this.b.size() - size);
                f();
            }
            return removeAll;
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            jk3.a(collection);
            int size = size();
            boolean retainAll = this.b.retainAll(collection);
            if (retainAll) {
                sk3.access$212(sk3.this, this.b.size() - size);
                f();
            }
            return retainAll;
        }

        @DexIgnore
        public int size() {
            e();
            return this.b.size();
        }

        @DexIgnore
        public String toString() {
            e();
            return this.b.toString();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<V> {
            @DexIgnore
            public /* final */ Iterator<V> a;
            @DexIgnore
            public /* final */ Collection<V> b; // = i.this.b;

            @DexIgnore
            public a() {
                this.a = sk3.this.a(i.this.b);
            }

            @DexIgnore
            public Iterator<V> a() {
                b();
                return this.a;
            }

            @DexIgnore
            public void b() {
                i.this.e();
                if (i.this.b != this.b) {
                    throw new ConcurrentModificationException();
                }
            }

            @DexIgnore
            public boolean hasNext() {
                b();
                return this.a.hasNext();
            }

            @DexIgnore
            public V next() {
                b();
                return this.a.next();
            }

            @DexIgnore
            public void remove() {
                this.a.remove();
                sk3.access$210(sk3.this);
                i.this.f();
            }

            @DexIgnore
            public a(Iterator<V> it) {
                this.a = it;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends sk3<K, V>.i implements List<V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends sk3<K, V>.i.a implements ListIterator<V> {
            @DexIgnore
            public a() {
                super();
            }

            @DexIgnore
            public void add(V v) {
                boolean isEmpty = j.this.isEmpty();
                c().add(v);
                sk3.access$208(sk3.this);
                if (isEmpty) {
                    j.this.a();
                }
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.sk3$i$a, com.fossil.sk3$j$a] */
            public final ListIterator<V> c() {
                return (ListIterator) a();
            }

            @DexIgnore
            public boolean hasPrevious() {
                return c().hasPrevious();
            }

            @DexIgnore
            public int nextIndex() {
                return c().nextIndex();
            }

            @DexIgnore
            public V previous() {
                return c().previous();
            }

            @DexIgnore
            public int previousIndex() {
                return c().previousIndex();
            }

            @DexIgnore
            public void set(V v) {
                c().set(v);
            }

            @DexIgnore
            public a(int i) {
                super(j.this.g().listIterator(i));
            }
        }

        @DexIgnore
        public j(K k, List<V> list, sk3<K, V>.i iVar) {
            super(k, list, iVar);
        }

        @DexIgnore
        public void add(int i, V v) {
            e();
            boolean isEmpty = c().isEmpty();
            g().add(i, v);
            sk3.access$208(sk3.this);
            if (isEmpty) {
                a();
            }
        }

        @DexIgnore
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = g().addAll(i, collection);
            if (addAll) {
                sk3.access$212(sk3.this, c().size() - size);
                if (size == 0) {
                    a();
                }
            }
            return addAll;
        }

        @DexIgnore
        public List<V> g() {
            return (List) c();
        }

        @DexIgnore
        public V get(int i) {
            e();
            return g().get(i);
        }

        @DexIgnore
        public int indexOf(Object obj) {
            e();
            return g().indexOf(obj);
        }

        @DexIgnore
        public int lastIndexOf(Object obj) {
            e();
            return g().lastIndexOf(obj);
        }

        @DexIgnore
        public ListIterator<V> listIterator() {
            e();
            return new a();
        }

        @DexIgnore
        public V remove(int i) {
            e();
            V remove = g().remove(i);
            sk3.access$210(sk3.this);
            f();
            return remove;
        }

        @DexIgnore
        public V set(int i, V v) {
            e();
            return g().set(i, v);
        }

        @DexIgnore
        public List<V> subList(int i, int i2) {
            e();
            return sk3.this.a(d(), g().subList(i, i2), b() == null ? this : b());
        }

        @DexIgnore
        public ListIterator<V> listIterator(int i) {
            e();
            return new a(i);
        }
    }

    @DexIgnore
    public final List<V> a(K k2, List<V> list, sk3<K, V>.i iVar) {
        return list instanceof RandomAccess ? new f(this, k2, list, iVar) : new j(k2, list, iVar);
    }

    @DexIgnore
    public final Iterator<V> a(Collection<V> collection) {
        return collection instanceof List ? ((List) collection).listIterator() : collection.iterator();
    }
}
