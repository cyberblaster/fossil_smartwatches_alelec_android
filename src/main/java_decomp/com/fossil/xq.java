package com.fossil;

import android.content.Context;
import com.fossil.ku;
import com.fossil.qy;
import com.fossil.su;
import com.fossil.wq;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq {
    @DexIgnore
    public /* final */ Map<Class<?>, gr<?, ?>> a; // = new p4();
    @DexIgnore
    public gt b;
    @DexIgnore
    public au c;
    @DexIgnore
    public xt d;
    @DexIgnore
    public ru e;
    @DexIgnore
    public uu f;
    @DexIgnore
    public uu g;
    @DexIgnore
    public ku.a h;
    @DexIgnore
    public su i;
    @DexIgnore
    public iy j;
    @DexIgnore
    public int k; // = 4;
    @DexIgnore
    public wq.a l; // = new a(this);
    @DexIgnore
    public qy.b m;
    @DexIgnore
    public uu n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public List<mz<Object>> p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements wq.a {
        @DexIgnore
        public a(xq xqVar) {
        }

        @DexIgnore
        public nz build() {
            return new nz();
        }
    }

    @DexIgnore
    public void a(qy.b bVar) {
        this.m = bVar;
    }

    @DexIgnore
    public wq a(Context context) {
        Context context2 = context;
        if (this.f == null) {
            this.f = uu.g();
        }
        if (this.g == null) {
            this.g = uu.e();
        }
        if (this.n == null) {
            this.n = uu.c();
        }
        if (this.i == null) {
            this.i = new su.a(context2).a();
        }
        if (this.j == null) {
            this.j = new ky();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new gu((long) b2);
            } else {
                this.c = new bu();
            }
        }
        if (this.d == null) {
            this.d = new fu(this.i.a());
        }
        if (this.e == null) {
            this.e = new qu((long) this.i.c());
        }
        if (this.h == null) {
            this.h = new pu(context2);
        }
        if (this.b == null) {
            this.b = new gt(this.e, this.h, this.g, this.f, uu.h(), this.n, this.o);
        }
        List<mz<Object>> list = this.p;
        if (list == null) {
            this.p = Collections.emptyList();
        } else {
            this.p = Collections.unmodifiableList(list);
        }
        return new wq(context, this.b, this.e, this.c, this.d, new qy(this.m), this.j, this.k, this.l, this.a, this.p, this.q, this.r);
    }
}
