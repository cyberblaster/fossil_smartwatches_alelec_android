package com.fossil;

import com.fossil.s00;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tu {
    @DexIgnore
    public /* final */ n00<vr, String> a; // = new n00<>(1000);
    @DexIgnore
    public /* final */ v8<b> b; // = s00.a(10, new a(this));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements s00.d<b> {
        @DexIgnore
        public a(tu tuVar) {
        }

        @DexIgnore
        public b create() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements s00.f {
        @DexIgnore
        public /* final */ MessageDigest a;
        @DexIgnore
        public /* final */ u00 b; // = u00.b();

        @DexIgnore
        public b(MessageDigest messageDigest) {
            this.a = messageDigest;
        }

        @DexIgnore
        public u00 d() {
            return this.b;
        }
    }

    @DexIgnore
    public final String a(vr vrVar) {
        b a2 = this.b.a();
        q00.a(a2);
        b bVar = a2;
        try {
            vrVar.a(bVar.a);
            return r00.a(bVar.a.digest());
        } finally {
            this.b.a(bVar);
        }
    }

    @DexIgnore
    public String b(vr vrVar) {
        String a2;
        synchronized (this.a) {
            a2 = this.a.a(vrVar);
        }
        if (a2 == null) {
            a2 = a(vrVar);
        }
        synchronized (this.a) {
            this.a.b(vrVar, a2);
        }
        return a2;
    }
}
