package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h24 implements Factory<kn4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public h24(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static h24 a(b14 b14) {
        return new h24(b14);
    }

    @DexIgnore
    public static kn4 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static kn4 c(b14 b14) {
        kn4 r = b14.r();
        z76.a(r, "Cannot return null from a non-@Nullable @Provides method");
        return r;
    }

    @DexIgnore
    public kn4 get() {
        return b(this.a);
    }
}
