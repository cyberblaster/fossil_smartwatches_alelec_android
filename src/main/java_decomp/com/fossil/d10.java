package com.fossil;

import android.content.Context;
import android.os.Looper;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d10 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ab6 b;

    @DexIgnore
    public d10(Context context, ab6 ab6) {
        this.a = context;
        this.b = ab6;
    }

    @DexIgnore
    public w10 a() throws IOException {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return new w10(this.a, new c20(), new n96(), new oa6(this.a, this.b.a(), "session_analytics.tap", "session_analytics_to_send"));
        }
        throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
    }
}
