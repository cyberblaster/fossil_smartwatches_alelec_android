package com.fossil;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt5 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ qt5 b;

    @DexIgnore
    public rt5(BaseActivity baseActivity, qt5 qt5) {
        wg6.b(baseActivity, "mContext");
        wg6.b(qt5, "mView");
        this.a = baseActivity;
        this.b = qt5;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final qt5 b() {
        return this.b;
    }
}
