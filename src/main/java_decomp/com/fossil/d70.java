package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum d70 {
    DC(rg1.DC),
    FTC(rg1.FTC),
    FTD(rg1.FTD),
    AUT(rg1.AUTHENTICATION),
    ASYNC(rg1.ASYNC),
    FTD_1(rg1.FTD_1),
    HEART_RATE(rg1.HEART_RATE);
    
    @DexIgnore
    public /* final */ rg1 a;

    @DexIgnore
    public d70(rg1 rg1) {
        this.a = rg1;
    }

    @DexIgnore
    public final rg1 a() {
        return this.a;
    }
}
