package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx3<E> extends qw3<E> {
    @DexIgnore
    public static /* final */ hx3<Object> c; // = new hx3<>();
    @DexIgnore
    public /* final */ List<E> b;

    /*
    static {
        c.l();
    }
    */

    @DexIgnore
    public hx3() {
        this(new ArrayList(10));
    }

    @DexIgnore
    public void add(int i, E e) {
        a();
        this.b.add(i, e);
        this.modCount++;
    }

    @DexIgnore
    public E get(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    public E remove(int i) {
        a();
        E remove = this.b.remove(i);
        this.modCount++;
        return remove;
    }

    @DexIgnore
    public E set(int i, E e) {
        a();
        E e2 = this.b.set(i, e);
        this.modCount++;
        return e2;
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }

    @DexIgnore
    public hx3(List<E> list) {
        this.b = list;
    }

    @DexIgnore
    public static <E> hx3<E> b() {
        return c;
    }

    @DexIgnore
    public hx3<E> b(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.b);
            return new hx3<>(arrayList);
        }
        throw new IllegalArgumentException();
    }
}
