package com.fossil;

import android.location.Location;
import com.fossil.s04;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ns4 extends m24<b, d, c> {
    @DexIgnore
    public /* final */ s04 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ DeviceLocation a;

        @DexIgnore
        public d(DeviceLocation deviceLocation) {
            wg6.b(deviceLocation, "deviceLocation");
            this.a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements s04.b {
        @DexIgnore
        public /* final */ /* synthetic */ ns4 a;

        @DexIgnore
        public e(ns4 ns4) {
            this.a = ns4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r9v5, types: [com.fossil.ns4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r9v8, types: [com.fossil.ns4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r9v12, types: [com.fossil.ns4, com.portfolio.platform.CoroutineUseCase] */
        public void a(Location location, int i) {
            if (i >= 0) {
                FLogger.INSTANCE.getLocal().d("LoadLocation", "onLocationUpdated OK");
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    if (accuracy <= 500.0f) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("LoadLocation", "onLocationUpdated - accuracy: " + accuracy);
                        try {
                            DeviceLocation deviceLocation = new DeviceLocation(PortfolioApp.get.instance().e(), location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                            zm4.p.a().g().saveDeviceLocation(deviceLocation);
                            this.a.a(new d(deviceLocation));
                        } catch (Exception unused) {
                            this.a.a(new c());
                        }
                        this.a.d().b((s04.b) this);
                        return;
                    }
                    return;
                }
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("LoadLocation", "onLocationUpdated error - error: " + i);
            this.a.d().b((s04.b) this);
            this.a.a(new c());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public ns4(s04 s04) {
        wg6.b(s04, "mfLocationService");
        jk3.a(s04, "mfLocationService cannot be null!", new Object[0]);
        wg6.a((Object) s04, "checkNotNull(mfLocationS\u2026Service cannot be null!\")");
        this.d = s04;
    }

    @DexIgnore
    public String c() {
        return "LoadLocation";
    }

    @DexIgnore
    public final s04 d() {
        return this.d;
    }

    @DexIgnore
    public Object a(b bVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d("LoadLocation", "running UseCase");
        this.d.a((s04.b) new e(this));
        return new Object();
    }
}
