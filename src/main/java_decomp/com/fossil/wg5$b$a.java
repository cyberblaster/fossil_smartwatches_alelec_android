package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg5$b$a<T> implements ld<yx5<? extends List<MFSleepDay>>> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewWeekPresenter.b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1$2$1", f = "SleepOverviewWeekPresenter.kt", l = {68}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wg5$b$a this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wg5$b$a$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1$2$1$1", f = "SleepOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.wg5$b$a$a$a  reason: collision with other inner class name */
        public static final class C0052a extends sf6 implements ig6<il6, xe6<? super BarChart.c>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0052a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0052a aVar = new C0052a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0052a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    SleepOverviewWeekPresenter sleepOverviewWeekPresenter = this.this$0.this$0.a.this$0;
                    Date d = SleepOverviewWeekPresenter.d(sleepOverviewWeekPresenter);
                    yx5 yx5 = (yx5) this.this$0.this$0.a.this$0.g.a();
                    return sleepOverviewWeekPresenter.a(d, (List<MFSleepDay>) yx5 != null ? (List) yx5.d() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wg5$b$a wg5_b_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = wg5_b_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            SleepOverviewWeekPresenter sleepOverviewWeekPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                SleepOverviewWeekPresenter sleepOverviewWeekPresenter2 = this.this$0.a.this$0;
                dl6 a2 = sleepOverviewWeekPresenter2.b();
                C0052a aVar = new C0052a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = sleepOverviewWeekPresenter2;
                this.label = 1;
                obj = gk6.a(a2, aVar, this);
                if (obj == a) {
                    return a;
                }
                sleepOverviewWeekPresenter = sleepOverviewWeekPresenter2;
            } else if (i == 1) {
                sleepOverviewWeekPresenter = (SleepOverviewWeekPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            sleepOverviewWeekPresenter.h = (BarChart.c) obj;
            this.this$0.a.this$0.i.a(this.this$0.a.this$0.h);
            return cd6.a;
        }
    }

    @DexIgnore
    public wg5$b$a(SleepOverviewWeekPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<? extends List<MFSleepDay>> yx5) {
        wh4 a2 = yx5.a();
        List list = (List) yx5.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - mSleepSummaries -- sleepSummaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepOverviewWeekPresenter", sb.toString());
        if (a2 != wh4.DATABASE_LOADING) {
            rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
        }
    }
}
