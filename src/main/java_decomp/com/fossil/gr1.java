package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gr1 implements Runnable {
    @DexIgnore
    public /* final */ lr1 a;
    @DexIgnore
    public /* final */ rp1 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Runnable d;

    @DexIgnore
    public gr1(lr1 lr1, rp1 rp1, int i, Runnable runnable) {
        this.a = lr1;
        this.b = rp1;
        this.c = i;
        this.d = runnable;
    }

    @DexIgnore
    public static Runnable a(lr1 lr1, rp1 rp1, int i, Runnable runnable) {
        return new gr1(lr1, rp1, i, runnable);
    }

    @DexIgnore
    public void run() {
        lr1.a(this.a, this.b, this.c, this.d);
    }
}
