package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.u12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cz2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<cz2> CREATOR; // = new jz2();
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;

        @DexIgnore
        public final a a(float f) {
            this.a = f;
            return this;
        }

        @DexIgnore
        public final a b(float f) {
            this.b = f;
            return this;
        }

        @DexIgnore
        public final cz2 a() {
            return new cz2(this.b, this.a);
        }
    }

    @DexIgnore
    public cz2(float f, float f2) {
        boolean z = -90.0f <= f && f <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f);
        w12.a(z, (Object) sb.toString());
        this.a = f + LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.b = (((double) f2) <= 0.0d ? (f2 % 360.0f) + 360.0f : f2) % 360.0f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cz2)) {
            return false;
        }
        cz2 cz2 = (cz2) obj;
        return Float.floatToIntBits(this.a) == Float.floatToIntBits(cz2.a) && Float.floatToIntBits(this.b) == Float.floatToIntBits(cz2.b);
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(Float.valueOf(this.a), Float.valueOf(this.b));
    }

    @DexIgnore
    public String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("tilt", Float.valueOf(this.a));
        a2.a("bearing", Float.valueOf(this.b));
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a);
        g22.a(parcel, 3, this.b);
        g22.a(parcel, a2);
    }
}
