package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.u12;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o82 extends e22 implements ew1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<o82> CREATOR; // = new p82();
    @DexIgnore
    public /* final */ List<f72> a;
    @DexIgnore
    public /* final */ Status b;

    @DexIgnore
    public o82(List<f72> list, Status status) {
        this.a = Collections.unmodifiableList(list);
        this.b = status;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof o82) {
                o82 o82 = (o82) obj;
                if (this.b.equals(o82.b) && u12.a(this.a, o82.a)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(this.b, this.a);
    }

    @DexIgnore
    public Status o() {
        return this.b;
    }

    @DexIgnore
    public List<f72> p() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("status", this.b);
        a2.a("dataSources", this.a);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.c(parcel, 1, p(), false);
        g22.a(parcel, 2, (Parcelable) o(), i, false);
        g22.a(parcel, a2);
    }
}
