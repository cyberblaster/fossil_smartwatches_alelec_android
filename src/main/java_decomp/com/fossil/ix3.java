package com.fossil;

import com.fossil.sw3;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix3 extends sw3 {
    @DexIgnore
    public static /* final */ int[] b;
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ sw3 left;
    @DexIgnore
    public /* final */ int leftLength;
    @DexIgnore
    public /* final */ sw3 right;
    @DexIgnore
    public /* final */ int totalLength;
    @DexIgnore
    public /* final */ int treeDepth;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Stack<sw3> a;

        @DexIgnore
        public b() {
            this.a = new Stack<>();
        }

        @DexIgnore
        public final void b(sw3 sw3) {
            int a2 = a(sw3.size());
            int i = ix3.b[a2 + 1];
            if (this.a.isEmpty() || this.a.peek().size() >= i) {
                this.a.push(sw3);
                return;
            }
            int i2 = ix3.b[a2];
            sw3 pop = this.a.pop();
            while (!this.a.isEmpty() && this.a.peek().size() < i2) {
                pop = new ix3(this.a.pop(), pop);
            }
            ix3 ix3 = new ix3(pop, sw3);
            while (!this.a.isEmpty()) {
                if (this.a.peek().size() >= ix3.b[a(ix3.size()) + 1]) {
                    break;
                }
                ix3 = new ix3(this.a.pop(), ix3);
            }
            this.a.push(ix3);
        }

        @DexIgnore
        public final sw3 a(sw3 sw3, sw3 sw32) {
            a(sw3);
            a(sw32);
            sw3 pop = this.a.pop();
            while (!this.a.isEmpty()) {
                pop = new ix3(this.a.pop(), pop);
            }
            return pop;
        }

        @DexIgnore
        public final void a(sw3 sw3) {
            if (sw3.isBalanced()) {
                b(sw3);
            } else if (sw3 instanceof ix3) {
                ix3 ix3 = (ix3) sw3;
                a(ix3.left);
                a(ix3.right);
            } else {
                throw new IllegalArgumentException("Has a new type of ByteString been created? Found " + sw3.getClass());
            }
        }

        @DexIgnore
        public final int a(int i) {
            int binarySearch = Arrays.binarySearch(ix3.b, i);
            return binarySearch < 0 ? (-(binarySearch + 1)) - 1 : binarySearch;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Iterator<sw3.g> {
        @DexIgnore
        public /* final */ Stack<ix3> a;
        @DexIgnore
        public sw3.g b;

        @DexIgnore
        public final sw3.g a(sw3 sw3) {
            while (sw3 instanceof ix3) {
                ix3 ix3 = (ix3) sw3;
                this.a.push(ix3);
                sw3 = ix3.left;
            }
            return (sw3.g) sw3;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b != null;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public c(sw3 sw3) {
            this.a = new Stack<>();
            this.b = a(sw3);
        }

        @DexIgnore
        public sw3.g next() {
            sw3.g gVar = this.b;
            if (gVar != null) {
                this.b = a();
                return gVar;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public final sw3.g a() {
            while (!this.a.isEmpty()) {
                sw3.g a2 = a(this.a.pop().right);
                if (!a2.isEmpty()) {
                    return a2;
                }
            }
            return null;
        }
    }

    /*
    static {
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 1;
        while (i > 0) {
            arrayList.add(Integer.valueOf(i));
            int i3 = i2 + i;
            i2 = i;
            i = i3;
        }
        arrayList.add(Integer.MAX_VALUE);
        b = new int[arrayList.size()];
        int i4 = 0;
        while (true) {
            int[] iArr = b;
            if (i4 < iArr.length) {
                iArr[i4] = ((Integer) arrayList.get(i4)).intValue();
                i4++;
            } else {
                return;
            }
        }
    }
    */

    @DexIgnore
    public static sw3 a(sw3 sw3, sw3 sw32) {
        int size = sw3.size();
        int size2 = sw32.size();
        byte[] bArr = new byte[(size + size2)];
        sw3.copyTo(bArr, 0, 0, size);
        sw32.copyTo(bArr, 0, size, size2);
        return sw3.wrap(bArr);
    }

    @DexIgnore
    public static sw3 concatenate(sw3 sw3, sw3 sw32) {
        if (sw32.size() == 0) {
            return sw3;
        }
        if (sw3.size() == 0) {
            return sw32;
        }
        int size = sw3.size() + sw32.size();
        if (size < 128) {
            return a(sw3, sw32);
        }
        if (sw3 instanceof ix3) {
            ix3 ix3 = (ix3) sw3;
            if (ix3.right.size() + sw32.size() < 128) {
                return new ix3(ix3.left, a(ix3.right, sw32));
            } else if (ix3.left.getTreeDepth() > ix3.right.getTreeDepth() && ix3.getTreeDepth() > sw32.getTreeDepth()) {
                return new ix3(ix3.left, new ix3(ix3.right, sw32));
            }
        }
        if (size >= b[Math.max(sw3.getTreeDepth(), sw32.getTreeDepth()) + 1]) {
            return new ix3(sw3, sw32);
        }
        return new b().a(sw3, sw32);
    }

    @DexIgnore
    public static ix3 newInstanceForTest(sw3 sw3, sw3 sw32) {
        return new ix3(sw3, sw32);
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        throw new InvalidObjectException("RopeByteStream instances are not to be serialized directly");
    }

    @DexIgnore
    public ByteBuffer asReadOnlyByteBuffer() {
        return ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer();
    }

    @DexIgnore
    public List<ByteBuffer> asReadOnlyByteBufferList() {
        ArrayList arrayList = new ArrayList();
        c cVar = new c(this);
        while (cVar.hasNext()) {
            arrayList.add(cVar.next().asReadOnlyByteBuffer());
        }
        return arrayList;
    }

    @DexIgnore
    public byte byteAt(int i) {
        sw3.checkIndex(i, this.totalLength);
        int i2 = this.leftLength;
        if (i < i2) {
            return this.left.byteAt(i);
        }
        return this.right.byteAt(i - i2);
    }

    @DexIgnore
    public void copyTo(ByteBuffer byteBuffer) {
        this.left.copyTo(byteBuffer);
        this.right.copyTo(byteBuffer);
    }

    @DexIgnore
    public void copyToInternal(byte[] bArr, int i, int i2, int i3) {
        int i4 = i + i3;
        int i5 = this.leftLength;
        if (i4 <= i5) {
            this.left.copyToInternal(bArr, i, i2, i3);
        } else if (i >= i5) {
            this.right.copyToInternal(bArr, i - i5, i2, i3);
        } else {
            int i6 = i5 - i;
            this.left.copyToInternal(bArr, i, i2, i6);
            this.right.copyToInternal(bArr, 0, i2 + i6, i3 - i6);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof sw3)) {
            return false;
        }
        sw3 sw3 = (sw3) obj;
        if (this.totalLength != sw3.size()) {
            return false;
        }
        if (this.totalLength == 0) {
            return true;
        }
        int peekCachedHashCode = peekCachedHashCode();
        int peekCachedHashCode2 = sw3.peekCachedHashCode();
        if (peekCachedHashCode == 0 || peekCachedHashCode2 == 0 || peekCachedHashCode == peekCachedHashCode2) {
            return a(sw3);
        }
        return false;
    }

    @DexIgnore
    public int getTreeDepth() {
        return this.treeDepth;
    }

    @DexIgnore
    public boolean isBalanced() {
        return this.totalLength >= b[this.treeDepth];
    }

    @DexIgnore
    public boolean isValidUtf8() {
        int partialIsValidUtf8 = this.left.partialIsValidUtf8(0, 0, this.leftLength);
        sw3 sw3 = this.right;
        if (sw3.partialIsValidUtf8(partialIsValidUtf8, 0, sw3.size()) == 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public tw3 newCodedInput() {
        return tw3.a((InputStream) new d());
    }

    @DexIgnore
    public InputStream newInput() {
        return new d();
    }

    @DexIgnore
    public int partialHash(int i, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = this.leftLength;
        if (i4 <= i5) {
            return this.left.partialHash(i, i2, i3);
        }
        if (i2 >= i5) {
            return this.right.partialHash(i, i2 - i5, i3);
        }
        int i6 = i5 - i2;
        return this.right.partialHash(this.left.partialHash(i, i2, i6), 0, i3 - i6);
    }

    @DexIgnore
    public int partialIsValidUtf8(int i, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = this.leftLength;
        if (i4 <= i5) {
            return this.left.partialIsValidUtf8(i, i2, i3);
        }
        if (i2 >= i5) {
            return this.right.partialIsValidUtf8(i, i2 - i5, i3);
        }
        int i6 = i5 - i2;
        return this.right.partialIsValidUtf8(this.left.partialIsValidUtf8(i, i2, i6), 0, i3 - i6);
    }

    @DexIgnore
    public int size() {
        return this.totalLength;
    }

    @DexIgnore
    public sw3 substring(int i, int i2) {
        int checkRange = sw3.checkRange(i, i2, this.totalLength);
        if (checkRange == 0) {
            return sw3.EMPTY;
        }
        if (checkRange == this.totalLength) {
            return this;
        }
        int i3 = this.leftLength;
        if (i2 <= i3) {
            return this.left.substring(i, i2);
        }
        if (i >= i3) {
            return this.right.substring(i - i3, i2 - i3);
        }
        return new ix3(this.left.substring(i), this.right.substring(0, i2 - this.leftLength));
    }

    @DexIgnore
    public String toStringInternal(Charset charset) {
        return new String(toByteArray(), charset);
    }

    @DexIgnore
    public Object writeReplace() {
        return sw3.wrap(toByteArray());
    }

    @DexIgnore
    public void writeTo(OutputStream outputStream) throws IOException {
        this.left.writeTo(outputStream);
        this.right.writeTo(outputStream);
    }

    @DexIgnore
    public void writeToInternal(OutputStream outputStream, int i, int i2) throws IOException {
        int i3 = i + i2;
        int i4 = this.leftLength;
        if (i3 <= i4) {
            this.left.writeToInternal(outputStream, i, i2);
        } else if (i >= i4) {
            this.right.writeToInternal(outputStream, i - i4, i2);
        } else {
            int i5 = i4 - i;
            this.left.writeToInternal(outputStream, i, i5);
            this.right.writeToInternal(outputStream, 0, i2 - i5);
        }
    }

    @DexIgnore
    public ix3(sw3 sw3, sw3 sw32) {
        this.left = sw3;
        this.right = sw32;
        this.leftLength = sw3.size();
        this.totalLength = this.leftLength + sw32.size();
        this.treeDepth = Math.max(sw3.getTreeDepth(), sw32.getTreeDepth()) + 1;
    }

    @DexIgnore
    public void writeTo(rw3 rw3) throws IOException {
        this.left.writeTo(rw3);
        this.right.writeTo(rw3);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends InputStream {
        @DexIgnore
        public c a;
        @DexIgnore
        public sw3.g b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public d() {
            l();
        }

        @DexIgnore
        public final int a(byte[] bArr, int i, int i2) {
            int i3 = i;
            int i4 = i2;
            while (true) {
                if (i4 <= 0) {
                    break;
                }
                k();
                if (this.b != null) {
                    int min = Math.min(this.c - this.d, i4);
                    if (bArr != null) {
                        this.b.copyTo(bArr, this.d, i3, min);
                        i3 += min;
                    }
                    this.d += min;
                    i4 -= min;
                } else if (i4 == i2) {
                    return -1;
                }
            }
            return i2 - i4;
        }

        @DexIgnore
        public int available() throws IOException {
            return ix3.this.size() - (this.e + this.d);
        }

        @DexIgnore
        public final void k() {
            int i;
            if (this.b != null && this.d == (i = this.c)) {
                this.e += i;
                this.d = 0;
                if (this.a.hasNext()) {
                    this.b = this.a.next();
                    this.c = this.b.size();
                    return;
                }
                this.b = null;
                this.c = 0;
            }
        }

        @DexIgnore
        public final void l() {
            this.a = new c(ix3.this);
            this.b = this.a.next();
            this.c = this.b.size();
            this.d = 0;
            this.e = 0;
        }

        @DexIgnore
        public void mark(int i) {
            this.f = this.e + this.d;
        }

        @DexIgnore
        public boolean markSupported() {
            return true;
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) {
            if (bArr == null) {
                throw new NullPointerException();
            } else if (i >= 0 && i2 >= 0 && i2 <= bArr.length - i) {
                return a(bArr, i, i2);
            } else {
                throw new IndexOutOfBoundsException();
            }
        }

        @DexIgnore
        public synchronized void reset() {
            l();
            a((byte[]) null, 0, this.f);
        }

        @DexIgnore
        public long skip(long j) {
            if (j >= 0) {
                if (j > 2147483647L) {
                    j = 2147483647L;
                }
                return (long) a((byte[]) null, 0, (int) j);
            }
            throw new IndexOutOfBoundsException();
        }

        @DexIgnore
        public int read() throws IOException {
            k();
            sw3.g gVar = this.b;
            if (gVar == null) {
                return -1;
            }
            int i = this.d;
            this.d = i + 1;
            return gVar.byteAt(i) & 255;
        }
    }

    @DexIgnore
    public final boolean a(sw3 sw3) {
        boolean z;
        c cVar = new c(this);
        sw3.g gVar = (sw3.g) cVar.next();
        c cVar2 = new c(sw3);
        sw3.g gVar2 = (sw3.g) cVar2.next();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int size = gVar.size() - i;
            int size2 = gVar2.size() - i2;
            int min = Math.min(size, size2);
            if (i == 0) {
                z = gVar.equalsRange(gVar2, i2, min);
            } else {
                z = gVar2.equalsRange(gVar, i, min);
            }
            if (!z) {
                return false;
            }
            i3 += min;
            int i4 = this.totalLength;
            if (i3 < i4) {
                if (min == size) {
                    gVar = (sw3.g) cVar.next();
                    i = 0;
                } else {
                    i += min;
                }
                if (min == size2) {
                    gVar2 = (sw3.g) cVar2.next();
                    i2 = 0;
                } else {
                    i2 += min;
                }
            } else if (i3 == i4) {
                return true;
            } else {
                throw new IllegalStateException();
            }
        }
    }
}
