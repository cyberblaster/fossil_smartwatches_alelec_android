package com.fossil;

import android.os.Bundle;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sp3 implements x73 {
    @DexIgnore
    public /* final */ /* synthetic */ ov2 a;

    @DexIgnore
    public sp3(ov2 ov2) {
        this.a = ov2;
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj) {
        this.a.a(str, str2, obj);
    }

    @DexIgnore
    public final void b(String str, String str2, Bundle bundle) {
        this.a.a(str, str2, bundle);
    }

    @DexIgnore
    public final void d(Bundle bundle) {
        this.a.a(bundle);
    }

    @DexIgnore
    public final String zza() {
        return this.a.d();
    }

    @DexIgnore
    public final String zzb() {
        return this.a.e();
    }

    @DexIgnore
    public final String zzc() {
        return this.a.b();
    }

    @DexIgnore
    public final String zzd() {
        return this.a.a();
    }

    @DexIgnore
    public final long zze() {
        return this.a.c();
    }

    @DexIgnore
    public final void a(boolean z) {
        this.a.b(z);
    }

    @DexIgnore
    public final void b(String str) {
        this.a.c(str);
    }

    @DexIgnore
    public final void zza(String str) {
        this.a.b(str);
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, boolean z) {
        return this.a.a(str, str2, z);
    }

    @DexIgnore
    public final void a(c73 c73) {
        this.a.a(c73);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle) {
        this.a.b(str, str2, bundle);
    }

    @DexIgnore
    public final List<Bundle> a(String str, String str2) {
        return this.a.b(str, str2);
    }

    @DexIgnore
    public final int a(String str) {
        return this.a.d(str);
    }
}
