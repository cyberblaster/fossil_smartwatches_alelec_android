package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xt {
    @DexIgnore
    <T> T a(int i, Class<T> cls);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    <T> T b(int i, Class<T> cls);

    @DexIgnore
    <T> void put(T t);
}
