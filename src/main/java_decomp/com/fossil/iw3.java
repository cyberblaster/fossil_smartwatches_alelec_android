package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iw3 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = Integer.MAX_VALUE;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i; // = 64;

    @DexIgnore
    public iw3(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        this.c = i3 + i2;
        this.e = i2;
    }

    @DexIgnore
    public static iw3 a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void b(int i2) {
        this.g = i2;
        k();
    }

    @DexIgnore
    public boolean c() throws IOException {
        return h() != 0;
    }

    @DexIgnore
    public int d() throws IOException {
        return h();
    }

    @DexIgnore
    public void e(int i2) {
        int i3 = this.e;
        int i4 = this.b;
        if (i2 > i3 - i4) {
            int i5 = i3 - i4;
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i2);
            sb.append(" is beyond current ");
            sb.append(i5);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0) {
            this.e = i4 + i2;
        } else {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    @DexIgnore
    public boolean f(int i2) throws IOException {
        int b2 = nw3.b(i2);
        if (b2 == 0) {
            d();
            return true;
        } else if (b2 == 1) {
            g();
            return true;
        } else if (b2 == 2) {
            g(h());
            return true;
        } else if (b2 == 3) {
            l();
            a(nw3.a(nw3.a(i2), 4));
            return true;
        } else if (b2 == 4) {
            return false;
        } else {
            if (b2 == 5) {
                f();
                return true;
            }
            throw kw3.invalidWireType();
        }
    }

    @DexIgnore
    public long g() throws IOException {
        byte e2 = e();
        byte e3 = e();
        return ((((long) e3) & 255) << 8) | (((long) e2) & 255) | ((((long) e()) & 255) << 16) | ((((long) e()) & 255) << 24) | ((((long) e()) & 255) << 32) | ((((long) e()) & 255) << 40) | ((((long) e()) & 255) << 48) | ((((long) e()) & 255) << 56);
    }

    @DexIgnore
    public int h() throws IOException {
        int i2;
        byte e2 = e();
        if (e2 >= 0) {
            return e2;
        }
        byte b2 = e2 & Byte.MAX_VALUE;
        byte e3 = e();
        if (e3 >= 0) {
            i2 = e3 << 7;
        } else {
            b2 |= (e3 & Byte.MAX_VALUE) << 7;
            byte e4 = e();
            if (e4 >= 0) {
                i2 = e4 << 14;
            } else {
                b2 |= (e4 & Byte.MAX_VALUE) << 14;
                byte e5 = e();
                if (e5 >= 0) {
                    i2 = e5 << 21;
                } else {
                    byte b3 = b2 | ((e5 & Byte.MAX_VALUE) << 21);
                    byte e6 = e();
                    byte b4 = b3 | (e6 << 28);
                    if (e6 >= 0) {
                        return b4;
                    }
                    for (int i3 = 0; i3 < 5; i3++) {
                        if (e() >= 0) {
                            return b4;
                        }
                    }
                    throw kw3.malformedVarint();
                }
            }
        }
        return b2 | i2;
    }

    @DexIgnore
    public String i() throws IOException {
        int h2 = h();
        int i2 = this.c;
        int i3 = this.e;
        if (h2 > i2 - i3 || h2 <= 0) {
            return new String(d(h2), "UTF-8");
        }
        String str = new String(this.a, i3, h2, "UTF-8");
        this.e += h2;
        return str;
    }

    @DexIgnore
    public int j() throws IOException {
        if (b()) {
            this.f = 0;
            return 0;
        }
        this.f = h();
        int i2 = this.f;
        if (i2 != 0) {
            return i2;
        }
        throw kw3.invalidTag();
    }

    @DexIgnore
    public final void k() {
        this.c += this.d;
        int i2 = this.c;
        int i3 = this.g;
        if (i2 > i3) {
            this.d = i2 - i3;
            this.c = i2 - this.d;
            return;
        }
        this.d = 0;
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    public void l() throws java.io.IOException {
        /*
            r1 = this;
        L_0x0000:
            int r0 = r1.j()
            if (r0 == 0) goto L_0x000c
            boolean r0 = r1.f(r0)
            if (r0 != 0) goto L_0x0000
        L_0x000c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw3.l():void");
    }

    @DexIgnore
    public static iw3 a(byte[] bArr, int i2, int i3) {
        return new iw3(bArr, i2, i3);
    }

    @DexIgnore
    public int c(int i2) throws kw3 {
        if (i2 >= 0) {
            int i3 = i2 + this.e;
            int i4 = this.g;
            if (i3 <= i4) {
                this.g = i3;
                k();
                return i4;
            }
            throw kw3.truncatedMessage();
        }
        throw kw3.negativeSize();
    }

    @DexIgnore
    public byte[] d(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = i3 + i2;
            int i5 = this.g;
            if (i4 > i5) {
                g(i5 - i3);
                throw kw3.truncatedMessage();
            } else if (i2 <= this.c - i3) {
                byte[] bArr = new byte[i2];
                System.arraycopy(this.a, i3, bArr, 0, i2);
                this.e += i2;
                return bArr;
            } else {
                throw kw3.truncatedMessage();
            }
        } else {
            throw kw3.negativeSize();
        }
    }

    @DexIgnore
    public void a(int i2) throws kw3 {
        if (this.f != i2) {
            throw kw3.invalidEndTag();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.e == this.c;
    }

    @DexIgnore
    public void a(lw3 lw3) throws IOException {
        int h2 = h();
        if (this.h < this.i) {
            int c2 = c(h2);
            this.h++;
            lw3.a(this);
            a(0);
            this.h--;
            b(c2);
            return;
        }
        throw kw3.recursionLimitExceeded();
    }

    @DexIgnore
    public byte e() throws IOException {
        int i2 = this.e;
        if (i2 != this.c) {
            byte[] bArr = this.a;
            this.e = i2 + 1;
            return bArr[i2];
        }
        throw kw3.truncatedMessage();
    }

    @DexIgnore
    public void g(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = i3 + i2;
            int i5 = this.g;
            if (i4 > i5) {
                g(i5 - i3);
                throw kw3.truncatedMessage();
            } else if (i2 <= this.c - i3) {
                this.e = i3 + i2;
            } else {
                throw kw3.truncatedMessage();
            }
        } else {
            throw kw3.negativeSize();
        }
    }

    @DexIgnore
    public int f() throws IOException {
        return (e() & 255) | ((e() & 255) << 8) | ((e() & 255) << 16) | ((e() & 255) << 24);
    }

    @DexIgnore
    public int a() {
        return this.e - this.b;
    }
}
