package com.fossil;

import com.fossil.ms6;
import com.fossil.yq6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.zendesk.sdk.network.Constants;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownServiceException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr6 extends ms6.h implements hq6 {
    @DexIgnore
    public /* final */ iq6 b;
    @DexIgnore
    public /* final */ ar6 c;
    @DexIgnore
    public Socket d;
    @DexIgnore
    public Socket e;
    @DexIgnore
    public rq6 f;
    @DexIgnore
    public wq6 g;
    @DexIgnore
    public ms6 h;
    @DexIgnore
    public lt6 i;
    @DexIgnore
    public kt6 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m; // = 1;
    @DexIgnore
    public /* final */ List<Reference<tr6>> n; // = new ArrayList();
    @DexIgnore
    public long o; // = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;

    @DexIgnore
    public pr6(iq6 iq6, ar6 ar6) {
        this.b = iq6;
        this.c = ar6;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0090 A[Catch:{ IOException -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0136  */
    public void a(int i2, int i3, int i4, int i5, boolean z, dq6 dq6, pq6 pq6) {
        dq6 dq62 = dq6;
        pq6 pq62 = pq6;
        if (this.g == null) {
            List<jq6> b2 = this.c.a().b();
            or6 or6 = new or6(b2);
            if (this.c.a().j() == null) {
                if (b2.contains(jq6.h)) {
                    String g2 = this.c.a().k().g();
                    if (!at6.d().b(g2)) {
                        throw new rr6(new UnknownServiceException("CLEARTEXT communication to " + g2 + " not permitted by network security policy"));
                    }
                } else {
                    throw new rr6(new UnknownServiceException("CLEARTEXT communication not enabled for client"));
                }
            } else if (this.c.a().e().contains(wq6.H2_PRIOR_KNOWLEDGE)) {
                throw new rr6(new UnknownServiceException("H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"));
            }
            rr6 rr6 = null;
            do {
                try {
                    if (!this.c.c()) {
                        a(i2, i3, i4, dq6, pq6);
                        if (this.d != null) {
                            int i6 = i2;
                            int i7 = i3;
                        } else if (this.c.c() && this.d == null) {
                            throw new rr6(new ProtocolException("Too many tunnel connections attempted: 21"));
                        } else if (this.h != null) {
                            synchronized (this.b) {
                                this.m = this.h.m();
                            }
                            return;
                        } else {
                            return;
                        }
                    } else {
                        try {
                            a(i2, i3, dq62, pq62);
                        } catch (IOException e2) {
                            e = e2;
                            int i8 = i5;
                            fr6.a(this.e);
                            fr6.a(this.d);
                            this.e = null;
                            this.d = null;
                            this.i = null;
                            this.j = null;
                            this.f = null;
                            this.g = null;
                            this.h = null;
                            pq6.a(dq6, this.c.d(), this.c.b(), (wq6) null, e);
                            if (rr6 != null) {
                            }
                            throw rr6;
                        }
                    }
                    try {
                        a(or6, i5, dq62, pq62);
                        pq62.a(dq62, this.c.d(), this.c.b(), this.g);
                        if (this.c.c() || this.d == null) {
                        }
                    } catch (IOException e3) {
                        e = e3;
                    }
                } catch (IOException e4) {
                    e = e4;
                    int i9 = i2;
                    int i10 = i3;
                    int i82 = i5;
                    fr6.a(this.e);
                    fr6.a(this.d);
                    this.e = null;
                    this.d = null;
                    this.i = null;
                    this.j = null;
                    this.f = null;
                    this.g = null;
                    this.h = null;
                    pq6.a(dq6, this.c.d(), this.c.b(), (wq6) null, e);
                    if (rr6 != null) {
                        rr6 = new rr6(e);
                    } else {
                        rr6.addConnectException(e);
                    }
                    if (!z || or6.a(e)) {
                        throw rr6;
                    }
                    do {
                        if (!this.c.c()) {
                        }
                        a(or6, i5, dq62, pq62);
                        pq62.a(dq62, this.c.d(), this.c.b(), this.g);
                        if (this.c.c() || this.d == null) {
                        }
                    } while (or6.a(e));
                    throw rr6;
                }
            } while (or6.a(e));
            throw rr6;
        }
        throw new IllegalStateException("already connected");
    }

    @DexIgnore
    public void b() {
        fr6.a(this.d);
    }

    @DexIgnore
    public final yq6 c() throws IOException {
        yq6.a aVar = new yq6.a();
        aVar.a(this.c.a().k());
        aVar.a("CONNECT", (RequestBody) null);
        aVar.b("Host", fr6.a(this.c.a().k(), true));
        aVar.b("Proxy-Connection", "Keep-Alive");
        aVar.b(Constants.USER_AGENT_HEADER, gr6.a());
        yq6 a = aVar.a();
        Response.a aVar2 = new Response.a();
        aVar2.a(a);
        aVar2.a(wq6.HTTP_1_1);
        aVar2.a(407);
        aVar2.a("Preemptive Authenticate");
        aVar2.a(fr6.c);
        aVar2.b(-1);
        aVar2.a(-1);
        aVar2.b("Proxy-Authenticate", "OkHttp-Preemptive");
        yq6 authenticate = this.c.a().g().authenticate(this.c, aVar2.a());
        return authenticate != null ? authenticate : a;
    }

    @DexIgnore
    public rq6 d() {
        return this.f;
    }

    @DexIgnore
    public boolean e() {
        return this.h != null;
    }

    @DexIgnore
    public ar6 f() {
        return this.c;
    }

    @DexIgnore
    public Socket g() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.c.a().k().g());
        sb.append(":");
        sb.append(this.c.a().k().k());
        sb.append(", proxy=");
        sb.append(this.c.b());
        sb.append(" hostAddress=");
        sb.append(this.c.d());
        sb.append(" cipherSuite=");
        rq6 rq6 = this.f;
        sb.append(rq6 != null ? rq6.a() : "none");
        sb.append(" protocol=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, dq6 dq6, pq6 pq6) throws IOException {
        yq6 c2 = c();
        tq6 g2 = c2.g();
        int i5 = 0;
        while (i5 < 21) {
            a(i2, i3, dq6, pq6);
            c2 = a(i3, i4, c2, g2);
            if (c2 != null) {
                fr6.a(this.d);
                this.d = null;
                this.j = null;
                this.i = null;
                pq6.a(dq6, this.c.d(), this.c.b(), (wq6) null);
                i5++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, dq6 dq6, pq6 pq6) throws IOException {
        Socket socket;
        Proxy b2 = this.c.b();
        aq6 a = this.c.a();
        if (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) {
            socket = a.i().createSocket();
        } else {
            socket = new Socket(b2);
        }
        this.d = socket;
        pq6.a(dq6, this.c.d(), b2);
        this.d.setSoTimeout(i3);
        try {
            at6.d().a(this.d, this.c.d(), i2);
            try {
                this.i = st6.a(st6.b(this.d));
                this.j = st6.a(st6.a(this.d));
            } catch (NullPointerException e2) {
                if ("throw with null exception".equals(e2.getMessage())) {
                    throw new IOException(e2);
                }
            }
        } catch (ConnectException e3) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.c.d());
            connectException.initCause(e3);
            throw connectException;
        }
    }

    @DexIgnore
    public final void a(or6 or6, int i2, dq6 dq6, pq6 pq6) throws IOException {
        if (this.c.a().j() != null) {
            pq6.g(dq6);
            a(or6);
            pq6.a(dq6, this.f);
            if (this.g == wq6.HTTP_2) {
                a(i2);
            }
        } else if (this.c.a().e().contains(wq6.H2_PRIOR_KNOWLEDGE)) {
            this.e = this.d;
            this.g = wq6.H2_PRIOR_KNOWLEDGE;
            a(i2);
        } else {
            this.e = this.d;
            this.g = wq6.HTTP_1_1;
        }
    }

    @DexIgnore
    public final void a(int i2) throws IOException {
        this.e.setSoTimeout(0);
        ms6.g gVar = new ms6.g(true);
        gVar.a(this.e, this.c.a().k().g(), this.i, this.j);
        gVar.a((ms6.h) this);
        gVar.a(i2);
        this.h = gVar.a();
        this.h.n();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0111 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0117 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011a  */
    public final void a(or6 or6) throws IOException {
        SSLSocket sSLSocket;
        wq6 wq6;
        aq6 a = this.c.a();
        String str = null;
        try {
            SSLSocket sSLSocket2 = (SSLSocket) a.j().createSocket(this.d, a.k().g(), a.k().k(), true);
            try {
                jq6 a2 = or6.a(sSLSocket2);
                if (a2.c()) {
                    at6.d().a(sSLSocket2, a.k().g(), a.e());
                }
                sSLSocket2.startHandshake();
                SSLSession session = sSLSocket2.getSession();
                rq6 a3 = rq6.a(session);
                if (a.d().verify(a.k().g(), session)) {
                    a.a().a(a.k().g(), a3.c());
                    if (a2.c()) {
                        str = at6.d().b(sSLSocket2);
                    }
                    this.e = sSLSocket2;
                    this.i = st6.a(st6.b(this.e));
                    this.j = st6.a(st6.a(this.e));
                    this.f = a3;
                    if (str != null) {
                        wq6 = wq6.get(str);
                    } else {
                        wq6 = wq6.HTTP_1_1;
                    }
                    this.g = wq6;
                    if (sSLSocket2 != 0) {
                        at6.d().a(sSLSocket2);
                        return;
                    }
                    return;
                }
                X509Certificate x509Certificate = (X509Certificate) a3.c().get(0);
                throw new SSLPeerUnverifiedException("Hostname " + a.k().g() + " not verified:\n    certificate: " + fq6.a((Certificate) x509Certificate) + "\n    DN: " + x509Certificate.getSubjectDN().getName() + "\n    subjectAltNames: " + ft6.a(x509Certificate));
            } catch (AssertionError e2) {
                e = e2;
                str = sSLSocket2;
                try {
                    if (!fr6.a(e)) {
                    }
                } catch (Throwable th) {
                    th = th;
                    sSLSocket = str;
                    if (sSLSocket != 0) {
                        at6.d().a(sSLSocket);
                    }
                    fr6.a((Socket) sSLSocket);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                sSLSocket = sSLSocket2;
                if (sSLSocket != 0) {
                }
                fr6.a((Socket) sSLSocket);
                throw th;
            }
        } catch (AssertionError e3) {
            e = e3;
            if (!fr6.a(e)) {
                throw new IOException(e);
            }
            throw e;
        }
    }

    @DexIgnore
    public final yq6 a(int i2, int i3, yq6 yq6, tq6 tq6) throws IOException {
        String str = "CONNECT " + fr6.a(tq6, true) + " HTTP/1.1";
        while (true) {
            fs6 fs6 = new fs6((OkHttpClient) null, (tr6) null, this.i, this.j);
            this.i.b().a((long) i2, TimeUnit.MILLISECONDS);
            this.j.b().a((long) i3, TimeUnit.MILLISECONDS);
            fs6.a(yq6.c(), str);
            fs6.a();
            Response.a a = fs6.a(false);
            a.a(yq6);
            Response a2 = a.a();
            long a3 = yr6.a(a2);
            if (a3 == -1) {
                a3 = 0;
            }
            zt6 b2 = fs6.b(a3);
            fr6.b(b2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b2.close();
            int n2 = a2.n();
            if (n2 != 200) {
                if (n2 == 407) {
                    yq6 authenticate = this.c.a().g().authenticate(this.c, a2);
                    if (authenticate == null) {
                        throw new IOException("Failed to authenticate with proxy");
                    } else if ("close".equalsIgnoreCase(a2.e("Connection"))) {
                        return authenticate;
                    } else {
                        yq6 = authenticate;
                    }
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + a2.n());
                }
            } else if (this.i.a().f() && this.j.a().f()) {
                return null;
            } else {
                throw new IOException("TLS tunnel buffered too many bytes!");
            }
        }
    }

    @DexIgnore
    public boolean a(aq6 aq6, ar6 ar6) {
        if (this.n.size() >= this.m || this.k || !dr6.a.a(this.c.a(), aq6)) {
            return false;
        }
        if (aq6.k().g().equals(f().a().k().g())) {
            return true;
        }
        if (this.h == null || ar6 == null || ar6.b().type() != Proxy.Type.DIRECT || this.c.b().type() != Proxy.Type.DIRECT || !this.c.d().equals(ar6.d()) || ar6.a().d() != ft6.a || !a(aq6.k())) {
            return false;
        }
        try {
            aq6.a().a(aq6.k().g(), d().c());
            return true;
        } catch (SSLPeerUnverifiedException unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean a(tq6 tq6) {
        if (tq6.k() != this.c.a().k().k()) {
            return false;
        }
        if (tq6.g().equals(this.c.a().k().g())) {
            return true;
        }
        if (this.f == null || !ft6.a.a(tq6.g(), (X509Certificate) this.f.c().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public wr6 a(OkHttpClient okHttpClient, Interceptor.Chain chain, tr6 tr6) throws SocketException {
        ms6 ms6 = this.h;
        if (ms6 != null) {
            return new ls6(okHttpClient, chain, tr6, ms6);
        }
        this.e.setSoTimeout(chain.a());
        this.i.b().a((long) chain.a(), TimeUnit.MILLISECONDS);
        this.j.b().a((long) chain.b(), TimeUnit.MILLISECONDS);
        return new fs6(okHttpClient, tr6, this.i, this.j);
    }

    @DexIgnore
    public boolean a(boolean z) {
        int soTimeout;
        if (this.e.isClosed() || this.e.isInputShutdown() || this.e.isOutputShutdown()) {
            return false;
        }
        ms6 ms6 = this.h;
        if (ms6 != null) {
            return !ms6.l();
        }
        if (z) {
            try {
                soTimeout = this.e.getSoTimeout();
                this.e.setSoTimeout(1);
                if (this.i.f()) {
                    this.e.setSoTimeout(soTimeout);
                    return false;
                }
                this.e.setSoTimeout(soTimeout);
                return true;
            } catch (SocketTimeoutException unused) {
            } catch (IOException unused2) {
                return false;
            } catch (Throwable th) {
                this.e.setSoTimeout(soTimeout);
                throw th;
            }
        }
        return true;
    }

    @DexIgnore
    public void a(os6 os6) throws IOException {
        os6.a(hs6.REFUSED_STREAM);
    }

    @DexIgnore
    public void a(ms6 ms6) {
        synchronized (this.b) {
            this.m = ms6.m();
        }
    }

    @DexIgnore
    public wq6 a() {
        return this.g;
    }
}
