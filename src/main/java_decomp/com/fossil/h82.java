package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fossil.u12;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h82 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<h82> CREATOR; // = new l82();
    @DexIgnore
    public static /* final */ TimeUnit e; // = TimeUnit.MILLISECONDS;
    @DexIgnore
    public /* final */ k72 a;
    @DexIgnore
    public /* final */ List<DataSet> b;
    @DexIgnore
    public /* final */ List<DataPoint> c;
    @DexIgnore
    public /* final */ nd2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public k72 a;
        @DexIgnore
        public List<DataSet> b; // = new ArrayList();
        @DexIgnore
        public List<DataPoint> c; // = new ArrayList();
        @DexIgnore
        public List<f72> d; // = new ArrayList();

        @DexIgnore
        public a a(k72 k72) {
            this.a = k72;
            return this;
        }

        @DexIgnore
        public a a(DataSet dataSet) {
            w12.a(dataSet != null, (Object) "Must specify a valid data set.");
            f72 C = dataSet.C();
            w12.b(!this.d.contains(C), "Data set for this data source %s is already added.", C);
            w12.a(!dataSet.B().isEmpty(), (Object) "No data points specified in the input data set.");
            this.d.add(C);
            this.b.add(dataSet);
            return this;
        }

        @DexIgnore
        public h82 a() {
            boolean z = true;
            w12.b(this.a != null, "Must specify a valid session.");
            if (this.a.a(TimeUnit.MILLISECONDS) == 0) {
                z = false;
            }
            w12.b(z, "Must specify a valid end time, cannot insert a continuing session.");
            for (DataSet B : this.b) {
                for (DataPoint a2 : B.B()) {
                    a(a2);
                }
            }
            for (DataPoint a3 : this.c) {
                a(a3);
            }
            return new h82(this);
        }

        @DexIgnore
        public final void a(DataPoint dataPoint) {
            DataPoint dataPoint2 = dataPoint;
            long b2 = this.a.b(TimeUnit.NANOSECONDS);
            long a2 = this.a.a(TimeUnit.NANOSECONDS);
            long c2 = dataPoint2.c(TimeUnit.NANOSECONDS);
            if (c2 != 0) {
                if (c2 < b2 || c2 > a2) {
                    c2 = ne2.a(c2, TimeUnit.NANOSECONDS, h82.e);
                }
                w12.b(c2 >= b2 && c2 <= a2, "Data point %s has time stamp outside session interval [%d, %d]", dataPoint2, Long.valueOf(b2), Long.valueOf(a2));
                if (dataPoint2.c(TimeUnit.NANOSECONDS) != c2) {
                    Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[]{Long.valueOf(dataPoint2.c(TimeUnit.NANOSECONDS)), Long.valueOf(c2), h82.e}));
                    dataPoint2.a(c2, TimeUnit.NANOSECONDS);
                }
            }
            long b3 = this.a.b(TimeUnit.NANOSECONDS);
            long a3 = this.a.a(TimeUnit.NANOSECONDS);
            long b4 = dataPoint2.b(TimeUnit.NANOSECONDS);
            long a4 = dataPoint2.a(TimeUnit.NANOSECONDS);
            if (b4 != 0 && a4 != 0) {
                if (a4 > a3) {
                    a4 = ne2.a(a4, TimeUnit.NANOSECONDS, h82.e);
                }
                w12.b(b4 >= b3 && a4 <= a3, "Data point %s has start and end times outside session interval [%d, %d]", dataPoint2, Long.valueOf(b3), Long.valueOf(a3));
                if (a4 != dataPoint2.a(TimeUnit.NANOSECONDS)) {
                    Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[]{Long.valueOf(dataPoint2.a(TimeUnit.NANOSECONDS)), Long.valueOf(a4), h82.e}));
                    dataPoint.a(b4, a4, TimeUnit.NANOSECONDS);
                }
            }
        }
    }

    @DexIgnore
    public h82(k72 k72, List<DataSet> list, List<DataPoint> list2, IBinder iBinder) {
        this.a = k72;
        this.b = Collections.unmodifiableList(list);
        this.c = Collections.unmodifiableList(list2);
        this.d = pd2.a(iBinder);
    }

    @DexIgnore
    public List<DataSet> B() {
        return this.b;
    }

    @DexIgnore
    public k72 C() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof h82) {
                h82 h82 = (h82) obj;
                if (u12.a(this.a, h82.a) && u12.a(this.b, h82.b) && u12.a(this.c, h82.c)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(this.a, this.b, this.c);
    }

    @DexIgnore
    public List<DataPoint> p() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("session", this.a);
        a2.a("dataSets", this.b);
        a2.a("aggregateDataPoints", this.c);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) C(), i, false);
        g22.c(parcel, 2, B(), false);
        g22.c(parcel, 3, p(), false);
        nd2 nd2 = this.d;
        g22.a(parcel, 4, nd2 == null ? null : nd2.asBinder(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public h82(a aVar) {
        this(aVar.a, (List<DataSet>) aVar.b, (List<DataPoint>) aVar.c, (nd2) null);
    }

    @DexIgnore
    public h82(h82 h82, nd2 nd2) {
        this(h82.a, h82.b, h82.c, nd2);
    }

    @DexIgnore
    public h82(k72 k72, List<DataSet> list, List<DataPoint> list2, nd2 nd2) {
        this.a = k72;
        this.b = Collections.unmodifiableList(list);
        this.c = Collections.unmodifiableList(list2);
        this.d = nd2;
    }
}
