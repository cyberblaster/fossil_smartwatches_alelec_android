package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw3 {
    @DexIgnore
    public static /* final */ Class<?> a; // = b();

    @DexIgnore
    public static ww3 a() {
        if (a != null) {
            try {
                return a("getEmptyRegistry");
            } catch (Exception unused) {
            }
        }
        return ww3.a;
    }

    @DexIgnore
    public static Class<?> b() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static final ww3 a(String str) throws Exception {
        return (ww3) a.getMethod(str, new Class[0]).invoke((Object) null, new Object[0]);
    }
}
