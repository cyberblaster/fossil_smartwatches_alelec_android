package com.fossil;

import android.os.LocaleList;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e8 implements d8 {
    @DexIgnore
    public /* final */ LocaleList a;

    @DexIgnore
    public e8(LocaleList localeList) {
        this.a = localeList;
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this.a.equals(((d8) obj).a());
    }

    @DexIgnore
    public Locale get(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }
}
