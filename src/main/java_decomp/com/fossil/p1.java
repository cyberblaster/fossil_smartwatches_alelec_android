package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.ListMenuItemView;
import com.fossil.y1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p1 extends BaseAdapter {
    @DexIgnore
    public q1 a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ LayoutInflater e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public p1(q1 q1Var, LayoutInflater layoutInflater, boolean z, int i) {
        this.d = z;
        this.e = layoutInflater;
        this.a = q1Var;
        this.f = i;
        a();
    }

    @DexIgnore
    public void a(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public q1 b() {
        return this.a;
    }

    @DexIgnore
    public int getCount() {
        ArrayList<t1> j = this.d ? this.a.j() : this.a.n();
        if (this.b < 0) {
            return j.size();
        }
        return j.size() - 1;
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.e.inflate(this.f, viewGroup, false);
        }
        int groupId = getItem(i).getGroupId();
        int i2 = i - 1;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.a.o() && groupId != (i2 >= 0 ? getItem(i2).getGroupId() : groupId));
        y1.a aVar = (y1.a) view;
        if (this.c) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.a(getItem(i), 0);
        return view;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }

    @DexIgnore
    public void a() {
        t1 f2 = this.a.f();
        if (f2 != null) {
            ArrayList<t1> j = this.a.j();
            int size = j.size();
            for (int i = 0; i < size; i++) {
                if (j.get(i) == f2) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    @DexIgnore
    public t1 getItem(int i) {
        ArrayList<t1> j = this.d ? this.a.j() : this.a.n();
        int i2 = this.b;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return j.get(i);
    }
}
