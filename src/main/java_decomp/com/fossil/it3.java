package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class it3 implements Runnable {
    @DexIgnore
    public /* final */ gt3 a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ rc3 c;

    @DexIgnore
    public it3(gt3 gt3, Intent intent, rc3 rc3) {
        this.a = gt3;
        this.b = intent;
        this.c = rc3;
    }

    @DexIgnore
    public final void run() {
        gt3 gt3 = this.a;
        Intent intent = this.b;
        rc3 rc3 = this.c;
        try {
            gt3.c(intent);
        } finally {
            rc3.a(null);
        }
    }
}
