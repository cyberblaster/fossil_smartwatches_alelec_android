package com.fossil;

import com.fossil.bt;
import com.fossil.fs;
import com.fossil.jv;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ys implements bt, fs.a<Object> {
    @DexIgnore
    public /* final */ List<vr> a;
    @DexIgnore
    public /* final */ ct<?> b;
    @DexIgnore
    public /* final */ bt.a c;
    @DexIgnore
    public int d;
    @DexIgnore
    public vr e;
    @DexIgnore
    public List<jv<File, ?>> f;
    @DexIgnore
    public int g;
    @DexIgnore
    public volatile jv.a<?> h;
    @DexIgnore
    public File i;

    @DexIgnore
    public ys(ct<?> ctVar, bt.a aVar) {
        this(ctVar.c(), ctVar, aVar);
    }

    @DexIgnore
    public boolean a() {
        while (true) {
            boolean z = false;
            if (this.f == null || !b()) {
                this.d++;
                if (this.d >= this.a.size()) {
                    return false;
                }
                vr vrVar = this.a.get(this.d);
                this.i = this.b.d().a(new zs(vrVar, this.b.l()));
                File file = this.i;
                if (file != null) {
                    this.e = vrVar;
                    this.f = this.b.a(file);
                    this.g = 0;
                }
            } else {
                this.h = null;
                while (!z && b()) {
                    List<jv<File, ?>> list = this.f;
                    int i2 = this.g;
                    this.g = i2 + 1;
                    this.h = list.get(i2).a(this.i, this.b.n(), this.b.f(), this.b.i());
                    if (this.h != null && this.b.c(this.h.c.getDataClass())) {
                        this.h.c.a(this.b.j(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.g < this.f.size();
    }

    @DexIgnore
    public void cancel() {
        jv.a<?> aVar = this.h;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public ys(List<vr> list, ct<?> ctVar, bt.a aVar) {
        this.d = -1;
        this.a = list;
        this.b = ctVar;
        this.c = aVar;
    }

    @DexIgnore
    public void a(Object obj) {
        this.c.a(this.e, obj, this.h.c, pr.DATA_DISK_CACHE, this.e);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.c.a(this.e, exc, this.h.c, pr.DATA_DISK_CACHE);
    }
}
