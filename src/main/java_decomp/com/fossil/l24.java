package com.fossil;

import androidx.lifecycle.MutableLiveData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l24 extends td {
    @DexIgnore
    public static /* final */ /* synthetic */ li6[] e;
    @DexIgnore
    public /* final */ ic6 a; // = jc6.a(c.INSTANCE);
    @DexIgnore
    public /* final */ ic6 b; // = jc6.a(d.INSTANCE);
    @DexIgnore
    public /* final */ ic6 c; // = jc6.a(e.INSTANCE);
    @DexIgnore
    public /* final */ ic6 d; // = jc6.a(f.INSTANCE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a() {
            this(false, false, 3, (qg6) null);
        }

        @DexIgnore
        public a(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.b == aVar.b;
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.a;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i = (z ? 1 : 0) * true;
            boolean z3 = this.b;
            if (!z3) {
                z2 = z3;
            }
            return i + (z2 ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "LoadingState(mStartLoading=" + this.a + ", mStopLoading=" + this.b + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(boolean z, boolean z2, int i, qg6 qg6) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2);
        }

        @DexIgnore
        public final void a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ List<uh4> a;

        @DexIgnore
        public b() {
            this((List) null, 1, (qg6) null);
        }

        @DexIgnore
        public b(List<uh4> list) {
            wg6.b(list, "permissionCodes");
            this.a = list;
        }

        @DexIgnore
        public final List<uh4> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && wg6.a((Object) this.a, (Object) ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<uh4> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PermissionState(permissionCodes=" + this.a + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(List list, int i, qg6 qg6) {
            this((i & 1) != 0 ? new ArrayList() : list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements gg6<a> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(0);
        }

        @DexIgnore
        public final a invoke() {
            return new a(false, false, 3, (qg6) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends xg6 implements gg6<MutableLiveData<a>> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(0);
        }

        @DexIgnore
        public final MutableLiveData<a> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends xg6 implements gg6<b> {
        @DexIgnore
        public static /* final */ e INSTANCE; // = new e();

        @DexIgnore
        public e() {
            super(0);
        }

        @DexIgnore
        public final b invoke() {
            return new b((List) null, 1, (qg6) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends xg6 implements gg6<MutableLiveData<b>> {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(0);
        }

        @DexIgnore
        public final MutableLiveData<b> invoke() {
            return new MutableLiveData<>();
        }
    }

    /*
    static {
        Class<l24> cls = l24.class;
        dh6 dh6 = new dh6(kh6.a((Class) cls), "mLoadingState", "getMLoadingState()Lcom/portfolio/platform/BaseViewModel$LoadingState;");
        kh6.a((ch6) dh6);
        dh6 dh62 = new dh6(kh6.a((Class) cls), "mLoadingStateLiveData", "getMLoadingStateLiveData()Landroidx/lifecycle/MutableLiveData;");
        kh6.a((ch6) dh62);
        dh6 dh63 = new dh6(kh6.a((Class) cls), "mPermissionState", "getMPermissionState()Lcom/portfolio/platform/BaseViewModel$PermissionState;");
        kh6.a((ch6) dh63);
        dh6 dh64 = new dh6(kh6.a((Class) cls), "mPermissionStateLiveData", "getMPermissionStateLiveData()Landroidx/lifecycle/MutableLiveData;");
        kh6.a((ch6) dh64);
        e = new li6[]{dh6, dh62, dh63, dh64};
    }
    */

    @DexIgnore
    public static /* synthetic */ void a(l24 l24, boolean z, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            if ((i & 2) != 0) {
                z2 = false;
            }
            l24.a(z, z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: emitLoadingState");
    }

    @DexIgnore
    public final a a() {
        ic6 ic6 = this.a;
        li6 li6 = e[0];
        return (a) ic6.getValue();
    }

    @DexIgnore
    public final MutableLiveData<a> b() {
        ic6 ic6 = this.b;
        li6 li6 = e[1];
        return (MutableLiveData) ic6.getValue();
    }

    @DexIgnore
    public final b c() {
        ic6 ic6 = this.c;
        li6 li6 = e[2];
        return (b) ic6.getValue();
    }

    @DexIgnore
    public final MutableLiveData<b> d() {
        ic6 ic6 = this.d;
        li6 li6 = e[3];
        return (MutableLiveData) ic6.getValue();
    }

    @DexIgnore
    public final void a(boolean z, boolean z2) {
        a().a(z);
        a().b(z2);
        b().a(a());
    }

    @DexIgnore
    public final void a(uh4... uh4Arr) {
        wg6.b(uh4Arr, "permissionCodes");
        c().a().clear();
        c().a().addAll(nd6.g(uh4Arr));
        d().a(c());
    }
}
