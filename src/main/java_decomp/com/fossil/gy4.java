package com.fossil;

import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gy4 extends y24<b, c, y24.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.b {
        @DexIgnore
        public /* final */ List<vx4> a;

        @DexIgnore
        public b(List<vx4> list) {
            wg6.b(list, "appWrapperList");
            this.a = list;
        }

        @DexIgnore
        public final List<vx4> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = gy4.class.getSimpleName();
        wg6.a((Object) simpleName, "SaveAppsNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public gy4(NotificationsRepository notificationsRepository) {
        wg6.b(notificationsRepository, "notificationsRepository");
        jk3.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        wg6.a((Object) notificationsRepository, "checkNotNull(notificatio\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<vx4> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(next.getCurrentHandGroup());
                InstalledApp installedApp = next.getInstalledApp();
                String str = null;
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved: App name = ");
                InstalledApp installedApp2 = next.getInstalledApp();
                if (installedApp2 != null) {
                    str = installedApp2.getIdentifier();
                }
                sb.append(str);
                sb.append(", hour = ");
                sb.append(next.getCurrentHandGroup());
                local.d(str2, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        wg6.b(bVar, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (AppWrapper next : bVar.a()) {
            InstalledApp installedApp = next.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                wg6.a();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList.add(next);
            } else {
                arrayList2.add(next);
            }
        }
        a((List<vx4>) arrayList2);
        b(arrayList);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAppsNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<vx4> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = next.getInstalledApp();
                Integer num = null;
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(next.getCurrentHandGroup());
                    InstalledApp installedApp2 = next.getInstalledApp();
                    String identifier = installedApp2 != null ? installedApp2.getIdentifier() : null;
                    if (identifier != null) {
                        appFilter.setType(identifier);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Removed: App name = ");
                        InstalledApp installedApp3 = next.getInstalledApp();
                        sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                        sb.append(", row id = ");
                        InstalledApp installedApp4 = next.getInstalledApp();
                        if (installedApp4 != null) {
                            num = Integer.valueOf(installedApp4.getDbRowId());
                        }
                        sb.append(num);
                        sb.append(", hour = ");
                        sb.append(next.getCurrentHandGroup());
                        local.d(str, sb.toString());
                        arrayList.add(appFilter);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }
}
