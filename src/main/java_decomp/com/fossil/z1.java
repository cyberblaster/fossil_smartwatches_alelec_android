package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z1 extends m1 implements Menu {
    @DexIgnore
    public /* final */ u7 d;

    @DexIgnore
    public z1(Context context, u7 u7Var) {
        super(context);
        if (u7Var != null) {
            this.d = u7Var;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }

    @DexIgnore
    public MenuItem add(CharSequence charSequence) {
        return a(this.d.add(charSequence));
    }

    @DexIgnore
    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        MenuItem[] menuItemArr2 = menuItemArr;
        MenuItem[] menuItemArr3 = menuItemArr2 != null ? new MenuItem[menuItemArr2.length] : null;
        int addIntentOptions = this.d.addIntentOptions(i, i2, i3, componentName, intentArr, intent, i4, menuItemArr3);
        if (menuItemArr3 != null) {
            int length = menuItemArr3.length;
            for (int i5 = 0; i5 < length; i5++) {
                menuItemArr2[i5] = a(menuItemArr3[i5]);
            }
        }
        return addIntentOptions;
    }

    @DexIgnore
    public SubMenu addSubMenu(CharSequence charSequence) {
        return a(this.d.addSubMenu(charSequence));
    }

    @DexIgnore
    public void clear() {
        b();
        this.d.clear();
    }

    @DexIgnore
    public void close() {
        this.d.close();
    }

    @DexIgnore
    public MenuItem findItem(int i) {
        return a(this.d.findItem(i));
    }

    @DexIgnore
    public MenuItem getItem(int i) {
        return a(this.d.getItem(i));
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        return this.d.hasVisibleItems();
    }

    @DexIgnore
    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return this.d.isShortcutKey(i, keyEvent);
    }

    @DexIgnore
    public boolean performIdentifierAction(int i, int i2) {
        return this.d.performIdentifierAction(i, i2);
    }

    @DexIgnore
    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        return this.d.performShortcut(i, keyEvent, i2);
    }

    @DexIgnore
    public void removeGroup(int i) {
        a(i);
        this.d.removeGroup(i);
    }

    @DexIgnore
    public void removeItem(int i) {
        b(i);
        this.d.removeItem(i);
    }

    @DexIgnore
    public void setGroupCheckable(int i, boolean z, boolean z2) {
        this.d.setGroupCheckable(i, z, z2);
    }

    @DexIgnore
    public void setGroupEnabled(int i, boolean z) {
        this.d.setGroupEnabled(i, z);
    }

    @DexIgnore
    public void setGroupVisible(int i, boolean z) {
        this.d.setGroupVisible(i, z);
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        this.d.setQwertyMode(z);
    }

    @DexIgnore
    public int size() {
        return this.d.size();
    }

    @DexIgnore
    public MenuItem add(int i) {
        return a(this.d.add(i));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i) {
        return a(this.d.addSubMenu(i));
    }

    @DexIgnore
    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return a(this.d.add(i, i2, i3, charSequence));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return a(this.d.addSubMenu(i, i2, i3, charSequence));
    }

    @DexIgnore
    public MenuItem add(int i, int i2, int i3, int i4) {
        return a(this.d.add(i, i2, i3, i4));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return a(this.d.addSubMenu(i, i2, i3, i4));
    }
}
