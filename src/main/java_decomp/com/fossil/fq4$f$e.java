package com.fossil;

import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$playbackState$1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class fq4$f$e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $currentState;
    @DexIgnore
    public /* final */ /* synthetic */ int $oldState;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$f$e(MusicControlComponent.f fVar, int i, int i2, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$oldState = i;
        this.$currentState = i2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        fq4$f$e fq4_f_e = new fq4$f$e(this.this$0, this.$oldState, this.$currentState, xe6);
        fq4_f_e.p$ = (il6) obj;
        return fq4_f_e;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((fq4$f$e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            MusicControlComponent.f fVar = this.this$0;
            fVar.a(this.$oldState, this.$currentState, (MusicControlComponent.b) fVar);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
