package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kb5 implements MembersInjector<ActiveTimeOverviewFragment> {
    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
        activeTimeOverviewFragment.g = activeTimeOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter) {
        activeTimeOverviewFragment.h = activeTimeOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
        activeTimeOverviewFragment.i = activeTimeOverviewMonthPresenter;
    }
}
