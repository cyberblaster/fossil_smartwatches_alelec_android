package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u80 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new u80(parcel.readLong(), parcel.readInt());
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new u80[i];
        }
    }

    @DexIgnore
    public u80(long j, int i) {
        this.a = j;
        this.b = i;
        int i2 = this.b;
        if (!(i2 >= 0 && 100 >= i2)) {
            throw new IllegalArgumentException(ze0.a(ze0.b("chanceOfRain("), this.b, ") is out of range ", "[0, 100]."));
        }
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.EXPIRED_TIMESTAMP_IN_SECOND, (Object) Long.valueOf(this.a)), bm0.CHANCE_OF_RAIN, (Object) Integer.valueOf(this.b));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.a);
            jSONObject.put("rain", this.b);
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(u80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            u80 u80 = (u80) obj;
            return this.a == u80.a && this.b == u80.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.ChanceOfRainInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.b;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return (((int) this.a) * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
