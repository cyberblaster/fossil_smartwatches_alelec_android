package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u14 implements Factory<ce> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public u14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static u14 a(b14 b14) {
        return new u14(b14);
    }

    @DexIgnore
    public static ce b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static ce c(b14 b14) {
        ce j = b14.j();
        z76.a(j, "Cannot return null from a non-@Nullable @Provides method");
        return j;
    }

    @DexIgnore
    public ce get() {
        return b(this.a);
    }
}
