package com.fossil;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class du6 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public List args; // = new LinkedList();
    @DexIgnore
    public List options; // = new ArrayList();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    public final ju6 a(String str) {
        String b = su6.b(str);
        for (ju6 ju6 : this.options) {
            if (b.equals(ju6.getOpt()) || b.equals(ju6.getLongOpt())) {
                return ju6;
            }
            while (r0.hasNext()) {
            }
        }
        return null;
    }

    @DexIgnore
    public void addArg(String str) {
        this.args.add(str);
    }

    @DexIgnore
    public void addOption(ju6 ju6) {
        this.options.add(ju6);
    }

    @DexIgnore
    public List getArgList() {
        return this.args;
    }

    @DexIgnore
    public String[] getArgs() {
        String[] strArr = new String[this.args.size()];
        this.args.toArray(strArr);
        return strArr;
    }

    @DexIgnore
    public Object getOptionObject(String str) {
        try {
            return getParsedOptionValue(str);
        } catch (nu6 e) {
            PrintStream printStream = System.err;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Exception found converting ");
            stringBuffer.append(str);
            stringBuffer.append(" to desired type: ");
            stringBuffer.append(e.getMessage());
            printStream.println(stringBuffer.toString());
            return null;
        }
    }

    @DexIgnore
    public Properties getOptionProperties(String str) {
        Properties properties = new Properties();
        for (ju6 ju6 : this.options) {
            if (str.equals(ju6.getOpt()) || str.equals(ju6.getLongOpt())) {
                List valuesList = ju6.getValuesList();
                if (valuesList.size() >= 2) {
                    properties.put(valuesList.get(0), valuesList.get(1));
                } else if (valuesList.size() == 1) {
                    properties.put(valuesList.get(0), "true");
                }
            }
        }
        return properties;
    }

    @DexIgnore
    public String getOptionValue(String str) {
        String[] optionValues = getOptionValues(str);
        if (optionValues == null) {
            return null;
        }
        return optionValues[0];
    }

    @DexIgnore
    public String[] getOptionValues(String str) {
        ArrayList arrayList = new ArrayList();
        for (ju6 ju6 : this.options) {
            if (str.equals(ju6.getOpt()) || str.equals(ju6.getLongOpt())) {
                arrayList.addAll(ju6.getValuesList());
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    @DexIgnore
    public ju6[] getOptions() {
        List list = this.options;
        return (ju6[]) list.toArray(new ju6[list.size()]);
    }

    @DexIgnore
    public Object getParsedOptionValue(String str) throws nu6 {
        String optionValue = getOptionValue(str);
        ju6 a = a(str);
        if (a == null) {
            return null;
        }
        Object type = a.getType();
        if (optionValue == null) {
            return null;
        }
        return qu6.a(optionValue, type);
    }

    @DexIgnore
    public boolean hasOption(String str) {
        return this.options.contains(a(str));
    }

    @DexIgnore
    public Iterator iterator() {
        return this.options.iterator();
    }

    @DexIgnore
    public boolean hasOption(char c) {
        return hasOption(String.valueOf(c));
    }

    @DexIgnore
    public Object getOptionObject(char c) {
        return getOptionObject(String.valueOf(c));
    }

    @DexIgnore
    public String getOptionValue(char c) {
        return getOptionValue(String.valueOf(c));
    }

    @DexIgnore
    public String getOptionValue(String str, String str2) {
        String optionValue = getOptionValue(str);
        return optionValue != null ? optionValue : str2;
    }

    @DexIgnore
    public String getOptionValue(char c, String str) {
        return getOptionValue(String.valueOf(c), str);
    }

    @DexIgnore
    public String[] getOptionValues(char c) {
        return getOptionValues(String.valueOf(c));
    }
}
