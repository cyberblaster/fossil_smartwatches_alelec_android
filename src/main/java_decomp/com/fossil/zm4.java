package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm4 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static zm4 o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public ContactProvider a;
    @DexIgnore
    public LocationProvider b;
    @DexIgnore
    public AppFilterProvider c;
    @DexIgnore
    public DeviceProvider d;
    @DexIgnore
    public bo4 e;
    @DexIgnore
    public MFSleepSessionProvider f;
    @DexIgnore
    public xn4 g;
    @DexIgnore
    public fo4 h;
    @DexIgnore
    public zn4 i;
    @DexIgnore
    public jo4 j;
    @DexIgnore
    public SecondTimezoneProvider k;
    @DexIgnore
    public do4 l;
    @DexIgnore
    public ho4 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(zm4 zm4) {
            zm4.o = zm4;
        }

        @DexIgnore
        public final zm4 b() {
            return zm4.o;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final zm4 a() {
            if (b() == null) {
                a(new zm4((qg6) null));
            }
            zm4 b = b();
            if (b != null) {
                return b;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = zm4.class.getSimpleName();
        wg6.a((Object) simpleName, "ProviderManager::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public zm4() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized ContactProvider b() {
        ContactProvider contactProvider;
        String c2 = c();
        if (this.a == null) {
            this.a = new vn4(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "entourage.db");
        } else {
            String str = c2 + "_" + "entourage.db";
            ContactProvider contactProvider2 = this.a;
            if (contactProvider2 != null) {
                String dbPath = contactProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.a = new vn4(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        contactProvider = this.a;
        if (contactProvider == null) {
            wg6.a();
            throw null;
        }
        return contactProvider;
    }

    @DexIgnore
    public final String c() {
        jo4 jo4 = this.j;
        if (jo4 == null) {
            return "Anonymous";
        }
        if (jo4 != null) {
            MFUser b2 = jo4.b();
            if (b2 == null || TextUtils.isEmpty(b2.getUserId())) {
                return "Anonymous";
            }
            String userId = b2.getUserId();
            wg6.a((Object) userId, "user.userId");
            return userId;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v9, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v13, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v16, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized DeviceProvider d() {
        DeviceProvider deviceProvider;
        DeviceProviderImp deviceProviderImp;
        String c2 = c();
        FLogger.INSTANCE.getLocal().d(n, "Inside .getDeviceProvider with userId=" + c2);
        if (this.d == null) {
            this.d = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + DeviceProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + DeviceProviderImp.DB_NAME;
            try {
                DeviceProvider deviceProvider2 = this.d;
                if (deviceProvider2 != null) {
                    String dbPath = deviceProvider2.getDbPath();
                    if (TextUtils.isEmpty(dbPath) || (!wg6.a((Object) str, (Object) dbPath))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str);
                        this.d = deviceProviderImp;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e2) {
                try {
                    FLogger.INSTANCE.getLocal().e(n, "getDeviceProvider - ex=" + e2);
                    if (TextUtils.isEmpty((CharSequence) null) || (!wg6.a((Object) str, (Object) null))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str);
                    }
                } catch (Throwable th) {
                    if (TextUtils.isEmpty((CharSequence) null) || (!wg6.a((Object) str, (Object) null))) {
                        this.d = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str);
                    }
                    throw th;
                }
            }
        }
        deviceProvider = this.d;
        if (deviceProvider == null) {
            wg6.a();
            throw null;
        }
        return deviceProvider;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized zn4 e() {
        zn4 zn4;
        String c2 = c();
        if (this.i == null) {
            this.i = new ao4(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "firmwares.db");
        } else {
            String str = c2 + "_" + "firmwares.db";
            zn4 zn42 = this.i;
            if (zn42 != null) {
                String dbPath = zn42.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.i = new ao4(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        zn4 = this.i;
        if (zn4 == null) {
            wg6.a();
            throw null;
        }
        return zn4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized xn4 f() {
        xn4 xn4;
        if (this.g == null) {
            String c2 = c();
            this.g = new yn4(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "hourNotification.db");
        }
        xn4 = this.g;
        if (xn4 == null) {
            wg6.a();
            throw null;
        }
        return xn4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized LocationProvider g() {
        LocationProvider locationProvider;
        String c2 = c();
        if (this.b == null) {
            this.b = new LocationProviderImpl(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "location.db");
        } else {
            String str = c2 + "_" + "location.db";
            LocationProvider locationProvider2 = this.b;
            if (locationProvider2 != null) {
                String dbPath = locationProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.b = new LocationProviderImpl(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        locationProvider = this.b;
        if (locationProvider == null) {
            wg6.a();
            throw null;
        }
        return locationProvider;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized bo4 h() {
        bo4 bo4;
        String c2 = c();
        if (this.e == null) {
            this.e = new co4(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "microAppSetting.db");
        } else {
            String str = c2 + "_" + "microAppSetting.db";
            bo4 bo42 = this.e;
            if (bo42 != null) {
                String dbPath = bo42.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getMicroAppSettingProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.e = new co4(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        bo4 = this.e;
        if (bo4 == null) {
            wg6.a();
            throw null;
        }
        return bo4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final do4 i() {
        if (this.l == null) {
            this.l = new eo4(PortfolioApp.get.instance().getApplicationContext(), "phone_favorites_contact.db");
        }
        do4 do4 = this.l;
        if (do4 != null) {
            return do4;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized fo4 j() {
        fo4 fo4;
        go4 go4;
        String c2 = c();
        if (this.h == null) {
            this.h = new go4(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "pin.db");
        } else {
            String str = c2 + "_" + "pin.db";
            try {
                fo4 fo42 = this.h;
                if (fo42 != null) {
                    String dbPath = fo42.getDbPath();
                    if (vv6.a(dbPath) || (!wg6.a((Object) str, (Object) dbPath))) {
                        go4 = new go4(PortfolioApp.get.instance().getApplicationContext(), str);
                        this.h = go4;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e2) {
                try {
                    FLogger.INSTANCE.getLocal().e(n, "getPinProvider - ex=" + e2);
                    if (vv6.a((String) null) || (!wg6.a((Object) str, (Object) null))) {
                        go4 = new go4(PortfolioApp.get.instance().getApplicationContext(), str);
                    }
                } catch (Throwable th) {
                    if (vv6.a((String) null) || (!wg6.a((Object) str, (Object) null))) {
                        this.h = new go4(PortfolioApp.get.instance().getApplicationContext(), str);
                    }
                    throw th;
                }
            }
        }
        fo4 = this.h;
        if (fo4 == null) {
            wg6.a();
            throw null;
        }
        return fo4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized SecondTimezoneProvider k() {
        SecondTimezoneProvider secondTimezoneProvider;
        String c2 = c();
        if (this.k == null) {
            this.k = new SecondTimezoneProviderImp(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + SecondTimezoneProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + SecondTimezoneProviderImp.DB_NAME;
            SecondTimezoneProvider secondTimezoneProvider2 = this.k;
            if (secondTimezoneProvider2 != null) {
                String dbPath = secondTimezoneProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSecondTimezoneProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.k = new SecondTimezoneProviderImp(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        secondTimezoneProvider = this.k;
        if (secondTimezoneProvider == null) {
            wg6.a();
            throw null;
        }
        return secondTimezoneProvider;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final ho4 l() {
        if (this.m == null) {
            synchronized (zm4.class) {
                if (this.m == null) {
                    String str = c() + "_" + "serverSetting.db";
                    FLogger.INSTANCE.getLocal().d(n, "getServerSettingProvider dbPath: " + str);
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    this.m = new io4(applicationContext, str);
                }
                cd6 cd6 = cd6.a;
            }
        }
        ho4 ho4 = this.m;
        if (ho4 != null) {
            return ho4;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized MFSleepSessionProvider m() {
        MFSleepSessionProvider mFSleepSessionProvider;
        String c2 = c();
        if (this.f == null) {
            this.f = new MFSleepSessionProviderImp(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "sleep.db");
        } else {
            String str = c2 + "_" + "sleep.db";
            MFSleepSessionProvider mFSleepSessionProvider2 = this.f;
            if (mFSleepSessionProvider2 != null) {
                String dbPath = mFSleepSessionProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.f = new MFSleepSessionProviderImp(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        mFSleepSessionProvider = this.f;
        if (mFSleepSessionProvider == null) {
            wg6.a();
            throw null;
        }
        return mFSleepSessionProvider;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final jo4 n() {
        if (this.j == null) {
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            this.j = new ko4(applicationContext, "user.db");
        }
        jo4 jo4 = this.j;
        if (jo4 != null) {
            return jo4;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void o() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.f = null;
        this.h = null;
        this.d = null;
        this.g = null;
        this.j = null;
        this.e = null;
        this.m = null;
    }

    @DexIgnore
    public /* synthetic */ zm4(qg6 qg6) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized AppFilterProvider a() {
        AppFilterProvider appFilterProvider;
        String c2 = c();
        if (this.c == null) {
            this.c = new un4(PortfolioApp.get.instance().getApplicationContext(), c2 + "_" + "appfilter.db");
        } else {
            String str = c2 + "_" + "appfilter.db";
            AppFilterProvider appFilterProvider2 = this.c;
            if (appFilterProvider2 != null) {
                String dbPath = appFilterProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wg6.a((Object) str, (Object) dbPath))) {
                    this.c = new un4(PortfolioApp.get.instance().getApplicationContext(), str);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        appFilterProvider = this.c;
        if (appFilterProvider == null) {
            wg6.a();
            throw null;
        }
        return appFilterProvider;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final synchronized DeviceProvider a(String str) {
        DeviceProvider deviceProvider;
        wg6.b(str, ButtonService.USER_ID);
        if (this.d == null) {
            String str2 = str + "_" + DeviceProviderImp.DB_NAME;
            FLogger.INSTANCE.getLocal().e(n, "Get device provider, current path " + str2);
            this.d = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
        }
        deviceProvider = this.d;
        if (deviceProvider == null) {
            wg6.a();
            throw null;
        }
        return deviceProvider;
    }
}
