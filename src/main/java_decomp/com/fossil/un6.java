package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un6 extends dl6 {
    @DexIgnore
    public static /* final */ un6 a; // = new un6();

    @DexIgnore
    public void a(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean b(af6 af6) {
        wg6.b(af6, "context");
        return false;
    }

    @DexIgnore
    public String toString() {
        return "Unconfined";
    }
}
