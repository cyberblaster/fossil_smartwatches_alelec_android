package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sv0 {
    SEND_PHONE_RANDOM_NUMBER(gs0.SET, zt0.SEND_PHONE_RANDOM_NUMBER, (gs0) null, (zt0) null, 12),
    SEND_BOTH_SIDES_RANDOM_NUMBERS(gs0.SET, zt0.SEND_BOTH_RANDOM_NUMBER, (gs0) null, (zt0) null, 12),
    EXCHANGE_PUBLIC_KEYS(gs0.SET, zt0.EXCHANGE_PUBLIC_KEY, (gs0) null, (zt0) null, 12),
    PROCESS_USER_AUTHORIZATION(gs0.GET, zt0.PROCESS_USER_AUTHORIZATION, (gs0) null, (zt0) null, 12),
    STOP_PROCESS(gs0.SET, zt0.STOP_PROCESS, (gs0) null, (zt0) null, 12);
    
    @DexIgnore
    public /* final */ gs0 a;
    @DexIgnore
    public /* final */ zt0 b;
    @DexIgnore
    public /* final */ gs0 c;
    @DexIgnore
    public /* final */ zt0 d;

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(2).put(this.a.a).put(this.b.a).array();
        wg6.a(array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        return array;
    }
}
