package com.fossil;

import android.content.Context;
import com.fossil.rv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aw2 {
    @DexIgnore
    public static /* final */ rv1.g<og2> a; // = new rv1.g<>();
    @DexIgnore
    public static /* final */ rv1.a<og2, rv1.d.C0044d> b; // = new kw2();
    @DexIgnore
    public static /* final */ rv1<rv1.d.C0044d> c; // = new rv1<>("LocationServices.API", b, a);
    @DexIgnore
    @Deprecated
    public static /* final */ vv2 d; // = new gh2();
    @DexIgnore
    @Deprecated
    public static /* final */ fw2 e; // = new wg2();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<R extends ew1> extends nw1<R, og2> {
        @DexIgnore
        public a(wv1 wv1) {
            super(aw2.c, wv1);
        }
    }

    /*
    static {
        new zf2();
    }
    */

    @DexIgnore
    public static wv2 a(Context context) {
        return new wv2(context);
    }

    @DexIgnore
    public static gw2 b(Context context) {
        return new gw2(context);
    }
}
