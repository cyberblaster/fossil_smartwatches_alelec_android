package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa2 extends aa2 implements da2 {
    @DexIgnore
    public fa2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    @DexIgnore
    public final boolean a(boolean z) throws RemoteException {
        Parcel q = q();
        ca2.a(q, true);
        Parcel a = a(2, q);
        boolean a2 = ca2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final String getId() throws RemoteException {
        Parcel a = a(1, q());
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    public final boolean zzc() throws RemoteException {
        Parcel a = a(6, q());
        boolean a2 = ca2.a(a);
        a.recycle();
        return a2;
    }
}
