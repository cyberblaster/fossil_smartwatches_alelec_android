package com.fossil;

import android.util.Log;
import com.fossil.ew1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gw1<R extends ew1> implements fw1<R> {
    @DexIgnore
    public abstract void a(R r);

    @DexIgnore
    public abstract void a(Status status);

    @DexIgnore
    public final void onResult(R r) {
        Status o = r.o();
        if (o.F()) {
            a(r);
            return;
        }
        a(o);
        if (r instanceof aw1) {
            try {
                ((aw1) r).a();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(r);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("ResultCallbacks", sb.toString(), e);
            }
        }
    }
}
