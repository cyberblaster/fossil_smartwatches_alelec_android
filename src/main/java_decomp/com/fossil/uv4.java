package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv4 implements Factory<qv4> {
    @DexIgnore
    public static qv4 a(rv4 rv4) {
        qv4 d = rv4.d();
        z76.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
