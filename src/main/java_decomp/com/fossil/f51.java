package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f51 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ j31 CREATOR; // = new j31((qg6) null);
    @DexIgnore
    public /* final */ vd0 a;
    @DexIgnore
    public /* final */ w40 b;

    @DexIgnore
    public f51(vd0 vd0, w40 w40) {
        this.a = vd0;
        this.b = w40;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.MICRO_APP_EVENT, (Object) this.a.a()), bm0.SYSTEM_VERSION, (Object) this.b.a());
    }

    @DexIgnore
    public abstract List<uj1> b();

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            f51 f51 = (f51) obj;
            return !(wg6.a(this.a, f51.a) ^ true) && !(wg6.a(this.b, f51.b) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppResponse");
    }

    @DexIgnore
    public final byte[] getData() {
        byte[] a2 = md6.a(this.a.b(), 0, 8);
        byte[] bArr = new byte[0];
        for (uj1 uj1 : b()) {
            ByteBuffer order = ByteBuffer.allocate(uj1.b().length + 2).order(ByteOrder.LITTLE_ENDIAN);
            order.putShort((short) ((uj1.b().length << 7) | uj1.a.a));
            order.put(uj1.b());
            byte[] array = order.array();
            wg6.a(array, "dataBuffer.array()");
            bArr = cw0.a(bArr, array);
        }
        ByteBuffer order2 = ByteBuffer.allocate(bArr.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order2, "ByteBuffer.allocate(3 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order2.put((byte) 255);
        order2.putShort((short) (((short) bArr.length) + 3));
        order2.put(bArr);
        byte[] array2 = order2.array();
        wg6.a(array2, "byteBuffer.array()");
        byte[] a3 = cw0.a(cw0.a(cw0.a(new byte[0], new byte[]{this.b.getMajor(), this.b.getMinor(), af0.REMOTE_ACTIVITY.a}), a2), array2);
        int a4 = (int) h51.a.a(a3, q11.CRC32);
        ByteBuffer order3 = ByteBuffer.allocate(a3.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order3, "ByteBuffer.allocate(resp\u2026(ByteOrder.LITTLE_ENDIAN)");
        order3.put(a3);
        order3.putInt(a4);
        byte[] array3 = order3.array();
        wg6.a(array3, "byteBuffer.array()");
        return array3;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getName());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public f51(Parcel parcel) {
        this(r0, (w40) r4);
        Parcelable readParcelable = parcel.readParcelable(vd0.class.getClassLoader());
        if (readParcelable != null) {
            vd0 vd0 = (vd0) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(w40.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
