package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h21 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B;
    @DexIgnore
    public short C;
    @DexIgnore
    public long D;
    @DexIgnore
    public int E;
    @DexIgnore
    public /* final */ ArrayList<ie1> F;
    @DexIgnore
    public /* final */ it0 G;
    @DexIgnore
    public /* final */ short H;
    @DexIgnore
    public float I;
    @DexIgnore
    public /* final */ h60 J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ float M;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ h21(ue1 ue1, q41 q41, h60 h60, boolean z, boolean z2, float f, String str, int i) {
        this(ue1, q41, h60, z, z2, (i & 32) != 0 ? 0.001f : f, (i & 64) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public static final /* synthetic */ void d(h21 h21) {
        h21.E++;
        if (h21.E < h21.C) {
            h21.n();
        } else if (h21.L) {
            h21.a(km1.a(h21.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else {
            h21.m();
        }
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (if1) new yy0(this.w, this.x, this.G, this.z), (hg6) new ty0(this), (hg6) new n01(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        JSONObject put = super.i().put(s60.BIOMETRIC_PROFILE.b(), this.J.d());
        wg6.a(put, "super.optionDescription(\u2026ofile.valueDescription())");
        return cw0.a(cw0.a(cw0.a(put, bm0.SKIP_READ_ACTIVITY_FILES, (Object) Boolean.valueOf(this.K)), bm0.SKIP_ERASE_ACTIVITY_FILES, (Object) Boolean.valueOf(this.L)), bm0.FILE_HANDLE, (Object) cw0.a(this.H));
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.FILES;
        Object[] array = this.F.toArray(new ie1[0]);
        if (array != null) {
            return cw0.a(k, bm0, (Object) cw0.a((p40[]) array));
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void m() {
        if1.a((if1) this, (qv0) new rq0(this.H, this.w, 0, 4), (hg6) new rm0(this), (hg6) new ko0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public final void n() {
        short s = (short) (this.H + this.E);
        if1.a((if1) this, (qv0) new kz0(s, this.D, this.w, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 24), (hg6) new cq0(this, s), (hg6) new ur0(this), (ig6) new nt0(this), (hg6) null, (hg6) null, 48, (Object) null);
    }

    @DexIgnore
    public h21(ue1 ue1, q41 q41, h60 h60, boolean z, boolean z2, float f, String str) {
        super(ue1, q41, eh1.LEGACY_SYNC, str);
        this.J = h60;
        this.K = z;
        this.L = z2;
        this.M = f;
        this.B = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.FILE_CONFIG, hl1.TRANSFER_DATA}));
        this.F = new ArrayList<>();
        this.G = mi0.A.p();
        this.H = 256;
    }

    @DexIgnore
    public Object d() {
        return this.F;
    }
}
