package com.fossil;

import android.database.Cursor;
import android.widget.Filter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv4 extends Filter {
    @DexIgnore
    public a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        Cursor a(CharSequence charSequence);

        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        Cursor b();

        @DexIgnore
        CharSequence b(Cursor cursor);
    }

    @DexIgnore
    public kv4(a aVar) {
        wg6.b(aVar, "mClient");
        this.a = aVar;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        wg6.b(obj, "resultValue");
        return this.a.b((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        wg6.b(charSequence, "constraint");
        Cursor a2 = this.a.a(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (a2 != null) {
            filterResults.count = a2.getCount();
            filterResults.values = a2;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        wg6.b(charSequence, "constraint");
        wg6.b(filterResults, "results");
        Cursor b = this.a.b();
        Object obj = filterResults.values;
        if (obj != null && (!wg6.a(obj, (Object) b))) {
            a aVar = this.a;
            Object obj2 = filterResults.values;
            if (obj2 != null) {
                aVar.a((Cursor) obj2);
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.database.Cursor");
        }
    }
}
