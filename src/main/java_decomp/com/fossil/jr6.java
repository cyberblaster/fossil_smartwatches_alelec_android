package com.fossil;

import com.fossil.sq6;
import com.fossil.yq6;
import com.misfit.frameworks.common.enums.Action;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr6 {
    @DexIgnore
    public /* final */ yq6 a;
    @DexIgnore
    public /* final */ Response b;

    @DexIgnore
    public jr6(yq6 yq6, Response response) {
        this.a = yq6;
        this.b = response;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        if (r3.l().b() == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        return false;
     */
    @DexIgnore
    public static boolean a(Response response, yq6 yq6) {
        int n = response.n();
        if (!(n == 200 || n == 410 || n == 414 || n == 501 || n == 203 || n == 204)) {
            if (n != 307) {
                if (!(n == 308 || n == 404 || n == 405)) {
                    switch (n) {
                        case 300:
                        case Action.Presenter.NEXT /*301*/:
                            break;
                        case Action.Presenter.PREVIOUS /*302*/:
                            break;
                    }
                }
            }
            if (response.e("Expires") == null) {
                if (response.l().d() == -1) {
                    if (!response.l().c()) {
                    }
                }
            }
        }
        if (response.l().i() || yq6.b().i()) {
            return false;
        }
        return true;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ yq6 b;
        @DexIgnore
        public /* final */ Response c;
        @DexIgnore
        public Date d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public String g;
        @DexIgnore
        public Date h;
        @DexIgnore
        public long i;
        @DexIgnore
        public long j;
        @DexIgnore
        public String k;
        @DexIgnore
        public int l; // = -1;

        @DexIgnore
        public a(long j2, yq6 yq6, Response response) {
            this.a = j2;
            this.b = yq6;
            this.c = response;
            if (response != null) {
                this.i = response.J();
                this.j = response.H();
                sq6 p = response.p();
                int b2 = p.b();
                for (int i2 = 0; i2 < b2; i2++) {
                    String a2 = p.a(i2);
                    String b3 = p.b(i2);
                    if ("Date".equalsIgnoreCase(a2)) {
                        this.d = xr6.a(b3);
                        this.e = b3;
                    } else if ("Expires".equalsIgnoreCase(a2)) {
                        this.h = xr6.a(b3);
                    } else if ("Last-Modified".equalsIgnoreCase(a2)) {
                        this.f = xr6.a(b3);
                        this.g = b3;
                    } else if ("ETag".equalsIgnoreCase(a2)) {
                        this.k = b3;
                    } else if ("Age".equalsIgnoreCase(a2)) {
                        this.l = yr6.a(b3, -1);
                    }
                }
            }
        }

        @DexIgnore
        public final long a() {
            Date date = this.d;
            long j2 = 0;
            if (date != null) {
                j2 = Math.max(0, this.j - date.getTime());
            }
            int i2 = this.l;
            if (i2 != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) i2));
            }
            long j3 = this.j;
            return j2 + (j3 - this.i) + (this.a - j3);
        }

        @DexIgnore
        public final long b() {
            long j2;
            long j3;
            cq6 l2 = this.c.l();
            if (l2.d() != -1) {
                return TimeUnit.SECONDS.toMillis((long) l2.d());
            }
            if (this.h != null) {
                Date date = this.d;
                if (date != null) {
                    j3 = date.getTime();
                } else {
                    j3 = this.j;
                }
                long time = this.h.getTime() - j3;
                if (time > 0) {
                    return time;
                }
                return 0;
            } else if (this.f == null || this.c.I().g().l() != null) {
                return 0;
            } else {
                Date date2 = this.d;
                if (date2 != null) {
                    j2 = date2.getTime();
                } else {
                    j2 = this.i;
                }
                long time2 = j2 - this.f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        @DexIgnore
        public jr6 c() {
            jr6 d2 = d();
            return (d2.a == null || !this.b.b().j()) ? d2 : new jr6((yq6) null, (Response) null);
        }

        @DexIgnore
        public final jr6 d() {
            if (this.c == null) {
                return new jr6(this.b, (Response) null);
            }
            if (this.b.d() && this.c.o() == null) {
                return new jr6(this.b, (Response) null);
            }
            if (!jr6.a(this.c, this.b)) {
                return new jr6(this.b, (Response) null);
            }
            cq6 b2 = this.b.b();
            if (b2.h() || a(this.b)) {
                return new jr6(this.b, (Response) null);
            }
            cq6 l2 = this.c.l();
            long a2 = a();
            long b3 = b();
            if (b2.d() != -1) {
                b3 = Math.min(b3, TimeUnit.SECONDS.toMillis((long) b2.d()));
            }
            long j2 = 0;
            long millis = b2.f() != -1 ? TimeUnit.SECONDS.toMillis((long) b2.f()) : 0;
            if (!l2.g() && b2.e() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) b2.e());
            }
            if (!l2.h()) {
                long j3 = millis + a2;
                if (j3 < j2 + b3) {
                    Response.a E = this.c.E();
                    if (j3 >= b3) {
                        E.a("Warning", "110 HttpURLConnection \"Response is stale\"");
                    }
                    if (a2 > LogBuilder.MAX_INTERVAL && e()) {
                        E.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                    }
                    return new jr6((yq6) null, E.a());
                }
            }
            String str = this.k;
            String str2 = "If-Modified-Since";
            if (str != null) {
                str2 = "If-None-Match";
            } else if (this.f != null) {
                str = this.g;
            } else if (this.d == null) {
                return new jr6(this.b, (Response) null);
            } else {
                str = this.e;
            }
            sq6.a a3 = this.b.c().a();
            dr6.a.a(a3, str2, str);
            yq6.a f2 = this.b.f();
            f2.a(a3.a());
            return new jr6(f2.a(), this.c);
        }

        @DexIgnore
        public final boolean e() {
            return this.c.l().d() == -1 && this.h == null;
        }

        @DexIgnore
        public static boolean a(yq6 yq6) {
            return (yq6.a("If-Modified-Since") == null && yq6.a("If-None-Match") == null) ? false : true;
        }
    }
}
