package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dz4 {
    @DexIgnore
    void a(NotificationAppsFragment notificationAppsFragment);

    @DexIgnore
    void a(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment);

    @DexIgnore
    void a(NotificationSettingsTypeFragment notificationSettingsTypeFragment);
}
