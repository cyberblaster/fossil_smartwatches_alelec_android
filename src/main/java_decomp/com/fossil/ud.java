package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ud {
    @DexIgnore
    public static final il6 a(td tdVar) {
        wg6.b(tdVar, "$this$viewModelScope");
        il6 il6 = (il6) tdVar.getTag("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (il6 != null) {
            return il6;
        }
        Object tagIfAbsent = tdVar.setTagIfAbsent("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new tc(mn6.a((rm6) null, 1, (Object) null).plus(zl6.c().o())));
        wg6.a(tagIfAbsent, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (il6) tagIfAbsent;
    }
}
