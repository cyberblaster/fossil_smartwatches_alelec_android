package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt0 extends xg6 implements ig6<qv0, Float, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ h21 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nt0(h21 h21) {
        super(2);
        this.a = h21;
    }

    @DexIgnore
    public Object invoke(Object obj, Object obj2) {
        qv0 qv0 = (qv0) obj;
        float floatValue = ((Number) obj2).floatValue();
        int i = 0;
        for (ie1 ie1 : this.a.F) {
            i += (int) ie1.f;
        }
        float e = ((floatValue * ((float) this.a.D)) + ((float) i)) / ((float) this.a.D);
        h21 h21 = this.a;
        if (e - h21.I > h21.M || e == 1.0f) {
            h21 h212 = this.a;
            h212.I = e;
            h212.a(e);
        }
        return cd6.a;
    }
}
