package com.fossil;

import android.util.Log;
import androidx.fragment.app.FragmentManager;
import pub.devrel.easypermissions.RationaleDialogFragmentCompat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xw6<T> extends zw6<T> {
    @DexIgnore
    public xw6(T t) {
        super(t);
    }

    @DexIgnore
    public void b(String str, String str2, String str3, int i, int i2, String... strArr) {
        FragmentManager c = c();
        if (c.b("RationaleDialogFragmentCompat") instanceof RationaleDialogFragmentCompat) {
            Log.d("BSPermissionsHelper", "Found existing fragment, not showing rationale.");
        } else {
            RationaleDialogFragmentCompat.a(str, str2, str3, i, i2, strArr).a(c, "RationaleDialogFragmentCompat");
        }
    }

    @DexIgnore
    public abstract FragmentManager c();
}
