package com.fossil;

import android.util.SparseArray;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface x15 extends k24<w15> {
    @DexIgnore
    void a(SparseArray<List<BaseFeatureModel>> sparseArray);
}
