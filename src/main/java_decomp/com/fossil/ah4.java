package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ah4 extends zg4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131362614, 1);
        z.put(2131362324, 2);
        z.put(2131362485, 3);
        z.put(2131362226, 4);
        z.put(2131362217, 5);
        z.put(2131362552, 6);
    }
    */

    @DexIgnore
    public ah4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public ah4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[4], objArr[2], objArr[3], objArr[6], objArr[1], objArr[0]);
        this.x = -1;
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
