package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tx1 extends qb3 {
    @DexIgnore
    public /* final */ WeakReference<ox1> a;

    @DexIgnore
    public tx1(ox1 ox1) {
        this.a = new WeakReference<>(ox1);
    }

    @DexIgnore
    public final void a(xb3 xb3) {
        ox1 ox1 = (ox1) this.a.get();
        if (ox1 != null) {
            ox1.a.a((iy1) new wx1(this, ox1, ox1, xb3));
        }
    }
}
