package com.fossil;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ij3 {
    @DexIgnore
    public /* final */ jj3[] a; // = new jj3[4];
    @DexIgnore
    public /* final */ Matrix[] b; // = new Matrix[4];
    @DexIgnore
    public /* final */ Matrix[] c; // = new Matrix[4];
    @DexIgnore
    public /* final */ PointF d; // = new PointF();
    @DexIgnore
    public /* final */ jj3 e; // = new jj3();
    @DexIgnore
    public /* final */ float[] f; // = new float[2];
    @DexIgnore
    public /* final */ float[] g; // = new float[2];

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(jj3 jj3, Matrix matrix, int i);

        @DexIgnore
        void b(jj3 jj3, Matrix matrix, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ hj3 a;
        @DexIgnore
        public /* final */ Path b;
        @DexIgnore
        public /* final */ RectF c;
        @DexIgnore
        public /* final */ a d;
        @DexIgnore
        public /* final */ float e;

        @DexIgnore
        public b(hj3 hj3, float f, RectF rectF, a aVar, Path path) {
            this.d = aVar;
            this.a = hj3;
            this.e = f;
            this.c = rectF;
            this.b = path;
        }
    }

    @DexIgnore
    public ij3() {
        for (int i = 0; i < 4; i++) {
            this.a[i] = new jj3();
            this.b[i] = new Matrix();
            this.c[i] = new Matrix();
        }
    }

    @DexIgnore
    public final float a(int i) {
        return (float) ((i + 1) * 90);
    }

    @DexIgnore
    public void a(hj3 hj3, float f2, RectF rectF, Path path) {
        a(hj3, f2, rectF, (a) null, path);
    }

    @DexIgnore
    public final void b(int i) {
        this.f[0] = this.a[i].c();
        this.f[1] = this.a[i].d();
        this.b[i].mapPoints(this.f);
        float a2 = a(i);
        this.c[i].reset();
        Matrix matrix = this.c[i];
        float[] fArr = this.f;
        matrix.setTranslate(fArr[0], fArr[1]);
        this.c[i].preRotate(a2);
    }

    @DexIgnore
    public final void c(b bVar, int i) {
        b(i, bVar.a).a(this.a[i], 90.0f, bVar.e, bVar.c, a(i, bVar.a));
        float a2 = a(i);
        this.b[i].reset();
        a(i, bVar.c, this.d);
        Matrix matrix = this.b[i];
        PointF pointF = this.d;
        matrix.setTranslate(pointF.x, pointF.y);
        this.b[i].preRotate(a2);
    }

    @DexIgnore
    public void a(hj3 hj3, float f2, RectF rectF, a aVar, Path path) {
        path.rewind();
        b bVar = new b(hj3, f2, rectF, aVar, path);
        for (int i = 0; i < 4; i++) {
            c(bVar, i);
            b(i);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            a(bVar, i2);
            b(bVar, i2);
        }
        path.close();
    }

    @DexIgnore
    public final void b(b bVar, int i) {
        int i2 = (i + 1) % 4;
        this.f[0] = this.a[i].c();
        this.f[1] = this.a[i].d();
        this.b[i].mapPoints(this.f);
        this.g[0] = this.a[i2].e();
        this.g[1] = this.a[i2].f();
        this.b[i2].mapPoints(this.g);
        float[] fArr = this.f;
        float f2 = fArr[0];
        float[] fArr2 = this.g;
        float max = Math.max(((float) Math.hypot((double) (f2 - fArr2[0]), (double) (fArr[1] - fArr2[1]))) - 0.001f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float a2 = a(bVar.c, i);
        this.e.b(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        c(i, bVar.a).a(max, a2, bVar.e, this.e);
        this.e.a(this.c[i], bVar.b);
        a aVar = bVar.d;
        if (aVar != null) {
            aVar.b(this.e, this.c[i], i);
        }
    }

    @DexIgnore
    public final void a(b bVar, int i) {
        this.f[0] = this.a[i].e();
        this.f[1] = this.a[i].f();
        this.b[i].mapPoints(this.f);
        if (i == 0) {
            Path path = bVar.b;
            float[] fArr = this.f;
            path.moveTo(fArr[0], fArr[1]);
        } else {
            Path path2 = bVar.b;
            float[] fArr2 = this.f;
            path2.lineTo(fArr2[0], fArr2[1]);
        }
        this.a[i].a(this.b[i], bVar.b);
        a aVar = bVar.d;
        if (aVar != null) {
            aVar.a(this.a[i], this.b[i], i);
        }
    }

    @DexIgnore
    public final cj3 c(int i, hj3 hj3) {
        if (i == 1) {
            return hj3.a();
        }
        if (i == 2) {
            return hj3.f();
        }
        if (i != 3) {
            return hj3.g();
        }
        return hj3.h();
    }

    @DexIgnore
    public final float a(RectF rectF, int i) {
        float[] fArr = this.f;
        jj3[] jj3Arr = this.a;
        fArr[0] = jj3Arr[i].c;
        fArr[1] = jj3Arr[i].d;
        this.b[i].mapPoints(fArr);
        if (i == 1 || i == 3) {
            return Math.abs(rectF.centerX() - this.f[0]);
        }
        return Math.abs(rectF.centerY() - this.f[1]);
    }

    @DexIgnore
    public final zi3 a(int i, hj3 hj3) {
        if (i == 1) {
            return hj3.e();
        }
        if (i == 2) {
            return hj3.c();
        }
        if (i != 3) {
            return hj3.l();
        }
        return hj3.j();
    }

    @DexIgnore
    public final aj3 b(int i, hj3 hj3) {
        if (i == 1) {
            return hj3.d();
        }
        if (i == 2) {
            return hj3.b();
        }
        if (i != 3) {
            return hj3.k();
        }
        return hj3.i();
    }

    @DexIgnore
    public final void a(int i, RectF rectF, PointF pointF) {
        if (i == 1) {
            pointF.set(rectF.right, rectF.bottom);
        } else if (i == 2) {
            pointF.set(rectF.left, rectF.bottom);
        } else if (i != 3) {
            pointF.set(rectF.right, rectF.top);
        } else {
            pointF.set(rectF.left, rectF.top);
        }
    }
}
