package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn2 extends sl2<String> implements xn2, RandomAccess {
    @DexIgnore
    public static /* final */ yn2 c;
    @DexIgnore
    public /* final */ List<Object> b;

    /*
    static {
        yn2 yn2 = new yn2();
        c = yn2;
        yn2.k();
    }
    */

    @DexIgnore
    public yn2() {
        this(10);
    }

    @DexIgnore
    public final void a(yl2 yl2) {
        a();
        this.b.add(yl2);
        this.modCount++;
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a();
        this.b.add(i, (String) obj);
        this.modCount++;
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @DexIgnore
    public final void clear() {
        a();
        this.b.clear();
        this.modCount++;
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        Object obj = this.b.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof yl2) {
            yl2 yl2 = (yl2) obj;
            String zzb = yl2.zzb();
            if (yl2.zzc()) {
                this.b.set(i, zzb);
            }
            return zzb;
        }
        byte[] bArr = (byte[]) obj;
        String b2 = hn2.b(bArr);
        if (hn2.a(bArr)) {
            this.b.set(i, b2);
        }
        return b2;
    }

    @DexIgnore
    public final xn2 n() {
        return zza() ? new bq2(this) : this;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        Object remove = this.b.remove(i);
        this.modCount++;
        return a(remove);
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        a();
        return a(this.b.set(i, (String) obj));
    }

    @DexIgnore
    public final int size() {
        return this.b.size();
    }

    @DexIgnore
    public final /* synthetic */ nn2 zza(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.b);
            return new yn2((ArrayList<Object>) arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final Object zzb(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    public yn2(int i) {
        this((ArrayList<Object>) new ArrayList(i));
    }

    @DexIgnore
    public final boolean addAll(int i, Collection<? extends String> collection) {
        a();
        if (collection instanceof xn2) {
            collection = ((xn2) collection).zzb();
        }
        boolean addAll = this.b.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    @DexIgnore
    public final List<?> zzb() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    public yn2(ArrayList<Object> arrayList) {
        this.b = arrayList;
    }

    @DexIgnore
    public static String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof yl2) {
            return ((yl2) obj).zzb();
        }
        return hn2.b((byte[]) obj);
    }
}
