package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yf2 implements Parcelable.Creator<xf2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 1) {
                f22.v(parcel, a);
            } else {
                status = (Status) f22.a(parcel, a, Status.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new xf2(status);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new xf2[i];
    }
}
