package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleButton E;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout t;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout u;
    @DexIgnore
    public /* final */ FloatingActionButton v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vd4(Object obj, View view, int i, Barrier barrier, Barrier barrier2, FlexibleButton flexibleButton, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FloatingActionButton floatingActionButton, RTLImageView rTLImageView, ImageView imageView, ImageView imageView2, ImageView imageView3, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ScrollView scrollView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextInputEditText;
        this.s = flexibleTextInputEditText2;
        this.t = flexibleTextInputLayout;
        this.u = flexibleTextInputLayout2;
        this.v = floatingActionButton;
        this.w = imageView;
        this.x = imageView2;
        this.y = imageView3;
        this.z = constraintLayout;
        this.A = constraintLayout2;
        this.B = constraintLayout4;
        this.C = flexibleTextView3;
        this.D = flexibleTextView4;
        this.E = flexibleButton2;
    }
}
