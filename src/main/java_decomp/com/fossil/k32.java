package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k32 implements Parcelable.Creator<j32> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        Bundle bundle = null;
        iv1[] iv1Arr = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                bundle = f22.a(parcel, a);
            } else if (a2 != 2) {
                f22.v(parcel, a);
            } else {
                iv1Arr = (iv1[]) f22.b(parcel, a, iv1.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new j32(bundle, iv1Arr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new j32[i];
    }
}
