package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.m24;
import com.fossil.v85;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o95 extends j95 {
    @DexIgnore
    public HybridCustomizeViewModel e;
    @DexIgnore
    public MutableLiveData<HybridPreset> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<n35> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<n35> i;
    @DexIgnore
    public /* final */ k95 j;
    @DexIgnore
    public /* final */ SetHybridPresetToWatchUseCase k;
    @DexIgnore
    public /* final */ an4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $currentPreset$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o95 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends sf6 implements ig6<il6, xe6<? super Parcelable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPresetAppSetting $buttonMapping;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, HybridPresetAppSetting hybridPresetAppSetting, xe6 xe6) {
                super(2, xe6);
                this.this$0 = bVar;
                this.$buttonMapping = hybridPresetAppSetting;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$buttonMapping, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    return o95.e(this.this$0.this$0).c(this.$buttonMapping.getAppId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.o95$b$b")
        /* renamed from: com.fossil.o95$b$b  reason: collision with other inner class name */
        public static final class C0032b implements m24.e<v85.d, v85.b> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0032b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
                wg6.b(dVar, "responseValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch success");
                this.a.this$0.j.n();
                this.a.this$0.j.f(true);
            }

            @DexIgnore
            public void a(SetHybridPresetToWatchUseCase.b bVar) {
                wg6.b(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch onError");
                this.a.this$0.j.n();
                int b = bVar.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HybridCustomizeEditPresenter", "setPresetToWatch() - mSetHybridPresetToWatchUseCase - onError - lastErrorCode = " + b);
                if (b != 1101) {
                    if (b == 8888) {
                        this.a.this$0.j.c();
                        return;
                    } else if (!(b == 1112 || b == 1113)) {
                        this.a.this$0.j.q();
                        return;
                    }
                }
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.a());
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                k95 g = this.a.this$0.j;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    g.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HybridPreset hybridPreset, xe6 xe6, o95 o95, HybridPreset hybridPreset2) {
            super(2, xe6);
            this.$it = hybridPreset;
            this.this$0 = o95;
            this.$currentPreset$inlined = hybridPreset2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$it, xe6, this.this$0, this.$currentPreset$inlined);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.o95$b$b] */
        /* JADX WARNING: type inference failed for: r1v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00b1, code lost:
            r9 = com.fossil.o95.a(r15.this$0);
            r10 = new com.fossil.o95.b.a(r15, r6, (com.fossil.xe6) null);
            r15.L$0 = r8;
            r15.L$1 = r7;
            r15.L$2 = r0;
            r15.L$3 = r6;
            r15.L$4 = r1;
            r15.label = 1;
            r9 = com.fossil.gk6.a(r9, r10, r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cc, code lost:
            if (r9 != r5) goto L_0x00cf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ce, code lost:
            return r5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00cf, code lost:
            r13 = r0;
            r0 = r15;
            r15 = r9;
            r9 = r5;
            r5 = r6;
            r6 = r13;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0087  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01a2  */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x0219  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0186 A[EDGE_INSN: B:74:0x0186->B:52:0x0186 ?: BREAK  , SYNTHETIC] */
        public final Object invokeSuspend(Object obj) {
            b bVar;
            il6 il6;
            List list;
            Object obj2;
            Iterator it;
            HybridPresetAppSetting hybridPresetAppSetting;
            HybridPresetAppSetting hybridPresetAppSetting2;
            b bVar2;
            Object obj3;
            HybridPresetAppSetting hybridPresetAppSetting3;
            HybridPresetAppSetting hybridPresetAppSetting4;
            String str;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                ArrayList<HybridPresetAppSetting> buttons = this.$it.getButtons();
                List arrayList = new ArrayList();
                for (HybridPresetAppSetting next : buttons) {
                    if (hf6.a(rk4.c.d(next.getAppId())).booleanValue()) {
                        arrayList.add(next);
                    }
                }
                if (!arrayList.isEmpty()) {
                    it = arrayList.iterator();
                    il6 = il62;
                    list = arrayList;
                    bVar = this;
                    obj2 = a2;
                    hybridPresetAppSetting = null;
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        hybridPresetAppSetting2 = (HybridPresetAppSetting) it.next();
                        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "check setting of " + hybridPresetAppSetting2);
                        try {
                        } catch (Exception e) {
                            e = e;
                            FLogger.INSTANCE.getLocal().e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                            while (true) {
                                if (!it.hasNext()) {
                                }
                            }
                            hybridPresetAppSetting = hybridPresetAppSetting2;
                            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                            if (hybridPresetAppSetting != null) {
                            }
                        }
                        if (vi4.a(hybridPresetAppSetting2.getSettings())) {
                            break;
                        }
                        String appId = hybridPresetAppSetting2.getAppId();
                        if (!wg6.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                            if (wg6.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue()) && TextUtils.isEmpty(((SecondTimezoneSetting) bVar.this$0.h.a(hybridPresetAppSetting2.getSettings(), SecondTimezoneSetting.class)).getTimeZoneId())) {
                                break;
                            }
                        } else if (TextUtils.isEmpty(((CommuteTimeSetting) bVar.this$0.h.a(hybridPresetAppSetting2.getSettings(), CommuteTimeSetting.class)).getAddress())) {
                            break;
                        }
                    }
                    hybridPresetAppSetting = hybridPresetAppSetting2;
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                    if (hybridPresetAppSetting != null) {
                        String appId2 = hybridPresetAppSetting.getAppId();
                        if (wg6.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                            str = jm4.a((Context) PortfolioApp.get.instance(), 2131886201);
                            wg6.a((Object) str, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                        } else if (wg6.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                            str = jm4.a((Context) PortfolioApp.get.instance(), 2131886361);
                            wg6.a((Object) str, "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)");
                        } else if (wg6.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887317);
                            wg6.a((Object) str, "LanguageHelper.getString\u2026ing_phone_not_configured)");
                        } else {
                            str = "";
                        }
                        bVar.this$0.j.b(str, hybridPresetAppSetting.getAppId(), hybridPresetAppSetting.getPosition());
                        return cd6.a;
                    }
                    bVar.this$0.j.m();
                    bVar.this$0.k.a(new SetHybridPresetToWatchUseCase.c(bVar.$currentPreset$inlined), new C0032b(bVar));
                    return cd6.a;
                }
                bVar = this;
                hybridPresetAppSetting = null;
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            } else if (i == 1) {
                it = (Iterator) this.L$4;
                hybridPresetAppSetting3 = (HybridPresetAppSetting) this.L$3;
                hybridPresetAppSetting4 = (HybridPresetAppSetting) this.L$2;
                list = (List) this.L$1;
                il6 = (il6) this.L$0;
                try {
                    nc6.a(obj);
                    obj3 = a2;
                    bVar2 = this;
                } catch (Exception e2) {
                    obj2 = a2;
                    hybridPresetAppSetting = hybridPresetAppSetting4;
                    e = e2;
                    bVar = this;
                    FLogger.INSTANCE.getLocal().e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                    while (true) {
                        if (!it.hasNext()) {
                        }
                    }
                    hybridPresetAppSetting = hybridPresetAppSetting2;
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                    if (hybridPresetAppSetting != null) {
                    }
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
            } catch (Exception e3) {
                obj2 = obj3;
                HybridPresetAppSetting hybridPresetAppSetting5 = hybridPresetAppSetting4;
                e = e3;
                bVar = bVar2;
                hybridPresetAppSetting = hybridPresetAppSetting5;
                FLogger.INSTANCE.getLocal().e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                while (true) {
                    if (!it.hasNext()) {
                    }
                }
                hybridPresetAppSetting = hybridPresetAppSetting2;
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            }
            Parcelable parcelable = (Parcelable) obj;
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "last setting " + parcelable);
            if (parcelable != null) {
                hybridPresetAppSetting3.setSettings(bVar2.this$0.h.a(parcelable));
                bVar = bVar2;
                hybridPresetAppSetting = hybridPresetAppSetting4;
                obj2 = obj3;
                while (true) {
                    if (!it.hasNext()) {
                    }
                }
                hybridPresetAppSetting = hybridPresetAppSetting2;
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            }
            bVar = bVar2;
            hybridPresetAppSetting = hybridPresetAppSetting3;
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
            if (hybridPresetAppSetting != null) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ o95 a;

        @DexIgnore
        public c(o95 o95) {
            this.a = o95;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(HybridPreset hybridPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe current preset value=" + hybridPreset);
            MutableLiveData b = this.a.f;
            if (hybridPreset != null) {
                b.a(hybridPreset.clone());
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ o95 a;

        @DexIgnore
        public d(o95 o95) {
            this.a = o95;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe selected microApp value=" + str);
            k95 g = this.a.j;
            if (str != null) {
                g.q(str);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<n35> {
        @DexIgnore
        public /* final */ /* synthetic */ o95 a;

        @DexIgnore
        public e(o95 o95) {
            this.a = o95;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(n35 n35) {
            if (n35 != null) {
                this.a.j.a(n35);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ o95 a;

        @DexIgnore
        public f(o95 o95) {
            this.a = o95;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "update preset status isChanged=" + bool);
            k95 g = this.a.j;
            if (bool != null) {
                g.e(bool.booleanValue());
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ o95 a;

        @DexIgnore
        public g(o95 o95) {
            this.a = o95;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r8v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public final MutableLiveData<n35> apply(HybridPreset hybridPreset) {
            String str;
            String component1 = hybridPreset.component1();
            String component2 = hybridPreset.component2();
            ArrayList<HybridPresetAppSetting> component4 = hybridPreset.component4();
            boolean component5 = hybridPreset.component5();
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(component4);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microApps=" + arrayList);
            ArrayList arrayList2 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) it.next();
                String component12 = hybridPresetAppSetting.component1();
                MicroApp b = o95.e(this.a).b(hybridPresetAppSetting.component2());
                if (b != null) {
                    String id = b.getId();
                    String icon = b.getIcon();
                    if (icon != null) {
                        str = icon;
                    } else {
                        str = "";
                    }
                    o35 o35 = r9;
                    o35 o352 = new o35(id, str, jm4.a(PortfolioApp.get.instance(), b.getNameKey(), b.getName()), component12, (String) null, 16, (qg6) null);
                    arrayList2.add(o35);
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microAppsDetail=" + arrayList2);
            MutableLiveData c = this.a.g;
            if (component2 == null) {
                component2 = "";
            }
            c.a(new n35(component1, component2, arrayList2, component5));
            return this.a.g;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public o95(k95 k95, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, an4 an4) {
        wg6.b(k95, "mView");
        wg6.b(setHybridPresetToWatchUseCase, "mSetHybridPresetToWatchUseCase");
        wg6.b(an4, "mSharedPreferencesManager");
        this.j = k95;
        this.k = setHybridPresetToWatchUseCase;
        this.l = an4;
        LiveData<n35> b2 = sd.b(this.f, new g(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.i = b2;
    }

    @DexIgnore
    public static final /* synthetic */ HybridCustomizeViewModel e(o95 o95) {
        HybridCustomizeViewModel hybridCustomizeViewModel = o95.e;
        if (hybridCustomizeViewModel != null) {
            return hybridCustomizeViewModel;
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            boolean g2 = hybridCustomizeViewModel.g();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "onUserExit isPresetChanged " + g2);
            if (g2) {
                this.j.p();
            } else {
                this.j.f(false);
            }
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.j.a(this);
    }

    @DexIgnore
    public void a(HybridCustomizeViewModel hybridCustomizeViewModel) {
        wg6.b(hybridCustomizeViewModel, "viewModel");
        this.e = hybridCustomizeViewModel;
    }

    @DexIgnore
    public void b(String str, String str2) {
        T t;
        wg6.b(str, "fromPosition");
        wg6.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!wg6.a((Object) str, (Object) str2)) {
            HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
            T t2 = null;
            if (hybridCustomizeViewModel != null) {
                HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel.a().a();
                if (hybridPreset != null) {
                    HybridPreset clone = hybridPreset.clone();
                    Iterator<T> it = clone.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (wg6.a((Object) ((HybridPresetAppSetting) t).getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                    Iterator<T> it2 = clone.getButtons().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (wg6.a((Object) ((HybridPresetAppSetting) next).getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t2;
                    if (hybridPresetAppSetting != null) {
                        hybridPresetAppSetting.setPosition(str2);
                    }
                    if (hybridPresetAppSetting2 != null) {
                        hybridPresetAppSetting2.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - update preset");
                    a(clone);
                    return;
                }
                return;
            }
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.get.instance().e())) {
            this.j.F();
        }
        this.k.e();
        BleCommandResultManager.d.a(CommunicateMode.SET_LINK_MAPPING);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            MutableLiveData<HybridPreset> a2 = hybridCustomizeViewModel.a();
            k95 k95 = this.j;
            if (k95 != null) {
                a2.a((HybridCustomizeEditFragment) k95, new c(this));
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.e().a(this.j, new d(this));
                    this.i.a(this.j, new e(this));
                    HybridCustomizeViewModel hybridCustomizeViewModel3 = this.e;
                    if (hybridCustomizeViewModel3 != null) {
                        hybridCustomizeViewModel3.b().a(this.j, new f(this));
                    } else {
                        wg6.d("mHybridCustomizeViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            }
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        this.k.g();
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            MutableLiveData<HybridPreset> a2 = hybridCustomizeViewModel.a();
            k95 k95 = this.j;
            if (k95 != null) {
                a2.a((HybridCustomizeEditFragment) k95);
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.e().a(this.j);
                    this.g.a(this.j);
                    return;
                }
                wg6.d("mHybridCustomizeViewModel");
                throw null;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "microAppPos");
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            hybridCustomizeViewModel.e(str);
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        wg6.b(str, "microAppId");
        wg6.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "dropMicroApp - microAppid=" + str + ", toPosition=" + str2);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        T t = null;
        if (hybridCustomizeViewModel == null) {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        } else if (!hybridCustomizeViewModel.d(str)) {
            HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
            if (hybridCustomizeViewModel2 != null) {
                HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel2.a().a();
                if (hybridPreset != null) {
                    Iterator<T> it = hybridPreset.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        if (wg6.a((Object) ((HybridPresetAppSetting) next).getPosition(), (Object) str2)) {
                            t = next;
                            break;
                        }
                    }
                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                    if (hybridPresetAppSetting != null) {
                        hybridPresetAppSetting.setAppId(str);
                        hybridPresetAppSetting.setSettings("");
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - update preset");
                    wg6.a((Object) hybridPreset, "currentPreset");
                    a(hybridPreset);
                    return;
                }
                return;
            }
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(boolean z) {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel.a().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "setPresetToWatch currentPreset=" + hybridPreset);
            Set<Integer> a2 = xm4.d.a(hybridPreset);
            if (z) {
                this.l.q(true);
                this.l.r(true);
            }
            xm4 xm4 = xm4.d;
            k95 k95 = this.j;
            if (k95 == null) {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            } else if (xm4.a(((HybridCustomizeEditFragment) k95).getContext(), a2) && hybridPreset != null) {
                rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(hybridPreset, (xe6) null, this, hybridPreset), 3, (Object) null);
            }
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(HybridPreset hybridPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "updateCurrentPreset=" + hybridPreset);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            hybridCustomizeViewModel.a(hybridPreset);
        } else {
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset hybridPreset = (HybridPreset) hybridCustomizeViewModel.a().a();
            if (!(hybridPreset == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", this.h.a(hybridPreset));
            }
            HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
            if (hybridCustomizeViewModel2 != null) {
                HybridPreset c2 = hybridCustomizeViewModel2.c();
                if (!(c2 == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", this.h.a(c2));
                }
                HybridCustomizeViewModel hybridCustomizeViewModel3 = this.e;
                if (hybridCustomizeViewModel3 != null) {
                    String str = (String) hybridCustomizeViewModel3.e().a();
                    if (!(str == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", str);
                    }
                    return bundle;
                }
                wg6.d("mHybridCustomizeViewModel");
                throw null;
            }
            wg6.d("mHybridCustomizeViewModel");
            throw null;
        }
        wg6.d("mHybridCustomizeViewModel");
        throw null;
    }
}
