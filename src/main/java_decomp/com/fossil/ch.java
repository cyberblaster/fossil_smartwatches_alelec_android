package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ch extends bh {
    @DexIgnore
    public ch(long j, RenderScript renderScript) {
        super(j, renderScript);
        if (j == 0) {
            throw new ah("Loading of ScriptIntrinsic failed.");
        }
    }
}
