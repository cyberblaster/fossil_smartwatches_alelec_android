package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class la4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ rg4 q;
    @DexIgnore
    public /* final */ rg4 r;
    @DexIgnore
    public /* final */ LinearLayout s;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public /* final */ TodayHeartRateChart u;

    @DexIgnore
    public la4(Object obj, View view, int i, FlexibleTextView flexibleTextView, rg4 rg4, rg4 rg42, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, TodayHeartRateChart todayHeartRateChart) {
        super(obj, view, i);
        this.q = rg4;
        a(this.q);
        this.r = rg42;
        a(this.r);
        this.s = linearLayout;
        this.t = linearLayout2;
        this.u = todayHeartRateChart;
    }
}
