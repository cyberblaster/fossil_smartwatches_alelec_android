package com.fossil;

import android.os.Bundle;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vx1 implements wv1.b, wv1.c {
    @DexIgnore
    public /* final */ /* synthetic */ ox1 a;

    @DexIgnore
    public vx1(ox1 ox1) {
        this.a = ox1;
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        this.a.b.lock();
        try {
            if (this.a.a(gv1)) {
                this.a.g();
                this.a.e();
            } else {
                this.a.b(gv1);
            }
        } finally {
            this.a.b.unlock();
        }
    }

    @DexIgnore
    public final void f(Bundle bundle) {
        if (this.a.r.k()) {
            this.a.b.lock();
            try {
                if (this.a.k != null) {
                    this.a.k.a(new tx1(this.a));
                    this.a.b.unlock();
                }
            } finally {
                this.a.b.unlock();
            }
        } else {
            this.a.k.a(new tx1(this.a));
        }
    }

    @DexIgnore
    public final void g(int i) {
    }

    @DexIgnore
    public /* synthetic */ vx1(ox1 ox1, nx1 nx1) {
        this(ox1);
    }
}
