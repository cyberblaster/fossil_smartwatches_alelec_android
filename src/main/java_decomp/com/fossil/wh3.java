package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wh3 implements ai3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ExtendedFloatingActionButton b;
    @DexIgnore
    public /* final */ ArrayList<Animator.AnimatorListener> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ vh3 d;
    @DexIgnore
    public fg3 e;
    @DexIgnore
    public fg3 f;

    @DexIgnore
    public wh3(ExtendedFloatingActionButton extendedFloatingActionButton, vh3 vh3) {
        this.b = extendedFloatingActionButton;
        this.a = extendedFloatingActionButton.getContext();
        this.d = vh3;
    }

    @DexIgnore
    public final void a(fg3 fg3) {
        this.f = fg3;
    }

    @DexIgnore
    public AnimatorSet b(fg3 fg3) {
        ArrayList arrayList = new ArrayList();
        if (fg3.c("opacity")) {
            arrayList.add(fg3.a("opacity", this.b, View.ALPHA));
        }
        if (fg3.c("scale")) {
            arrayList.add(fg3.a("scale", this.b, View.SCALE_Y));
            arrayList.add(fg3.a("scale", this.b, View.SCALE_X));
        }
        if (fg3.c("width")) {
            arrayList.add(fg3.a("width", this.b, ExtendedFloatingActionButton.F));
        }
        if (fg3.c("height")) {
            arrayList.add(fg3.a("height", this.b, ExtendedFloatingActionButton.G));
        }
        AnimatorSet animatorSet = new AnimatorSet();
        zf3.a(animatorSet, arrayList);
        return animatorSet;
    }

    @DexIgnore
    public fg3 d() {
        return this.f;
    }

    @DexIgnore
    public void f() {
        this.d.b();
    }

    @DexIgnore
    public AnimatorSet g() {
        return b(i());
    }

    @DexIgnore
    public final List<Animator.AnimatorListener> h() {
        return this.c;
    }

    @DexIgnore
    public final fg3 i() {
        fg3 fg3 = this.f;
        if (fg3 != null) {
            return fg3;
        }
        if (this.e == null) {
            this.e = fg3.a(this.a, b());
        }
        fg3 fg32 = this.e;
        y8.a(fg32);
        return fg32;
    }

    @DexIgnore
    public void onAnimationStart(Animator animator) {
        this.d.a(animator);
    }

    @DexIgnore
    public void a() {
        this.d.b();
    }
}
