package com.fossil;

import android.view.View;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RelativeLayout r;
    @DexIgnore
    public /* final */ TabLayout s;
    @DexIgnore
    public /* final */ TabLayout t;
    @DexIgnore
    public /* final */ TabLayout u;
    @DexIgnore
    public /* final */ TabLayout v;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bd4(Object obj, View view, int i, RTLImageView rTLImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, ConstraintLayout constraintLayout3, RelativeLayout relativeLayout, ConstraintLayout constraintLayout4, TabItem tabItem, TabItem tabItem2, TabItem tabItem3, TabItem tabItem4, TabItem tabItem5, TabItem tabItem6, TabItem tabItem7, TabItem tabItem8, TabLayout tabLayout, TabLayout tabLayout2, TabLayout tabLayout3, TabLayout tabLayout4, FlexibleTextView flexibleTextView5, ConstraintLayout constraintLayout5) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = relativeLayout;
        this.s = tabLayout;
        this.t = tabLayout2;
        this.u = tabLayout3;
        this.v = tabLayout4;
    }
}
