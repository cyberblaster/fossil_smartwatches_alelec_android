package com.fossil;

import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y34 {
    @DexIgnore
    public final long a(DateTime dateTime) {
        if (dateTime != null) {
            return dateTime.getMillis();
        }
        return 0;
    }

    @DexIgnore
    public final DateTime a(long j) {
        return new DateTime(j);
    }
}
