package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bb5$b$a<T> implements ld<cf<ActivitySummary>> {
    @DexIgnore
    public /* final */ /* synthetic */ DashboardActiveTimePresenter.b a;

    @DexIgnore
    public bb5$b$a(DashboardActiveTimePresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(cf<ActivitySummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getSummariesPaging observer size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardActiveTimePresenter", sb.toString());
        if (cfVar != null) {
            this.a.this$0.g.a(cfVar);
        }
    }
}
