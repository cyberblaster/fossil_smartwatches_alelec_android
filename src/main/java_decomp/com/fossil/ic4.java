package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ic4 extends hc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131362542, 1);
        z.put(2131362442, 2);
        z.put(2131362019, 3);
        z.put(2131361815, 4);
        z.put(2131362691, 5);
        z.put(2131362787, 6);
        z.put(2131362260, 7);
        z.put(2131362656, 8);
        z.put(2131362079, 9);
        z.put(2131361816, 10);
        z.put(2131362690, 11);
        z.put(2131362786, 12);
        z.put(2131362261, 13);
    }
    */

    @DexIgnore
    public ic4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 14, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public ic4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[10], objArr[3], objArr[9], objArr[7], objArr[13], objArr[2], objArr[1], objArr[8], objArr[11], objArr[5], objArr[12], objArr[6], objArr[0]);
        this.x = -1;
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
