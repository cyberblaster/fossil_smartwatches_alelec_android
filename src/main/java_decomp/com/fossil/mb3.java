package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.c12;
import com.fossil.wv1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mb3 extends i12<rb3> implements ac3 {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ e12 F;
    @DexIgnore
    public /* final */ Bundle G;
    @DexIgnore
    public Integer H;

    @DexIgnore
    public mb3(Context context, Looper looper, boolean z, e12 e12, Bundle bundle, wv1.b bVar, wv1.c cVar) {
        super(context, looper, 44, e12, bVar, cVar);
        this.E = true;
        this.F = e12;
        this.G = bundle;
        this.H = e12.e();
    }

    @DexIgnore
    public String A() {
        return "com.google.android.gms.signin.service.START";
    }

    @DexIgnore
    public final void a(n12 n12, boolean z) {
        try {
            ((rb3) y()).a(n12, this.H.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    @DexIgnore
    public final void b() {
        a((c12.c) new c12.d());
    }

    @DexIgnore
    public final void h() {
        try {
            ((rb3) y()).d(this.H.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    @DexIgnore
    public int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public boolean m() {
        return this.E;
    }

    @DexIgnore
    public Bundle v() {
        if (!u().getPackageName().equals(this.F.h())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.h());
        }
        return this.G;
    }

    @DexIgnore
    public String z() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @DexIgnore
    public final void a(pb3 pb3) {
        w12.a(pb3, (Object) "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.F.c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = vt1.a(u()).b();
            }
            ((rb3) y()).a(new vb3(new x12(c, this.H.intValue(), googleSignInAccount)), pb3);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                pb3.a(new xb3(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @DexIgnore
    public mb3(Context context, Looper looper, boolean z, e12 e12, lb3 lb3, wv1.b bVar, wv1.c cVar) {
        this(context, looper, true, e12, a(e12), bVar, cVar);
    }

    @DexIgnore
    public static Bundle a(e12 e12) {
        lb3 j = e12.j();
        Integer e = e12.e();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", e12.a());
        if (e != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", e.intValue());
        }
        if (j != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", j.h());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", j.g());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", j.e());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", j.f());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", j.b());
            bundle.putString("com.google.android.gms.signin.internal.logSessionId", j.c());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", j.i());
            if (j.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", j.a().longValue());
            }
            if (j.d() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", j.d().longValue());
            }
        }
        return bundle;
    }

    @DexIgnore
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof rb3) {
            return (rb3) queryLocalInterface;
        }
        return new tb3(iBinder);
    }
}
