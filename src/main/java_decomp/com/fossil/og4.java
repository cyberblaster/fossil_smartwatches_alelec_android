package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class og4 extends ng4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131362321, 1);
        B.put(2131362320, 2);
        B.put(2131362653, 3);
        B.put(2131362026, 4);
        B.put(2131362417, 5);
        B.put(2131362416, 6);
        B.put(2131362939, 7);
        B.put(2131362389, 8);
        B.put(2131362388, 9);
        B.put(2131362397, 10);
        B.put(2131362374, 11);
    }
    */

    @DexIgnore
    public og4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public og4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[0], objArr[2], objArr[1], objArr[11], objArr[9], objArr[8], objArr[10], objArr[6], objArr[5], objArr[3], objArr[7]);
        this.z = -1;
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
