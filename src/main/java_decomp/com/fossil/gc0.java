package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gc0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new gc0(parcel.readLong(), parcel.readLong());
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new gc0[i];
        }
    }

    @DexIgnore
    public gc0(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.STEP, (Object) Long.valueOf(this.a)), bm0.CALORIES, (Object) Long.valueOf(this.b));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final long getCaloriesOffset() {
        return this.b;
    }

    @DexIgnore
    public final long getStepOffset() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.b);
    }
}
