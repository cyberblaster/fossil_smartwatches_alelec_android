package com.fossil;

import java.util.Collection;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o40 extends n40 {
    @DexIgnore
    public /* final */ HashMap<String, n40> a; // = new HashMap<>();

    @DexIgnore
    public boolean a(ii1 ii1) {
        for (n40 a2 : this.a.values()) {
            if (!a2.a(ii1)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final o40 setDeviceTypes(s40[] s40Arr) {
        HashMap<String, n40> hashMap = this.a;
        String simpleName = zq0.class.getSimpleName();
        wg6.a(simpleName, "DeviceTypesScanFilter::class.java.simpleName");
        hashMap.put(simpleName, new zq0(s40Arr));
        return this;
    }

    @DexIgnore
    public final o40 setSerialNumberPattern(String str) {
        HashMap<String, n40> hashMap = this.a;
        String simpleName = ss0.class.getSimpleName();
        wg6.a(simpleName, "SerialNumberPatternScanF\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new ss0(str));
        return this;
    }

    @DexIgnore
    public final o40 setSerialNumberPrefixes(String[] strArr) {
        HashMap<String, n40> hashMap = this.a;
        String simpleName = ku0.class.getSimpleName();
        wg6.a(simpleName, "SerialNumberPrefixesScan\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new ku0(strArr));
        return this;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        bm0 bm0 = bm0.DEVICE_FILTER;
        Collection<n40> values = this.a.values();
        wg6.a(values, "deviceFilters.values");
        Object[] array = values.toArray(new n40[0]);
        if (array != null) {
            return cw0.a(jSONObject, bm0, (Object) cw0.a((p40[]) array));
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
