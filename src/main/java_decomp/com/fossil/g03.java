package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g03 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ i03 f;

    @DexIgnore
    public g03(x53 x53, String str, String str2, String str3, long j, long j2, i03 i03) {
        w12.b(str2);
        w12.b(str3);
        w12.a(i03);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        long j3 = this.e;
        if (j3 != 0 && j3 > this.d) {
            x53.b().w().a("Event created with reverse previous/current timestamps. appId, name", t43.a(str2), t43.a(str3));
        }
        this.f = i03;
    }

    @DexIgnore
    public final g03 a(x53 x53, long j) {
        return new g03(x53, this.c, this.a, this.b, this.d, j, this.f);
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public g03(x53 x53, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        i03 i03;
        w12.b(str2);
        w12.b(str3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        long j3 = this.e;
        if (j3 != 0 && j3 > this.d) {
            x53.b().w().a("Event created with reverse previous/current timestamps. appId", t43.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            i03 = new i03(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String str4 = (String) it.next();
                if (str4 == null) {
                    x53.b().t().a("Param name can't be null");
                    it.remove();
                } else {
                    Object a2 = x53.w().a(str4, bundle2.get(str4));
                    if (a2 == null) {
                        x53.b().w().a("Param value can't be null", x53.x().b(str4));
                        it.remove();
                    } else {
                        x53.w().a(bundle2, str4, a2);
                    }
                }
            }
            i03 = new i03(bundle2);
        }
        this.f = i03;
    }
}
