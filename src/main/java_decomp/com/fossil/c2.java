package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.fossil.q1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c2 extends q1 implements SubMenu {
    @DexIgnore
    public q1 B;
    @DexIgnore
    public t1 C;

    @DexIgnore
    public c2(Context context, q1 q1Var, t1 t1Var) {
        super(context);
        this.B = q1Var;
        this.C = t1Var;
    }

    @DexIgnore
    public void a(q1.a aVar) {
        this.B.a(aVar);
    }

    @DexIgnore
    public boolean b(t1 t1Var) {
        return this.B.b(t1Var);
    }

    @DexIgnore
    public String d() {
        t1 t1Var = this.C;
        int itemId = t1Var != null ? t1Var.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.d() + ":" + itemId;
    }

    @DexIgnore
    public MenuItem getItem() {
        return this.C;
    }

    @DexIgnore
    public q1 m() {
        return this.B.m();
    }

    @DexIgnore
    public boolean o() {
        return this.B.o();
    }

    @DexIgnore
    public boolean p() {
        return this.B.p();
    }

    @DexIgnore
    public boolean q() {
        return this.B.q();
    }

    @DexIgnore
    public void setGroupDividerEnabled(boolean z) {
        this.B.setGroupDividerEnabled(z);
    }

    @DexIgnore
    public SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(Drawable drawable) {
        this.C.setIcon(drawable);
        return this;
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        this.B.setQwertyMode(z);
    }

    @DexIgnore
    public Menu t() {
        return this.B;
    }

    @DexIgnore
    public boolean a(q1 q1Var, MenuItem menuItem) {
        return super.a(q1Var, menuItem) || this.B.a(q1Var, menuItem);
    }

    @DexIgnore
    public SubMenu setHeaderIcon(int i) {
        super.d(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(int i) {
        super.e(i);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(int i) {
        this.C.setIcon(i);
        return this;
    }

    @DexIgnore
    public boolean a(t1 t1Var) {
        return this.B.a(t1Var);
    }
}
