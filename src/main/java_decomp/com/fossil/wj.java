package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wj extends bk implements yj {
    @DexIgnore
    public wj(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    public static wj a(ViewGroup viewGroup) {
        return (wj) bk.c(viewGroup);
    }

    @DexIgnore
    public void b(View view) {
        this.a.b(view);
    }

    @DexIgnore
    public void a(View view) {
        this.a.a(view);
    }
}
