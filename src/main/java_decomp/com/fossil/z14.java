package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z14 implements Factory<zm4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public z14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static z14 a(b14 b14) {
        return new z14(b14);
    }

    @DexIgnore
    public static zm4 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static zm4 c(b14 b14) {
        zm4 m = b14.m();
        z76.a(m, "Cannot return null from a non-@Nullable @Provides method");
        return m;
    }

    @DexIgnore
    public zm4 get() {
        return b(this.a);
    }
}
