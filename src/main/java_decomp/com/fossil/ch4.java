package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ch4 extends bh4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362321, 1);
        A.put(2131362320, 2);
        A.put(2131362653, 3);
        A.put(2131362028, 4);
        A.put(2131363159, 5);
        A.put(2131363160, 6);
        A.put(2131363183, 7);
        A.put(2131363185, 8);
        A.put(2131362316, 9);
        A.put(2131362342, 10);
        A.put(2131362374, 11);
    }
    */

    @DexIgnore
    public ch4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public ch4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[0], objArr[9], objArr[2], objArr[1], objArr[10], objArr[11], objArr[3], objArr[5], objArr[6], objArr[7], objArr[8]);
        this.y = -1;
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
