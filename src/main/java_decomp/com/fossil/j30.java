package com.fossil;

import com.fossil.y30;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j30 implements y30 {
    @DexIgnore
    public /* final */ File[] a;
    @DexIgnore
    public /* final */ Map<String, String> b; // = new HashMap(z30.g);
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public j30(String str, File[] fileArr) {
        this.a = fileArr;
        this.c = str;
    }

    @DexIgnore
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.b);
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public File c() {
        return this.a[0];
    }

    @DexIgnore
    public File[] d() {
        return this.a;
    }

    @DexIgnore
    public String e() {
        return this.a[0].getName();
    }

    @DexIgnore
    public y30.a getType() {
        return y30.a.JAVA;
    }

    @DexIgnore
    public void remove() {
        for (File file : this.a) {
            c86.g().d("CrashlyticsCore", "Removing invalid report file at " + file.getPath());
            file.delete();
        }
    }
}
