package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs3 implements Parcelable.Creator<ds3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder != null) {
            return new ds3(readStrongBinder);
        }
        return null;
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ds3[i];
    }
}
