package com.fossil;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go3<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Comparator<? super T> b;
    @DexIgnore
    public /* final */ T[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public T e;

    @DexIgnore
    public go3(Comparator<? super T> comparator, int i) {
        jk3.a(comparator, (Object) "comparator");
        this.b = comparator;
        this.a = i;
        jk3.a(i >= 0, "k must be nonnegative, was %s", i);
        this.c = new Object[(i * 2)];
        this.d = 0;
        this.e = null;
    }

    @DexIgnore
    public static <T> go3<T> a(int i, Comparator<? super T> comparator) {
        return new go3<>(comparator, i);
    }

    @DexIgnore
    public final void b() {
        int i = (this.a * 2) - 1;
        int a2 = vo3.a(i + 0, RoundingMode.CEILING) * 3;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i2 < i) {
                int a3 = a(i2, i, ((i2 + i) + 1) >>> 1);
                int i5 = this.a;
                if (a3 <= i5) {
                    if (a3 >= i5) {
                        break;
                    }
                    i2 = Math.max(a3, i2 + 1);
                    i4 = a3;
                } else {
                    i = a3 - 1;
                }
                i3++;
                if (i3 >= a2) {
                    Arrays.sort(this.c, i2, i, this.b);
                    break;
                }
            } else {
                break;
            }
        }
        this.d = this.a;
        this.e = this.c[i4];
        while (true) {
            i4++;
            if (i4 >= this.a) {
                return;
            }
            if (this.b.compare(this.c[i4], this.e) > 0) {
                this.e = this.c[i4];
            }
        }
    }

    @DexIgnore
    public void a(T t) {
        int i = this.a;
        if (i != 0) {
            int i2 = this.d;
            if (i2 == 0) {
                this.c[0] = t;
                this.e = t;
                this.d = 1;
            } else if (i2 < i) {
                T[] tArr = this.c;
                this.d = i2 + 1;
                tArr[i2] = t;
                if (this.b.compare(t, this.e) > 0) {
                    this.e = t;
                }
            } else if (this.b.compare(t, this.e) < 0) {
                T[] tArr2 = this.c;
                int i3 = this.d;
                this.d = i3 + 1;
                tArr2[i3] = t;
                if (this.d == this.a * 2) {
                    b();
                }
            }
        }
    }

    @DexIgnore
    public final int a(int i, int i2, int i3) {
        T[] tArr = this.c;
        T t = tArr[i3];
        tArr[i3] = tArr[i2];
        int i4 = i;
        while (i < i2) {
            if (this.b.compare(this.c[i], t) < 0) {
                a(i4, i);
                i4++;
            }
            i++;
        }
        T[] tArr2 = this.c;
        tArr2[i2] = tArr2[i4];
        tArr2[i4] = t;
        return i4;
    }

    @DexIgnore
    public final void a(int i, int i2) {
        T[] tArr = this.c;
        T t = tArr[i];
        tArr[i] = tArr[i2];
        tArr[i2] = t;
    }

    @DexIgnore
    public void a(Iterator<? extends T> it) {
        while (it.hasNext()) {
            a(it.next());
        }
    }

    @DexIgnore
    public List<T> a() {
        Arrays.sort(this.c, 0, this.d, this.b);
        int i = this.d;
        int i2 = this.a;
        if (i > i2) {
            T[] tArr = this.c;
            Arrays.fill(tArr, i2, tArr.length, (Object) null);
            int i3 = this.a;
            this.d = i3;
            this.e = this.c[i3 - 1];
        }
        return Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(this.c, this.d)));
    }
}
