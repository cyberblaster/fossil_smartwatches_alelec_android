package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qn0 {
    HOUR((byte) 1),
    MINUTE((byte) 2);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public qn0(byte b) {
        this.a = b;
    }
}
