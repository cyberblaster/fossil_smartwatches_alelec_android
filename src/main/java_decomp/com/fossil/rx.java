package com.fossil;

import android.util.Log;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rx implements as<qx> {
    @DexIgnore
    public rr a(xr xrVar) {
        return rr.SOURCE;
    }

    @DexIgnore
    public boolean a(rt<qx> rtVar, File file, xr xrVar) {
        try {
            h00.a(rtVar.get().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
