package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.w52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g62 implements w52.a {
    @DexIgnore
    public /* final */ /* synthetic */ FrameLayout a;
    @DexIgnore
    public /* final */ /* synthetic */ LayoutInflater b;
    @DexIgnore
    public /* final */ /* synthetic */ ViewGroup c;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle d;
    @DexIgnore
    public /* final */ /* synthetic */ w52 e;

    @DexIgnore
    public g62(w52 w52, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.e = w52;
        this.a = frameLayout;
        this.b = layoutInflater;
        this.c = viewGroup;
        this.d = bundle;
    }

    @DexIgnore
    public final void a(y52 y52) {
        this.a.removeAllViews();
        this.a.addView(this.e.a.a(this.b, this.c, this.d));
    }

    @DexIgnore
    public final int getState() {
        return 2;
    }
}
