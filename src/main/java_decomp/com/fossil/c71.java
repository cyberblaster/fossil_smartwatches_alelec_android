package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c71 implements Parcelable.Creator<z81> {
    @DexIgnore
    public /* synthetic */ c71(qg6 qg6) {
    }

    @DexIgnore
    public z81 createFromParcel(Parcel parcel) {
        return new z81(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new z81[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m6createFromParcel(Parcel parcel) {
        return new z81(parcel, (qg6) null);
    }
}
