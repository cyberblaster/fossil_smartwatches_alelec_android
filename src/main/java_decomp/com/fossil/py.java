package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class py extends Fragment {
    @DexIgnore
    public /* final */ fy a;
    @DexIgnore
    public /* final */ ry b;
    @DexIgnore
    public /* final */ Set<py> c;
    @DexIgnore
    public fr d;
    @DexIgnore
    public py e;
    @DexIgnore
    public Fragment f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ry {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Set<fr> a() {
            Set<py> a2 = py.this.a();
            HashSet hashSet = new HashSet(a2.size());
            for (py next : a2) {
                if (next.d() != null) {
                    hashSet.add(next.d());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + py.this + "}";
        }
    }

    @DexIgnore
    public py() {
        this(new fy());
    }

    @DexIgnore
    public void a(fr frVar) {
        this.d = frVar;
    }

    @DexIgnore
    public fy b() {
        return this.a;
    }

    @DexIgnore
    @TargetApi(17)
    public final Fragment c() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.f;
    }

    @DexIgnore
    public fr d() {
        return this.d;
    }

    @DexIgnore
    public ry e() {
        return this.b;
    }

    @DexIgnore
    public final void f() {
        py pyVar = this.e;
        if (pyVar != null) {
            pyVar.b(this);
            this.e = null;
        }
    }

    @DexIgnore
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            a(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.a.a();
        f();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        f();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.a.b();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.a.c();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{parent=" + c() + "}";
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public py(fy fyVar) {
        this.b = new a();
        this.c = new HashSet();
        this.a = fyVar;
    }

    @DexIgnore
    public final void a(py pyVar) {
        this.c.add(pyVar);
    }

    @DexIgnore
    public final void b(py pyVar) {
        this.c.remove(pyVar);
    }

    @DexIgnore
    @TargetApi(17)
    public Set<py> a() {
        if (equals(this.e)) {
            return Collections.unmodifiableSet(this.c);
        }
        if (this.e == null || Build.VERSION.SDK_INT < 17) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet();
        for (py next : this.e.a()) {
            if (a(next.getParentFragment())) {
                hashSet.add(next);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public void b(Fragment fragment) {
        this.f = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            a(fragment.getActivity());
        }
    }

    @DexIgnore
    @TargetApi(17)
    public final boolean a(Fragment fragment) {
        Fragment parentFragment = getParentFragment();
        while (true) {
            Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        f();
        this.e = wq.a((Context) activity).h().b(activity);
        if (!equals(this.e)) {
            this.e.a(this);
        }
    }
}
