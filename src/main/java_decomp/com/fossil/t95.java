package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface t95 extends k24<s95> {
    @DexIgnore
    void C(String str);

    @DexIgnore
    void I(String str);

    @DexIgnore
    void a(int i, int i2, String str, String str2);

    @DexIgnore
    void a(String str, String str2, String str3);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, String str3);

    @DexIgnore
    void b(MicroApp microApp);

    @DexIgnore
    void c(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void f(String str);

    @DexIgnore
    void g(String str);

    @DexIgnore
    void w(List<MicroApp> list);
}
