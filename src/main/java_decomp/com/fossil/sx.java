package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sx extends ix<qx> implements nt {
    @DexIgnore
    public sx(qx qxVar) {
        super(qxVar);
    }

    @DexIgnore
    public void a() {
        ((qx) this.a).stop();
        ((qx) this.a).k();
    }

    @DexIgnore
    public int b() {
        return ((qx) this.a).i();
    }

    @DexIgnore
    public Class<qx> c() {
        return qx.class;
    }

    @DexIgnore
    public void d() {
        ((qx) this.a).e().prepareToDraw();
    }
}
