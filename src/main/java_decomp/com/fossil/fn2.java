package com.fossil;

import com.fossil.fn2;
import com.fossil.fn2.b;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fn2<MessageType extends fn2<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends ql2<MessageType, BuilderType> {
    @DexIgnore
    public static Map<Object, fn2<?, ?>> zzd; // = new ConcurrentHashMap();
    @DexIgnore
    public zp2 zzb; // = zp2.d();
    @DexIgnore
    public int zzc; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T extends fn2<T, ?>> extends tl2<T> {
        @DexIgnore
        public a(T t) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements zm2<c> {
        @DexIgnore
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        public final nq2 zzb() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        public final qq2 zzc() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        public final boolean zzd() {
            throw new NoSuchMethodError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<MessageType extends d<MessageType, BuilderType>, BuilderType> extends fn2<MessageType, BuilderType> implements to2 {
        @DexIgnore
        public xm2<c> zzc; // = xm2.g();

        @DexIgnore
        public final xm2<c> n() {
            if (this.zzc.b()) {
                this.zzc = (xm2) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* 'enum' modifier removed */
    public static final class e {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ int e; // = 5;
        @DexIgnore
        public static /* final */ int f; // = 6;
        @DexIgnore
        public static /* final */ int g; // = 7;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] h; // = {a, b, c, d, e, f, g};
        @DexIgnore
        public static /* final */ int i; // = 1;
        @DexIgnore
        public static /* final */ int j; // = 2;
        @DexIgnore
        public static /* final */ int k; // = 1;
        @DexIgnore
        public static /* final */ int l; // = 2;

        /*
        static {
            int[] iArr = {i, j};
            int[] iArr2 = {k, l};
        }
        */

        @DexIgnore
        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<ContainingType extends ro2, Type> extends tm2<ContainingType, Type> {
    }

    @DexIgnore
    public static ln2 k() {
        return in2.b();
    }

    @DexIgnore
    public static on2 l() {
        return fo2.b();
    }

    @DexIgnore
    public static <E> nn2<E> m() {
        return ep2.b();
    }

    @DexIgnore
    public abstract Object a(int i, Object obj, Object obj2);

    @DexIgnore
    public final void a(int i) {
        this.zzc = i;
    }

    @DexIgnore
    public final /* synthetic */ qo2 b() {
        b bVar = (b) a(e.e, (Object) null, (Object) null);
        bVar.a(this);
        return bVar;
    }

    @DexIgnore
    public final /* synthetic */ qo2 c() {
        return (b) a(e.e, (Object) null, (Object) null);
    }

    @DexIgnore
    public final int e() {
        if (this.zzc == -1) {
            this.zzc = ap2.a().a(this).zzb(this);
        }
        return this.zzc;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((fn2) a(e.f, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return ap2.a().a(this).zza(this, (fn2) obj);
    }

    @DexIgnore
    public final int g() {
        return this.zzc;
    }

    @DexIgnore
    public final <MessageType extends fn2<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> BuilderType h() {
        return (b) a(e.e, (Object) null, (Object) null);
    }

    @DexIgnore
    public int hashCode() {
        int i = this.zza;
        if (i != 0) {
            return i;
        }
        this.zza = ap2.a().a(this).zza(this);
        return this.zza;
    }

    @DexIgnore
    public final boolean i() {
        return a(this, Boolean.TRUE.booleanValue());
    }

    @DexIgnore
    public final BuilderType j() {
        BuilderType buildertype = (b) a(e.e, (Object) null, (Object) null);
        buildertype.a(this);
        return buildertype;
    }

    @DexIgnore
    public String toString() {
        return so2.a(this, super.toString());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<MessageType extends fn2<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends rl2<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType a;
        @DexIgnore
        public MessageType b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public b(MessageType messagetype) {
            this.a = messagetype;
            this.b = (fn2) messagetype.a(e.d, (Object) null, (Object) null);
        }

        @DexIgnore
        public final BuilderType a(MessageType messagetype) {
            f();
            a(this.b, messagetype);
            return this;
        }

        @DexIgnore
        public final BuilderType b(byte[] bArr, int i, int i2, sm2 sm2) throws qn2 {
            f();
            try {
                ap2.a().a(this.b).a(this.b, bArr, 0, i2 + 0, new xl2(sm2));
                return this;
            } catch (qn2 e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw qn2.zza();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        @DexIgnore
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            b bVar = (b) this.a.a(e.e, (Object) null, (Object) null);
            bVar.a((fn2) u());
            return bVar;
        }

        @DexIgnore
        public final void f() {
            if (this.c) {
                MessageType messagetype = (fn2) this.b.a(e.d, (Object) null, (Object) null);
                a(messagetype, this.b);
                this.b = messagetype;
                this.c = false;
            }
        }

        @DexIgnore
        /* renamed from: g */
        public MessageType u() {
            if (this.c) {
                return this.b;
            }
            MessageType messagetype = this.b;
            ap2.a().a(messagetype).zzc(messagetype);
            this.c = true;
            return this.b;
        }

        @DexIgnore
        /* renamed from: h */
        public final MessageType i() {
            MessageType messagetype = (fn2) u();
            if (messagetype.i()) {
                return messagetype;
            }
            throw new xp2(messagetype);
        }

        @DexIgnore
        public static void a(MessageType messagetype, MessageType messagetype2) {
            ap2.a().a(messagetype).zzb(messagetype, messagetype2);
        }

        @DexIgnore
        public final /* synthetic */ rl2 a(ql2 ql2) {
            a((fn2) ql2);
            return this;
        }

        @DexIgnore
        public final /* synthetic */ rl2 a(byte[] bArr, int i, int i2, sm2 sm2) throws qn2 {
            b(bArr, 0, i2, sm2);
            return this;
        }

        @DexIgnore
        public final /* synthetic */ rl2 a(byte[] bArr, int i, int i2) throws qn2 {
            b(bArr, 0, i2, sm2.a());
            return this;
        }

        @DexIgnore
        public final /* synthetic */ ro2 a() {
            return this.a;
        }
    }

    @DexIgnore
    public final void a(pm2 pm2) throws IOException {
        ap2.a().a(this).a(this, rm2.a(pm2));
    }

    @DexIgnore
    public static <T extends fn2<?, ?>> T a(Class<T> cls) {
        T t = (fn2) zzd.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (fn2) zzd.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t == null) {
            t = (fn2) ((fn2) cq2.a(cls)).a(e.f, (Object) null, (Object) null);
            if (t != null) {
                zzd.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    @DexIgnore
    public static <T extends fn2<?, ?>> void a(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    @DexIgnore
    public static Object a(ro2 ro2, String str, Object[] objArr) {
        return new dp2(ro2, str, objArr);
    }

    @DexIgnore
    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static final <T extends fn2<T, ?>> boolean a(T t, boolean z) {
        byte byteValue = ((Byte) t.a(e.a, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzd2 = ap2.a().a(t).zzd(t);
        if (z) {
            t.a(e.b, (Object) zzd2 ? t : null, (Object) null);
        }
        return zzd2;
    }

    @DexIgnore
    public static on2 a(on2 on2) {
        int size = on2.size();
        return on2.zzc(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public static <E> nn2<E> a(nn2<E> nn2) {
        int size = nn2.size();
        return nn2.zza(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public final /* synthetic */ ro2 a() {
        return (fn2) a(e.f, (Object) null, (Object) null);
    }
}
