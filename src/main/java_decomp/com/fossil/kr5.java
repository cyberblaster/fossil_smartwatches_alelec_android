package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kr5 implements Factory<ir5> {
    @DexIgnore
    public static ir5 a(jr5 jr5) {
        ir5 a = jr5.a();
        z76.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
