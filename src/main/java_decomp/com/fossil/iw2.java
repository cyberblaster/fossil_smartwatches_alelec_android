package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iw2 implements Parcelable.Creator<LocationRequest> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        long j4 = 0;
        int i = 102;
        boolean z = false;
        int i2 = Integer.MAX_VALUE;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel2, a);
                    break;
                case 2:
                    j = f22.s(parcel2, a);
                    break;
                case 3:
                    j2 = f22.s(parcel2, a);
                    break;
                case 4:
                    z = f22.i(parcel2, a);
                    break;
                case 5:
                    j3 = f22.s(parcel2, a);
                    break;
                case 6:
                    i2 = f22.q(parcel2, a);
                    break;
                case 7:
                    f = f22.n(parcel2, a);
                    break;
                case 8:
                    j4 = f22.s(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new LocationRequest(i, j, j2, z, j3, i2, f, j4);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationRequest[i];
    }
}
