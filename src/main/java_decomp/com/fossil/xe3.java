package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe3 implements Parcelable.Creator<we3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        byte b2 = 0;
        String str = null;
        byte b3 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                b2 = f22.k(parcel, a);
            } else if (a2 == 3) {
                b3 = f22.k(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                str = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new we3(b2, b3, str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new we3[i];
    }
}
