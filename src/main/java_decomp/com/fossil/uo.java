package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uo implements to {
    @DexIgnore
    public /* final */ lo a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Executor c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            uo.this.b(runnable);
        }
    }

    @DexIgnore
    public uo(Executor executor) {
        this.a = new lo(executor);
    }

    @DexIgnore
    public Executor a() {
        return this.c;
    }

    @DexIgnore
    public void b(Runnable runnable) {
        this.b.post(runnable);
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.execute(runnable);
    }

    @DexIgnore
    public lo b() {
        return this.a;
    }
}
