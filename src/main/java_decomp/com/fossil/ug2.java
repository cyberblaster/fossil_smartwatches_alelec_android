package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug2 implements Parcelable.Creator<tg2> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        rg2 rg2 = null;
        IBinder iBinder = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel, a);
                    break;
                case 2:
                    rg2 = f22.a(parcel, a, rg2.CREATOR);
                    break;
                case 3:
                    iBinder = f22.p(parcel, a);
                    break;
                case 4:
                    pendingIntent = f22.a(parcel, a, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = f22.p(parcel, a);
                    break;
                case 6:
                    iBinder3 = f22.p(parcel, a);
                    break;
                default:
                    f22.v(parcel, a);
                    break;
            }
        }
        f22.h(parcel, b);
        return new tg2(i, rg2, iBinder, pendingIntent, iBinder2, iBinder3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new tg2[i];
    }
}
