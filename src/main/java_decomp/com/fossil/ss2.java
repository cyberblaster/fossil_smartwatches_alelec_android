package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss2 implements ts2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.client.sessions.check_on_startup", true);
        b = vk2.a("measurement.client.sessions.start_session_before_view_screen", true);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return b.b().booleanValue();
    }
}
