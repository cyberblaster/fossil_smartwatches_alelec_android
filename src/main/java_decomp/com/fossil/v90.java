package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v90 extends z90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ vd0 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<v90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new v90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new v90[i];
        }
    }

    @DexIgnore
    public v90(byte b, vd0 vd0) {
        super(e90.COMMUTE_TIME_TRAVEL_MICRO_APP, b, vd0);
        this.e = vd0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(v90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.e, ((v90) obj).e) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest");
    }

    @DexIgnore
    public final vd0 getCommuteTimeTravelMicroAppEvent() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return this.e.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public /* synthetic */ v90(Parcel parcel, qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(vd0.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (vd0) readParcelable;
        } else {
            wg6.a();
            throw null;
        }
    }
}
