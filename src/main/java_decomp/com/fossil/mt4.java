package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt4 extends m24<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public /* final */ in4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mt4.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<wq4> a;

        @DexIgnore
        public b(WeakReference<wq4> weakReference) {
            wg6.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<wq4> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, gv1 gv1) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            wg6.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = mt4.class.getSimpleName();
        wg6.a((Object) simpleName, "LoginGoogleUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public mt4(in4 in4) {
        wg6.b(in4, "mLoginGoogleManager");
        this.d = in4;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ln4 {
        @DexIgnore
        public /* final */ /* synthetic */ mt4 a;

        @DexIgnore
        public e(mt4 mt4) {
            this.a = mt4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.fossil.mt4, com.portfolio.platform.CoroutineUseCase] */
        public void a(SignUpSocialAuth signUpSocialAuth) {
            wg6.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = mt4.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v3, types: [com.fossil.mt4, com.portfolio.platform.CoroutineUseCase] */
        public void a(int i, gv1 gv1, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = mt4.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + gv1);
            this.a.a(new c(i, gv1));
        }
    }

    @DexIgnore
    public Object a(b bVar, xe6<Object> xe6) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.get.instance().y()) {
                return new c(601, (gv1) null);
            }
            in4 in4 = this.d;
            WeakReference<wq4> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                in4.a(a2, new e(this));
                return cd6.a;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, (gv1) null);
        }
    }
}
