package com.fossil;

import com.fossil.fn2;
import com.fossil.qj2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pj2 extends fn2<pj2, a> implements to2 {
    @DexIgnore
    public static /* final */ pj2 zzd;
    @DexIgnore
    public static volatile yo2<pj2> zze;
    @DexIgnore
    public nn2<qj2> zzc; // = fn2.m();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<pj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(pj2.zzd);
        }

        @DexIgnore
        public final qj2 a(int i) {
            return ((pj2) this.b).b(0);
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(qj2.a aVar) {
            f();
            ((pj2) this.b).a(aVar);
            return this;
        }
    }

    /*
    static {
        pj2 pj2 = new pj2();
        zzd = pj2;
        fn2.a(pj2.class, pj2);
    }
    */

    @DexIgnore
    public static a o() {
        return (a) zzd.h();
    }

    @DexIgnore
    public final void a(qj2.a aVar) {
        if (!this.zzc.zza()) {
            this.zzc = fn2.a(this.zzc);
        }
        this.zzc.add((qj2) aVar.i());
    }

    @DexIgnore
    public final qj2 b(int i) {
        return this.zzc.get(0);
    }

    @DexIgnore
    public final List<qj2> n() {
        return this.zzc;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new pj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", qj2.class});
            case 4:
                return zzd;
            case 5:
                yo2<pj2> yo2 = zze;
                if (yo2 == null) {
                    synchronized (pj2.class) {
                        yo2 = zze;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzd);
                            zze = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
