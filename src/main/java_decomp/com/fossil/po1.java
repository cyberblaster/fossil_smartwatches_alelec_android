package com.fossil;

import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po1 extends yw3<po1, b> implements qo1 {
    @DexIgnore
    public static /* final */ po1 g; // = new po1();
    @DexIgnore
    public static volatile gx3<po1> h;
    @DexIgnore
    public int d;
    @DexIgnore
    public zw3.c<no1> e; // = yw3.i();
    @DexIgnore
    public long f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<po1, b> implements qo1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b() {
            super(po1.g);
        }
    }

    /*
    static {
        g.g();
    }
    */

    @DexIgnore
    public static po1 k() {
        return g;
    }

    @DexIgnore
    public static gx3<po1> l() {
        return g.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new po1();
            case 2:
                return g;
            case 3:
                this.e.l();
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                po1 po1 = (po1) obj2;
                this.e = kVar.a(this.e, po1.e);
                this.f = kVar.a(this.f != 0, this.f, po1.f != 0, po1.f);
                if (kVar == yw3.i.a) {
                    this.d |= po1.d;
                }
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 10) {
                                if (!this.e.m()) {
                                    this.e = yw3.a(this.e);
                                }
                                this.e.add((no1) tw3.a(no1.k(), ww3));
                            } else if (l == 16) {
                                this.f = tw3.e();
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (h == null) {
                    synchronized (po1.class) {
                        if (h == null) {
                            h = new yw3.c(g);
                        }
                    }
                }
                return h;
            default:
                throw new UnsupportedOperationException();
        }
        return g;
    }

    @DexIgnore
    public int d() {
        int i = this.c;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.e.size(); i3++) {
            i2 += uw3.b(1, (dx3) this.e.get(i3));
        }
        long j = this.f;
        if (j != 0) {
            i2 += uw3.d(2, j);
        }
        this.c = i2;
        return i2;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        for (int i = 0; i < this.e.size(); i++) {
            uw3.a(1, (dx3) this.e.get(i));
        }
        long j = this.f;
        if (j != 0) {
            uw3.a(2, j);
        }
    }
}
