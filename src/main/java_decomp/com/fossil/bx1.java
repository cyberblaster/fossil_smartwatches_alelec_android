package com.fossil;

import android.os.RemoteException;
import com.fossil.rv1;
import com.fossil.rv1.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bx1<A extends rv1.b, ResultT> {
    @DexIgnore
    public /* final */ iv1[] a; // = null;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public abstract void a(A a2, rc3<ResultT> rc3) throws RemoteException;

    @DexIgnore
    public boolean a() {
        return this.b;
    }

    @DexIgnore
    public final iv1[] b() {
        return this.a;
    }
}
