package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cf;
import com.fossil.uf;
import com.fossil.zf;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ue<T> {
    @DexIgnore
    public /* final */ jg a;
    @DexIgnore
    public /* final */ uf<T> b;
    @DexIgnore
    public Executor c; // = q3.d();
    @DexIgnore
    public /* final */ List<e<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public cf<T> f;
    @DexIgnore
    public cf<T> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ cf.k i; // = new a();
    @DexIgnore
    public cf.j j; // = new b();
    @DexIgnore
    public /* final */ List<cf.j> k; // = new CopyOnWriteArrayList();
    @DexIgnore
    public cf.g l; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends cf.k {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(cf.l lVar, cf.i iVar, Throwable th) {
            for (cf.j a : ue.this.k) {
                a.a(lVar, iVar, th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements cf.j {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(cf.l lVar, cf.i iVar, Throwable th) {
            ue.this.i.a(lVar, iVar, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends cf.g {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            ue.this.a.a(i, i2, (Object) null);
        }

        @DexIgnore
        public void b(int i, int i2) {
            ue.this.a.b(i, i2);
        }

        @DexIgnore
        public void c(int i, int i2) {
            ue.this.a.c(i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cf a;
        @DexIgnore
        public /* final */ /* synthetic */ cf b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ cf d;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ zf.c a;

            @DexIgnore
            public a(zf.c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void run() {
                d dVar = d.this;
                ue ueVar = ue.this;
                if (ueVar.h == dVar.c) {
                    ueVar.a(dVar.d, dVar.b, this.a, dVar.a.f, dVar.e);
                }
            }
        }

        @DexIgnore
        public d(cf cfVar, cf cfVar2, int i, cf cfVar3, Runnable runnable) {
            this.a = cfVar;
            this.b = cfVar2;
            this.c = i;
            this.d = cfVar3;
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            ue.this.c.execute(new a(ff.a(this.a.e, this.b.e, ue.this.b.b())));
        }
    }

    @DexIgnore
    public interface e<T> {
        @DexIgnore
        void a(cf<T> cfVar, cf<T> cfVar2);
    }

    @DexIgnore
    public ue(RecyclerView.g gVar, zf.d<T> dVar) {
        this.a = new tf(gVar);
        this.b = new uf.a(dVar).a();
    }

    @DexIgnore
    public T a(int i2) {
        cf<T> cfVar = this.f;
        if (cfVar == null) {
            cf<T> cfVar2 = this.g;
            if (cfVar2 != null) {
                return cfVar2.get(i2);
            }
            throw new IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        cfVar.d(i2);
        return this.f.get(i2);
    }

    @DexIgnore
    public int a() {
        cf<T> cfVar = this.f;
        if (cfVar != null) {
            return cfVar.size();
        }
        cf<T> cfVar2 = this.g;
        if (cfVar2 == null) {
            return 0;
        }
        return cfVar2.size();
    }

    @DexIgnore
    public void a(cf<T> cfVar) {
        a(cfVar, (Runnable) null);
    }

    @DexIgnore
    public void a(cf<T> cfVar, Runnable runnable) {
        if (cfVar != null) {
            if (this.f == null && this.g == null) {
                this.e = cfVar.g();
            } else if (cfVar.g() != this.e) {
                throw new IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i2 = this.h + 1;
        this.h = i2;
        cf<T> cfVar2 = this.f;
        if (cfVar != cfVar2) {
            cf<T> cfVar3 = this.g;
            if (cfVar3 != null) {
                cfVar2 = cfVar3;
            }
            if (cfVar == null) {
                int a2 = a();
                cf<T> cfVar4 = this.f;
                if (cfVar4 != null) {
                    cfVar4.a(this.l);
                    this.f.b(this.j);
                    this.f = null;
                } else if (this.g != null) {
                    this.g = null;
                }
                this.a.c(0, a2);
                a(cfVar2, (cf<T>) null, runnable);
            } else if (this.f == null && this.g == null) {
                this.f = cfVar;
                this.f.a(this.j);
                cfVar.a((List<T>) null, this.l);
                this.a.b(0, cfVar.size());
                a((cf) null, cfVar, runnable);
            } else {
                cf<T> cfVar5 = this.f;
                if (cfVar5 != null) {
                    cfVar5.a(this.l);
                    this.f.b(this.j);
                    this.g = (cf) this.f.j();
                    this.f = null;
                }
                cf<T> cfVar6 = this.g;
                if (cfVar6 == null || this.f != null) {
                    throw new IllegalStateException("must be in snapshot state to diff");
                }
                this.b.a().execute(new d(cfVar6, (cf) cfVar.j(), i2, cfVar, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(cf<T> cfVar, cf<T> cfVar2, zf.c cVar, int i2, Runnable runnable) {
        cf<T> cfVar3 = this.g;
        if (cfVar3 == null || this.f != null) {
            throw new IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f = cfVar;
        this.f.a(this.j);
        this.g = null;
        ff.a(this.a, cfVar3.e, cfVar.e, cVar);
        cfVar.a((List<T>) cfVar2, this.l);
        if (!this.f.isEmpty()) {
            int a2 = ff.a(cVar, (ef) cfVar3.e, (ef) cfVar2.e, i2);
            cf<T> cfVar4 = this.f;
            cfVar4.d(Math.max(0, Math.min(cfVar4.size() - 1, a2)));
        }
        a(cfVar3, this.f, runnable);
    }

    @DexIgnore
    public final void a(cf<T> cfVar, cf<T> cfVar2, Runnable runnable) {
        for (e<T> a2 : this.d) {
            a2.a(cfVar, cfVar2);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(e<T> eVar) {
        this.d.add(eVar);
    }

    @DexIgnore
    public void a(cf.j jVar) {
        cf<T> cfVar = this.f;
        if (cfVar != null) {
            cfVar.a(jVar);
        } else {
            jVar.a(cf.l.REFRESH, this.i.c(), this.i.d());
            jVar.a(cf.l.START, this.i.e(), this.i.f());
            jVar.a(cf.l.END, this.i.a(), this.i.b());
        }
        this.k.add(jVar);
    }
}
