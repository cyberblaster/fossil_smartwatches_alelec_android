package com.fossil;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.util.StateSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ui3 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public static /* final */ int[] b; // = {16842919};
    @DexIgnore
    public static /* final */ int[] c; // = {16843623, 16842908};
    @DexIgnore
    public static /* final */ int[] d; // = {16842908};
    @DexIgnore
    public static /* final */ int[] e; // = {16843623};
    @DexIgnore
    public static /* final */ int[] f; // = {16842913, 16842919};
    @DexIgnore
    public static /* final */ int[] g; // = {16842913, 16843623, 16842908};
    @DexIgnore
    public static /* final */ int[] h; // = {16842913, 16842908};
    @DexIgnore
    public static /* final */ int[] i; // = {16842913, 16843623};
    @DexIgnore
    public static /* final */ int[] j; // = {16842913};
    @DexIgnore
    public static /* final */ int[] k; // = {16842910, 16842919};
    @DexIgnore
    public static /* final */ String l; // = ui3.class.getSimpleName();

    @DexIgnore
    public static ColorStateList a(ColorStateList colorStateList) {
        if (a) {
            return new ColorStateList(new int[][]{j, StateSet.NOTHING}, new int[]{a(colorStateList, f), a(colorStateList, b)});
        }
        int[] iArr = f;
        int[] iArr2 = g;
        int[] iArr3 = h;
        int[] iArr4 = i;
        int[] iArr5 = b;
        int[] iArr6 = c;
        int[] iArr7 = d;
        int[] iArr8 = e;
        return new ColorStateList(new int[][]{iArr, iArr2, iArr3, iArr4, j, iArr5, iArr6, iArr7, iArr8, StateSet.NOTHING}, new int[]{a(colorStateList, iArr), a(colorStateList, iArr2), a(colorStateList, iArr3), a(colorStateList, iArr4), 0, a(colorStateList, iArr5), a(colorStateList, iArr6), a(colorStateList, iArr7), a(colorStateList, iArr8), 0});
    }

    @DexIgnore
    public static ColorStateList b(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return ColorStateList.valueOf(0);
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 22 && i2 <= 27 && Color.alpha(colorStateList.getDefaultColor()) == 0 && Color.alpha(colorStateList.getColorForState(k, 0)) != 0) {
            Log.w(l, "Use a non-transparent color for the default color as it will be used to finish ripple animations.");
        }
        return colorStateList;
    }

    @DexIgnore
    public static boolean a(int[] iArr) {
        boolean z = false;
        boolean z2 = false;
        for (int i2 : iArr) {
            if (i2 == 16842910) {
                z = true;
            } else if (i2 == 16842908 || i2 == 16842919 || i2 == 16843623) {
                z2 = true;
            }
        }
        return z && z2;
    }

    @DexIgnore
    public static int a(ColorStateList colorStateList, int[] iArr) {
        int colorForState = colorStateList != null ? colorStateList.getColorForState(iArr, colorStateList.getDefaultColor()) : 0;
        return a ? a(colorForState) : colorForState;
    }

    @DexIgnore
    @TargetApi(21)
    public static int a(int i2) {
        return f7.c(i2, Math.min(Color.alpha(i2) * 2, 255));
    }
}
