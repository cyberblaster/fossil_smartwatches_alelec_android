package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c96 {
    @DexIgnore
    public static c96 f;
    @DexIgnore
    public static Object g; // = new Object();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public volatile boolean b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public /* final */ g96 d;
    @DexIgnore
    public boolean e; // = false;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002f, code lost:
        r1 = r1.getApplicationInfo(r7.getPackageName(), com.j256.ormlite.logger.Logger.DEFAULT_FULL_MESSAGE_LENGTH);
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0066  */
    public c96(Context context) {
        boolean z;
        boolean z2;
        ApplicationInfo applicationInfo;
        boolean z3 = false;
        if (context != null) {
            this.a = context.getSharedPreferences("com.google.firebase.crashlytics.prefs", 0);
            this.d = h96.a(context);
            if (this.a.contains("firebase_crashlytics_collection_enabled")) {
                z2 = this.a.getBoolean("firebase_crashlytics_collection_enabled", true);
            } else {
                try {
                    PackageManager packageManager = context.getPackageManager();
                    z2 = (packageManager == null || applicationInfo == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_crashlytics_collection_enabled")) ? z2 : applicationInfo.metaData.getBoolean("firebase_crashlytics_collection_enabled");
                } catch (PackageManager.NameNotFoundException e2) {
                    c86.g().b("Fabric", "Unable to get PackageManager. Falling through", e2);
                }
                z2 = true;
                z = false;
                this.c = z2;
                this.b = z;
                this.e = z86.o(context) != null ? true : z3;
                return;
            }
            z = true;
            this.c = z2;
            this.b = z;
            this.e = z86.o(context) != null ? true : z3;
            return;
        }
        throw new RuntimeException("null context");
    }

    @DexIgnore
    public static c96 a(Context context) {
        c96 c96;
        synchronized (g) {
            if (f == null) {
                f = new c96(context);
            }
            c96 = f;
        }
        return c96;
    }

    @DexIgnore
    public boolean b() {
        return this.c;
    }

    @DexIgnore
    public boolean a() {
        if (this.e && this.b) {
            return this.c;
        }
        g96 g96 = this.d;
        if (g96 != null) {
            return g96.a();
        }
        return true;
    }
}
