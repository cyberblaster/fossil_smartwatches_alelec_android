package com.fossil;

import android.graphics.Bitmap;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sz extends vz<Bitmap> {
    @DexIgnore
    public sz(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: a */
    public void c(Bitmap bitmap) {
        ((ImageView) this.a).setImageBitmap(bitmap);
    }
}
