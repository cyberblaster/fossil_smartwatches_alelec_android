package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs0 implements Parcelable.Creator<nu0> {
    @DexIgnore
    public /* synthetic */ vs0(qg6 qg6) {
    }

    @DexIgnore
    public nu0 createFromParcel(Parcel parcel) {
        return new nu0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new nu0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m64createFromParcel(Parcel parcel) {
        return new nu0(parcel, (qg6) null);
    }
}
