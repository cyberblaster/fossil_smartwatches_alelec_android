package com.fossil;

import android.content.Context;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface x1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(q1 q1Var, boolean z);

        @DexIgnore
        boolean a(q1 q1Var);
    }

    @DexIgnore
    void a(Context context, q1 q1Var);

    @DexIgnore
    void a(Parcelable parcelable);

    @DexIgnore
    void a(q1 q1Var, boolean z);

    @DexIgnore
    void a(a aVar);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    boolean a(c2 c2Var);

    @DexIgnore
    boolean a(q1 q1Var, t1 t1Var);

    @DexIgnore
    Parcelable b();

    @DexIgnore
    boolean b(q1 q1Var, t1 t1Var);

    @DexIgnore
    int getId();
}
