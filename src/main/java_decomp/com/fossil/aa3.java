package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class aa3 extends ca3 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public aa3(ea3 ea3) {
        super(ea3);
        this.b.a(this);
    }

    @DexIgnore
    public final boolean q() {
        return this.c;
    }

    @DexIgnore
    public final void r() {
        if (!q()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void s() {
        if (!this.c) {
            t();
            this.b.u();
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean t();
}
