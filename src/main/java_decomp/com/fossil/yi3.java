package com.fossil;

import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yi3 implements zi3 {
    @DexIgnore
    public /* final */ zi3 a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public yi3(float f, zi3 zi3) {
        while (zi3 instanceof yi3) {
            zi3 = ((yi3) zi3).a;
            f += ((yi3) zi3).b;
        }
        this.a = zi3;
        this.b = f;
    }

    @DexIgnore
    public float a(RectF rectF) {
        return Math.max(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.a.a(rectF) + this.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof yi3)) {
            return false;
        }
        yi3 yi3 = (yi3) obj;
        if (!this.a.equals(yi3.a) || this.b != yi3.b) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, Float.valueOf(this.b)});
    }
}
