package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vz1 extends LifecycleCallback implements DialogInterface.OnCancelListener {
    @DexIgnore
    public volatile boolean b;
    @DexIgnore
    public /* final */ AtomicReference<xz1> c;
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ jv1 e;

    @DexIgnore
    public vz1(tw1 tw1) {
        this(tw1, jv1.a());
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null) {
            this.c.set(bundle.getBoolean("resolving_error", false) ? new xz1(new gv1(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    @DexIgnore
    public abstract void a(gv1 gv1, int i);

    @DexIgnore
    public void b(Bundle bundle) {
        super.b(bundle);
        xz1 xz1 = this.c.get();
        if (xz1 != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", xz1.b());
            bundle.putInt("failed_status", xz1.a().p());
            bundle.putParcelable("failed_resolution", xz1.a().C());
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        this.b = true;
    }

    @DexIgnore
    public void e() {
        super.e();
        this.b = false;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public final void g() {
        this.c.set((Object) null);
        f();
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        a(new gv1(13, (PendingIntent) null), a(this.c.get()));
        g();
    }

    @DexIgnore
    public vz1(tw1 tw1, jv1 jv1) {
        super(tw1);
        this.c = new AtomicReference<>((Object) null);
        this.d = new bb2(Looper.getMainLooper());
        this.e = jv1;
    }

    @DexIgnore
    public final void b(gv1 gv1, int i) {
        xz1 xz1 = new xz1(gv1, i);
        if (this.c.compareAndSet((Object) null, xz1)) {
            this.d.post(new wz1(this, xz1));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0064  */
    public void a(int i, int i2, Intent intent) {
        xz1 xz1 = this.c.get();
        boolean z = true;
        if (i == 1) {
            if (i2 != -1) {
                if (i2 == 0) {
                    int i3 = 13;
                    if (intent != null) {
                        i3 = intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
                    }
                    xz1 xz12 = new xz1(new gv1(i3, (PendingIntent) null, xz1.a().toString()), a(xz1));
                    this.c.set(xz12);
                    xz1 = xz12;
                }
            }
            if (z) {
            }
        } else if (i == 2) {
            int c2 = this.e.c((Context) a());
            if (c2 != 0) {
                z = false;
            }
            if (xz1 != null) {
                if (xz1.a().p() == 18 && c2 == 18) {
                    return;
                }
                if (z) {
                    g();
                    return;
                } else if (xz1 != null) {
                    a(xz1.a(), xz1.b());
                    return;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        z = false;
        if (z) {
        }
    }

    @DexIgnore
    public static int a(xz1 xz1) {
        if (xz1 == null) {
            return -1;
        }
        return xz1.b();
    }
}
