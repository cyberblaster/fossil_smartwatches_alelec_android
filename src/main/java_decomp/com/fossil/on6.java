package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on6 {
    @DexIgnore
    public static /* final */ ThreadLocal<em6> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ on6 b; // = new on6();

    @DexIgnore
    public final em6 a() {
        return a.get();
    }

    @DexIgnore
    public final em6 b() {
        em6 em6 = a.get();
        if (em6 != null) {
            return em6;
        }
        em6 a2 = hm6.a();
        a.set(a2);
        return a2;
    }

    @DexIgnore
    public final void c() {
        a.set((Object) null);
    }

    @DexIgnore
    public final void a(em6 em6) {
        wg6.b(em6, "eventLoop");
        a.set(em6);
    }
}
