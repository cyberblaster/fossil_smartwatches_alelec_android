package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y70 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ e80 c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            byte readByte = parcel.readByte();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                Parcelable readParcelable = parcel.readParcelable(e80.class.getClassLoader());
                if (readParcelable != null) {
                    return new y70(readByte, readString, (e80) readParcelable);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new y70[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ y70(byte b2, String str, e80 e80, int i, qg6 qg6) {
        this(b2, str, (i & 4) != 0 ? new e80("", new byte[0]) : e80);
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.ID, (Object) Byte.valueOf(this.a)), bm0.MESSAGE, (Object) this.b), bm0.ICON, (Object) this.c.a());
    }

    @DexIgnore
    public final byte[] b() {
        byte[] bArr;
        String a2 = cw0.a(this.b);
        Charset f = mi0.A.f();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            if (this.c.e()) {
                bArr = new byte[0];
            } else {
                String fileName = this.c.getFileName();
                Charset defaultCharset = Charset.defaultCharset();
                wg6.a(defaultCharset, "Charset.defaultCharset()");
                if (fileName != null) {
                    byte[] bytes2 = fileName.getBytes(defaultCharset);
                    wg6.a(bytes2, "(this as java.lang.String).getBytes(charset)");
                    bArr = md6.a(bytes2, (byte) 0);
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            int length = bytes.length;
            int length2 = bArr.length;
            int i = length + 8 + length2;
            byte[] array = ByteBuffer.allocate(i).order(ByteOrder.LITTLE_ENDIAN).putShort((short) i).put((byte) 8).put(this.a).putShort((short) length).putShort((short) length2).put(bytes).put(bArr).array();
            wg6.a(array, "ByteBuffer.allocate(entr\u2026\n                .array()");
            return array;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] c() {
        return this.d;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(y70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            y70 y70 = (y70) obj;
            return this.a == y70.a && !(wg6.a(this.b, y70.b) ^ true) && !(wg6.a(this.c, y70.c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationReplyMessage");
    }

    @DexIgnore
    public final String getMessageContent() {
        return this.b;
    }

    @DexIgnore
    public final e80 getMessageIcon() {
        return this.c;
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + (cw0.b(this.a) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.a);
        parcel.writeString(this.b);
        parcel.writeParcelable(this.c, i);
    }

    @DexIgnore
    public y70(byte b2, String str, e80 e80) {
        this.a = b2;
        this.b = str;
        this.c = e80;
        this.d = b();
    }
}
