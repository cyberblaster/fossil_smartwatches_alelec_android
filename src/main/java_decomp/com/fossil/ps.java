package com.fossil;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ps extends ds<InputStream> {
    @DexIgnore
    public ps(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @DexIgnore
    public InputStream a(AssetManager assetManager, String str) throws IOException {
        return assetManager.open(str);
    }

    @DexIgnore
    public void a(InputStream inputStream) throws IOException {
        inputStream.close();
    }
}
