package com.fossil;

import com.fossil.t40;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum u40 implements bc0 {
    BLUETOOTH_OFF(0),
    INVALID_PARAMETERS(1),
    DEVICE_BUSY(2),
    EXECUTION_TIMEOUT(3),
    REQUEST_FAILED(4),
    REQUEST_UNSUPPORTED(5),
    RESPONSE_TIMEOUT(6),
    RESPONSE_FAILED(7),
    CONNECTION_DROPPED(8),
    INTERRUPTED(9),
    SERVICE_CHANGED(10),
    AUTHENTICATION_FAILED(11),
    SECRET_KEY_IS_REQUIRED(12),
    DATA_SIZE_OVER_LIMIT(13),
    UNSUPPORTED_FORMAT(14),
    TARGET_FIRMWARE_NOT_MATCHED(15),
    REQUEST_TIMEOUT(16),
    UNKNOWN_ERROR(255);
    
    @DexIgnore
    public static /* final */ a d; // = null;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final u40 a(km1 km1, HashMap<t40.b, Object> hashMap) {
            Boolean bool = (Boolean) hashMap.get(t40.b.HAS_SERVICE_CHANGED);
            boolean booleanValue = bool != null ? bool.booleanValue() : false;
            switch (ln1.a[km1.b.ordinal()]) {
                case 1:
                    return u40.INTERRUPTED;
                case 2:
                    if (booleanValue) {
                        return u40.SERVICE_CHANGED;
                    }
                    return u40.CONNECTION_DROPPED;
                case 3:
                    return u40.REQUEST_UNSUPPORTED;
                case 4:
                    bn0 bn0 = km1.c;
                    il0 il0 = bn0.c;
                    if (il0 == il0.REQUEST_TIMEOUT) {
                        return u40.REQUEST_TIMEOUT;
                    }
                    if (il0 == il0.RESPONSE_TIMEOUT) {
                        return u40.RESPONSE_TIMEOUT;
                    }
                    if (il0 == il0.RESPONSE_ERROR || il0 == il0.EMPTY_SERVICES) {
                        return u40.RESPONSE_FAILED;
                    }
                    if (bn0.e == re0.SIZE_OVER_LIMIT) {
                        return u40.DATA_SIZE_OVER_LIMIT;
                    }
                    return u40.REQUEST_FAILED;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    return u40.RESPONSE_FAILED;
                case 16:
                    return u40.EXECUTION_TIMEOUT;
                case 17:
                case 18:
                    return u40.REQUEST_UNSUPPORTED;
                case 19:
                    return u40.BLUETOOTH_OFF;
                case 20:
                case 21:
                    return u40.AUTHENTICATION_FAILED;
                case 22:
                    return u40.SECRET_KEY_IS_REQUIRED;
                case 23:
                    return u40.INVALID_PARAMETERS;
                case 24:
                    return u40.REQUEST_FAILED;
                case 25:
                    return u40.DEVICE_BUSY;
                case 26:
                    return u40.UNSUPPORTED_FORMAT;
                case 27:
                    return u40.TARGET_FIRMWARE_NOT_MATCHED;
                case 28:
                case 29:
                case 30:
                case 31:
                    return u40.UNKNOWN_ERROR;
                default:
                    throw new kc6();
            }
        }
    }

    /*
    static {
        d = new a((qg6) null);
    }
    */

    @DexIgnore
    public u40(int i) {
        this.b = i;
        this.a = cw0.a((Enum<?>) this);
    }

    @DexIgnore
    public int getCode() {
        return this.b;
    }

    @DexIgnore
    public String getLogName() {
        return this.a;
    }
}
