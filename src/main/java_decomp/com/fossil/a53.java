package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a53 extends t63 implements v63 {
    @DexIgnore
    public a53(x53 x53) {
        super(x53);
        w12.a(x53);
    }

    @DexIgnore
    public void e() {
        this.a.i();
    }

    @DexIgnore
    public void f() {
        this.a.a().f();
    }

    @DexIgnore
    public void g() {
        this.a.a().g();
    }

    @DexIgnore
    public void m() {
        this.a.j();
        throw null;
    }

    @DexIgnore
    public w03 n() {
        return this.a.I();
    }

    @DexIgnore
    public e73 o() {
        return this.a.v();
    }

    @DexIgnore
    public q43 p() {
        return this.a.H();
    }

    @DexIgnore
    public l83 q() {
        return this.a.F();
    }

    @DexIgnore
    public g83 r() {
        return this.a.E();
    }

    @DexIgnore
    public p43 s() {
        return this.a.y();
    }

    @DexIgnore
    public m93 t() {
        return this.a.s();
    }
}
