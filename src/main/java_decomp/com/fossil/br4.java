package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.debug.DebugActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br4 implements MembersInjector<DebugActivity> {
    @DexIgnore
    public static void a(DebugActivity debugActivity, an4 an4) {
        debugActivity.B = an4;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, fs4 fs4) {
        debugActivity.C = fs4;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, FirmwareFileRepository firmwareFileRepository) {
        debugActivity.D = firmwareFileRepository;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, GuestApiService guestApiService) {
        debugActivity.E = guestApiService;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, DianaPresetRepository dianaPresetRepository) {
        debugActivity.F = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, ShakeFeedbackService shakeFeedbackService) {
        debugActivity.G = shakeFeedbackService;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, cj4 cj4) {
        debugActivity.H = cj4;
    }
}
