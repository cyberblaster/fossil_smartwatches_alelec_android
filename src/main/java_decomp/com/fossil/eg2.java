package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg2 extends uf2 implements dg2 {
    @DexIgnore
    public eg2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    @DexIgnore
    public final void a(bw2 bw2, fg2 fg2, String str) throws RemoteException {
        Parcel q = q();
        zg2.a(q, (Parcelable) bw2);
        zg2.a(q, (IInterface) fg2);
        q.writeString(str);
        b(63, q);
    }

    @DexIgnore
    public final void a(eh2 eh2) throws RemoteException {
        Parcel q = q();
        zg2.a(q, (Parcelable) eh2);
        b(75, q);
    }

    @DexIgnore
    public final void a(tg2 tg2) throws RemoteException {
        Parcel q = q();
        zg2.a(q, (Parcelable) tg2);
        b(59, q);
    }

    @DexIgnore
    public final void e(boolean z) throws RemoteException {
        Parcel q = q();
        zg2.a(q, z);
        b(12, q);
    }

    @DexIgnore
    public final Location zza(String str) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        Parcel a = a(21, q);
        Location location = (Location) zg2.a(a, Location.CREATOR);
        a.recycle();
        return location;
    }
}
