package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum BodyState {
    OFF_BODY(0),
    ON_BODY(1);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public BodyState(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
