package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum SkinProximity {
    OFF_SKIN(0),
    ON_SKIN(1),
    UNKNOWN(2);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public SkinProximity(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
