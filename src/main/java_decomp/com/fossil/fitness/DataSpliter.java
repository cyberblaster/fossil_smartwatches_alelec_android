package com.fossil.fitness;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DataSpliter {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends DataSpliter {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native DataSpliter create();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native ArrayList<BinaryFile> native_splitHexString(long j, String str, int i, int i2);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        public ArrayList<BinaryFile> splitHexString(String str, int i, int i2) {
            return native_splitHexString(this.nativeRef, str, i, i2);
        }
    }

    @DexIgnore
    public static DataSpliter create() {
        return CppProxy.create();
    }

    @DexIgnore
    public abstract ArrayList<BinaryFile> splitHexString(String str, int i, int i2);
}
