package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Looper;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q81 implements ef0<p51> {
    @DexIgnore
    public /* final */ int a; // = 255;
    @DexIgnore
    public /* final */ UUID b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public b31 f;
    @DexIgnore
    public hg6<? super q81, cd6> g;
    @DexIgnore
    public hg6<? super q81, cd6> h;
    @DexIgnore
    public hg6<? super q81, cd6> i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ ma1 k;
    @DexIgnore
    public /* final */ BluetoothDevice l;
    @DexIgnore
    public /* final */ vj0 m;

    @DexIgnore
    public q81(ma1 ma1, BluetoothDevice bluetoothDevice, vj0 vj0) {
        this.k = ma1;
        this.l = bluetoothDevice;
        this.m = vj0;
        UUID randomUUID = UUID.randomUUID();
        wg6.a(randomUUID, "UUID.randomUUID()");
        this.b = randomUUID;
        this.c = 1000;
        cw0.a((Enum<?>) this.k);
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.d = new Handler(myLooper);
            this.f = new b31(this.k, g11.NOT_START, (t31) null, 4);
            this.j = true;
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public abstract JSONObject a(boolean z);

    @DexIgnore
    public abstract void a(fu0 fu0);

    @DexIgnore
    public void a(Object obj) {
        p51 p51 = (p51) obj;
        if (b(p51)) {
            b31 b31 = this.f;
            if (b31.b != g11.INTERRUPTED) {
                a(p51);
            } else {
                this.f = b31.a(b31, (ma1) null, (g11) null, p51.a, 3);
            }
            a();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.j;
    }

    @DexIgnore
    public abstract boolean b(p51 p51);

    @DexIgnore
    public abstract hn1<p51> c();

    @DexIgnore
    public boolean d() {
        return false;
    }

    @DexIgnore
    public void a(p51 p51) {
        b31 a2 = b31.d.a(p51.a);
        this.f = b31.a(this.f, (ma1) null, a2.b, a2.c, 1);
    }

    @DexIgnore
    public final void a() {
        if (!this.e) {
            this.e = true;
            hn1<p51> c2 = c();
            if (c2 != null) {
                c2.b(this);
            }
            g11 g11 = g11.SUCCESS;
            b31 b31 = this.f;
            if (g11 == b31.b) {
                oa1 oa1 = oa1.a;
                p40.a(b31, 0, 1, (Object) null);
                hg6<? super q81, cd6> hg6 = this.g;
                if (hg6 != null) {
                    cd6 cd6 = (cd6) hg6.invoke(this);
                }
            } else {
                oa1 oa12 = oa1.a;
                p40.a(b31, 0, 1, (Object) null);
                hg6<? super q81, cd6> hg62 = this.h;
                if (hg62 != null) {
                    cd6 cd62 = (cd6) hg62.invoke(this);
                }
            }
            if (b()) {
                qs0 qs0 = qs0.h;
                String a2 = cw0.a((Enum<?>) this.k);
                og0 og0 = og0.RESPONSE;
                String address = this.l.getAddress();
                wg6.a(address, "device.address");
                String uuid = this.b.toString();
                wg6.a(uuid, "requestUuid.toString()");
                qs0.a(new nn0(a2, og0, address, "", uuid, true, (String) null, (r40) null, (bw0) null, cw0.a(a(true), this.f.a()), 448));
            }
            hg6<? super q81, cd6> hg63 = this.i;
            if (hg63 != null) {
                cd6 cd63 = (cd6) hg63.invoke(this);
            }
        }
    }
}
