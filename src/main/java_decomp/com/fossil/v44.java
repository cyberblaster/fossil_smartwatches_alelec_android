package com.fossil;

import android.text.TextUtils;
import com.fossil.ci4;
import com.fossil.fi4;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutLocation;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v44 {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<WorkoutStateChange>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<List<WorkoutStateChange>> {
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = v44.class.getSimpleName();
        wg6.a((Object) simpleName, "WorkoutTypeConverter::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(WorkoutCalorie workoutCalorie) {
        if (workoutCalorie == null) {
            return null;
        }
        try {
            return new Gson().a(workoutCalorie);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutDistance b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutDistance) new Gson().a(str, WorkoutDistance.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutHeartRate c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutHeartRate) new Gson().a(str, WorkoutHeartRate.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutLocation d(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutLocation) new Gson().a(str, WorkoutLocation.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutLocation ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ci4 e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        ci4.a aVar = ci4.Companion;
        if (str != null) {
            return aVar.a(str);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSpeed f(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutSpeed) new Gson().a(str, WorkoutSpeed.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final List<WorkoutStateChange> g(String str) {
        wg6.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList();
        }
        try {
            Object a2 = new Gson().a(str, new c().getType());
            wg6.a(a2, "Gson().fromJson(data, type)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final WorkoutStep h(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutStep) new Gson().a(str, WorkoutStep.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStep ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final fi4 i(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        fi4.a aVar = fi4.Companion;
        if (str != null) {
            return aVar.a(str);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutCalorie a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutCalorie) new Gson().a(str, WorkoutCalorie.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutDistance workoutDistance) {
        if (workoutDistance == null) {
            return null;
        }
        try {
            return new Gson().a(workoutDistance);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutHeartRate workoutHeartRate) {
        if (workoutHeartRate == null) {
            return null;
        }
        try {
            return new Gson().a(workoutHeartRate);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutStep workoutStep) {
        if (workoutStep == null) {
            return null;
        }
        try {
            return new Gson().a(workoutStep);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStep ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(ci4 ci4) {
        if (ci4 != null) {
            return ci4.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final String a(fi4 fi4) {
        if (fi4 != null) {
            return fi4.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final String a(WorkoutSpeed workoutSpeed) {
        try {
            return new Gson().a(workoutSpeed);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutLocation workoutLocation) {
        if (workoutLocation == null) {
            return null;
        }
        try {
            return new Gson().a(workoutLocation);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutLocation ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(List<WorkoutStateChange> list) {
        wg6.b(list, "workoutStateChangeList");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = new Gson().a(list, new b().getType());
            wg6.a((Object) a2, "Gson().toJson(workoutStateChangeList, type)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }
}
