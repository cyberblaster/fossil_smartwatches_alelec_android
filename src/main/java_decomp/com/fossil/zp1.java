package com.fossil;

import com.fossil.eq1;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp1 extends eq1 {
    @DexIgnore
    public /* final */ Iterable<np1> a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Iterable<np1> a() {
        return this.a;
    }

    @DexIgnore
    public byte[] b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof eq1)) {
            return false;
        }
        eq1 eq1 = (eq1) obj;
        if (this.a.equals(eq1.a())) {
            if (Arrays.equals(this.b, eq1 instanceof zp1 ? ((zp1) eq1).b : eq1.b())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "BackendRequest{events=" + this.a + ", extras=" + Arrays.toString(this.b) + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends eq1.a {
        @DexIgnore
        public Iterable<np1> a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        public eq1.a a(Iterable<np1> iterable) {
            if (iterable != null) {
                this.a = iterable;
                return this;
            }
            throw new NullPointerException("Null events");
        }

        @DexIgnore
        public eq1.a a(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @DexIgnore
        public eq1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " events";
            }
            if (str.isEmpty()) {
                return new zp1(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public zp1(Iterable<np1> iterable, byte[] bArr) {
        this.a = iterable;
        this.b = bArr;
    }
}
