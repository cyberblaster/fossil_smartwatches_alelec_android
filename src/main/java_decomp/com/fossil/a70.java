package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.stetho.dumpapp.Framer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a70 extends r60 {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b((qg6) null);
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public enum a {
        OFF((byte) 0),
        LOW(Framer.STDERR_FRAME_PREFIX),
        MEDIUM((byte) 75),
        HIGH((byte) 100);
        
        @DexIgnore
        public static /* final */ C0007a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.a70$a$a")
        /* renamed from: com.fossil.a70$a$a  reason: collision with other inner class name */
        public static final class C0007a {
            @DexIgnore
            public /* synthetic */ C0007a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(byte b) {
                boolean z = false;
                if (true == (b <= 25)) {
                    return a.OFF;
                }
                if (true == (b <= 50)) {
                    return a.LOW;
                }
                if (b <= 75) {
                    z = true;
                }
                if (true == z) {
                    return a.MEDIUM;
                }
                return a.HIGH;
            }
        }

        /*
        static {
            c = new C0007a((qg6) null);
        }
        */

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<a70> {
        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
        }

        @DexIgnore
        public final a70 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new a70(a.c.a(bArr[0]));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        public a70 createFromParcel(Parcel parcel) {
            return new a70(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new a70[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m3createFromParcel(Parcel parcel) {
            return new a70(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public a70(a aVar) {
        super(s60.VIBE_STRENGTH);
        this.b = aVar;
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(a70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((a70) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
    }

    @DexIgnore
    public final a getVibeStrengthLevel() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }

    @DexIgnore
    public String d() {
        return cw0.a((Enum<?>) this.b);
    }

    @DexIgnore
    public /* synthetic */ a70(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.b = a.valueOf(readString);
            return;
        }
        wg6.a();
        throw null;
    }
}
