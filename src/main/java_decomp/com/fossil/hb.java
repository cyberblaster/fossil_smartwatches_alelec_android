package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hb {
    @DexIgnore
    public abstract ViewDataBinding a(jb jbVar, View view, int i);

    @DexIgnore
    public abstract ViewDataBinding a(jb jbVar, View[] viewArr, int i);

    @DexIgnore
    public List<hb> a() {
        return Collections.emptyList();
    }
}
