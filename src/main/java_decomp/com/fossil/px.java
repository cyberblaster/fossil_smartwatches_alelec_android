package com.fossil;

import android.graphics.Bitmap;
import com.fossil.kr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px implements kr.a {
    @DexIgnore
    public /* final */ au a;
    @DexIgnore
    public /* final */ xt b;

    @DexIgnore
    public px(au auVar, xt xtVar) {
        this.a = auVar;
        this.b = xtVar;
    }

    @DexIgnore
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.a.b(i, i2, config);
    }

    @DexIgnore
    public byte[] b(int i) {
        xt xtVar = this.b;
        if (xtVar == null) {
            return new byte[i];
        }
        return (byte[]) xtVar.b(i, byte[].class);
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.a.a(bitmap);
    }

    @DexIgnore
    public void a(byte[] bArr) {
        xt xtVar = this.b;
        if (xtVar != null) {
            xtVar.put(bArr);
        }
    }

    @DexIgnore
    public int[] a(int i) {
        xt xtVar = this.b;
        if (xtVar == null) {
            return new int[i];
        }
        return (int[]) xtVar.b(i, int[].class);
    }

    @DexIgnore
    public void a(int[] iArr) {
        xt xtVar = this.b;
        if (xtVar != null) {
            xtVar.put(iArr);
        }
    }
}
