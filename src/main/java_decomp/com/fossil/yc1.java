package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yc1 extends xg6 implements hg6<BluetoothGattCharacteristic, String> {
    @DexIgnore
    public static /* final */ yc1 a; // = new yc1();

    @DexIgnore
    public yc1() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic = (BluetoothGattCharacteristic) obj;
        StringBuilder sb = new StringBuilder();
        sb.append("    ");
        wg6.a(bluetoothGattCharacteristic, "characteristic");
        sb.append(bluetoothGattCharacteristic.getUuid().toString());
        return sb.toString();
    }
}
