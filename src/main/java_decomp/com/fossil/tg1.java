package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg1 extends jb1<y40[], y40[]> {
    @DexIgnore
    public static /* final */ u31<y40[]>[] b; // = {new q51(), new n71(), new l91(new w40((byte) 3, (byte) 0))};
    @DexIgnore
    public static /* final */ ed1<y40[]>[] c; // = {new hb1(w31.ALARM), new cd1(w31.ALARM), new xe1(w31.ALARM, new w40((byte) 3, (byte) 0))};
    @DexIgnore
    public static /* final */ tg1 d; // = new tg1();

    @DexIgnore
    public ed1<y40[]>[] b() {
        return c;
    }

    @DexIgnore
    public final y40[] c(byte[] bArr) throws sw0 {
        try {
            return y40.CREATOR.a(jb1.a.a(bArr));
        } catch (IllegalArgumentException e) {
            throw new sw0(yu0.INVALID_FILE_DATA, e.getLocalizedMessage(), e);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v3, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v5 */
    /* JADX WARNING: type inference failed for: r8v2, types: [com.fossil.h50] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.fossil.d50] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final y40[] b(byte[] bArr) throws sw0 {
        Object r7;
        byte[] a = jb1.a.a(bArr);
        if (a.length % 3 == 0) {
            ArrayList arrayList = new ArrayList();
            uh6 a2 = ci6.a(ci6.d(0, a.length), 3);
            int a3 = a2.a();
            int b2 = a2.b();
            int c2 = a2.c();
            if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
                while (true) {
                    b50 a4 = b50.CREATOR.a(md6.a(a, a3, a3 + 3));
                    if (a4 instanceof e50) {
                        r7 = new d50((e50) a4, (k50) null, (c50) null, 6, (qg6) null);
                    } else {
                        r7 = a4 instanceof i50 ? new h50((i50) a4, (k50) null, (c50) null, 6, (qg6) null) : 0;
                    }
                    if (r7 != 0) {
                        arrayList.add(r7);
                    }
                    if (a3 == b2) {
                        break;
                    }
                    a3 += c2;
                }
            }
            Object[] array = arrayList.toArray(new y40[0]);
            if (array != null) {
                return (y40[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new sw0(yu0.INVALID_FILE_DATA, ze0.a(ze0.b("Size("), a.length, ") not divide to 3."), (Throwable) null, 4);
    }

    @DexIgnore
    public u31<y40[]>[] a() {
        return b;
    }

    @DexIgnore
    public final byte[] a(y40[] y40Arr) {
        a50 a50;
        ByteBuffer allocate = ByteBuffer.allocate(y40Arr.length * 3);
        for (y40 c2 : y40Arr) {
            a50[] c3 = c2.c();
            int length = c3.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    a50 = null;
                    break;
                }
                a50 = c3[i];
                if (a50 instanceof b50) {
                    break;
                }
                i++;
            }
            if (a50 != null) {
                allocate.put(a50.c());
            }
        }
        byte[] array = allocate.array();
        wg6.a(array, "entryDataBuffer.array()");
        return array;
    }

    @DexIgnore
    public final byte[] b(y40[] y40Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (y40 b2 : y40Arr) {
            byteArrayOutputStream.write(b2.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }
}
