package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty3 implements uy3 {
    @DexIgnore
    public int a() {
        return 4;
    }

    @DexIgnore
    public void a(vy3 vy3) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!vy3.i()) {
                break;
            }
            a(vy3.c(), sb);
            vy3.f++;
            if (sb.length() >= 4) {
                vy3.a(a((CharSequence) sb, 0));
                sb.delete(0, 4);
                if (xy3.a(vy3.d(), vy3.f, a()) != a()) {
                    vy3.b(0);
                    break;
                }
            }
        }
        sb.append(31);
        a(vy3, (CharSequence) sb);
    }

    @DexIgnore
    public static void a(vy3 vy3, CharSequence charSequence) {
        try {
            int length = charSequence.length();
            if (length != 0) {
                boolean z = true;
                if (length == 1) {
                    vy3.l();
                    int a = vy3.g().a() - vy3.a();
                    if (vy3.f() == 0 && a <= 2) {
                        vy3.b(0);
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String a2 = a(charSequence, 0);
                    if (!(!vy3.i()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        vy3.c(vy3.a() + i);
                        if (vy3.g().a() - vy3.a() >= 3) {
                            vy3.c(vy3.a() + a2.length());
                            z = false;
                        }
                    }
                    if (z) {
                        vy3.k();
                        vy3.f -= i;
                    } else {
                        vy3.a(a2);
                    }
                    vy3.b(0);
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            vy3.b(0);
        }
    }

    @DexIgnore
    public static void a(char c, StringBuilder sb) {
        if (c >= ' ' && c <= '?') {
            sb.append(c);
        } else if (c < '@' || c > '^') {
            xy3.a(c);
            throw null;
        } else {
            sb.append((char) (c - '@'));
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int length = charSequence.length() - i;
        if (length != 0) {
            char charAt = charSequence.charAt(i);
            char c = 0;
            char charAt2 = length >= 2 ? charSequence.charAt(i + 1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(i + 2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(i + 3);
            }
            int i2 = (charAt << 18) + (charAt2 << 12) + (charAt3 << 6) + c;
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }
}
