package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.util.Log;
import com.fossil.a7;
import com.fossil.j8;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m7 {
    @DexIgnore
    public ConcurrentHashMap<Long, a7.b> a; // = new ConcurrentHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements c<j8.f> {
        @DexIgnore
        public a(m7 m7Var) {
        }

        @DexIgnore
        public int a(j8.f fVar) {
            return fVar.d();
        }

        @DexIgnore
        public boolean b(j8.f fVar) {
            return fVar.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c<a7.c> {
        @DexIgnore
        public b(m7 m7Var) {
        }

        @DexIgnore
        public int a(a7.c cVar) {
            return cVar.e();
        }

        @DexIgnore
        public boolean b(a7.c cVar) {
            return cVar.f();
        }
    }

    @DexIgnore
    public interface c<T> {
        @DexIgnore
        int a(T t);

        @DexIgnore
        boolean b(T t);
    }

    @DexIgnore
    public static <T> T a(T[] tArr, int i, c<T> cVar) {
        int i2 = (i & 1) == 0 ? 400 : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (Math.abs(cVar.a(t2) - i2) * 2) + (cVar.b(t2) == z ? 0 : 1);
            if (t == null || i3 > abs) {
                t = t2;
                i3 = abs;
            }
        }
        return t;
    }

    @DexIgnore
    public static long b(Typeface typeface) {
        if (typeface == null) {
            return 0;
        }
        try {
            Field declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
            return ((Number) declaredField.get(typeface)).longValue();
        } catch (NoSuchFieldException e) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e);
            return 0;
        } catch (IllegalAccessException e2) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e2);
            return 0;
        }
    }

    @DexIgnore
    public j8.f a(j8.f[] fVarArr, int i) {
        return (j8.f) a(fVarArr, i, new a(this));
    }

    @DexIgnore
    public Typeface a(Context context, InputStream inputStream) {
        File a2 = n7.a(context);
        if (a2 == null) {
            return null;
        }
        try {
            if (!n7.a(a2, inputStream)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(a2.getPath());
            a2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            a2.delete();
        }
    }

    @DexIgnore
    public Typeface a(Context context, CancellationSignal cancellationSignal, j8.f[] fVarArr, int i) {
        InputStream inputStream;
        InputStream inputStream2 = null;
        if (fVarArr.length < 1) {
            return null;
        }
        try {
            inputStream = context.getContentResolver().openInputStream(a(fVarArr, i).c());
            try {
                Typeface a2 = a(context, inputStream);
                n7.a((Closeable) inputStream);
                return a2;
            } catch (IOException unused) {
                n7.a((Closeable) inputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream2 = inputStream;
                n7.a((Closeable) inputStream2);
                throw th;
            }
        } catch (IOException unused2) {
            inputStream = null;
            n7.a((Closeable) inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            n7.a((Closeable) inputStream2);
            throw th;
        }
    }

    @DexIgnore
    public final a7.c a(a7.b bVar, int i) {
        return (a7.c) a(bVar.a(), i, new b(this));
    }

    @DexIgnore
    public Typeface a(Context context, a7.b bVar, Resources resources, int i) {
        a7.c a2 = a(bVar, i);
        if (a2 == null) {
            return null;
        }
        Typeface a3 = h7.a(context, resources, a2.b(), a2.a(), i);
        a(a3, bVar);
        return a3;
    }

    @DexIgnore
    public Typeface a(Context context, Resources resources, int i, String str, int i2) {
        File a2 = n7.a(context);
        if (a2 == null) {
            return null;
        }
        try {
            if (!n7.a(a2, resources, i)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(a2.getPath());
            a2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            a2.delete();
        }
    }

    @DexIgnore
    public a7.b a(Typeface typeface) {
        long b2 = b(typeface);
        if (b2 == 0) {
            return null;
        }
        return this.a.get(Long.valueOf(b2));
    }

    @DexIgnore
    public final void a(Typeface typeface, a7.b bVar) {
        long b2 = b(typeface);
        if (b2 != 0) {
            this.a.put(Long.valueOf(b2), bVar);
        }
    }
}
