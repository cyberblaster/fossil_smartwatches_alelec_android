package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr1 implements Factory<lr1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<dq1> b;
    @DexIgnore
    public /* final */ Provider<ur1> c;
    @DexIgnore
    public /* final */ Provider<rr1> d;
    @DexIgnore
    public /* final */ Provider<Executor> e;
    @DexIgnore
    public /* final */ Provider<ys1> f;
    @DexIgnore
    public /* final */ Provider<zs1> g;

    @DexIgnore
    public mr1(Provider<Context> provider, Provider<dq1> provider2, Provider<ur1> provider3, Provider<rr1> provider4, Provider<Executor> provider5, Provider<ys1> provider6, Provider<zs1> provider7) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static mr1 a(Provider<Context> provider, Provider<dq1> provider2, Provider<ur1> provider3, Provider<rr1> provider4, Provider<Executor> provider5, Provider<ys1> provider6, Provider<zs1> provider7) {
        return new mr1(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    public lr1 get() {
        return new lr1((Context) this.a.get(), (dq1) this.b.get(), (ur1) this.c.get(), (rr1) this.d.get(), (Executor) this.e.get(), (ys1) this.f.get(), (zs1) this.g.get());
    }
}
