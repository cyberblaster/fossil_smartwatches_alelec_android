package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mm4 {
    @DexIgnore
    public static /* final */ String b; // = ("Localization_" + mm4.class.getSimpleName());
    @DexIgnore
    public Map<String, String> a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public a(mm4 mm4) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public b(mm4 mm4) {
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v10, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(String str, boolean z) {
        FileReader fileReader;
        InputStream open;
        InputStreamReader inputStreamReader;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "parseJSONFile() called with: filePath = [" + str + "], isDownloaded = [" + z + "]");
        try {
            Gson gson = new Gson();
            if (!z) {
                open = PortfolioApp.T.getAssets().open(str);
                inputStreamReader = new InputStreamReader(open, "UTF-8");
                try {
                    this.a.putAll((Map) gson.a(inputStreamReader, new a(this).getType()));
                    inputStreamReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    inputStreamReader.close();
                }
                open.close();
                return;
            }
            fileReader = new FileReader(new File(str));
            try {
                this.a.putAll((Map) gson.a(fileReader, new b(this).getType()));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            fileReader.close();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.e(str3, "parseJSONFile failed exception=" + e3);
        } catch (Throwable th) {
            inputStreamReader.close();
            open.close();
            throw th;
        }
    }

    @DexIgnore
    public Map<String, String> b() {
        return this.a;
    }

    @DexIgnore
    public void a() {
        Map<String, String> map = this.a;
        if (map != null) {
            map.clear();
        }
    }

    @DexIgnore
    public String a(String str) {
        return (this.a == null || TextUtils.isEmpty(str)) ? "" : this.a.get(str);
    }
}
