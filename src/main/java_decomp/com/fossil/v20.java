package com.fossil;

import android.content.Context;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@t96({z20.class})
public class v20 extends i86<Void> {
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, String> h;
    @DexIgnore
    public w20 i;
    @DexIgnore
    public w20 j;
    @DexIgnore
    public x20 o;
    @DexIgnore
    public u20 p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public float t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ s30 v;
    @DexIgnore
    public va6 w;
    @DexIgnore
    public t20 x;
    @DexIgnore
    public z20 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends w96<Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public u96 getPriority() {
            return u96.IMMEDIATE;
        }

        @DexIgnore
        public Void call() throws Exception {
            return v20.this.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public Void call() throws Exception {
            v20.this.i.a();
            c86.g().d("CrashlyticsCore", "Initialization marker file created.");
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Boolean> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public Boolean call() throws Exception {
            try {
                boolean d = v20.this.i.d();
                l86 g = c86.g();
                g.d("CrashlyticsCore", "Initialization marker file removed: " + d);
                return Boolean.valueOf(d);
            } catch (Exception e) {
                c86.g().e("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", e);
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Callable<Boolean> {
        @DexIgnore
        public /* final */ w20 a;

        @DexIgnore
        public d(w20 w20) {
            this.a = w20;
        }

        @DexIgnore
        public Boolean call() throws Exception {
            if (!this.a.c()) {
                return Boolean.FALSE;
            }
            c86.g().d("CrashlyticsCore", "Found previous crash marker.");
            this.a.d();
            return Boolean.TRUE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements x20 {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }
    }

    @DexIgnore
    public v20() {
        this(1.0f, (x20) null, (s30) null, false);
    }

    @DexIgnore
    public static v20 B() {
        return c86.a(v20.class);
    }

    @DexIgnore
    public static boolean d(String str) {
        v20 B = B();
        if (B != null && B.p != null) {
            return true;
        }
        l86 g2 = c86.g();
        g2.e("CrashlyticsCore", "Crashlytics must be initialized by calling Fabric.with(Context) " + str, (Throwable) null);
        return false;
    }

    @DexIgnore
    public static String e(String str) {
        if (str == null) {
            return str;
        }
        String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    @DexIgnore
    public void A() {
        this.x.b(new b());
    }

    @DexIgnore
    public void b(int i2, String str, String str2) {
        a(i2, str, str2);
        c86.g().a(i2, "" + str, "" + str2, true);
    }

    @DexIgnore
    public String h() {
        return "com.crashlytics.sdk.android.crashlytics-core";
    }

    @DexIgnore
    public String j() {
        return "2.7.0.33";
    }

    @DexIgnore
    public boolean m() {
        return a(v20.super.d());
    }

    @DexIgnore
    public final void n() {
        if (Boolean.TRUE.equals((Boolean) this.x.b(new d(this.j)))) {
            try {
                this.o.a();
            } catch (Exception e2) {
                c86.g().e("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", e2);
            }
        }
    }

    @DexIgnore
    public void o() {
        this.j.a();
    }

    @DexIgnore
    public boolean p() {
        return this.i.c();
    }

    @DexIgnore
    public final void s() {
        z96 aVar = new a();
        for (ba6 a2 : e()) {
            aVar.a(a2);
        }
        Future submit = f().b().submit(aVar);
        c86.g().d("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            c86.g().e("CrashlyticsCore", "Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            c86.g().e("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", e3);
        } catch (TimeoutException e4) {
            c86.g().e("CrashlyticsCore", "Crashlytics timed out during initialization.", e4);
        }
    }

    @DexIgnore
    public Map<String, String> t() {
        return Collections.unmodifiableMap(this.h);
    }

    @DexIgnore
    public y20 u() {
        z20 z20 = this.y;
        if (z20 != null) {
            return z20.a();
        }
        return null;
    }

    @DexIgnore
    public String v() {
        if (g().a()) {
            return this.r;
        }
        return null;
    }

    @DexIgnore
    public String x() {
        if (g().a()) {
            return this.q;
        }
        return null;
    }

    @DexIgnore
    public String y() {
        if (g().a()) {
            return this.s;
        }
        return null;
    }

    @DexIgnore
    public void z() {
        this.x.a(new c());
    }

    @DexIgnore
    public v20(float f, x20 x20, s30 s30, boolean z) {
        this(f, x20, s30, z, f96.a("Crashlytics Exception Handler"));
    }

    @DexIgnore
    public boolean a(Context context) {
        String d2;
        Context context2 = context;
        if (!c96.a(context).a()) {
            c86.g().d("CrashlyticsCore", "Crashlytics is disabled, because data collection is disabled by Firebase.");
            this.u = true;
        }
        if (this.u || (d2 = new x86().d(context2)) == null) {
            return false;
        }
        String n = z86.n(context);
        if (a(n, z86.a(context2, "com.crashlytics.RequireBuildId", true))) {
            try {
                l86 g2 = c86.g();
                g2.i("CrashlyticsCore", "Initializing Crashlytics Core " + j());
                bb6 bb6 = new bb6(this);
                this.j = new w20("crash_marker", bb6);
                this.i = new w20("initialization_marker", bb6);
                t30 a2 = t30.a(new db6(d(), "com.crashlytics.android.core.CrashlyticsCore"), this);
                xa6 a30 = this.v != null ? new a30(this.v) : null;
                this.w = new sa6(c86.g());
                this.w.a(a30);
                j96 g3 = g();
                k20 a3 = k20.a(context2, g3, d2, n);
                u20 u20 = r1;
                u20 u202 = new u20(this, this.x, this.w, g3, a2, bb6, a3, new a40(context2, new l30(context2, a3.d)), new e30(this), h10.b(context));
                this.p = u20;
                boolean p2 = p();
                n();
                this.p.a(Thread.getDefaultUncaughtExceptionHandler(), new i96().e(context2));
                if (!p2 || !z86.b(context)) {
                    c86.g().d("CrashlyticsCore", "Exception handling initialization successful");
                    return true;
                }
                c86.g().d("CrashlyticsCore", "Crashlytics did not finish previous background initialization. Initializing synchronously.");
                s();
                return false;
            } catch (Exception e2) {
                c86.g().e("CrashlyticsCore", "Crashlytics was not started due to an exception during initialization", e2);
                this.p = null;
                return false;
            }
        } else {
            throw new ca6("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    @DexIgnore
    public Void c() {
        A();
        this.p.a();
        try {
            this.p.p();
            yb6 a2 = vb6.d().a();
            if (a2 == null) {
                c86.g().w("CrashlyticsCore", "Received null settings, skipping report submission!");
                z();
                return null;
            }
            this.p.a(a2);
            if (!a2.d.b) {
                c86.g().d("CrashlyticsCore", "Collection of crash reports disabled in Crashlytics settings.");
                z();
                return null;
            } else if (!c96.a(d()).a()) {
                c86.g().d("CrashlyticsCore", "Automatic collection of crash reports disabled by Firebase settings.");
                z();
                return null;
            } else {
                y20 u2 = u();
                if (u2 != null && !this.p.a(u2)) {
                    c86.g().d("CrashlyticsCore", "Could not finalize previous NDK sessions.");
                }
                if (!this.p.b(a2.b)) {
                    c86.g().d("CrashlyticsCore", "Could not finalize previous sessions.");
                }
                this.p.a(this.t, a2);
                z();
                return null;
            }
        } catch (Exception e2) {
            c86.g().e("CrashlyticsCore", "Crashlytics encountered a problem during asynchronous initialization.", e2);
        } catch (Throwable th) {
            z();
            throw th;
        }
    }

    @DexIgnore
    public void b(String str) {
        if (!this.u && d("prior to setting user data.")) {
            this.r = e(str);
            this.p.a(this.q, this.s, this.r);
        }
    }

    @DexIgnore
    public v20(float f, x20 x20, s30 s30, boolean z, ExecutorService executorService) {
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = f;
        this.o = x20 == null ? new e((a) null) : x20;
        this.v = s30;
        this.u = z;
        this.x = new t20(executorService);
        this.h = new ConcurrentHashMap<>();
        this.g = System.currentTimeMillis();
    }

    @DexIgnore
    public void c(String str) {
        if (!this.u && d("prior to setting user data.")) {
            this.q = e(str);
            this.p.a(this.q, this.s, this.r);
        }
    }

    @DexIgnore
    public static String c(int i2, String str, String str2) {
        return z86.a(i2) + "/" + str + " " + str2;
    }

    @DexIgnore
    public void a(String str) {
        a(3, "CrashlyticsCore", str);
    }

    @DexIgnore
    public final void a(int i2, String str, String str2) {
        if (!this.u && d("prior to logging messages.")) {
            this.p.a(System.currentTimeMillis() - this.g, c(i2, str, str2));
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        String str3;
        if (this.u || !d("prior to setting keys.")) {
            return;
        }
        if (str == null) {
            Context d2 = d();
            if (d2 == null || !z86.j(d2)) {
                c86.g().e("CrashlyticsCore", "Attempting to set custom attribute with null key, ignoring.", (Throwable) null);
                return;
            }
            throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
        String e2 = e(str);
        if (this.h.size() < 64 || this.h.containsKey(e2)) {
            if (str2 == null) {
                str3 = "";
            } else {
                str3 = e(str2);
            }
            this.h.put(e2, str3);
            this.p.a((Map<String, String>) this.h);
            return;
        }
        c86.g().d("CrashlyticsCore", "Exceeded maximum number of custom attributes (64)");
    }

    @DexIgnore
    public static boolean a(String str, boolean z) {
        if (!z) {
            c86.g().d("CrashlyticsCore", "Configured not to require a build ID.");
            return true;
        } else if (!z86.b(str)) {
            return true;
        } else {
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("CrashlyticsCore", ".     |  | ");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".   \\ |  | /");
            Log.e("CrashlyticsCore", ".    \\    /");
            Log.e("CrashlyticsCore", ".     \\  /");
            Log.e("CrashlyticsCore", ".      \\/");
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("CrashlyticsCore", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("CrashlyticsCore", ".      /\\");
            Log.e("CrashlyticsCore", ".     /  \\");
            Log.e("CrashlyticsCore", ".    /    \\");
            Log.e("CrashlyticsCore", ".   / |  | \\");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            return false;
        }
    }
}
