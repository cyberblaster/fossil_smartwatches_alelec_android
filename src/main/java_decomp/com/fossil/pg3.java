package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pg3 implements x1 {
    @DexIgnore
    public q1 a;
    @DexIgnore
    public BottomNavigationMenuView b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0036a();
        @DexIgnore
        public int a;
        @DexIgnore
        public gi3 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.pg3$a$a")
        /* renamed from: com.fossil.pg3$a$a  reason: collision with other inner class name */
        public static class C0036a implements Parcelable.Creator<a> {
            @DexIgnore
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.b, 0);
        }

        @DexIgnore
        public a(Parcel parcel) {
            this.a = parcel.readInt();
            this.b = (gi3) parcel.readParcelable(a.class.getClassLoader());
        }
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z) {
    }

    @DexIgnore
    public void a(BottomNavigationMenuView bottomNavigationMenuView) {
        this.b = bottomNavigationMenuView;
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        return false;
    }

    @DexIgnore
    public boolean a(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public Parcelable b() {
        a aVar = new a();
        aVar.a = this.b.getSelectedItemId();
        aVar.b = mg3.a(this.b.getBadgeDrawables());
        return aVar;
    }

    @DexIgnore
    public boolean b(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public int getId() {
        return this.d;
    }

    @DexIgnore
    public void a(Context context, q1 q1Var) {
        this.a = q1Var;
        this.b.a(this.a);
    }

    @DexIgnore
    public void a(boolean z) {
        if (!this.c) {
            if (z) {
                this.b.a();
            } else {
                this.b.d();
            }
        }
    }

    @DexIgnore
    public void b(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void a(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        if (parcelable instanceof a) {
            a aVar = (a) parcelable;
            this.b.c(aVar.a);
            this.b.setBadgeDrawables(mg3.a(this.b.getContext(), aVar.b));
        }
    }
}
