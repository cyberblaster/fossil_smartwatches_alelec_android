package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.StressWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b44 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<RestingWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<ArrayList<SleepSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<ArrayList<RestingWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends TypeToken<ArrayList<SleepSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends TypeToken<ArrayList<WorkoutSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends TypeToken<ArrayList<WorkoutSessionWrapper>> {
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = j44.class.getSimpleName();
        wg6.a((Object) simpleName, "JsonObjectConverter::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public b44() {
        du3 du3 = new du3();
        du3.a(DateTime.class, new GsonConvertDateTime());
        Gson a2 = du3.a();
        wg6.a((Object) a2, "GsonBuilder().registerTy\u2026nvertDateTime()).create()");
        this.a = a2;
    }

    @DexIgnore
    public final String a(StepWrapper stepWrapper) {
        wg6.b(stepWrapper, "step");
        try {
            String a2 = this.a.a(stepWrapper, StepWrapper.class);
            wg6.a((Object) a2, "mGson.toJson(step, StepWrapper::class.java)");
            return a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("stepToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final CalorieWrapper b(String str) {
        wg6.b(str, "data");
        try {
            Object a2 = this.a.a(str, CalorieWrapper.class);
            wg6.a(a2, "mGson.fromJson(data, CalorieWrapper::class.java)");
            return (CalorieWrapper) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toCalorie exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new CalorieWrapper(0, new ArrayList(), 0.0f);
        }
    }

    @DexIgnore
    public final DistanceWrapper c(String str) {
        wg6.b(str, "data");
        try {
            Object a2 = this.a.a(str, DistanceWrapper.class);
            wg6.a(a2, "mGson.fromJson(data, DistanceWrapper::class.java)");
            return (DistanceWrapper) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toDistance exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new DistanceWrapper(0, new ArrayList(), 0.0d);
        }
    }

    @DexIgnore
    public final HeartRateWrapper d(String str) {
        if (str == null) {
            return null;
        }
        try {
            return (HeartRateWrapper) this.a.a(str, HeartRateWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toDistance exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final List<RestingWrapper> e(String str) {
        try {
            Object a2 = this.a.a(str, new d().getType());
            wg6.a(a2, "mGson.fromJson(data, listType)");
            return (List) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toResting exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final List<SleepSessionWrapper> f(String str) {
        wg6.b(str, "data");
        try {
            Object a2 = this.a.a(str, new e().getType());
            wg6.a(a2, "mGson.fromJson(data, listType)");
            return (List) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toSleepSessions exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final StepWrapper g(String str) {
        wg6.b(str, "data");
        try {
            Object a2 = this.a.a(str, StepWrapper.class);
            wg6.a(a2, "mGson.fromJson(data, StepWrapper::class.java)");
            return (StepWrapper) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toStep exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new StepWrapper(0, new ArrayList(), 0);
        }
    }

    @DexIgnore
    public final StressWrapper h(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            return (StressWrapper) this.a.a(str, StressWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toStress exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final List<WorkoutSessionWrapper> i(String str) {
        wg6.b(str, "data");
        try {
            Object a2 = this.a.a(str, new f().getType());
            wg6.a(a2, "mGson.fromJson(data, listType)");
            return (List) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSession exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final String a(ActiveMinuteWrapper activeMinuteWrapper) {
        wg6.b(activeMinuteWrapper, "activeMinute");
        try {
            String a2 = this.a.a(activeMinuteWrapper, ActiveMinuteWrapper.class);
            wg6.a((Object) a2, "mGson.toJson(activeMinut\u2026inuteWrapper::class.java)");
            return a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("activeMinuteToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String b(List<SleepSessionWrapper> list) {
        wg6.b(list, "sleepSessions");
        try {
            String a2 = this.a.a(list, new c().getType());
            wg6.a((Object) a2, "mGson.toJson(sleepSessions, listType)");
            return a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("sleepSessionsToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String c(List<WorkoutSessionWrapper> list) {
        wg6.b(list, "workoutSessions");
        try {
            String a2 = this.a.a(list, new g().getType());
            wg6.a((Object) a2, "mGson.toJson(workoutSessions, listType)");
            return a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("workoutSessionToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final ActiveMinuteWrapper a(String str) {
        wg6.b(str, "data");
        try {
            Object a2 = this.a.a(str, ActiveMinuteWrapper.class);
            wg6.a(a2, "mGson.fromJson(data, Act\u2026inuteWrapper::class.java)");
            return (ActiveMinuteWrapper) a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toActiveMinute exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            return new ActiveMinuteWrapper(0, new ArrayList(), 0);
        }
    }

    @DexIgnore
    public final String a(CalorieWrapper calorieWrapper) {
        wg6.b(calorieWrapper, "calorie");
        try {
            String a2 = this.a.a(calorieWrapper, CalorieWrapper.class);
            wg6.a((Object) a2, "mGson.toJson(calorie, CalorieWrapper::class.java)");
            return a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("calorieToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String a(DistanceWrapper distanceWrapper) {
        wg6.b(distanceWrapper, "distance");
        try {
            String a2 = this.a.a(distanceWrapper, DistanceWrapper.class);
            wg6.a((Object) a2, "mGson.toJson(distance, D\u2026tanceWrapper::class.java)");
            return a2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("distanceToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String a(StressWrapper stressWrapper) {
        if (stressWrapper == null) {
            return null;
        }
        try {
            return this.a.a(stressWrapper, StressWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("stressToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String a(List<RestingWrapper> list) {
        wg6.b(list, "resting");
        try {
            return this.a.a(list, new b().getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("restingToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String a(HeartRateWrapper heartRateWrapper) {
        if (heartRateWrapper == null) {
            return null;
        }
        try {
            return this.a.a(heartRateWrapper, HeartRateWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("heartRateToString exception=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }
}
