package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wt<T> {
    @DexIgnore
    int a();

    @DexIgnore
    int a(T t);

    @DexIgnore
    String b();

    @DexIgnore
    T newArray(int i);
}
