package com.fossil;

import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs6 {
    @DexIgnore
    public static String a(yq6 yq6, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(yq6.e());
        sb.append(' ');
        if (b(yq6, type)) {
            sb.append(yq6.g());
        } else {
            sb.append(a(yq6.g()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(yq6 yq6, Proxy.Type type) {
        return !yq6.d() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String a(tq6 tq6) {
        String c = tq6.c();
        String e = tq6.e();
        if (e == null) {
            return c;
        }
        return c + '?' + e;
    }
}
