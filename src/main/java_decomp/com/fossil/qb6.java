package com.fossil;

import com.fossil.ua6;
import com.zendesk.sdk.network.Constants;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qb6 extends r86 implements cc6 {
    @DexIgnore
    public qb6(i86 i86, String str, String str2, va6 va6) {
        this(i86, str, str2, va6, ta6.GET);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0087  */
    public JSONObject a(bc6 bc6) {
        ua6 ua6;
        l86 g;
        StringBuilder sb;
        JSONObject jSONObject = null;
        try {
            Map<String, String> b = b(bc6);
            ua6 = a(b);
            try {
                a(ua6, bc6);
                c86.g().d("Fabric", "Requesting settings from " + b());
                c86.g().d("Fabric", "Settings query params were: " + b);
                jSONObject = a(ua6);
                if (ua6 != null) {
                    g = c86.g();
                    sb = new StringBuilder();
                    sb.append("Settings request ID: ");
                    sb.append(ua6.c("X-REQUEST-ID"));
                    g.d("Fabric", sb.toString());
                }
            } catch (ua6.d e) {
                e = e;
                try {
                    c86.g().e("Fabric", "Settings request failed.", e);
                    if (ua6 != null) {
                        g = c86.g();
                        sb = new StringBuilder();
                        sb.append("Settings request ID: ");
                        sb.append(ua6.c("X-REQUEST-ID"));
                        g.d("Fabric", sb.toString());
                    }
                    return jSONObject;
                } catch (Throwable th) {
                    th = th;
                    if (ua6 != null) {
                    }
                    throw th;
                }
            }
        } catch (ua6.d e2) {
            e = e2;
            ua6 = null;
            c86.g().e("Fabric", "Settings request failed.", e);
            if (ua6 != null) {
            }
            return jSONObject;
        } catch (Throwable th2) {
            th = th2;
            ua6 = null;
            if (ua6 != null) {
                c86.g().d("Fabric", "Settings request ID: " + ua6.c("X-REQUEST-ID"));
            }
            throw th;
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean a(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            l86 g = c86.g();
            g.b("Fabric", "Failed to parse settings JSON from " + b(), e);
            l86 g2 = c86.g();
            g2.d("Fabric", "Settings response " + str);
            return null;
        }
    }

    @DexIgnore
    public qb6(i86 i86, String str, String str2, va6 va6, ta6 ta6) {
        super(i86, str, str2, va6, ta6);
    }

    @DexIgnore
    public final Map<String, String> b(bc6 bc6) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", bc6.h);
        hashMap.put("display_version", bc6.g);
        hashMap.put("source", Integer.toString(bc6.i));
        String str = bc6.j;
        if (str != null) {
            hashMap.put("icon_hash", str);
        }
        String str2 = bc6.f;
        if (!z86.b(str2)) {
            hashMap.put("instance", str2);
        }
        return hashMap;
    }

    @DexIgnore
    public JSONObject a(ua6 ua6) {
        int g = ua6.g();
        l86 g2 = c86.g();
        g2.d("Fabric", "Settings result was: " + g);
        if (a(g)) {
            return b(ua6.a());
        }
        l86 g3 = c86.g();
        g3.e("Fabric", "Failed to retrieve settings from " + b());
        return null;
    }

    @DexIgnore
    public final ua6 a(ua6 ua6, bc6 bc6) {
        a(ua6, "X-CRASHLYTICS-API-KEY", bc6.a);
        a(ua6, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a(ua6, "X-CRASHLYTICS-API-CLIENT-VERSION", this.e.j());
        a(ua6, Constants.ACCEPT_HEADER, Constants.APPLICATION_JSON);
        a(ua6, "X-CRASHLYTICS-DEVICE-MODEL", bc6.b);
        a(ua6, "X-CRASHLYTICS-OS-BUILD-VERSION", bc6.c);
        a(ua6, "X-CRASHLYTICS-OS-DISPLAY-VERSION", bc6.d);
        a(ua6, "X-CRASHLYTICS-INSTALLATION-ID", bc6.e);
        return ua6;
    }

    @DexIgnore
    public final void a(ua6 ua6, String str, String str2) {
        if (str2 != null) {
            ua6.c(str, str2);
        }
    }
}
