package com.fossil;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m53 implements ServiceConnection {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ n53 b;

    @DexIgnore
    public m53(n53 n53, String str) {
        this.b = n53;
        this.a = str;
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.b.a.b().w().a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            el2 a2 = co2.a(iBinder);
            if (a2 == null) {
                this.b.a.b().w().a("Install Referrer Service implementation was not found");
                return;
            }
            this.b.a.b().z().a("Install Referrer Service connected");
            this.b.a.a().a((Runnable) new p53(this, a2, this));
        } catch (Exception e) {
            this.b.a.b().w().a("Exception occurred while calling Install Referrer API", e);
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        this.b.a.b().z().a("Install Referrer Service disconnected");
    }
}
