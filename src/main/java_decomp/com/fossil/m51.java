package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum m51 {
    DISCONNECTED(0),
    CONNECTING(1),
    CONNECTED(2),
    DISCONNECTING(3);
    
    @DexIgnore
    public static /* final */ q31 f; // = null;

    /*
    static {
        f = new q31((qg6) null);
    }
    */

    @DexIgnore
    public m51(int i) {
    }
}
