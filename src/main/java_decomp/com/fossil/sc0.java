package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ s50 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<sc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new sc0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new sc0[i];
        }
    }

    @DexIgnore
    public sc0(w90 w90, s50 s50) throws IllegalArgumentException {
        super(w90, (uc0) null);
        this.c = s50;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        x90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (this.c != null) {
                jSONObject2.put("dest", this.c.getDestination()).put("commute", this.c.getCommuteTimeInMinute()).put("traffic", this.c.getTraffic());
            } else {
                uc0 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
            }
            jSONObject.put("commuteApp._.config.commute_info", jSONObject2);
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        wg6.a(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset f = mi0.A.f();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public JSONObject b() {
        Object obj;
        JSONObject b = super.b();
        bm0 bm0 = bm0.COMMUTE_INFO;
        s50 s50 = this.c;
        if (s50 == null || (obj = s50.a()) == null) {
            obj = JSONObject.NULL;
        }
        return cw0.a(b, bm0, obj);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(sc0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.c, ((sc0) obj).c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData");
    }

    @DexIgnore
    public final s50 getCommuteTimeInfo() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        s50 s50 = this.c;
        return hashCode + (s50 != null ? s50.hashCode() : 0);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            s50 s50 = this.c;
            if (s50 != null) {
                parcel.writeParcelable(s50, i);
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.os.Parcelable");
        }
    }

    @DexIgnore
    public sc0(w90 w90, uc0 uc0) throws IllegalArgumentException {
        super(w90, uc0);
        this.c = null;
    }

    @DexIgnore
    public sc0(w90 w90, uc0 uc0, s50 s50) {
        super(w90, uc0);
        this.c = s50;
    }

    @DexIgnore
    public sc0(s50 s50) throws IllegalArgumentException {
        this((w90) null, (uc0) null, s50);
    }

    @DexIgnore
    public sc0(uc0 uc0) throws IllegalArgumentException {
        this((w90) null, uc0, (s50) null);
    }

    @DexIgnore
    public /* synthetic */ sc0(Parcel parcel, qg6 qg6) {
        super((x90) parcel.readParcelable(w90.class.getClassLoader()), (uc0) parcel.readParcelable(uc0.class.getClassLoader()));
        this.c = (s50) parcel.readParcelable(s50.class.getClassLoader());
    }
}
