package com.fossil;

import com.fossil.fn2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi2 extends fn2<wi2, a> implements to2 {
    @DexIgnore
    public static /* final */ wi2 zzl;
    @DexIgnore
    public static volatile yo2<wi2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public nn2<xi2> zzf; // = fn2.m();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public yi2 zzh;
    @DexIgnore
    public boolean zzi;
    @DexIgnore
    public boolean zzj;
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<wi2, a> implements to2 {
        @DexIgnore
        public a() {
            super(wi2.zzl);
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((wi2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public final String j() {
            return ((wi2) this.b).p();
        }

        @DexIgnore
        public final int k() {
            return ((wi2) this.b).r();
        }

        @DexIgnore
        public /* synthetic */ a(bj2 bj2) {
            this();
        }

        @DexIgnore
        public final xi2 a(int i) {
            return ((wi2) this.b).b(i);
        }

        @DexIgnore
        public final a a(int i, xi2 xi2) {
            f();
            ((wi2) this.b).a(i, xi2);
            return this;
        }
    }

    /*
    static {
        wi2 wi2 = new wi2();
        zzl = wi2;
        fn2.a(wi2.class, wi2);
    }
    */

    @DexIgnore
    public static a z() {
        return (a) zzl.h();
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 2;
            this.zze = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final xi2 b(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int o() {
        return this.zzd;
    }

    @DexIgnore
    public final String p() {
        return this.zze;
    }

    @DexIgnore
    public final List<xi2> q() {
        return this.zzf;
    }

    @DexIgnore
    public final int r() {
        return this.zzf.size();
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final yi2 t() {
        yi2 yi2 = this.zzh;
        return yi2 == null ? yi2.y() : yi2;
    }

    @DexIgnore
    public final boolean v() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean w() {
        return this.zzj;
    }

    @DexIgnore
    public final boolean x() {
        return (this.zzc & 64) != 0;
    }

    @DexIgnore
    public final boolean y() {
        return this.zzk;
    }

    @DexIgnore
    public final void a(int i, xi2 xi2) {
        if (xi2 != null) {
            if (!this.zzf.zza()) {
                this.zzf = fn2.a(this.zzf);
            }
            this.zzf.set(i, xi2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (bj2.a[i - 1]) {
            case 1:
                return new wi2();
            case 2:
                return new a((bj2) null);
            case 3:
                return fn2.a((ro2) zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\u001b\u0004\u0007\u0002\u0005\t\u0003\u0006\u0007\u0004\u0007\u0007\u0005\b\u0007\u0006", new Object[]{"zzc", "zzd", "zze", "zzf", xi2.class, "zzg", "zzh", "zzi", "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                yo2<wi2> yo2 = zzm;
                if (yo2 == null) {
                    synchronized (wi2.class) {
                        yo2 = zzm;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzl);
                            zzm = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
