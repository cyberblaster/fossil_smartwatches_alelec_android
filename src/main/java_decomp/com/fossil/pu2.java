package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu2 implements mu2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;
    @DexIgnore
    public static /* final */ pk2<Boolean> c;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.service.sessions.remove_disabled_session_number", true);
        b = vk2.a("measurement.service.sessions.session_number_enabled", true);
        c = vk2.a("measurement.service.sessions.session_number_backfill_enabled", true);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzb() {
        return b.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return c.b().booleanValue();
    }
}
