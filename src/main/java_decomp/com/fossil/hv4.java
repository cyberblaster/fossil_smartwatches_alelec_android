package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FossilNotificationImageView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hv4 extends RecyclerView.g<c> {
    @DexIgnore
    public List<ContactGroup> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ nz c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(ContactGroup contactGroup);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ hg4 a;
        @DexIgnore
        public /* final */ /* synthetic */ hv4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b = this.a.b.b;
                if (b != null) {
                    b.a();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1 && (b = this.a.b.b) != null) {
                    b.a((ContactGroup) this.a.b.a.get(adapterPosition));
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hv4$c$c")
        /* renamed from: com.fossil.hv4$c$c  reason: collision with other inner class name */
        public static final class C0014c implements mz<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ ContactGroup b;

            @DexIgnore
            public C0014c(c cVar, ContactGroup contactGroup) {
                this.a = cVar;
                this.b = contactGroup;
            }

            @DexIgnore
            public boolean a(mt mtVar, Object obj, yz<Drawable> yzVar, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onLoadFailed");
                Object obj2 = this.b.getContacts().get(0);
                wg6.a(obj2, "contactGroup.contacts[0]");
                ((Contact) obj2).setPhotoThumbUri((String) null);
                return false;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, yz<Drawable> yzVar, pr prVar, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onResourceReady");
                this.a.a.t.setImageDrawable(drawable);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(hv4 hv4, hg4 hg4) {
            super(hg4.d());
            wg6.b(hg4, "binding");
            this.b = hv4;
            this.a = hg4;
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                this.a.q.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.a.u.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.q.setOnClickListener(new a(this));
            this.a.s.setOnClickListener(new b(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v6, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
        /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(ContactGroup contactGroup) {
            Uri uri;
            wg6.b(contactGroup, "contactGroup");
            FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "set favorite contact data");
            if (contactGroup.getContacts().size() > 0) {
                Object obj = contactGroup.getContacts().get(0);
                wg6.a(obj, "contactGroup.contacts[0]");
                if (!TextUtils.isEmpty(((Contact) obj).getPhotoThumbUri())) {
                    Object obj2 = contactGroup.getContacts().get(0);
                    wg6.a(obj2, "contactGroup.contacts[0]");
                    uri = Uri.parse(((Contact) obj2).getPhotoThumbUri());
                } else {
                    uri = null;
                }
                FossilNotificationImageView fossilNotificationImageView = this.a.t;
                wg6.a((Object) fossilNotificationImageView, "binding.ivFavoriteContactIcon");
                mj4 a2 = jj4.a(fossilNotificationImageView.getContext());
                Object obj3 = contactGroup.getContacts().get(0);
                wg6.a(obj3, "contactGroup.contacts[0]");
                lj4 a3 = a2.a((Object) new ij4(uri, ((Contact) obj3).getDisplayName())).a(true).a(ft.a).a((gz) this.b.c);
                FossilNotificationImageView fossilNotificationImageView2 = this.a.t;
                wg6.a((Object) fossilNotificationImageView2, "binding.ivFavoriteContactIcon");
                mj4 a4 = jj4.a(fossilNotificationImageView2.getContext());
                Object obj4 = contactGroup.getContacts().get(0);
                wg6.a(obj4, "contactGroup.contacts[0]");
                lj4 b2 = a3.a(a4.a((Object) new ij4((Uri) null, ((Contact) obj4).getDisplayName())).a((gz) this.b.c)).b(new C0014c(this, contactGroup));
                FossilNotificationImageView fossilNotificationImageView3 = this.a.t;
                wg6.a((Object) fossilNotificationImageView3, "binding.ivFavoriteContactIcon");
                b2.a(fossilNotificationImageView3.getFossilCircleImageView());
                Object obj5 = contactGroup.getContacts().get(0);
                wg6.a(obj5, "contactGroup.contacts[0]");
                if (TextUtils.isEmpty(((Contact) obj5).getPhotoThumbUri())) {
                    this.a.t.a();
                    FossilNotificationImageView fossilNotificationImageView4 = this.a.t;
                    wg6.a((Object) fossilNotificationImageView4, "binding.ivFavoriteContactIcon");
                    fossilNotificationImageView4.setBackground(w6.c(PortfolioApp.get.instance(), 2131231300));
                } else {
                    this.a.t.b();
                    this.a.t.setBackgroundResource(R.color.transparent);
                }
                Object r0 = this.a.r;
                wg6.a((Object) r0, "binding.ftvFavoriteContactName");
                Object obj6 = contactGroup.getContacts().get(0);
                wg6.a(obj6, "contactGroup.contacts[0]");
                r0.setText(((Contact) obj6).getDisplayName());
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public hv4() {
        nz a2 = new nz().a(new xj4());
        wg6.a((Object) a2, "RequestOptions().transform(CircleTransform())");
        this.c = a2;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "holder");
        cVar.a(this.a.get(i));
    }

    @DexIgnore
    public final List<ContactGroup> c() {
        return this.a;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        hg4 a2 = hg4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemFavoriteContactNotif\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public final void a(List<ContactGroup> list) {
        wg6.b(list, "listContactGroup");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0061, code lost:
        if (r3 == ((com.fossil.wearables.fsl.contact.Contact) r5).getContactId()) goto L_0x0065;
     */
    @DexIgnore
    public final void a(ContactGroup contactGroup) {
        wg6.b(contactGroup, "contactGroup");
        Iterator<ContactGroup> it = this.a.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            ContactGroup next = it.next();
            List contacts = next.getContacts();
            wg6.a((Object) contacts, "it.contacts");
            boolean z = true;
            if (!contacts.isEmpty()) {
                List contacts2 = contactGroup.getContacts();
                wg6.a((Object) contacts2, "contactGroup.contacts");
                if (!contacts2.isEmpty()) {
                    Object obj = next.getContacts().get(0);
                    wg6.a(obj, "it.contacts[0]");
                    int contactId = ((Contact) obj).getContactId();
                    Object obj2 = contactGroup.getContacts().get(0);
                    wg6.a(obj2, "contactGroup.contacts[0]");
                }
            }
            z = false;
            if (z) {
                break;
            }
            i++;
        }
        if (i != -1) {
            this.a.remove(i);
            notifyItemRemoved(i);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.b = bVar;
    }
}
