package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v00 extends i86<Void> implements j86 {
    @DexIgnore
    public /* final */ v20 g;
    @DexIgnore
    public /* final */ Collection<? extends i86> h;

    @DexIgnore
    public v00() {
        this(new y00(), new j20(), new v20());
    }

    @DexIgnore
    public static void n() {
        if (o() == null) {
            throw new IllegalStateException("Crashlytics must be initialized by calling Fabric.with(Context) prior to calling Crashlytics.getInstance()");
        }
    }

    @DexIgnore
    public static v00 o() {
        return c86.a(v00.class);
    }

    @DexIgnore
    public Collection<? extends i86> a() {
        return this.h;
    }

    @DexIgnore
    public Void c() {
        return null;
    }

    @DexIgnore
    public String h() {
        return "com.crashlytics.sdk.android:crashlytics";
    }

    @DexIgnore
    public String j() {
        return "2.10.1.34";
    }

    @DexIgnore
    public v00(y00 y00, j20 j20, v20 v20) {
        this.g = v20;
        this.h = Collections.unmodifiableCollection(Arrays.asList(new i86[]{y00, j20, v20}));
    }

    @DexIgnore
    public static void a(int i, String str, String str2) {
        n();
        o().g.b(i, str, str2);
    }
}
