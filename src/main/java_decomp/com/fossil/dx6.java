package com.fossil;

import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dx6<T> {
    @DexIgnore
    void onFailure(Call<T> call, Throwable th);

    @DexIgnore
    void onResponse(Call<T> call, rx6<T> rx6);
}
