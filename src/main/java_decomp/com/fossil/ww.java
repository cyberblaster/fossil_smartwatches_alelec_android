package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww implements zr<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ gw a; // = new gw();

    @DexIgnore
    public boolean a(InputStream inputStream, xr xrVar) throws IOException {
        return true;
    }

    @DexIgnore
    public rt<Bitmap> a(InputStream inputStream, int i, int i2, xr xrVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(h00.a(inputStream)), i, i2, xrVar);
    }
}
