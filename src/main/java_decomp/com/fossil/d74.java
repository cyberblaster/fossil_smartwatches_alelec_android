package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class d74 extends ViewDataBinding {
    @DexIgnore
    public /* final */ LinearLayout A;
    @DexIgnore
    public /* final */ LinearLayout B;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ RecyclerView D;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat E;
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ fg4 u;
    @DexIgnore
    public /* final */ fg4 v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d74(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, View view2, ConstraintLayout constraintLayout, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, fg4 fg4, fg4 fg42, ImageView imageView2, ImageView imageView3, ImageView imageView4, ConstraintLayout constraintLayout2, View view3, ImageView imageView5, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout3, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, View view4) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = imageView;
        this.s = flexibleTextView;
        this.t = rTLImageView;
        this.u = fg4;
        a(this.u);
        this.v = fg42;
        a(this.v);
        this.w = imageView2;
        this.x = imageView3;
        this.y = imageView4;
        this.z = view3;
        this.A = linearLayout;
        this.B = linearLayout2;
        this.C = constraintLayout3;
        this.D = recyclerView;
        this.E = flexibleSwitchCompat;
    }
}
