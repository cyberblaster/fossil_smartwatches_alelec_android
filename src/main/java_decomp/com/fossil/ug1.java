package com.fossil;

import com.fossil.j60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ug1 extends tg6 implements hg6<byte[], j60> {
    @DexIgnore
    public ug1(j60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(j60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyActiveMinuteGoalConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((j60.a) this.receiver).a((byte[]) obj);
    }
}
