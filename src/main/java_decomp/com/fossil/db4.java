package com.fossil;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class db4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public /* final */ ViewPager2 u;
    @DexIgnore
    public /* final */ TabLayout v;
    @DexIgnore
    public /* final */ FlexibleTextView w;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public db4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton, RTLImageView rTLImageView, ProgressBar progressBar, RelativeLayout relativeLayout, ViewPager2 viewPager2, TabLayout tabLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = rTLImageView;
        this.t = relativeLayout;
        this.u = viewPager2;
        this.v = tabLayout;
        this.w = flexibleTextView4;
    }
}
