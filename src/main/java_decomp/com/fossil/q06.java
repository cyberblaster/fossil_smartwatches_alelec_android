package com.fossil;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q06 {
    @DexIgnore
    public static final q06 a = new a();
    @DexIgnore
    public static final q06 b = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements q06 {
        @DexIgnore
        public void a(j06 j06) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements q06 {
        @DexIgnore
        public void a(j06 j06) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new IllegalStateException("Event bus " + j06 + " accessed from non-main thread " + Looper.myLooper());
            }
        }
    }

    @DexIgnore
    void a(j06 j06);
}
