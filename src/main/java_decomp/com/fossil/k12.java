package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k12 {
    @DexIgnore
    public static /* final */ Object a; // = new Object();
    @DexIgnore
    public static k12 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ComponentName c; // = null;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public a(String str, String str2, int i) {
            w12.b(str);
            this.a = str;
            w12.b(str2);
            this.b = str2;
            this.d = i;
        }

        @DexIgnore
        public final ComponentName a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.d;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return u12.a(this.a, aVar.a) && u12.a(this.b, aVar.b) && u12.a(this.c, aVar.c) && this.d == aVar.d;
        }

        @DexIgnore
        public final int hashCode() {
            return u12.a(this.a, this.b, this.c, Integer.valueOf(this.d));
        }

        @DexIgnore
        public final String toString() {
            String str = this.a;
            return str == null ? this.c.flattenToString() : str;
        }

        @DexIgnore
        public final Intent a(Context context) {
            String str = this.a;
            if (str != null) {
                return new Intent(str).setPackage(this.b);
            }
            return new Intent().setComponent(this.c);
        }
    }

    @DexIgnore
    public static k12 a(Context context) {
        synchronized (a) {
            if (b == null) {
                b = new m32(context.getApplicationContext());
            }
        }
        return b;
    }

    @DexIgnore
    public abstract boolean a(a aVar, ServiceConnection serviceConnection, String str);

    @DexIgnore
    public abstract void b(a aVar, ServiceConnection serviceConnection, String str);

    @DexIgnore
    public final void a(String str, String str2, int i, ServiceConnection serviceConnection, String str3) {
        b(new a(str, str2, i), serviceConnection, str3);
    }
}
