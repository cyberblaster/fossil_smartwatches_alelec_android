package com.fossil;

import com.fossil.fn2;
import com.fossil.wi2;
import com.fossil.zi2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi2 extends fn2<vi2, a> implements to2 {
    @DexIgnore
    public static /* final */ vi2 zzi;
    @DexIgnore
    public static volatile yo2<vi2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public nn2<zi2> zze; // = fn2.m();
    @DexIgnore
    public nn2<wi2> zzf; // = fn2.m();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<vi2, a> implements to2 {
        @DexIgnore
        public a() {
            super(vi2.zzi);
        }

        @DexIgnore
        public final zi2 a(int i) {
            return ((vi2) this.b).b(i);
        }

        @DexIgnore
        public final wi2 b(int i) {
            return ((vi2) this.b).c(i);
        }

        @DexIgnore
        public final int j() {
            return ((vi2) this.b).q();
        }

        @DexIgnore
        public final int k() {
            return ((vi2) this.b).s();
        }

        @DexIgnore
        public /* synthetic */ a(bj2 bj2) {
            this();
        }

        @DexIgnore
        public final a a(int i, zi2.a aVar) {
            f();
            ((vi2) this.b).a(i, aVar);
            return this;
        }

        @DexIgnore
        public final a a(int i, wi2.a aVar) {
            f();
            ((vi2) this.b).a(i, aVar);
            return this;
        }
    }

    /*
    static {
        vi2 vi2 = new vi2();
        zzi = vi2;
        fn2.a(vi2.class, vi2);
    }
    */

    @DexIgnore
    public final void a(int i, zi2.a aVar) {
        if (!this.zze.zza()) {
            this.zze = fn2.a(this.zze);
        }
        this.zze.set(i, (zi2) aVar.i());
    }

    @DexIgnore
    public final zi2 b(int i) {
        return this.zze.get(i);
    }

    @DexIgnore
    public final wi2 c(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int o() {
        return this.zzd;
    }

    @DexIgnore
    public final List<zi2> p() {
        return this.zze;
    }

    @DexIgnore
    public final int q() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<wi2> r() {
        return this.zzf;
    }

    @DexIgnore
    public final int s() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void a(int i, wi2.a aVar) {
        if (!this.zzf.zza()) {
            this.zzf = fn2.a(this.zzf);
        }
        this.zzf.set(i, (wi2) aVar.i());
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (bj2.a[i - 1]) {
            case 1:
                return new vi2();
            case 2:
                return new a((bj2) null);
            case 3:
                return fn2.a((ro2) zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001\u0004\u0000\u0002\u001b\u0003\u001b\u0004\u0007\u0001\u0005\u0007\u0002", new Object[]{"zzc", "zzd", "zze", zi2.class, "zzf", wi2.class, "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                yo2<vi2> yo2 = zzj;
                if (yo2 == null) {
                    synchronized (vi2.class) {
                        yo2 = zzj;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzi);
                            zzj = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
