package com.fossil;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x96<V> extends FutureTask<V> implements r96<ba6>, y96, ba6, q96 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public x96(Callable<V> callable) {
        super(callable);
        this.a = b(callable);
    }

    @DexIgnore
    public boolean b() {
        return ((r96) ((y96) d())).b();
    }

    @DexIgnore
    public Collection<ba6> c() {
        return ((r96) ((y96) d())).c();
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return ((y96) d()).compareTo(obj);
    }

    @DexIgnore
    public <T extends r96<ba6> & y96 & ba6> T d() {
        return (r96) this.a;
    }

    @DexIgnore
    public u96 getPriority() {
        return ((y96) d()).getPriority();
    }

    @DexIgnore
    public void a(ba6 ba6) {
        ((r96) ((y96) d())).a(ba6);
    }

    @DexIgnore
    public <T extends r96<ba6> & y96 & ba6> T b(Object obj) {
        if (z96.b(obj)) {
            return (r96) obj;
        }
        return new z96();
    }

    @DexIgnore
    public x96(Runnable runnable, V v) {
        super(runnable, v);
        this.a = b(runnable);
    }

    @DexIgnore
    public void a(boolean z) {
        ((ba6) ((y96) d())).a(z);
    }

    @DexIgnore
    public boolean a() {
        return ((ba6) ((y96) d())).a();
    }

    @DexIgnore
    public void a(Throwable th) {
        ((ba6) ((y96) d())).a(th);
    }
}
