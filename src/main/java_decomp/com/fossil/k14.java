package com.fossil;

import android.content.ContentResolver;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k14 implements Factory<ContentResolver> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public k14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static k14 a(b14 b14) {
        return new k14(b14);
    }

    @DexIgnore
    public static ContentResolver b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static ContentResolver c(b14 b14) {
        ContentResolver e = b14.e();
        z76.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }

    @DexIgnore
    public ContentResolver get() {
        return b(this.a);
    }
}
