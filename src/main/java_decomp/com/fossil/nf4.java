package com.fossil;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nf4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;

    @DexIgnore
    public nf4(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageButton imageButton, ImageView imageView, View view2) {
        super(obj, view, i);
        this.q = constraintLayout;
    }
}
