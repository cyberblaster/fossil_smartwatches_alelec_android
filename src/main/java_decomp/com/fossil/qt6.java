package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt6 implements zt6 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public /* final */ lt6 b;
    @DexIgnore
    public /* final */ Inflater c;
    @DexIgnore
    public /* final */ rt6 d;
    @DexIgnore
    public /* final */ CRC32 e; // = new CRC32();

    @DexIgnore
    public qt6(zt6 zt6) {
        if (zt6 != null) {
            this.c = new Inflater(true);
            this.b = st6.a(zt6);
            this.d = new rt6(this.b, this.c);
            return;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public final void a(jt6 jt6, long j, long j2) {
        vt6 vt6 = jt6.a;
        while (true) {
            int i = vt6.c;
            int i2 = vt6.b;
            if (j < ((long) (i - i2))) {
                break;
            }
            j -= (long) (i - i2);
            vt6 = vt6.f;
        }
        while (j2 > 0) {
            int i3 = (int) (((long) vt6.b) + j);
            int min = (int) Math.min((long) (vt6.c - i3), j2);
            this.e.update(vt6.a, i3, min);
            j2 -= (long) min;
            vt6 = vt6.f;
            j = 0;
        }
    }

    @DexIgnore
    public long b(jt6 jt6, long j) throws IOException {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (i == 0) {
            return 0;
        } else {
            if (this.a == 0) {
                c();
                this.a = 1;
            }
            if (this.a == 1) {
                long j2 = jt6.b;
                long b2 = this.d.b(jt6, j);
                if (b2 != -1) {
                    a(jt6, j2, b2);
                    return b2;
                }
                this.a = 2;
            }
            if (this.a == 2) {
                d();
                this.a = 3;
                if (!this.b.f()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public final void c() throws IOException {
        this.b.i(10);
        byte a2 = this.b.a().a(3);
        boolean z = ((a2 >> 1) & 1) == 1;
        if (z) {
            a(this.b.a(), 0, 10);
        }
        a("ID1ID2", 8075, (int) this.b.readShort());
        this.b.skip(8);
        if (((a2 >> 2) & 1) == 1) {
            this.b.i(2);
            if (z) {
                a(this.b.a(), 0, 2);
            }
            long j = (long) this.b.a().j();
            this.b.i(j);
            if (z) {
                a(this.b.a(), 0, j);
            }
            this.b.skip(j);
        }
        if (((a2 >> 3) & 1) == 1) {
            long a3 = this.b.a((byte) 0);
            if (a3 != -1) {
                if (z) {
                    a(this.b.a(), 0, a3 + 1);
                }
                this.b.skip(a3 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (((a2 >> 4) & 1) == 1) {
            long a4 = this.b.a((byte) 0);
            if (a4 != -1) {
                if (z) {
                    a(this.b.a(), 0, a4 + 1);
                }
                this.b.skip(a4 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (z) {
            a("FHCRC", (int) this.b.j(), (int) (short) ((int) this.e.getValue()));
            this.e.reset();
        }
    }

    @DexIgnore
    public void close() throws IOException {
        this.d.close();
    }

    @DexIgnore
    public final void d() throws IOException {
        a("CRC", this.b.i(), (int) this.e.getValue());
        a("ISIZE", this.b.i(), (int) this.c.getBytesWritten());
    }

    @DexIgnore
    public final void a(String str, int i, int i2) throws IOException {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}));
        }
    }

    @DexIgnore
    public au6 b() {
        return this.b.b();
    }
}
