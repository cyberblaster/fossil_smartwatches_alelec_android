package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rx2 extends IInterface {
    @DexIgnore
    ph2 a(az2 az2) throws RemoteException;

    @DexIgnore
    void a(ty2 ty2) throws RemoteException;

    @DexIgnore
    void a(vy2 vy2) throws RemoteException;

    @DexIgnore
    void a(x52 x52) throws RemoteException;

    @DexIgnore
    void a(x52 x52, my2 my2) throws RemoteException;

    @DexIgnore
    void c(x52 x52) throws RemoteException;

    @DexIgnore
    void clear() throws RemoteException;

    @DexIgnore
    CameraPosition n() throws RemoteException;

    @DexIgnore
    xx2 o() throws RemoteException;
}
