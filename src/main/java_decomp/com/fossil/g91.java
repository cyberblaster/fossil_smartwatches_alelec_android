package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g91 implements Parcelable.Creator<cb1> {
    @DexIgnore
    public /* synthetic */ g91(qg6 qg6) {
    }

    @DexIgnore
    public cb1 createFromParcel(Parcel parcel) {
        return new cb1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new cb1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m19createFromParcel(Parcel parcel) {
        return new cb1(parcel, (qg6) null);
    }
}
