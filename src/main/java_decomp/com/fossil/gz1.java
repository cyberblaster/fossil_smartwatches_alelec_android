package com.fossil;

import android.util.Log;
import com.fossil.ew1;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz1<R extends ew1> extends iw1<R> implements fw1<R> {
    @DexIgnore
    public hw1<? super R, ? extends ew1> a;
    @DexIgnore
    public gz1<? extends ew1> b;
    @DexIgnore
    public volatile gw1<? super R> c;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public Status e;
    @DexIgnore
    public /* final */ WeakReference<wv1> f;
    @DexIgnore
    public /* final */ iz1 g;

    @DexIgnore
    public final void a(Status status) {
        synchronized (this.d) {
            this.e = status;
            b(this.e);
        }
    }

    @DexIgnore
    public final void b(Status status) {
        synchronized (this.d) {
            if (this.a != null) {
                Status a2 = this.a.a(status);
                w12.a(a2, (Object) "onFailure must not return null");
                this.b.a(a2);
            } else if (b()) {
                this.c.a(status);
            }
        }
    }

    @DexIgnore
    public final void onResult(R r) {
        synchronized (this.d) {
            if (!r.o().F()) {
                a(r.o());
                a((ew1) r);
            } else if (this.a != null) {
                bz1.a().submit(new jz1(this, r));
            } else if (b()) {
                this.c.a(r);
            }
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
    }

    @DexIgnore
    public static void a(ew1 ew1) {
        if (ew1 instanceof aw1) {
            try {
                ((aw1) ew1).a();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(ew1);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return (this.c == null || ((wv1) this.f.get()) == null) ? false : true;
    }
}
