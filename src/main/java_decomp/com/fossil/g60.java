package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ bd0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<g60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final g60 a(byte[] bArr) throws IllegalArgumentException {
            bd0 bd0;
            if (bArr.length >= 3) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                short s = order.getShort(0);
                byte b = order.get(2);
                if (bArr.length > 3) {
                    bd0 = bd0.c.a(order.get(3));
                    if (bd0 == null) {
                        StringBuilder b2 = ze0.b("Invalid state: ");
                        b2.append(order.get(3));
                        throw new IllegalArgumentException(b2.toString());
                    }
                } else {
                    bd0 = bd0.NORMAL;
                }
                return new g60(s, b, bd0);
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require as ", "least: 3"));
        }

        @DexIgnore
        public g60 createFromParcel(Parcel parcel) {
            return new g60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new g60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m17createFromParcel(Parcel parcel) {
            return new g60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public g60(short s, byte b2, bd0 bd0) throws IllegalArgumentException {
        super(s60.BATTERY);
        this.b = s;
        this.c = b2;
        this.d = bd0;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b).put(this.c).put(this.d.a()).array();
        wg6.a(array, "ByteBuffer.allocate(MIN_\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        short s = this.b;
        boolean z = true;
        if (s >= 0 && 4400 >= s) {
            byte b2 = this.c;
            if (b2 < 0 || 100 < b2) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(ze0.a(ze0.b("percentage("), this.c, ") is out of range ", "[0, 100]."));
            }
            return;
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("voltage("), this.b, ") is out of range ", "[0, 4400]."));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(g60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            g60 g60 = (g60) obj;
            return this.b == g60.b && this.c == g60.c && this.d == g60.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    public final byte getPercentage() {
        return this.c;
    }

    @DexIgnore
    public final bd0 getState() {
        return this.d;
    }

    @DexIgnore
    public final short getVoltage() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Byte.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode + (this.b * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.b));
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.VOLTAGE, (Object) Short.valueOf(this.b));
            cw0.a(jSONObject, bm0.PERCENTAGE, (Object) Byte.valueOf(this.c));
            cw0.a(jSONObject, bm0.STATE, (Object) this.d);
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ g60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = (short) parcel.readInt();
        this.c = parcel.readByte();
        bd0 a2 = bd0.c.a(parcel.readByte());
        if (a2 != null) {
            this.d = a2;
            e();
            return;
        }
        wg6.a();
        throw null;
    }
}
