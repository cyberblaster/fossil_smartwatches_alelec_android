package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.TimeZone;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os0 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ hu0 a;

    @DexIgnore
    public os0(hu0 hu0) {
        this.a = hu0;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        sl0 sl0;
        long currentTimeMillis = System.currentTimeMillis();
        long j = (long) 1000;
        long j2 = currentTimeMillis / j;
        long j3 = currentTimeMillis - (j * j2);
        int offset = (TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60;
        oa1 oa1 = oa1.a;
        String str = this.a.d;
        Object[] objArr = {Long.valueOf(j2), Long.valueOf(j3), Integer.valueOf(offset)};
        qs0.h.a(new nn0(cw0.a((Enum<?>) ju0.TIME_CHANGED), og0.SYSTEM_EVENT, "", "", ze0.a("UUID.randomUUID().toString()"), true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.SECOND, (Object) Long.valueOf(j2)), bm0.MILLISECOND, (Object) Long.valueOf(j3)), bm0.TIMEZONE_OFFSET_IN_MINUTE, (Object) Integer.valueOf(offset)), 448));
        String action = intent.getAction();
        if (action != null) {
            int hashCode = action.hashCode();
            if (hashCode != 502473491) {
                if (hashCode == 505380757 && action.equals("android.intent.action.TIME_SET")) {
                    sl0 = sl0.ADJUST_MANUAL;
                    this.a.a(System.currentTimeMillis(), sl0);
                }
            } else if (action.equals("android.intent.action.TIMEZONE_CHANGED")) {
                sl0 = sl0.ADJUST_TIMEZONE;
                this.a.a(System.currentTimeMillis(), sl0);
            }
        }
        sl0 = sl0.ADJUST_NONE;
        this.a.a(System.currentTimeMillis(), sl0);
    }
}
