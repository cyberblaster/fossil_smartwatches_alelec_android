package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no2 implements ko2 {
    @DexIgnore
    public final io2<?, ?> a(Object obj) {
        jo2 jo2 = (jo2) obj;
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final Object b(Object obj) {
        return lo2.zza().zzb();
    }

    @DexIgnore
    public final Map<?, ?> zza(Object obj) {
        return (lo2) obj;
    }

    @DexIgnore
    public final Map<?, ?> zzb(Object obj) {
        return (lo2) obj;
    }

    @DexIgnore
    public final boolean zzc(Object obj) {
        return !((lo2) obj).zzd();
    }

    @DexIgnore
    public final Object zzd(Object obj) {
        ((lo2) obj).zzc();
        return obj;
    }

    @DexIgnore
    public final Object zza(Object obj, Object obj2) {
        lo2 lo2 = (lo2) obj;
        lo2 lo22 = (lo2) obj2;
        if (!lo22.isEmpty()) {
            if (!lo2.zzd()) {
                lo2 = lo2.zzb();
            }
            lo2.zza(lo22);
        }
        return lo2;
    }

    @DexIgnore
    public final int zza(int i, Object obj, Object obj2) {
        lo2 lo2 = (lo2) obj;
        jo2 jo2 = (jo2) obj2;
        if (lo2.isEmpty()) {
            return 0;
        }
        Iterator it = lo2.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }
}
