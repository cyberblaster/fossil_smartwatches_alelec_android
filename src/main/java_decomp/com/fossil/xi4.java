package com.fossil;

import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi4 {
    @DexIgnore
    public static final String a(String str, String str2) {
        wg6.b(str2, DatabaseFieldConfigLoader.FIELD_NAME_DEFAULT_VALUE);
        return str == null || xj6.a(str) ? str2 : str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0026  */
    public static final UserDisplayUnit a(MFUser mFUser) {
        UserDisplayUnit.TemperatureUnit temperatureUnit;
        zh4 distanceUnit;
        UserDisplayUnit.DistanceUnit distanceUnit2;
        if (mFUser == null) {
            return null;
        }
        zh4 temperatureUnit2 = mFUser.getTemperatureUnit();
        if (temperatureUnit2 != null) {
            int i = wi4.a[temperatureUnit2.ordinal()];
            if (i == 1) {
                temperatureUnit = UserDisplayUnit.TemperatureUnit.C;
            } else if (i == 2) {
                temperatureUnit = UserDisplayUnit.TemperatureUnit.F;
            }
            distanceUnit = mFUser.getDistanceUnit();
            if (distanceUnit != null) {
                int i2 = wi4.b[distanceUnit.ordinal()];
                if (i2 == 1) {
                    distanceUnit2 = UserDisplayUnit.DistanceUnit.KM;
                } else if (i2 == 2) {
                    distanceUnit2 = UserDisplayUnit.DistanceUnit.MILE;
                }
                return new UserDisplayUnit(temperatureUnit, distanceUnit2);
            }
            distanceUnit2 = UserDisplayUnit.DistanceUnit.KM;
            return new UserDisplayUnit(temperatureUnit, distanceUnit2);
        }
        temperatureUnit = UserDisplayUnit.TemperatureUnit.C;
        distanceUnit = mFUser.getDistanceUnit();
        if (distanceUnit != null) {
        }
        distanceUnit2 = UserDisplayUnit.DistanceUnit.KM;
        return new UserDisplayUnit(temperatureUnit, distanceUnit2);
    }
}
