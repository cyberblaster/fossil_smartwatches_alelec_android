package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import com.fossil.am;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mm implements Runnable {
    @DexIgnore
    public static /* final */ String w; // = tl.a("WorkerWrapper");
    @DexIgnore
    public Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public List<hm> c;
    @DexIgnore
    public WorkerParameters.a d;
    @DexIgnore
    public zn e;
    @DexIgnore
    public ListenableWorker f;
    @DexIgnore
    public ListenableWorker.a g; // = ListenableWorker.a.a();
    @DexIgnore
    public ml h;
    @DexIgnore
    public to i;
    @DexIgnore
    public WorkDatabase j;
    @DexIgnore
    public ao o;
    @DexIgnore
    public rn p;
    @DexIgnore
    public Cdo q;
    @DexIgnore
    public List<String> r;
    @DexIgnore
    public String s;
    @DexIgnore
    public so<Boolean> t; // = so.e();
    @DexIgnore
    public ip3<ListenableWorker.a> u; // = null;
    @DexIgnore
    public volatile boolean v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ so a;

        @DexIgnore
        public a(so soVar) {
            this.a = soVar;
        }

        @DexIgnore
        public void run() {
            try {
                tl.a().a(mm.w, String.format("Starting work for %s", new Object[]{mm.this.e.c}), new Throwable[0]);
                mm.this.u = mm.this.f.j();
                this.a.a(mm.this.u);
            } catch (Throwable th) {
                this.a.a(th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ so a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public b(so soVar, String str) {
            this.a = soVar;
            this.b = str;
        }

        @DexIgnore
        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                ListenableWorker.a aVar = (ListenableWorker.a) this.a.get();
                if (aVar == null) {
                    tl.a().b(mm.w, String.format("%s returned a null result. Treating it as a failure.", new Object[]{mm.this.e.c}), new Throwable[0]);
                } else {
                    tl.a().a(mm.w, String.format("%s returned a %s result.", new Object[]{mm.this.e.c, aVar}), new Throwable[0]);
                    mm.this.g = aVar;
                }
            } catch (CancellationException e) {
                tl.a().c(mm.w, String.format("%s was cancelled", new Object[]{this.b}), e);
            } catch (InterruptedException | ExecutionException e2) {
                tl.a().b(mm.w, String.format("%s failed because it threw an exception/error", new Object[]{this.b}), e2);
            } catch (Throwable th) {
                mm.this.b();
                throw th;
            }
            mm.this.b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public Context a;
        @DexIgnore
        public ListenableWorker b;
        @DexIgnore
        public to c;
        @DexIgnore
        public ml d;
        @DexIgnore
        public WorkDatabase e;
        @DexIgnore
        public String f;
        @DexIgnore
        public List<hm> g;
        @DexIgnore
        public WorkerParameters.a h; // = new WorkerParameters.a();

        @DexIgnore
        public c(Context context, ml mlVar, to toVar, WorkDatabase workDatabase, String str) {
            this.a = context.getApplicationContext();
            this.c = toVar;
            this.d = mlVar;
            this.e = workDatabase;
            this.f = str;
        }

        @DexIgnore
        public c a(List<hm> list) {
            this.g = list;
            return this;
        }

        @DexIgnore
        public c a(WorkerParameters.a aVar) {
            if (aVar != null) {
                this.h = aVar;
            }
            return this;
        }

        @DexIgnore
        public mm a() {
            return new mm(this);
        }
    }

    @DexIgnore
    public mm(c cVar) {
        this.a = cVar.a;
        this.i = cVar.c;
        this.b = cVar.f;
        this.c = cVar.g;
        this.d = cVar.h;
        this.f = cVar.b;
        this.h = cVar.d;
        this.j = cVar.e;
        this.o = this.j.d();
        this.p = this.j.a();
        this.q = this.j.e();
    }

    @DexIgnore
    public ip3<Boolean> a() {
        return this.t;
    }

    @DexIgnore
    public void b() {
        boolean z = false;
        if (!i()) {
            this.j.beginTransaction();
            try {
                am.a d2 = this.o.d(this.b);
                if (d2 == null) {
                    b(false);
                    z = true;
                } else if (d2 == am.a.RUNNING) {
                    a(this.g);
                    z = this.o.d(this.b).isFinished();
                } else if (!d2.isFinished()) {
                    c();
                }
                this.j.setTransactionSuccessful();
            } finally {
                this.j.endTransaction();
            }
        }
        List<hm> list = this.c;
        if (list != null) {
            if (z) {
                for (hm a2 : list) {
                    a2.a(this.b);
                }
            }
            im.a(this.h, this.j, this.c);
        }
    }

    @DexIgnore
    public final void c() {
        this.j.beginTransaction();
        try {
            this.o.a(am.a.ENQUEUED, this.b);
            this.o.b(this.b, System.currentTimeMillis());
            this.o.a(this.b, -1);
            this.j.setTransactionSuccessful();
        } finally {
            this.j.endTransaction();
            b(true);
        }
    }

    @DexIgnore
    public final void d() {
        this.j.beginTransaction();
        try {
            this.o.b(this.b, System.currentTimeMillis());
            this.o.a(am.a.ENQUEUED, this.b);
            this.o.f(this.b);
            this.o.a(this.b, -1);
            this.j.setTransactionSuccessful();
        } finally {
            this.j.endTransaction();
            b(false);
        }
    }

    @DexIgnore
    public final void e() {
        am.a d2 = this.o.d(this.b);
        if (d2 == am.a.RUNNING) {
            tl.a().a(w, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", new Object[]{this.b}), new Throwable[0]);
            b(true);
            return;
        }
        tl.a().a(w, String.format("Status for %s is %s; not doing any work", new Object[]{this.b, d2}), new Throwable[0]);
        b(false);
    }

    @DexIgnore
    public final void f() {
        pl a2;
        if (!i()) {
            this.j.beginTransaction();
            try {
                this.e = this.o.e(this.b);
                if (this.e == null) {
                    tl.a().b(w, String.format("Didn't find WorkSpec for id %s", new Object[]{this.b}), new Throwable[0]);
                    b(false);
                } else if (this.e.b != am.a.ENQUEUED) {
                    e();
                    this.j.setTransactionSuccessful();
                    tl.a().a(w, String.format("%s is not in ENQUEUED state. Nothing more to do.", new Object[]{this.e.c}), new Throwable[0]);
                    this.j.endTransaction();
                } else {
                    if (this.e.d() || this.e.c()) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (!(this.e.n == 0) && currentTimeMillis < this.e.a()) {
                            tl.a().a(w, String.format("Delaying execution for %s because it is being executed before schedule.", new Object[]{this.e.c}), new Throwable[0]);
                            b(true);
                            this.j.endTransaction();
                            return;
                        }
                    }
                    this.j.setTransactionSuccessful();
                    this.j.endTransaction();
                    if (this.e.d()) {
                        a2 = this.e.e;
                    } else {
                        rl b2 = this.h.c().b(this.e.d);
                        if (b2 == null) {
                            tl.a().b(w, String.format("Could not create Input Merger %s", new Object[]{this.e.d}), new Throwable[0]);
                            g();
                            return;
                        }
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(this.e.e);
                        arrayList.addAll(this.o.g(this.b));
                        a2 = b2.a((List<pl>) arrayList);
                    }
                    WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.b), a2, this.r, this.d, this.e.k, this.h.b(), this.i, this.h.i(), new po(this.j, this.i));
                    if (this.f == null) {
                        this.f = this.h.i().b(this.a, this.e.c, workerParameters);
                    }
                    ListenableWorker listenableWorker = this.f;
                    if (listenableWorker == null) {
                        tl.a().b(w, String.format("Could not create Worker %s", new Object[]{this.e.c}), new Throwable[0]);
                        g();
                    } else if (listenableWorker.g()) {
                        tl.a().b(w, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", new Object[]{this.e.c}), new Throwable[0]);
                        g();
                    } else {
                        this.f.i();
                        if (!j()) {
                            e();
                        } else if (!i()) {
                            so e2 = so.e();
                            this.i.a().execute(new a(e2));
                            e2.a((Runnable) new b(e2, this.s), (Executor) this.i.b());
                        }
                    }
                }
            } finally {
                this.j.endTransaction();
            }
        }
    }

    @DexIgnore
    public void g() {
        this.j.beginTransaction();
        try {
            a(this.b);
            this.o.a(this.b, ((ListenableWorker.a.C0006a) this.g).d());
            this.j.setTransactionSuccessful();
        } finally {
            this.j.endTransaction();
            b(false);
        }
    }

    @DexIgnore
    public final void h() {
        this.j.beginTransaction();
        try {
            this.o.a(am.a.SUCCEEDED, this.b);
            this.o.a(this.b, ((ListenableWorker.a.c) this.g).d());
            long currentTimeMillis = System.currentTimeMillis();
            for (String next : this.p.a(this.b)) {
                if (this.o.d(next) == am.a.BLOCKED && this.p.b(next)) {
                    tl.a().c(w, String.format("Setting status to enqueued for %s", new Object[]{next}), new Throwable[0]);
                    this.o.a(am.a.ENQUEUED, next);
                    this.o.b(next, currentTimeMillis);
                }
            }
            this.j.setTransactionSuccessful();
        } finally {
            this.j.endTransaction();
            b(false);
        }
    }

    @DexIgnore
    public final boolean i() {
        if (!this.v) {
            return false;
        }
        tl.a().a(w, String.format("Work interrupted for %s", new Object[]{this.s}), new Throwable[0]);
        am.a d2 = this.o.d(this.b);
        if (d2 == null) {
            b(false);
        } else {
            b(!d2.isFinished());
        }
        return true;
    }

    @DexIgnore
    public final boolean j() {
        this.j.beginTransaction();
        try {
            boolean z = true;
            if (this.o.d(this.b) == am.a.ENQUEUED) {
                this.o.a(am.a.RUNNING, this.b);
                this.o.h(this.b);
            } else {
                z = false;
            }
            this.j.setTransactionSuccessful();
            return z;
        } finally {
            this.j.endTransaction();
        }
    }

    @DexIgnore
    public void run() {
        this.r = this.q.a(this.b);
        this.s = a(this.r);
        f();
    }

    @DexIgnore
    public void a(boolean z) {
        this.v = true;
        i();
        ip3<ListenableWorker.a> ip3 = this.u;
        if (ip3 != null) {
            ip3.cancel(true);
        }
        ListenableWorker listenableWorker = this.f;
        if (listenableWorker != null) {
            listenableWorker.k();
        }
    }

    @DexIgnore
    public final void a(ListenableWorker.a aVar) {
        if (aVar instanceof ListenableWorker.a.c) {
            tl.a().c(w, String.format("Worker result SUCCESS for %s", new Object[]{this.s}), new Throwable[0]);
            if (this.e.d()) {
                d();
            } else {
                h();
            }
        } else if (aVar instanceof ListenableWorker.a.b) {
            tl.a().c(w, String.format("Worker result RETRY for %s", new Object[]{this.s}), new Throwable[0]);
            c();
        } else {
            tl.a().c(w, String.format("Worker result FAILURE for %s", new Object[]{this.s}), new Throwable[0]);
            if (this.e.d()) {
                d();
            } else {
                g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e A[Catch:{ all -> 0x0039 }] */
    public final void b(boolean z) {
        boolean z2;
        this.j.beginTransaction();
        try {
            List<String> c2 = this.j.d().c();
            if (c2 != null) {
                if (!c2.isEmpty()) {
                    z2 = false;
                    if (z2) {
                        jo.a(this.a, RescheduleReceiver.class, false);
                    }
                    this.j.setTransactionSuccessful();
                    this.j.endTransaction();
                    this.t.b(Boolean.valueOf(z));
                }
            }
            z2 = true;
            if (z2) {
            }
            this.j.setTransactionSuccessful();
            this.j.endTransaction();
            this.t.b(Boolean.valueOf(z));
        } catch (Throwable th) {
            this.j.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            if (this.o.d(str2) != am.a.CANCELLED) {
                this.o.a(am.a.FAILED, str2);
            }
            linkedList.addAll(this.p.a(str2));
        }
    }

    @DexIgnore
    public final String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.b);
        sb.append(", tags={ ");
        boolean z = true;
        for (String next : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(next);
        }
        sb.append(" } ]");
        return sb.toString();
    }
}
