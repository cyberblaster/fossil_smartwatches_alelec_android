package com.fossil;

import android.os.Bundle;
import com.fossil.rv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gy1 {
    @DexIgnore
    <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t);

    @DexIgnore
    void a(gv1 gv1, rv1<?> rv1, boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void f(Bundle bundle);

    @DexIgnore
    void g(int i);
}
