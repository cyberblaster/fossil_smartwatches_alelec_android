package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.i5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j5 {
    @DexIgnore
    public static float j0; // = 0.5f;
    @DexIgnore
    public i5[] A; // = {this.s, this.u, this.t, this.v, this.w, this.z};
    @DexIgnore
    public ArrayList<i5> B; // = new ArrayList<>();
    @DexIgnore
    public b[] C;
    @DexIgnore
    public j5 D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public float G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public int Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public float W;
    @DexIgnore
    public Object X;
    @DexIgnore
    public int Y;
    @DexIgnore
    public String Z;
    @DexIgnore
    public int a; // = -1;
    @DexIgnore
    public String a0;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public boolean b0;
    @DexIgnore
    public r5 c;
    @DexIgnore
    public boolean c0;
    @DexIgnore
    public r5 d;
    @DexIgnore
    public boolean d0;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int e0;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public int f0;
    @DexIgnore
    public int[] g; // = new int[2];
    @DexIgnore
    public float[] g0;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public j5[] h0;
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public j5[] i0;
    @DexIgnore
    public float j; // = 1.0f;
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public int l; // = 0;
    @DexIgnore
    public float m; // = 1.0f;
    @DexIgnore
    public int n; // = -1;
    @DexIgnore
    public float o; // = 1.0f;
    @DexIgnore
    public l5 p; // = null;
    @DexIgnore
    public int[] q; // = {Integer.MAX_VALUE, Integer.MAX_VALUE};
    @DexIgnore
    public float r; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public i5 s; // = new i5(this, i5.d.LEFT);
    @DexIgnore
    public i5 t; // = new i5(this, i5.d.TOP);
    @DexIgnore
    public i5 u; // = new i5(this, i5.d.RIGHT);
    @DexIgnore
    public i5 v; // = new i5(this, i5.d.BOTTOM);
    @DexIgnore
    public i5 w; // = new i5(this, i5.d.BASELINE);
    @DexIgnore
    public i5 x; // = new i5(this, i5.d.CENTER_X);
    @DexIgnore
    public i5 y; // = new i5(this, i5.d.CENTER_Y);
    @DexIgnore
    public i5 z; // = new i5(this, i5.d.CENTER);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[i5.d.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(3:33|34|36)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(3:33|34|36)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0093 */
        /*
        static {
            try {
                b[b.FIXED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[b.WRAP_CONTENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[b.MATCH_PARENT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[b.MATCH_CONSTRAINT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            a[i5.d.LEFT.ordinal()] = 1;
            a[i5.d.TOP.ordinal()] = 2;
            a[i5.d.RIGHT.ordinal()] = 3;
            a[i5.d.BOTTOM.ordinal()] = 4;
            a[i5.d.BASELINE.ordinal()] = 5;
            a[i5.d.CENTER.ordinal()] = 6;
            a[i5.d.CENTER_X.ordinal()] = 7;
            a[i5.d.CENTER_Y.ordinal()] = 8;
            try {
                a[i5.d.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError unused5) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        FIXED,
        WRAP_CONTENT,
        MATCH_CONSTRAINT,
        MATCH_PARENT
    }

    @DexIgnore
    public j5() {
        b bVar = b.FIXED;
        this.C = new b[]{bVar, bVar};
        this.D = null;
        this.E = 0;
        this.F = 0;
        this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        this.Y = 0;
        this.Z = null;
        this.a0 = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
        this.e0 = 0;
        this.f0 = 0;
        this.g0 = new float[]{-1.0f, -1.0f};
        this.h0 = new j5[]{null, null};
        this.i0 = new j5[]{null, null};
        a();
    }

    @DexIgnore
    public boolean A() {
        i5 i5Var = this.s;
        i5 i5Var2 = i5Var.d;
        if (i5Var2 != null && i5Var2.d == i5Var) {
            return true;
        }
        i5 i5Var3 = this.u;
        i5 i5Var4 = i5Var3.d;
        return i5Var4 != null && i5Var4.d == i5Var3;
    }

    @DexIgnore
    public boolean B() {
        i5 i5Var = this.t;
        i5 i5Var2 = i5Var.d;
        if (i5Var2 != null && i5Var2.d == i5Var) {
            return true;
        }
        i5 i5Var3 = this.v;
        i5 i5Var4 = i5Var3.d;
        return i5Var4 != null && i5Var4.d == i5Var3;
    }

    @DexIgnore
    public boolean C() {
        return this.f == 0 && this.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.k == 0 && this.l == 0 && this.C[1] == b.MATCH_CONSTRAINT;
    }

    @DexIgnore
    public boolean D() {
        return this.e == 0 && this.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.h == 0 && this.i == 0 && this.C[0] == b.MATCH_CONSTRAINT;
    }

    @DexIgnore
    public void E() {
        this.s.j();
        this.t.j();
        this.u.j();
        this.v.j();
        this.w.j();
        this.x.j();
        this.y.j();
        this.z.j();
        this.D = null;
        this.r = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.E = 0;
        this.F = 0;
        this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        this.R = 0;
        this.S = 0;
        this.T = 0;
        this.U = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        b[] bVarArr = this.C;
        b bVar = b.FIXED;
        bVarArr[0] = bVar;
        bVarArr[1] = bVar;
        this.X = null;
        this.Y = 0;
        this.a0 = null;
        this.e0 = 0;
        this.f0 = 0;
        float[] fArr = this.g0;
        fArr[0] = -1.0f;
        fArr[1] = -1.0f;
        this.a = -1;
        this.b = -1;
        int[] iArr = this.q;
        iArr[0] = Integer.MAX_VALUE;
        iArr[1] = Integer.MAX_VALUE;
        this.e = 0;
        this.f = 0;
        this.j = 1.0f;
        this.m = 1.0f;
        this.i = Integer.MAX_VALUE;
        this.l = Integer.MAX_VALUE;
        this.h = 0;
        this.k = 0;
        this.n = -1;
        this.o = 1.0f;
        r5 r5Var = this.c;
        if (r5Var != null) {
            r5Var.d();
        }
        r5 r5Var2 = this.d;
        if (r5Var2 != null) {
            r5Var2.d();
        }
        this.p = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
    }

    @DexIgnore
    public void F() {
        j5 l2 = l();
        if (l2 == null || !(l2 instanceof k5) || !((k5) l()).O()) {
            int size = this.B.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.B.get(i2).j();
            }
        }
    }

    @DexIgnore
    public void G() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].d().d();
        }
    }

    @DexIgnore
    public void H() {
    }

    @DexIgnore
    public void I() {
        int i2 = this.I;
        int i3 = this.J;
        this.M = i2;
        this.N = i3;
    }

    @DexIgnore
    public void J() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].d().g();
        }
    }

    @DexIgnore
    public void a(int i2) {
        o5.a(i2, this);
    }

    @DexIgnore
    public void a(boolean z2) {
    }

    @DexIgnore
    public void b(z4 z4Var) {
        z4Var.a((Object) this.s);
        z4Var.a((Object) this.t);
        z4Var.a((Object) this.u);
        z4Var.a((Object) this.v);
        if (this.Q > 0) {
            z4Var.a((Object) this.w);
        }
    }

    @DexIgnore
    public void b(boolean z2) {
    }

    @DexIgnore
    public ArrayList<i5> c() {
        return this.B;
    }

    @DexIgnore
    public int d(int i2) {
        if (i2 == 0) {
            return t();
        }
        if (i2 == 1) {
            return j();
        }
        return 0;
    }

    @DexIgnore
    public int e() {
        return x() + this.F;
    }

    @DexIgnore
    public Object f() {
        return this.X;
    }

    @DexIgnore
    public String g() {
        return this.Z;
    }

    @DexIgnore
    public int h() {
        return this.M + this.O;
    }

    @DexIgnore
    public int i() {
        return this.N + this.P;
    }

    @DexIgnore
    public void j(int i2) {
        this.q[1] = i2;
    }

    @DexIgnore
    public void k(int i2) {
        this.q[0] = i2;
    }

    @DexIgnore
    public j5 l() {
        return this.D;
    }

    @DexIgnore
    public r5 m() {
        if (this.d == null) {
            this.d = new r5();
        }
        return this.d;
    }

    @DexIgnore
    public r5 n() {
        if (this.c == null) {
            this.c = new r5();
        }
        return this.c;
    }

    @DexIgnore
    public void o(int i2) {
        this.Y = i2;
    }

    @DexIgnore
    public int p() {
        return this.I + this.O;
    }

    @DexIgnore
    public int q() {
        return this.J + this.P;
    }

    @DexIgnore
    public void r(int i2) {
        this.T = i2;
    }

    @DexIgnore
    public int s() {
        return this.Y;
    }

    @DexIgnore
    public int t() {
        if (this.Y == 8) {
            return 0;
        }
        return this.E;
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (this.a0 != null) {
            str = "type: " + this.a0 + " ";
        } else {
            str = str2;
        }
        sb.append(str);
        if (this.Z != null) {
            str2 = "id: " + this.Z + " ";
        }
        sb.append(str2);
        sb.append("(");
        sb.append(this.I);
        sb.append(", ");
        sb.append(this.J);
        sb.append(") - (");
        sb.append(this.E);
        sb.append(" x ");
        sb.append(this.F);
        sb.append(") wrap: (");
        sb.append(this.T);
        sb.append(" x ");
        sb.append(this.U);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public int u() {
        return this.U;
    }

    @DexIgnore
    public int v() {
        return this.T;
    }

    @DexIgnore
    public int w() {
        return this.I;
    }

    @DexIgnore
    public int x() {
        return this.J;
    }

    @DexIgnore
    public boolean y() {
        return this.Q > 0;
    }

    @DexIgnore
    public boolean z() {
        if (this.s.d().b == 1 && this.u.d().b == 1 && this.t.d().b == 1 && this.v.d().b == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(x4 x4Var) {
        this.s.a(x4Var);
        this.t.a(x4Var);
        this.u.a(x4Var);
        this.v.a(x4Var);
        this.w.a(x4Var);
        this.z.a(x4Var);
        this.x.a(x4Var);
        this.y.a(x4Var);
    }

    @DexIgnore
    public void c(int i2, int i3) {
        this.I = i2;
        this.J = i3;
    }

    @DexIgnore
    public void e(int i2, int i3) {
        this.J = i2;
        this.F = i3 - i2;
        int i4 = this.F;
        int i5 = this.S;
        if (i4 < i5) {
            this.F = i5;
        }
    }

    @DexIgnore
    public final boolean f(int i2) {
        int i3 = i2 * 2;
        i5[] i5VarArr = this.A;
        if (!(i5VarArr[i3].d == null || i5VarArr[i3].d.d == i5VarArr[i3])) {
            int i4 = i3 + 1;
            return i5VarArr[i4].d != null && i5VarArr[i4].d.d == i5VarArr[i4];
        }
    }

    @DexIgnore
    public void g(int i2) {
        this.Q = i2;
    }

    @DexIgnore
    public void h(int i2) {
        this.F = i2;
        int i3 = this.F;
        int i4 = this.S;
        if (i3 < i4) {
            this.F = i4;
        }
    }

    @DexIgnore
    public void i(int i2) {
        this.e0 = i2;
    }

    @DexIgnore
    public int j() {
        if (this.Y == 8) {
            return 0;
        }
        return this.F;
    }

    @DexIgnore
    public b k() {
        return this.C[0];
    }

    @DexIgnore
    public void l(int i2) {
        if (i2 < 0) {
            this.S = 0;
        } else {
            this.S = i2;
        }
    }

    @DexIgnore
    public int o() {
        return w() + this.E;
    }

    @DexIgnore
    public void p(int i2) {
        this.E = i2;
        int i3 = this.E;
        int i4 = this.R;
        if (i3 < i4) {
            this.E = i4;
        }
    }

    @DexIgnore
    public void q(int i2) {
        this.U = i2;
    }

    @DexIgnore
    public b r() {
        return this.C[1];
    }

    @DexIgnore
    public void s(int i2) {
        this.I = i2;
    }

    @DexIgnore
    public int d() {
        return this.Q;
    }

    @DexIgnore
    public void t(int i2) {
        this.J = i2;
    }

    @DexIgnore
    public void c(float f2) {
        this.W = f2;
    }

    @DexIgnore
    public void d(int i2, int i3) {
        if (i3 == 0) {
            this.K = i2;
        } else if (i3 == 1) {
            this.L = i2;
        }
    }

    @DexIgnore
    public void m(int i2) {
        if (i2 < 0) {
            this.R = 0;
        } else {
            this.R = i2;
        }
    }

    @DexIgnore
    public void n(int i2) {
        this.f0 = i2;
    }

    @DexIgnore
    public b c(int i2) {
        if (i2 == 0) {
            return k();
        }
        if (i2 == 1) {
            return r();
        }
        return null;
    }

    @DexIgnore
    public void d(float f2) {
        this.g0[1] = f2;
    }

    @DexIgnore
    public int e(int i2) {
        if (i2 == 0) {
            return this.K;
        }
        if (i2 == 1) {
            return this.L;
        }
        return 0;
    }

    @DexIgnore
    public float b(int i2) {
        if (i2 == 0) {
            return this.V;
        }
        if (i2 == 1) {
            return this.W;
        }
        return -1.0f;
    }

    @DexIgnore
    public void c(z4 z4Var) {
        int b2 = z4Var.b((Object) this.s);
        int b3 = z4Var.b((Object) this.t);
        int b4 = z4Var.b((Object) this.u);
        int b5 = z4Var.b((Object) this.v);
        int i2 = b5 - b3;
        if (b4 - b2 < 0 || i2 < 0 || b2 == Integer.MIN_VALUE || b2 == Integer.MAX_VALUE || b3 == Integer.MIN_VALUE || b3 == Integer.MAX_VALUE || b4 == Integer.MIN_VALUE || b4 == Integer.MAX_VALUE || b5 == Integer.MIN_VALUE || b5 == Integer.MAX_VALUE) {
            b5 = 0;
            b2 = 0;
            b3 = 0;
            b4 = 0;
        }
        a(b2, b3, b4, b5);
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.O = i2;
        this.P = i3;
    }

    @DexIgnore
    public final void a() {
        this.B.add(this.s);
        this.B.add(this.t);
        this.B.add(this.u);
        this.B.add(this.v);
        this.B.add(this.x);
        this.B.add(this.y);
        this.B.add(this.z);
        this.B.add(this.w);
    }

    @DexIgnore
    public void b(int i2, int i3, int i4, float f2) {
        this.f = i2;
        this.k = i3;
        this.l = i4;
        this.m = f2;
        if (f2 < 1.0f && this.f == 0) {
            this.f = 2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    public void b(String str) {
        float f2;
        if (str == null || str.length() == 0) {
            this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            return;
        }
        int i2 = -1;
        int length = str.length();
        int indexOf = str.indexOf(44);
        int i3 = 0;
        if (indexOf > 0 && indexOf < length - 1) {
            String substring = str.substring(0, indexOf);
            if (substring.equalsIgnoreCase("W")) {
                i2 = 0;
            } else if (substring.equalsIgnoreCase("H")) {
                i2 = 1;
            }
            i3 = indexOf + 1;
        }
        int indexOf2 = str.indexOf(58);
        if (indexOf2 < 0 || indexOf2 >= length - 1) {
            String substring2 = str.substring(i3);
            if (substring2.length() > 0) {
                f2 = Float.parseFloat(substring2);
                if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    this.G = f2;
                    this.H = i2;
                    return;
                }
                return;
            }
        } else {
            String substring3 = str.substring(i3, indexOf2);
            String substring4 = str.substring(indexOf2 + 1);
            if (substring3.length() > 0 && substring4.length() > 0) {
                try {
                    float parseFloat = Float.parseFloat(substring3);
                    float parseFloat2 = Float.parseFloat(substring4);
                    if (parseFloat > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && parseFloat2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        f2 = i2 == 1 ? Math.abs(parseFloat2 / parseFloat) : Math.abs(parseFloat / parseFloat2);
                        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        }
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
        }
    }

    @DexIgnore
    public void a(j5 j5Var) {
        this.D = j5Var;
    }

    @DexIgnore
    public void a(j5 j5Var, float f2, int i2) {
        i5.d dVar = i5.d.CENTER;
        a(dVar, j5Var, dVar, i2, 0);
        this.r = f2;
    }

    @DexIgnore
    public void a(String str) {
        this.Z = str;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, float f2) {
        this.e = i2;
        this.h = i3;
        this.i = i4;
        this.j = f2;
        if (f2 < 1.0f && this.e == 0) {
            this.e = 2;
        }
    }

    @DexIgnore
    public void a(float f2) {
        this.V = f2;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8 = i4 - i2;
        int i9 = i5 - i3;
        this.I = i2;
        this.J = i3;
        if (this.Y == 8) {
            this.E = 0;
            this.F = 0;
            return;
        }
        if (this.C[0] != b.FIXED || i8 >= (i6 = this.E)) {
            i6 = i8;
        }
        if (this.C[1] != b.FIXED || i9 >= (i7 = this.F)) {
            i7 = i9;
        }
        this.E = i6;
        this.F = i7;
        int i10 = this.F;
        int i11 = this.S;
        if (i10 < i11) {
            this.F = i11;
        }
        int i12 = this.E;
        int i13 = this.R;
        if (i12 < i13) {
            this.E = i13;
        }
        this.c0 = true;
    }

    @DexIgnore
    public void b(float f2) {
        this.g0[0] = f2;
    }

    @DexIgnore
    public boolean b() {
        return this.Y != 8;
    }

    @DexIgnore
    public void b(b bVar) {
        this.C[1] = bVar;
        if (bVar == b.WRAP_CONTENT) {
            h(this.U);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4) {
        if (i4 == 0) {
            a(i2, i3);
        } else if (i4 == 1) {
            e(i2, i3);
        }
        this.c0 = true;
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.I = i2;
        this.E = i3 - i2;
        int i4 = this.E;
        int i5 = this.R;
        if (i4 < i5) {
            this.E = i5;
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.X = obj;
    }

    @DexIgnore
    public void a(i5.d dVar, j5 j5Var, i5.d dVar2, int i2, int i3) {
        a(dVar).a(j5Var.a(dVar2), i2, i3, i5.c.STRONG, 0, true);
    }

    @DexIgnore
    public i5 a(i5.d dVar) {
        switch (a.a[dVar.ordinal()]) {
            case 1:
                return this.s;
            case 2:
                return this.t;
            case 3:
                return this.u;
            case 4:
                return this.v;
            case 5:
                return this.w;
            case 6:
                return this.z;
            case 7:
                return this.x;
            case 8:
                return this.y;
            case 9:
                return null;
            default:
                throw new AssertionError(dVar.name());
        }
    }

    @DexIgnore
    public void a(b bVar) {
        this.C[0] = bVar;
        if (bVar == b.WRAP_CONTENT) {
            p(this.T);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x01d0  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0237  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0248 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0249  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02aa  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02b3  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x02b9  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x02c1  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02f8  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0321  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:170:? A[RETURN, SYNTHETIC] */
    public void a(z4 z4Var) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        int i2;
        int i3;
        boolean z6;
        int i4;
        int i5;
        d5 d5Var;
        boolean z7;
        d5 d5Var2;
        d5 d5Var3;
        boolean z8;
        d5 d5Var4;
        d5 d5Var5;
        boolean z9;
        z4 z4Var2;
        d5 d5Var6;
        j5 j5Var;
        int i6;
        int i7;
        boolean z10;
        boolean z11;
        z4 z4Var3 = z4Var;
        d5 a2 = z4Var3.a((Object) this.s);
        d5 a3 = z4Var3.a((Object) this.u);
        d5 a4 = z4Var3.a((Object) this.t);
        d5 a5 = z4Var3.a((Object) this.v);
        d5 a6 = z4Var3.a((Object) this.w);
        j5 j5Var2 = this.D;
        if (j5Var2 != null) {
            z5 = j5Var2 != null && j5Var2.C[0] == b.WRAP_CONTENT;
            j5 j5Var3 = this.D;
            boolean z12 = j5Var3 != null && j5Var3.C[1] == b.WRAP_CONTENT;
            if (f(0)) {
                ((k5) this.D).a(this, 0);
                z10 = true;
            } else {
                z10 = A();
            }
            if (f(1)) {
                ((k5) this.D).a(this, 1);
                z11 = true;
            } else {
                z11 = B();
            }
            if (z5 && this.Y != 8 && this.s.d == null && this.u.d == null) {
                z4Var3.b(z4Var3.a((Object) this.D.u), a3, 0, 1);
            }
            if (z12 && this.Y != 8 && this.t.d == null && this.v.d == null && this.w == null) {
                z4Var3.b(z4Var3.a((Object) this.D.v), a5, 0, 1);
            }
            z4 = z12;
            z3 = z10;
            z2 = z11;
        } else {
            z5 = false;
            z4 = false;
            z3 = false;
            z2 = false;
        }
        int i8 = this.E;
        int i9 = this.R;
        if (i8 < i9) {
            i8 = i9;
        }
        int i10 = this.F;
        int i11 = this.S;
        if (i10 < i11) {
            i10 = i11;
        }
        boolean z13 = this.C[0] != b.MATCH_CONSTRAINT;
        boolean z14 = this.C[1] != b.MATCH_CONSTRAINT;
        this.n = this.H;
        float f2 = this.G;
        this.o = f2;
        int i12 = this.e;
        int i13 = this.f;
        if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || this.Y == 8) {
            d5Var = a6;
            i5 = i12;
            i3 = i8;
            i2 = i10;
            i4 = i13;
        } else {
            d5Var = a6;
            if (this.C[0] == b.MATCH_CONSTRAINT && i12 == 0) {
                i12 = 3;
            }
            if (this.C[1] == b.MATCH_CONSTRAINT && i13 == 0) {
                i13 = 3;
            }
            b[] bVarArr = this.C;
            b bVar = bVarArr[0];
            b bVar2 = b.MATCH_CONSTRAINT;
            if (bVar == bVar2 && bVarArr[1] == bVar2) {
                i7 = 3;
                if (i12 == 3 && i13 == 3) {
                    a(z5, z4, z13, z14);
                    i5 = i12;
                    i3 = i8;
                    i2 = i10;
                    i4 = i13;
                    z6 = true;
                    int[] iArr = this.g;
                    iArr[0] = i5;
                    iArr[1] = i4;
                    if (z6) {
                        int i14 = this.n;
                        if (i14 == 0 || i14 == -1) {
                            z7 = true;
                            boolean z15 = this.C[0] == b.WRAP_CONTENT && (this instanceof k5);
                            boolean z16 = !this.z.i();
                            if (this.a == 2) {
                                j5 j5Var4 = this.D;
                                d5 a7 = j5Var4 != null ? z4Var3.a((Object) j5Var4.u) : null;
                                j5 j5Var5 = this.D;
                                z8 = z4;
                                d5Var3 = d5Var;
                                d5Var5 = a5;
                                d5Var2 = a4;
                                boolean z17 = z15;
                                d5Var4 = a3;
                                a(z4Var, z5, j5Var5 != null ? z4Var3.a((Object) j5Var5.s) : null, a7, this.C[0], z17, this.s, this.u, this.I, i3, this.R, this.q[0], this.V, z7, z3, i5, this.h, this.i, this.j, z16);
                            } else {
                                d5Var2 = a4;
                                d5Var4 = a3;
                                z8 = z4;
                                d5Var3 = d5Var;
                                d5Var5 = a5;
                            }
                            if (this.b == 2) {
                                boolean z18 = this.C[1] == b.WRAP_CONTENT && (this instanceof k5);
                                boolean z19 = z6 && ((i6 = this.n) == 1 || i6 == -1);
                                if (this.Q <= 0) {
                                    z4Var2 = z4Var;
                                } else if (this.w.d().b == 1) {
                                    z4Var2 = z4Var;
                                    this.w.d().a(z4Var2);
                                } else {
                                    z4Var2 = z4Var;
                                    d5 d5Var7 = d5Var3;
                                    d5Var6 = d5Var2;
                                    z4Var2.a(d5Var7, d5Var6, d(), 6);
                                    i5 i5Var = this.w.d;
                                    if (i5Var != null) {
                                        z4Var2.a(d5Var7, z4Var2.a((Object) i5Var), 0, 6);
                                        z9 = false;
                                        j5 j5Var6 = this.D;
                                        d5 a8 = j5Var6 != null ? z4Var2.a((Object) j5Var6.v) : null;
                                        j5 j5Var7 = this.D;
                                        d5 d5Var8 = d5Var6;
                                        a(z4Var, z8, j5Var7 != null ? z4Var2.a((Object) j5Var7.t) : null, a8, this.C[1], z18, this.t, this.v, this.J, i2, this.S, this.q[1], this.W, z19, z2, i4, this.k, this.l, this.m, z9);
                                        if (z6) {
                                            j5Var = this;
                                            if (j5Var.n == 1) {
                                                z4Var.a(d5Var5, d5Var8, d5Var4, a2, j5Var.o, 6);
                                            } else {
                                                z4Var.a(d5Var4, a2, d5Var5, d5Var8, j5Var.o, 6);
                                            }
                                        } else {
                                            j5Var = this;
                                        }
                                        if (j5Var.z.i()) {
                                            z4Var.a(j5Var, j5Var.z.g().c(), (float) Math.toRadians((double) (j5Var.r + 90.0f)), j5Var.z.b());
                                            return;
                                        }
                                        return;
                                    }
                                    z9 = z16;
                                    j5 j5Var62 = this.D;
                                    if (j5Var62 != null) {
                                    }
                                    j5 j5Var72 = this.D;
                                    d5 d5Var82 = d5Var6;
                                    a(z4Var, z8, j5Var72 != null ? z4Var2.a((Object) j5Var72.t) : null, a8, this.C[1], z18, this.t, this.v, this.J, i2, this.S, this.q[1], this.W, z19, z2, i4, this.k, this.l, this.m, z9);
                                    if (z6) {
                                    }
                                    if (j5Var.z.i()) {
                                    }
                                }
                                d5Var6 = d5Var2;
                                z9 = z16;
                                j5 j5Var622 = this.D;
                                if (j5Var622 != null) {
                                }
                                j5 j5Var722 = this.D;
                                d5 d5Var822 = d5Var6;
                                a(z4Var, z8, j5Var722 != null ? z4Var2.a((Object) j5Var722.t) : null, a8, this.C[1], z18, this.t, this.v, this.J, i2, this.S, this.q[1], this.W, z19, z2, i4, this.k, this.l, this.m, z9);
                                if (z6) {
                                }
                                if (j5Var.z.i()) {
                                }
                            } else {
                                return;
                            }
                        }
                    }
                    z7 = false;
                    if (this.C[0] == b.WRAP_CONTENT || (this instanceof k5)) {
                    }
                    boolean z162 = !this.z.i();
                    if (this.a == 2) {
                    }
                    if (this.b == 2) {
                    }
                }
            } else {
                i7 = 3;
            }
            b[] bVarArr2 = this.C;
            b bVar3 = bVarArr2[0];
            b bVar4 = b.MATCH_CONSTRAINT;
            if (bVar3 == bVar4 && i12 == i7) {
                this.n = 0;
                b bVar5 = bVarArr2[1];
                i3 = (int) (this.o * ((float) this.F));
                if (bVar5 != bVar4) {
                    i2 = i10;
                    i4 = i13;
                    i5 = 4;
                } else {
                    i5 = i12;
                    i2 = i10;
                    i4 = i13;
                    z6 = true;
                    int[] iArr2 = this.g;
                    iArr2[0] = i5;
                    iArr2[1] = i4;
                    if (z6) {
                    }
                    z7 = false;
                    if (this.C[0] == b.WRAP_CONTENT || (this instanceof k5)) {
                    }
                    boolean z1622 = !this.z.i();
                    if (this.a == 2) {
                    }
                    if (this.b == 2) {
                    }
                }
            } else {
                if (this.C[1] == b.MATCH_CONSTRAINT && i13 == 3) {
                    this.n = 1;
                    if (this.H == -1) {
                        this.o = 1.0f / this.o;
                    }
                    b bVar6 = this.C[0];
                    b bVar7 = b.MATCH_CONSTRAINT;
                    i2 = (int) (this.o * ((float) this.E));
                    i5 = i12;
                    i3 = i8;
                    if (bVar6 != bVar7) {
                        i4 = 4;
                    }
                    i4 = i13;
                    z6 = true;
                    int[] iArr22 = this.g;
                    iArr22[0] = i5;
                    iArr22[1] = i4;
                    if (z6) {
                    }
                    z7 = false;
                    if (this.C[0] == b.WRAP_CONTENT || (this instanceof k5)) {
                    }
                    boolean z16222 = !this.z.i();
                    if (this.a == 2) {
                    }
                    if (this.b == 2) {
                    }
                }
                i5 = i12;
                i3 = i8;
                i2 = i10;
                i4 = i13;
                z6 = true;
                int[] iArr222 = this.g;
                iArr222[0] = i5;
                iArr222[1] = i4;
                if (z6) {
                }
                z7 = false;
                if (this.C[0] == b.WRAP_CONTENT || (this instanceof k5)) {
                }
                boolean z162222 = !this.z.i();
                if (this.a == 2) {
                }
                if (this.b == 2) {
                }
            }
        }
        z6 = false;
        int[] iArr2222 = this.g;
        iArr2222[0] = i5;
        iArr2222[1] = i4;
        if (z6) {
        }
        z7 = false;
        if (this.C[0] == b.WRAP_CONTENT || (this instanceof k5)) {
        }
        boolean z1622222 = !this.z.i();
        if (this.a == 2) {
        }
        if (this.b == 2) {
        }
    }

    @DexIgnore
    public void a(boolean z2, boolean z3, boolean z4, boolean z5) {
        if (this.n == -1) {
            if (z4 && !z5) {
                this.n = 0;
            } else if (!z4 && z5) {
                this.n = 1;
                if (this.H == -1) {
                    this.o = 1.0f / this.o;
                }
            }
        }
        if (this.n == 0 && (!this.t.i() || !this.v.i())) {
            this.n = 1;
        } else if (this.n == 1 && (!this.s.i() || !this.u.i())) {
            this.n = 0;
        }
        if (this.n == -1 && (!this.t.i() || !this.v.i() || !this.s.i() || !this.u.i())) {
            if (this.t.i() && this.v.i()) {
                this.n = 0;
            } else if (this.s.i() && this.u.i()) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (z2 && !z3) {
                this.n = 0;
            } else if (!z2 && z3) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (this.h > 0 && this.k == 0) {
                this.n = 0;
            } else if (this.h == 0 && this.k > 0) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1 && z2 && z3) {
            this.o = 1.0f / this.o;
            this.n = 1;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02a2  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02e5  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x02f4  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0313  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x031c  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0323  */
    /* JADX WARNING: Removed duplicated region for block: B:189:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01d8 A[ADDED_TO_REGION] */
    public final void a(z4 z4Var, boolean z2, d5 d5Var, d5 d5Var2, b bVar, boolean z3, i5 i5Var, i5 i5Var2, int i2, int i3, int i4, int i5, float f2, boolean z4, boolean z5, int i6, int i7, int i8, float f3, boolean z6) {
        boolean z7;
        int i9;
        int i10;
        int i11;
        d5 d5Var3;
        d5 d5Var4;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        d5 d5Var5;
        boolean z8;
        int i19;
        d5 d5Var6;
        boolean z9;
        boolean z10;
        d5 d5Var7;
        d5 d5Var8;
        int i20;
        int i21;
        boolean z11;
        d5 d5Var9;
        boolean z12;
        boolean z13;
        int i22;
        int i23;
        int i24;
        int i25;
        boolean z14;
        boolean z15;
        d5 d5Var10;
        d5 d5Var11;
        z4 z4Var2 = z4Var;
        d5 d5Var12 = d5Var;
        d5 d5Var13 = d5Var2;
        int i26 = i4;
        int i27 = i5;
        d5 a2 = z4Var2.a((Object) i5Var);
        d5 a3 = z4Var2.a((Object) i5Var2);
        d5 a4 = z4Var2.a((Object) i5Var.g());
        d5 a5 = z4Var2.a((Object) i5Var2.g());
        if (z4Var2.g && i5Var.d().b == 1 && i5Var2.d().b == 1) {
            if (z4.j() != null) {
                z4.j().r++;
            }
            i5Var.d().a(z4Var2);
            i5Var2.d().a(z4Var2);
            if (!z5 && z2) {
                z4Var2.b(d5Var13, a3, 0, 6);
                return;
            }
            return;
        }
        if (z4.j() != null) {
            z4.j().z++;
        }
        boolean i28 = i5Var.i();
        boolean i29 = i5Var2.i();
        boolean i30 = this.z.i();
        int i31 = i28 ? 1 : 0;
        if (i29) {
            i31++;
        }
        if (i30) {
            i31++;
        }
        int i32 = i31;
        int i33 = z4 ? 3 : i6;
        int i34 = a.b[bVar.ordinal()];
        boolean z16 = (i34 == 1 || i34 == 2 || i34 == 3 || i34 != 4 || i33 == 4) ? false : true;
        if (this.Y == 8) {
            i9 = 0;
            z7 = false;
        } else {
            z7 = z16;
            i9 = i3;
        }
        if (z6) {
            if (!i28 && !i29 && !i30) {
                z4Var2.a(a2, i2);
            } else if (i28 && !i29) {
                i10 = 6;
                z4Var2.a(a2, a4, i5Var.b(), 6);
                if (z7) {
                    if (z3) {
                        z4Var2.a(a3, a2, 0, 3);
                        if (i26 > 0) {
                            z4Var2.b(a3, a2, i26, 6);
                        }
                        if (i27 < Integer.MAX_VALUE) {
                            z4Var2.c(a3, a2, i27, 6);
                        }
                    } else {
                        z4Var2.a(a3, a2, i9, i10);
                    }
                    i12 = i8;
                    i11 = i33;
                    i15 = i32;
                    d5Var3 = a5;
                    d5Var4 = a4;
                    i14 = 0;
                    i13 = i7;
                } else {
                    int i35 = i7;
                    if (i35 == -2) {
                        i12 = i8;
                        i23 = i9;
                    } else {
                        i23 = i35;
                        i12 = i8;
                    }
                    if (i12 == -2) {
                        i12 = i9;
                    }
                    if (i23 > 0) {
                        z4Var2.b(a3, a2, i23, 6);
                        i9 = Math.max(i9, i23);
                    }
                    if (i12 > 0) {
                        z4Var2.c(a3, a2, i12, 6);
                        i9 = Math.min(i9, i12);
                    }
                    if (i33 != 1) {
                        z15 = z7;
                        if (i33 == 2) {
                            if (i5Var.h() == i5.d.TOP || i5Var.h() == i5.d.BOTTOM) {
                                d5Var10 = z4Var2.a((Object) this.D.a(i5.d.TOP));
                                d5Var11 = z4Var2.a((Object) this.D.a(i5.d.BOTTOM));
                            } else {
                                d5Var10 = z4Var2.a((Object) this.D.a(i5.d.LEFT));
                                d5Var11 = z4Var2.a((Object) this.D.a(i5.d.RIGHT));
                            }
                            d5 d5Var14 = d5Var10;
                            d5 d5Var15 = d5Var11;
                            w4 c2 = z4Var.c();
                            w4 w4Var = c2;
                            i11 = i33;
                            d5Var4 = a4;
                            i25 = i9;
                            i15 = i32;
                            i14 = 0;
                            d5 d5Var16 = d5Var15;
                            i24 = i23;
                            d5Var3 = a5;
                            c2.a(a3, a2, d5Var16, d5Var14, f3);
                            z4Var2.a(c2);
                            z14 = false;
                            if (z14) {
                            }
                            i13 = i24;
                            z7 = z14;
                        }
                    } else if (z2) {
                        z4Var2.a(a3, a2, i9, 6);
                        i11 = i33;
                        i15 = i32;
                        d5Var3 = a5;
                        d5Var4 = a4;
                        z15 = z7;
                        i14 = 0;
                        i25 = i9;
                        i24 = i23;
                        z14 = z15;
                        if (z14 || i15 == 2 || z4) {
                            i13 = i24;
                            z7 = z14;
                        } else {
                            int max = Math.max(i24, i25);
                            if (i12 > 0) {
                                max = Math.min(i12, max);
                            }
                            z4Var2.a(a3, a2, max, 6);
                            i13 = i24;
                            z7 = false;
                        }
                    } else if (z5) {
                        z15 = z7;
                        z4Var2.a(a3, a2, i9, 4);
                    } else {
                        z15 = z7;
                        z4Var2.a(a3, a2, i9, 1);
                    }
                    i11 = i33;
                    i15 = i32;
                    i24 = i23;
                    d5Var3 = a5;
                    d5Var4 = a4;
                    i14 = 0;
                    i25 = i9;
                    z14 = z15;
                    if (z14) {
                    }
                    i13 = i24;
                    z7 = z14;
                }
                if (z6 || z5) {
                    i16 = i15;
                    d5 d5Var17 = a3;
                    d5 d5Var18 = d5Var12;
                    d5 d5Var19 = d5Var13;
                    if (i16 < 2 && z2) {
                        z4Var2.b(a2, d5Var18, 0, 6);
                        z4Var2.b(d5Var19, d5Var17, 0, 6);
                        return;
                    }
                }
                if (i28 || i29 || i30) {
                    if (!i28 || i29) {
                        if (i28 || !i29) {
                            d5 d5Var20 = d5Var3;
                            if (i28 && i29) {
                                if (z7) {
                                    if (z2 && i4 == 0) {
                                        z4Var2.b(a3, a2, 0, 6);
                                    }
                                    if (i11 == 0) {
                                        if (i12 > 0 || i13 > 0) {
                                            i22 = 4;
                                            z13 = true;
                                        } else {
                                            i22 = 6;
                                            z13 = false;
                                        }
                                        d5Var6 = d5Var4;
                                        z4Var2.a(a2, d5Var6, i5Var.b(), i22);
                                        z4Var2.a(a3, d5Var20, -i5Var2.b(), i22);
                                        z9 = i12 > 0 || i13 > 0;
                                        z8 = z13;
                                        i19 = 5;
                                    } else {
                                        int i36 = i11;
                                        d5Var6 = d5Var4;
                                        if (i36 == 1) {
                                            z9 = true;
                                            i19 = 6;
                                            z8 = true;
                                        } else if (i36 == 3) {
                                            int i37 = (z4 || this.n == -1 || i12 > 0) ? 4 : 6;
                                            z4Var2.a(a2, d5Var6, i5Var.b(), i37);
                                            z4Var2.a(a3, d5Var20, -i5Var2.b(), i37);
                                            z9 = true;
                                            i19 = 5;
                                            z8 = true;
                                            if (z9) {
                                                z10 = true;
                                                d5Var7 = d5Var20;
                                                d5Var9 = d5Var6;
                                                d5Var8 = a3;
                                                z4Var.a(a2, d5Var6, i5Var.b(), f2, d5Var20, a3, i5Var2.b(), i19);
                                                boolean z17 = i5Var.d.b instanceof f5;
                                                boolean z18 = i5Var2.d.b instanceof f5;
                                                if (z17 && !z18) {
                                                    z10 = z2;
                                                    z11 = true;
                                                    i21 = 5;
                                                    i20 = 6;
                                                    if (z8) {
                                                    }
                                                    z4Var2.b(a2, d5Var9, i5Var.b(), i21);
                                                    z4Var2.c(d5Var8, d5Var7, -i5Var2.b(), i20);
                                                    if (z2) {
                                                    }
                                                } else if (!z17 && z18) {
                                                    z11 = z2;
                                                    i21 = 6;
                                                    i20 = 5;
                                                    if (z8) {
                                                        i21 = 6;
                                                        i20 = 6;
                                                    }
                                                    if ((!z7 && z10) || z8) {
                                                        z4Var2.b(a2, d5Var9, i5Var.b(), i21);
                                                    }
                                                    if ((!z7 && z11) || z8) {
                                                        z4Var2.c(d5Var8, d5Var7, -i5Var2.b(), i20);
                                                    }
                                                    if (z2) {
                                                        d5Var5 = d5Var8;
                                                        i18 = 6;
                                                        i17 = 0;
                                                        z4Var2.b(a2, d5Var, 0, 6);
                                                        if (z2) {
                                                            z4Var2.b(d5Var2, d5Var5, i17, i18);
                                                            return;
                                                        }
                                                        return;
                                                    }
                                                    d5Var5 = d5Var8;
                                                    i18 = 6;
                                                    i17 = 0;
                                                    if (z2) {
                                                    }
                                                }
                                            } else {
                                                i5 i5Var3 = i5Var;
                                                i5 i5Var4 = i5Var2;
                                                d5Var9 = d5Var6;
                                                d5Var7 = d5Var20;
                                                d5Var8 = a3;
                                            }
                                            z11 = z2;
                                            z10 = z11;
                                            i21 = 5;
                                            i20 = 5;
                                            if (z8) {
                                            }
                                            z4Var2.b(a2, d5Var9, i5Var.b(), i21);
                                            z4Var2.c(d5Var8, d5Var7, -i5Var2.b(), i20);
                                            if (z2) {
                                            }
                                        } else {
                                            z12 = false;
                                        }
                                    }
                                    if (z9) {
                                    }
                                    z11 = z2;
                                    z10 = z11;
                                    i21 = 5;
                                    i20 = 5;
                                    if (z8) {
                                    }
                                    z4Var2.b(a2, d5Var9, i5Var.b(), i21);
                                    z4Var2.c(d5Var8, d5Var7, -i5Var2.b(), i20);
                                    if (z2) {
                                    }
                                } else {
                                    d5Var6 = d5Var4;
                                    z12 = true;
                                }
                                i19 = 5;
                                z8 = false;
                                if (z9) {
                                }
                                z11 = z2;
                                z10 = z11;
                                i21 = 5;
                                i20 = 5;
                                if (z8) {
                                }
                                z4Var2.b(a2, d5Var9, i5Var.b(), i21);
                                z4Var2.c(d5Var8, d5Var7, -i5Var2.b(), i20);
                                if (z2) {
                                }
                            }
                        } else {
                            z4Var2.a(a3, d5Var3, -i5Var2.b(), 6);
                            if (z2) {
                                z4Var2.b(a2, d5Var12, i14, 5);
                            }
                        }
                    } else if (z2) {
                        z4Var2.b(d5Var13, a3, i14, 5);
                    }
                } else if (z2) {
                    z4Var2.b(d5Var13, a3, i14, 5);
                }
                d5Var5 = a3;
                i18 = 6;
                i17 = 0;
                if (z2) {
                }
            }
        }
        i10 = 6;
        if (z7) {
        }
        if (z6) {
        }
        i16 = i15;
        d5 d5Var172 = a3;
        d5 d5Var182 = d5Var12;
        d5 d5Var192 = d5Var13;
        if (i16 < 2) {
        }
    }
}
