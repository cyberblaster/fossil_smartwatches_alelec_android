package com.fossil;

import com.fossil.fn2;
import com.fossil.oj2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj2 extends fn2<mj2, a> implements to2 {
    @DexIgnore
    public static /* final */ mj2 zzi;
    @DexIgnore
    public static volatile yo2<mj2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public nn2<oj2> zzd; // = fn2.m();
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public long zzg;
    @DexIgnore
    public int zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<mj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(mj2.zzi);
        }

        @DexIgnore
        public final oj2 a(int i) {
            return ((mj2) this.b).b(i);
        }

        @DexIgnore
        public final a b(int i) {
            f();
            ((mj2) this.b).c(i);
            return this;
        }

        @DexIgnore
        public final List<oj2> j() {
            return Collections.unmodifiableList(((mj2) this.b).n());
        }

        @DexIgnore
        public final int k() {
            return ((mj2) this.b).o();
        }

        @DexIgnore
        public final String l() {
            return ((mj2) this.b).p();
        }

        @DexIgnore
        public final long m() {
            return ((mj2) this.b).r();
        }

        @DexIgnore
        public final long n() {
            return ((mj2) this.b).t();
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(int i, oj2 oj2) {
            f();
            ((mj2) this.b).a(i, oj2);
            return this;
        }

        @DexIgnore
        public final a b(long j) {
            f();
            ((mj2) this.b).b(j);
            return this;
        }

        @DexIgnore
        public final a a(int i, oj2.a aVar) {
            f();
            ((mj2) this.b).a(i, aVar);
            return this;
        }

        @DexIgnore
        public final a a(oj2 oj2) {
            f();
            ((mj2) this.b).a(oj2);
            return this;
        }

        @DexIgnore
        public final a a(oj2.a aVar) {
            f();
            ((mj2) this.b).a(aVar);
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((mj2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public final a a(long j) {
            f();
            ((mj2) this.b).a(j);
            return this;
        }
    }

    /*
    static {
        mj2 mj2 = new mj2();
        zzi = mj2;
        fn2.a(mj2.class, mj2);
    }
    */

    @DexIgnore
    public static a y() {
        return (a) zzi.h();
    }

    @DexIgnore
    public final void a(int i, oj2 oj2) {
        if (oj2 != null) {
            x();
            this.zzd.set(i, oj2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final oj2 b(int i) {
        return this.zzd.get(i);
    }

    @DexIgnore
    public final void c(int i) {
        x();
        this.zzd.remove(i);
    }

    @DexIgnore
    public final List<oj2> n() {
        return this.zzd;
    }

    @DexIgnore
    public final int o() {
        return this.zzd.size();
    }

    @DexIgnore
    public final String p() {
        return this.zze;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long r() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long t() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean v() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int w() {
        return this.zzh;
    }

    @DexIgnore
    public final void x() {
        if (!this.zzd.zza()) {
            this.zzd = fn2.a(this.zzd);
        }
    }

    @DexIgnore
    public final void b(long j) {
        this.zzc |= 4;
        this.zzg = j;
    }

    @DexIgnore
    public final void a(int i, oj2.a aVar) {
        x();
        this.zzd.set(i, (oj2) aVar.i());
    }

    @DexIgnore
    public final void a(oj2 oj2) {
        if (oj2 != null) {
            x();
            this.zzd.add(oj2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(oj2.a aVar) {
        x();
        this.zzd.add((oj2) aVar.i());
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 1;
            this.zze = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 2;
        this.zzf = j;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new mj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002\b\u0000\u0003\u0002\u0001\u0004\u0002\u0002\u0005\u0004\u0003", new Object[]{"zzc", "zzd", oj2.class, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                yo2<mj2> yo2 = zzj;
                if (yo2 == null) {
                    synchronized (mj2.class) {
                        yo2 = zzj;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzi);
                            zzj = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
