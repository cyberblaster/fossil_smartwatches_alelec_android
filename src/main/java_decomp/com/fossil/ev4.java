package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev4 extends df<DailyHeartRateSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar d; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ PortfolioApp i;
    @DexIgnore
    public /* final */ fv4 j;
    @DexIgnore
    public /* final */ FragmentManager k;
    @DexIgnore
    public /* final */ BaseFragment l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public String f;
        @DexIgnore
        public int g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4) {
            wg6.b(str, "mDayOfWeek");
            wg6.b(str2, "mDayOfMonth");
            wg6.b(str3, "mDailyRestingUnit");
            wg6.b(str4, "mDailyMaxUnit");
            this.a = date;
            this.b = z;
            this.c = str;
            this.d = str2;
            this.e = i;
            this.f = str3;
            this.g = i2;
            this.h = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void c(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void d(String str) {
            wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final Date e() {
            return this.a;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final String g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4, int i3, qg6 qg6) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) != 0 ? r5 : str, (r0 & 8) != 0 ? r5 : str2, (r0 & 16) != 0 ? 0 : i, (r0 & 32) != 0 ? r5 : str3, (r0 & 64) == 0 ? i2 : 0, (r0 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 0 ? str4 : r5);
            int i4 = i3;
            Date date2 = (i4 & 1) != 0 ? null : date;
            String str5 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void b(int i) {
            this.e = i;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final int d() {
            return this.e;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final int b() {
            return this.g;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void a(int i) {
            this.g = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ ng4 b;
        @DexIgnore
        public /* final */ /* synthetic */ ev4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.c.j.b(a2);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ev4 ev4, ng4 ng4, View view) {
            super(view);
            wg6.b(ng4, "binding");
            wg6.b(view, "root");
            this.c = ev4;
            this.b = ng4;
            this.b.d().setOnClickListener(new a(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r6v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r6v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v38, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v40, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v45, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(DailyHeartRateSummary dailyHeartRateSummary) {
            b a2 = this.c.a(dailyHeartRateSummary);
            this.a = a2.e();
            Object r0 = this.b.t;
            wg6.a((Object) r0, "binding.ftvDayOfWeek");
            r0.setText(a2.g());
            Object r02 = this.b.s;
            wg6.a((Object) r02, "binding.ftvDayOfMonth");
            r02.setText(a2.f());
            if (a2.b() == 0 && a2.d() == 0) {
                ConstraintLayout constraintLayout = this.b.q;
                wg6.a((Object) constraintLayout, "binding.clContainer");
                constraintLayout.setVisibility(8);
                Object r03 = this.b.w;
                wg6.a((Object) r03, "binding.ftvNoRecord");
                r03.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                wg6.a((Object) constraintLayout2, "binding.clContainer");
                constraintLayout2.setVisibility(0);
                Object r04 = this.b.w;
                wg6.a((Object) r04, "binding.ftvNoRecord");
                r04.setVisibility(8);
                Object r05 = this.b.y;
                wg6.a((Object) r05, "binding.ftvRestingValue");
                r05.setText(String.valueOf(a2.d()));
                Object r06 = this.b.x;
                wg6.a((Object) r06, "binding.ftvRestingUnit");
                r06.setText(a2.c());
                Object r07 = this.b.v;
                wg6.a((Object) r07, "binding.ftvMaxValue");
                r07.setText(String.valueOf(a2.b()));
                Object r08 = this.b.u;
                wg6.a((Object) r08, "binding.ftvMaxUnit");
                r08.setText(a2.a());
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            wg6.a((Object) constraintLayout3, "binding.container");
            constraintLayout3.setSelected(!a2.h());
            if (a2.h()) {
                this.b.r.setBackgroundColor(this.c.f);
                this.b.t.setBackgroundColor(this.c.f);
                this.b.s.setBackgroundColor(this.c.f);
                this.b.t.setTextColor(this.c.g);
                this.b.s.setTextColor(this.c.e);
                return;
            }
            this.b.r.setBackgroundColor(this.c.h);
            this.b.t.setBackgroundColor(this.c.h);
            this.b.s.setBackgroundColor(this.c.h);
            this.b.t.setTextColor(this.c.g);
            this.b.s.setTextColor(this.c.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            wg6.b(str, "mWeekly");
            wg6.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ pg4 f;
        @DexIgnore
        public /* final */ /* synthetic */ ev4 g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.d != null && this.a.e != null) {
                    fv4 c = this.a.g.j;
                    Date b = this.a.d;
                    if (b != null) {
                        Date a2 = this.a.e;
                        if (a2 != null) {
                            c.b(b, a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(ev4 ev4, pg4 pg4) {
            super(ev4, r0, r1);
            wg6.b(pg4, "binding");
            this.g = ev4;
            ng4 ng4 = pg4.r;
            if (ng4 != null) {
                wg6.a((Object) ng4, "binding.dailyItem!!");
                View d2 = pg4.d();
                wg6.a((Object) d2, "binding.root");
                this.f = pg4;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(DailyHeartRateSummary dailyHeartRateSummary) {
            d b = this.g.b(dailyHeartRateSummary);
            this.e = b.a();
            this.d = b.b();
            Object r1 = this.f.s;
            wg6.a((Object) r1, "binding.ftvWeekly");
            r1.setText(b.c());
            Object r12 = this.f.t;
            wg6.a((Object) r12, "binding.ftvWeeklyValue");
            r12.setText(b.d());
            super.a(dailyHeartRateSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev4 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(ev4 ev4, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = ev4;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            wg6.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.l.getId() + ", isAdded=" + this.a.l.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.k.b(this.a.l.h1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                hc b3 = this.a.k.b();
                b3.a(view.getId(), this.a.l, this.a.l.h1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                hc b4 = this.a.k.b();
                b4.d(b2);
                b4.d();
                hc b5 = this.a.k.b();
                b5.a(view.getId(), this.a.l, this.a.l.h1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.l.getId() + ", isAdded2=" + this.a.l.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            wg6.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ev4(dv4 dv4, PortfolioApp portfolioApp, fv4 fv4, FragmentManager fragmentManager, BaseFragment baseFragment) {
        super(dv4);
        wg6.b(dv4, "dailyHeartRateSummaryDifference");
        wg6.b(portfolioApp, "mApp");
        wg6.b(fv4, "mOnItemClick");
        wg6.b(fragmentManager, "mFragmentManager");
        wg6.b(baseFragment, "mFragment");
        this.i = portfolioApp;
        this.j = fv4;
        this.k = fragmentManager;
        this.l = baseFragment;
        String b2 = ThemeManager.l.a().b("primaryText");
        this.e = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("nonBrandSurface");
        this.f = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("secondaryText");
        this.g = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.h = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
    }

    @DexIgnore
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return ev4.super.getItemId(i2);
        }
        if (this.l.getId() == 0) {
            return 1010101;
        }
        return (long) this.l.getId();
    }

    @DexIgnore
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) getItem(i2);
        if (dailyHeartRateSummary == null) {
            return 1;
        }
        Calendar calendar = this.d;
        wg6.a((Object) calendar, "mCalendar");
        calendar.setTime(dailyHeartRateSummary.getDate());
        Calendar calendar2 = this.d;
        wg6.a((Object) calendar2, "mCalendar");
        Boolean t = bk4.t(calendar2.getTime());
        wg6.a((Object) t, "DateHelper.isToday(mCalendar.time)");
        if (t.booleanValue() || this.d.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        wg6.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardHeartRatesAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            wg6.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            wg6.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardHeartRatesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            wg6.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((DailyHeartRateSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((DailyHeartRateSummary) getItem(i2));
        } else {
            ((e) viewHolder).a((DailyHeartRateSummary) getItem(i2));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            ng4 a2 = ng4.a(from, viewGroup, false);
            wg6.a((Object) a2, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View d2 = a2.d();
            wg6.a((Object) d2, "itemActivityDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            ng4 a3 = ng4.a(from, viewGroup, false);
            wg6.a((Object) a3, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View d3 = a3.d();
            wg6.a((Object) d3, "itemActivityDayBinding.root");
            return new c(this, a3, d3);
        } else {
            pg4 a4 = pg4.a(from, viewGroup, false);
            wg6.a((Object) a4, "ItemHeartRateWeekBinding\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(cf<DailyHeartRateSummary> cfVar) {
        List j2;
        if (!(cfVar == null || (j2 = cfVar.j()) == null)) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) j2, "summaries");
            if (!j2.isEmpty()) {
                wg6.a((Object) instance, "calendar");
                instance.setTime(((DailyHeartRateSummary) yd6.d(j2)).getDate());
                if (!bk4.t(instance.getTime()).booleanValue()) {
                    instance.setTime(new Date());
                    Date time = instance.getTime();
                    wg6.a((Object) time, "calendar.time");
                    Date time2 = instance.getTime();
                    wg6.a((Object) time2, "calendar.time");
                    long time3 = time2.getTime();
                    Date time4 = instance.getTime();
                    wg6.a((Object) time4, "calendar.time");
                    new DailyHeartRateSummary(0.0f, time, time3, time4.getTime(), 0, 0, 0, (Resting) null);
                } else {
                    Object d2 = yd6.d(j2);
                    wg6.a(d2, "summaries.first()");
                    DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) d2;
                }
            } else {
                wg6.a((Object) instance, "calendar");
                Date time5 = instance.getTime();
                wg6.a((Object) time5, "calendar.time");
                Date time6 = instance.getTime();
                wg6.a((Object) time6, "calendar.time");
                long time7 = time6.getTime();
                Date time8 = instance.getTime();
                wg6.a((Object) time8, "calendar.time");
                new DailyHeartRateSummary(0.0f, time5, time7, time8.getTime(), 0, 0, 0, (Resting) null);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardHeartRatesAdapter", sb.toString());
        ev4.super.b(cfVar);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r13v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final b a(DailyHeartRateSummary dailyHeartRateSummary) {
        b bVar = new b((Date) null, false, (String) null, (String) null, 0, (String) null, 0, (String) null, 255, (qg6) null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int i2 = instance.get(7);
            Boolean t = bk4.t(instance.getTime());
            wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
            if (t.booleanValue()) {
                String a2 = jm4.a((Context) this.i, 2131886431);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026artRateToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(yk4.b.b(i2));
            }
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            Resting resting = dailyHeartRateSummary.getResting();
            boolean z = false;
            bVar.b(resting != null ? resting.getValue() : 0);
            String a3 = jm4.a((Context) this.i, 2131886464);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
            if (a3 != null) {
                String lowerCase = a3.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                bVar.b(lowerCase);
                bVar.a(dailyHeartRateSummary.getMax());
                String a4 = jm4.a((Context) this.i, 2131886462);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Label__Max)");
                if (a4 != null) {
                    String lowerCase2 = a4.toLowerCase();
                    wg6.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                    bVar.a(lowerCase2);
                    if (bVar.d() + bVar.b() == 0) {
                        z = true;
                    }
                    bVar.a(z);
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        return bVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final d b(DailyHeartRateSummary dailyHeartRateSummary) {
        String str;
        String str2;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (qg6) null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            Boolean t = bk4.t(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String b2 = bk4.b(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String b3 = bk4.b(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            wg6.a((Object) t, "isToday");
            if (t.booleanValue()) {
                str = jm4.a((Context) this.i, 2131886433);
                wg6.a((Object) str, "LanguageHelper.getString\u2026ateToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = b3 + ' ' + i5 + " - " + b3 + ' ' + i2;
            } else if (i7 == i4) {
                str = b3 + ' ' + i5 + " - " + b2 + ' ' + i2;
            } else {
                str = b3 + ' ' + i5 + ", " + i7 + " - " + b2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            if (dailyHeartRateSummary.getAvgRestingHeartRateOfWeek() == null) {
                str2 = ShareWebViewClient.RESP_SUCC_CODE;
            } else {
                str2 = String.valueOf(dailyHeartRateSummary.getAvgRestingHeartRateOfWeek());
            }
            nh6 nh6 = nh6.a;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886430);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026y_Text__NumberRestingBpm)");
            Object[] objArr = {str2};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }
}
