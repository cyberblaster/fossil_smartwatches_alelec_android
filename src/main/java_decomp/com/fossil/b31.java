package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b31 extends p40 {
    @DexIgnore
    public static /* final */ xv0 d; // = new xv0((qg6) null);
    @DexIgnore
    public /* final */ ma1 a;
    @DexIgnore
    public /* final */ g11 b;
    @DexIgnore
    public /* final */ t31 c;

    @DexIgnore
    public /* synthetic */ b31(ma1 ma1, g11 g11, t31 t31, int i) {
        ma1 = (i & 1) != 0 ? ma1.UNKNOWN : ma1;
        t31 = (i & 4) != 0 ? new t31(x11.SUCCESS, 0, 2) : t31;
        this.a = ma1;
        this.b = g11;
        this.c = t31;
    }

    @DexIgnore
    public static /* synthetic */ b31 a(b31 b31, ma1 ma1, g11 g11, t31 t31, int i) {
        if ((i & 1) != 0) {
            ma1 = b31.a;
        }
        if ((i & 2) != 0) {
            g11 = b31.b;
        }
        if ((i & 4) != 0) {
            t31 = b31.c;
        }
        return b31.a(ma1, g11, t31);
    }

    @DexIgnore
    public final b31 a(ma1 ma1, g11 g11, t31 t31) {
        return new b31(ma1, g11, t31);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(cw0.a(jSONObject, bm0.COMMAND_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.RESULT_CODE, (Object) cw0.a((Enum<?>) this.b));
            if (this.c.a != x11.SUCCESS) {
                cw0.a(jSONObject, bm0.GATT_RESULT, (Object) this.c.a());
            }
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b31)) {
            return false;
        }
        b31 b31 = (b31) obj;
        return wg6.a(this.a, b31.a) && wg6.a(this.b, b31.b) && wg6.a(this.c, b31.c);
    }

    @DexIgnore
    public int hashCode() {
        ma1 ma1 = this.a;
        int i = 0;
        int hashCode = (ma1 != null ? ma1.hashCode() : 0) * 31;
        g11 g11 = this.b;
        int hashCode2 = (hashCode + (g11 != null ? g11.hashCode() : 0)) * 31;
        t31 t31 = this.c;
        if (t31 != null) {
            i = t31.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("Result(commandId=");
        b2.append(this.a);
        b2.append(", resultCode=");
        b2.append(this.b);
        b2.append(", gattResult=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public b31(ma1 ma1, g11 g11, t31 t31) {
        this.a = ma1;
        this.b = g11;
        this.c = t31;
    }
}
