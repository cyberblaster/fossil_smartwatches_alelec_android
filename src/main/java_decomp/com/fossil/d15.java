package com.fossil;

import android.text.TextUtils;
import com.fossil.m24;
import com.fossil.tj4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d15 extends m24<m24.b, a, m24.a> {
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<vx4> a;

        @DexIgnore
        public a(List<vx4> list) {
            wg6.b(list, "apps");
            jk3.a(list, "apps cannot be null!", new Object[0]);
            wg6.a((Object) list, "checkNotNull(apps, \"apps cannot be null!\")");
            this.a = list;
        }

        @DexIgnore
        public final List<vx4> a() {
            return this.a;
        }
    }

    @DexIgnore
    public d15() {
        String simpleName = d15.class.getSimpleName();
        wg6.a((Object) simpleName, "GetApps::class.java.simpleName");
        this.d = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public Object a(m24.b bVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(this.d, "executeUseCase GetApps");
        List allAppFilters = zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<tj4.b> it = tj4.f.a().iterator();
        while (it.hasNext()) {
            tj4.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !xj6.b(next.b(), PortfolioApp.get.instance().getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), hf6.a(false));
                Iterator it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter appFilter = (AppFilter) it2.next();
                    wg6.a((Object) appFilter, "appFilter");
                    if (wg6.a((Object) appFilter.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(appFilter.getDbRowId());
                        installedApp.setCurrentHandGroup(appFilter.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        ud6.c(linkedList);
        return new a(linkedList);
    }

    @DexIgnore
    public String c() {
        return this.d;
    }
}
