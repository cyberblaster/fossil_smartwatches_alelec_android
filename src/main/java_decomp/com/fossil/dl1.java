package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum dl1 {
    GET_FILE((byte) 1),
    LIST_FILE((byte) 2),
    PUT_FILE((byte) 3),
    VERIFY_FILE((byte) 4),
    GET_SIZE_WRITTEN((byte) 5),
    VERIFY_DATA((byte) 6),
    EOF_REACH((byte) 8),
    ABORT_FILE((byte) 9),
    WAITING_REQUEST((byte) 10),
    DELETE_FILE((byte) 11);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public dl1(byte b) {
        this.a = b;
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.a | Byte.MIN_VALUE);
    }
}
