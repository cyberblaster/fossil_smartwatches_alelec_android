package com.fossil;

import android.content.Context;
import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj4 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = qd6.a((T[]) new String[]{"commute-time", "second-timezone"});
    @DexIgnore
    public static /* final */ yj4 b; // = new yj4();

    /*
    static {
        qd6.a((T[]) new String[]{"commute-time", "weather", "chance-of-rain"});
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(String str) {
        wg6.b(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 134170930 && str.equals("second-timezone")) {
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886364);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return a2;
            }
        } else if (str.equals("commute-time")) {
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886202);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        }
        return "";
    }

    @DexIgnore
    public final List<String> b(String str) {
        wg6.b(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode == -829740640 ? !str.equals("commute-time") : hashCode == -48173007 ? !str.equals("chance-of-rain") : hashCode != 1223440372 || !str.equals("weather")) {
            return new ArrayList();
        }
        int i = Build.VERSION.SDK_INT;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationHelper", "android.os.Build.VERSION.SDK_INT=" + i);
        if (i >= 29) {
            return qd6.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
        }
        return qd6.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
    }

    @DexIgnore
    public final boolean c(String str) {
        wg6.b(str, "complicationId");
        return a.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        wg6.b(str, "complicationId");
        List<String> b2 = b(str);
        String[] a2 = xx5.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationHelper", "isPermissionGrantedForComplication " + str + " granted=" + a2 + " required=" + b2);
        for (String a3 : b2) {
            if (!nd6.a((T[]) a2, a3)) {
                return false;
            }
        }
        return true;
    }
}
