package com.fossil;

import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oz4 extends y24<b, c, y24.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.b {
        @DexIgnore
        public /* final */ ContactGroup a;

        @DexIgnore
        public b(ContactGroup contactGroup) {
            wg6.b(contactGroup, "contactGroup");
            this.a = contactGroup;
        }

        @DexIgnore
        public final ContactGroup a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = oz4.class.getSimpleName();
        wg6.a((Object) simpleName, "RemoveContactGroup::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public oz4(NotificationsRepository notificationsRepository) {
        wg6.b(notificationsRepository, "notificationsRepository");
        jk3.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        wg6.a((Object) notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public void a(b bVar) {
        wg6.b(bVar, "requestValues");
        a(bVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .RemoveContactGroup done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(ContactGroup contactGroup) {
        Contact contact = (Contact) contactGroup.getContacts().get(0);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        StringBuilder sb = new StringBuilder();
        sb.append("Removed contact = ");
        wg6.a((Object) contact, "contact");
        sb.append(contact.getFirstName());
        sb.append(" row id = ");
        sb.append(contact.getDbRowId());
        local.d(str, sb.toString());
        ArrayList arrayList = new ArrayList();
        for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
            wg6.a((Object) phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            arrayList.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
        }
        this.d.removeContact(contact);
        this.d.removeContactGroup(contactGroup);
        a((List<? extends PhoneFavoritesContact>) arrayList);
    }

    @DexIgnore
    public final void a(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact removePhoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(removePhoneFavoritesContact);
        }
    }
}
