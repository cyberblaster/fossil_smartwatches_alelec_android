package com.fossil;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u4<K, V> {
    @DexIgnore
    public u4<K, V>.b a;
    @DexIgnore
    public u4<K, V>.c b;
    @DexIgnore
    public u4<K, V>.e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public a(int i) {
            this.a = i;
            this.b = u4.this.c();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c < this.b;
        }

        @DexIgnore
        public T next() {
            if (hasNext()) {
                T a2 = u4.this.a(this.c, this.a);
                this.c++;
                this.d = true;
                return a2;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            if (this.d) {
                this.c--;
                this.b--;
                this.d = false;
                u4.this.a(this.c);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean a(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ boolean add(Object obj) {
            a((Map.Entry) obj);
            throw null;
        }

        @DexIgnore
        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int c = u4.this.c();
            for (Map.Entry entry : collection) {
                u4.this.a(entry.getKey(), entry.getValue());
            }
            return c != u4.this.c();
        }

        @DexIgnore
        public void clear() {
            u4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int a2 = u4.this.a(entry.getKey());
            if (a2 < 0) {
                return false;
            }
            return r4.a(u4.this.a(a2, 1), entry.getValue());
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return u4.a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            for (int c = u4.this.c() - 1; c >= 0; c--) {
                Object a2 = u4.this.a(c, 0);
                Object a3 = u4.this.a(c, 1);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                if (a3 == null) {
                    i2 = 0;
                } else {
                    i2 = a3.hashCode();
                }
                i3 += i ^ i2;
            }
            return i3;
        }

        @DexIgnore
        public boolean isEmpty() {
            return u4.this.c() == 0;
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return new d();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public int size() {
            return u4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements Set<K> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            u4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return u4.this.a(obj) >= 0;
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            return u4.a(u4.this.b(), collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return u4.a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2 = 0;
            for (int c = u4.this.c() - 1; c >= 0; c--) {
                Object a2 = u4.this.a(c, 0);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                i2 += i;
            }
            return i2;
        }

        @DexIgnore
        public boolean isEmpty() {
            return u4.this.c() == 0;
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return new a(0);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int a2 = u4.this.a(obj);
            if (a2 < 0) {
                return false;
            }
            u4.this.a(a2);
            return true;
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            return u4.b(u4.this.b(), collection);
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            return u4.c(u4.this.b(), collection);
        }

        @DexIgnore
        public int size() {
            return u4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            return u4.this.b(0);
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            return u4.this.a(tArr, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public d() {
            this.a = u4.this.c() - 1;
            this.b = -1;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!this.c) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!r4.a(entry.getKey(), u4.this.a(this.b, 0)) || !r4.a(entry.getValue(), u4.this.a(this.b, 1))) {
                    return false;
                }
                return true;
            }
        }

        @DexIgnore
        public K getKey() {
            if (this.c) {
                return u4.this.a(this.b, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public V getValue() {
            if (this.c) {
                return u4.this.a(this.b, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.a;
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.c) {
                int i2 = 0;
                Object a2 = u4.this.a(this.b, 0);
                Object a3 = u4.this.a(this.b, 1);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                if (a3 != null) {
                    i2 = a3.hashCode();
                }
                return i ^ i2;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public void remove() {
            if (this.c) {
                u4.this.a(this.b);
                this.b--;
                this.a--;
                this.c = false;
                return;
            }
            throw new IllegalStateException();
        }

        @DexIgnore
        public V setValue(V v) {
            if (this.c) {
                return u4.this.a(this.b, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public String toString() {
            return getKey() + "=" + getValue();
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            if (hasNext()) {
                this.b++;
                this.c = true;
                return this;
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Collection<V> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            u4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return u4.this.b(obj) >= 0;
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean isEmpty() {
            return u4.this.c() == 0;
        }

        @DexIgnore
        public Iterator<V> iterator() {
            return new a(1);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int b = u4.this.b(obj);
            if (b < 0) {
                return false;
            }
            u4.this.a(b);
            return true;
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            int c = u4.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (collection.contains(u4.this.a(i, 1))) {
                    u4.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            int c = u4.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (!collection.contains(u4.this.a(i, 1))) {
                    u4.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public int size() {
            return u4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            return u4.this.b(1);
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            return u4.this.a(tArr, 1);
        }
    }

    @DexIgnore
    public static <K, V> boolean a(Map<K, V> map, Collection<?> collection) {
        for (Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <K, V> boolean b(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        for (Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    @DexIgnore
    public static <K, V> boolean c(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    @DexIgnore
    public abstract int a(Object obj);

    @DexIgnore
    public abstract Object a(int i, int i2);

    @DexIgnore
    public abstract V a(int i, V v);

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(K k, V v);

    @DexIgnore
    public abstract int b(Object obj);

    @DexIgnore
    public abstract Map<K, V> b();

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public Set<Map.Entry<K, V>> d() {
        if (this.a == null) {
            this.a = new b();
        }
        return this.a;
    }

    @DexIgnore
    public Set<K> e() {
        if (this.b == null) {
            this.b = new c();
        }
        return this.b;
    }

    @DexIgnore
    public Collection<V> f() {
        if (this.c == null) {
            this.c = new e();
        }
        return this.c;
    }

    @DexIgnore
    public <T> T[] a(T[] tArr, int i) {
        int c2 = c();
        if (tArr.length < c2) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), c2);
        }
        for (int i2 = 0; i2 < c2; i2++) {
            tArr[i2] = a(i2, i);
        }
        if (tArr.length > c2) {
            tArr[c2] = null;
        }
        return tArr;
    }

    @DexIgnore
    public Object[] b(int i) {
        int c2 = c();
        Object[] objArr = new Object[c2];
        for (int i2 = 0; i2 < c2; i2++) {
            objArr[i2] = a(i2, i);
        }
        return objArr;
    }

    @DexIgnore
    public static <T> boolean a(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }
}
