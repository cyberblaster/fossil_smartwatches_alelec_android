package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cd3<TResult> implements kd3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public kc3<TResult> c;

    @DexIgnore
    public cd3(Executor executor, kc3<TResult> kc3) {
        this.a = executor;
        this.c = kc3;
    }

    @DexIgnore
    public final void onComplete(qc3<TResult> qc3) {
        synchronized (this.b) {
            if (this.c != null) {
                this.a.execute(new dd3(this, qc3));
            }
        }
    }
}
