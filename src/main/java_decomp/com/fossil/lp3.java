package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.u12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public lp3(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        w12.b(!u42.a(str), "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    @DexIgnore
    public static lp3 a(Context context) {
        c22 c22 = new c22(context);
        String a2 = c22.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new lp3(a2, c22.a("google_api_key"), c22.a("firebase_database_url"), c22.a("ga_trackingId"), c22.a("gcm_defaultSenderId"), c22.a("google_storage_bucket"), c22.a("project_id"));
    }

    @DexIgnore
    public String b() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof lp3)) {
            return false;
        }
        lp3 lp3 = (lp3) obj;
        if (!u12.a(this.b, lp3.b) || !u12.a(this.a, lp3.a) || !u12.a(this.c, lp3.c) || !u12.a(this.d, lp3.d) || !u12.a(this.e, lp3.e) || !u12.a(this.f, lp3.f) || !u12.a(this.g, lp3.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("applicationId", this.b);
        a2.a("apiKey", this.a);
        a2.a("databaseUrl", this.c);
        a2.a("gcmSenderId", this.e);
        a2.a("storageBucket", this.f);
        a2.a("projectId", this.g);
        return a2.toString();
    }

    @DexIgnore
    public String a() {
        return this.b;
    }
}
