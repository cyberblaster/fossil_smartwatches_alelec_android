package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pc2 extends qe2<md2> {
    @DexIgnore
    public static /* final */ je2 E; // = je2.FIT_SESSIONS;
    @DexIgnore
    public static /* final */ rv1.g<pc2> F; // = new rv1.g<>();
    @DexIgnore
    public static /* final */ rv1<rv1.d.C0044d> G; // = new rv1<>("Fitness.SESSIONS_API", new qc2(), F);
    @DexIgnore
    public static /* final */ rv1<rv1.d.b> H; // = new rv1<>("Fitness.SESSIONS_CLIENT", new sc2(), F);

    @DexIgnore
    public pc2(Context context, Looper looper, e12 e12, wv1.b bVar, wv1.c cVar) {
        super(context, looper, E, bVar, cVar, e12);
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.fitness.SessionsApi";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
        if (queryLocalInterface instanceof md2) {
            return (md2) queryLocalInterface;
        }
        return new ld2(iBinder);
    }

    @DexIgnore
    public final int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi";
    }
}
