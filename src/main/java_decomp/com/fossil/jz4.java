package com.fossil;

import android.content.Context;
import androidx.lifecycle.MutableLiveData;
import com.fossil.bz4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz4 extends ez4 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public bz4 f;
    @DexIgnore
    public /* final */ fz4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<bz4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ jz4 a;

        @DexIgnore
        public b(jz4 jz4) {
            this.a = jz4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bz4.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String i = jz4.h;
            local.d(i, "NotificationSettingChanged value = " + aVar);
            this.a.e = aVar.b();
            this.a.g.n(aVar.a());
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = jz4.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationSettingsType\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public jz4(fz4 fz4) {
        wg6.b(fz4, "mView");
        this.g = fz4;
    }

    @DexIgnore
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "start: isCall = " + this.e);
        bz4 bz4 = this.f;
        if (bz4 != null) {
            MutableLiveData<bz4.a> a2 = bz4.a();
            fz4 fz4 = this.g;
            if (fz4 != null) {
                a2.a((NotificationSettingsTypeFragment) fz4, new b(this));
                this.g.a(a(this.e));
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        wg6.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
        bz4 bz4 = this.f;
        if (bz4 != null) {
            MutableLiveData<bz4.a> a2 = bz4.a();
            fz4 fz4 = this.g;
            if (fz4 != null) {
                a2.a((NotificationSettingsTypeFragment) fz4);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        wg6.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        this.g.a(this);
    }

    @DexIgnore
    public void a(bz4 bz4) {
        wg6.b(bz4, "viewModel");
        this.f = bz4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(boolean z) {
        if (z) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886094);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026ngs_Text__AllowCallsFrom)");
            return a2;
        }
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886095);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026_Text__AllowMessagesFrom)");
        return a3;
    }

    @DexIgnore
    public void a(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "changeNotificationSettingsTypeTo: settingsType = " + i);
        bz4.a aVar = new bz4.a(i, this.e);
        bz4 bz4 = this.f;
        if (bz4 != null) {
            bz4.a().a(aVar);
        } else {
            wg6.d("mNotificationSettingViewModel");
            throw null;
        }
    }
}
