package com.fossil;

import android.app.Activity;
import android.content.IntentSender;
import android.util.Log;
import com.fossil.ew1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cw1<R extends ew1> extends gw1<R> {
    @DexIgnore
    public /* final */ Activity a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public cw1(Activity activity, int i) {
        w12.a(activity, (Object) "Activity must not be null");
        this.a = activity;
        this.b = i;
    }

    @DexIgnore
    public final void a(Status status) {
        if (status.D()) {
            try {
                status.a(this.a, this.b);
            } catch (IntentSender.SendIntentException e) {
                Log.e("ResolvingResultCallback", "Failed to start resolution", e);
                b(new Status(8));
            }
        } else {
            b(status);
        }
    }

    @DexIgnore
    public abstract void b(Status status);
}
