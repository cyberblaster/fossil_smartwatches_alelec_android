package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z12 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<z12> CREATOR; // = new i32();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    @Deprecated
    public /* final */ Scope[] d;

    @DexIgnore
    public z12(int i, int i2, int i3, Scope[] scopeArr) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = scopeArr;
    }

    @DexIgnore
    public int B() {
        return this.c;
    }

    @DexIgnore
    @Deprecated
    public Scope[] C() {
        return this.d;
    }

    @DexIgnore
    public int p() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, p());
        g22.a(parcel, 3, B());
        g22.a(parcel, 4, (T[]) C(), i, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public z12(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, (Scope[]) null);
    }
}
