package com.fossil;

import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok5 implements Factory<mk5> {
    @DexIgnore
    public /* final */ Provider<ft4> a;
    @DexIgnore
    public /* final */ Provider<dt4> b;

    @DexIgnore
    public ok5(Provider<ft4> provider, Provider<dt4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ok5 a(Provider<ft4> provider, Provider<dt4> provider2) {
        return new ok5(provider, provider2);
    }

    @DexIgnore
    public static mk5 b(Provider<ft4> provider, Provider<dt4> provider2) {
        return new ProfileEditViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public ProfileEditViewModel get() {
        return b(this.a, this.b);
    }
}
