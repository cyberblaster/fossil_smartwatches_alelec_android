package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv5 implements Factory<uv5> {
    @DexIgnore
    public static FindDevicePresenter a(ce ceVar, DeviceRepository deviceRepository, an4 an4, rv5 rv5, ks4 ks4, ns4 ns4, GetAddress getAddress, os4 os4, PortfolioApp portfolioApp) {
        return new FindDevicePresenter(ceVar, deviceRepository, an4, rv5, ks4, ns4, getAddress, os4, portfolioApp);
    }
}
