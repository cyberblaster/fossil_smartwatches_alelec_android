package com.fossil;

import android.view.View;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface k45 extends xt4<j45> {
    @DexIgnore
    void a(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2);

    @DexIgnore
    void a(List<m35> list, DianaComplicationRingStyle dianaComplicationRingStyle);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b(boolean z, boolean z2);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void e(int i);

    @DexIgnore
    void f(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j();

    @DexIgnore
    void m();

    @DexIgnore
    void n();

    @DexIgnore
    void v();
}
