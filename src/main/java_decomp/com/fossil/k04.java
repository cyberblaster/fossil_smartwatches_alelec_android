package com.fossil;

import com.fossil.h04;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k04 {
    @DexIgnore
    public static /* final */ int[] a; // = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[g04.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[g04.NUMERIC.ordinal()] = 1;
            a[g04.ALPHANUMERIC.ordinal()] = 2;
            a[g04.BYTE.ordinal()] = 3;
            try {
                a[g04.KANJI.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public static int a(j04 j04) {
        return l04.a(j04) + l04.b(j04) + l04.c(j04) + l04.d(j04);
    }

    @DexIgnore
    public static void b(CharSequence charSequence, hy3 hy3) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int charAt = charSequence.charAt(i) - '0';
            int i2 = i + 2;
            if (i2 < length) {
                hy3.a((charAt * 100) + ((charSequence.charAt(i + 1) - '0') * 10) + (charSequence.charAt(i2) - '0'), 10);
                i += 3;
            } else {
                i++;
                if (i < length) {
                    hy3.a((charAt * 10) + (charSequence.charAt(i) - '0'), 7);
                    i = i2;
                } else {
                    hy3.a(charAt, 4);
                }
            }
        }
    }

    @DexIgnore
    public static n04 a(String str, f04 f04, Map<sx3, ?> map) throws yx3 {
        String str2;
        h04 h04;
        jy3 characterSetECIByName;
        if (map == null || !map.containsKey(sx3.CHARACTER_SET)) {
            str2 = "ISO-8859-1";
        } else {
            str2 = map.get(sx3.CHARACTER_SET).toString();
        }
        g04 a2 = a(str, str2);
        hy3 hy3 = new hy3();
        if (a2 == g04.BYTE && !"ISO-8859-1".equals(str2) && (characterSetECIByName = jy3.getCharacterSetECIByName(str2)) != null) {
            a(characterSetECIByName, hy3);
        }
        a(a2, hy3);
        hy3 hy32 = new hy3();
        a(str, a2, hy32, str2);
        if (map == null || !map.containsKey(sx3.QR_VERSION)) {
            h04 = a(f04, a2, hy3, hy32);
        } else {
            h04 = h04.a(Integer.parseInt(map.get(sx3.QR_VERSION).toString()));
            if (!a(a(a2, hy3, hy32, h04), h04, f04)) {
                throw new yx3("Data too big for requested version");
            }
        }
        hy3 hy33 = new hy3();
        hy33.a(hy3);
        a(a2 == g04.BYTE ? hy32.b() : str.length(), h04, a2, hy33);
        hy33.a(hy32);
        h04.b a3 = h04.a(f04);
        int b = h04.b() - a3.d();
        a(b, hy33);
        hy3 a4 = a(hy33, h04.b(), b, a3.c());
        n04 n04 = new n04();
        n04.a(f04);
        n04.a(a2);
        n04.a(h04);
        int a5 = h04.a();
        j04 j04 = new j04(a5, a5);
        int a6 = a(a4, f04, h04, j04);
        n04.a(a6);
        m04.a(a4, f04, h04, a6, j04);
        n04.a(j04);
        return n04;
    }

    @DexIgnore
    public static h04 a(f04 f04, g04 g04, hy3 hy3, hy3 hy32) throws yx3 {
        return a(a(g04, hy3, hy32, a(a(g04, hy3, hy32, h04.a(1)), f04)), f04);
    }

    @DexIgnore
    public static int a(g04 g04, hy3 hy3, hy3 hy32, h04 h04) {
        return hy3.a() + g04.getCharacterCountBits(h04) + hy32.a();
    }

    @DexIgnore
    public static int a(int i) {
        int[] iArr = a;
        if (i < iArr.length) {
            return iArr[i];
        }
        return -1;
    }

    @DexIgnore
    public static g04 a(String str, String str2) {
        if ("Shift_JIS".equals(str2) && a(str)) {
            return g04.KANJI;
        }
        boolean z = false;
        boolean z2 = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                z2 = true;
            } else if (a((int) charAt) == -1) {
                return g04.BYTE;
            } else {
                z = true;
            }
        }
        if (z) {
            return g04.ALPHANUMERIC;
        }
        if (z2) {
            return g04.NUMERIC;
        }
        return g04.BYTE;
    }

    @DexIgnore
    public static boolean a(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                byte b = bytes[i] & 255;
                if ((b < 129 || b > 159) && (b < 224 || b > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    @DexIgnore
    public static int a(hy3 hy3, f04 f04, h04 h04, j04 j04) throws yx3 {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        for (int i3 = 0; i3 < 8; i3++) {
            m04.a(hy3, f04, h04, i3, j04);
            int a2 = a(j04);
            if (a2 < i) {
                i2 = i3;
                i = a2;
            }
        }
        return i2;
    }

    @DexIgnore
    public static h04 a(int i, f04 f04) throws yx3 {
        for (int i2 = 1; i2 <= 40; i2++) {
            h04 a2 = h04.a(i2);
            if (a(i, a2, f04)) {
                return a2;
            }
        }
        throw new yx3("Data too big");
    }

    @DexIgnore
    public static boolean a(int i, h04 h04, f04 f04) {
        return h04.b() - h04.a(f04).d() >= (i + 7) / 8;
    }

    @DexIgnore
    public static void a(int i, hy3 hy3) throws yx3 {
        int i2 = i << 3;
        if (hy3.a() <= i2) {
            for (int i3 = 0; i3 < 4 && hy3.a() < i2; i3++) {
                hy3.a(false);
            }
            int a2 = hy3.a() & 7;
            if (a2 > 0) {
                while (a2 < 8) {
                    hy3.a(false);
                    a2++;
                }
            }
            int b = i - hy3.b();
            for (int i4 = 0; i4 < b; i4++) {
                hy3.a((i4 & 1) == 0 ? 236 : 17, 8);
            }
            if (hy3.a() != i2) {
                throw new yx3("Bits size does not equal capacity");
            }
            return;
        }
        throw new yx3("data bits cannot fit in the QR Code" + hy3.a() + " > " + i2);
    }

    @DexIgnore
    public static void a(int i, int i2, int i3, int i4, int[] iArr, int[] iArr2) throws yx3 {
        if (i4 < i3) {
            int i5 = i % i3;
            int i6 = i3 - i5;
            int i7 = i / i3;
            int i8 = i7 + 1;
            int i9 = i2 / i3;
            int i10 = i9 + 1;
            int i11 = i7 - i9;
            int i12 = i8 - i10;
            if (i11 != i12) {
                throw new yx3("EC bytes mismatch");
            } else if (i3 != i6 + i5) {
                throw new yx3("RS blocks mismatch");
            } else if (i != ((i9 + i11) * i6) + ((i10 + i12) * i5)) {
                throw new yx3("Total bytes mismatch");
            } else if (i4 < i6) {
                iArr[0] = i9;
                iArr2[0] = i11;
            } else {
                iArr[0] = i10;
                iArr2[0] = i12;
            }
        } else {
            throw new yx3("Block ID too large");
        }
    }

    @DexIgnore
    public static hy3 a(hy3 hy3, int i, int i2, int i3) throws yx3 {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (hy3.b() == i5) {
            ArrayList<i04> arrayList = new ArrayList<>(i6);
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            for (int i10 = 0; i10 < i6; i10++) {
                int[] iArr = new int[1];
                int[] iArr2 = new int[1];
                a(i, i2, i3, i10, iArr, iArr2);
                int i11 = iArr[0];
                byte[] bArr = new byte[i11];
                hy3.a(i7 << 3, bArr, 0, i11);
                byte[] a2 = a(bArr, iArr2[0]);
                arrayList.add(new i04(bArr, a2));
                i8 = Math.max(i8, i11);
                i9 = Math.max(i9, a2.length);
                i7 += iArr[0];
            }
            if (i5 == i7) {
                hy3 hy32 = new hy3();
                for (int i12 = 0; i12 < i8; i12++) {
                    for (i04 a3 : arrayList) {
                        byte[] a4 = a3.a();
                        if (i12 < a4.length) {
                            hy32.a(a4[i12], 8);
                        }
                    }
                }
                for (int i13 = 0; i13 < i9; i13++) {
                    for (i04 b : arrayList) {
                        byte[] b2 = b.b();
                        if (i13 < b2.length) {
                            hy32.a(b2[i13], 8);
                        }
                    }
                }
                if (i4 == hy32.b()) {
                    return hy32;
                }
                throw new yx3("Interleaving error: " + i4 + " and " + hy32.b() + " differ.");
            }
            throw new yx3("Data bytes does not match offset");
        }
        throw new yx3("Number of bits and data bytes does not match");
    }

    @DexIgnore
    public static byte[] a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        new my3(ky3.k).a(iArr, i);
        byte[] bArr2 = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr2[i3] = (byte) iArr[length + i3];
        }
        return bArr2;
    }

    @DexIgnore
    public static void a(g04 g04, hy3 hy3) {
        hy3.a(g04.getBits(), 4);
    }

    @DexIgnore
    public static void a(int i, h04 h04, g04 g04, hy3 hy3) throws yx3 {
        int characterCountBits = g04.getCharacterCountBits(h04);
        int i2 = 1 << characterCountBits;
        if (i < i2) {
            hy3.a(i, characterCountBits);
            return;
        }
        throw new yx3(i + " is bigger than " + (i2 - 1));
    }

    @DexIgnore
    public static void a(String str, g04 g04, hy3 hy3, String str2) throws yx3 {
        int i = a.a[g04.ordinal()];
        if (i == 1) {
            b(str, hy3);
        } else if (i == 2) {
            a((CharSequence) str, hy3);
        } else if (i == 3) {
            a(str, hy3, str2);
        } else if (i == 4) {
            a(str, hy3);
        } else {
            throw new yx3("Invalid mode: " + g04);
        }
    }

    @DexIgnore
    public static void a(CharSequence charSequence, hy3 hy3) throws yx3 {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int a2 = a((int) charSequence.charAt(i));
            if (a2 != -1) {
                int i2 = i + 1;
                if (i2 < length) {
                    int a3 = a((int) charSequence.charAt(i2));
                    if (a3 != -1) {
                        hy3.a((a2 * 45) + a3, 11);
                        i += 2;
                    } else {
                        throw new yx3();
                    }
                } else {
                    hy3.a(a2, 6);
                    i = i2;
                }
            } else {
                throw new yx3();
            }
        }
    }

    @DexIgnore
    public static void a(String str, hy3 hy3, String str2) throws yx3 {
        try {
            for (byte a2 : str.getBytes(str2)) {
                hy3.a(a2, 8);
            }
        } catch (UnsupportedEncodingException e) {
            throw new yx3((Throwable) e);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035 A[LOOP:0: B:4:0x0008->B:17:0x0035, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0044 A[SYNTHETIC] */
    public static void a(String str, hy3 hy3) throws yx3 {
        int i;
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            int i2 = 0;
            while (i2 < length) {
                byte b = ((bytes[i2] & 255) << 8) | (bytes[i2 + 1] & 255);
                byte b2 = 33088;
                if (b < 33088 || b > 40956) {
                    if (b < 57408 || b > 60351) {
                        i = -1;
                        if (i == -1) {
                            hy3.a(((i >> 8) * 192) + (i & 255), 13);
                            i2 += 2;
                        } else {
                            throw new yx3("Invalid byte sequence");
                        }
                    } else {
                        b2 = 49472;
                    }
                }
                i = b - b2;
                if (i == -1) {
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new yx3((Throwable) e);
        }
    }

    @DexIgnore
    public static void a(jy3 jy3, hy3 hy3) {
        hy3.a(g04.ECI.getBits(), 4);
        hy3.a(jy3.getValue(), 8);
    }
}
