package com.fossil;

import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum s40 {
    UNKNOWN(fm0.f.i()),
    DIANA(fm0.f.b()),
    SE1(fm0.f.g()),
    SLIM(fm0.f.h()),
    MINI(fm0.f.e()),
    HELLAS(fm0.f.c()),
    SE0(fm0.f.g()),
    WEAR_OS(new Type[0]),
    IVY(fm0.f.b());
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ Type[] a;

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public s40(Type[] typeArr) {
        this.a = typeArr;
    }

    @DexIgnore
    public final Type[] a() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final s40 a(String str) {
            if (!r40.t.a(str)) {
                return s40.UNKNOWN;
            }
            if (xj6.c(str, "D", false, 2, (Object) null)) {
                return s40.DIANA;
            }
            if (xj6.c(str, "W", false, 2, (Object) null)) {
                return s40.SE1;
            }
            if (xj6.c(str, "L", false, 2, (Object) null)) {
                return s40.SLIM;
            }
            if (xj6.c(str, "M", false, 2, (Object) null)) {
                return s40.MINI;
            }
            if (xj6.c(str, "Y", false, 2, (Object) null)) {
                return s40.HELLAS;
            }
            if (xj6.c(str, "Z", false, 2, (Object) null)) {
                return s40.SE0;
            }
            if (xj6.c(str, "V", false, 2, (Object) null)) {
                return s40.IVY;
            }
            return s40.UNKNOWN;
        }

        @DexIgnore
        public final s40 a(String str, String str2) {
            if (xj6.c(str, "DN", false, 2, (Object) null)) {
                return s40.DIANA;
            }
            if (xj6.c(str, "HW", false, 2, (Object) null)) {
                s40 a = a(str2);
                if (a != s40.UNKNOWN) {
                    return a;
                }
                return s40.SE1;
            } else if (xj6.c(str, "HL", false, 2, (Object) null)) {
                return s40.SLIM;
            } else {
                if (xj6.c(str, "HM", false, 2, (Object) null)) {
                    return s40.MINI;
                }
                if (xj6.c(str, "AW", false, 2, (Object) null)) {
                    return s40.HELLAS;
                }
                if (xj6.c(str, "IV", false, 2, (Object) null)) {
                    return s40.IVY;
                }
                return a(str2);
            }
        }
    }
}
