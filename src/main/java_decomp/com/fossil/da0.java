package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da0 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<da0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new da0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new da0[i];
        }
    }

    @DexIgnore
    public da0(byte b, int i) {
        super(e90.WEATHER_WATCH_APP, b, i);
    }

    @DexIgnore
    public /* synthetic */ da0(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
