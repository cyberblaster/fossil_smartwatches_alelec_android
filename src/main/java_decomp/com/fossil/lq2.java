package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lq2 extends gq2 {
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0061, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b9, code lost:
        return -1;
     */
    @DexIgnore
    public final int a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        long j;
        long j2;
        byte[] bArr2 = bArr;
        int i5 = i2;
        int i6 = i3;
        if ((i5 | i6 | (bArr2.length - i6)) >= 0) {
            long j3 = (long) i5;
            int i7 = (int) (((long) i6) - j3);
            if (i7 >= 16) {
                long j4 = j3;
                i4 = 0;
                while (true) {
                    if (i4 >= i7) {
                        i4 = i7;
                        break;
                    }
                    long j5 = j4 + 1;
                    if (cq2.a(bArr2, j4) < 0) {
                        break;
                    }
                    i4++;
                    j4 = j5;
                }
            } else {
                i4 = 0;
            }
            int i8 = i7 - i4;
            long j6 = j3 + ((long) i4);
            while (true) {
                byte b = 0;
                while (true) {
                    if (i8 <= 0) {
                        j = j6;
                        break;
                    }
                    j = j6 + 1;
                    b = cq2.a(bArr2, j6);
                    if (b < 0) {
                        break;
                    }
                    i8--;
                    j6 = j;
                }
                if (i8 == 0) {
                    return 0;
                }
                int i9 = i8 - 1;
                if (b >= -32) {
                    if (b >= -16) {
                        if (i9 >= 3) {
                            i8 = i9 - 3;
                            long j7 = j + 1;
                            byte a = cq2.a(bArr2, j);
                            if (a > -65 || (((b << 28) + (a + 112)) >> 30) != 0) {
                                break;
                            }
                            long j8 = j7 + 1;
                            if (cq2.a(bArr2, j7) > -65) {
                                break;
                            }
                            j2 = j8 + 1;
                            if (cq2.a(bArr2, j8) > -65) {
                                break;
                            }
                        } else {
                            return a(bArr2, (int) b, j, i9);
                        }
                    } else if (i9 >= 2) {
                        i8 = i9 - 2;
                        long j9 = j + 1;
                        byte a2 = cq2.a(bArr2, j);
                        if (a2 > -65 || ((b == -32 && a2 < -96) || (b == -19 && a2 >= -96))) {
                            break;
                        }
                        long j10 = j9 + 1;
                        if (cq2.a(bArr2, j9) > -65) {
                            break;
                        }
                        j6 = j10;
                    } else {
                        return a(bArr2, (int) b, j, i9);
                    }
                } else if (i9 != 0) {
                    i8 = i9 - 1;
                    if (b < -62) {
                        break;
                    }
                    j2 = j + 1;
                    if (cq2.a(bArr2, j) > -65) {
                        break;
                    }
                } else {
                    return b;
                }
                j6 = j2;
            }
            return -1;
        }
        throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[]{Integer.valueOf(bArr2.length), Integer.valueOf(i2), Integer.valueOf(i3)}));
    }

    @DexIgnore
    public final String b(byte[] bArr, int i, int i2) throws qn2 {
        if ((i | i2 | ((bArr.length - i) - i2)) >= 0) {
            int i3 = i + i2;
            char[] cArr = new char[i2];
            int i4 = 0;
            while (r13 < i3) {
                byte a = cq2.a(bArr, (long) r13);
                if (!hq2.d(a)) {
                    break;
                }
                i = r13 + 1;
                hq2.b(a, cArr, i4);
                i4++;
            }
            int i5 = i4;
            while (r13 < i3) {
                int i6 = r13 + 1;
                byte a2 = cq2.a(bArr, (long) r13);
                if (hq2.d(a2)) {
                    int i7 = i5 + 1;
                    hq2.b(a2, cArr, i5);
                    while (i6 < i3) {
                        byte a3 = cq2.a(bArr, (long) i6);
                        if (!hq2.d(a3)) {
                            break;
                        }
                        i6++;
                        hq2.b(a3, cArr, i7);
                        i7++;
                    }
                    r13 = i6;
                    i5 = i7;
                } else if (hq2.e(a2)) {
                    if (i6 < i3) {
                        int i8 = i6 + 1;
                        hq2.b(a2, cq2.a(bArr, (long) i6), cArr, i5);
                        r13 = i8;
                        i5++;
                    } else {
                        throw qn2.zzh();
                    }
                } else if (hq2.f(a2)) {
                    if (i6 < i3 - 1) {
                        int i9 = i6 + 1;
                        int i10 = i9 + 1;
                        hq2.b(a2, cq2.a(bArr, (long) i6), cq2.a(bArr, (long) i9), cArr, i5);
                        r13 = i10;
                        i5++;
                    } else {
                        throw qn2.zzh();
                    }
                } else if (i6 < i3 - 2) {
                    int i11 = i6 + 1;
                    byte a4 = cq2.a(bArr, (long) i6);
                    int i12 = i11 + 1;
                    hq2.b(a2, a4, cq2.a(bArr, (long) i11), cq2.a(bArr, (long) i12), cArr, i5);
                    r13 = i12 + 1;
                    i5 = i5 + 1 + 1;
                } else {
                    throw qn2.zzh();
                }
            }
            return new String(cArr, 0, i5);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)}));
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0033 A[LOOP:1: B:13:0x0033->B:38:0x00fb, LOOP_START, PHI: r2 r3 r4 r11 
  PHI: (r2v4 int) = (r2v3 int), (r2v6 int) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r3v3 char) = (r3v2 char), (r3v4 char) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r4v3 long) = (r4v2 long), (r4v5 long) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r11v3 long) = (r11v2 long), (r11v4 long) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]] */
    @DexIgnore
    public final int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        long j;
        long j2;
        int i3;
        char charAt;
        CharSequence charSequence2 = charSequence;
        byte[] bArr2 = bArr;
        int i4 = i;
        int i5 = i2;
        long j3 = (long) i4;
        long j4 = ((long) i5) + j3;
        int length = charSequence.length();
        if (length > i5 || bArr2.length - i5 < i4) {
            char charAt2 = charSequence2.charAt(length - 1);
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt2);
            sb.append(" at index ");
            sb.append(i4 + i5);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i6 = 0;
        while (true) {
            char c = 128;
            long j5 = 1;
            if (i6 < length && (charAt = charSequence2.charAt(i6)) < 128) {
                cq2.a(bArr2, j3, (byte) charAt);
                i6++;
                j3 = 1 + j3;
            } else if (i6 != length) {
                return (int) j3;
            } else {
                while (i6 < length) {
                    char charAt3 = charSequence2.charAt(i6);
                    if (charAt3 < c && j3 < j4) {
                        long j6 = j3 + j5;
                        cq2.a(bArr2, j3, (byte) charAt3);
                        j2 = j5;
                        j = j6;
                    } else if (charAt3 < 2048 && j3 <= j4 - 2) {
                        long j7 = j3 + j5;
                        cq2.a(bArr2, j3, (byte) ((charAt3 >>> 6) | 960));
                        cq2.a(bArr2, j7, (byte) ((charAt3 & '?') | 128));
                        j = j7 + j5;
                        j2 = j5;
                        i6++;
                        c = 128;
                        long j8 = j2;
                        j3 = j;
                        j5 = j8;
                    } else if ((charAt3 < 55296 || 57343 < charAt3) && j3 <= j4 - 3) {
                        long j9 = j3 + j5;
                        cq2.a(bArr2, j3, (byte) ((charAt3 >>> 12) | 480));
                        long j10 = j9 + j5;
                        cq2.a(bArr2, j9, (byte) (((charAt3 >>> 6) & 63) | 128));
                        cq2.a(bArr2, j10, (byte) ((charAt3 & '?') | 128));
                        j = j10 + 1;
                        j2 = 1;
                    } else if (j3 <= j4 - 4) {
                        int i7 = i6 + 1;
                        if (i7 != length) {
                            char charAt4 = charSequence2.charAt(i7);
                            if (Character.isSurrogatePair(charAt3, charAt4)) {
                                int codePoint = Character.toCodePoint(charAt3, charAt4);
                                long j11 = j3 + 1;
                                cq2.a(bArr2, j3, (byte) ((codePoint >>> 18) | 240));
                                long j12 = j11 + 1;
                                cq2.a(bArr2, j11, (byte) (((codePoint >>> 12) & 63) | 128));
                                long j13 = j12 + 1;
                                cq2.a(bArr2, j12, (byte) (((codePoint >>> 6) & 63) | 128));
                                j2 = 1;
                                j = j13 + 1;
                                cq2.a(bArr2, j13, (byte) ((codePoint & 63) | 128));
                                i6 = i7;
                                i6++;
                                c = 128;
                                long j82 = j2;
                                j3 = j;
                                j5 = j82;
                            } else {
                                i6 = i7;
                            }
                        }
                        throw new iq2(i6 - 1, length);
                    } else if (55296 > charAt3 || charAt3 > 57343 || ((i3 = i6 + 1) != length && Character.isSurrogatePair(charAt3, charSequence2.charAt(i3)))) {
                        StringBuilder sb2 = new StringBuilder(46);
                        sb2.append("Failed writing ");
                        sb2.append(charAt3);
                        sb2.append(" at index ");
                        sb2.append(j3);
                        throw new ArrayIndexOutOfBoundsException(sb2.toString());
                    } else {
                        throw new iq2(i6, length);
                    }
                    i6++;
                    c = 128;
                    long j822 = j2;
                    j3 = j;
                    j5 = j822;
                }
                return (int) j3;
            }
        }
        if (i6 != length) {
        }
    }

    @DexIgnore
    public static int a(byte[] bArr, int i, long j, int i2) {
        if (i2 == 0) {
            return fq2.b(i);
        }
        if (i2 == 1) {
            return fq2.b(i, cq2.a(bArr, j));
        }
        if (i2 == 2) {
            return fq2.b(i, (int) cq2.a(bArr, j), (int) cq2.a(bArr, j + 1));
        }
        throw new AssertionError();
    }
}
