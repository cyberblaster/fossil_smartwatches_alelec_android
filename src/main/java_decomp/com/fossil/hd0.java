package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hd0 {
    ON,
    OFF,
    TOGGLE;
    
    @DexIgnore
    public static /* final */ a b; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final hd0 a(String str) {
            for (hd0 hd0 : hd0.values()) {
                String a = cw0.a((Enum<?>) hd0);
                String lowerCase = str.toLowerCase(mi0.A.h());
                wg6.a(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (wg6.a(a, lowerCase)) {
                    return hd0;
                }
            }
            return null;
        }
    }

    /*
    static {
        b = new a((qg6) null);
    }
    */
}
