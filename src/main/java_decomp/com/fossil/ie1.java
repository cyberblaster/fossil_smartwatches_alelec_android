package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ie1 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ sa1 CREATOR; // = new sa1((qg6) null);
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte[] e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ boolean i;

    @DexIgnore
    public ie1(String str, byte b2, byte b3, byte[] bArr, long j, long j2, long j3, boolean z) {
        this.b = str;
        this.c = b2;
        this.d = b3;
        this.e = bArr;
        this.f = j;
        this.g = j2;
        this.h = j3;
        this.i = z;
    }

    @DexIgnore
    public static /* synthetic */ ie1 a(ie1 ie1, String str, byte b2, byte b3, byte[] bArr, long j, long j2, long j3, boolean z, int i2) {
        ie1 ie12 = ie1;
        int i3 = i2;
        return ie1.a((i3 & 1) != 0 ? ie12.b : str, (i3 & 2) != 0 ? ie12.c : b2, (i3 & 4) != 0 ? ie12.d : b3, (i3 & 8) != 0 ? ie12.e : bArr, (i3 & 16) != 0 ? ie12.f : j, (i3 & 32) != 0 ? ie12.g : j2, (i3 & 64) != 0 ? ie12.h : j3, (i3 & 128) != 0 ? ie12.i : z);
    }

    @DexIgnore
    public final ie1 a(String str, byte b2, byte b3, byte[] bArr, long j, long j2, long j3, boolean z) {
        return new ie1(str, b2, b3, bArr, j, j2, j3, z);
    }

    @DexIgnore
    public JSONObject a() {
        return a(true);
    }

    @DexIgnore
    public final long b() {
        return this.h;
    }

    @DexIgnore
    public final short c() {
        return ByteBuffer.allocate(2).put(this.c).put(this.d).getShort(0);
    }

    @DexIgnore
    public final boolean d() {
        return this.i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(ie1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ie1 ie1 = (ie1) obj;
            return !(wg6.a(this.b, ie1.b) ^ true) && this.c == ie1.c && this.d == ie1.d && Arrays.equals(this.e, ie1.e) && this.f == ie1.f && this.g == ie1.g && this.h == ie1.h && this.i == ie1.i;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.database.entity.DeviceFile");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.e);
        int hashCode2 = Long.valueOf(this.f).hashCode();
        int hashCode3 = Long.valueOf(this.g).hashCode();
        int hashCode4 = Long.valueOf(this.h).hashCode();
        return Boolean.valueOf(this.i).hashCode() + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (((((this.b.hashCode() * 31) + this.c) * 31) + this.d) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("DeviceFile(deviceMacAddress=");
        b2.append(this.b);
        b2.append(", fileType=");
        b2.append(this.c);
        b2.append(", fileIndex=");
        b2.append(this.d);
        b2.append(", rawData=");
        b2.append(Arrays.toString(this.e));
        b2.append(", fileLength=");
        b2.append(this.f);
        b2.append(", fileCrc=");
        b2.append(this.g);
        b2.append(", createdTimeStamp=");
        b2.append(this.h);
        b2.append(", isCompleted=");
        b2.append(this.i);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.e);
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i ? 1 : 0);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ie1(String str, short s, long j, long j2) {
        this(str, CREATOR.b(r0), CREATOR.a(r0), new byte[0], j, j2, 0, false);
        short s2 = s;
    }

    @DexIgnore
    public final JSONObject a(boolean z) {
        JSONObject a2 = cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.MAC_ADDRESS, (Object) this.b), bm0.FILE_HANDLE, (Object) cw0.a(c())), bm0.FILE_SIZE, (Object) Long.valueOf(this.f)), bm0.FILE_CRC, (Object) Long.valueOf(this.g)), bm0.RAW_DATA_LENGTH, (Object) Integer.valueOf(this.e.length)), bm0.CREATED_AT, (Object) Double.valueOf(cw0.a(this.h))), bm0.IS_COMPLETED, (Object) Boolean.valueOf(this.i)), bm0.FILE_HANDLE_DESCRIPTION, (Object) g01.d.a(c()));
        if (z) {
            cw0.a(a2, bm0.RAW_DATA, (Object) cw0.a(this.e, (String) null, 1));
        }
        w31 a3 = w31.x.a(this.c);
        if (a3 != null) {
            int i2 = nc1.a[a3.ordinal()];
            if (i2 == 1) {
                cw0.a(a2, bm0.ABSOLUTE_FILE_NUMBER, (Object) -1);
                byte[] bArr = this.e;
                if (bArr.length >= 18) {
                    ByteBuffer order = ByteBuffer.wrap(md6.a(bArr, 16, 18)).order(ByteOrder.LITTLE_ENDIAN);
                    wg6.a(order, "byteBuffer");
                    cw0.a(a2, bm0.ABSOLUTE_FILE_NUMBER, (Object) Integer.valueOf(cw0.b(order.getShort())));
                }
            } else if (i2 == 2) {
                cw0.a(a2, bm0.FILE_VERSION, (Object) "unknown");
                cw0.a(a2, bm0.ABSOLUTE_FILE_NUMBER, (Object) -1);
                int i3 = 0;
                while (true) {
                    int i4 = i3 + 16;
                    byte[] bArr2 = this.e;
                    if (i4 > bArr2.length) {
                        break;
                    }
                    byte[] a4 = md6.a(bArr2, i3, i4);
                    byte b2 = a4[0];
                    if (b2 != -59) {
                        if (b2 == 0) {
                            bm0 bm0 = bm0.FILE_VERSION;
                            StringBuilder sb = new StringBuilder();
                            sb.append(cw0.b(a4[1]));
                            sb.append('.');
                            sb.append(cw0.b(a4[2]));
                            cw0.a(a2, bm0, (Object) sb.toString());
                        }
                    } else if (a4[1] == 9) {
                        ByteBuffer order2 = ByteBuffer.wrap(md6.a(md6.a(a4, 2, 5), (byte) 0)).order(ByteOrder.LITTLE_ENDIAN);
                        bm0 bm02 = bm0.ABSOLUTE_FILE_NUMBER;
                        wg6.a(order2, "byteBuffer");
                        cw0.a(a2, bm02, (Object) Integer.valueOf(order2.getInt()));
                    }
                    i3 = i4;
                }
            }
        }
        return a2;
    }
}
