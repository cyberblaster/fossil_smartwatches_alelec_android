package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wd2 implements a72 {
    @DexIgnore
    public final yv1<Status> a(wv1 wv1, DataSet dataSet) {
        w12.a(dataSet, (Object) "Must set the data set");
        w12.b(!dataSet.B().isEmpty(), "Cannot use an empty data set");
        w12.a(dataSet.C().J(), (Object) "Must set the app package name for the data source");
        return wv1.a(new vd2(this, wv1, dataSet, false));
    }
}
