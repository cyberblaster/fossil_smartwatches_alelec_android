package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o00 extends FilterInputStream {
    @DexIgnore
    public int a; // = RecyclerView.UNDEFINED_DURATION;

    @DexIgnore
    public o00(InputStream inputStream) {
        super(inputStream);
    }

    @DexIgnore
    public final long a(long j) {
        int i = this.a;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    @DexIgnore
    public int available() throws IOException {
        int i = this.a;
        if (i == Integer.MIN_VALUE) {
            return super.available();
        }
        return Math.min(i, super.available());
    }

    @DexIgnore
    public final void b(long j) {
        int i = this.a;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.a = (int) (((long) i) - j);
        }
    }

    @DexIgnore
    public synchronized void mark(int i) {
        super.mark(i);
        this.a = i;
    }

    @DexIgnore
    public int read() throws IOException {
        if (a(1) == -1) {
            return -1;
        }
        int read = super.read();
        b(1);
        return read;
    }

    @DexIgnore
    public synchronized void reset() throws IOException {
        super.reset();
        this.a = RecyclerView.UNDEFINED_DURATION;
    }

    @DexIgnore
    public long skip(long j) throws IOException {
        long a2 = a(j);
        if (a2 == -1) {
            return 0;
        }
        long skip = super.skip(a2);
        b(skip);
        return skip;
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int a2 = (int) a((long) i2);
        if (a2 == -1) {
            return -1;
        }
        int read = super.read(bArr, i, a2);
        b((long) read);
        return read;
    }
}
