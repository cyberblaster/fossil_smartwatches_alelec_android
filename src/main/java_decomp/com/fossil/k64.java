package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k64 extends j64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(2131362442, 1);
        w.put(2131363266, 2);
        w.put(2131362406, 3);
        w.put(2131363267, 4);
        w.put(2131362436, 5);
        w.put(2131363268, 6);
        w.put(2131362307, 7);
    }
    */

    @DexIgnore
    public k64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 8, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public k64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[7], objArr[3], objArr[5], objArr[1], objArr[2], objArr[4], objArr[6]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
