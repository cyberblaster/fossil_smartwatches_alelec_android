package com.fossil;

import android.location.Location;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$Listener$onLocationResult$1", f = "WatchAppCommuteTimeManager.kt", l = {}, m = "invokeSuspend")
public final class pq4$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Location $location;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pq4$b$a(WatchAppCommuteTimeManager.b bVar, Location location, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$location = location;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pq4$b$a pq4_b_a = new pq4$b$a(this.this$0, this.$location, xe6);
        pq4_b_a.p$ = (il6) obj;
        return pq4_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pq4$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            WatchAppCommuteTimeManager.this.a(this.$location.getLatitude(), this.$location.getLongitude());
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
