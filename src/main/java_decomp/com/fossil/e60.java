package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e60 extends x50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<e60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public e60 createFromParcel(Parcel parcel) {
            return new e60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new e60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m12createFromParcel(Parcel parcel) {
            return new e60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public e60(kc0 kc0) {
        super(z50.SECOND_TIMEZONE, kc0, (lc0) null, (mc0) null, 12);
    }

    @DexIgnore
    public final kc0 getDataConfig() {
        ic0 ic0 = this.b;
        if (ic0 != null) {
            return (kc0) ic0;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ e60(kc0 kc0, lc0 lc0, mc0 mc0, int i, qg6 qg6) {
        this(kc0, lc0, (i & 4) != 0 ? new mc0(mc0.CREATOR.a()) : mc0);
    }

    @DexIgnore
    public e60(kc0 kc0, lc0 lc0, mc0 mc0) {
        super(z50.SECOND_TIMEZONE, kc0, lc0, mc0);
    }

    @DexIgnore
    public /* synthetic */ e60(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
