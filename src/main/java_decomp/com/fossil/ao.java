package com.fossil;

import com.fossil.am;
import com.fossil.zn;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ao {
    @DexIgnore
    int a(am.a aVar, String... strArr);

    @DexIgnore
    int a(String str, long j);

    @DexIgnore
    List<zn> a();

    @DexIgnore
    List<zn> a(int i);

    @DexIgnore
    List<zn.b> a(String str);

    @DexIgnore
    void a(zn znVar);

    @DexIgnore
    void a(String str, pl plVar);

    @DexIgnore
    List<zn> b();

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(String str, long j);

    @DexIgnore
    List<String> c();

    @DexIgnore
    List<String> c(String str);

    @DexIgnore
    int d();

    @DexIgnore
    am.a d(String str);

    @DexIgnore
    zn e(String str);

    @DexIgnore
    int f(String str);

    @DexIgnore
    List<pl> g(String str);

    @DexIgnore
    int h(String str);
}
