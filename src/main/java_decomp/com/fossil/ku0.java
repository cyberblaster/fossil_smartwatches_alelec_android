package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku0 extends n40 {
    @DexIgnore
    public /* final */ String[] a;

    @DexIgnore
    public ku0(String[] strArr) {
        this.a = strArr;
    }

    @DexIgnore
    public boolean a(ii1 ii1) {
        if (this.a.length == 0) {
            return true;
        }
        String serialNumber = ii1.r.getSerialNumber();
        for (String c : this.a) {
            if (xj6.c(serialNumber, c, false, 2, (Object) null)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.FILTER_TYPE, (Object) "serial_number_prefixes"), bm0.SERIAL_NUMBER_PREFIXES, (Object) cw0.a(this.a));
    }
}
