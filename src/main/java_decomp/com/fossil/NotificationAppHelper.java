package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.tj4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppHelper {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ NotificationAppHelper b; // = new NotificationAppHelper();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2", f = "NotificationAppHelper.kt", l = {150, 151}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super List<AppNotificationFilter>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ mz4 $getAllContactGroup;
        @DexIgnore
        public /* final */ /* synthetic */ d15 $getApps;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsDatabase $notificationSettingsDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ an4 $sharedPrefs;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(an4 an4, NotificationSettingsDatabase notificationSettingsDatabase, mz4 mz4, d15 d15, xe6 xe6) {
            super(2, xe6);
            this.$sharedPrefs = an4;
            this.$notificationSettingsDatabase = notificationSettingsDatabase;
            this.$getAllContactGroup = mz4;
            this.$getApps = d15;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            List list;
            Object obj2;
            List list2;
            rl6 rl6;
            rl6 rl62;
            hh6 hh6;
            hh6 hh62;
            NotificationSettingsModel notificationSettingsModel;
            boolean z;
            NotificationSettingsModel notificationSettingsModel2;
            il6 il6;
            Object obj3;
            List list3;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                hh6 = new hh6();
                hh6.element = 0;
                hh62 = new hh6();
                hh62.element = 0;
                list = new ArrayList();
                z = this.$sharedPrefs.B();
                notificationSettingsModel2 = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
                notificationSettingsModel = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = NotificationAppHelper.a;
                local.d(a2, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsModel2 + ", messageSettingsTypeModel = " + notificationSettingsModel + ", isAllAppToggleEnabled = " + z);
                if (notificationSettingsModel2 != null) {
                    hh6.element = notificationSettingsModel2.getSettingsType();
                } else {
                    this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
                }
                if (notificationSettingsModel != null) {
                    hh62.element = notificationSettingsModel.getSettingsType();
                } else {
                    this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
                }
                rl6 a3 = ik6.a(il62, (af6) null, (ll6) null, new vx5$a$a(this, hh6, hh62, (xe6) null), 3, (Object) null);
                rl62 = ik6.a(il62, (af6) null, (ll6) null, new vx5$a$b(this, list, z, (xe6) null), 3, (Object) null);
                this.L$0 = il62;
                this.L$1 = hh6;
                this.L$2 = hh62;
                this.L$3 = list;
                this.Z$0 = z;
                this.L$4 = notificationSettingsModel2;
                this.L$5 = notificationSettingsModel;
                this.L$6 = a3;
                this.L$7 = rl62;
                this.L$8 = list;
                this.label = 1;
                obj3 = a3.a(this);
                if (obj3 == a) {
                    return a;
                }
                rl6 = a3;
                il6 = il62;
                list3 = list;
            } else if (i == 1) {
                list3 = (List) this.L$8;
                notificationSettingsModel2 = (NotificationSettingsModel) this.L$4;
                z = this.Z$0;
                hh62 = (hh6) this.L$2;
                hh6 = (hh6) this.L$1;
                nc6.a(obj);
                rl6 = (rl6) this.L$6;
                il6 = (il6) this.L$0;
                rl62 = (rl6) this.L$7;
                obj3 = obj;
                List list4 = (List) this.L$3;
                notificationSettingsModel = (NotificationSettingsModel) this.L$5;
                list = list4;
            } else if (i == 2) {
                list2 = (List) this.L$8;
                rl6 rl63 = (rl6) this.L$7;
                rl6 rl64 = (rl6) this.L$6;
                NotificationSettingsModel notificationSettingsModel3 = (NotificationSettingsModel) this.L$5;
                NotificationSettingsModel notificationSettingsModel4 = (NotificationSettingsModel) this.L$4;
                hh6 hh63 = (hh6) this.L$2;
                hh6 hh64 = (hh6) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                list = (List) this.L$3;
                obj2 = obj;
                list2.addAll((Collection) obj2);
                return list;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list3.addAll((Collection) obj3);
            this.L$0 = il6;
            this.L$1 = hh6;
            this.L$2 = hh62;
            this.L$3 = list;
            this.Z$0 = z;
            this.L$4 = notificationSettingsModel2;
            this.L$5 = notificationSettingsModel;
            this.L$6 = rl6;
            this.L$7 = rl62;
            this.L$8 = list;
            this.label = 2;
            obj2 = rl62.a(this);
            if (obj2 == a) {
                return a;
            }
            list2 = list;
            list2.addAll((Collection) obj2);
            return list;
        }
    }

    /*
    static {
        String simpleName = NotificationAppHelper.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationAppHelper::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final List<AppFilter> a(MFDeviceFamily mFDeviceFamily) {
        List<AppFilter> allAppFilters = mFDeviceFamily != null ? zm4.p.a().a().getAllAppFilters(mFDeviceFamily.ordinal()) : null;
        return allAppFilters == null ? new ArrayList() : allAppFilters;
    }

    @DexIgnore
    public final AppFilter a(String str, MFDeviceFamily mFDeviceFamily) {
        wg6.b(str, "type");
        if (TextUtils.isEmpty(str) || mFDeviceFamily == null) {
            return null;
        }
        return zm4.p.a().a().getAppFilterMatchingType(str, mFDeviceFamily.ordinal());
    }

    @DexIgnore
    public final boolean a(String str) {
        wg6.b(str, "type");
        return a(str, MFDeviceFamily.DEVICE_FAMILY_SAM) != null;
    }

    @DexIgnore
    public final Object a(d15 d15, mz4 mz4, NotificationSettingsDatabase notificationSettingsDatabase, an4 an4, xe6<? super List<AppNotificationFilter>> xe6) {
        return gk6.a(zl6.a(), new a(an4, notificationSettingsDatabase, mz4, d15, (xe6) null), xe6);
    }

    @DexIgnore
    public final AppNotificationFilterSettings a(SparseArray<List<BaseFeatureModel>> sparseArray, boolean z) {
        String str;
        ArrayList arrayList = new ArrayList();
        if (sparseArray != null) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            wg6.a((Object) clone, "it.clone()");
            int size = clone.size();
            short s = -1;
            for (int i = 0; i < size; i++) {
                List<ContactGroup> valueAt = clone.valueAt(i);
                if (valueAt != null) {
                    for (ContactGroup contactGroup : valueAt) {
                        if (contactGroup.isEnabled()) {
                            short c = (short) al4.c(contactGroup.getHour());
                            int i2 = 10000;
                            if (contactGroup instanceof ContactGroup) {
                                ContactGroup contactGroup2 = contactGroup;
                                if (contactGroup2.getContacts() != null) {
                                    for (Contact contact : contactGroup2.getContacts()) {
                                        wg6.a((Object) contact, "contact");
                                        if (contact.isUseCall()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(c, c, s, i2);
                                            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                                            appNotificationFilter.setSender(contact.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                                            appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (contact.isUseSms()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(c, c, s, i2);
                                            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                                            appNotificationFilter2.setSender(contact.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                                            appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                        }
                                        i2 = 10000;
                                    }
                                }
                            } else if (contactGroup instanceof AppFilter) {
                                AppFilter appFilter = (AppFilter) contactGroup;
                                String type = appFilter.getType();
                                if (!(type == null || xj6.a(type))) {
                                    if (appFilter.getName() == null) {
                                        str = "";
                                    } else {
                                        str = appFilter.getName();
                                    }
                                    if (z) {
                                        s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                    }
                                    NotificationHandMovingConfig notificationHandMovingConfig3 = new NotificationHandMovingConfig(c, c, s, 10000);
                                    wg6.a((Object) str, "appName");
                                    String type2 = appFilter.getType();
                                    wg6.a((Object) type2, "item.type");
                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(str, type2, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                    appNotificationFilter3.setHandMovingConfig(notificationHandMovingConfig3);
                                    appNotificationFilter3.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                                    arrayList.add(appNotificationFilter3);
                                }
                            }
                        }
                    }
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "buildAppNotificationFilterSettings size = " + arrayList.size());
        return new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
    }

    @DexIgnore
    public final boolean a(SKUModel sKUModel, String str) {
        SpecialSkuSetting.SpecialSku specialSku;
        wg6.b(str, "serial");
        if (sKUModel != null) {
            SpecialSkuSetting.SpecialSku.Companion companion = SpecialSkuSetting.SpecialSku.Companion;
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            specialSku = companion.fromType(sku);
        } else {
            specialSku = SpecialSkuSetting.SpecialSku.Companion.fromSerialNumber(str);
        }
        return specialSku == SpecialSkuSetting.SpecialSku.MOVEMBER;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(Context context, NotificationSettingsDatabase notificationSettingsDatabase, an4 an4) {
        int i;
        wg6.b(context, "context");
        wg6.b(notificationSettingsDatabase, "notificationSettingsDatabase");
        wg6.b(an4, "sharedPrefs");
        ArrayList arrayList = new ArrayList();
        boolean B = an4.B();
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
        int i2 = 0;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + B);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i = notificationSettingsWithIsCallNoLiveData.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            i = 0;
        }
        if (notificationSettingsWithIsCallNoLiveData2 != null) {
            i2 = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
        }
        List<AppNotificationFilter> a2 = a(i, i2);
        List<vx4> a3 = a(context);
        arrayList.addAll(a2);
        arrayList.addAll(ux5.a(a3, B));
        return arrayList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        ArrayList arrayList = new ArrayList();
        List allContactGroups = zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (i3 == 0) {
            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
            arrayList.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
        } else if (i3 == 1) {
            int size = allContactGroups.size();
            for (int i5 = 0; i5 < size; i5++) {
                ContactGroup contactGroup = (ContactGroup) allContactGroups.get(i5);
                DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                wg6.a((Object) contactGroup, "item");
                List contacts = contactGroup.getContacts();
                wg6.a((Object) contacts, "item.contacts");
                if (!contacts.isEmpty()) {
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                    Object obj = contactGroup.getContacts().get(0);
                    wg6.a(obj, "item.contacts[0]");
                    appNotificationFilter.setSender(((Contact) obj).getDisplayName());
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        if (i4 == 0) {
            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
            arrayList.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
        } else if (i4 == 1) {
            int size2 = allContactGroups.size();
            for (int i6 = 0; i6 < size2; i6++) {
                ContactGroup contactGroup2 = (ContactGroup) allContactGroups.get(i6);
                DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                wg6.a((Object) contactGroup2, "item");
                List contacts2 = contactGroup2.getContacts();
                wg6.a((Object) contacts2, "item.contacts");
                if (!contacts2.isEmpty()) {
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                    Object obj2 = contactGroup2.getContacts().get(0);
                    wg6.a(obj2, "item.contacts[0]");
                    appNotificationFilter2.setSender(((Contact) obj2).getDisplayName());
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<vx4> a(Context context) {
        List allAppFilters = zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<tj4.b> it = tj4.f.f().iterator();
        while (it.hasNext()) {
            tj4.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !xj6.b(next.b(), context.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter appFilter = (AppFilter) it2.next();
                    wg6.a((Object) appFilter, "appFilter");
                    if (wg6.a((Object) appFilter.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(appFilter.getDbRowId());
                        installedApp.setCurrentHandGroup(appFilter.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        return linkedList;
    }
}
