package com.fossil.blesdk.database;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.fossil.w81;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SdkDatabase_Impl extends SdkDatabase {
    @DexIgnore
    public volatile w81 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qh.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `DeviceFile` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `deviceMacAddress` TEXT NOT NULL, `fileType` INTEGER NOT NULL, `fileIndex` INTEGER NOT NULL, `rawData` BLOB NOT NULL, `fileLength` INTEGER NOT NULL, `fileCrc` INTEGER NOT NULL, `createdTimeStamp` INTEGER NOT NULL, `isCompleted` INTEGER NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'f0501e661379ccab31d0b3858cee8e83')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `DeviceFile`");
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) SdkDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) SdkDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = SdkDatabase_Impl.this.mDatabase = iiVar;
            SdkDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) SdkDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(9);
            hashMap.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap.put("deviceMacAddress", new fi.a("deviceMacAddress", "TEXT", true, 0, (String) null, 1));
            hashMap.put("fileType", new fi.a("fileType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("fileIndex", new fi.a("fileIndex", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("rawData", new fi.a("rawData", "BLOB", true, 0, (String) null, 1));
            hashMap.put("fileLength", new fi.a("fileLength", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("fileCrc", new fi.a("fileCrc", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("createdTimeStamp", new fi.a("createdTimeStamp", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("isCompleted", new fi.a("isCompleted", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("DeviceFile", hashMap, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar, "DeviceFile");
            if (fiVar.equals(a2)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "DeviceFile(com.fossil.blesdk.database.entity.DeviceFile).\n Expected:\n" + fiVar + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        ii a2 = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a2.b("DELETE FROM `DeviceFile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a2.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a2.A()) {
                a2.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), "DeviceFile");
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new a(4), "f0501e661379ccab31d0b3858cee8e83", "05778a001d8750bf160c2cf361f4e16f");
        ji.b.a a2 = ji.b.a(fhVar.b);
        a2.a(fhVar.c);
        a2.a((ji.a) qhVar);
        return fhVar.a.a(a2.a());
    }

    @DexIgnore
    public w81 a() {
        w81 w81;
        if (this.f != null) {
            return this.f;
        }
        synchronized (this) {
            if (this.f == null) {
                this.f = new w81(this);
            }
            w81 = this.f;
        }
        return w81;
    }
}
