package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah6 implements ng6 {
    @DexIgnore
    public /* final */ Class<?> a;

    @DexIgnore
    public ah6(Class<?> cls, String str) {
        wg6.b(cls, "jClass");
        wg6.b(str, "moduleName");
        this.a = cls;
    }

    @DexIgnore
    public Class<?> a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof ah6) && wg6.a((Object) a(), (Object) ((ah6) obj).a());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
