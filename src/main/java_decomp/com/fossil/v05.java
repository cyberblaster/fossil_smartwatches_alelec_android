package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v05 {
    @DexIgnore
    public /* final */ b05 a;
    @DexIgnore
    public /* final */ q05 b;

    @DexIgnore
    public v05(b05 b05, q05 q05) {
        wg6.b(b05, "mInActivityNudgeTimeContractView");
        wg6.b(q05, "mRemindTimeContractView");
        this.a = b05;
        this.b = q05;
    }

    @DexIgnore
    public final b05 a() {
        return this.a;
    }

    @DexIgnore
    public final q05 b() {
        return this.b;
    }
}
