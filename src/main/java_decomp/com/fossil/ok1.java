package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok1 extends x51 {
    @DexIgnore
    public /* final */ boolean T; // = true;
    @DexIgnore
    public HashMap<s60, r60> U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ok1(ue1 ue1, q41 q41, short s) {
        super(ue1, q41, r3, s, r5, 1.0f, (String) null, 64);
        eh1 eh1 = eh1.GET_DEVICE_CONFIGS;
        HashMap a = he6.a(new lc6[]{qc6.a(io0.SKIP_ERASE, true), qc6.a(io0.NUMBER_OF_FILE_REQUIRED, 1), qc6.a(io0.ERASE_CACHE_FILE_BEFORE_GET, true)});
    }

    @DexIgnore
    public void a(ArrayList<ie1> arrayList) {
        a(this.v);
    }

    @DexIgnore
    public boolean b() {
        return this.T;
    }

    @DexIgnore
    public void c(ie1 ie1) {
        sk1 sk1;
        super.c(ie1);
        try {
            this.U = (HashMap) lm0.f.a(ie1.e);
            sk1 = sk1.SUCCESS;
        } catch (sw0 e) {
            qs0.h.a(e);
            sk1 = sk1.UNSUPPORTED_FORMAT;
        }
        this.v = km1.a(this.v, (eh1) null, sk1, (bn0) null, 5);
    }

    @DexIgnore
    public Object d() {
        HashMap<s60, r60> hashMap = this.U;
        return hashMap != null ? hashMap : new HashMap();
    }

    @DexIgnore
    public JSONObject k() {
        JSONArray jSONArray;
        Collection<r60> values;
        JSONObject k = super.k();
        bm0 bm0 = bm0.CONFIGS;
        HashMap<s60, r60> hashMap = this.U;
        if (hashMap == null || (values = hashMap.values()) == null) {
            jSONArray = null;
        } else {
            Object[] array = values.toArray(new r60[0]);
            if (array != null) {
                jSONArray = cw0.a((p40[]) (r60[]) array);
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return cw0.a(k, bm0, (Object) jSONArray);
    }
}
