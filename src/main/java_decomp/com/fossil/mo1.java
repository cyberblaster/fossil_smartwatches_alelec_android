package com.fossil;

import com.facebook.stetho.inspector.protocol.module.Database;
import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo1 extends yw3<mo1, b> implements ro1 {
    @DexIgnore
    public static /* final */ mo1 A; // = new mo1();
    @DexIgnore
    public static volatile gx3<mo1> B;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public String i; // = "";
    @DexIgnore
    public String j; // = "";
    @DexIgnore
    public String o; // = "";
    @DexIgnore
    public String p; // = "";
    @DexIgnore
    public String q; // = "";
    @DexIgnore
    public String r; // = "";
    @DexIgnore
    public String s; // = "";
    @DexIgnore
    public String t; // = "";
    @DexIgnore
    public String u; // = "";
    @DexIgnore
    public String v; // = "";
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public String x; // = "";
    @DexIgnore
    public String y; // = "";
    @DexIgnore
    public zw3.c<String> z; // = yw3.i();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<mo1, b> implements ro1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b a(int i) {
            d();
            ((mo1) this.b).e = i;
            return this;
        }

        @DexIgnore
        public b b(String str) {
            d();
            mo1.e((mo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b c(String str) {
            d();
            mo1.g((mo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b d(String str) {
            d();
            mo1.d((mo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b e(String str) {
            d();
            mo1.c((mo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b f(String str) {
            d();
            mo1.b((mo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b g(String str) {
            d();
            mo1.f((mo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b() {
            super(mo1.A);
        }

        @DexIgnore
        public b a(String str) {
            d();
            mo1.a((mo1) this.b, str);
            return this;
        }
    }

    /*
    static {
        A.g();
    }
    */

    @DexIgnore
    public static /* synthetic */ void b(mo1 mo1, String str) {
        if (str != null) {
            mo1.j = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void c(mo1 mo1, String str) {
        if (str != null) {
            mo1.f = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void e(mo1 mo1, String str) {
        if (str != null) {
            mo1.w = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void f(mo1 mo1, String str) {
        if (str != null) {
            mo1.g = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void g(mo1 mo1, String str) {
        if (str != null) {
            mo1.h = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static mo1 k() {
        return A;
    }

    @DexIgnore
    public static b l() {
        return (b) A.c();
    }

    @DexIgnore
    public static gx3<mo1> m() {
        return A.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z2 = false;
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new mo1();
            case 2:
                return A;
            case 3:
                this.z.l();
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                mo1 mo1 = (mo1) obj2;
                boolean z3 = this.e != 0;
                int i2 = this.e;
                if (mo1.e != 0) {
                    z2 = true;
                }
                this.e = kVar.a(z3, i2, z2, mo1.e);
                this.f = kVar.a(!this.f.isEmpty(), this.f, !mo1.f.isEmpty(), mo1.f);
                this.g = kVar.a(!this.g.isEmpty(), this.g, !mo1.g.isEmpty(), mo1.g);
                this.h = kVar.a(!this.h.isEmpty(), this.h, !mo1.h.isEmpty(), mo1.h);
                this.i = kVar.a(!this.i.isEmpty(), this.i, !mo1.i.isEmpty(), mo1.i);
                this.j = kVar.a(!this.j.isEmpty(), this.j, !mo1.j.isEmpty(), mo1.j);
                this.o = kVar.a(!this.o.isEmpty(), this.o, !mo1.o.isEmpty(), mo1.o);
                this.p = kVar.a(!this.p.isEmpty(), this.p, !mo1.p.isEmpty(), mo1.p);
                this.q = kVar.a(!this.q.isEmpty(), this.q, !mo1.q.isEmpty(), mo1.q);
                this.r = kVar.a(!this.r.isEmpty(), this.r, !mo1.r.isEmpty(), mo1.r);
                this.s = kVar.a(!this.s.isEmpty(), this.s, !mo1.s.isEmpty(), mo1.s);
                this.t = kVar.a(!this.t.isEmpty(), this.t, !mo1.t.isEmpty(), mo1.t);
                this.u = kVar.a(!this.u.isEmpty(), this.u, !mo1.u.isEmpty(), mo1.u);
                this.v = kVar.a(!this.v.isEmpty(), this.v, !mo1.v.isEmpty(), mo1.v);
                this.w = kVar.a(!this.w.isEmpty(), this.w, !mo1.w.isEmpty(), mo1.w);
                this.x = kVar.a(!this.x.isEmpty(), this.x, !mo1.x.isEmpty(), mo1.x);
                this.y = kVar.a(!this.y.isEmpty(), this.y, !mo1.y.isEmpty(), mo1.y);
                this.z = kVar.a(this.z, mo1.z);
                if (kVar == yw3.i.a) {
                    this.d |= mo1.d;
                }
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z2) {
                    try {
                        int l = tw3.l();
                        switch (l) {
                            case 0:
                                z2 = true;
                                break;
                            case 24:
                                this.e = tw3.d();
                                break;
                            case 34:
                                this.f = tw3.k();
                                break;
                            case 42:
                                this.g = tw3.k();
                                break;
                            case 50:
                                this.j = tw3.k();
                                break;
                            case 58:
                                this.o = tw3.k();
                                break;
                            case 66:
                                this.h = tw3.k();
                                break;
                            case 74:
                                this.i = tw3.k();
                                break;
                            case 82:
                                this.p = tw3.k();
                                break;
                            case 90:
                                this.q = tw3.k();
                                break;
                            case 98:
                                this.r = tw3.k();
                                break;
                            case 106:
                                this.s = tw3.k();
                                break;
                            case 114:
                                this.t = tw3.k();
                                break;
                            case 122:
                                this.u = tw3.k();
                                break;
                            case 130:
                                this.v = tw3.k();
                                break;
                            case 138:
                                this.w = tw3.k();
                                break;
                            case 210:
                                this.x = tw3.k();
                                break;
                            case 218:
                                this.y = tw3.k();
                                break;
                            case Database.MAX_EXECUTE_RESULTS:
                                String k = tw3.k();
                                if (!this.z.m()) {
                                    this.z = yw3.a(this.z);
                                }
                                this.z.add(k);
                                break;
                            default:
                                if (tw3.f(l)) {
                                    break;
                                }
                                z2 = true;
                                break;
                        }
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (B == null) {
                    synchronized (mo1.class) {
                        if (B == null) {
                            B = new yw3.c(A);
                        }
                    }
                }
                return B;
            default:
                throw new UnsupportedOperationException();
        }
        return A;
    }

    @DexIgnore
    public int d() {
        int i2 = this.c;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.e;
        int d2 = i3 != 0 ? uw3.d(3, i3) + 0 : 0;
        if (!this.f.isEmpty()) {
            d2 += uw3.b(4, this.f);
        }
        if (!this.g.isEmpty()) {
            d2 += uw3.b(5, this.g);
        }
        if (!this.j.isEmpty()) {
            d2 += uw3.b(6, this.j);
        }
        if (!this.o.isEmpty()) {
            d2 += uw3.b(7, this.o);
        }
        if (!this.h.isEmpty()) {
            d2 += uw3.b(8, this.h);
        }
        if (!this.i.isEmpty()) {
            d2 += uw3.b(9, this.i);
        }
        if (!this.p.isEmpty()) {
            d2 += uw3.b(10, this.p);
        }
        if (!this.q.isEmpty()) {
            d2 += uw3.b(11, this.q);
        }
        if (!this.r.isEmpty()) {
            d2 += uw3.b(12, this.r);
        }
        if (!this.s.isEmpty()) {
            d2 += uw3.b(13, this.s);
        }
        if (!this.t.isEmpty()) {
            d2 += uw3.b(14, this.t);
        }
        if (!this.u.isEmpty()) {
            d2 += uw3.b(15, this.u);
        }
        if (!this.v.isEmpty()) {
            d2 += uw3.b(16, this.v);
        }
        if (!this.w.isEmpty()) {
            d2 += uw3.b(17, this.w);
        }
        if (!this.x.isEmpty()) {
            d2 += uw3.b(26, this.x);
        }
        if (!this.y.isEmpty()) {
            d2 += uw3.b(27, this.y);
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.z.size(); i5++) {
            i4 += uw3.a(this.z.get(i5));
        }
        int size = (this.z.size() * 2) + d2 + i4;
        this.c = size;
        return size;
    }

    @DexIgnore
    public static /* synthetic */ void d(mo1 mo1, String str) {
        if (str != null) {
            mo1.s = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        int i2 = this.e;
        if (i2 != 0) {
            uw3.b(3, i2);
        }
        if (!this.f.isEmpty()) {
            uw3.a(4, this.f);
        }
        if (!this.g.isEmpty()) {
            uw3.a(5, this.g);
        }
        if (!this.j.isEmpty()) {
            uw3.a(6, this.j);
        }
        if (!this.o.isEmpty()) {
            uw3.a(7, this.o);
        }
        if (!this.h.isEmpty()) {
            uw3.a(8, this.h);
        }
        if (!this.i.isEmpty()) {
            uw3.a(9, this.i);
        }
        if (!this.p.isEmpty()) {
            uw3.a(10, this.p);
        }
        if (!this.q.isEmpty()) {
            uw3.a(11, this.q);
        }
        if (!this.r.isEmpty()) {
            uw3.a(12, this.r);
        }
        if (!this.s.isEmpty()) {
            uw3.a(13, this.s);
        }
        if (!this.t.isEmpty()) {
            uw3.a(14, this.t);
        }
        if (!this.u.isEmpty()) {
            uw3.a(15, this.u);
        }
        if (!this.v.isEmpty()) {
            uw3.a(16, this.v);
        }
        if (!this.w.isEmpty()) {
            uw3.a(17, this.w);
        }
        if (!this.x.isEmpty()) {
            uw3.a(26, this.x);
        }
        if (!this.y.isEmpty()) {
            uw3.a(27, this.y);
        }
        for (int i3 = 0; i3 < this.z.size(); i3++) {
            uw3.a(31, this.z.get(i3));
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(mo1 mo1, String str) {
        if (str != null) {
            mo1.i = str;
            return;
        }
        throw new NullPointerException();
    }
}
