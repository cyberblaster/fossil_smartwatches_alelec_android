package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<la3> CREATOR; // = new ka3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ Long d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Double g;

    @DexIgnore
    public la3(na3 na3) {
        this(na3.c, na3.d, na3.e, na3.b);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b, false);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d, false);
        g22.a(parcel, 5, (Float) null, false);
        g22.a(parcel, 6, this.e, false);
        g22.a(parcel, 7, this.f, false);
        g22.a(parcel, 8, this.g, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public final Object zza() {
        Long l = this.d;
        if (l != null) {
            return l;
        }
        Double d2 = this.g;
        if (d2 != null) {
            return d2;
        }
        String str = this.e;
        if (str != null) {
            return str;
        }
        return null;
    }

    @DexIgnore
    public la3(String str, long j, Object obj, String str2) {
        w12.b(str);
        this.a = 2;
        this.b = str;
        this.c = j;
        this.f = str2;
        if (obj == null) {
            this.d = null;
            this.g = null;
            this.e = null;
        } else if (obj instanceof Long) {
            this.d = (Long) obj;
            this.g = null;
            this.e = null;
        } else if (obj instanceof String) {
            this.d = null;
            this.g = null;
            this.e = (String) obj;
        } else if (obj instanceof Double) {
            this.d = null;
            this.g = (Double) obj;
            this.e = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    @DexIgnore
    public la3(String str, long j, String str2) {
        w12.b(str);
        this.a = 2;
        this.b = str;
        this.c = 0;
        this.d = null;
        this.g = null;
        this.e = null;
        this.f = null;
    }

    @DexIgnore
    public la3(int i, String str, long j, Long l, Float f2, String str2, String str3, Double d2) {
        this.a = i;
        this.b = str;
        this.c = j;
        this.d = l;
        if (i == 1) {
            this.g = f2 != null ? Double.valueOf(f2.doubleValue()) : null;
        } else {
            this.g = d2;
        }
        this.e = str2;
        this.f = str3;
    }
}
