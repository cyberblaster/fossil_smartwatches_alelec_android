package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r76 extends AppCompatActivity implements o76, t76 {
    @DexIgnore
    public k76<Fragment> a;

    @DexIgnore
    public d76<Fragment> T() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.fossil.r76, android.app.Activity, androidx.appcompat.app.AppCompatActivity] */
    public void onCreate(Bundle bundle) {
        c76.a((Activity) this);
        r76.super.onCreate(bundle);
    }
}
