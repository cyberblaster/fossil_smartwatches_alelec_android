package com.fossil;

import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt6 implements lt6, kt6, Cloneable, ByteChannel {
    @DexIgnore
    public static /* final */ byte[] c; // = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    @DexIgnore
    public vt6 a;
    @DexIgnore
    public long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OutputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public void flush() {
        }

        @DexIgnore
        public String toString() {
            return jt6.this + ".outputStream()";
        }

        @DexIgnore
        public void write(int i) {
            jt6.this.writeByte((int) (byte) i);
        }

        @DexIgnore
        public void write(byte[] bArr, int i, int i2) {
            jt6.this.write(bArr, i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends InputStream {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int available() {
            return (int) Math.min(jt6.this.b, 2147483647L);
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public int read() {
            jt6 jt6 = jt6.this;
            if (jt6.b > 0) {
                return jt6.readByte() & 255;
            }
            return -1;
        }

        @DexIgnore
        public String toString() {
            return jt6.this + ".inputStream()";
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) {
            return jt6.this.a(bArr, i, i2);
        }
    }

    @DexIgnore
    public jt6 a() {
        return this;
    }

    @DexIgnore
    public String b(long j) throws EOFException {
        return a(j, bu6.a);
    }

    @DexIgnore
    public jt6 c() {
        return this;
    }

    @DexIgnore
    public void close() {
    }

    @DexIgnore
    public mt6 e(long j) throws EOFException {
        return new mt6(h(j));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof jt6)) {
            return false;
        }
        jt6 jt6 = (jt6) obj;
        long j = this.b;
        if (j != jt6.b) {
            return false;
        }
        long j2 = 0;
        if (j == 0) {
            return true;
        }
        vt6 vt6 = this.a;
        vt6 vt62 = jt6.a;
        int i = vt6.b;
        int i2 = vt62.b;
        while (j2 < this.b) {
            long min = (long) Math.min(vt6.c - i, vt62.c - i2);
            int i3 = i2;
            int i4 = i;
            int i5 = 0;
            while (((long) i5) < min) {
                int i6 = i4 + 1;
                int i7 = i3 + 1;
                if (vt6.a[i4] != vt62.a[i3]) {
                    return false;
                }
                i5++;
                i4 = i6;
                i3 = i7;
            }
            if (i4 == vt6.c) {
                vt6 = vt6.f;
                i = vt6.b;
            } else {
                i = i4;
            }
            if (i3 == vt62.c) {
                vt62 = vt62.f;
                i2 = vt62.b;
            } else {
                i2 = i3;
            }
            j2 += min;
        }
        return true;
    }

    @DexIgnore
    public boolean f() {
        return this.b == 0;
    }

    @DexIgnore
    public void flush() {
    }

    @DexIgnore
    public boolean g(long j) {
        return this.b >= j;
    }

    @DexIgnore
    public String h() throws EOFException {
        return f(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public int hashCode() {
        vt6 vt6 = this.a;
        if (vt6 == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = vt6.c;
            for (int i3 = vt6.b; i3 < i2; i3++) {
                i = (i * 31) + vt6.a[i3];
            }
            vt6 = vt6.f;
        } while (vt6 != this.a);
        return i;
    }

    @DexIgnore
    public void i(long j) throws EOFException {
        if (this.b < j) {
            throw new EOFException();
        }
    }

    @DexIgnore
    public boolean isOpen() {
        return true;
    }

    @DexIgnore
    public short j() {
        return bu6.a(readShort());
    }

    @DexIgnore
    public final void k() {
        try {
            skip(this.b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final long l() {
        long j = this.b;
        if (j == 0) {
            return 0;
        }
        vt6 vt6 = this.a.g;
        int i = vt6.c;
        return (i >= 8192 || !vt6.e) ? j : j - ((long) (i - vt6.b));
    }

    @DexIgnore
    public mt6 m() {
        return new mt6(e());
    }

    @DexIgnore
    public String n() {
        try {
            return a(this.b, bu6.a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public int o() throws EOFException {
        byte b2;
        int i;
        byte b3;
        if (this.b != 0) {
            byte a2 = a(0);
            int i2 = 1;
            if ((a2 & 128) == 0) {
                b3 = a2 & Byte.MAX_VALUE;
                i = 1;
                b2 = 0;
            } else if ((a2 & 224) == 192) {
                b3 = a2 & 31;
                i = 2;
                b2 = 128;
            } else if ((a2 & 240) == 224) {
                b3 = a2 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY;
                i = 3;
                b2 = 2048;
            } else if ((a2 & 248) == 240) {
                b3 = a2 & 7;
                i = 4;
                b2 = 65536;
            } else {
                skip(1);
                return 65533;
            }
            long j = (long) i;
            if (this.b >= j) {
                while (i2 < i) {
                    long j2 = (long) i2;
                    byte a3 = a(j2);
                    if ((a3 & 192) == 128) {
                        b3 = (b3 << 6) | (a3 & 63);
                        i2++;
                    } else {
                        skip(j2);
                        return 65533;
                    }
                }
                skip(j);
                if (b3 > 1114111) {
                    return 65533;
                }
                if ((b3 < 55296 || b3 > 57343) && b3 >= b2) {
                    return b3;
                }
                return 65533;
            }
            throw new EOFException("size < " + i + ": " + this.b + " (to read code point prefixed 0x" + Integer.toHexString(a2) + ")");
        }
        throw new EOFException();
    }

    @DexIgnore
    public final long p() {
        return this.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        if (r8 != r9) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
        r15.a = r6.b();
        com.fossil.wt6.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009b, code lost:
        r6.b = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009d, code lost:
        if (r0 != false) goto L_0x00a3;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0074 A[SYNTHETIC] */
    public long q() {
        int i;
        int i2;
        if (this.b != 0) {
            boolean z = false;
            long j = 0;
            int i3 = 0;
            do {
                vt6 vt6 = this.a;
                byte[] bArr = vt6.a;
                int i4 = vt6.b;
                int i5 = vt6.c;
                while (true) {
                    if (i4 >= i5) {
                        break;
                    }
                    byte b2 = bArr[i4];
                    if (b2 < 48 || b2 > 57) {
                        if (b2 >= 97 && b2 <= 102) {
                            i2 = b2 - 97;
                        } else if (b2 >= 65 && b2 <= 70) {
                            i2 = b2 - 65;
                        } else if (i3 == 0) {
                            z = true;
                        } else {
                            throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + Integer.toHexString(b2));
                        }
                        i = i2 + 10;
                    } else {
                        i = b2 - 48;
                    }
                    if ((-1152921504606846976L & j) == 0) {
                        j = (j << 4) | ((long) i);
                        i4++;
                        i3++;
                    } else {
                        jt6 jt6 = new jt6();
                        jt6.c(j);
                        jt6.writeByte((int) b2);
                        throw new NumberFormatException("Number too large: " + jt6.n());
                    }
                }
                if (i3 == 0) {
                }
            } while (this.a != null);
            this.b -= (long) i3;
            return j;
        }
        throw new IllegalStateException("size == 0");
    }

    @DexIgnore
    public InputStream r() {
        return new b();
    }

    @DexIgnore
    public int read(ByteBuffer byteBuffer) throws IOException {
        vt6 vt6 = this.a;
        if (vt6 == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), vt6.c - vt6.b);
        byteBuffer.put(vt6.a, vt6.b, min);
        vt6.b += min;
        this.b -= (long) min;
        if (vt6.b == vt6.c) {
            this.a = vt6.b();
            wt6.a(vt6);
        }
        return min;
    }

    @DexIgnore
    public byte readByte() {
        long j = this.b;
        if (j != 0) {
            vt6 vt6 = this.a;
            int i = vt6.b;
            int i2 = vt6.c;
            int i3 = i + 1;
            byte b2 = vt6.a[i];
            this.b = j - 1;
            if (i3 == i2) {
                this.a = vt6.b();
                wt6.a(vt6);
            } else {
                vt6.b = i3;
            }
            return b2;
        }
        throw new IllegalStateException("size == 0");
    }

    @DexIgnore
    public void readFully(byte[] bArr) throws EOFException {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 != -1) {
                i += a2;
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    public int readInt() {
        long j = this.b;
        if (j >= 4) {
            vt6 vt6 = this.a;
            int i = vt6.b;
            int i2 = vt6.c;
            if (i2 - i < 4) {
                return ((readByte() & 255) << 24) | ((readByte() & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((readByte() & 255) << 8) | (readByte() & 255);
            }
            byte[] bArr = vt6.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY);
            int i5 = i4 + 1;
            byte b3 = b2 | ((bArr[i4] & 255) << 8);
            int i6 = i5 + 1;
            byte b4 = b3 | (bArr[i5] & 255);
            this.b = j - 4;
            if (i6 == i2) {
                this.a = vt6.b();
                wt6.a(vt6);
            } else {
                vt6.b = i6;
            }
            return b4;
        }
        throw new IllegalStateException("size < 4: " + this.b);
    }

    @DexIgnore
    public short readShort() {
        long j = this.b;
        if (j >= 2) {
            vt6 vt6 = this.a;
            int i = vt6.b;
            int i2 = vt6.c;
            if (i2 - i < 2) {
                return (short) (((readByte() & 255) << 8) | (readByte() & 255));
            }
            byte[] bArr = vt6.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
            this.b = j - 2;
            if (i4 == i2) {
                this.a = vt6.b();
                wt6.a(vt6);
            } else {
                vt6.b = i4;
            }
            return (short) b2;
        }
        throw new IllegalStateException("size < 2: " + this.b);
    }

    @DexIgnore
    public void skip(long j) throws EOFException {
        while (j > 0) {
            vt6 vt6 = this.a;
            if (vt6 != null) {
                int min = (int) Math.min(j, (long) (vt6.c - vt6.b));
                long j2 = (long) min;
                this.b -= j2;
                j -= j2;
                vt6 vt62 = this.a;
                vt62.b += min;
                if (vt62.b == vt62.c) {
                    this.a = vt62.b();
                    wt6.a(vt62);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    public String toString() {
        return w().toString();
    }

    @DexIgnore
    public final mt6 w() {
        long j = this.b;
        if (j <= 2147483647L) {
            return a((int) j);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.b);
    }

    @DexIgnore
    public vt6 b(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        }
        vt6 vt6 = this.a;
        if (vt6 == null) {
            this.a = wt6.a();
            vt6 vt62 = this.a;
            vt62.g = vt62;
            vt62.f = vt62;
            return vt62;
        }
        vt6 vt63 = vt6.g;
        if (vt63.c + i <= 8192 && vt63.e) {
            return vt63;
        }
        vt6 a2 = wt6.a();
        vt63.a(a2);
        return a2;
    }

    @DexIgnore
    public jt6 clone() {
        jt6 jt6 = new jt6();
        if (this.b == 0) {
            return jt6;
        }
        jt6.a = this.a.c();
        vt6 vt6 = jt6.a;
        vt6.g = vt6;
        vt6.f = vt6;
        vt6 vt62 = this.a;
        while (true) {
            vt62 = vt62.f;
            if (vt62 != this.a) {
                jt6.a.g.a(vt62.c());
            } else {
                jt6.b = this.b;
                return jt6;
            }
        }
    }

    @DexIgnore
    public OutputStream d() {
        return new a();
    }

    @DexIgnore
    public byte[] e() {
        try {
            return h(this.b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public String f(long j) throws EOFException {
        if (j >= 0) {
            long j2 = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
            if (j != ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                j2 = j + 1;
            }
            long a2 = a((byte) 10, 0, j2);
            if (a2 != -1) {
                return j(a2);
            }
            if (j2 < p() && a(j2 - 1) == 13 && a(j2) == 10) {
                return j(j2);
            }
            jt6 jt6 = new jt6();
            a(jt6, 0, Math.min(32, p()));
            throw new EOFException("\\n not found: limit=" + Math.min(p(), j) + " content=" + jt6.m().hex() + 8230);
        }
        throw new IllegalArgumentException("limit < 0: " + j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        if (r5 != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        r1.readByte();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0067, code lost:
        throw new java.lang.NumberFormatException("Number too large: " + r1.n());
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0096 A[EDGE_INSN: B:47:0x0096->B:29:0x0096 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x001a  */
    public long g() {
        long j = 0;
        if (this.b != 0) {
            int i = 0;
            long j2 = -7;
            boolean z = false;
            boolean z2 = false;
            loop0:
            do {
                vt6 vt6 = this.a;
                byte[] bArr = vt6.a;
                int i2 = vt6.b;
                int i3 = vt6.c;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    byte b2 = bArr[i2];
                    if (b2 >= 48 && b2 <= 57) {
                        int i4 = 48 - b2;
                        int i5 = (j > -922337203685477580L ? 1 : (j == -922337203685477580L ? 0 : -1));
                        if (i5 < 0 || (i5 == 0 && ((long) i4) < j2)) {
                            jt6 jt6 = new jt6();
                            jt6.d(j);
                            jt6.writeByte((int) b2);
                        } else {
                            j = (j * 10) + ((long) i4);
                        }
                    } else if (b2 == 45 && i == 0) {
                        j2--;
                        z = true;
                    } else if (i != 0) {
                        z2 = true;
                    } else {
                        throw new NumberFormatException("Expected leading [0-9] or '-' character but was 0x" + Integer.toHexString(b2));
                    }
                    i2++;
                    i++;
                }
                if (i2 != i3) {
                    this.a = vt6.b();
                    wt6.a(vt6);
                } else {
                    vt6.b = i2;
                }
                if (z2 || this.a == null) {
                    this.b -= (long) i;
                }
                vt6 vt62 = this.a;
                byte[] bArr2 = vt62.a;
                int i22 = vt62.b;
                int i32 = vt62.c;
                while (true) {
                    if (i22 >= i32) {
                    }
                    i22++;
                    i++;
                }
                if (i22 != i32) {
                }
                break;
            } while (this.a == null);
            this.b -= (long) i;
            return z ? j : -j;
        }
        throw new IllegalStateException("size == 0");
    }

    @DexIgnore
    public byte[] h(long j) throws EOFException {
        bu6.a(this.b, 0, j);
        if (j <= 2147483647L) {
            byte[] bArr = new byte[((int) j)];
            readFully(bArr);
            return bArr;
        }
        throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
    }

    @DexIgnore
    public int i() {
        return bu6.a(readInt());
    }

    @DexIgnore
    public String j(long j) throws EOFException {
        if (j > 0) {
            long j2 = j - 1;
            if (a(j2) == 13) {
                String b2 = b(j2);
                skip(2);
                return b2;
            }
        }
        String b3 = b(j);
        skip(1);
        return b3;
    }

    @DexIgnore
    public jt6 writeByte(int i) {
        vt6 b2 = b(1);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        b2.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.b++;
        return this;
    }

    @DexIgnore
    public jt6 writeInt(int i) {
        vt6 b2 = b(4);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        b2.c = i5 + 1;
        this.b += 4;
        return this;
    }

    @DexIgnore
    public jt6 writeShort(int i) {
        vt6 b2 = b(2);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        b2.c = i3 + 1;
        this.b += 2;
        return this;
    }

    @DexIgnore
    public final jt6 a(jt6 jt6, long j, long j2) {
        if (jt6 != null) {
            bu6.a(this.b, j, j2);
            if (j2 == 0) {
                return this;
            }
            jt6.b += j2;
            vt6 vt6 = this.a;
            while (true) {
                int i = vt6.c;
                int i2 = vt6.b;
                if (j < ((long) (i - i2))) {
                    break;
                }
                j -= (long) (i - i2);
                vt6 = vt6.f;
            }
            while (j2 > 0) {
                vt6 c2 = vt6.c();
                c2.b = (int) (((long) c2.b) + j);
                c2.c = Math.min(c2.b + ((int) j2), c2.c);
                vt6 vt62 = jt6.a;
                if (vt62 == null) {
                    c2.g = c2;
                    c2.f = c2;
                    jt6.a = c2;
                } else {
                    vt62.g.a(c2);
                }
                j2 -= (long) (c2.c - c2.b);
                vt6 = vt6.f;
                j = 0;
            }
            return this;
        }
        throw new IllegalArgumentException("out == null");
    }

    @DexIgnore
    public jt6 c(int i) {
        if (i < 128) {
            writeByte(i);
        } else if (i < 2048) {
            writeByte((i >> 6) | 192);
            writeByte((i & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                writeByte((i >> 12) | 224);
                writeByte(((i >> 6) & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                writeByte((i & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            } else {
                writeByte(63);
            }
        } else if (i <= 1114111) {
            writeByte((i >> 18) | 240);
            writeByte(((i >> 12) & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            writeByte(((i >> 6) & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            writeByte((i & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    @DexIgnore
    public jt6 d(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            writeByte(48);
            return this;
        }
        boolean z = false;
        int i2 = 1;
        if (i < 0) {
            j = -j;
            if (j < 0) {
                a("-9223372036854775808");
                return this;
            }
            z = true;
        }
        if (j >= 100000000) {
            i2 = j < 1000000000000L ? j < 10000000000L ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j >= ButtonService.CONNECT_TIMEOUT) {
            i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
        } else if (j >= 100) {
            i2 = j < 1000 ? 3 : 4;
        } else if (j >= 10) {
            i2 = 2;
        }
        if (z) {
            i2++;
        }
        vt6 b2 = b(i2);
        byte[] bArr = b2.a;
        int i3 = b2.c + i2;
        while (j != 0) {
            i3--;
            bArr[i3] = c[(int) (j % 10)];
            j /= 10;
        }
        if (z) {
            bArr[i3 - 1] = 45;
        }
        b2.c += i2;
        this.b += (long) i2;
        return this;
    }

    @DexIgnore
    public jt6 write(byte[] bArr) {
        if (bArr != null) {
            write(bArr, 0, bArr.length);
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public jt6 write(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            long j = (long) i2;
            bu6.a((long) bArr.length, (long) i, j);
            int i3 = i2 + i;
            while (i < i3) {
                vt6 b2 = b(1);
                int min = Math.min(i3 - i, 8192 - b2.c);
                System.arraycopy(bArr, i, b2.a, b2.c, min);
                i += min;
                b2.c += min;
            }
            this.b += j;
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public long b(jt6 jt6, long j) {
        if (jt6 == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j >= 0) {
            long j2 = this.b;
            if (j2 == 0) {
                return -1;
            }
            if (j > j2) {
                j = j2;
            }
            jt6.a(this, j);
            return j;
        } else {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        }
    }

    @DexIgnore
    public int write(ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer != null) {
            int remaining = byteBuffer.remaining();
            int i = remaining;
            while (i > 0) {
                vt6 b2 = b(1);
                int min = Math.min(i, 8192 - b2.c);
                byteBuffer.get(b2.a, b2.c, min);
                i -= min;
                b2.c += min;
            }
            this.b += (long) remaining;
            return remaining;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public au6 b() {
        return au6.d;
    }

    @DexIgnore
    public final byte a(long j) {
        int i;
        bu6.a(this.b, j, 1);
        long j2 = this.b;
        if (j2 - j > j) {
            vt6 vt6 = this.a;
            while (true) {
                int i2 = vt6.c;
                int i3 = vt6.b;
                long j3 = (long) (i2 - i3);
                if (j < j3) {
                    return vt6.a[i3 + ((int) j)];
                }
                j -= j3;
                vt6 = vt6.f;
            }
        } else {
            long j4 = j - j2;
            vt6 vt62 = this.a;
            do {
                vt62 = vt62.g;
                int i4 = vt62.c;
                i = vt62.b;
                j4 += (long) (i4 - i);
            } while (j4 < 0);
            return vt62.a[i + ((int) j4)];
        }
    }

    @DexIgnore
    public jt6 c(long j) {
        if (j == 0) {
            writeByte(48);
            return this;
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        vt6 b2 = b(numberOfTrailingZeros);
        byte[] bArr = b2.a;
        int i = b2.c;
        for (int i2 = (i + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = c[(int) (15 & j)];
            j >>>= 4;
        }
        b2.c += numberOfTrailingZeros;
        this.b += (long) numberOfTrailingZeros;
        return this;
    }

    @DexIgnore
    public long a(yt6 yt6) throws IOException {
        long j = this.b;
        if (j > 0) {
            yt6.a(this, j);
        }
        return j;
    }

    @DexIgnore
    public String a(Charset charset) {
        try {
            return a(this.b, charset);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public String a(long j, Charset charset) throws EOFException {
        bu6.a(this.b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            vt6 vt6 = this.a;
            int i = vt6.b;
            if (((long) i) + j > ((long) vt6.c)) {
                return new String(h(j), charset);
            }
            String str = new String(vt6.a, i, (int) j, charset);
            vt6.b = (int) (((long) vt6.b) + j);
            this.b -= j;
            if (vt6.b == vt6.c) {
                this.a = vt6.b();
                wt6.a(vt6);
            }
            return str;
        }
    }

    @DexIgnore
    public int a(byte[] bArr, int i, int i2) {
        bu6.a((long) bArr.length, (long) i, (long) i2);
        vt6 vt6 = this.a;
        if (vt6 == null) {
            return -1;
        }
        int min = Math.min(i2, vt6.c - vt6.b);
        System.arraycopy(vt6.a, vt6.b, bArr, i, min);
        vt6.b += min;
        this.b -= (long) min;
        if (vt6.b == vt6.c) {
            this.a = vt6.b();
            wt6.a(vt6);
        }
        return min;
    }

    @DexIgnore
    public jt6 a(mt6 mt6) {
        if (mt6 != null) {
            mt6.write(this);
            return this;
        }
        throw new IllegalArgumentException("byteString == null");
    }

    @DexIgnore
    public jt6 a(String str) {
        a(str, 0, str.length());
        return this;
    }

    @DexIgnore
    public jt6 a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 <= str.length()) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    vt6 b2 = b(1);
                    byte[] bArr = b2.a;
                    int i3 = b2.c - i;
                    int min = Math.min(i2, 8192 - i3);
                    int i4 = i + 1;
                    bArr[i + i3] = (byte) charAt;
                    while (i4 < min) {
                        char charAt2 = str.charAt(i4);
                        if (charAt2 >= 128) {
                            break;
                        }
                        bArr[i4 + i3] = (byte) charAt2;
                        i4++;
                    }
                    int i5 = b2.c;
                    int i6 = (i3 + i4) - i5;
                    b2.c = i5 + i6;
                    this.b += (long) i6;
                    i = i4;
                } else {
                    if (charAt < 2048) {
                        writeByte((charAt >> 6) | 192);
                        writeByte((int) (charAt & '?') | 128);
                    } else if (charAt < 55296 || charAt > 57343) {
                        writeByte((charAt >> 12) | 224);
                        writeByte(((charAt >> 6) & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                        writeByte((int) (charAt & '?') | 128);
                    } else {
                        int i7 = i + 1;
                        char charAt3 = i7 < i2 ? str.charAt(i7) : 0;
                        if (charAt > 56319 || charAt3 < 56320 || charAt3 > 57343) {
                            writeByte(63);
                            i = i7;
                        } else {
                            int i8 = (((charAt & 10239) << 10) | (9215 & charAt3)) + 0;
                            writeByte((i8 >> 18) | 240);
                            writeByte(((i8 >> 12) & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                            writeByte(((i8 >> 6) & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                            writeByte((i8 & 63) | Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                            i += 2;
                        }
                    }
                    i++;
                }
            }
            return this;
        } else {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        }
    }

    @DexIgnore
    public jt6 a(String str, Charset charset) {
        a(str, 0, str.length(), charset);
        return this;
    }

    @DexIgnore
    public jt6 a(String str, int i, int i2, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalAccessError("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (charset.equals(bu6.a)) {
            a(str, i, i2);
            return this;
        } else {
            byte[] bytes = str.substring(i, i2).getBytes(charset);
            write(bytes, 0, bytes.length);
            return this;
        }
    }

    @DexIgnore
    public long a(zt6 zt6) throws IOException {
        if (zt6 != null) {
            long j = 0;
            while (true) {
                long b2 = zt6.b(this, 8192);
                if (b2 == -1) {
                    return j;
                }
                j += b2;
            }
        } else {
            throw new IllegalArgumentException("source == null");
        }
    }

    @DexIgnore
    public void a(jt6 jt6, long j) {
        int i;
        if (jt6 == null) {
            throw new IllegalArgumentException("source == null");
        } else if (jt6 != this) {
            bu6.a(jt6.b, 0, j);
            while (j > 0) {
                vt6 vt6 = jt6.a;
                if (j < ((long) (vt6.c - vt6.b))) {
                    vt6 vt62 = this.a;
                    vt6 vt63 = vt62 != null ? vt62.g : null;
                    if (vt63 != null && vt63.e) {
                        long j2 = ((long) vt63.c) + j;
                        if (vt63.d) {
                            i = 0;
                        } else {
                            i = vt63.b;
                        }
                        if (j2 - ((long) i) <= 8192) {
                            jt6.a.a(vt63, (int) j);
                            jt6.b -= j;
                            this.b += j;
                            return;
                        }
                    }
                    jt6.a = jt6.a.a((int) j);
                }
                vt6 vt64 = jt6.a;
                long j3 = (long) (vt64.c - vt64.b);
                jt6.a = vt64.b();
                vt6 vt65 = this.a;
                if (vt65 == null) {
                    this.a = vt64;
                    vt6 vt66 = this.a;
                    vt66.g = vt66;
                    vt66.f = vt66;
                } else {
                    vt65.g.a(vt64);
                    vt64.a();
                }
                jt6.b -= j3;
                this.b += j3;
                j -= j3;
            }
        } else {
            throw new IllegalArgumentException("source == this");
        }
    }

    @DexIgnore
    public long a(byte b2) {
        return a(b2, 0, (long) ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public long a(byte b2, long j, long j2) {
        vt6 vt6;
        long j3 = 0;
        if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(this.b), Long.valueOf(j), Long.valueOf(j2)}));
        }
        long j4 = this.b;
        if (j2 <= j4) {
            j4 = j2;
        }
        if (j == j4 || (vt6 = this.a) == null) {
            return -1;
        }
        long j5 = this.b;
        if (j5 - j >= j) {
            while (true) {
                j5 = j3;
                j3 = ((long) (vt6.c - vt6.b)) + j5;
                if (j3 >= j) {
                    break;
                }
                vt6 = vt6.f;
            }
        } else {
            while (j5 > j) {
                vt6 = vt6.g;
                j5 -= (long) (vt6.c - vt6.b);
            }
        }
        long j6 = j;
        while (j5 < j4) {
            byte[] bArr = vt6.a;
            int min = (int) Math.min((long) vt6.c, (((long) vt6.b) + j4) - j5);
            for (int i = (int) ((((long) vt6.b) + j6) - j5); i < min; i++) {
                if (bArr[i] == b2) {
                    return ((long) (i - vt6.b)) + j5;
                }
            }
            byte b3 = b2;
            j6 = ((long) (vt6.c - vt6.b)) + j5;
            vt6 = vt6.f;
            j5 = j6;
        }
        return -1;
    }

    @DexIgnore
    public boolean a(long j, mt6 mt6) {
        return a(j, mt6, 0, mt6.size());
    }

    @DexIgnore
    public boolean a(long j, mt6 mt6, int i, int i2) {
        if (j < 0 || i < 0 || i2 < 0 || this.b - j < ((long) i2) || mt6.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (a(((long) i3) + j) != mt6.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final mt6 a(int i) {
        if (i == 0) {
            return mt6.EMPTY;
        }
        return new xt6(this, i);
    }
}
