package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hc0 extends p40 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ gc0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                Parcelable readParcelable = parcel.readParcelable(gc0.class.getClassLoader());
                if (readParcelable != null) {
                    wg6.a(readParcelable, "parcel.readParcelable<Fi\u2026class.java.classLoader)!!");
                    return new hc0(readString, readInt, (gc0) readParcelable);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new hc0[i];
        }
    }

    @DexIgnore
    public hc0(String str, int i, gc0 gc0) {
        this.a = str;
        this.b = i;
        this.c = gc0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.NAME, (Object) this.a), bm0.RANK, (Object) Integer.valueOf(this.b)), this.c.a());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hc0)) {
            return false;
        }
        hc0 hc0 = (hc0) obj;
        return wg6.a(this.a, hc0.a) && this.b == hc0.b && wg6.a(this.c, hc0.c);
    }

    @DexIgnore
    public final gc0 getFitnessData() {
        return this.c;
    }

    @DexIgnore
    public final String getName() {
        return this.a;
    }

    @DexIgnore
    public final int getRank() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.b) * 31;
        gc0 gc0 = this.c;
        if (gc0 != null) {
            i = gc0.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("Player(name=");
        b2.append(this.a);
        b2.append(", rank=");
        b2.append(this.b);
        b2.append(", fitnessData=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeInt(this.b);
        parcel.writeParcelable(this.c, i);
    }
}
