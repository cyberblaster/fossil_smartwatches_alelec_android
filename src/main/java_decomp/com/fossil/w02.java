package com.fossil;

import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w02<T> extends s02<T> {
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public ArrayList<Integer> c;

    @DexIgnore
    public w02(DataHolder dataHolder) {
        super(dataHolder);
    }

    @DexIgnore
    public final int a(int i) {
        if (i >= 0 && i < this.c.size()) {
            return this.c.get(i).intValue();
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(i);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public abstract T a(int i, int i2);

    @DexIgnore
    public String b() {
        return null;
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final void d() {
        synchronized (this) {
            if (!this.b) {
                int count = this.a.getCount();
                this.c = new ArrayList<>();
                if (count > 0) {
                    this.c.add(0);
                    String c2 = c();
                    String c3 = this.a.c(c2, 0, this.a.c(0));
                    int i = 1;
                    while (i < count) {
                        int c4 = this.a.c(i);
                        String c5 = this.a.c(c2, i, c4);
                        if (c5 != null) {
                            if (!c5.equals(c3)) {
                                this.c.add(Integer.valueOf(i));
                                c3 = c5;
                            }
                            i++;
                        } else {
                            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 78);
                            sb.append("Missing value for markerColumn: ");
                            sb.append(c2);
                            sb.append(", at row: ");
                            sb.append(i);
                            sb.append(", for window: ");
                            sb.append(c4);
                            throw new NullPointerException(sb.toString());
                        }
                    }
                }
                this.b = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0063, code lost:
        if (r6.a.c(r4, r7, r3) == null) goto L_0x0067;
     */
    @DexIgnore
    public final T get(int i) {
        int i2;
        int i3;
        d();
        int a = a(i);
        int i4 = 0;
        if (i >= 0 && i != this.c.size()) {
            if (i == this.c.size() - 1) {
                i3 = this.a.getCount();
                i2 = this.c.get(i).intValue();
            } else {
                i3 = this.c.get(i + 1).intValue();
                i2 = this.c.get(i).intValue();
            }
            int i5 = i3 - i2;
            if (i5 == 1) {
                int a2 = a(i);
                int c2 = this.a.c(a2);
                String b2 = b();
                if (b2 != null) {
                }
            }
            i4 = i5;
        }
        return a(a, i4);
    }

    @DexIgnore
    public int getCount() {
        d();
        return this.c.size();
    }
}
