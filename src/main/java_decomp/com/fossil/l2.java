package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l2 {
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public g3 b;
    @DexIgnore
    public g3 c;
    @DexIgnore
    public g3 d;

    @DexIgnore
    public l2(ImageView imageView) {
        this.a = imageView;
    }

    @DexIgnore
    public void a(AttributeSet attributeSet, int i) {
        int g;
        i3 a2 = i3.a(this.a.getContext(), attributeSet, j0.AppCompatImageView, i, 0);
        try {
            Drawable drawable = this.a.getDrawable();
            if (!(drawable != null || (g = a2.g(j0.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = u0.c(this.a.getContext(), g)) == null)) {
                this.a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                s2.b(drawable);
            }
            if (a2.g(j0.AppCompatImageView_tint)) {
                qa.a(this.a, a2.a(j0.AppCompatImageView_tint));
            }
            if (a2.g(j0.AppCompatImageView_tintMode)) {
                qa.a(this.a, s2.a(a2.d(j0.AppCompatImageView_tintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            a2.a();
        }
    }

    @DexIgnore
    public ColorStateList b() {
        g3 g3Var = this.c;
        if (g3Var != null) {
            return g3Var.a;
        }
        return null;
    }

    @DexIgnore
    public PorterDuff.Mode c() {
        g3 g3Var = this.c;
        if (g3Var != null) {
            return g3Var.b;
        }
        return null;
    }

    @DexIgnore
    public boolean d() {
        return Build.VERSION.SDK_INT < 21 || !(this.a.getBackground() instanceof RippleDrawable);
    }

    @DexIgnore
    public final boolean e() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.b != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(int i) {
        if (i != 0) {
            Drawable c2 = u0.c(this.a.getContext(), i);
            if (c2 != null) {
                s2.b(c2);
            }
            this.a.setImageDrawable(c2);
        } else {
            this.a.setImageDrawable((Drawable) null);
        }
        a();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new g3();
        }
        g3 g3Var = this.c;
        g3Var.a = colorStateList;
        g3Var.d = true;
        a();
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new g3();
        }
        g3 g3Var = this.c;
        g3Var.b = mode;
        g3Var.c = true;
        a();
    }

    @DexIgnore
    public void a() {
        Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            s2.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!e() || !a(drawable)) {
            g3 g3Var = this.c;
            if (g3Var != null) {
                j2.a(drawable, g3Var, this.a.getDrawableState());
                return;
            }
            g3 g3Var2 = this.b;
            if (g3Var2 != null) {
                j2.a(drawable, g3Var2, this.a.getDrawableState());
            }
        }
    }

    @DexIgnore
    public final boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new g3();
        }
        g3 g3Var = this.d;
        g3Var.a();
        ColorStateList a2 = qa.a(this.a);
        if (a2 != null) {
            g3Var.d = true;
            g3Var.a = a2;
        }
        PorterDuff.Mode b2 = qa.b(this.a);
        if (b2 != null) {
            g3Var.c = true;
            g3Var.b = b2;
        }
        if (!g3Var.d && !g3Var.c) {
            return false;
        }
        j2.a(drawable, g3Var, this.a.getDrawableState());
        return true;
    }
}
