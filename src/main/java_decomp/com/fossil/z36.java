package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z36 extends v36 {
    @DexIgnore
    public Long m; // = null;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;

    @DexIgnore
    public z36(Context context, String str, String str2, int i, Long l, r36 r36) {
        super(context, i, r36);
        this.o = str;
        this.n = str2;
        this.m = l;
    }

    @DexIgnore
    public w36 a() {
        return w36.PAGE_VIEW;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        r56.a(jSONObject, "pi", this.n);
        r56.a(jSONObject, "rf", this.o);
        Long l = this.m;
        if (l == null) {
            return true;
        }
        jSONObject.put("du", l);
        return true;
    }
}
