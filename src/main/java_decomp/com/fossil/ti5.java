package com.fossil;

import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ti5 implements Factory<si5> {
    @DexIgnore
    public static HeartRateDetailPresenter a(pi5 pi5, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, u04 u04) {
        return new HeartRateDetailPresenter(pi5, heartRateSummaryRepository, heartRateSampleRepository, userRepository, workoutSessionRepository, fitnessDataDao, workoutDao, fitnessDatabase, u04);
    }
}
