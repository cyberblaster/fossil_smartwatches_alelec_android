package com.fossil;

import com.fossil.fn2;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sm2 {
    @DexIgnore
    public static volatile sm2 b;
    @DexIgnore
    public static volatile sm2 c;
    @DexIgnore
    public static /* final */ sm2 d; // = new sm2(true);
    @DexIgnore
    public /* final */ Map<a, fn2.f<?, ?>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.b == aVar.b) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    /*
    static {
        c();
    }
    */

    @DexIgnore
    public sm2() {
        this.a = new HashMap();
    }

    @DexIgnore
    public static sm2 a() {
        sm2 sm2 = b;
        if (sm2 == null) {
            synchronized (sm2.class) {
                sm2 = b;
                if (sm2 == null) {
                    sm2 = d;
                    b = sm2;
                }
            }
        }
        return sm2;
    }

    @DexIgnore
    public static sm2 b() {
        Class<sm2> cls = sm2.class;
        sm2 sm2 = c;
        if (sm2 == null) {
            synchronized (cls) {
                sm2 = c;
                if (sm2 == null) {
                    sm2 = dn2.a(cls);
                    c = sm2;
                }
            }
        }
        return sm2;
    }

    @DexIgnore
    public static Class<?> c() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public sm2(boolean z) {
        this.a = Collections.emptyMap();
    }

    @DexIgnore
    public final <ContainingType extends ro2> fn2.f<ContainingType, ?> a(ContainingType containingtype, int i) {
        return this.a.get(new a(containingtype, i));
    }
}
