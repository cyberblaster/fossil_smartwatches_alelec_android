package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.ap3;
import com.fossil.ep3;
import com.fossil.hp3;
import com.fossil.im3;
import com.fossil.zl3;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fp3<T> extends cp3<T> implements Serializable {
    @DexIgnore
    public /* final */ Type runtimeType;
    @DexIgnore
    public transient ep3 typeResolver;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ap3.b<T> {
        @DexIgnore
        public a(Method method) {
            super(method);
        }

        @DexIgnore
        public fp3<T> a() {
            return fp3.this;
        }

        @DexIgnore
        public String toString() {
            return a() + CodelessMatcher.CURRENT_CLASS_NAME + super.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ap3.a<T> {
        @DexIgnore
        public b(Constructor constructor) {
            super(constructor);
        }

        @DexIgnore
        public fp3<T> a() {
            return fp3.this;
        }

        @DexIgnore
        public Type[] b() {
            return fp3.this.resolveInPlace(super.b());
        }

        @DexIgnore
        public String toString() {
            return a() + "(" + ek3.c(", ").a((Object[]) b()) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends gp3 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(TypeVariable<?> typeVariable) {
            throw new IllegalArgumentException(fp3.this.runtimeType + "contains a type variable and is not safe for the operation");
        }

        @DexIgnore
        public void a(WildcardType wildcardType) {
            a(wildcardType.getLowerBounds());
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        public void a(ParameterizedType parameterizedType) {
            a(parameterizedType.getActualTypeArguments());
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        public void a(GenericArrayType genericArrayType) {
            a(genericArrayType.getGenericComponentType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends gp3 {
        @DexIgnore
        public /* final */ /* synthetic */ im3.a b;

        @DexIgnore
        public d(fp3 fp3, im3.a aVar) {
            this.b = aVar;
        }

        @DexIgnore
        public void a(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        public void a(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        public void a(ParameterizedType parameterizedType) {
            this.b.a((Class) parameterizedType.getRawType());
        }

        @DexIgnore
        public void a(Class<?> cls) {
            this.b.a(cls);
        }

        @DexIgnore
        public void a(GenericArrayType genericArrayType) {
            this.b.a(hp3.a((Class<?>) fp3.of(genericArrayType.getGenericComponentType()).getRawType()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public /* final */ Type[] a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(Type[] typeArr, boolean z) {
            this.a = typeArr;
            this.b = z;
        }

        @DexIgnore
        public boolean a(Type type) {
            for (Type of : this.a) {
                boolean isSubtypeOf = fp3.of(of).isSubtypeOf(type);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }

        @DexIgnore
        public boolean b(Type type) {
            fp3<?> of = fp3.of(type);
            for (Type isSubtypeOf : this.a) {
                boolean isSubtypeOf2 = of.isSubtypeOf(isSubtypeOf);
                boolean z = this.b;
                if (isSubtypeOf2 == z) {
                    return z;
                }
            }
            return !this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends fp3<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient im3<fp3<? super T>> b;

        @DexIgnore
        public f() {
            super();
        }

        @DexIgnore
        private Object readResolve() {
            return fp3.this.getTypes().classes();
        }

        @DexIgnore
        public fp3<T>.k classes() {
            return this;
        }

        @DexIgnore
        public fp3<T>.k interfaces() {
            throw new UnsupportedOperationException("classes().interfaces() not supported.");
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return im3.copyOf(i.b.a().a(fp3.this.getRawTypes()));
        }

        @DexIgnore
        public /* synthetic */ f(fp3 fp3, a aVar) {
            this();
        }

        @DexIgnore
        public Set<fp3<? super T>> delegate() {
            im3<fp3<? super T>> im3 = this.b;
            if (im3 != null) {
                return im3;
            }
            im3<fp3<?>> b2 = ll3.a(i.a.a().a(fp3.this)).a(j.IGNORE_TYPE_VARIABLE_OR_WILDCARD).b();
            this.b = b2;
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends fp3<T> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public h(Type type) {
            super(type, (a) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i<K> {
        @DexIgnore
        public static /* final */ i<fp3<?>> a; // = new a();
        @DexIgnore
        public static /* final */ i<Class<?>> b; // = new b();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a extends i<fp3<?>> {
            @DexIgnore
            public a() {
                super((a) null);
            }

            @DexIgnore
            /* renamed from: a */
            public Iterable<? extends fp3<?>> b(fp3<?> fp3) {
                return fp3.getGenericInterfaces();
            }

            @DexIgnore
            /* renamed from: b */
            public Class<?> c(fp3<?> fp3) {
                return fp3.getRawType();
            }

            @DexIgnore
            /* renamed from: c */
            public fp3<?> d(fp3<?> fp3) {
                return fp3.getGenericSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class b extends i<Class<?>> {
            @DexIgnore
            public b() {
                super((a) null);
            }

            @DexIgnore
            /* renamed from: a */
            public Iterable<? extends Class<?>> b(Class<?> cls) {
                return Arrays.asList(cls.getInterfaces());
            }

            @DexIgnore
            public Class<?> b(Class<?> cls) {
                return cls;
            }

            @DexIgnore
            public /* bridge */ /* synthetic */ Class c(Object obj) {
                Class cls = (Class) obj;
                b((Class<?>) cls);
                return cls;
            }

            @DexIgnore
            /* renamed from: c */
            public Class<?> d(Class<?> cls) {
                return cls.getSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends e<K> {
            @DexIgnore
            public c(i iVar, i iVar2) {
                super(iVar2);
            }

            @DexIgnore
            public zl3<K> a(Iterable<? extends K> iterable) {
                zl3.b builder = zl3.builder();
                for (Object next : iterable) {
                    if (!c(next).isInterface()) {
                        builder.a((Object) next);
                    }
                }
                return super.a(builder.a());
            }

            @DexIgnore
            public Iterable<? extends K> b(K k) {
                return im3.of();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class d extends jn3<K> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator a;
            @DexIgnore
            public /* final */ /* synthetic */ Map b;

            @DexIgnore
            public d(Comparator comparator, Map map) {
                this.a = comparator;
                this.b = map;
            }

            @DexIgnore
            public int compare(K k, K k2) {
                return this.a.compare(this.b.get(k), this.b.get(k2));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class e<K> extends i<K> {
            @DexIgnore
            public /* final */ i<K> c;

            @DexIgnore
            public e(i<K> iVar) {
                super((a) null);
                this.c = iVar;
            }

            @DexIgnore
            public Class<?> c(K k) {
                return this.c.c(k);
            }

            @DexIgnore
            public K d(K k) {
                return this.c.d(k);
            }
        }

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public final i<K> a() {
            return new c(this, this);
        }

        @DexIgnore
        public abstract Iterable<? extends K> b(K k);

        @DexIgnore
        public abstract Class<?> c(K k);

        @DexIgnore
        public abstract K d(K k);

        @DexIgnore
        public /* synthetic */ i(a aVar) {
            this();
        }

        @DexIgnore
        public final zl3<K> a(K k) {
            return a(zl3.of(k));
        }

        @DexIgnore
        public zl3<K> a(Iterable<? extends K> iterable) {
            HashMap b2 = ym3.b();
            for (Object a2 : iterable) {
                a(a2, b2);
            }
            return a(b2, jn3.natural().reverse());
        }

        @DexIgnore
        public final int a(K k, Map<? super K, Integer> map) {
            Integer num = map.get(k);
            if (num != null) {
                return num.intValue();
            }
            int isInterface = c(k).isInterface();
            for (Object a2 : b(k)) {
                isInterface = Math.max(isInterface, a(a2, map));
            }
            Object d2 = d(k);
            if (d2 != null) {
                isInterface = Math.max(isInterface, a(d2, map));
            }
            int i = isInterface + 1;
            map.put(k, Integer.valueOf(i));
            return i;
        }

        @DexIgnore
        public static <K, V> zl3<K> a(Map<K, V> map, Comparator<? super V> comparator) {
            return new d(comparator, map).immutableSortedCopy(map.keySet());
        }
    }

    @DexIgnore
    public enum j implements kk3<fp3<?>> {
        IGNORE_TYPE_VARIABLE_OR_WILDCARD {
            @DexIgnore
            public boolean apply(fp3<?> fp3) {
                return !(fp3.runtimeType instanceof TypeVariable) && !(fp3.runtimeType instanceof WildcardType);
            }
        },
        INTERFACE_ONLY {
            @DexIgnore
            public boolean apply(fp3<?> fp3) {
                return fp3.getRawType().isInterface();
            }
        };
    }

    @DexIgnore
    public /* synthetic */ fp3(Type type, a aVar) {
        this(type);
    }

    @DexIgnore
    public static e any(Type[] typeArr) {
        return new e(typeArr, true);
    }

    @DexIgnore
    private fp3<? super T> boundAsSuperclass(Type type) {
        fp3<?> of = of(type);
        if (of.getRawType().isInterface()) {
            return null;
        }
        return of;
    }

    @DexIgnore
    private zl3<fp3<? super T>> boundsAsInterfaces(Type[] typeArr) {
        zl3.b builder = zl3.builder();
        for (Type of : typeArr) {
            fp3<?> of2 = of(of);
            if (of2.getRawType().isInterface()) {
                builder.a((Object) of2);
            }
        }
        return builder.a();
    }

    @DexIgnore
    public static e every(Type[] typeArr) {
        return new e(typeArr, false);
    }

    @DexIgnore
    private fp3<? extends T> getArraySubtype(Class<?> cls) {
        return of(newArrayClassOrGenericArrayType(getComponentType().getSubtype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    private fp3<? super T> getArraySupertype(Class<? super T> cls) {
        fp3<?> componentType = getComponentType();
        jk3.a(componentType, "%s isn't a super type of %s", (Object) cls, (Object) this);
        return of(newArrayClassOrGenericArrayType(componentType.getSupertype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    private Type getOwnerTypeIfPresent() {
        Type type = this.runtimeType;
        if (type instanceof ParameterizedType) {
            return ((ParameterizedType) type).getOwnerType();
        }
        if (type instanceof Class) {
            return ((Class) type).getEnclosingClass();
        }
        return null;
    }

    @DexIgnore
    private im3<Class<? super T>> getRawTypes() {
        im3.a builder = im3.builder();
        new d(this, builder).a(this.runtimeType);
        return builder.a();
    }

    @DexIgnore
    private fp3<? extends T> getSubtypeFromLowerBounds(Class<?> cls, Type[] typeArr) {
        if (typeArr.length > 0) {
            return of(typeArr[0]).getSubtype(cls);
        }
        throw new IllegalArgumentException(cls + " isn't a subclass of " + this);
    }

    @DexIgnore
    private fp3<? super T> getSupertypeFromUpperBounds(Class<? super T> cls, Type[] typeArr) {
        for (Type of : typeArr) {
            fp3<?> of2 = of(of);
            if (of2.isSubtypeOf((Type) cls)) {
                return of2.getSupertype(cls);
            }
        }
        throw new IllegalArgumentException(cls + " isn't a super type of " + this);
    }

    @DexIgnore
    private boolean is(Type type) {
        if (this.runtimeType.equals(type)) {
            return true;
        }
        if (!(type instanceof WildcardType)) {
            return false;
        }
        WildcardType wildcardType = (WildcardType) type;
        if (!every(wildcardType.getUpperBounds()).b(this.runtimeType) || !every(wildcardType.getLowerBounds()).a(this.runtimeType)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    private boolean isOwnedBySubtypeOf(Type type) {
        Iterator it = getTypes().iterator();
        while (it.hasNext()) {
            Type ownerTypeIfPresent = ((fp3) it.next()).getOwnerTypeIfPresent();
            if (ownerTypeIfPresent != null && of(ownerTypeIfPresent).isSubtypeOf(type)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private boolean isSubtypeOfArrayType(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (!cls.isArray()) {
                return false;
            }
            return of(cls.getComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(((GenericArrayType) type).getGenericComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isSubtypeOfParameterizedType(ParameterizedType parameterizedType) {
        Class<? super Object> rawType = of((Type) parameterizedType).getRawType();
        if (!someRawTypeIsSubclassOf(rawType)) {
            return false;
        }
        TypeVariable[] typeParameters = rawType.getTypeParameters();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (int i2 = 0; i2 < typeParameters.length; i2++) {
            if (!resolveType(typeParameters[i2]).is(actualTypeArguments[i2])) {
                return false;
            }
        }
        if (Modifier.isStatic(((Class) parameterizedType.getRawType()).getModifiers()) || parameterizedType.getOwnerType() == null || isOwnedBySubtypeOf(parameterizedType.getOwnerType())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    private boolean isSupertypeOfArray(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (!cls.isArray()) {
                return cls.isAssignableFrom(Object[].class);
            }
            return of(genericArrayType.getGenericComponentType()).isSubtypeOf((Type) cls.getComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(genericArrayType.getGenericComponentType()).isSubtypeOf(((GenericArrayType) this.runtimeType).getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isWrapper() {
        return yo3.a().contains(this.runtimeType);
    }

    @DexIgnore
    public static Type newArrayClassOrGenericArrayType(Type type) {
        return hp3.e.JAVA7.newArrayType(type);
    }

    @DexIgnore
    public static <T> fp3<T> of(Class<T> cls) {
        return new h(cls);
    }

    @DexIgnore
    private Type[] resolveInPlace(Type[] typeArr) {
        for (int i2 = 0; i2 < typeArr.length; i2++) {
            typeArr[i2] = resolveType(typeArr[i2]).getType();
        }
        return typeArr;
    }

    @DexIgnore
    private fp3<?> resolveSupertype(Type type) {
        fp3<?> resolveType = resolveType(type);
        resolveType.typeResolver = this.typeResolver;
        return resolveType;
    }

    @DexIgnore
    private Type resolveTypeArgsForSubclass(Class<?> cls) {
        if ((this.runtimeType instanceof Class) && (cls.getTypeParameters().length == 0 || getRawType().getTypeParameters().length != 0)) {
            return cls;
        }
        fp3<? extends Object> genericType = toGenericType(cls);
        return new ep3().a(genericType.getSupertype(getRawType()).runtimeType, this.runtimeType).a(genericType.runtimeType);
    }

    @DexIgnore
    private boolean someRawTypeIsSubclassOf(Class<?> cls) {
        Iterator it = getRawTypes().iterator();
        while (it.hasNext()) {
            if (cls.isAssignableFrom((Class) it.next())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static <T> fp3<? extends T> toGenericType(Class<T> cls) {
        if (cls.isArray()) {
            return of(hp3.b(toGenericType(cls.getComponentType()).runtimeType));
        }
        TypeVariable[] typeParameters = cls.getTypeParameters();
        Type type = (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) ? null : toGenericType(cls.getEnclosingClass()).runtimeType;
        if (typeParameters.length > 0 || (type != null && type != cls.getEnclosingClass())) {
            return of((Type) hp3.a(type, (Class<?>) cls, (Type[]) typeParameters));
        }
        return of(cls);
    }

    @DexIgnore
    public final ap3<T, T> constructor(Constructor<?> constructor) {
        jk3.a(constructor.getDeclaringClass() == getRawType(), "%s not declared by %s", (Object) constructor, (Object) getRawType());
        return new b(constructor);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof fp3) {
            return this.runtimeType.equals(((fp3) obj).runtimeType);
        }
        return false;
    }

    @DexIgnore
    public final fp3<?> getComponentType() {
        Type a2 = hp3.a(this.runtimeType);
        if (a2 == null) {
            return null;
        }
        return of(a2);
    }

    @DexIgnore
    public final zl3<fp3<? super T>> getGenericInterfaces() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundsAsInterfaces(((TypeVariable) type).getBounds());
        }
        if (type instanceof WildcardType) {
            return boundsAsInterfaces(((WildcardType) type).getUpperBounds());
        }
        zl3.b builder = zl3.builder();
        for (Type resolveSupertype : getRawType().getGenericInterfaces()) {
            builder.a((Object) resolveSupertype(resolveSupertype));
        }
        return builder.a();
    }

    @DexIgnore
    public final fp3<? super T> getGenericSuperclass() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundAsSuperclass(((TypeVariable) type).getBounds()[0]);
        }
        if (type instanceof WildcardType) {
            return boundAsSuperclass(((WildcardType) type).getUpperBounds()[0]);
        }
        Type genericSuperclass = getRawType().getGenericSuperclass();
        if (genericSuperclass == null) {
            return null;
        }
        return resolveSupertype(genericSuperclass);
    }

    @DexIgnore
    public final Class<? super T> getRawType() {
        return (Class) getRawTypes().iterator().next();
    }

    @DexIgnore
    public final fp3<? extends T> getSubtype(Class<?> cls) {
        jk3.a(!(this.runtimeType instanceof TypeVariable), "Cannot get subtype of type variable <%s>", (Object) this);
        Type type = this.runtimeType;
        if (type instanceof WildcardType) {
            return getSubtypeFromLowerBounds(cls, ((WildcardType) type).getLowerBounds());
        }
        if (isArray()) {
            return getArraySubtype(cls);
        }
        jk3.a(getRawType().isAssignableFrom(cls), "%s isn't a subclass of %s", (Object) cls, (Object) this);
        return of(resolveTypeArgsForSubclass(cls));
    }

    @DexIgnore
    public final fp3<? super T> getSupertype(Class<? super T> cls) {
        jk3.a(someRawTypeIsSubclassOf(cls), "%s is not a super class of %s", (Object) cls, (Object) this);
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return getSupertypeFromUpperBounds(cls, ((TypeVariable) type).getBounds());
        }
        if (type instanceof WildcardType) {
            return getSupertypeFromUpperBounds(cls, ((WildcardType) type).getUpperBounds());
        }
        if (cls.isArray()) {
            return getArraySupertype(cls);
        }
        return resolveSupertype(toGenericType(cls).runtimeType);
    }

    @DexIgnore
    public final Type getType() {
        return this.runtimeType;
    }

    @DexIgnore
    public final fp3<T>.k getTypes() {
        return new k();
    }

    @DexIgnore
    public int hashCode() {
        return this.runtimeType.hashCode();
    }

    @DexIgnore
    public final boolean isArray() {
        return getComponentType() != null;
    }

    @DexIgnore
    public final boolean isPrimitive() {
        Type type = this.runtimeType;
        return (type instanceof Class) && ((Class) type).isPrimitive();
    }

    @DexIgnore
    public final boolean isSubtypeOf(fp3<?> fp3) {
        return isSubtypeOf(fp3.getType());
    }

    @DexIgnore
    public final boolean isSupertypeOf(fp3<?> fp3) {
        return fp3.isSubtypeOf(getType());
    }

    @DexIgnore
    public final ap3<T, Object> method(Method method) {
        jk3.a(someRawTypeIsSubclassOf(method.getDeclaringClass()), "%s not declared by %s", (Object) method, (Object) this);
        return new a(method);
    }

    @DexIgnore
    public final fp3<T> rejectTypeVariables() {
        new c().a(this.runtimeType);
        return this;
    }

    @DexIgnore
    public final fp3<?> resolveType(Type type) {
        jk3.a(type);
        ep3 ep3 = this.typeResolver;
        if (ep3 == null) {
            ep3 = ep3.b(this.runtimeType);
            this.typeResolver = ep3;
        }
        return of(ep3.a(type));
    }

    @DexIgnore
    public String toString() {
        return hp3.e(this.runtimeType);
    }

    @DexIgnore
    public final fp3<T> unwrap() {
        return isWrapper() ? of(yo3.a((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public final <X> fp3<T> where(dp3<X> dp3, fp3<X> fp3) {
        return new h(new ep3().a((Map<ep3.d, ? extends Type>) bm3.of(new ep3.d(dp3.a), fp3.runtimeType)).a(this.runtimeType));
    }

    @DexIgnore
    public final fp3<T> wrap() {
        return isPrimitive() ? of(yo3.b((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public Object writeReplace() {
        return of(new ep3().a(this.runtimeType));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends fp3<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ transient fp3<T>.k b;
        @DexIgnore
        public transient im3<fp3<? super T>> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements kk3<Class<?>> {
            @DexIgnore
            public a(g gVar) {
            }

            @DexIgnore
            /* renamed from: a */
            public boolean apply(Class<?> cls) {
                return cls.isInterface();
            }
        }

        @DexIgnore
        public g(fp3<T>.k kVar) {
            super();
            this.b = kVar;
        }

        @DexIgnore
        private Object readResolve() {
            return fp3.this.getTypes().interfaces();
        }

        @DexIgnore
        public fp3<T>.k classes() {
            throw new UnsupportedOperationException("interfaces().classes() not supported.");
        }

        @DexIgnore
        public fp3<T>.k interfaces() {
            return this;
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return ll3.a(i.b.a(fp3.this.getRawTypes())).a(new a(this)).b();
        }

        @DexIgnore
        public Set<fp3<? super T>> delegate() {
            im3<fp3<? super T>> im3 = this.c;
            if (im3 != null) {
                return im3;
            }
            im3<fp3<? super T>> b2 = ll3.a(this.b).a(j.INTERFACE_ONLY).b();
            this.c = b2;
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends rl3<fp3<? super T>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient im3<fp3<? super T>> a;

        @DexIgnore
        public k() {
        }

        @DexIgnore
        public fp3<T>.k classes() {
            return new f(fp3.this, (a) null);
        }

        @DexIgnore
        public fp3<T>.k interfaces() {
            return new g(this);
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return im3.copyOf(i.b.a(fp3.this.getRawTypes()));
        }

        @DexIgnore
        public Set<fp3<? super T>> delegate() {
            im3<fp3<? super T>> im3 = this.a;
            if (im3 != null) {
                return im3;
            }
            im3<fp3<?>> b = ll3.a(i.a.a(fp3.this)).a(j.IGNORE_TYPE_VARIABLE_OR_WILDCARD).b();
            this.a = b;
            return b;
        }
    }

    @DexIgnore
    public fp3() {
        this.runtimeType = capture();
        Type type = this.runtimeType;
        jk3.b(!(type instanceof TypeVariable), "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead.", (Object) type);
    }

    @DexIgnore
    public static fp3<?> of(Type type) {
        return new h(type);
    }

    @DexIgnore
    public final boolean isSubtypeOf(Type type) {
        jk3.a(type);
        if (type instanceof WildcardType) {
            return any(((WildcardType) type).getLowerBounds()).b(this.runtimeType);
        }
        Type type2 = this.runtimeType;
        if (type2 instanceof WildcardType) {
            return any(((WildcardType) type2).getUpperBounds()).a(type);
        }
        if (type2 instanceof TypeVariable) {
            if (type2.equals(type) || any(((TypeVariable) this.runtimeType).getBounds()).a(type)) {
                return true;
            }
            return false;
        } else if (type2 instanceof GenericArrayType) {
            return of(type).isSupertypeOfArray((GenericArrayType) this.runtimeType);
        } else {
            if (type instanceof Class) {
                return someRawTypeIsSubclassOf((Class) type);
            }
            if (type instanceof ParameterizedType) {
                return isSubtypeOfParameterizedType((ParameterizedType) type);
            }
            if (type instanceof GenericArrayType) {
                return isSubtypeOfArrayType((GenericArrayType) type);
            }
            return false;
        }
    }

    @DexIgnore
    public final boolean isSupertypeOf(Type type) {
        return of(type).isSubtypeOf(getType());
    }

    @DexIgnore
    public final <X> fp3<T> where(dp3<X> dp3, Class<X> cls) {
        return where(dp3, of(cls));
    }

    @DexIgnore
    public fp3(Class<?> cls) {
        Type capture = super.capture();
        if (capture instanceof Class) {
            this.runtimeType = capture;
        } else {
            this.runtimeType = of(cls).resolveType(capture).runtimeType;
        }
    }

    @DexIgnore
    public fp3(Type type) {
        jk3.a(type);
        this.runtimeType = type;
    }
}
