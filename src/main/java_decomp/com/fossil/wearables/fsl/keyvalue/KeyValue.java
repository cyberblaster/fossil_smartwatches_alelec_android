package com.fossil.wearables.fsl.keyvalue;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class KeyValue extends BaseModel {
    @DexIgnore
    @DatabaseField
    public String key;
    @DexIgnore
    @DatabaseField
    public String value;

    @DexIgnore
    public KeyValue() {
    }

    @DexIgnore
    public String getKey() {
        return this.key;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }

    @DexIgnore
    public void setKey(String str) {
        this.key = str;
    }

    @DexIgnore
    public void setValue(String str) {
        this.value = str;
    }

    @DexIgnore
    public KeyValue(String str, String str2) {
        this.key = str;
        this.value = str2;
    }
}
