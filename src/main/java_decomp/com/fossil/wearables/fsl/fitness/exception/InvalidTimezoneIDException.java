package com.fossil.wearables.fsl.fitness.exception;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class InvalidTimezoneIDException extends Exception {
    @DexIgnore
    public InvalidTimezoneIDException(String str) {
        super("Invalid timezone id: " + str);
    }
}
