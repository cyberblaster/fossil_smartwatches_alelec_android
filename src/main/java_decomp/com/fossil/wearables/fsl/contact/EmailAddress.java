package com.fossil.wearables.fsl.contact;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class EmailAddress extends BaseModel {
    @DexIgnore
    @DatabaseField
    public String address;
    @DexIgnore
    @DatabaseField(columnName = "email_address_id", foreign = true, foreignAutoRefresh = true)
    public Contact contact;

    @DexIgnore
    public String getAddress() {
        return this.address;
    }

    @DexIgnore
    public Contact getContact() {
        return this.contact;
    }

    @DexIgnore
    public void setAddress(String str) {
        this.address = str;
    }

    @DexIgnore
    public void setContact(Contact contact2) {
        this.contact = contact2;
    }
}
