package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.Where;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Firmware;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ao4 extends BaseDbProvider implements zn4 {
    @DexIgnore
    public static /* final */ String a; // = "ao4";

    @DexIgnore
    public ao4(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public Firmware a(String str) {
        try {
            Where<Firmware, Integer> where = g().queryBuilder().where();
            where.eq("deviceModel", str);
            List<Firmware> query = where.query();
            if (query == null || query.isEmpty()) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            local.e(str2, "Error inside " + a + ".getLatestFirmware with model " + str + ") - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final Dao<Firmware, Integer> g() throws SQLException {
        return this.databaseHelper.getDao(Firmware.class);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{Firmware.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public void a(Firmware firmware) {
        try {
            g().createOrUpdate(firmware);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "Error inside " + a + ".addOrUpdatePhoto - e=" + e);
        }
    }
}
