package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class um6 extends ym6 implements uk6 {
    @DexIgnore
    public /* final */ boolean b; // = j();

    @DexIgnore
    public um6(rm6 rm6) {
        super(true);
        a(rm6);
    }

    @DexIgnore
    public boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return true;
    }

    @DexIgnore
    public final boolean j() {
        ym6 ym6;
        qk6 qk6 = this.parentHandle;
        if (!(qk6 instanceof rk6)) {
            qk6 = null;
        }
        rk6 rk6 = (rk6) qk6;
        if (!(rk6 == null || (ym6 = (ym6) rk6.d) == null)) {
            while (!ym6.b()) {
                qk6 qk62 = ym6.parentHandle;
                if (!(qk62 instanceof rk6)) {
                    qk62 = null;
                }
                rk6 rk62 = (rk6) qk62;
                if (rk62 != null) {
                    ym6 = (ym6) rk62.d;
                    if (ym6 == null) {
                    }
                }
            }
            return true;
        }
        return false;
    }
}
