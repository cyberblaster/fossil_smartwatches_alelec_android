package com.fossil;

import com.j256.ormlite.logger.Logger;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp6 {
    @DexIgnore
    public static /* final */ long a; // = xo6.a("kotlinx.coroutines.scheduler.resolution.ns", 100000, 0, 0, 12, (Object) null);
    @DexIgnore
    public static /* final */ int b; // = xo6.a("kotlinx.coroutines.scheduler.offload.threshold", 96, 0, (int) Logger.DEFAULT_FULL_MESSAGE_LENGTH, 4, (Object) null);
    @DexIgnore
    public static /* final */ int c; // = xo6.a("kotlinx.coroutines.scheduler.core.pool.size", ci6.a(vo6.a(), 2), 1, 0, 8, (Object) null);
    @DexIgnore
    public static /* final */ int d; // = xo6.a("kotlinx.coroutines.scheduler.max.pool.size", ci6.a(vo6.a() * Logger.DEFAULT_FULL_MESSAGE_LENGTH, c, 2097150), 0, 2097150, 4, (Object) null);
    @DexIgnore
    public static /* final */ long e; // = TimeUnit.SECONDS.toNanos(xo6.a("kotlinx.coroutines.scheduler.keep.alive.sec", 5, 0, 0, 12, (Object) null));
    @DexIgnore
    public static rp6 f; // = kp6.a;

    /*
    static {
        int unused = xo6.a("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, (Object) null);
    }
    */
}
