package com.fossil;

import com.fossil.ns6;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms6 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService y; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), fr6.a("OkHttp Http2Connection", true));
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ h b;
    @DexIgnore
    public /* final */ Map<Integer, os6> c; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ ScheduledExecutorService h;
    @DexIgnore
    public /* final */ ExecutorService i;
    @DexIgnore
    public /* final */ rs6 j;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public long p; // = 0;
    @DexIgnore
    public long q;
    @DexIgnore
    public ss6 r; // = new ss6();
    @DexIgnore
    public /* final */ ss6 s; // = new ss6();
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public /* final */ Socket u;
    @DexIgnore
    public /* final */ ps6 v;
    @DexIgnore
    public /* final */ j w;
    @DexIgnore
    public /* final */ Set<Integer> x; // = new LinkedHashSet();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends er6 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ hs6 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, hs6 hs6) {
            super(str, objArr);
            this.b = i;
            this.c = hs6;
        }

        @DexIgnore
        public void b() {
            try {
                ms6.this.b(this.b, this.c);
            } catch (IOException unused) {
                ms6.this.k();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends er6 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.b = i;
            this.c = j;
        }

        @DexIgnore
        public void b() {
            try {
                ms6.this.v.b(this.b, this.c);
            } catch (IOException unused) {
                ms6.this.k();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends er6 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.b = i;
            this.c = list;
        }

        @DexIgnore
        public void b() {
            if (ms6.this.j.a(this.b, (List<is6>) this.c)) {
                try {
                    ms6.this.v.a(this.b, hs6.CANCEL);
                    synchronized (ms6.this) {
                        ms6.this.x.remove(Integer.valueOf(this.b));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends er6 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i, List list, boolean z) {
            super(str, objArr);
            this.b = i;
            this.c = list;
            this.d = z;
        }

        @DexIgnore
        public void b() {
            boolean a = ms6.this.j.a(this.b, this.c, this.d);
            if (a) {
                try {
                    ms6.this.v.a(this.b, hs6.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.d) {
                synchronized (ms6.this) {
                    ms6.this.x.remove(Integer.valueOf(this.b));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends er6 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ jt6 c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i, jt6 jt6, int i2, boolean z) {
            super(str, objArr);
            this.b = i;
            this.c = jt6;
            this.d = i2;
            this.e = z;
        }

        @DexIgnore
        public void b() {
            try {
                boolean a = ms6.this.j.a(this.b, this.c, this.d, this.e);
                if (a) {
                    ms6.this.v.a(this.b, hs6.CANCEL);
                }
                if (a || this.e) {
                    synchronized (ms6.this) {
                        ms6.this.x.remove(Integer.valueOf(this.b));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends er6 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ hs6 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i, hs6 hs6) {
            super(str, objArr);
            this.b = i;
            this.c = hs6;
        }

        @DexIgnore
        public void b() {
            ms6.this.j.a(this.b, this.c);
            synchronized (ms6.this) {
                ms6.this.x.remove(Integer.valueOf(this.b));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h {
        @DexIgnore
        public static /* final */ h a; // = new a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h {
            @DexIgnore
            public void a(os6 os6) throws IOException {
                os6.a(hs6.REFUSED_STREAM);
            }
        }

        @DexIgnore
        public void a(ms6 ms6) {
        }

        @DexIgnore
        public abstract void a(os6 os6) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i extends er6 {
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public i(boolean z, int i, int i2) {
            super("OkHttp %s ping %08x%08x", ms6.this.d, Integer.valueOf(i), Integer.valueOf(i2));
            this.b = z;
            this.c = i;
            this.d = i2;
        }

        @DexIgnore
        public void b() {
            ms6.this.a(this.b, this.c, this.d);
        }
    }

    /*
    static {
        Class<ms6> cls = ms6.class;
    }
    */

    @DexIgnore
    public ms6(g gVar) {
        g gVar2 = gVar;
        this.j = gVar2.f;
        boolean z = gVar2.g;
        this.a = z;
        this.b = gVar2.e;
        this.f = z ? 1 : 2;
        if (gVar2.g) {
            this.f += 2;
        }
        if (gVar2.g) {
            this.r.a(7, 16777216);
        }
        this.d = gVar2.b;
        this.h = new ScheduledThreadPoolExecutor(1, fr6.a(fr6.a("OkHttp %s Writer", this.d), false));
        if (gVar2.h != 0) {
            ScheduledExecutorService scheduledExecutorService = this.h;
            i iVar = new i(false, 0, 0);
            int i2 = gVar2.h;
            scheduledExecutorService.scheduleAtFixedRate(iVar, (long) i2, (long) i2, TimeUnit.MILLISECONDS);
        }
        this.i = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), fr6.a(fr6.a("OkHttp %s Push Observer", this.d), true));
        this.s.a(7, 65535);
        this.s.a(5, 16384);
        this.q = (long) this.s.c();
        this.u = gVar2.a;
        this.v = new ps6(gVar2.d, this.a);
        this.w = new j(new ns6(gVar2.c, this.a));
    }

    @DexIgnore
    public void c(int i2, hs6 hs6) {
        try {
            this.h.execute(new a("OkHttp %s stream %d", new Object[]{this.d, Integer.valueOf(i2)}, i2, hs6));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public boolean c(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public void close() throws IOException {
        a(hs6.NO_ERROR, hs6.CANCEL);
    }

    @DexIgnore
    public synchronized os6 d(int i2) {
        os6 remove;
        remove = this.c.remove(Integer.valueOf(i2));
        notifyAll();
        return remove;
    }

    @DexIgnore
    public void flush() throws IOException {
        this.v.flush();
    }

    @DexIgnore
    public final void k() {
        try {
            a(hs6.PROTOCOL_ERROR, hs6.PROTOCOL_ERROR);
        } catch (IOException unused) {
        }
    }

    @DexIgnore
    public synchronized boolean l() {
        return this.g;
    }

    @DexIgnore
    public synchronized int m() {
        return this.s.b(Integer.MAX_VALUE);
    }

    @DexIgnore
    public void n() throws IOException {
        a(true);
    }

    @DexIgnore
    public synchronized os6 b(int i2) {
        return this.c.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public synchronized void a(long j2) {
        this.p += j2;
        if (this.p >= ((long) (this.r.c() / 2))) {
            b(0, this.p);
            this.p = 0;
        }
    }

    @DexIgnore
    public void b(int i2, hs6 hs6) throws IOException {
        this.v.a(i2, hs6);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public Socket a;
        @DexIgnore
        public String b;
        @DexIgnore
        public lt6 c;
        @DexIgnore
        public kt6 d;
        @DexIgnore
        public h e; // = h.a;
        @DexIgnore
        public rs6 f; // = rs6.a;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public int h;

        @DexIgnore
        public g(boolean z) {
            this.g = z;
        }

        @DexIgnore
        public g a(Socket socket, String str, lt6 lt6, kt6 kt6) {
            this.a = socket;
            this.b = str;
            this.c = lt6;
            this.d = kt6;
            return this;
        }

        @DexIgnore
        public g a(h hVar) {
            this.e = hVar;
            return this;
        }

        @DexIgnore
        public g a(int i) {
            this.h = i;
            return this;
        }

        @DexIgnore
        public ms6 a() {
            return new ms6(this);
        }
    }

    @DexIgnore
    public void b(int i2, long j2) {
        try {
            this.h.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.d, Integer.valueOf(i2)}, i2, j2));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public void b(int i2, List<is6> list, boolean z) {
        try {
            a((er6) new d("OkHttp %s Push Headers[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, list, z));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends er6 implements ns6.b {
        @DexIgnore
        public /* final */ ns6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends er6 {
            @DexIgnore
            public /* final */ /* synthetic */ os6 b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, os6 os6) {
                super(str, objArr);
                this.b = os6;
            }

            @DexIgnore
            public void b() {
                try {
                    ms6.this.b.a(this.b);
                } catch (IOException e) {
                    at6 d = at6.d();
                    d.a(4, "Http2Connection.Listener failure for " + ms6.this.d, (Throwable) e);
                    try {
                        this.b.a(hs6.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends er6 {
            @DexIgnore
            public b(String str, Object... objArr) {
                super(str, objArr);
            }

            @DexIgnore
            public void b() {
                ms6 ms6 = ms6.this;
                ms6.b.a(ms6);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends er6 {
            @DexIgnore
            public /* final */ /* synthetic */ ss6 b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(String str, Object[] objArr, ss6 ss6) {
                super(str, objArr);
                this.b = ss6;
            }

            @DexIgnore
            public void b() {
                try {
                    ms6.this.v.a(this.b);
                } catch (IOException unused) {
                    ms6.this.k();
                }
            }
        }

        @DexIgnore
        public j(ns6 ns6) {
            super("OkHttp %s", ms6.this.d);
            this.b = ns6;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        public void a(boolean z, int i, lt6 lt6, int i2) throws IOException {
            if (ms6.this.c(i)) {
                ms6.this.a(i, lt6, i2, z);
                return;
            }
            os6 b2 = ms6.this.b(i);
            if (b2 == null) {
                ms6.this.c(i, hs6.PROTOCOL_ERROR);
                long j = (long) i2;
                ms6.this.a(j);
                lt6.skip(j);
                return;
            }
            b2.a(lt6, i2);
            if (z) {
                b2.i();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1 = com.fossil.hs6.PROTOCOL_ERROR;
            r0 = com.fossil.hs6.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            r2 = r4.c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
            r2 = th;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
        public void b() {
            hs6 hs6;
            ms6 ms6;
            hs6 hs62 = hs6.INTERNAL_ERROR;
            try {
                this.b.a((ns6.b) this);
                while (this.b.a(false, (ns6.b) this)) {
                }
                hs6 = hs6.NO_ERROR;
                hs62 = hs6.CANCEL;
                try {
                    ms6 = ms6.this;
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                hs6 = hs62;
            } catch (Throwable th) {
                th = th;
                hs6 = hs62;
                try {
                    ms6.this.a(hs6, hs62);
                } catch (IOException unused3) {
                }
                fr6.a((Closeable) this.b);
                throw th;
            }
            ms6.a(hs6, hs62);
            fr6.a((Closeable) this.b);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0074, code lost:
            r0.a(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0077, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0079, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        @DexIgnore
        public void a(boolean z, int i, int i2, List<is6> list) {
            if (ms6.this.c(i)) {
                ms6.this.b(i, list, z);
                return;
            }
            synchronized (ms6.this) {
                os6 b2 = ms6.this.b(i);
                if (b2 == null) {
                    if (!ms6.this.g) {
                        if (i > ms6.this.e) {
                            if (i % 2 != ms6.this.f % 2) {
                                int i3 = i;
                                os6 os6 = new os6(i3, ms6.this, false, z, fr6.b(list));
                                ms6.this.e = i;
                                ms6.this.c.put(Integer.valueOf(i), os6);
                                ms6.y.execute(new a("OkHttp %s stream %d", new Object[]{ms6.this.d, Integer.valueOf(i)}, os6));
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void a(int i, hs6 hs6) {
            if (ms6.this.c(i)) {
                ms6.this.a(i, hs6);
                return;
            }
            os6 d = ms6.this.d(i);
            if (d != null) {
                d.d(hs6);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v13, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        public void a(boolean z, ss6 ss6) {
            os6[] os6Arr;
            long j;
            int i;
            synchronized (ms6.this) {
                int c2 = ms6.this.s.c();
                if (z) {
                    ms6.this.s.a();
                }
                ms6.this.s.a(ss6);
                a(ss6);
                int c3 = ms6.this.s.c();
                os6Arr = null;
                if (c3 == -1 || c3 == c2) {
                    j = 0;
                } else {
                    j = (long) (c3 - c2);
                    if (!ms6.this.t) {
                        ms6.this.t = true;
                    }
                    if (!ms6.this.c.isEmpty()) {
                        os6Arr = ms6.this.c.values().toArray(new os6[ms6.this.c.size()]);
                    }
                }
                ms6.y.execute(new b("OkHttp %s settings", ms6.this.d));
            }
            if (os6Arr != null && j != 0) {
                for (os6 os6 : os6Arr) {
                    synchronized (os6) {
                        os6.a(j);
                    }
                }
            }
        }

        @DexIgnore
        public final void a(ss6 ss6) {
            try {
                ms6.this.h.execute(new c("OkHttp %s ACK Settings", new Object[]{ms6.this.d}, ss6));
            } catch (RejectedExecutionException unused) {
            }
        }

        @DexIgnore
        public void a(boolean z, int i, int i2) {
            if (z) {
                synchronized (ms6.this) {
                    boolean unused = ms6.this.o = false;
                    ms6.this.notifyAll();
                }
                return;
            }
            try {
                ms6.this.h.execute(new i(true, i, i2));
            } catch (RejectedExecutionException unused2) {
            }
        }

        @DexIgnore
        public void a(int i, hs6 hs6, mt6 mt6) {
            os6[] os6Arr;
            mt6.size();
            synchronized (ms6.this) {
                os6Arr = (os6[]) ms6.this.c.values().toArray(new os6[ms6.this.c.size()]);
                ms6.this.g = true;
            }
            for (os6 os6 : os6Arr) {
                if (os6.c() > i && os6.f()) {
                    os6.d(hs6.REFUSED_STREAM);
                    ms6.this.d(os6.c());
                }
            }
        }

        @DexIgnore
        public void a(int i, long j) {
            if (i == 0) {
                synchronized (ms6.this) {
                    ms6.this.q += j;
                    ms6.this.notifyAll();
                }
                return;
            }
            os6 b2 = ms6.this.b(i);
            if (b2 != null) {
                synchronized (b2) {
                    b2.a(j);
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, List<is6> list) {
            ms6.this.a(i2, list);
        }
    }

    @DexIgnore
    public os6 a(List<is6> list, boolean z) throws IOException {
        return a(0, list, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    public final os6 a(int i2, List<is6> list, boolean z) throws IOException {
        int i3;
        os6 os6;
        boolean z2;
        boolean z3 = !z;
        synchronized (this.v) {
            synchronized (this) {
                if (this.f > 1073741823) {
                    a(hs6.REFUSED_STREAM);
                }
                if (!this.g) {
                    i3 = this.f;
                    this.f += 2;
                    os6 = new os6(i3, this, z3, false, (sq6) null);
                    if (z && this.q != 0) {
                        if (os6.b != 0) {
                            z2 = false;
                            if (os6.g()) {
                                this.c.put(Integer.valueOf(i3), os6);
                            }
                        }
                    }
                    z2 = true;
                    if (os6.g()) {
                    }
                } else {
                    throw new gs6();
                }
            }
            if (i2 == 0) {
                this.v.a(z3, i3, i2, list);
            } else if (!this.a) {
                this.v.a(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (z2) {
            this.v.flush();
        }
        return os6;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.q), r8.v.l());
        r6 = (long) r3;
        r8.q -= r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        throw new java.io.InterruptedIOException();
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005a */
    public void a(int i2, boolean z, jt6 jt6, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.v.a(z, i2, jt6, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (true) {
                    if (this.q > 0) {
                        break;
                    } else if (this.c.containsKey(Integer.valueOf(i2))) {
                        wait();
                    } else {
                        throw new IOException("stream closed");
                    }
                }
            }
            j2 -= j3;
            this.v.a(z && j2 == 0, i2, jt6, min);
        }
    }

    @DexIgnore
    public void a(boolean z, int i2, int i3) {
        boolean z2;
        if (!z) {
            synchronized (this) {
                z2 = this.o;
                this.o = true;
            }
            if (z2) {
                k();
                return;
            }
        }
        try {
            this.v.a(z, i2, i3);
        } catch (IOException unused) {
            k();
        }
    }

    @DexIgnore
    public void a(hs6 hs6) throws IOException {
        synchronized (this.v) {
            synchronized (this) {
                if (!this.g) {
                    this.g = true;
                    int i2 = this.e;
                    this.v.a(i2, hs6, fr6.a);
                }
            }
        }
    }

    @DexIgnore
    public void a(hs6 hs6, hs6 hs62) throws IOException {
        os6[] os6Arr = null;
        try {
            a(hs6);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.c.isEmpty()) {
                os6Arr = (os6[]) this.c.values().toArray(new os6[this.c.size()]);
                this.c.clear();
            }
        }
        if (os6Arr != null) {
            for (os6 a2 : os6Arr) {
                try {
                    a2.a(hs62);
                } catch (IOException e3) {
                    if (e != null) {
                        e = e3;
                    }
                }
            }
        }
        try {
            this.v.close();
        } catch (IOException e4) {
            if (e == null) {
                e = e4;
            }
        }
        try {
            this.u.close();
        } catch (IOException e5) {
            e = e5;
        }
        this.h.shutdown();
        this.i.shutdown();
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public void a(boolean z) throws IOException {
        if (z) {
            this.v.k();
            this.v.b(this.r);
            int c2 = this.r.c();
            if (c2 != 65535) {
                this.v.b(0, (long) (c2 - 65535));
            }
        }
        new Thread(this.w).start();
    }

    @DexIgnore
    public void a(int i2, List<is6> list) {
        synchronized (this) {
            if (this.x.contains(Integer.valueOf(i2))) {
                c(i2, hs6.PROTOCOL_ERROR);
                return;
            }
            this.x.add(Integer.valueOf(i2));
            try {
                a((er6) new c("OkHttp %s Push Request[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, list));
            } catch (RejectedExecutionException unused) {
            }
        }
    }

    @DexIgnore
    public void a(int i2, lt6 lt6, int i3, boolean z) throws IOException {
        jt6 jt6 = new jt6();
        long j2 = (long) i3;
        lt6.i(j2);
        lt6.b(jt6, j2);
        if (jt6.p() == j2) {
            a((er6) new e("OkHttp %s Push Data[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, jt6, i3, z));
            return;
        }
        throw new IOException(jt6.p() + " != " + i3);
    }

    @DexIgnore
    public void a(int i2, hs6 hs6) {
        a((er6) new f("OkHttp %s Push Reset[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, hs6));
    }

    @DexIgnore
    public final synchronized void a(er6 er6) {
        if (!l()) {
            this.i.execute(er6);
        }
    }
}
