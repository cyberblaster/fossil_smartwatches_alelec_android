package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke5 implements MembersInjector<GoalTrackingOverviewFragment> {
    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
        goalTrackingOverviewFragment.g = goalTrackingOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
        goalTrackingOverviewFragment.h = goalTrackingOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        goalTrackingOverviewFragment.i = goalTrackingOverviewMonthPresenter;
    }
}
