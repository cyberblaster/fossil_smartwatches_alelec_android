package com.fossil;

import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface pu3<T> {
    @DexIgnore
    JsonElement serialize(T t, Type type, ou3 ou3);
}
