package com.fossil;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qg extends RecyclerView.o {
    @DexIgnore
    public RecyclerView a;
    @DexIgnore
    public Scroller b;
    @DexIgnore
    public /* final */ RecyclerView.q c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.q {
        @DexIgnore
        public boolean a; // = false;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0 && this.a) {
                this.a = false;
                qg.this.c();
            }
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            if (i != 0 || i2 != 0) {
                this.a = true;
            }
        }
    }

    @DexIgnore
    public abstract int a(RecyclerView.m mVar, int i, int i2);

    @DexIgnore
    public boolean a(int i, int i2) {
        RecyclerView.m layoutManager = this.a.getLayoutManager();
        if (layoutManager == null || this.a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.a.getMinFlingVelocity();
        if ((Math.abs(i2) > minFlingVelocity || Math.abs(i) > minFlingVelocity) && b(layoutManager, i, i2)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public abstract int[] a(RecyclerView.m mVar, View view);

    @DexIgnore
    public final void b() throws IllegalStateException {
        if (this.a.getOnFlingListener() == null) {
            this.a.addOnScrollListener(this.c);
            this.a.setOnFlingListener(this);
            return;
        }
        throw new IllegalStateException("An instance of OnFlingListener already set.");
    }

    @DexIgnore
    public abstract View c(RecyclerView.m mVar);

    @DexIgnore
    public void c() {
        RecyclerView.m layoutManager;
        View c2;
        RecyclerView recyclerView = this.a;
        if (recyclerView != null && (layoutManager = recyclerView.getLayoutManager()) != null && (c2 = c(layoutManager)) != null) {
            int[] a2 = a(layoutManager, c2);
            if (a2[0] != 0 || a2[1] != 0) {
                this.a.smoothScrollBy(a2[0], a2[1]);
            }
        }
    }

    @DexIgnore
    public int[] b(int i, int i2) {
        this.b.fling(0, 0, i, i2, RecyclerView.UNDEFINED_DURATION, Integer.MAX_VALUE, RecyclerView.UNDEFINED_DURATION, Integer.MAX_VALUE);
        return new int[]{this.b.getFinalX(), this.b.getFinalY()};
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gg {
        @DexIgnore
        public b(Context context) {
            super(context);
        }

        @DexIgnore
        public void a(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
            qg qgVar = qg.this;
            RecyclerView recyclerView = qgVar.a;
            if (recyclerView != null) {
                int[] a = qgVar.a(recyclerView.getLayoutManager(), view);
                int i = a[0];
                int i2 = a[1];
                int d = d(Math.max(Math.abs(i), Math.abs(i2)));
                if (d > 0) {
                    aVar.a(i, i2, d, this.j);
                }
            }
        }

        @DexIgnore
        public float a(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) throws IllegalStateException {
        RecyclerView recyclerView2 = this.a;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                a();
            }
            this.a = recyclerView;
            if (this.a != null) {
                b();
                this.b = new Scroller(this.a.getContext(), new DecelerateInterpolator());
                c();
            }
        }
    }

    @DexIgnore
    public final boolean b(RecyclerView.m mVar, int i, int i2) {
        RecyclerView.v a2;
        int a3;
        if (!(mVar instanceof RecyclerView.v.b) || (a2 = a(mVar)) == null || (a3 = a(mVar, i, i2)) == -1) {
            return false;
        }
        a2.c(a3);
        mVar.b(a2);
        return true;
    }

    @DexIgnore
    public final void a() {
        this.a.removeOnScrollListener(this.c);
        this.a.setOnFlingListener((RecyclerView.o) null);
    }

    @DexIgnore
    @Deprecated
    public gg b(RecyclerView.m mVar) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return null;
        }
        return new b(this.a.getContext());
    }

    @DexIgnore
    public RecyclerView.v a(RecyclerView.m mVar) {
        return b(mVar);
    }
}
