package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cf<T> extends AbstractList<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ e<T> c;
    @DexIgnore
    public /* final */ h d;
    @DexIgnore
    public /* final */ ef<T> e;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public T g; // = null;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public int o; // = Integer.MAX_VALUE;
    @DexIgnore
    public int p; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ArrayList<WeakReference<g>> r; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WeakReference<j>> s; // = new ArrayList<>();
    @DexIgnore
    public /* final */ k t; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends k {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cf$a$a")
        /* renamed from: com.fossil.cf$a$a  reason: collision with other inner class name */
        public class C0010a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ l a;
            @DexIgnore
            public /* final */ /* synthetic */ i b;
            @DexIgnore
            public /* final */ /* synthetic */ Throwable c;

            @DexIgnore
            public C0010a(l lVar, i iVar, Throwable th) {
                this.a = lVar;
                this.b = iVar;
                this.c = th;
            }

            @DexIgnore
            public void run() {
                for (int size = cf.this.s.size() - 1; size >= 0; size--) {
                    j jVar = (j) cf.this.s.get(size).get();
                    if (jVar == null) {
                        cf.this.s.remove(size);
                    } else {
                        jVar.a(this.a, this.b, this.c);
                    }
                }
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(l lVar, i iVar, Throwable th) {
            cf.this.a.execute(new C0010a(lVar, iVar, th));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public b(boolean z, boolean z2, boolean z3) {
            this.a = z;
            this.b = z2;
            this.c = z3;
        }

        @DexIgnore
        public void run() {
            if (this.a) {
                cf.this.c.a();
            }
            if (this.b) {
                cf.this.i = true;
            }
            if (this.c) {
                cf.this.j = true;
            }
            cf.this.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public c(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public void run() {
            cf.this.a(this.a, this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class d {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[l.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /*
        static {
            a[l.REFRESH.ordinal()] = 1;
            a[l.START.ordinal()] = 2;
            try {
                a[l.END.ordinal()] = 3;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T> {
        @DexIgnore
        public abstract void a();

        @DexIgnore
        public abstract void a(T t);

        @DexIgnore
        public abstract void b(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<Key, Value> {
        @DexIgnore
        public /* final */ xe<Key, Value> a;
        @DexIgnore
        public /* final */ h b;
        @DexIgnore
        public Executor c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public e e;
        @DexIgnore
        public Key f;

        @DexIgnore
        public f(xe<Key, Value> xeVar, h hVar) {
            if (xeVar == null) {
                throw new IllegalArgumentException("DataSource may not be null");
            } else if (hVar != null) {
                this.a = xeVar;
                this.b = hVar;
            } else {
                throw new IllegalArgumentException("Config may not be null");
            }
        }

        @DexIgnore
        public f<Key, Value> a(Executor executor) {
            this.d = executor;
            return this;
        }

        @DexIgnore
        public f<Key, Value> b(Executor executor) {
            this.c = executor;
            return this;
        }

        @DexIgnore
        public f<Key, Value> a(e eVar) {
            this.e = eVar;
            return this;
        }

        @DexIgnore
        public f<Key, Value> a(Key key) {
            this.f = key;
            return this;
        }

        @DexIgnore
        public cf<Value> a() {
            Executor executor = this.c;
            if (executor != null) {
                Executor executor2 = this.d;
                if (executor2 != null) {
                    return cf.a(this.a, executor, executor2, this.e, this.b, this.f);
                }
                throw new IllegalArgumentException("BackgroundThreadExecutor required");
            }
            throw new IllegalArgumentException("MainThreadExecutor required");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g {
        @DexIgnore
        public abstract void a(int i, int i2);

        @DexIgnore
        public abstract void b(int i, int i2);

        @DexIgnore
        public abstract void c(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public int a; // = -1;
            @DexIgnore
            public int b; // = -1;
            @DexIgnore
            public int c; // = -1;
            @DexIgnore
            public boolean d; // = true;
            @DexIgnore
            public int e; // = Integer.MAX_VALUE;

            @DexIgnore
            public a a(boolean z) {
                this.d = z;
                return this;
            }

            @DexIgnore
            public a b(int i) {
                if (i >= 1) {
                    this.a = i;
                    return this;
                }
                throw new IllegalArgumentException("Page size must be a positive number");
            }

            @DexIgnore
            public a c(int i) {
                this.b = i;
                return this;
            }

            @DexIgnore
            public a a(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public h a() {
                if (this.b < 0) {
                    this.b = this.a;
                }
                if (this.c < 0) {
                    this.c = this.a * 3;
                }
                if (this.d || this.b != 0) {
                    int i = this.e;
                    if (i == Integer.MAX_VALUE || i >= this.a + (this.b * 2)) {
                        return new h(this.a, this.b, this.d, this.c, this.e);
                    }
                    throw new IllegalArgumentException("Maximum size must be at least pageSize + 2*prefetchDist, pageSize=" + this.a + ", prefetchDist=" + this.b + ", maxSize=" + this.e);
                }
                throw new IllegalArgumentException("Placeholders and prefetch are the only ways to trigger loading of more data in the PagedList, so either placeholders must be enabled, or prefetch distance must be > 0.");
            }
        }

        @DexIgnore
        public h(int i, int i2, boolean z, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.e = i3;
            this.d = i4;
        }
    }

    @DexIgnore
    public enum i {
        IDLE,
        LOADING,
        DONE,
        ERROR,
        RETRYABLE_ERROR
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        void a(l lVar, i iVar, Throwable th);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class k {
        @DexIgnore
        public i a;
        @DexIgnore
        public Throwable b; // = null;
        @DexIgnore
        public i c;
        @DexIgnore
        public Throwable d;
        @DexIgnore
        public i e;
        @DexIgnore
        public Throwable f;

        @DexIgnore
        public k() {
            i iVar = i.IDLE;
            this.a = iVar;
            this.c = iVar;
            this.d = null;
            this.e = iVar;
            this.f = null;
        }

        @DexIgnore
        public i a() {
            return this.e;
        }

        @DexIgnore
        public abstract void a(l lVar, i iVar, Throwable th);

        @DexIgnore
        public Throwable b() {
            return this.f;
        }

        @DexIgnore
        public i c() {
            return this.a;
        }

        @DexIgnore
        public Throwable d() {
            return this.b;
        }

        @DexIgnore
        public i e() {
            return this.c;
        }

        @DexIgnore
        public Throwable f() {
            return this.d;
        }

        @DexIgnore
        public void b(l lVar, i iVar, Throwable th) {
            boolean z = false;
            boolean z2 = iVar == i.RETRYABLE_ERROR || iVar == i.ERROR;
            if (th != null) {
                z = true;
            }
            if (z2 == z) {
                int i = d.a[lVar.ordinal()];
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (!this.e.equals(iVar) || !cf.a((Object) this.f, (Object) th)) {
                                this.e = iVar;
                                this.f = th;
                            } else {
                                return;
                            }
                        }
                    } else if (!this.c.equals(iVar) || !cf.a((Object) this.d, (Object) th)) {
                        this.c = iVar;
                        this.d = th;
                    } else {
                        return;
                    }
                } else if (!this.a.equals(iVar) || !cf.a((Object) this.b, (Object) th)) {
                    this.a = iVar;
                    this.b = th;
                } else {
                    return;
                }
                a(lVar, iVar, th);
                return;
            }
            throw new IllegalArgumentException("Error states must be accompanied by a throwable, other states must not");
        }
    }

    @DexIgnore
    public enum l {
        REFRESH,
        START,
        END
    }

    @DexIgnore
    public cf(ef<T> efVar, Executor executor, Executor executor2, e<T> eVar, h hVar) {
        this.e = efVar;
        this.a = executor;
        this.b = executor2;
        this.c = eVar;
        this.d = hVar;
        h hVar2 = this.d;
        this.h = (hVar2.b * 2) + hVar2.a;
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public abstract void a(cf<T> cfVar, g gVar);

    @DexIgnore
    public void b(j jVar) {
        for (int size = this.s.size() - 1; size >= 0; size--) {
            j jVar2 = (j) this.s.get(size).get();
            if (jVar2 == null || jVar2 == jVar) {
                this.s.remove(size);
            }
        }
    }

    @DexIgnore
    public void c() {
        this.q.set(true);
    }

    @DexIgnore
    public abstract xe<?, T> d();

    @DexIgnore
    public void d(int i2) {
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        this.f = f() + i2;
        e(i2);
        this.o = Math.min(this.o, i2);
        this.p = Math.max(this.p, i2);
        a(true);
    }

    @DexIgnore
    public abstract Object e();

    @DexIgnore
    public abstract void e(int i2);

    @DexIgnore
    public void e(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.r.size() - 1; size >= 0; size--) {
                g gVar = (g) this.r.get(size).get();
                if (gVar != null) {
                    gVar.b(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void f(int i2) {
        this.f += i2;
        this.o += i2;
        this.p += i2;
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public T get(int i2) {
        T t2 = this.e.get(i2);
        if (t2 != null) {
            this.g = t2;
        }
        return t2;
    }

    @DexIgnore
    public boolean h() {
        return this.q.get();
    }

    @DexIgnore
    public boolean i() {
        return h();
    }

    @DexIgnore
    public List<T> j() {
        if (i()) {
            return this;
        }
        return new hf(this);
    }

    @DexIgnore
    public int size() {
        return this.e.size();
    }

    @DexIgnore
    public static <K, T> cf<T> a(xe<K, T> xeVar, Executor executor, Executor executor2, e<T> eVar, h hVar, K k2) {
        int i2;
        if (xeVar.isContiguous() || !hVar.c) {
            if (!xeVar.isContiguous()) {
                xeVar = ((gf) xeVar).wrapAsContiguousWithoutPlaceholders();
                if (k2 != null) {
                    i2 = ((Integer) k2).intValue();
                    return new we((ve) xeVar, executor, executor2, eVar, hVar, k2, i2);
                }
            }
            i2 = -1;
            return new we((ve) xeVar, executor, executor2, eVar, hVar, k2, i2);
        }
        return new Cif((gf) xeVar, executor, executor2, eVar, hVar, k2 != null ? ((Integer) k2).intValue() : 0);
    }

    @DexIgnore
    public int f() {
        return this.e.j();
    }

    @DexIgnore
    public void f(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.r.size() - 1; size >= 0; size--) {
                g gVar = (g) this.r.get(size).get();
                if (gVar != null) {
                    gVar.c(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.r.size() - 1; size >= 0; size--) {
                g gVar = (g) this.r.get(size).get();
                if (gVar != null) {
                    gVar.a(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void a(boolean z, boolean z2, boolean z3) {
        if (this.c != null) {
            if (this.o == Integer.MAX_VALUE) {
                this.o = this.e.size();
            }
            if (this.p == Integer.MIN_VALUE) {
                this.p = 0;
            }
            if (z || z2 || z3) {
                this.a.execute(new b(z, z2, z3));
                return;
            }
            return;
        }
        throw new IllegalStateException("Can't defer BoundaryCallback, no instance");
    }

    @DexIgnore
    public void a(boolean z) {
        boolean z2 = true;
        boolean z3 = this.i && this.o <= this.d.b;
        if (!this.j || this.p < (size() - 1) - this.d.b) {
            z2 = false;
        }
        if (z3 || z2) {
            if (z3) {
                this.i = false;
            }
            if (z2) {
                this.j = false;
            }
            if (z) {
                this.a.execute(new c(z3, z2));
            } else {
                a(z3, z2);
            }
        }
    }

    @DexIgnore
    public void a(boolean z, boolean z2) {
        if (z) {
            this.c.b(this.e.c());
        }
        if (z2) {
            this.c.a(this.e.d());
        }
    }

    @DexIgnore
    public void a(j jVar) {
        for (int size = this.s.size() - 1; size >= 0; size--) {
            if (((j) this.s.get(size).get()) == null) {
                this.s.remove(size);
            }
        }
        this.s.add(new WeakReference(jVar));
        jVar.a(l.REFRESH, this.t.c(), this.t.d());
        jVar.a(l.START, this.t.e(), this.t.f());
        jVar.a(l.END, this.t.a(), this.t.b());
    }

    @DexIgnore
    public void a(List<T> list, g gVar) {
        if (!(list == null || list == this)) {
            if (!list.isEmpty()) {
                a((cf) list, gVar);
            } else if (!this.e.isEmpty()) {
                gVar.b(0, this.e.size());
            }
        }
        for (int size = this.r.size() - 1; size >= 0; size--) {
            if (((g) this.r.get(size).get()) == null) {
                this.r.remove(size);
            }
        }
        this.r.add(new WeakReference(gVar));
    }

    @DexIgnore
    public void a(g gVar) {
        for (int size = this.r.size() - 1; size >= 0; size--) {
            g gVar2 = (g) this.r.get(size).get();
            if (gVar2 == null || gVar2 == gVar) {
                this.r.remove(size);
            }
        }
    }
}
