package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj1 {
    @DexIgnore
    public static /* final */ HashMap<String, ei1> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ yj1 b; // = new yj1();

    @DexIgnore
    public final long a(String str) {
        long j;
        synchronized (a) {
            ei1 ei1 = a.get(str);
            if (ei1 == null) {
                ei1 = new ei1(0, 128);
            }
            long min = Math.min(128, rh6.b(Math.pow((double) 2, (double) (ei1.a + 1))));
            if (min < ei1.b) {
                ei1.a++;
            }
            a.put(str, ei1);
            oa1 oa1 = oa1.a;
            Object[] objArr = {str, Long.valueOf(min)};
            j = min * 1000;
        }
        return j;
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (a) {
            ei1 ei1 = a.get(str);
            if (ei1 == null) {
                ei1 = new ei1(0, 128);
            }
            a.put(str, ei1.a(0, ei1.b));
            cd6 cd6 = cd6.a;
        }
    }
}
