package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gp4 {
    @DexIgnore
    public static /* final */ String b; // = "com.fossil.gp4";
    @DexIgnore
    public Weather a;

    @DexIgnore
    public void a(ku3 ku3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".parse - json=" + ku3);
        try {
            du3 du3 = new du3();
            du3.a(DateTime.class, new GsonConvertDateTime());
            du3.a(Date.class, new GsonConverterShortDate());
            this.a = (Weather) du3.a().a(ku3.toString(), Weather.class);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b;
            local2.d(str2, "parse mWeather=" + this.a);
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local3.e(str3, "parse error=" + e.toString());
        }
    }

    @DexIgnore
    public Weather a() {
        return this.a;
    }
}
