package com.fossil;

import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zx3 implements xx3 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("ISO-8859-1");

    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) {
        int i3;
        int i4;
        Charset charset;
        Charset charset2 = a;
        int i5 = 33;
        if (map != null) {
            if (map.containsKey(sx3.CHARACTER_SET)) {
                charset2 = Charset.forName(map.get(sx3.CHARACTER_SET).toString());
            }
            if (map.containsKey(sx3.ERROR_CORRECTION)) {
                i5 = Integer.parseInt(map.get(sx3.ERROR_CORRECTION).toString());
            }
            if (map.containsKey(sx3.AZTEC_LAYERS)) {
                charset = charset2;
                i4 = i5;
                i3 = Integer.parseInt(map.get(sx3.AZTEC_LAYERS).toString());
                return a(str, qx3, i, i2, charset, i4, i3);
            }
            charset = charset2;
            i4 = i5;
        } else {
            charset = charset2;
            i4 = 33;
        }
        i3 = 0;
        return a(str, qx3, i, i2, charset, i4, i3);
    }

    @DexIgnore
    public static iy3 a(String str, qx3 qx3, int i, int i2, Charset charset, int i3, int i4) {
        if (qx3 == qx3.AZTEC) {
            return a(cy3.a(str.getBytes(charset), i3, i4), i, i2);
        }
        throw new IllegalArgumentException("Can only encode AZTEC, but got " + qx3);
    }

    @DexIgnore
    public static iy3 a(ay3 ay3, int i, int i2) {
        iy3 a2 = ay3.a();
        if (a2 != null) {
            int c = a2.c();
            int b = a2.b();
            int max = Math.max(i, c);
            int max2 = Math.max(i2, b);
            int min = Math.min(max / c, max2 / b);
            int i3 = (max - (c * min)) / 2;
            int i4 = (max2 - (b * min)) / 2;
            iy3 iy3 = new iy3(max, max2);
            int i5 = 0;
            while (i5 < b) {
                int i6 = i3;
                int i7 = 0;
                while (i7 < c) {
                    if (a2.a(i7, i5)) {
                        iy3.a(i6, i4, min, min);
                    }
                    i7++;
                    i6 += min;
                }
                i5++;
                i4 += min;
            }
            return iy3;
        }
        throw new IllegalStateException();
    }
}
