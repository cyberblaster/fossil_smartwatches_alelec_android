package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v04 implements Factory<u04> {
    @DexIgnore
    public static /* final */ v04 a; // = new v04();

    @DexIgnore
    public static v04 a() {
        return a;
    }

    @DexIgnore
    public static u04 b() {
        return new u04();
    }

    @DexIgnore
    public u04 get() {
        return b();
    }
}
