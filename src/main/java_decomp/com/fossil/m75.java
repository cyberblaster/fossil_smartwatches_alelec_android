package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m75 extends k24<l75> {
    @DexIgnore
    void D(String str);

    @DexIgnore
    void H(String str);

    @DexIgnore
    void O(String str);

    @DexIgnore
    void a(int i, int i2, String str, String str2);

    @DexIgnore
    void a(String str, String str2, String str3);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, String str3);

    @DexIgnore
    void b(WatchApp watchApp);

    @DexIgnore
    void c(String str);

    @DexIgnore
    void d(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void f(String str);

    @DexIgnore
    void v(List<WatchApp> list);

    @DexIgnore
    void w(boolean z);
}
