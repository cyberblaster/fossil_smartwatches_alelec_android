package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class we4 extends ve4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362026, 1);
        A.put(2131362869, 2);
        A.put(2131362893, 3);
        A.put(2131363207, 4);
        A.put(2131363235, 5);
        A.put(2131363191, 6);
        A.put(2131363234, 7);
        A.put(2131363236, 8);
        A.put(2131363233, 9);
    }
    */

    @DexIgnore
    public we4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 10, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public we4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[0], objArr[2], objArr[3], objArr[6], objArr[4], objArr[9], objArr[7], objArr[5], objArr[8]);
        this.y = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
