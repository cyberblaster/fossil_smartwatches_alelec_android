package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ce0 implements Serializable {
    TOP_PRESS,
    TOP_HOLD,
    TOP_SHORT_PRESS_RELEASE,
    TOP_LONG_PRESS_RELEASE,
    TOP_SINGLE_CLICK,
    TOP_DOUBLE_CLICK,
    MIDDLE_PRESS,
    MIDDLE_HOLD,
    MIDDLE_SHORT_PRESS_RELEASE,
    MIDDLE_LONG_PRESS_RELEASE,
    MIDDLE_SINGLE_CLICK,
    MIDDLE_DOUBLE_CLICK,
    BOTTOM_PRESS,
    BOTTOM_HOLD,
    BOTTOM_SHORT_PRESS_RELEASE,
    BOTTOM_LONG_PRESS_RELEASE,
    BOTTOM_SINGLE_CLICK,
    BOTTOM_DOUBLE_CLICK;
    
    @DexIgnore
    public static /* final */ a b; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final ce0 a(String str) {
            for (ce0 ce0 : ce0.values()) {
                if (wg6.a(cw0.a((Enum<?>) ce0), str)) {
                    return ce0;
                }
            }
            return null;
        }
    }

    /*
    static {
        b = new a((qg6) null);
    }
    */
}
