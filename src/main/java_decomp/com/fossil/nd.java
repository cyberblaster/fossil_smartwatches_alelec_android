package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import com.fossil.od;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nd implements LifecycleOwner {
    @DexIgnore
    public static /* final */ nd i; // = new nd();
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public boolean c; // = true;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public Handler e;
    @DexIgnore
    public /* final */ LifecycleRegistry f; // = new LifecycleRegistry(this);
    @DexIgnore
    public Runnable g; // = new a();
    @DexIgnore
    public od.a h; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            nd.this.e();
            nd.this.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements od.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a() {
            nd.this.c();
        }

        @DexIgnore
        public void d() {
            nd.this.b();
        }

        @DexIgnore
        public void e() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends wc {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            od.a(activity).d(nd.this.h);
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            nd.this.a();
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            nd.this.d();
        }
    }

    @DexIgnore
    public static void b(Context context) {
        i.a(context);
    }

    @DexIgnore
    public static LifecycleOwner g() {
        return i;
    }

    @DexIgnore
    public void a() {
        this.b--;
        if (this.b == 0) {
            this.e.postDelayed(this.g, 700);
        }
    }

    @DexIgnore
    public void c() {
        this.a++;
        if (this.a == 1 && this.d) {
            this.f.a(Lifecycle.a.ON_START);
            this.d = false;
        }
    }

    @DexIgnore
    public void d() {
        this.a--;
        f();
    }

    @DexIgnore
    public void e() {
        if (this.b == 0) {
            this.c = true;
            this.f.a(Lifecycle.a.ON_PAUSE);
        }
    }

    @DexIgnore
    public void f() {
        if (this.a == 0 && this.c) {
            this.f.a(Lifecycle.a.ON_STOP);
            this.d = true;
        }
    }

    @DexIgnore
    public Lifecycle getLifecycle() {
        return this.f;
    }

    @DexIgnore
    public void b() {
        this.b++;
        if (this.b != 1) {
            return;
        }
        if (this.c) {
            this.f.a(Lifecycle.a.ON_RESUME);
            this.c = false;
            return;
        }
        this.e.removeCallbacks(this.g);
    }

    @DexIgnore
    public void a(Context context) {
        this.e = new Handler();
        this.f.a(Lifecycle.a.ON_CREATE);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new c());
    }
}
