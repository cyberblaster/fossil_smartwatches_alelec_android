package com.fossil;

import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g10 implements na6 {
    @DexIgnore
    public /* final */ x10 a;
    @DexIgnore
    public /* final */ u10 b;

    @DexIgnore
    public g10(x10 x10, u10 u10) {
        this.a = x10;
        this.b = u10;
    }

    @DexIgnore
    public static g10 a(x10 x10) {
        return new g10(x10, new u10(new ha6(new t10(new fa6(1000, 8), 0.1d), new ea6(5))));
    }

    @DexIgnore
    public boolean a(List<File> list) {
        long nanoTime = System.nanoTime();
        if (this.b.a(nanoTime)) {
            if (this.a.a(list)) {
                this.b.a();
                return true;
            }
            this.b.b(nanoTime);
        }
        return false;
    }
}
