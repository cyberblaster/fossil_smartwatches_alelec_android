package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wc4 extends vc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131362561, 1);
        y.put(2131362820, 2);
        y.put(2131362442, 3);
        y.put(2131362567, 4);
        y.put(2131362323, 5);
        y.put(2131362821, 6);
        y.put(2131361921, 7);
        y.put(2131362845, 8);
        y.put(2131362450, 9);
        y.put(2131362226, 10);
        y.put(2131362215, 11);
    }
    */

    @DexIgnore
    public wc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public wc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[7], objArr[11], objArr[10], objArr[5], objArr[3], objArr[9], objArr[1], objArr[4], objArr[6], objArr[2], objArr[8], objArr[0]);
        this.w = -1;
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
