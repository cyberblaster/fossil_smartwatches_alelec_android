package com.fossil;

import com.google.gson.JsonElement;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku3 extends JsonElement {
    @DexIgnore
    public /* final */ ev3<String, JsonElement> a; // = new ev3<>();

    @DexIgnore
    public void a(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = ju3.a;
        }
        this.a.put(str, jsonElement);
    }

    @DexIgnore
    public fu3 b(String str) {
        return (fu3) this.a.get(str);
    }

    @DexIgnore
    public ku3 c(String str) {
        return (ku3) this.a.get(str);
    }

    @DexIgnore
    public boolean d(String str) {
        return this.a.containsKey(str);
    }

    @DexIgnore
    public Set<Map.Entry<String, JsonElement>> entrySet() {
        return this.a.entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof ku3) && ((ku3) obj).a.equals(this.a));
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public void a(String str, String str2) {
        a(str, a((Object) str2));
    }

    @DexIgnore
    public void a(String str, Number number) {
        a(str, a((Object) number));
    }

    @DexIgnore
    public void a(String str, Boolean bool) {
        a(str, a((Object) bool));
    }

    @DexIgnore
    public final JsonElement a(Object obj) {
        return obj == null ? ju3.a : new nu3(obj);
    }

    @DexIgnore
    public JsonElement a(String str) {
        return this.a.get(str);
    }
}
