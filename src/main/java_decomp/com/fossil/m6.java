package com.fossil;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobServiceEngine;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m6 extends Service {
    @DexIgnore
    public static /* final */ HashMap<ComponentName, h> g; // = new HashMap<>();
    @DexIgnore
    public b a;
    @DexIgnore
    public h b;
    @DexIgnore
    public a c;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public /* final */ ArrayList<d> f;

    @DexIgnore
    public interface b {
        @DexIgnore
        IBinder a();

        @DexIgnore
        e b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends h {
        @DexIgnore
        public /* final */ PowerManager.WakeLock d;
        @DexIgnore
        public /* final */ PowerManager.WakeLock e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public c(Context context, ComponentName componentName) {
            super(componentName);
            context.getApplicationContext();
            PowerManager powerManager = (PowerManager) context.getSystemService("power");
            this.d = powerManager.newWakeLock(1, componentName.getClassName() + ":launch");
            this.d.setReferenceCounted(false);
            this.e = powerManager.newWakeLock(1, componentName.getClassName() + ":run");
            this.e.setReferenceCounted(false);
        }

        @DexIgnore
        public void a() {
            synchronized (this) {
                if (this.g) {
                    if (this.f) {
                        this.d.acquire(60000);
                    }
                    this.g = false;
                    this.e.release();
                }
            }
        }

        @DexIgnore
        public void b() {
            synchronized (this) {
                if (!this.g) {
                    this.g = true;
                    this.e.acquire(600000);
                    this.d.release();
                }
            }
        }

        @DexIgnore
        public void c() {
            synchronized (this) {
                this.f = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements e {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public d(Intent intent, int i) {
            this.a = intent;
            this.b = i;
        }

        @DexIgnore
        public void a() {
            m6.this.stopSelf(this.b);
        }

        @DexIgnore
        public Intent getIntent() {
            return this.a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a();

        @DexIgnore
        Intent getIntent();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends JobServiceEngine implements b {
        @DexIgnore
        public /* final */ m6 a;
        @DexIgnore
        public /* final */ Object b; // = new Object();
        @DexIgnore
        public JobParameters c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a implements e {
            @DexIgnore
            public /* final */ JobWorkItem a;

            @DexIgnore
            public a(JobWorkItem jobWorkItem) {
                this.a = jobWorkItem;
            }

            @DexIgnore
            public void a() {
                synchronized (f.this.b) {
                    if (f.this.c != null) {
                        f.this.c.completeWork(this.a);
                    }
                }
            }

            @DexIgnore
            public Intent getIntent() {
                return this.a.getIntent();
            }
        }

        @DexIgnore
        public f(m6 m6Var) {
            super(m6Var);
            this.a = m6Var;
        }

        @DexIgnore
        public IBinder a() {
            return getBinder();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
            r1.getIntent().setExtrasClassLoader(r3.a.getClassLoader());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            return new com.fossil.m6.f.a(r3, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r1 == null) goto L_0x0026;
         */
        @DexIgnore
        public e b() {
            synchronized (this.b) {
                if (this.c == null) {
                    return null;
                }
                JobWorkItem dequeueWork = this.c.dequeueWork();
            }
        }

        @DexIgnore
        public boolean onStartJob(JobParameters jobParameters) {
            this.c = jobParameters;
            this.a.a(false);
            return true;
        }

        @DexIgnore
        public boolean onStopJob(JobParameters jobParameters) {
            boolean b2 = this.a.b();
            synchronized (this.b) {
                this.c = null;
            }
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends h {
        @DexIgnore
        public g(Context context, ComponentName componentName, int i) {
            super(componentName);
            a(i);
            new JobInfo.Builder(i, this.a).setOverrideDeadline(0).build();
            JobScheduler jobScheduler = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h {
        @DexIgnore
        public /* final */ ComponentName a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public int c;

        @DexIgnore
        public h(ComponentName componentName) {
            this.a = componentName;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i) {
            if (!this.b) {
                this.b = true;
                this.c = i;
            } else if (this.c != i) {
                throw new IllegalArgumentException("Given job ID " + i + " is different than previous " + this.c);
            }
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void c() {
        }
    }

    @DexIgnore
    public m6() {
        if (Build.VERSION.SDK_INT >= 26) {
            this.f = null;
        } else {
            this.f = new ArrayList<>();
        }
    }

    @DexIgnore
    public static h a(Context context, ComponentName componentName, boolean z, int i) {
        h hVar;
        h hVar2 = g.get(componentName);
        if (hVar2 != null) {
            return hVar2;
        }
        if (Build.VERSION.SDK_INT < 26) {
            hVar = new c(context, componentName);
        } else if (z) {
            hVar = new g(context, componentName, i);
        } else {
            throw new IllegalArgumentException("Can't be here without a job id");
        }
        h hVar3 = hVar;
        g.put(componentName, hVar3);
        return hVar3;
    }

    @DexIgnore
    public abstract void a(Intent intent);

    @DexIgnore
    public boolean b() {
        a aVar = this.c;
        if (aVar != null) {
            aVar.cancel(this.d);
        }
        return c();
    }

    @DexIgnore
    public boolean c() {
        return true;
    }

    @DexIgnore
    public void d() {
        ArrayList<d> arrayList = this.f;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.c = null;
                if (this.f != null && this.f.size() > 0) {
                    a(false);
                } else if (!this.e) {
                    this.b.a();
                }
            }
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        b bVar = this.a;
        if (bVar != null) {
            return bVar.a();
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.a = new f(this);
            this.b = null;
            return;
        }
        this.a = null;
        this.b = a(this, new ComponentName(this, m6.class), false, 0);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        ArrayList<d> arrayList = this.f;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.e = true;
                this.b.a();
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        if (this.f == null) {
            return 2;
        }
        this.b.c();
        synchronized (this.f) {
            ArrayList<d> arrayList = this.f;
            if (intent == null) {
                intent = new Intent();
            }
            arrayList.add(new d(intent, i2));
            a(true);
        }
        return 3;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            while (true) {
                e a2 = m6.this.a();
                if (a2 == null) {
                    return null;
                }
                m6.this.a(a2.getIntent());
                a2.a();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void onPostExecute(Void voidR) {
            m6.this.d();
        }

        @DexIgnore
        /* renamed from: a */
        public void onCancelled(Void voidR) {
            m6.this.d();
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.c == null) {
            this.c = new a();
            h hVar = this.b;
            if (hVar != null && z) {
                hVar.b();
            }
            this.c.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    @DexIgnore
    public e a() {
        b bVar = this.a;
        if (bVar != null) {
            return bVar.b();
        }
        synchronized (this.f) {
            if (this.f.size() <= 0) {
                return null;
            }
            e remove = this.f.remove(0);
            return remove;
        }
    }
}
