package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ku {

    @DexIgnore
    public interface a {
        @DexIgnore
        ku build();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(File file);
    }

    @DexIgnore
    File a(vr vrVar);

    @DexIgnore
    void a(vr vrVar, b bVar);
}
