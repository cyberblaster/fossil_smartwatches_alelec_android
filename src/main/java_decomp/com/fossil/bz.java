package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bz {
    @DexIgnore
    public /* final */ List<ImageHeaderParser> a; // = new ArrayList();

    @DexIgnore
    public synchronized List<ImageHeaderParser> a() {
        return this.a;
    }

    @DexIgnore
    public synchronized void a(ImageHeaderParser imageHeaderParser) {
        this.a.add(imageHeaderParser);
    }
}
