package com.fossil;

import com.google.android.gms.maps.model.LatLng;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i85$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LatLng $it;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ i85$b$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(i85$b$a i85_b_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = i85_b_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.this$0.a.h.o(this.this$0.this$0.a.g);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i85$b$a(LatLng latLng, xe6 xe6, WeatherSettingPresenter.b bVar) {
        super(2, xe6);
        this.$it = latLng;
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        i85$b$a i85_b_a = new i85$b$a(this.$it, xe6, this.this$0);
        i85_b_a.p$ = (il6) obj;
        return i85_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((i85$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            String a3 = gy5.a(this.this$0.b);
            List b = this.this$0.a.g;
            String str = this.this$0.c;
            LatLng latLng = this.$it;
            double d = latLng.a;
            double d2 = latLng.b;
            wg6.a((Object) a3, "name");
            WeatherLocationWrapper weatherLocationWrapper = r5;
            WeatherLocationWrapper weatherLocationWrapper2 = new WeatherLocationWrapper(str, d, d2, a3, this.this$0.b, false, true, 32, (qg6) null);
            b.add(weatherLocationWrapper);
            cn6 c = zl6.c();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = a3;
            this.label = 1;
            if (gk6.a(c, aVar, this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            String str2 = (String) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
