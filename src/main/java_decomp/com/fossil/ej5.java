package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.chart.HeartRateSleepSessionChart;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ej5 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public ArrayList<a> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ArrayList<iz5> a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ short c;
        @DexIgnore
        public /* final */ short d;

        @DexIgnore
        public a() {
            this((ArrayList) null, 0, 0, 0, 15, (qg6) null);
        }

        @DexIgnore
        public a(ArrayList<iz5> arrayList, int i, short s, short s2) {
            wg6.b(arrayList, "heartRateSessionData");
            this.a = arrayList;
            this.b = i;
            this.c = s;
            this.d = s2;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final ArrayList<iz5> b() {
            return this.a;
        }

        @DexIgnore
        public final short c() {
            return this.d;
        }

        @DexIgnore
        public final short d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return wg6.a((Object) this.a, (Object) aVar.a) && this.b == aVar.b && this.c == aVar.c && this.d == aVar.d;
        }

        @DexIgnore
        public int hashCode() {
            ArrayList<iz5> arrayList = this.a;
            return ((((((arrayList != null ? arrayList.hashCode() : 0) * 31) + d.a(this.b)) * 31) + f.a(this.c)) * 31) + f.a(this.d);
        }

        @DexIgnore
        public String toString() {
            return "SleepHeartRateUIData(heartRateSessionData=" + this.a + ", duration=" + this.b + ", minHR=" + this.c + ", maxHR=" + this.d + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(ArrayList arrayList, int i, short s, short s2, int i2, qg6 qg6) {
            this((i2 & 1) != 0 ? new ArrayList() : arrayList, (i2 & 2) != 0 ? 1 : i, (i2 & 4) != 0 ? 0 : s, (i2 & 8) != 0 ? 0 : s2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ HeartRateSleepSessionChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(View view) {
            super(view);
            wg6.b(view, "itemView");
            View findViewById = view.findViewById(2131362480);
            wg6.a((Object) findViewById, "itemView.findViewById(R.id.hrssc)");
            this.a = (HeartRateSleepSessionChart) findViewById;
        }

        @DexIgnore
        public final HeartRateSleepSessionChart a() {
            return this.a;
        }
    }

    @DexIgnore
    public ej5(ArrayList<a> arrayList) {
        wg6.b(arrayList, "data");
        this.d = arrayList;
        String b2 = ThemeManager.l.a().b("awakeSleep");
        this.a = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("lightSleep");
        this.b = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("deepSleep");
        this.c = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        a aVar = this.d.get(i);
        wg6.a((Object) aVar, "data[position]");
        a aVar2 = aVar;
        bVar.a().setMDuration(aVar2.a());
        bVar.a().setMMinHRValue(aVar2.d());
        bVar.a().setMMaxHRValue(aVar2.c());
        bVar.a().a(this.c, this.b, this.a);
        bVar.a().a(aVar2.b());
    }

    @DexIgnore
    public int getItemCount() {
        return this.d.size();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558667, viewGroup, false);
        wg6.a((Object) inflate, "view");
        return new b(inflate);
    }

    @DexIgnore
    public final void a(ArrayList<a> arrayList) {
        wg6.b(arrayList, "data");
        this.d = arrayList;
        notifyDataSetChanged();
    }
}
