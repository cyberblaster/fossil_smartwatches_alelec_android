package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u50 extends x50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public u50 createFromParcel(Parcel parcel) {
            return new u50(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new u50[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m60createFromParcel(Parcel parcel) {
            return new u50(parcel, (qg6) null);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ u50(jc0 jc0, int i, qg6 qg6) {
        this((i & 1) != 0 ? new jc0(false) : jc0);
    }

    @DexIgnore
    public final jc0 getDataConfig() {
        ic0 ic0 = this.b;
        if (ic0 != null) {
            return (jc0) ic0;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.PercentageCircleComplicationDataConfig");
    }

    @DexIgnore
    public u50(jc0 jc0) {
        super(z50.BATTERY, jc0, (lc0) null, (mc0) null, 12);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ u50(lc0 lc0, mc0 mc0, jc0 jc0, int i, qg6 qg6) {
        this(lc0, (i & 2) != 0 ? new mc0(mc0.CREATOR.a()) : mc0, (i & 4) != 0 ? new jc0(false) : jc0);
    }

    @DexIgnore
    public u50(lc0 lc0, mc0 mc0, jc0 jc0) {
        super(z50.BATTERY, jc0, lc0, mc0);
    }

    @DexIgnore
    public /* synthetic */ u50(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
