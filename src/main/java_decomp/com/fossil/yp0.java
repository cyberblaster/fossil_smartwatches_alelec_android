package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp0 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.AUTHENTICATION}));
    @DexIgnore
    public /* final */ long C;

    @DexIgnore
    public yp0(ue1 ue1, q41 q41, long j) {
        super(ue1, q41, eh1.CONFIRM_AUTHORIZATION, (String) null, 8);
        this.C = j;
    }

    @DexIgnore
    public Object d() {
        return cd6.a;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        long j = this.C;
        if (j >= 0) {
            vg6 vg6 = vg6.a;
            if (j <= 4294967295L) {
                if1.a((if1) this, (qv0) new nh1(this.w, j), (hg6) new vn1(this), (hg6) new sf0(this), (ig6) null, (hg6) jh0.a, (hg6) bj0.a, 8, (Object) null);
                return;
            }
        }
        a(sk1.INVALID_PARAMETER);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.TIMEOUT_IN_MS, (Object) Long.valueOf(this.C));
    }

    @DexIgnore
    public boolean a(qv0 qv0) {
        return qv0 instanceof pe0;
    }
}
