package com.fossil;

import com.fossil.rv1;
import com.fossil.rv1.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw1<O extends rv1.d> {
    @DexIgnore
    public /* final */ boolean a; // = true;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ rv1<O> c;
    @DexIgnore
    public /* final */ O d;

    @DexIgnore
    public lw1(rv1<O> rv1, O o) {
        this.c = rv1;
        this.d = o;
        this.b = u12.a(this.c, this.d);
    }

    @DexIgnore
    public static <O extends rv1.d> lw1<O> a(rv1<O> rv1, O o) {
        return new lw1<>(rv1, o);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof lw1)) {
            return false;
        }
        lw1 lw1 = (lw1) obj;
        return !this.a && !lw1.a && u12.a(this.c, lw1.c) && u12.a(this.d, lw1.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b;
    }

    @DexIgnore
    public static <O extends rv1.d> lw1<O> a(rv1<O> rv1) {
        return new lw1<>(rv1);
    }

    @DexIgnore
    public final String a() {
        return this.c.b();
    }

    @DexIgnore
    public lw1(rv1<O> rv1) {
        this.c = rv1;
        this.d = null;
        this.b = System.identityHashCode(this);
    }
}
