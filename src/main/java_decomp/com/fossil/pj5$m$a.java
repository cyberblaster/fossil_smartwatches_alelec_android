package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$updateUser$1$onSuccess$1", f = "HomeProfilePresenter.kt", l = {297}, m = "invokeSuspend")
public final class pj5$m$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter.m this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$updateUser$1$onSuccess$1$currentUser$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pj5$m$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(pj5$m$a pj5_m_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = pj5_m_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return this.this$0.this$0.a.x.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pj5$m$a(HomeProfilePresenter.m mVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = mVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pj5$m$a pj5_m_a = new pj5$m$a(this.this$0, xe6);
        pj5_m_a.p$ = (il6) obj;
        return pj5_m_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pj5$m$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 b = this.this$0.a.c();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(b, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            this.this$0.a.m().a(mFUser);
        }
        return cd6.a;
    }
}
