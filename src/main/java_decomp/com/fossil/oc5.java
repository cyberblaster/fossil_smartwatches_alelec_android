package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc5 implements Factory<vc5> {
    @DexIgnore
    public static vc5 a(lc5 lc5) {
        vc5 c = lc5.c();
        z76.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
