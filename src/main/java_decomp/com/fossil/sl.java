package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sl {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends sl {
        @DexIgnore
        public rl a(String str) {
            return null;
        }
    }

    @DexIgnore
    public static sl a() {
        return new a();
    }

    @DexIgnore
    public abstract rl a(String str);

    @DexIgnore
    public final rl b(String str) {
        rl a2 = a(str);
        return a2 == null ? rl.a(str) : a2;
    }
}
