package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yb3 extends rv1.a<mb3, lb3> {
    @DexIgnore
    public final /* synthetic */ rv1.f a(Context context, Looper looper, e12 e12, Object obj, wv1.b bVar, wv1.c cVar) {
        lb3 lb3 = (lb3) obj;
        if (lb3 == null) {
            lb3 = lb3.j;
        }
        return new mb3(context, looper, true, e12, lb3, bVar, cVar);
    }
}
