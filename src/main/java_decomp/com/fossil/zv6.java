package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zv6 extends Serializable {
    @DexIgnore
    public static final String ANY_MARKER = "*";
    @DexIgnore
    public static final String ANY_NON_NULL_MARKER = "+";

    @DexIgnore
    void add(zv6 zv6);

    @DexIgnore
    boolean contains(zv6 zv6);

    @DexIgnore
    boolean contains(String str);

    @DexIgnore
    boolean equals(Object obj);

    @DexIgnore
    String getName();

    @DexIgnore
    boolean hasChildren();

    @DexIgnore
    boolean hasReferences();

    @DexIgnore
    int hashCode();

    @DexIgnore
    Iterator<zv6> iterator();

    @DexIgnore
    boolean remove(zv6 zv6);
}
