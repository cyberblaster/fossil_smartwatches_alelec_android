package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c01 extends ok0 {
    @DexIgnore
    public UUID[] j; // = new UUID[0];
    @DexIgnore
    public rg1[] k; // = new rg1[0];

    @DexIgnore
    public c01(at0 at0) {
        super(hm0.DISCOVER_SERVICE, at0);
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.c();
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return p51 instanceof pw0;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.g;
    }

    @DexIgnore
    public void c(p51 p51) {
        pw0 pw0 = (pw0) p51;
        this.j = pw0.b;
        this.k = pw0.c;
    }
}
