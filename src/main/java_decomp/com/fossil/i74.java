package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i74 extends h74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C; // = new SparseIntArray();
    @DexIgnore
    public long A;

    /*
    static {
        C.put(2131362482, 1);
        C.put(2131361960, 2);
        C.put(2131362235, 3);
        C.put(2131363278, 4);
        C.put(2131361902, 5);
        C.put(2131362645, 6);
        C.put(2131362097, 7);
        C.put(2131362656, 8);
        C.put(2131362615, 9);
        C.put(2131362653, 10);
        C.put(2131362291, 11);
        C.put(2131361916, 12);
        C.put(2131362670, 13);
        C.put(2131362902, 14);
    }
    */

    @DexIgnore
    public i74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 15, B, C));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.A != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.A = 1;
        }
        g();
    }

    @DexIgnore
    public i74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[12], objArr[2], objArr[7], objArr[3], objArr[11], objArr[1], objArr[9], objArr[6], objArr[10], objArr[8], objArr[13], objArr[0], objArr[14], objArr[4]);
        this.A = -1;
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
