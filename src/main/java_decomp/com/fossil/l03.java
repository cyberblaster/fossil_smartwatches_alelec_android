package com.fossil;

import android.content.Context;
import com.facebook.UserSettingsManager;
import com.facebook.internal.Utility;
import com.facebook.login.LoginStatusClient;
import com.facebook.stetho.server.http.HttpStatus;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l03 {
    @DexIgnore
    public static m43<Long> A; // = a("measurement.upload.debug_upload_interval", 1000L, 1000L, f13.a);
    @DexIgnore
    public static m43<Boolean> A0; // = a("measurement.service.audience.use_bundle_timestamp_for_property_filters", false, false, l33.a);
    @DexIgnore
    public static m43<Long> B; // = a("measurement.upload.minimum_delay", 500L, 500L, e13.a);
    @DexIgnore
    public static m43<Boolean> B0; // = a("measurement.service.audience.not_prepend_timestamps_for_sequence_session_scoped_filters", false, false, k33.a);
    @DexIgnore
    public static m43<Long> C; // = a("measurement.alarm_manager.minimum_interval", 60000L, 60000L, h13.a);
    @DexIgnore
    public static m43<Boolean> C0; // = a("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true, true, m33.a);
    @DexIgnore
    public static m43<Long> D; // = a("measurement.upload.stale_data_deletion_interval", 86400000L, 86400000L, g13.a);
    @DexIgnore
    public static m43<Boolean> D0; // = a("measurement.app_launch.event_ordering_fix", false, false, p33.a);
    @DexIgnore
    public static m43<Long> E;
    @DexIgnore
    public static m43<Boolean> E0; // = a("measurement.sdk.collection.last_deep_link_referrer", false, false, o33.a);
    @DexIgnore
    public static m43<Long> F; // = a("measurement.upload.initial_upload_delay_time", 15000L, 15000L, i13.a);
    @DexIgnore
    public static m43<Boolean> F0; // = a("measurement.sdk.collection.last_deep_link_referrer_campaign", false, false, q33.a);
    @DexIgnore
    public static m43<Long> G; // = a("measurement.upload.retry_time", 1800000L, 1800000L, l13.a);
    @DexIgnore
    public static m43<Boolean> G0; // = a("measurement.sdk.collection.last_gclid_from_referrer", false, false, t33.a);
    @DexIgnore
    public static m43<Integer> H; // = a("measurement.upload.retry_count", 6, 6, n13.a);
    @DexIgnore
    public static m43<Boolean> H0; // = a("measurement.sdk.collection.worker_thread_referrer", true, true, s33.a);
    @DexIgnore
    public static m43<Long> I; // = a("measurement.upload.max_queue_time", 2419200000L, 2419200000L, m13.a);
    @DexIgnore
    public static m43<Boolean> I0; // = a("measurement.upload.file_lock_state_check", false, false, v33.a);
    @DexIgnore
    public static m43<Integer> J; // = a("measurement.lifetimevalue.max_currency_tracked", 4, 4, p13.a);
    @DexIgnore
    public static m43<Boolean> J0; // = a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true, true, u33.a);
    @DexIgnore
    public static m43<Integer> K; // = a("measurement.audience.filter_result_max_count", 200, 200, o13.a);
    @DexIgnore
    public static m43<Boolean> K0; // = a("measurement.ga.ga_app_id", false, Boolean.valueOf(bt2.a()), x33.a);
    @DexIgnore
    public static m43<Long> L;
    @DexIgnore
    public static m43<Boolean> L0; // = a("measurement.lifecycle.app_backgrounded_tracking", false, false, w33.a);
    @DexIgnore
    public static m43<Boolean> M; // = a("measurement.test.boolean_flag", false, false, q13.a);
    @DexIgnore
    public static m43<Boolean> M0; // = a("measurement.lifecycle.app_in_background_parameter", false, false, a43.a);
    @DexIgnore
    public static m43<String> N; // = a("measurement.test.string_flag", "---", "---", t13.a);
    @DexIgnore
    public static m43<Boolean> N0; // = a("measurement.integration.disable_firebase_instance_id", false, false, y33.a);
    @DexIgnore
    public static m43<Long> O; // = a("measurement.test.long_flag", -1L, -1L, s13.a);
    @DexIgnore
    public static m43<Boolean> O0; // = a("measurement.lifecycle.app_backgrounded_engagement", false, false, c43.a);
    @DexIgnore
    public static m43<Integer> P; // = a("measurement.test.int_flag", -2, -2, v13.a);
    @DexIgnore
    public static m43<Boolean> P0; // = a("measurement.service.fix_gmp_version", false, false, e43.a);
    @DexIgnore
    public static m43<Double> Q;
    @DexIgnore
    public static m43<Boolean> Q0; // = a("measurement.collection.service.update_with_analytics_fix", false, false, d43.a);
    @DexIgnore
    public static m43<Integer> R; // = a("measurement.experiment.max_ids", 50, 50, w13.a);
    @DexIgnore
    public static m43<Boolean> R0; // = a("measurement.service.disable_install_state_reporting", false, false, g43.a);
    @DexIgnore
    public static m43<Boolean> S; // = a("measurement.validation.internal_limits_internal_event_params", false, false, a23.a);
    @DexIgnore
    public static m43<Boolean> S0; // = a("measurement.service.use_appinfo_modified", false, false, f43.a);
    @DexIgnore
    public static m43<Boolean> T; // = a("measurement.reset_analytics.persist_time", true, true, z13.a);
    @DexIgnore
    public static m43<Boolean> T0; // = a("measurement.client.firebase_feature_rollout.v1.enable", true, true, h43.a);
    @DexIgnore
    public static m43<Boolean> U; // = a("measurement.sampling.time_zone_offset_enabled", false, false, c23.a);
    @DexIgnore
    public static m43<Boolean> V; // = a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false, false, b23.a);
    @DexIgnore
    public static m43<Boolean> W; // = a("measurement.client.sessions.session_id_enabled", true, true, e23.a);
    @DexIgnore
    public static m43<Boolean> X; // = a("measurement.service.sessions.session_number_enabled", true, true, d23.a);
    @DexIgnore
    public static m43<Boolean> Y; // = a("measurement.client.sessions.immediate_start_enabled_foreground", true, true, g23.a);
    @DexIgnore
    public static m43<Boolean> Z; // = a("measurement.client.sessions.background_sessions_enabled", true, true, f23.a);
    @DexIgnore
    public static bb3 a;
    @DexIgnore
    public static m43<Boolean> a0; // = a("measurement.client.sessions.remove_expired_session_properties_enabled", true, true, i23.a);
    @DexIgnore
    public static List<m43<?>> b; // = Collections.synchronizedList(new ArrayList());
    @DexIgnore
    public static m43<Boolean> b0; // = a("measurement.service.sessions.session_number_backfill_enabled", true, true, k23.a);
    @DexIgnore
    public static volatile x53 c;
    @DexIgnore
    public static m43<Boolean> c0; // = a("measurement.service.sessions.remove_disabled_session_number", true, true, j23.a);
    @DexIgnore
    public static Boolean d;
    @DexIgnore
    public static m43<Boolean> d0; // = a("measurement.client.sessions.start_session_before_view_screen", true, true, m23.a);
    @DexIgnore
    public static m43<Boolean> e; // = a("measurement.upload_dsid_enabled", false, false, n03.a);
    @DexIgnore
    public static m43<Boolean> e0; // = a("measurement.client.sessions.check_on_startup", true, true, l23.a);
    @DexIgnore
    public static m43<String> f; // = a("measurement.log_tag", "FA", "FA-SVC", b13.a);
    @DexIgnore
    public static m43<Boolean> f0; // = a("measurement.collection.firebase_global_collection_flag_enabled", true, true, o23.a);
    @DexIgnore
    public static m43<Long> g; // = a("measurement.ad_id_cache_time", 10000L, 10000L, k13.a);
    @DexIgnore
    public static m43<Boolean> g0; // = a("measurement.collection.efficient_engagement_reporting_enabled", false, false, n23.a);
    @DexIgnore
    public static m43<Long> h; // = a("measurement.monitoring.sample_period_millis", 86400000L, 86400000L, y13.a);
    @DexIgnore
    public static m43<Boolean> h0; // = a("measurement.collection.redundant_engagement_removal_enabled", false, false, q23.a);
    @DexIgnore
    public static m43<Long> i; // = a("measurement.config.cache_time", 86400000L, 3600000L, h23.a);
    @DexIgnore
    public static m43<Boolean> i0; // = a("measurement.personalized_ads_signals_collection_enabled", true, true, p23.a);
    @DexIgnore
    public static m43<String> j; // = a("measurement.config.url_scheme", Utility.URL_SCHEME, Utility.URL_SCHEME, u23.a);
    @DexIgnore
    public static m43<Boolean> j0; // = a("measurement.personalized_ads_property_translation_enabled", true, true, s23.a);
    @DexIgnore
    public static m43<String> k; // = a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com", e33.a);
    @DexIgnore
    public static m43<Boolean> k0; // = a("measurement.collection.init_params_control_enabled", true, true, r23.a);
    @DexIgnore
    public static m43<Integer> l; // = a("measurement.upload.max_bundles", 100, 100, r33.a);
    @DexIgnore
    public static m43<Boolean> l0; // = a("measurement.upload.disable_is_uploader", true, true, t23.a);
    @DexIgnore
    public static m43<Integer> m; // = a("measurement.upload.max_batch_size", 65536, 65536, b43.a);
    @DexIgnore
    public static m43<Boolean> m0; // = a("measurement.experiment.enable_experiment_reporting", true, true, w23.a);
    @DexIgnore
    public static m43<Integer> n; // = a("measurement.upload.max_bundle_size", 65536, 65536, q03.a);
    @DexIgnore
    public static m43<Boolean> n0; // = a("measurement.collection.log_event_and_bundle_v2", true, true, v23.a);
    @DexIgnore
    public static m43<Integer> o; // = a("measurement.upload.max_events_per_bundle", 1000, 1000, p03.a);
    @DexIgnore
    public static m43<Boolean> o0; // = a("measurement.quality.checksum", false, false, (j43) null);
    @DexIgnore
    public static m43<Integer> p; // = a("measurement.upload.max_events_per_day", 100000, 100000, s03.a);
    @DexIgnore
    public static m43<Boolean> p0; // = a("measurement.module.collection.conditionally_omit_admob_app_id", true, true, z23.a);
    @DexIgnore
    public static m43<Integer> q; // = a("measurement.upload.max_error_events_per_day", 1000, 1000, r03.a);
    @DexIgnore
    public static m43<Boolean> q0; // = a("measurement.sdk.dynamite.use_dynamite2", false, false, x23.a);
    @DexIgnore
    public static m43<Integer> r; // = a("measurement.upload.max_public_events_per_day", 50000, 50000, u03.a);
    @DexIgnore
    public static m43<Boolean> r0; // = a("measurement.sdk.dynamite.allow_remote_dynamite", false, false, b33.a);
    @DexIgnore
    public static m43<Integer> s;
    @DexIgnore
    public static m43<Boolean> s0; // = a("measurement.sdk.collection.validate_param_names_alphabetical", false, false, a33.a);
    @DexIgnore
    public static m43<Integer> t; // = a("measurement.upload.max_realtime_events_per_day", 10, 10, x03.a);
    @DexIgnore
    public static m43<Boolean> t0; // = a("measurement.collection.event_safelist", true, true, d33.a);
    @DexIgnore
    public static m43<Integer> u; // = a("measurement.store.max_stored_events_per_app", 100000, 100000, v03.a);
    @DexIgnore
    public static m43<Boolean> u0; // = a("measurement.service.audience.scoped_filters_v27", false, false, c33.a);
    @DexIgnore
    public static m43<String> v; // = a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a", z03.a);
    @DexIgnore
    public static m43<Boolean> v0; // = a("measurement.service.audience.session_scoped_event_aggregates", false, false, f33.a);
    @DexIgnore
    public static m43<Long> w; // = a("measurement.upload.backoff_period", 43200000L, 43200000L, y03.a);
    @DexIgnore
    public static m43<Boolean> w0; // = a("measurement.service.audience.session_scoped_user_engagement", false, false, h33.a);
    @DexIgnore
    public static m43<Long> x; // = a("measurement.upload.window_interval", 3600000L, 3600000L, a13.a);
    @DexIgnore
    public static m43<Boolean> x0; // = a("measurement.service.audience.scoped_engagement_removal_when_session_expired", true, true, g33.a);
    @DexIgnore
    public static m43<Long> y; // = a("measurement.upload.interval", 3600000L, 3600000L, d13.a);
    @DexIgnore
    public static m43<Boolean> y0; // = a("measurement.client.audience.scoped_engagement_removal_when_session_expired", true, true, j33.a);
    @DexIgnore
    public static m43<Long> z; // = a("measurement.upload.realtime_upload_interval", 10000L, 10000L, c13.a);
    @DexIgnore
    public static m43<Boolean> z0; // = a("measurement.service.audience.remove_disabled_session_scoped_user_engagement", false, false, i33.a);

    /*
    static {
        Collections.synchronizedSet(new HashSet());
        a("measurement.log_androidId_enabled", false, false, o03.a);
        Integer valueOf = Integer.valueOf(HttpStatus.HTTP_INTERNAL_SERVER_ERROR);
        s = a("measurement.upload.max_conversions_per_day", valueOf, valueOf, t03.a);
        Long valueOf2 = Long.valueOf(UserSettingsManager.TIMEOUT_7D);
        E = a("measurement.upload.refresh_blacklisted_config_interval", valueOf2, valueOf2, j13.a);
        Long valueOf3 = Long.valueOf(LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
        L = a("measurement.service_client.idle_disconnect_millis", valueOf3, valueOf3, r13.a);
        Double valueOf4 = Double.valueOf(-3.0d);
        Q = a("measurement.test.double_flag", valueOf4, valueOf4, u13.a);
        a("measurement.service.audience.invalidate_config_cache_after_app_unisntall", false, false, n33.a);
        a("measurement.upload.dsid_reflection_failure_logging", true, true, i43.a);
    }
    */

    @DexIgnore
    public static Map<String, String> a(Context context) {
        ak2 a2 = ak2.a(context.getContentResolver(), qk2.a("com.google.android.gms.measurement"));
        return a2 == null ? Collections.emptyMap() : a2.a();
    }

    @DexIgnore
    public static final /* synthetic */ Long p0() {
        long j2;
        if (w0()) {
            j2 = kr2.A();
        } else {
            j2 = kr2.m();
        }
        return Long.valueOf(j2);
    }

    @DexIgnore
    public static final /* synthetic */ String s0() {
        return w0() ? kr2.C() : kr2.n();
    }

    @DexIgnore
    public static boolean w0() {
        if (a != null) {
        }
        return false;
    }

    @DexIgnore
    public static void a(x53 x53) {
        c = x53;
    }

    @DexIgnore
    public static void a(Exception exc) {
        if (c != null) {
            Context c2 = c.c();
            if (d == null) {
                d = Boolean.valueOf(kv1.a().a(c2, (int) nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE) == 0);
            }
            if (d.booleanValue()) {
                c.b().t().a("Got Exception on PhenotypeFlag.get on Play device", exc);
            }
        }
    }

    @DexIgnore
    public static <V> m43<V> a(String str, V v2, V v3, j43<V> j43) {
        m43 m43 = new m43(str, v2, v3, j43);
        b.add(m43);
        return m43;
    }

    @DexIgnore
    public static void a(bb3 bb3) {
        a = bb3;
    }
}
