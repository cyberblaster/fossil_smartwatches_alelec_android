package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n05 implements Factory<m05> {
    @DexIgnore
    public static NotificationWatchRemindersPresenter a(i05 i05, an4 an4, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new NotificationWatchRemindersPresenter(i05, an4, remindersSettingsDatabase);
    }
}
