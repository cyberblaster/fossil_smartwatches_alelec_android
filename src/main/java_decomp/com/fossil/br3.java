package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br3 extends Exception {
    @DexIgnore
    public /* final */ int zza;

    @DexIgnore
    public br3(int i, String str) {
        super(str);
        this.zza = i;
    }

    @DexIgnore
    public final int zza() {
        return this.zza;
    }
}
