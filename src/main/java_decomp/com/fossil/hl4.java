package com.fossil;

import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public Map<String, String> c; // = new HashMap();
    @DexIgnore
    public AnalyticsHelper d;

    @DexIgnore
    public hl4(AnalyticsHelper analyticsHelper, String str) {
        wg6.b(analyticsHelper, "analyticsHelper");
        wg6.b(str, "eventName");
        this.d = analyticsHelper;
        this.a = str;
    }

    @DexIgnore
    public final hl4 a(String str, String str2) {
        wg6.b(str, "paramName");
        wg6.b(str2, "paramValue");
        Map<String, String> map = this.c;
        if (map != null) {
            map.put(str, str2);
            return this;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a() {
        String str;
        Map<String, String> map = this.c;
        if (map == null) {
            wg6.a();
            throw null;
        } else if (!map.isEmpty() || (str = this.b) == null) {
            AnalyticsHelper analyticsHelper = this.d;
            if (analyticsHelper != null) {
                analyticsHelper.a(this.a, (Map<String, ? extends Object>) this.c);
            } else {
                wg6.a();
                throw null;
            }
        } else if (str != null) {
            AnalyticsHelper analyticsHelper2 = this.d;
            if (analyticsHelper2 != null) {
                String str2 = this.a;
                if (str != null) {
                    analyticsHelper2.a(str2, str);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            AnalyticsHelper analyticsHelper3 = this.d;
            if (analyticsHelper3 != null) {
                analyticsHelper3.a(this.a);
            } else {
                wg6.a();
                throw null;
            }
        }
    }
}
