package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai1 implements Parcelable.Creator<uj1> {
    @DexIgnore
    public /* synthetic */ ai1(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            ck0 valueOf = ck0.valueOf(readString);
            parcel.setDataPosition(0);
            switch (fg1.a[valueOf.ordinal()]) {
                case 1:
                    return dw0.CREATOR.createFromParcel(parcel);
                case 2:
                    return rz0.CREATOR.createFromParcel(parcel);
                case 3:
                    return cr0.CREATOR.createFromParcel(parcel);
                case 4:
                    return tg0.CREATOR.createFromParcel(parcel);
                case 5:
                    return a71.CREATOR.createFromParcel(parcel);
                case 6:
                    return fn1.CREATOR.createFromParcel(parcel);
                case 7:
                    return ek0.CREATOR.createFromParcel(parcel);
                case 8:
                    return ta1.CREATOR.createFromParcel(parcel);
                case 9:
                    return h31.CREATOR.createFromParcel(parcel);
                case 10:
                    return nu0.CREATOR.createFromParcel(parcel);
                case 11:
                    return sn0.CREATOR.createFromParcel(parcel);
                case 12:
                    return je1.CREATOR.createFromParcel(parcel);
                default:
                    throw new kc6();
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new uj1[i];
    }
}
