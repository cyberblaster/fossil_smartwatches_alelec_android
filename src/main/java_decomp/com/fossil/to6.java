package com.fossil;

import com.fossil.mc6;
import com.portfolio.platform.data.model.PinObject;
import java.util.ArrayDeque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to6 {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        Object obj;
        Object obj2;
        try {
            mc6.a aVar = mc6.Companion;
            Class<?> cls = Class.forName("com.fossil.gf6");
            wg6.a((Object) cls, "Class.forName(baseContinuationImplClass)");
            obj = mc6.m1constructorimpl(cls.getCanonicalName());
        } catch (Throwable th) {
            mc6.a aVar2 = mc6.Companion;
            obj = mc6.m1constructorimpl(nc6.a(th));
        }
        if (mc6.m4exceptionOrNullimpl(obj) != null) {
            obj = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        a = (String) obj;
        try {
            mc6.a aVar3 = mc6.Companion;
            Class<?> cls2 = Class.forName("com.fossil.to6");
            wg6.a((Object) cls2, "Class.forName(stackTraceRecoveryClass)");
            obj2 = mc6.m1constructorimpl(cls2.getCanonicalName());
        } catch (Throwable th2) {
            mc6.a aVar4 = mc6.Companion;
            obj2 = mc6.m1constructorimpl(nc6.a(th2));
        }
        if (mc6.m4exceptionOrNullimpl(obj2) != null) {
            obj2 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        String str = (String) obj2;
    }
    */

    @DexIgnore
    public static final <E extends Throwable> E b(E e, kf6 kf6) {
        lc6 a2 = a(e);
        E e2 = (Throwable) a2.component1();
        StackTraceElement[] stackTraceElementArr = (StackTraceElement[]) a2.component2();
        E a3 = fo6.a(e2);
        if (a3 == null) {
            return e;
        }
        ArrayDeque<StackTraceElement> a4 = a(kf6);
        if (a4.isEmpty()) {
            return e;
        }
        if (e2 != e) {
            a(stackTraceElementArr, a4);
        }
        a(e2, a3, a4);
        return a3;
    }

    @DexIgnore
    public static final <E extends Throwable> E a(E e, xe6<?> xe6) {
        wg6.b(e, "exception");
        wg6.b(xe6, "continuation");
        return (!nl6.d() || !(xe6 instanceof kf6)) ? e : b(e, (kf6) xe6);
    }

    @DexIgnore
    public static final <E extends Throwable> E a(E e, E e2, ArrayDeque<StackTraceElement> arrayDeque) {
        arrayDeque.addFirst(a("Coroutine boundary"));
        StackTraceElement[] stackTrace = e.getStackTrace();
        wg6.a((Object) stackTrace, "causeTrace");
        String str = a;
        wg6.a((Object) str, "baseContinuationImplClassName");
        int a2 = a(stackTrace, str);
        int i = 0;
        if (a2 == -1) {
            Object[] array = arrayDeque.toArray(new StackTraceElement[0]);
            if (array != null) {
                e2.setStackTrace((StackTraceElement[]) array);
                return e2;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        StackTraceElement[] stackTraceElementArr = new StackTraceElement[(arrayDeque.size() + a2)];
        for (int i2 = 0; i2 < a2; i2++) {
            stackTraceElementArr[i2] = stackTrace[i2];
        }
        for (StackTraceElement stackTraceElement : arrayDeque) {
            stackTraceElementArr[a2 + i] = stackTraceElement;
            i++;
        }
        e2.setStackTrace(stackTraceElementArr);
        return e2;
    }

    @DexIgnore
    public static final <E extends Throwable> E b(E e) {
        E cause;
        wg6.b(e, "exception");
        if (nl6.d() && (cause = e.getCause()) != null) {
            boolean z = true;
            if (!(!wg6.a((Object) cause.getClass(), (Object) e.getClass()))) {
                StackTraceElement[] stackTrace = e.getStackTrace();
                wg6.a((Object) stackTrace, "exception.stackTrace");
                int length = stackTrace.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = false;
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i];
                    wg6.a((Object) stackTraceElement, "it");
                    if (a(stackTraceElement)) {
                        break;
                    }
                    i++;
                }
                if (z) {
                    return cause;
                }
            }
        }
        return e;
    }

    @DexIgnore
    public static final <E extends Throwable> lc6<E, StackTraceElement[]> a(E e) {
        boolean z;
        Throwable cause = e.getCause();
        if (cause == null || !wg6.a((Object) cause.getClass(), (Object) e.getClass())) {
            return qc6.a(e, new StackTraceElement[0]);
        }
        StackTraceElement[] stackTrace = e.getStackTrace();
        wg6.a((Object) stackTrace, "currentTrace");
        int length = stackTrace.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            }
            StackTraceElement stackTraceElement = stackTrace[i];
            wg6.a((Object) stackTraceElement, "it");
            if (a(stackTraceElement)) {
                z = true;
                break;
            }
            i++;
        }
        if (z) {
            return qc6.a(cause, stackTrace);
        }
        return qc6.a(e, new StackTraceElement[0]);
    }

    @DexIgnore
    public static final ArrayDeque<StackTraceElement> a(kf6 kf6) {
        ArrayDeque<StackTraceElement> arrayDeque = new ArrayDeque<>();
        StackTraceElement stackTraceElement = kf6.getStackTraceElement();
        if (stackTraceElement != null) {
            arrayDeque.add(stackTraceElement);
        }
        while (true) {
            if (!(kf6 instanceof kf6)) {
                kf6 = null;
            }
            if (kf6 == null || (kf6 = kf6.getCallerFrame()) == null) {
                return arrayDeque;
            }
            StackTraceElement stackTraceElement2 = kf6.getStackTraceElement();
            if (stackTraceElement2 != null) {
                arrayDeque.add(stackTraceElement2);
            }
        }
        return arrayDeque;
    }

    @DexIgnore
    public static final StackTraceElement a(String str) {
        wg6.b(str, "message");
        return new StackTraceElement("\b\b\b(" + str, "\b", "\b", -1);
    }

    @DexIgnore
    public static final boolean a(StackTraceElement stackTraceElement) {
        wg6.b(stackTraceElement, "$this$isArtificial");
        String className = stackTraceElement.getClassName();
        wg6.a((Object) className, PinObject.COLUMN_CLASS_NAME);
        return xj6.c(className, "\b\b\b", false, 2, (Object) null);
    }

    @DexIgnore
    public static final boolean a(StackTraceElement stackTraceElement, StackTraceElement stackTraceElement2) {
        return stackTraceElement.getLineNumber() == stackTraceElement2.getLineNumber() && wg6.a((Object) stackTraceElement.getMethodName(), (Object) stackTraceElement2.getMethodName()) && wg6.a((Object) stackTraceElement.getFileName(), (Object) stackTraceElement2.getFileName()) && wg6.a((Object) stackTraceElement.getClassName(), (Object) stackTraceElement2.getClassName());
    }

    @DexIgnore
    public static final void a(StackTraceElement[] stackTraceElementArr, ArrayDeque<StackTraceElement> arrayDeque) {
        int length = stackTraceElementArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (a(stackTraceElementArr[i])) {
                break;
            } else {
                i++;
            }
        }
        int i2 = i + 1;
        int length2 = stackTraceElementArr.length - 1;
        if (length2 >= i2) {
            while (true) {
                StackTraceElement stackTraceElement = stackTraceElementArr[length2];
                StackTraceElement last = arrayDeque.getLast();
                wg6.a((Object) last, "result.last");
                if (a(stackTraceElement, last)) {
                    arrayDeque.removeLast();
                }
                arrayDeque.addFirst(stackTraceElementArr[length2]);
                if (length2 != i2) {
                    length2--;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public static final int a(StackTraceElement[] stackTraceElementArr, String str) {
        int length = stackTraceElementArr.length;
        for (int i = 0; i < length; i++) {
            if (wg6.a((Object) str, (Object) stackTraceElementArr[i].getClassName())) {
                return i;
            }
        }
        return -1;
    }
}
