package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cb6 {
    @DexIgnore
    boolean a(SharedPreferences.Editor editor);

    @DexIgnore
    SharedPreferences.Editor edit();

    @DexIgnore
    SharedPreferences get();
}
