package com.fossil;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa3 extends za3 {
    @DexIgnore
    public wi2 g;
    @DexIgnore
    public /* final */ /* synthetic */ sa3 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wa3(sa3 sa3, String str, int i, wi2 wi2) {
        super(str, i);
        this.h = sa3;
        this.g = wi2;
    }

    @DexIgnore
    public final int a() {
        return this.g.o();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v7, types: [java.lang.Integer] */
    /* JADX WARNING: type inference failed for: r1v16, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x03bb  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x03be  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x03c6 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x03c7  */
    public final boolean a(mj2 mj2, String str, List<oj2> list, long j, f03 f03, boolean z) {
        Boolean bool;
        boolean d = this.h.l().d(this.a, l03.u0);
        if (this.h.l().d(this.a, l03.v0) && d && this.g.y()) {
            j = f03.e;
        }
        Boolean bool2 = null;
        if (this.h.b().a(2)) {
            this.h.b().B().a("Evaluating filter. audience, filter, event", Integer.valueOf(this.b), this.g.n() ? Integer.valueOf(this.g.o()) : null, this.h.i().a(this.g.p()));
            this.h.b().B().a("Filter definition", this.h.n().a(this.g));
        }
        if (!this.g.n() || this.g.o() > 256) {
            v43 w = this.h.b().w();
            Object a = t43.a(this.a);
            if (this.g.n()) {
                bool2 = Integer.valueOf(this.g.o());
            }
            w.a("Invalid event filter ID. appId, id", a, String.valueOf(bool2));
            return true;
        }
        boolean v = this.g.v();
        boolean w2 = this.g.w();
        boolean z2 = v || w2 || (d && this.g.y());
        if (!z || z2) {
            wi2 wi2 = this.g;
            if (wi2.s()) {
                Boolean a2 = za3.a(j, wi2.t());
                if (a2 != null) {
                    if (!a2.booleanValue()) {
                        bool2 = false;
                    }
                }
                this.h.b().B().a("Event filter result", bool2 != null ? "null" : bool2);
                if (bool2 != null) {
                    return false;
                }
                this.c = true;
                if (!bool2.booleanValue()) {
                    return true;
                }
                this.d = true;
                if (z2 && mj2.q()) {
                    if (w2) {
                        this.f = Long.valueOf(mj2.r());
                    } else {
                        this.e = Long.valueOf(mj2.r());
                    }
                }
                return true;
            }
            HashSet hashSet = new HashSet();
            Iterator<xi2> it = wi2.q().iterator();
            while (true) {
                if (!it.hasNext()) {
                    p4 p4Var = new p4();
                    Iterator<oj2> it2 = list.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            Iterator<xi2> it3 = wi2.q().iterator();
                            while (true) {
                                if (!it3.hasNext()) {
                                    bool2 = true;
                                    break;
                                }
                                xi2 next = it3.next();
                                boolean z3 = next.r() && next.s();
                                String t = next.t();
                                if (t.isEmpty()) {
                                    this.h.b().w().a("Event has empty param name. event", this.h.i().a(str));
                                    break;
                                }
                                Object obj = p4Var.get(t);
                                if (obj instanceof Long) {
                                    if (next.p()) {
                                        Boolean a3 = za3.a(((Long) obj).longValue(), next.q());
                                        if (a3 == null) {
                                            break;
                                        } else if (a3.booleanValue() == z3) {
                                            bool2 = false;
                                            break;
                                        }
                                    } else {
                                        this.h.b().w().a("No number filter for long param. event, param", this.h.i().a(str), this.h.i().b(t));
                                        break;
                                    }
                                } else if (obj instanceof Double) {
                                    if (next.p()) {
                                        Boolean a4 = za3.a(((Double) obj).doubleValue(), next.q());
                                        if (a4 == null) {
                                            break;
                                        } else if (a4.booleanValue() == z3) {
                                            bool2 = false;
                                            break;
                                        }
                                    } else {
                                        this.h.b().w().a("No number filter for double param. event, param", this.h.i().a(str), this.h.i().b(t));
                                        break;
                                    }
                                } else if (obj instanceof String) {
                                    if (!next.n()) {
                                        if (!next.p()) {
                                            this.h.b().w().a("No filter for String param. event, param", this.h.i().a(str), this.h.i().b(t));
                                            break;
                                        }
                                        String str2 = (String) obj;
                                        if (!ia3.a(str2)) {
                                            this.h.b().w().a("Invalid param value for number filter. event, param", this.h.i().a(str), this.h.i().b(t));
                                            break;
                                        }
                                        bool = za3.a(str2, next.q());
                                    } else {
                                        bool = za3.a((String) obj, next.o(), this.h.b());
                                    }
                                    if (bool == null) {
                                        break;
                                    } else if (bool.booleanValue() == z3) {
                                        bool2 = false;
                                        break;
                                    }
                                } else if (obj == null) {
                                    this.h.b().B().a("Missing param for filter. event, param", this.h.i().a(str), this.h.i().b(t));
                                    bool2 = false;
                                } else {
                                    this.h.b().w().a("Unknown param type. event, param", this.h.i().a(str), this.h.i().b(t));
                                }
                            }
                        } else {
                            oj2 next2 = it2.next();
                            if (hashSet.contains(next2.n())) {
                                if (!next2.q()) {
                                    if (!next2.s()) {
                                        if (!next2.o()) {
                                            this.h.b().w().a("Unknown value for param. event, param", this.h.i().a(str), this.h.i().b(next2.n()));
                                            break;
                                        }
                                        p4Var.put(next2.n(), next2.p());
                                    } else {
                                        p4Var.put(next2.n(), next2.s() ? Double.valueOf(next2.t()) : null);
                                    }
                                } else {
                                    p4Var.put(next2.n(), next2.q() ? Long.valueOf(next2.r()) : null);
                                }
                            }
                        }
                    }
                } else {
                    xi2 next3 = it.next();
                    if (next3.t().isEmpty()) {
                        this.h.b().w().a("null or empty param name in filter. event", this.h.i().a(str));
                        break;
                    }
                    hashSet.add(next3.t());
                }
            }
            this.h.b().B().a("Event filter result", bool2 != null ? "null" : bool2);
            if (bool2 != null) {
            }
        } else {
            v43 B = this.h.b().B();
            Integer valueOf = Integer.valueOf(this.b);
            if (this.g.n()) {
                bool2 = Integer.valueOf(this.g.o());
            }
            B.a("Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID", valueOf, bool2);
            return true;
        }
    }
}
