package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ub4 extends tb4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131362561, 1);
        y.put(2131362442, 2);
        y.put(2131362240, 3);
        y.put(2131362558, 4);
        y.put(2131362007, 5);
        y.put(2131362294, 6);
        y.put(2131362981, 7);
        y.put(2131362882, 8);
    }
    */

    @DexIgnore
    public ub4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 9, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ub4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[3], objArr[6], objArr[2], objArr[4], objArr[1], objArr[0], objArr[8], objArr[7]);
        this.w = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
