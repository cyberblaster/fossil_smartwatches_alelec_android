package com.fossil;

import android.widget.RadioGroup;
import com.portfolio.platform.view.AlertDialogFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class iy5 implements RadioGroup.OnCheckedChangeListener {
    @DexIgnore
    private /* final */ /* synthetic */ AlertDialogFragment a;
    @DexIgnore
    private /* final */ /* synthetic */ Integer b;

    @DexIgnore
    public /* synthetic */ iy5(AlertDialogFragment alertDialogFragment, Integer num) {
        this.a = alertDialogFragment;
        this.b = num;
    }

    @DexIgnore
    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.a.a(this.b, radioGroup, i);
    }
}
