package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk2 implements fk2 {
    @DexIgnore
    public static /* final */ Map<String, yk2> f; // = new p4();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences.OnSharedPreferenceChangeListener b; // = new xk2(this);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, ?> d;
    @DexIgnore
    public /* final */ List<gk2> e; // = new ArrayList();

    @DexIgnore
    public yk2(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
        this.a.registerOnSharedPreferenceChangeListener(this.b);
    }

    @DexIgnore
    public static yk2 a(Context context, String str) {
        yk2 yk2;
        SharedPreferences sharedPreferences;
        if (!((!bk2.a() || str.startsWith("direct_boot:")) ? true : bk2.a(context))) {
            return null;
        }
        synchronized (yk2.class) {
            yk2 = f.get(str);
            if (yk2 == null) {
                if (str.startsWith("direct_boot:")) {
                    if (bk2.a()) {
                        context = context.createDeviceProtectedStorageContext();
                    }
                    sharedPreferences = context.getSharedPreferences(str.substring(12), 0);
                } else {
                    sharedPreferences = context.getSharedPreferences(str, 0);
                }
                yk2 = new yk2(sharedPreferences);
                f.put(str, yk2);
            }
        }
        return yk2;
    }

    @DexIgnore
    public final Object zza(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    map = this.a.getAll();
                    this.d = map;
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    @DexIgnore
    public static synchronized void a() {
        synchronized (yk2.class) {
            for (yk2 next : f.values()) {
                next.a.unregisterOnSharedPreferenceChangeListener(next.b);
            }
            f.clear();
        }
    }

    @DexIgnore
    public final /* synthetic */ void a(SharedPreferences sharedPreferences, String str) {
        synchronized (this.c) {
            this.d = null;
            pk2.c();
        }
        synchronized (this) {
            for (gk2 zza : this.e) {
                zza.zza();
            }
        }
    }
}
