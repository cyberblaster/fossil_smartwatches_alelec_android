package com.fossil;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr6 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;

    @DexIgnore
    public nr6(OkHttpClient okHttpClient) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        as6 as6 = (as6) chain;
        yq6 t = as6.t();
        tr6 h = as6.h();
        return as6.a(t, h, h.a(this.a, chain, !t.e().equals("GET")), h.c());
    }
}
