package com.fossil;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import com.facebook.internal.FileLruCache;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ak2 implements fk2 {
    @DexIgnore
    public static /* final */ Map<Uri, ak2> g; // = new p4();
    @DexIgnore
    public static /* final */ String[] h; // = {FileLruCache.HEADER_CACHEKEY_KEY, "value"};
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentObserver c; // = new ck2(this, (Handler) null);
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public volatile Map<String, String> e;
    @DexIgnore
    public /* final */ List<gk2> f; // = new ArrayList();

    @DexIgnore
    public ak2(ContentResolver contentResolver, Uri uri) {
        this.a = contentResolver;
        this.b = uri;
        contentResolver.registerContentObserver(uri, false, this.c);
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|3|(5:5|6|7|8|9)|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0018 */
    public static ak2 a(ContentResolver contentResolver, Uri uri) {
        ak2 ak2;
        synchronized (ak2.class) {
            ak2 = g.get(uri);
            if (ak2 == null) {
                ak2 ak22 = new ak2(contentResolver, uri);
                try {
                    g.put(uri, ak22);
                } catch (SecurityException unused) {
                }
                ak2 = ak22;
            }
        }
        return ak2;
    }

    @DexIgnore
    public static synchronized void e() {
        synchronized (ak2.class) {
            for (ak2 next : g.values()) {
                next.a.unregisterContentObserver(next.c);
            }
            g.clear();
        }
    }

    @DexIgnore
    public final void b() {
        synchronized (this.d) {
            this.e = null;
            pk2.c();
        }
        synchronized (this) {
            for (gk2 zza : this.f) {
                zza.zza();
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Map c() {
        Map map;
        Cursor query = this.a.query(this.b, h, (String) null, (String[]) null, (String) null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            if (count <= 256) {
                map = new p4(count);
            } else {
                map = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                map.put(query.getString(0), query.getString(1));
            }
            query.close();
            return map;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final Map<String, String> d() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            Map<String, String> map = (Map) ik2.a(new ek2(this));
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return map;
        } catch (SQLiteException | IllegalStateException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return null;
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object zza(String str) {
        return a().get(str);
    }

    @DexIgnore
    public final Map<String, String> a() {
        Map<String, String> map = this.e;
        if (map == null) {
            synchronized (this.d) {
                map = this.e;
                if (map == null) {
                    map = d();
                    this.e = map;
                }
            }
        }
        if (map != null) {
            return map;
        }
        return Collections.emptyMap();
    }
}
