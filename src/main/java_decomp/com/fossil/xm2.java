package com.fossil;

import com.fossil.zm2;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm2<T extends zm2<T>> {
    @DexIgnore
    public static /* final */ xm2 d; // = new xm2(true);
    @DexIgnore
    public /* final */ jp2<T, Object> a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public xm2() {
        this.a = jp2.c(16);
    }

    @DexIgnore
    public static <T extends zm2<T>> xm2<T> g() {
        return d;
    }

    @DexIgnore
    public final void a() {
        if (!this.b) {
            this.a.a();
            this.b = true;
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final Iterator<Map.Entry<T, Object>> c() {
        if (this.c) {
            return new wn2(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        xm2 xm2 = new xm2();
        if (this.a.c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = this.a.d().iterator();
            if (!it.hasNext()) {
                xm2.c = this.c;
                return xm2;
            }
            Map.Entry next = it.next();
            xm2.a((zm2) next.getKey(), next.getValue());
            throw null;
        }
        Map.Entry<T, Object> a2 = this.a.a(0);
        xm2.a((zm2) a2.getKey(), a2.getValue());
        throw null;
    }

    @DexIgnore
    public final Iterator<Map.Entry<T, Object>> d() {
        if (this.c) {
            return new wn2(this.a.e().iterator());
        }
        return this.a.e().iterator();
    }

    @DexIgnore
    public final boolean e() {
        if (this.a.c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = this.a.d().iterator();
            if (!it.hasNext()) {
                return true;
            }
            b(it.next());
            throw null;
        }
        b(this.a.a(0));
        throw null;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof xm2)) {
            return false;
        }
        return this.a.equals(((xm2) obj).a);
    }

    @DexIgnore
    public final int f() {
        if (this.a.c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = this.a.d().iterator();
            if (!it.hasNext()) {
                return 0;
            }
            c(it.next());
            throw null;
        }
        c(this.a.a(0));
        throw null;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public static <T extends zm2<T>> boolean b(Map.Entry<T, Object> entry) {
        ((zm2) entry.getKey()).zzc();
        throw null;
    }

    @DexIgnore
    public xm2(boolean z) {
        this(jp2.c(0));
        a();
    }

    @DexIgnore
    public static int b(zm2<?> zm2, Object obj) {
        zm2.zzb();
        throw null;
    }

    @DexIgnore
    public static int c(Map.Entry<T, Object> entry) {
        entry.getValue();
        ((zm2) entry.getKey()).zzc();
        throw null;
    }

    @DexIgnore
    public final void a(T t, Object obj) {
        t.zzd();
        throw null;
    }

    @DexIgnore
    public xm2(jp2<T, Object> jp2) {
        this.a = jp2;
        a();
    }

    @DexIgnore
    public final void a(xm2<T> xm2) {
        if (xm2.a.c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = xm2.a.d().iterator();
            if (it.hasNext()) {
                a(it.next());
                throw null;
            }
            return;
        }
        a(xm2.a.a(0));
        throw null;
    }

    @DexIgnore
    public final void a(Map.Entry<T, Object> entry) {
        zm2 zm2 = (zm2) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof rn2) {
            rn2 rn2 = (rn2) value;
            rn2.c();
            throw null;
        }
        zm2.zzd();
        throw null;
    }
}
