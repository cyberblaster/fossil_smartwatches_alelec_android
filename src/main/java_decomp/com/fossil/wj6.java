package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wj6 extends vj6 {
    @DexIgnore
    public static final Byte a(String str) {
        wg6.b(str, "$this$toByteOrNull");
        return a(str, 10);
    }

    @DexIgnore
    public static final Integer b(String str) {
        wg6.b(str, "$this$toIntOrNull");
        return b(str, 10);
    }

    @DexIgnore
    public static final Long c(String str) {
        wg6.b(str, "$this$toLongOrNull");
        return c(str, 10);
    }

    @DexIgnore
    public static final Byte a(String str, int i) {
        int intValue;
        wg6.b(str, "$this$toByteOrNull");
        Integer b = b(str, i);
        if (b == null || (intValue = b.intValue()) < -128 || intValue > 127) {
            return null;
        }
        return Byte.valueOf((byte) intValue);
    }

    @DexIgnore
    public static final Integer b(String str, int i) {
        boolean z;
        int i2;
        wg6.b(str, "$this$toIntOrNull");
        cj6.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        int i3 = 0;
        char charAt = str.charAt(0);
        int i4 = -2147483647;
        int i5 = 1;
        if (charAt >= '0') {
            z = false;
            i5 = 0;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                i4 = Integer.MIN_VALUE;
                z = true;
            } else if (charAt != '+') {
                return null;
            } else {
                z = false;
            }
        }
        int i6 = -59652323;
        while (i5 < length) {
            int a = cj6.a(str.charAt(i5), i);
            if (a < 0) {
                return null;
            }
            if ((i3 < i6 && (i6 != -59652323 || i3 < (i6 = i4 / i))) || (i2 = i3 * i) < i4 + a) {
                return null;
            }
            i3 = i2 - a;
            i5++;
        }
        return z ? Integer.valueOf(i3) : Integer.valueOf(-i3);
    }

    @DexIgnore
    public static final Long c(String str, int i) {
        String str2 = str;
        int i2 = i;
        wg6.b(str2, "$this$toLongOrNull");
        cj6.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        boolean z = false;
        char charAt = str2.charAt(0);
        long j = -9223372036854775807L;
        int i3 = 1;
        if (charAt >= '0') {
            i3 = 0;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                j = Long.MIN_VALUE;
                z = true;
            } else if (charAt != '+') {
                return null;
            }
        }
        long j2 = -256204778801521550L;
        long j3 = 0;
        long j4 = -256204778801521550L;
        while (i3 < length) {
            int a = cj6.a(str2.charAt(i3), i2);
            if (a < 0) {
                return null;
            }
            if (j3 < j4) {
                if (j4 == j2) {
                    j4 = j / ((long) i2);
                    if (j3 < j4) {
                    }
                }
                return null;
            }
            long j5 = j3 * ((long) i2);
            long j6 = (long) a;
            if (j5 < j + j6) {
                return null;
            }
            j3 = j5 - j6;
            i3++;
            j2 = -256204778801521550L;
        }
        return z ? Long.valueOf(j3) : Long.valueOf(-j3);
    }
}
