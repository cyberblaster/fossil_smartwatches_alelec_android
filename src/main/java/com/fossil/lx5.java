package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.ReturnCodeRangeChecker;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.ColorPickerDialog;
import com.portfolio.platform.view.NumberPickerLarge;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ String b; // = b;
    @DexIgnore
    public static /* final */ lx5 c; // = new lx5();

    /*
    static {
        String simpleName = lx5.class.getSimpleName();
        wg6.a((Object) simpleName, "DialogUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void A(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886681);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886574);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        a(fragmentManager, a2, a3);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void B(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886650));
        fVar.b(2131363190);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.a(false);
        fVar.a(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void C(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, Constants.ACTIVITY);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887137));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886752));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "WRONG_FORMAT_PASSWORD");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void D(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        nh6 _nh6 = nh6.a;
        String a2 = jm4.a(PortfolioApp.get.instance().getApplicationContext(), com.fossil.wearables.fossil.R.string.Customization_Complications_Addphotomenu_Text__UpToNumberPhotosCanBe);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026t__UpToNumberPhotosCanBe)");
        Object[] objArr = {20};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, format);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886596));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void E(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887137));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886669));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "PROCESS_IMAGE_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void F(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886126));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886646));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "QUICK_RESPONSE_WARNING");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void G(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886083));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886225));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "MAX_NUMBER_OF_ALARMS");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void H(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886864));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886145));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886144));
        fVar.a(fragmentManager, "CONFIRM_REMOVE_WATCH_FACE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void I(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887058));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131887332));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886829));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.a(fragmentManager, "REQUEST_CONTACT_PHONE_SMS_PERMISSION");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void J(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886595);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026ourSmartwatchsFindDevice)");
        c(fragmentManager, a2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void K(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886694);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886695);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        b(fragmentManager, a2, a3);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void L(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886694);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886574);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        b(fragmentManager, a2, a3);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void M(FragmentManager fragmentManager) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886647));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886648));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886646));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void N(FragmentManager fragmentManager) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887137));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886744));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void O(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragment");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887137));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131887312));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "CONFIRM_SET_ALARM_FAILED");
    }

    @DexIgnore
    public final void P(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558617);
        fVar.b(2131362561);
        fVar.b(2131362561);
        fVar.a(fragmentManager, "DEVICE_SET_DATA_FAILED");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void Q(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887314));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131887313));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131887295));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886709));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "SET TO WATCH FAIL");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void R(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886650));
        fVar.b(2131363190);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.a(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void S(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886167));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886166));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886165));
        fVar.a(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void T(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886558));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886557));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886554));
        fVar.a(false);
        fVar.b(2131363190);
        fVar.a(fragmentManager, "FIRMWARE_UPDATE_FAIL");
    }

    @DexIgnore
    public final String a() {
        return b;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(String str, FragmentManager fragmentManager) {
        wg6.b(str, "serial");
        wg6.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886590));
        nh6 _nh6 = nh6.a;
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886591);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026AnActiveDeviceIsDisabled)");
        Object[] objArr = {str};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363129, format);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886588));
        fVar.b(2131363190);
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886586));
        fVar.b(2131363105);
        fVar.a(fragmentManager, "REMOVE_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(String str, FragmentManager fragmentManager) {
        wg6.b(str, "serial");
        wg6.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886584));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886583));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886582));
        fVar.b(2131363190);
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886581));
        fVar.b(2131363105);
        fVar.a(fragmentManager, "REMOVE_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d(String str, FragmentManager fragmentManager) {
        wg6.b(str, "serial");
        wg6.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886584));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886583));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886582));
        fVar.b(2131363190);
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886581));
        fVar.b(2131363105);
        fVar.a(fragmentManager, "SWITCH_DEVICE_ERASE_FAIL", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void e(String str, FragmentManager fragmentManager) {
        wg6.b(str, "serial");
        wg6.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886589));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886593));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886588));
        fVar.b(2131363190);
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886586));
        fVar.b(2131363105);
        fVar.a(fragmentManager, "SWITCH_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void f(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131887131));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131887234));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131887059));
        fVar.a(fragmentManager, "DELETE_THEME");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void g(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558486);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131887132));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131887234));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131887059));
        fVar.a(fragmentManager, "APPLY_NEW_COLOR_THEME");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void h(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886850));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886849));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886845));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886844));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_DELETE_ACCOUNT");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void i(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886101));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886100));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886099));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_DELETE_ALARM");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void j(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886600));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886599));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886598));
        fVar.a(fragmentManager, "DOWNLOAD");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void k(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886614));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886613));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "SEARCH_ON_STORE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void l(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886649));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886978));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "FAIL_DUE_TO_DEVICE_DISCONNECTED");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void m(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886645));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886644));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886643));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886642));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "FEEDBACK_CONFIRM");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void n(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886502));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886501));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "GOAL_TRACKING_ADD_FUTURE_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void o(FragmentManager fragmentManager) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886562));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886597));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886560));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void p(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886641));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886638));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886637));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886635));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "HAPPINESS_CONFIRM");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void q(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886348));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886198));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "COMMUTE_TIME_LOAD_LOCATION_FAIL");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void r(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886226));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886225));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "LOCATION_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void s(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886200));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886198));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void t(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886199));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886198));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void u(FragmentManager fragmentManager) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558490);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886562));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886561));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886560));
        fVar.b(2131363190);
        fVar.a(false);
        fVar.a(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void v(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886864));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886863));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886862));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_LOGOUT_ACCOUNT");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void w(FragmentManager fragmentManager) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.b(2131363190);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886562));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886561));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886563));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886564));
        fVar.b(2131363105);
        fVar.a(fragmentManager, "NO_INTERNET_CONNECTION");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void x(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.a(2131363129, jm4.a(PortfolioApp.get.instance().getApplicationContext(), 2131886301));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886596));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void y(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886656));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886657));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886829));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.a(fragmentManager, "REQUEST_NOTIFICATION_ACCESS");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void z(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886667);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886695);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        a(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i) {
        wg6.b(fragmentManager, "fragmentManager");
        ColorPickerDialog.k i1 = ColorPickerDialog.i1();
        i1.c(0);
        i1.a(false);
        i1.b(i);
        i1.a(-65536);
        i1.b(false);
        i1.a().show(fragmentManager, "COLOR_PICKER_DIALOG");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, int i2, String str) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(str, "emailAddress");
        DashbarData dashbarData = new DashbarData(2131362820, i, i2);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558551);
        fVar.a(2131362338, str);
        fVar.a(dashbarData);
        fVar.b(2131362561);
        fVar.b(2131361935);
        fVar.b(true);
        fVar.a(false);
        fVar.c(R.color.transparent);
        wg6.a((Object) fVar, "AlertDialogFragment.Buil\u2026olor(R.color.transparent)");
        AlertDialogFragment a2 = fVar.a("EMAIL_OTP_VERIFICATION");
        wg6.a((Object) a2, "builder.create(EMAIL_OTP_VERIFICATION)");
        a2.setStyle(0, 2131951629);
        a2.show(fragmentManager, "EMAIL_OTP_VERIFICATION");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void f(String str, FragmentManager fragmentManager) {
        wg6.b(str, "serial");
        wg6.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886584));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886583));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886582));
        fVar.b(2131363190);
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886581));
        fVar.b(2131363105);
        fVar.a(fragmentManager, "SWITCH_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(FragmentManager fragmentManager, String str, String str2) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, str2);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886565));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886566));
        fVar.a(fragmentManager, "REQUEST_LOCATION_SERVICE_PERMISSION");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(FragmentManager fragmentManager, String str) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363218, str);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886570));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886571));
        fVar.a(fragmentManager, b);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886640));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886639));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886636));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886634));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "APP_RATING_CONFIRM");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void e(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, Constants.ACTIVITY);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.b(2131363105);
        fVar.b(2131363190);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886666));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886696));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886829));
        fVar.a(false);
        fVar.a(fragmentManager, "BLUETOOTH_OFF");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(FragmentManager fragmentManager, String str) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(str, "deviceName");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886936));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886935));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886934));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886933));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_REMOVE_DEVICE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886594));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886592));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886587));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886585));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(false);
        AlertDialogFragment a2 = fVar.a(fragmentManager, "ASK_TO_CANCEL_WORKOUT");
        wg6.a((Object) a2, "AlertDialogFragment.Buil\u2026r, ASK_TO_CANCEL_WORKOUT)");
        a2.setCancelable(false);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, String str, String str2) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363218, str);
        fVar.a(2131363129, str2);
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886829));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
        fVar.a(fragmentManager, "REQUEST_OPEN_LOCATION_SERVICE");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886167));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886166));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886165));
        fVar.a(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void a(int i, String str, FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                o(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        w(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            o(fragmentManager);
                            return;
                        } else if (str != null) {
                            a(str, fragmentManager);
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                }
                M(fragmentManager);
            } else {
                N(fragmentManager);
            }
        }
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, String str) {
        wg6.b(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                o(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        u(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            o(fragmentManager);
                            return;
                        } else if (str != null) {
                            a(str, fragmentManager);
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                }
                M(fragmentManager);
            } else {
                N(fragmentManager);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(String str, FragmentManager fragmentManager) {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886562));
        fVar.a(2131363129, str);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886560));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "SERVER_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, ContactGroup contactGroup) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(contactGroup, "contactGroup");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE", contactGroup);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131887288));
        nh6 _nh6 = nh6.a;
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131887289);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026move_contact_description)");
        Object obj = contactGroup.getContacts().get(0);
        wg6.a(obj, "contactGroup.contacts[0]");
        Object[] objArr = {((Contact) obj).getDisplayName()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363129, format);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886934));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886376));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_REMOVE_CONTACT", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, String str) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(str, "description");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558484);
        fVar.a(2131363218, "");
        fVar.a(2131363129, str);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886114));
        fVar.b(2131363190);
        fVar.a(fragmentManager, "DND_SCHEDULED_TIME_ERROR");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, wx4 wx4, int i, int i2) {
        String str;
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(wx4, "contactWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER", wx4);
        Contact contact = wx4.getContact();
        if (contact == null || contact.getContactId() != -100) {
            Contact contact2 = wx4.getContact();
            if (contact2 == null || contact2.getContactId() != -200) {
                Contact contact3 = wx4.getContact();
                if (contact3 != null) {
                    str = contact3.getDisplayName();
                    wg6.a((Object) str, "contactWrapper.contact!!.displayName");
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                str = jm4.a((Context) PortfolioApp.get.instance(), 2131886148);
                wg6.a((Object) str, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
            }
        } else {
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131886147);
            wg6.a((Object) str, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
        }
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        nh6 _nh6 = nh6.a;
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886146);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        Object[] objArr = {str, Integer.valueOf(i), Integer.valueOf(i2)};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363129, format);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886145));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886144));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_REASSIGN_CONTACT", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, String str, int i, int i2, AppWrapper appWrapper) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(str, "name");
        wg6.b(appWrapper, "appWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_APPWRAPPER", appWrapper);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        nh6 _nh6 = nh6.a;
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886146);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        Object[] objArr = {str, String.valueOf(i), String.valueOf(i2)};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363129, format);
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886145));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886144));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "CONFIRM_REASSIGN_APP", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, int i2, int i3, String[] strArr) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(strArr, "displayedValues");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558480);
        fVar.b(2131362204);
        fVar.b(2131362850);
        fVar.a(2131362748, 1, 12, i);
        fVar.a(2131362749, 0, 59, i2);
        fVar.a(2131362751, 0, 1, i3, NumberPickerLarge.getTwoDigitFormatter(), strArr);
        fVar.a(fragmentManager, "GOAL_TRACKING_ADD");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, GoalTrackingData goalTrackingData) {
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(goalTrackingData, "goalTrackingData");
        Bundle bundle = new Bundle();
        bundle.putSerializable("GOAL_TRACKING_DELETE_BUNDLE", goalTrackingData);
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
        fVar.a(2131363218, jm4.a((Context) PortfolioApp.get.instance(), 2131886506));
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886505));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886504));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886503));
        fVar.b(2131363190);
        fVar.b(2131363105);
        fVar.a(fragmentManager, "GOAL_TRACKING_DELETE", bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager) {
        wg6.b(fragmentManager, "fragmentManager");
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        fVar.b(2131363190);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886152));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886151));
        fVar.a(fragmentManager, "NOTIFICATION_WARNING");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v25, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(FragmentManager fragmentManager, sh4 sh4, int i) {
        String str;
        wg6.b(fragmentManager, "fragmentManager");
        wg6.b(sh4, "type");
        throw null;
        // int i2 = kx5.a[sh4.ordinal()];
        // if (i2 == 1) {
        //     nh6 _nh6 = nh6.a;
        //     String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886874);
        //     wg6.a((Object) a2, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
        //     Object[] objArr = {Integer.valueOf(i)};
        //     str = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        //     wg6.a((Object) str, "java.lang.String.format(format, *args)");
        // } else if (i2 == 2) {
        //     nh6 nh62 = nh6.a;
        //     String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886865);
        //     wg6.a((Object) a3, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
        //     Object[] objArr2 = {Integer.valueOf(i)};
        //     str = String.format(a3, Arrays.copyOf(objArr2, objArr2.length));
        //     wg6.a((Object) str, "java.lang.String.format(format, *args)");
        // } else if (i2 == 3) {
        //     nh6 nh63 = nh6.a;
        //     String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886867);
        //     wg6.a((Object) a4, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
        //     Object[] objArr3 = {Integer.valueOf(i)};
        //     str = String.format(a4, Arrays.copyOf(objArr3, objArr3.length));
        //     wg6.a((Object) str, "java.lang.String.format(format, *args)");
        // } else if (i2 == 4) {
        //     nh6 nh64 = nh6.a;
        //     String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886869);
        //     wg6.a((Object) a5, "LanguageHelper.getString\u2026t__PleaseSetASleepGoalOf)");
        //     Object[] objArr4 = {Integer.valueOf(i)};
        //     str = String.format(a5, Arrays.copyOf(objArr4, objArr4.length));
        //     wg6.a((Object) str, "java.lang.String.format(format, *args)");
        // } else if (i2 != 5) {
        //     str = "";
        // } else {
        //     nh6 nh65 = nh6.a;
        //     String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886882);
        //     wg6.a((Object) a6, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
        //     Object[] objArr5 = {Integer.valueOf(i)};
        //     str = String.format(a6, Arrays.copyOf(objArr5, objArr5.length));
        //     wg6.a((Object) str, "java.lang.String.format(format, *args)");
        // }
        // AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
        // fVar.b(2131363190);
        // fVar.a(2131363129, str);
        // fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886873));
        // fVar.a(fragmentManager, "NOTIFICATION_WARNING");
    }
}
