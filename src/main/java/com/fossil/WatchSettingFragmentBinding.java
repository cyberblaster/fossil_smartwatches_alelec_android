package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WatchSettingFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView fw_version_value;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleTextView serial_value;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleButton w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ScrollView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexAdd
    public FlexibleTextView secret_key_value;

    @DexAdd
    public FlexibleSwitchCompat switchAndroidDND;
    @DexAdd
    public FlexibleSwitchCompat switchAndroidDND_HIGH;
    @DexAdd
    public FlexibleSwitchCompat switchEmptyNotifications;
    @DexAdd
    public FlexibleSwitchCompat switchAutoSync;


    @DexAdd
    public WatchSettingFragmentBinding(Object obj, View view, int i, FlexibleButton flexibleButton, RTLImageView rTLImageView, FlexibleButton flexibleButton2, ConstraintLayout constraintLayout, CardView cardView, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleButton flexibleButton5, ImageView imageView, ScrollView scrollView, View view2, View view3, View view4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, FlexibleSwitchCompat switchAndroidDND, FlexibleSwitchCompat switchAndroidDND_HIGH, FlexibleSwitchCompat switchEmptyNotifications, FlexibleSwitchCompat switchAutoSync) {
        this(obj, view, i, flexibleButton, rTLImageView, flexibleButton2, constraintLayout, cardView, flexibleButton3, flexibleButton4, flexibleButton5, imageView, scrollView, view2, view3, view4, flexibleTextView, flexibleTextView2, flexibleTextView3, flexibleTextView4, flexibleTextView5, flexibleTextView6, flexibleTextView7, flexibleTextView8, flexibleTextView9, flexibleTextView10, flexibleTextView11, flexibleTextView12, flexibleTextView13, flexibleTextView14);
        this.secret_key_value = flexibleTextView15;
        this.switchAndroidDND = switchAndroidDND;
        this.switchAndroidDND_HIGH = switchAndroidDND_HIGH;
        this.switchEmptyNotifications = switchEmptyNotifications;
        this.switchAutoSync = switchAutoSync;
    }

    @DexIgnore
    public WatchSettingFragmentBinding(Object obj, View view, int i, FlexibleButton flexibleButton, RTLImageView rTLImageView, FlexibleButton flexibleButton2, ConstraintLayout constraintLayout, CardView cardView, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleButton flexibleButton5, ImageView imageView, ScrollView scrollView, View view2, View view3, View view4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = rTLImageView;
        this.s = flexibleButton2;
        this.t = constraintLayout;
        this.u = flexibleButton3;
        this.v = flexibleButton4;
        this.w = flexibleButton5;
        this.x = imageView;
        this.y = scrollView;
        this.z = flexibleTextView;
        this.A = flexibleTextView3;
        this.B = flexibleTextView4;
        this.C = flexibleTextView5;
        this.fw_version_value = flexibleTextView7;
        this.E = flexibleTextView9;
        this.F = flexibleTextView10;
        this.serial_value = flexibleTextView12;
        this.H = flexibleTextView14;
    }
}
