package com.portfolio.platform.uirenew.alarm;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.MutableLiveData;

import com.fossil.AlarmPresenter;
import com.fossil.bk4;
import com.fossil.ov4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rv4;
import com.fossil.wearables.fossil.R;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

import static android.provider.AlarmClock.ACTION_SET_ALARM;
import static android.provider.AlarmClock.ACTION_SET_TIMER;
import static android.provider.AlarmClock.ACTION_SHOW_ALARMS;
import static android.provider.AlarmClock.EXTRA_DAYS;
import static android.provider.AlarmClock.EXTRA_HOUR;
import static android.provider.AlarmClock.EXTRA_MINUTES;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public AlarmPresenter B;

    @DexAdd
    Alarm auto_set;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        public final void a(Context context, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
            wg6.b(context, "context");
            wg6.b(str, "deviceId");
            wg6.b(arrayList, "currentAlarms");
            Intent intent = new Intent(context, AlarmActivity.class);
            intent.putExtra("EXTRA_DEVICE_ID", str);
            intent.putExtra("EXTRA_ALARM", alarm);
            intent.putParcelableArrayListExtra("EXTRA_ALARMS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.alarm.AlarmActivity, androidx.fragment.app.FragmentActivity] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.ArrayList, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /*
    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        String stringExtra = getIntent().getStringExtra("EXTRA_DEVICE_ID");
        Alarm alarm = (Alarm) getIntent().getParcelableExtra("EXTRA_ALARM");
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_ALARMS");
        AlarmFragment b = (AlarmFragment)getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = AlarmFragment.q.b();
            a(b, AlarmFragment.q.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            wg6.a((Object) stringExtra, "deviceId");
            wg6.a((Object) parcelableArrayListExtra, "alarms");
            g.a(new rv4(b, stringExtra, parcelableArrayListExtra, alarm)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmContract.View");
    }
    */

    @DexReplace
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.alarm.AlarmActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity); //2131558439);
        String device_id = getIntent().getStringExtra("EXTRA_DEVICE_ID");
        Alarm alarm = getIntent().getParcelableExtra("EXTRA_ALARM");
        ArrayList<Alarm> parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_ALARMS");
        // i93 a2 = (i93) getSupportFragmentManager().a(sk2.content);
        // if (a2 == null) {
        //     a2 = i93.o.b();
        //     a(a2, i93.o.a(), sk2.content);
        // }
        // xi2 g = PortfolioApp.Z.c().g();
        // if (a2 == null) {
        //     throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmContract.View");
        // }
        AlarmFragment b = (AlarmFragment)getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = AlarmFragment.q.b();
            a((AlarmFragment) b, AlarmFragment.q.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmContract.View");
        }

        if (device_id == null) {
            // Found by browsing variables in watch window...
            device_id = (String)((MutableLiveData)PortfolioApp.get.instance().B).d;
        }

        wg6.a((Object) device_id, "deviceId");
        if (parcelableArrayListExtra == null) {
            parcelableArrayListExtra = new ArrayList<>();
        }
        wg6.a((Object) parcelableArrayListExtra, "alarms");
        g.a(new rv4(b, device_id, parcelableArrayListExtra, alarm)).a(this);


        // Ref https://github.com/carlosperate/LightUpDroid-Alarm/blob/master/app/src/main/java/com/embeddedlog/LightUpDroid/HandleApiCalls.java
        Intent intent = getIntent();
        auto_set = null;
        if (intent != null) {
            if (ACTION_SET_ALARM.equals(intent.getAction())) {
                Alarm a = handleSetAlarm(intent);
                parcelableArrayListExtra.add(a);
                auto_set = a;
                // alarm = a;  // makes it act like editing existing, then complains that can't find existing
            } else if (ACTION_SHOW_ALARMS.equals(intent.getAction())) {
                handleShowAlarms();
            } else if (ACTION_SET_TIMER.equals(intent.getAction())) {
                handleSetTimer(intent);
            }
        }

        rv4 var10 = new rv4(b, device_id, parcelableArrayListExtra, alarm);
        ov4 var11 = g.a(var10);
        // jk2.g var12 = (jk2.g) var11;
        // if (this.B == null) {
        //     this.B = var12.a();
        // }

        var11.a(this);
    }

    @DexAdd
    public void onStart() {
        super.onStart();
        if (this.B != null && auto_set != null) {
            try {
                // this.B.o = auto_set;
                if (this.B.e == null) {
                    this.B.e = "Alarm";
                }
                // function with "updateTime: hourValue = " in it
                this.B.b("" + auto_set.hour, "" + auto_set.minute, false);

                if (auto_set.isRepeated) {
                    if (this.B.i == null) {
                        // function with "new SparseIntArray()"
                        this.B.i = this.B.a(auto_set.days);
                    }
                    // function with "new int[]{2, 3, 4, 5, 6, 7, 1}: "
                    this.B.a(true);
                }

                // function with "saveAlarm - uri" in it
                this.B.i();

            } finally {
                auto_set = null;
            }
        }
    }

    @DexAdd
    Alarm handleSetAlarm(Intent intent) {
        final int hour = intent.getIntExtra(EXTRA_HOUR, -1);
        final int minutes;
        if (intent.hasExtra(EXTRA_MINUTES)) {
            minutes = intent.getIntExtra(EXTRA_MINUTES, -1);
        } else {
            minutes = 0;
        }

        StringBuilder var12 = new StringBuilder();
        // AlarmPresenter pres = this.B;
        // MFUser var11 = pres.j;
        String uri = "external";
        // if (var11 != null) {
        //     uri = var11.getUserId();
        // } else {
        //     uri = null;
        // }
        var12.append(uri);
        var12.append(':');
        Calendar var9 = Calendar.getInstance();
        String createdAt = bk4.f(var9.getTime());
        var9 = Calendar.getInstance();
        wg6.a(var9, "Calendar.getInstance()");
        var12.append(var9.getTimeInMillis());
        uri = var12.toString();


        // TODO get title / message from assistant?
        String title = "";
        String message = "";
        // if (title == null) {
        //     title = "";
        // }

        int[] days;
        days = getDaysFromIntent(intent);

        boolean isRepeated = days.length != 0;

        // if (hour >= 0 && hour <= 23 && minutes >= 0 && minutes <= 59) {
        //
        // }

        Boolean isActive = true;
        // wg6.a(var3, "createdAt");
        String updatedAt = createdAt;
        int pinType = 0;
        Alarm var19 = new Alarm((String)null, uri, title, message, hour, minutes % 60, days, isActive, isRepeated, createdAt, updatedAt, pinType);//, 1024, (er4)null);

        return var19;

    }

    @DexAdd
    private int[] getDaysFromIntent(Intent intent) {
        final ArrayList<Integer> days = intent.getIntegerArrayListExtra(EXTRA_DAYS);
        if (days != null) {
            final int[] daysArray = new int[days.size()];
            for (int i = 0; i < days.size(); i++) {
                daysArray[i] = days.get(i);
            }
            return daysArray;
        } else {
            // API says to use an ArrayList<Integer> but we allow the user to use a int[] too.
            final int[] daysArray = intent.getIntArrayExtra(EXTRA_DAYS);
            if (daysArray == null) {
                return new int[0];
            }
            return daysArray;
        }
    }


    @DexAdd
    void handleShowAlarms() {}

    @DexAdd
    void handleSetTimer(Intent intent) {}
}
